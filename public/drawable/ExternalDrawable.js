export default {
    mtIcon:'https://api.mehrtakhfif.com/static/mtIcon.svg',
    mtTypo:'https://api.mehrtakhfif.com/static/mtTypo.svg',
    contactUsEmail:'https://api.mehrtakhfif.com/static/email.svg',
    contactUsPhone:'https://api.mehrtakhfif.com/static/headset.svg',
    contactUsAddress:'https://api.mehrtakhfif.com/static/map.svg',
}
