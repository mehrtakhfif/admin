import React, {Fragment} from "react";
import {useSnackbar} from "notistack";
import PropTypes from "prop-types";
import Button from "@material-ui/core/Button";
import {lang} from "../repository";

export function SuccessSnackWhitClose(message) {
    const {closeSnackbar} = useSnackbar();
    const action = (key) => (
        <Fragment>
            <Button onClick={() => {
                closeSnackbar(key)
            }}>
                {lang.get('close')}
            </Button>
        </Fragment>
    );
    SuccessSnack(message, action)
}

SuccessSnackWhitClose.propTypes = {
    message: PropTypes.string.isRequired,
};

export function SuccessSnack(message, action = null) {
    DefaultSnack(message, "success", action)
}

SuccessSnack.propTypes = {
    message: PropTypes.string.isRequired,
    action: PropTypes.object
};

export function DefaultSnack(message, variant = "default", action = null) {
    const {enqueueSnackbar} = useSnackbar();
    enqueueSnackbar(message);
}

DefaultSnack.propTypes = {
    message: PropTypes.string.isRequired,
    variant: PropTypes.oneOf(['default', 'success', 'error', 'info']),
    action: PropTypes.object
};
