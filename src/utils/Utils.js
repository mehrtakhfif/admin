import jwt from "jsonwebtoken";
import SHA from 'js-sha3';
import md5 from 'md5'
import _ from 'lodash'
import HTMLParser from 'html-react-parser'
import {adminPrivateKeys} from "../sv";
import {gcLog} from "./ObjectUtils";


export const UtilsKeyboardKey = {
    ENTER_KEY: 13
};


export const UtilsEncrypt = {
    HSA: {
        encode: function (key, value) {
            return jwt.sign(value, key + adminPrivateKeys.uniqueHSACode, {algorithm: 'HS256'});
        },
        decode: function (key, value) {
            return jwt.verify(value, key + adminPrivateKeys.uniqueHSACode)
        }
    },
    SHA: {
        S224: function (value) {
            return SHA.sha3_224(value);
        }
    },
    MD5: function (val) {
        return md5(val)
    }
};

export const UtilsImage = {
    getImageBrightness: function (imageSrc, callback) {
        try {
            const fuzzy = 0.1;
            const img = document.createElement("img");
            img.src = imageSrc;
            img.style.display = "none";
            img.crossOrigin = "Anonymous";
            document.body.appendChild(img);
            img.onload = function () {
                // create canvas
                const canvas = document.createElement("canvas");
                canvas.width = this.width;
                canvas.height = this.height;

                const ctx = canvas.getContext("2d");
                ctx.drawImage(this, 0, 0);

                const imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);
                const data = imageData.data;
                let r, g, b, max_rgb;
                let light = 0, dark = 0;

                for (let x = 0, len = data.length; x < len; x += 4) {
                    r = data[x];
                    g = data[x + 1];
                    b = data[x + 2];

                    max_rgb = Math.max(Math.max(r, g), b);
                    if (max_rgb < 128)
                        dark++;
                    else
                        light++;
                }

                const dl_diff = ((light - dark) / (this.width * this.height));
                if (dl_diff + fuzzy < 0)
                    callback(true); /* Dark. */
                else
                    callback(false);  /* Not dark. */
            }
        } catch (err) {
            console.log("error on getImageBrightness", err)
        }
    }
};

export const UtilsDataConverter = {
    specialProductConvertToProduct: function (props) {
        if (props === undefined)
            return;
        if (_.isArray(props)) {
            let newProps = [];
            try {
                _.forEach(props, function (product) {
                    newProps.push(UtilsDataConverter.specialProductConvertToProduct(product));
                });
                return newProps;
            } catch (err) {
                return []
            }
        }
        try {
            let item = props.storage;
            if (props.media && props.media.type === 'image') {
                item.product.thumbnail = props.media.file;
            }
            if (!_.isEmpty(props.title)) {
                item.product.name = props.title;
            }
            return item
        } catch (err) {
            return null
        }
    },
};

export const UtilsDate = {
    minuteToSecond: (min) => {
        return min * 60
    },
    secondToMillisecond: (second) => {
        return second * 1000
    },
    minuteToMillisecond: (min) => {
        return UtilsDate.secondToMillisecond(UtilsDate.minuteToSecond(min))
    },
    millisecondToSecond: (ms) => {
        return ms / 1000
    },
    secondToMinute: (second) => {
        return second / 60
    },
    millisecondToMinute: (ms) => {
        return UtilsDate.secondToMinute(UtilsDate.millisecondToSecond(ms))
    },
};

export const UtilsData = {
    createId: (data = [], idKey = "id") => {
        let id = 0;
        _.forEach(data, (d) => {
            if (d[idKey] > id)
                id = d[idKey]
        });
        return id + 1;
    },
    generateKey: (length = 5) => {
        let result = '';
        const fCharacters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        const charactersLength = characters.length;
        for (let i = 0; i < length; i++) {
            result += (i === 0 ? fCharacters : characters).charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }
};

export const UtilsFormat = {
    numberToMoney: (props) => {
        if (!props && props !== 0)
            return;
        return props.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
        // return true
    },
    moneyToNumber: (props) => {
        if (!props && props !== 0)
            return;
        return _.toInteger(props.toString().replace(/,/gi, ''))
        // return true
    },
    toMobileTemplate: (mobile, splitter = " ") => {
        try {
            return mobile.substr(0, 4) + splitter + mobile.substr(4, 3) + splitter + mobile.substr(7, 4)
        } catch (e) {
        }
    },
    toMeliCodeTemplate: (meliCode, splitter = " ") => {
        try {
            return meliCode.substr(0, 3) + splitter + meliCode.substr(3, 6) + splitter + meliCode.substr(9, 1)
        } catch (e) {
        }
    }
};

export const UtilsConverter = {

    addRem: (rem, rem2) => {
        const match1 = rem.toString().match(/\d+/g);
        const match2 = rem2.toString().match(/\d+/g);
        const num1 = parseFloat(match1[0] + ((match1[1]) ? '.' + match1[1] : ''));
        const num2 = parseFloat(match2[0] + ((match2[1]) ? '.' + match2[1] : ''));
        return (num1 * num2) + 'rem'
    }
};

export const UtilsRouter = {
    redirectRout: (props, defaultRout) => {
        console.log("redirectRout", props);
        if (!(props && ((props.location && props.location.state && props.location.state.from) || _.isString(props)))) {
            return {
                from: {
                    pathname: defaultRout
                }
            }
        }
        return {
            from: _.isString(props) ? {pathname: props} : props.location.state.from
        }
    },
    redirectRoutByWindow: (defaultRout) => {
        try {
            return {
                from: {
                    pathname: window.location.pathname,
                    search: window.location.search
                }
            }
        } catch (e) {
            console.error("UtilsRouter::redirectRoutByWindow", e);
            return {
                from: {
                    pathname: defaultRout
                }
            }
        }
        // return {
        //     from: _.isString(props) ? {pathname: props} : props.location.state.from
        // }
    },
    goTo: (history, {routUrl, props = {}}) => {
        try {
            if (!history)
                return;
            history.push(routUrl)
        } catch (e) {
            console.error("UtilsRouter::goTo", e)
        }
    }
};

export const UtilsStyle = {
    transition: (duration = 500) => {
        return {
            'WebkitTransition': 'all ' + duration + 'ms ease',
            'MozTransition': 'all ' + duration + 'ms ease',
            'MsTransition': 'all ' + duration + 'ms ease',
            'OTransition': 'all ' + duration + 'ms ease',
            transition: 'all ' + duration + 'ms ease'
        }
    },
    borderRadius: (radius) => {
        return {
            borderRadius: radius,
            WebkitBorderRadius: radius,
        }
    },
    widthFitContent: () => {
        return {
            width: 'max-content',
            whiteSpace: 'nowrap'
        }
    },
    disableTextSelection: () => {
        return {
            WebkitTouchCallout: 'none',
            WebkitUserSelect: 'none',
            KhtmlUserSelect: 'none',
            MozUserSelect: 'none',
            MsUserSelect: 'none',
            userSelect: 'none'
        }
    },
    boxShadow: (val) => {
        return {
            WebkitBoxShadow: val,
            MozBoxShadow: val,
            boxShadow: val,
        }
    }
};

export const ScrollToRef = (ref) => window.scrollTo(0, ref.current.offsetTop - 250);

export const Utils = {
    copyToClipboard: str => {
        const person = prompt("برای کپی ctrl+c بزنید", str);
        return !(person == null || person === "");
        try {
            const el = document.createElement('textarea');
            const id = `clipboard-${Math.floor(Math.random() * 1000)}`;
            el.id = id;
            el.value = str;
            document.body.appendChild(el);
            try {
                const newEl = document.querySelector(`#${id}`);
                newEl.select()
                document.execCommand('copy');
                document.body.removeChild(newEl);
            } catch (e) {
                return false
            }

            return true
        } catch (e) {
            return false
        }
    },
    copyToClipboard2: str => {
        try {
            const el = document.createElement('textarea');  // Create a <textarea> element
            const id = `copyToClipboard-${Math.floor(Math.random() * 1000)}`
            el.id = id;
            el.value = str;                                 // Set its value to the string that you want copied
            el.setAttribute('readonly', '');                // Make it readonly to be tamper-proof
            el.style.position = 'absolute';
            el.style.left = '-9999px';                      // Move outside the screen to make it invisible
            document.body.appendChild(el);                  // Append the <textarea> element to the HTML document


            setTimeout(() => {
                const selected =
                    document.getSelection().rangeCount > 0        // Check if there is any content selected previously
                        ? document.getSelection().getRangeAt(0)     // Store selection if found
                        : false;                                    // Mark as false to know no selection existed before
                el.select();                                    // Select the <textarea> content
                document.execCommand('copy');                   // Copy - only works as a result of a user action (e.g. click events)
                document.body.removeChild(el);                  // Remove the <textarea> element
                if (selected) {                                 // If a selection existed before copying
                    document.getSelection().removeAllRanges();    // Unselect everything on the HTML document
                    document.getSelection().addRange(selected);   // Restore the original selection
                }
            }, 1000);
            return true
        } catch (e) {
        }
        return false
    },
    disableScrolling: () => {
        document.getElementsByTagName("html")[0].style.overflow = 'hidden'
    },
    enableScrolling: () => {
        document.getElementsByTagName("html")[0].style.overflow = 'auto'
    },
    isIos: () => {
        var iDevices = [
            'iPad Simulator',
            'iPhone Simulator',
            'iPod Simulator',
            'iPad',
            'iPhone',
            'iPod'
        ];

        if (!!navigator.platform) {
            while (iDevices.length) {
                if (navigator.platform === iDevices.pop()) {
                    return true;
                }
            }
        }

        return false;
    },
    isAndroid: () => {
        return navigator.userAgent.toLowerCase().indexOf("android") > -1;
    },
    insert: (arr, index, newItem) => [
        ...arr.slice(0, index),
        newItem,
        ...arr.slice(index)
    ],
    randomColor: () => {
        const letters = '0123456789ABCDEF';
        let color = '#';
        for (let i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    },
};

export const UtilsParser = {
    html: (string) => {
        return HTMLParser(string)
    }
};

export function _delete(obj, prop) {
    if (_.isObject(obj)) {
        let newObj = {};
        _.forEach(obj, function (objItem, key) {
            if (_.isString(prop)) {
                if (prop !== key) {
                    newObj[key] = objItem;
                }
                return
            }
            if (_.isArray(prop)) {
                _.forEach(prop, function (prop) {
                    if (prop !== key) {
                        newObj[key] = prop;
                    }
                });
                return
            }
            newObj[key] = prop;
        });
        return newObj;
    }
    return obj
}
