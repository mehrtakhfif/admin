import _ from "lodash";
import {UtilsEncrypt} from "./Utils";
import {adminPrivateKeys} from "../sv";

const LocalStorageUtils = {
    set: function (key, value) {
        try {
            if (_.isArray(value)) {
                value = {val: value};
            }
            if (_.isObject(value)) {
                value = JSON.stringify(value)
            }
            localStorage.setItem(LocalStorageUtils.generateKey(key), LocalStorageUtils.generateValue(key, value))
        } catch (e) {
            console.error("LocalStorage::set", e)
        }
    },
    get: function (key, defaultData) {
        try {
            const data = UtilsEncrypt.HSA.decode(key, localStorage.getItem(LocalStorageUtils.generateKey(key)));
            if (data)
                return data
        } catch (e) {
        }
        return defaultData
    },
    getArray: function (key, defaultData = []) {
        try {
            const data = LocalStorageUtils.get(key, null);
            if (data && data.val)
                return data.val
        } catch (e) {
        }
        return defaultData
    },
    remove:function(key){
      localStorage.removeItem(LocalStorageUtils.generateKey(key))
    },
    removeAll: function () {
        localStorage.clear();
    },
    generateKey: function (key) {
        return UtilsEncrypt.HSA.encode(adminPrivateKeys.uniqueLocalStorage + key, key);
    },
    generateValue: function (key, value) {
        return UtilsEncrypt.HSA.encode(key, value);
    }
};

export default LocalStorageUtils;


