import React from "react";
import {DEBUG} from "../repository";
import _ from 'lodash';

export const isNumeric = (x) => {
    if (x === ""){
        return false
    }
    return !(isNaN(x)) && (typeof x !== "object") && (x !== Number.POSITIVE_INFINITY) && (x !== Number.NEGATIVE_INFINITY);
}

export const toNumberSafe = (x) => {
    return isNumeric(x) ? _.toNumber(x) : x
}

export const isElement = (element) => {
    return React.isValidElement(element);
}

export const tryIt = (fun,defaultValue) => {
    try {
        return fun()
    } catch (e) {
    }
    return defaultValue
}

export const getSafe = (fun, defaultVal) => {
    try {
        return fun()
    } catch (e) {

        return defaultVal
    }
}

function baseLog(type = "log", m) {
    try {
        if (DEBUG)
            console[type](...m)
    } catch (e) {
    }
}

export function gcLog(m1, m2) {
    baseLog("log", ["%c" + m1, "color: green", m2])
}

export function gcRedLog(m1, m2) {
    console.log("%c" + m1, "color: red", m2)
}

export function gcError(m1, m2) {
    console.error(m1, m2)
}

export function gcInfo(m1, m2) {
    console.info(m1, m2)
}

String.prototype.replaceAll = function (regex, to) {
    return this.replace(regex, to)
};
String.prototype.trimAll = function () {
    return this.replaceAll(/ /g, "").trim()
};
