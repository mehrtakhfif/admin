import JsCookies from "js-cookie";
import {UtilsEncrypt} from "./Utils";
import _ from "lodash";
import {adminPrivateKeys} from "../sv";


const UtilsCookies = {
    set: function (key, value) {
        try {
            return JsCookies.set(UtilsCookies.generateKey(key), UtilsCookies.generateValue(key, value));
        } catch (e) {
            console.error("UtilsCookies::set", e)
        }
    },
    get: function (key, defaultData) {
        try {
            const data = (UtilsEncrypt.HSA.decode(key, JsCookies.get(UtilsCookies.generateKey(key))));
            if (data)
                return data
        } catch (e) {
        }
        return defaultData
    },
    getJson: function (key, defaultData) {
        try {
            const data = (UtilsEncrypt.HSA.decode(key, JsCookies.getJSON(UtilsCookies.generateKey(key))));
            if (data)
                return data
        } catch (e) {
        }
        return defaultData
    },
    getServer: function ({key, defaultData}) {
        try {
            return _.split(JsCookies.get(key), ':', 1)[0]
        } catch (e) {
            // console.error("error storage::getServer", e)
        }
        return defaultData
    },
    remove: function (key) {
        try {
            return JsCookies.remove(UtilsCookies.generateKey(key));
        } catch (e) {
        }
    },
    removeAll: function () {
        try {
            Object.keys(JsCookies.get()).forEach(function (cookieName) {
                JsCookies.remove(cookieName);
            });
        } catch (e) {
        }
    },
    generateKey: function (key) {
        return UtilsEncrypt.HSA.encode(adminPrivateKeys.uniqueCookieCode + key, key);
    },
    generateValue: function (key, value) {
        return UtilsEncrypt.HSA.encode(key, value);
    }
};


export default UtilsCookies;
