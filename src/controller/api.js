export const api = {
    User: {
        roll: '/roll',
        supplier: '/supplier',
        Vip: {
            vipType:'/vip_type'
        },
        user:'/user'
    },
    Main: {
        Special: {
            specialProduct: "/special_product",
            allSpecialProduct: "/box_special_product",
        },
        Global: {
            slider: "/slider",
            ads: "/ads",
            search: "/search",
            Test: {
                test: "/test",
                pagination: "/pagination"
            }
        },
        Dashboard:{
            main:'/dashboard',
            productCount:'/dashboard/product_count',
            dateProductCount:'/dashboard/date_product_count',
            soldProductCount : '/dashboard/sold_product_count',
            profitSummary : '/dashboard/profit_summary',
        },
        Ads:{
            main:'/ads'
        },
        Slider:{
            main:'/slider'
        }
    },
    Products: {
        main: '/product',
        storage: '/storage',
        reviewPrice: '/review_price',
        tags: '/tag',
        HousePrice: '/house_price',
        tagGroup:"/tag_group",
        features: '/feature',
        featureGroup: '/feature_group',
        featureValue: '/feature_value',
        productFeature: '/product_feature',
        brands: '/brand',
        categories: '/category',
        invoices: '/invoice',
        invoiceProduct: '/invoice_product',
        specialProduct:'/special_product',
        discountCode:'/discount_code',
        dateRange: '/date_range'
    },
    tableFilter: '/table_filter',
    media: '/media',
    feature: '/feature',
    featureValue: '/feature_value',
    icon: '/icon',
    icon2: '/icon2',
    search: '/search',
    ordering:"/ordering",
    Settings: {
        main: '/settings',
        box: '/settings/box'
    },
    Box:{
        main:'/box',
    }
};
export default api
