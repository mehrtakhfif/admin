import React from "react";
import _ from "lodash"
import {
    ApiHelper,
    convertImage,
    convertPagination,
    convertProducts,
    getMediaTypeId,
} from "./converter";
import {serverFileTypes} from "./type";
import axios from "axios";
import api from "./api";
import {apiRout, mediaApiRout} from "../repository";
import {gcLog} from "../utils/ObjectUtils";

export const mediaDefaultStep = 24;

export const Helper = {};

const cs = {
    Dashboard: {
        productCount: async () => {
            return await axios.get(api.Main.Dashboard.productCount).then((res) => {
                return {
                    status: res.status,
                    data: res.data
                }
            })
        },
        dateProductCount: async () => {
            return await axios.get(api.Main.Dashboard.dateProductCount).then((res) => {
                return {
                    status: res.status,
                    data: res.data
                }
            })
        },
        profitSummery: async ({startTime, endTime}) => {
            return await axios.get(api.Main.Dashboard.profitSummary, {
                params: {
                    start: _.toInteger(startTime / 1000),
                    end: _.toInteger(endTime / 1000)
                }
            }).then(res => {
                return {
                    status: res.status,
                    data: res.data
                }
            })
        }
    },
    Settings: {
        get: async () => {

        }
    },
    Media: {
        upload: async ({
                           boxId,
                           title,
                           file,
                           type,
                           onUploadProgress,
                           getParams = {},
                           ...params
                       }) => {

            return cs.Media.uploads({
                category_id: boxId,
                titles: title ? [title] : undefined,
                files: [file],
                type: getMediaTypeId(type),
                onUploadProgress,
                getParams,
                ...params
            }).then((res) => {

                return {
                    status: res.status,
                    data: {
                        media: res.data.media[0]
                    }
                }
            }).catch((e) => {

            })
        },
        update: async ({
                           imageId,
                           title
                       }) => {
            return await axios.patch(api.media, {
                id: imageId,
                title: title
            }).then((res => {
                return {
                    status: res.status,
                    data: {
                        media: convertImage(res.data.media)
                    }
                }
            }))
        },
        uploads: async ({
                            boxId,
                            titles = [],
                            files,
                            type,
                            onUploadProgress,
                            getParams = {},
                            ...params
                        }) => {

            if (_.isEmpty(titles)) {
                const title = cs.Media.getImageTitle(type, boxId);
                _.forEach(files, () => {
                    const newTitle = {};
                    _.forEach(title, (t, key) => {
                        newTitle[key] = t + ` d-${new Date().getTime()}`
                    });
                    titles.push(newTitle)
                })
            }

            const config = {
                headers: {'content-type': 'multipart/form-data'},
                params: {
                    ...getParams
                }
            };
            if (onUploadProgress)
                config.onUploadProgress = onUploadProgress;

            let formData = new FormData();
            _.forEach(files, (f) => {
                formData.append('file', f);
            });
            formData.append('data', JSON.stringify({
                category_id: boxId,
                titles: titles,
                type: getMediaTypeId(type),
                ...params
            }));

            return await axios.post(api.media, formData, config).then(res => {

                const newData = [];

                _.forEach(res.data.media, (m) => {
                    newData.push(convertImage(m))
                });

                return {
                    status: res.status,
                    data: {
                        ...res.data,
                        media: newData
                    }
                }
            })
        },
        getImageTitle: (type, boxId) => {
            let title = "";
            switch (type) {
                case serverFileTypes.Image.Thumbnail.type: {
                    title = {
                        fa: `تامبنیل b-${boxId}`,
                        en: `thumbnail b-${boxId}`,
                        ar: `ظفري b-${boxId}`,
                    };
                    break
                }
                case serverFileTypes.Image.Media.type: {
                    title = {
                        fa: `رسانه b-${boxId}`,
                        en: `thumbnail b-${boxId}`,
                        ar: `الإعلام b-${boxId}`,
                    };
                    break
                }
                default: {
                    title = {
                        fa: `فایل b-${boxId}`,
                        en: `file b-${boxId}`,
                        ar: `ملف b-${boxId}`,
                    };
                }
            }
            return title;
        },
        mediaAlt: async ({id,text, ...params}) => {
            return await axios.patch(api.media, {
                id: id,
                title:{fa:text},
                ...params
            }).then((res)=>{
                return {
                    status: res.status,
                    data: res.data.data,}
            })
        },
        media: () => {
            const ap = api.media;
            const req = async ({
                                   boxId = null,
                                   type,
                                   page,
                                   order = {
                                       asc: false,
                                       orderBy: "id",
                                   },
                                   step = 20
                               }) => {
                return await axios.get(ap, {
                    params: {
                        category_id: boxId,
                        type: getMediaTypeId(type),
                        p: page,
                        s: step,
                        o: ApiHelper.convertOrder(order)
                    }
                }).then((res) => {
                    return {
                        status: res.status,
                        data: res.data.data,
                        pagination: convertPagination(res.data.pagination, page)
                    }
                })
            };
            return [ap, req]
        },
        uploadDescription: async ({
                                      boxId,
                                      title,
                                      file,
                                      onUploadProgress,
                                      getParams = {},
                                      ...params
                                  }) => {
            return await cs.Media.upload({
                boxId,
                title,
                file,
                onUploadProgress,
                getParams,
                type: serverFileTypes.Image.Description.type, ...params
            })
        }
    },
    Icon: {
        get: async ({type = "all"}) => {
            return await axios.get(mediaApiRout + '/font-icons.json').then((res) => {
                if (type !== 'all')
                    res.data = _.filter(res.data, function (o) {
                        return o[type];
                    });

                return {
                    status: res.status,
                    data: res.data
                }
            })
        }
    },
    cities: {
        get: async ({stateId}) => {
            return await axios.get(apiRout + '/cities/' + stateId).then(res => {
                const cities = [];
                _.forEach(res.data.cities, (c) => {
                    cities.push({
                        ...c,
                        label: c.name
                    })
                });

                return {
                    status: res.status,
                    data: {
                        cities: cities
                    }
                }
            })
        }
    },
    Ads: {
        get: async ({type, page = 1, step = 100, ...params}) => {
            return await axios.get(api.Main.Ads.main, {
                params: {
                    p: page,
                    s: step,
                    type: type,
                    ...params
                }
            }).then(res => {
                return {
                    status: res.status,
                    data: res.data.data,
                    pagination: convertPagination(res.data.pagination, page),
                }
            })
        },
        getActives: async ({type, ...params}) => {
            return cs.Ads.get({type: type, priority: true, o: 'priority'})
        },
        set: async ({id, type, title, url, media_id, mobile_media_id,settings}) => {
            const params = {
                id,
                type,
                title,
                url,
                media_id,
                mobile_media_id,
                settings,
            }

            if (id)
                return await axios.put(api.Main.Ads.main, {...params})
            return await axios.post(api.Main.Ads.main, {...params})
        },
        setPriorities: async ({data, ...params}) => {
            return await axios.patch(api.Main.Ads.main, {
                priorities: data,
                ...params
            })
        },
    },
    Slider: {
        get: async ({type, page = 1, step = 100, ...params}) => {
            return await axios.get(api.Main.Slider.main, {
                params: {
                    p: page,
                    s: step,
                    type: type,
                    ...params
                }
            }).then(res => {
                return {
                    status: res.status,
                    data: res.data.data,
                    pagination: convertPagination(res.data.pagination, page),
                }
            })
        },
        getActives: async ({type, ...params}) => {
            return cs.Slider.get({type: type, priority: true, o: 'priority'})
        },
        set: async ({id, type, title, url, media_id, mobile_media_id}) => {
            const params = {
                id,
                type,
                title,
                url,
                media_id,
                mobile_media_id
            }

            if (id)
                return await axios.put(api.Main.Slider.main, {...params})
            return await axios.post(api.Main.Slider.main, {...params})
        },
        setPriorities: async ({data, ...params}) => {
            return await axios.patch(api.Main.Slider.main, {
                priorities: data,
                ...params
            })
        },
    },
    Order: {
        set: async ({model, ids}) => {
            return await axios.put(api.ordering, {model, ids})
        }
    },
    search: ({searchIn, query, ...param}) => {
        const ap = api.search;
        const req = async () => {
            return await axios.get(ap, {
                params: {
                    type: searchIn,
                    q: query,
                    ...param
                }
            }).then((res) => {
                return {
                    status: res.status,
                    data: res.data,
                }
            })
        };
        return [ap + `${searchIn}-${query}`, req]
    }
};

export default cs;
