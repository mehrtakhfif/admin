import api from "./api";
import axios from "axios";
import {createMultiLanguage, createMultiLanguageObject} from "./converter";
import {siteLang, sLang} from "../repository";
import _ from 'lodash'
import {UtilsData} from "../utils/Utils";
import {gcLog} from "../utils/ObjectUtils";
import rout from "../router";


export const defProperty = {
    items: []
}

const cb = {
    get: async () => {
        return await axios.get(api.Box.main).then(res => {
            return res
        })
    },
    getActive: async () => {
        return await cb.get().then(res => {
            return _.filter(res.data.data, function (o) {
                return !o.disable;
            })
        })
    },
    getSummary:  ({boxId,}) => {
        return [
            `box-get-summary-${boxId}`,
            async ()=>{
                return await axios.get(api.Main.Dashboard.soldProductCount, {params: {category_id: boxId}}).then(res => {
                    return res
                })
            }
        ]
    },
    TagGroup: {
        get: async ({id, boxId,}) => {
            return await axios.get(api.Products.tagGroup, {
                params: {
                    id: id,
                    category_id: boxId,
                    s: 100
                }
            })
        },
        getTags: async ({id,}) => {
            return await axios.get(api.Products.tagGroup, {
                params: {
                    id: id,
                }
            }).then(res => {
                return {
                    status: res.status,
                    data: res.data.data
                }
            })
        },
        save: async (boxId, {tgId, name, tagsId}) => {
            const data = {
                id: tgId,
                category_id: boxId,
                name: name,
                tags: tagsId
            }
            if (tgId)
                return await axios.put(api.Products.tagGroup, data)
            return await axios.post(api.Products.tagGroup, data)
        }
    },
    Settings: {
        get: async (boxId) => {
            return axios.get(api.Settings.box, {
                params: {
                    id: boxId
                }
            }).then(res => {

                return {
                    status: res.status,
                    data: res.data.data
                }
            })
        },
        save: async (boxId, {settings}) => {
            return axios.patch(api.Settings.box, {
                id: boxId,
                settings: settings
            });
        },
        Template: {
            get: async (boxId) => {
                return cb.Settings.get(boxId).then(res => {
                    return {
                        ...res,
                        data: res.data.settings.template ? res.data.settings.template : {}
                    }
                })
            },
            save: async (boxId, {template}) => {

                return cb.Settings.get(boxId).then((res) => {
                    const params = {
                        ...res.data.settings,
                        template: {
                            ...res.data.settings.template ? res.data.settings.template : {},
                            ...template
                        }
                    };
                    return cb.Settings.save(boxId, {settings: params})
                })
            },
            Property: {
                save: async (boxId, {property: prData, override = false}) => {
                    return cb.Settings.get(boxId).then((res) => {
                        const template = res.data.settings.template || {};
                        const property = template.property || createMultiLanguageObject({
                            fa: _.cloneDeep(defProperty),
                            en: _.cloneDeep(defProperty),
                            ar: _.cloneDeep(defProperty)
                        });

                        const newPr = override ? prData : createMultiLanguage({
                            fa: _.cloneDeep(defProperty),
                            en: _.cloneDeep(defProperty),
                            ar: _.cloneDeep(defProperty)
                        });
                        if (!override)
                            _.forEach(property, (data, lang) => {
                                if (property[lang] && property[lang].items) {
                                    newPr[lang] = property[lang];
                                }

                                _.forEach(prData[lang].items, (it) => {
                                    newPr[lang].items.push({
                                        ...it,
                                        templateId: UtilsData.createId(newPr[siteLang].items, "templateId")
                                    })
                                })
                            });

                        template.property = newPr;

                        const params = {
                            ...res.data.settings,
                            template: {
                                ...res.data.settings.template ? res.data.settings.template : {},
                                ...template
                            }
                        };
                        return cb.Settings.save(boxId, {settings: params})
                    })
                },
                remove: (boxId, {item}) => {
                    return cb.Settings.get(boxId).then((res) => {
                        const template = res.data.settings.template || {};
                        const property = template.property || createMultiLanguageObject();
                        _.forEach(sLang, (i, lang) => {
                            _.remove(property[lang].items, (it) => it.templateId === item.templateId);
                        })
                        template.property = property;

                        const params = {
                            ...res.data.settings,
                            template: {
                                ...res.data.settings.template ? res.data.settings.template : {},
                                ...template
                            }
                        };
                        return cb.Settings.save(boxId, {settings: params})
                    })
                },
                update: async (boxId, {lastItem, newItem}) => {
                    if (!_.isEmpty(newItem[siteLang][lastItem.type])) {
                        return cb.Settings.Template.get(boxId).then((res) => {
                            const property = res.data.property;
                            if (property[siteLang][lastItem.type]) {
                                const i = _.findIndex(property[siteLang][lastItem.type], (p) => {
                                    return _.isEqual(p, lastItem)
                                });
                                if (i !== -1) {
                                    _.forEach(sLang, ({key}) => {
                                        property[key][lastItem.type][i] = newItem[key][lastItem.type][0]
                                    })
                                }
                                console.log("ggggggggggggggggggasgasnewItem property", property);

                                return cb.Settings.Template.Property.save(boxId, {property: property, override: true})
                            }
                            throw "Bad data"
                        })
                    }
                    return cb.Settings.Template.Property.remove(boxId, {item: lastItem}).then(() => {
                        return cb.Settings.Template.Property.save(boxId, {property: newItem})
                    })
                }
            }
        }
    },
};

export default cb;
