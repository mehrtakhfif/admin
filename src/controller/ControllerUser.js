import React from "react";
import api from "./api";
import {ApiHelper, convertPagination, convertUser, convertUsers} from "./converter";
import axios from "axios";
import {userUpdate} from "../redux/actions/UserAction";
import ControllerSite from "./ControllerSite";
import {productDefaultStep} from "./defualt";
import DataUtils from "../utils/DataUtils";
import ControllerProduct from "./ControllerProduct";
import {DEBUG} from "../repository";

const uc = {
    Roll: {
        get: (dispatch, {...params} = {}) => {
            return axios.get(api.User.roll, {
                params: {
                    ...params
                }
            }).then((res) => {
                const boxes = res.data.categories;
                const user = convertUser({user: res.data.user, boxes: boxes});

                dispatch(userUpdate({user: user}));
                return {
                    status: res.status,
                    user: user,
                }
            })
        }
    },
    Supplier: {
        get: () => {
            const ap = api.User.supplier;
            const req = async ({
                                   order = {
                                       asc: true,
                                       orderBy: "index",
                                   },
                                   filters = {},
                                   page = 1,
                                   step = productDefaultStep,
                                   ...params
                               }) => {
                return await axios.get(ap, {
                    params: {
                        p: page,
                        s: step,
                        o: ApiHelper.convertOrder(order),
                        ...DataUtils().TableFilter().convertForSendToServer({filters: filters}),
                        ...params,
                    }
                }).then(res => {
                    return {
                        status: 200,
                        data: convertUsers({users: res.data.data}),
                        pagination: convertPagination(res.data.pagination, page),
                    }
                })
            };
            return [ap, req]
        },
        getOne: async ({userId}) => {
            return await axios.get(api.User.supplier, {
                params: {
                    id: userId,
                }
            }).then(res => {
                return {
                    status: 200,
                    data: convertUser({user: res.data.data}),
                }
            })
        },
        set: async ({userId, username, firstName, lastName, shaba, details, isVerify, depositId}) => {
            const props = {
                id: userId,
                username: username,
                first_name: firstName,
                last_name: lastName,
                shaba: shaba,
                is_verify: isVerify,
                deposit_id: depositId,
                settings: userId ? undefined : {
                    supplier_details: details
                }
            };

            if (!userId)
                return await axios.post(api.User.supplier, props).then(res => {
                    return {
                        status: 200,
                        data: convertUser({user: res.data.data}),
                    }
                });
            return await uc.Supplier.getOne({userId: userId}).then(res => {
                props.settings = {
                    ...res.data.settings,
                    supplier_details: details
                }
                return axios.put(api.User.supplier, props).then(res => {
                    return {
                        status: 200,
                        data: convertUser({user: res.data.data}),
                    }
                })
            })
        },
        Filters: {
            get: async ({boxId, ...params} = {}) => {
                return await ControllerProduct.Filters.get({boxId: boxId, state: api.Products.main, ...params});
            },
        },
        search: ({query}) => {
            const d = ControllerSite.search({searchIn: 'supplier', query: query});
            return [d[0], async () => {
                return await d[1]().then((res) => {
                    return {
                        ...res,
                        data: {
                            ...res.data,
                            suppliers: convertUsers({users: res.data.suppliers})
                        }
                    }
                })
            }]
        }
    },
    Vip: {
        Type: {
            get: ({...params} = {}) => {
                return axios.get(api.User.Vip.vipType, {
                    params: {
                        ...params
                    }
                }).then((res) => {
                    const data = res.data;
                    return {
                        status: res.status,
                        data: data,
                    }
                })
            }

        }
    },
    user: {
        get: async ({userId}) => {
            return await axios.get(api.User.user,
                {
                    params: {
                        id: userId
                    }
                }).then(res=>{
                    return{
                        status:res.status,
                        user:convertUser({user:res.data.data})
                    }
            })
        }
    }
};

export default uc;
