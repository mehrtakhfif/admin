import {createMultiLanguageObject} from "./converter";
import _ from "lodash";

export const productDefaultStep = 20;
export const featuresDefaultStep = 20;
export const tagsDefaultStep = 20;


//region fun

export const defaultDescription = (params={}) => {

    const items = [];
    _.forEach(Object.keys(params), key => {
        if (!params[key])
            return
        items.push({
            key: key,
            value: params[key]
        });
    })
    return {
        data: createMultiLanguageObject({
            fa: {
                items: items
            }
        })
    }
}

export const defaultProperties = () => {
    return {
        data: createMultiLanguageObject({fa: {items: []}, en: {items: []}, ar: {items: []}})
    }
}


//endregion fun
