import _ from "lodash";
import { apiRout, DEBUG } from "../repository";
import {
  featuresDefaultStep,
  productDefaultStep,
  tagsDefaultStep,
} from "./defualt";
import DataUtils from "../utils/DataUtils";
import ControllerSite from "./ControllerSite";
import axios from "axios";
import {
  ApiHelper,
  convertBrand,
  convertBrands,
  convertCategories,
  convertCategory,
  convertFeature,
  convertFeatureGroup,
  convertFeatures,
  convertFeaturesGroup,
  convertInvoice,
  convertInvoiceProducts,
  convertInvoices,
  convertMultiLanguageObj,
  convertPagination,
  convertProduct,
  convertProducts,
  convertStorages,
  convertFeatureValue,
  convertTableFilters,
  createMultiLanguageObject,
  convertDiscountCodes,
  convertStorage,
  generateId,
} from "./converter";
import api from "./api";
import { gcLog, isNumeric, toNumberSafe, tryIt } from "../utils/ObjectUtils";
import { defaultDescription } from "./defualt";
import rout from "../router";

const cp = {
  Products: {
    get: ({
      boxId,
      order = {
        asc: true,
        orderBy: "id",
      },
      filters = {},
      page = 1,
      step = productDefaultStep,
      productTypes = [],
      getOnlyActiveProducts,
      ...params
    }) => {
      const ap = api.Products.main;
      const apiKey = ApiHelper.createSWRKey({
        api: ap,
        order: order,
        box: boxId,
        page,
        step,
        filters,
      });

      const req = async () => {
        let getProps = {
          p: page,
          s: step,
          category_id: boxId,
          type: productTypes,
          disable: DEBUG ? undefined : getOnlyActiveProducts ? 0 : undefined,
          ...params,
        };

        if (!productTypes || _.isEmpty(productTypes)) {
          getProps = {
            ...getProps,
            category_id: boxId,
            p: page,
            s: step,
            o: ApiHelper.convertOrder(order),
            ...DataUtils()
              .TableFilter()
              .convertForSendToServer({ filters: filters }),
          };
        }
        return await axios
          .get(ap, {
            params: {
              ...getProps,
            },
          })
          .then((res) => {
            return {
              status: 200,
              data: convertProducts(res.data.data),
              pagination: convertPagination(res.data.pagination, page),
            };
          });
      };
      return [apiKey, req];
    },
    Filters: {
      get: async ({ boxId, ...params } = {}) => {
        return await cp.Filters.get({
          boxId: boxId,
          state: api.Products.main,
          ...params,
        });
      },
    },
    reviewProduct: ({
      boxId,
      order = {
        asc: true,
        orderBy: "id",
      },
      filters = {},
      page = 1,
      step = productDefaultStep,
      reviewState,
    }) => {
      const d = cp.Products.get({
        boxId: boxId,
        state: reviewState,
        order: order,
        filters: filters,
        page: page,
        step: step,
      });
      return [
        `review-${d[0]}-re-${reviewState}`,
        () => {
          return d[1]();
        },
      ];
    },
    SpecialProducts: {
      get: async ({ boxId }) => {
        return await axios
          .get(api.Products.specialProduct, {
            params: {
              category_id: boxId,
            },
          })
          .then((res) => {
            const newList = [];
            _.forEach(res.data.data, (item) => {
              newList.push({
                ...item,
                product: convertProduct(item.product),
              });
            });
            return {
              status: res.status,
              data: newList,
              pagination: convertPagination(res.data.pagination),
            };
          });
      },
      remove: async ({ removeItem }) => {
        return await axios.delete(api.Products.specialProduct, {
          params: {
            id: removeItem,
          },
        });
      },
      save: async ({ id, boxId, storageId, name, thumbnail, date_id }) => {
        const param = {
          id: id,
          category_id: boxId,
          storage_id: storageId,
          name: name,
          thumbnail_id: thumbnail,
          date_id: date_id,
        };
        if (!id) return axios.post(api.Products.specialProduct, param);
        return axios.put(api.Products.specialProduct, param);
      },
    },
    getIds: ({ boxId, available, categoriesId }) => {
      const ap = api.Products.main;
      const apiKey = `${boxId}-${(() => {
        let k = "";
        _.forEach(categoriesId, (c) => {
          k = c + "-";
        });
        return k;
      })()}`;

      const req = async () => {
        return await axios
          .get(ap, {
            params: {
              category_id: boxId,
              available: available ? true : undefined,
              categories: categoriesId,
              only_id: true,
            },
          })
          .then((res) => {
            return {
              status: 200,
              data: res.data.data,
            };
          });
      };

      return [apiKey, req];
    },
  },
  Product: {
    get: async ({ pId, ...params }) => {
      return await axios
        .get(api.Products.main, {
          params: {
            id: pId,
            ...params,
          },
        })
        .then((res) => {
          return {
            status: 200,
            data: convertProduct(res.data.data),
          };
        });
    },
    search: ({
      query = "",
      boxId = undefined,
      types = undefined,
      ...params
    }) => {
      const d = ControllerSite.search({
        searchIn: "product",
        query: query,
        category_id: boxId,
        types: types,
        ...params,
      });

      let apiKey = d[0] + query;
      if (boxId) apiKey = apiKey + boxId;
      if (!_.isEmpty(types))
        _.forEach(types, (ty) => {
          apiKey = apiKey + ty;
        });

      return [
        apiKey,
        async () => {
          if (!boxId) return undefined;
          return await d[1]().then((res) => {
            return {
              ...res,
              data: {
                ...res.data,
                products: convertProducts(res.data.products),
              },
            };
          });
        },
      ];
    },
    save: async ({
      id,
      box_id,
      booking_type,
      name,
      permalink,
      thumbnail_id,
      description,
      shortDescription,
      categories,
      media,
      tags,
      tag_groups,
      type,
      brand_id,
      cities,
      address,
      short_address,
      location,
      details,
      properties,
      settings,
      features,
      review,
    }) => {
      const params = {
        id: id,
        category_id: box_id,
        booking_type: booking_type,
        categories: categories,
        name: name,
        type: type,
        description: description,
        short_description: shortDescription,
        cities: cities,
        address: address,
        short_address: short_address,
        features: features ? convertFeatureValue(features) : undefined,
        location: location,
        details: details,
        properties: properties,
        tags: tags,
        tag_groups: tag_groups,
        thumbnail_id: thumbnail_id,
        media: media,
        brand_id: brand_id,
        settings: settings,
        review: review,
      };
      if (!id) params.permalink = permalink;
      if (id) return axios.put(api.Products.main, { ...params });
      return axios.post(api.Products.main, { ...params });
    },
    updatePermalink: async ({ pId, permalink, ...params }) => {
      return await axios.put(api.Products.main, {
        id: pId,
        permalink: permalink,
        ...params,
      });
    },
    activeProduct: async ({ pId, disable, ...params }) => {
      return await axios.put(api.Products.main, {
        id: pId,
        disable: disable,
        ...params,
      });
    },
    changeDefaultStorage: async ({ pId, storageId, ...params }) => {
      return await axios.put(api.Products.main, {
        id: pId,
        default_storage_id: storageId,
        ...params,
      });
    },
    manage: async ({ pId, manage, ...params }) => {
      return await axios.put(api.Products.main, {
        id: pId,
        manage: manage,
        ...params,
      });
    },
    searchProduct: async ({
      query,
      box = undefined,
      type = "product",
      ...data
    }) => {
      if (query === undefined) return [null, null];
      const reqParams = {
        q: query,
        category_id: box,
        type: type,
        ...data,
      };
      gcLog("searchProduct REQ PARAMS", reqParams);
      return await axios.get(api.search, { params: reqParams }).then((res) => {
        return {
          status: 200,
          data: res.data,
        };
      });
    },
    createProduct: async ({ box, data }) => {
      if (box === undefined) return [null, null];
      const reqParams = {
        category_id: box,
        ...data,
      };
      gcLog("createProduct REQ PARAMS", reqParams);
      return await axios.post(api.Products.main, reqParams).then((res) => {
        return {
          status: 200,
          data: res.data,
        };
      });
    },
    getProduct: (pId, onlyFields = [], excludeFields = [], ...params) => {
      if (pId === undefined || pId === null) return [null, null];
      const ap = api.Products.main;
      const req = async () => {
        return await axios
          .get(ap, {
            params: {
              id: pId,
              only_fields: onlyFields,
              exclude_fields: excludeFields,
              ...params,
            },
          })
          .then((res) => {
            return {
              status: 200,
              data: res.data.data,
            };
          });
      };
      return [ap, req];
    },

    putProduct: async ({ id, data }) => {
      if (id === undefined) return [null, null];
      return await axios
        .put(api.Products.main, {
          id: id,
          notif: false,
          ...data,
        })
        .then((res) => {
          return {
            status: res.status,
            data: res.data.data,
          };
        });
    },
    Review: {
      add: async ({ productId, user_id, reviewText, question, state }) => {
        if (!(productId && user_id && reviewText && _.isBoolean(question)))
          throw "params error";

        return cp.Product.get({ pId: productId }).then((res) => {
          const review = res.data.review;
          const chats = review.chats || [];
          review.chats.push({
            id: generateId(chats),
            user_id: user_id,
            created_at: _.toInteger(new Date().getTime() / 1000),
            text: reviewText,
            question: question,
          });
          return cp.Product.save({
            id: productId,
            review: {
              ...review,
              state: cp.Product.Review.getState(state, question),
            },
          });
        });
      },
      stateReview: async ({ productId, state, question }) => {
        if (!_.isBoolean(state)) return;
        return cp.Product.get({ pId: productId }).then((res) => {
          const review = res.data.review;
          return cp.Product.save({
            id: productId,
            review: {
              ...review,
              state: cp.Product.Review.getState(state, question),
            },
          });
        });
      },
      getState: (state, question) => {
        if (!_.isBoolean(state)) {
          return undefined;
        }
        if (state) {
          if (state) return "reviewed";
          return "request_review";
        }
        if (question) return "ready";
        return "request_review";
      },
    },
    Media: {
      Thumbnail: {
        upload: async ({
          boxId,
          title = {},
          file,
          onUploadProgress,
          getParams = {},
          ...params
        }) => {
          return ControllerSite.Media.upload({
            boxId: boxId,
            title: title,
            file: file,
            onUploadProgress: onUploadProgress,
            type: "thumbnail",
            getParams: getParams,
            ...params,
          });
        },
      },
    },
    Tags: {
      get: async ({
        contain = "",
        page,
        step = tagsDefaultStep,
        ...params
      }) => {
        if (contain) {
          params.contain = contain;
        }
        return await axios
          .get(api.Products.tags, {
            params: {
              p: page,
              s: step,
              ...params,
            },
          })
          .then((res) => {
            return {
              status: res.status,
              data: res.data.data,
              pagination: convertPagination(res.data.pagination),
            };
          });
      },
      search: ({ query = "", ...params }) => {
        const d = ControllerSite.search({ searchIn: "tag", query: query });
        return [
          d[0],
          async () => {
            return await d[1]();
          },
        ];
      },
      getSingle: async ({ tagId }) => {
        return await axios
          .get(api.Products.tags, {
            params: {
              id: tagId,
            },
          })
          .then((res) => {
            return {
              status: res.status,
              data: res.data.data,
            };
          });
      },
      set: async ({ tagId, permalink, name, ...params }) => {
        const paams = {
          id: tagId,
          permalink,
          name,
          ...params,
        };
        if (!tagId)
          return await axios
            .post(api.Products.tags, {
              ...paams,
            })
            .then((res) => {
              return {
                status: res.status,
                item: convertMultiLanguageObj({
                  keys: ["name"],
                  val: res.data.data,
                }),
              };
            });
        return await axios
          .put(api.Products.tags, {
            ...paams,
          })
          .then((res) => {
            return {
              status: res.status,
              item: convertMultiLanguageObj({
                keys: ["name"],
                val: res.data.data,
              }),
            };
          });
      },
      getList: async ({ tags = [] }) => {
        let tagsId = [];
        if (!_.isEmpty(tags) && _.isObject(tags[0])) {
          _.forEach(tags, (t) => {
            tagsId.push(t.id);
          });
        } else {
          tagsId = tags;
        }
        return await axios
          .patch(api.Products.tags, {
            tags: tagsId,
          })
          .then((res) => {
            let list = [];
            if (!_.isEmpty(tags) && _.isObject(tags[0])) {
              _.forEach(res.data.tags, (d) => {
                const i = _.findIndex(tags, (t) => {
                  return t.id === d.id;
                });
                if (i === -1) return;
                list.push({
                  ...d,
                  show: Boolean(tags[i].show),
                });
              });
            } else {
              list = res.data.tags;
            }
            return {
              status: res.status,
              data: list,
            };
          });
      },
    },
  },
  Storage: {
    get: async ({
      pId,
      order = {
        asc: true,
        orderBy: "priority",
      },
      ...params
    }) => {
      return await axios
        .get(api.Products.storage, {
          params: {
            product_id: pId,
            all: true,
            o: ApiHelper.convertOrder(order),
            ...params,
          },
        })
        .then((res) => {
          const product = convertProduct(res.data.product);
          const { isPackage } = product.type;
          const storages = convertStorages(res.data.data);
          if (isPackage) {
            try {
              _.forEach(res.data.data.data, (s, index) => {
                const newSt = [];
                _.forEach(s.items, (it) => {
                  const pr = convertProduct(it.product);
                  pr.defaultStorage = _.find(pr.storages, (st) => {
                    return st.storageId === pr.default_storage_id;
                  });
                  pr.defaultStorage.count = it.count;
                  newSt.push(pr);
                });
                storages[index] = {
                  ...storages[index],
                  items: newSt,
                };
              });
            } catch (e) {}
          }
          return {
            status: 200,
            product: product,
            storages: storages,
            activeStorage: res.data.product.default_storage_id || -1,
          };
        });
    },
    getSingle: ({ sId, pId, product_only }) => {
      if (product_only === undefined) {
        product_only = !sId && pId ? true : undefined;
      }
      const ap = `storage-getSingle-${sId}-${pId}-${product_only}`;
      return [
        ap,
        async () => {
          return await axios
            .get(api.Products.storage, {
              params: {
                id: sId === 0 ? null : sId,
                product_id: !sId ? pId : undefined,
              },
            })
            .then((res) => {
              const pr = {
                status: res.status,
                data: {
                  product: convertProduct(res.data.product),
                },
              };

              tryIt(() => {
                pr.data.storage = convertStorage(res.data.data);
              });

              return pr;
            });
        },
      ];
    },
    save: async ({
      duplicate,
      pId,
      sId,
      title,
      media_id,
      invoiceTitle,
      invoiceDescription,
      availableCount,
      availableCountForSale,
      unavailable,
      maxCountForSale,
      minCountForSale,
      vipMaxCountForSale,
      minCountAlert,
      startPrice,
      finalPrice,
      discountPrice,
      shipping_cost,
      vipDiscountPrice,
      tax,
      least_booking_time,
      booking_cost,
      supplierId,
      gender,
      startTime,
      deadline,
      features,
      vip_prices,
      dimensions,
      items,
      package_discount_price,
      maxShippingTime,
      settings,
    }) => {
      if (duplicate && !(pId && sId)) {
        throw "when duplicate=true (pId && sId) required";
      }

      let id = toNumberSafe(sId);
      id = id >= 1 ? id : undefined;
      const params = {
        product_id: toNumberSafe(pId),
        id: !duplicate ? toNumberSafe(id) : undefined,
        reference_id: duplicate ? toNumberSafe(id) : undefined,
        title: title,
        unavailable: unavailable,
        invoice_title: invoiceTitle,
        invoice_description: invoiceDescription,
        available_count: toNumberSafe(availableCount),
        available_count_for_sale: toNumberSafe(availableCountForSale),
        max_count_for_sale: toNumberSafe(maxCountForSale),
        min_count_for_sale: toNumberSafe(minCountForSale),
        vip_max_count_for_sale: toNumberSafe(vipMaxCountForSale),
        min_count_alert: toNumberSafe(minCountAlert),
        start_price: toNumberSafe(startPrice),
        final_price: toNumberSafe(finalPrice),
        discount_price: toNumberSafe(discountPrice),
        shipping_cost: toNumberSafe(shipping_cost),
        vip_discount_price: toNumberSafe(vipDiscountPrice),
        booking_cost: toNumberSafe(booking_cost),
        least_booking_time: toNumberSafe(least_booking_time),
        tax_type: tax,
        media_id: toNumberSafe(media_id),
        supplier_id: toNumberSafe(supplierId),
        gender: gender,
        start_time: startTime ? Math.round(toNumberSafe(startTime)) : undefined,
        deadline:
          deadline !== undefined
            ? deadline
              ? Math.round(toNumberSafe(deadline))
              : deadline
            : undefined,
        features: features,
        vip_prices: toNumberSafe(vip_prices),
        dimensions: toNumberSafe(dimensions),
        items: items,
        package_discount_price: toNumberSafe(package_discount_price),
        max_shipping_time: toNumberSafe(maxShippingTime),
        settings: !_.isEmpty(settings) ? settings : undefined,
      };
      if (sId && !duplicate)
        return axios.put(api.Products.storage, { ...params });
      return axios.post(api.Products.storage, { ...params });
    },
    activeStorage: ({ sId, active }) => {
      return axios.put(api.Products.storage, { id: sId, disable: !active });
    },
    sortChange: async ({ pId, storagesId, ...params }) => {
      return await axios.put(
        api.Products.main,
        {
          id: pId,
          storages_id: storagesId,
        },
        {
          params: {
            ...params,
          },
        }
      );
    },
    reviewPrice: async ({
      boxId,
      startPrice,
      finalPrice,
      discountPrice,
      taxType,
      shippingCost = 0,
    }) => {
      return axios.post(api.Products.reviewPrice, {
        category_id: boxId,
        start_price: startPrice,
        final_price: finalPrice,
        discount_price: discountPrice,
        tax_type: taxType,
        shipping_cost: shippingCost,
      });
    },
    Filters: {
      get: async ({ boxId, ...params } = {}) => {
        return await cp.Filters.get({
          boxId: boxId,
          state: api.Products.storage,
          ...params,
        });
      },
    },
  },
  HousePrice: {
    update: ({ id, ...params }) => {
      gcLog("HousePrice Update", params);
      if (id) {
        return axios.put(api.Products.HousePrice, {
          id: id,
          notif: false,
          ...params,
        });
      }
      return axios.post(api.Products.HousePrice, {
        id: id,
        ...params,
      });
    },
  },
  Features: {
    get: () => {
      const ap = api.Products.features;
      const req = async ({
        boxId,
        order = {
          asc: true,
          orderBy: "id",
        },
        filters = {},
        page = 1,
        step = featuresDefaultStep,
        ...params
      }) => {
        return await axios
          .get(ap, {
            params: {
              s: step,
              o: ApiHelper.convertOrder(order),
              ...DataUtils()
                .TableFilter()
                .convertForSendToServer({ filters: filters }),
              ...params,
            },
          })
          .then((res) => {
            return {
              status: 200,
              data: convertFeatures(res.data.data),
              pagination: convertPagination(res.data.pagination, page),
            };
          });
      };
      return [ap, req];
    },
    getFeaturesStorage: (pId, ...params) => {
      const ap = api.Products.main;
      const req = async () => {
        return await axios
          .get(ap, {
            params: {
              id: pId,
              only_fields: ["features"],
              only_selectable: true,
              ...params,
            },
          })
          .then((res) => {
            return {
              status: 200,
              data: convertFeature(res.data.data),
            };
          });
      };
      return [ap, req];
    },
    getFeatureGroup: (pId, ...params) => {
      const ap = api.Products.main;
      const req = async (/*{
                                   boxId,
                                   order = {
                                       asc: true,
                                       orderBy: "id",
                                   },
                                   filters = {},
                                   page = 1,
                                   step = featuresDefaultStep,
                                   ...params
                               }*/) => {
        return await axios
          .get(ap, {
            params: {
              id: pId,
              only_fields: ["feature_groups"],
              ...params,
            },
          })
          .then((res) => {
            return {
              status: 200,
              data: convertFeatureGroup(res.data.data),
            };
          });
      };
      return [ap, req];
    },
    getSingle: async ({ fId }) => {
      return await axios
        .get(api.Products.features, {
          params: {
            id: fId,
          },
        })
        .then((res) => {
          return {
            status: 200,
            data: convertFeature(res.data.data),
          };
        });
    },
    save: async ({
      id,
      name,
      values,
      type,
      layout_type = "default",
      settings,
    }) => {
      const newValue = [];
      _.forEach(values, (v) => {
        newValue.push({
          value: v,
        });
      });
      const params = {
        id: id,
        name: name,
        values: newValue,
        type: !id ? type : undefined,
        layout_type: layout_type,
        settings: settings,
      };
      if (id) return axios.put(api.Products.features, { ...params });
      return axios.post(api.Products.features, { ...params });
    },
    sort: async ({ ids }) => {
      return axios.patch(api.Products.features, {
        ids: ids,
      });
    },
    searchFeature: ({ search }) => {
      const key = api.feature + "-" + "-" + search;
      const req = async () => {
        return await axios.get(api.feature, {
          params: {
            name__fa: search,
          },
        });
      };
      return [key, req];
    },
    addFeatureValue: async ({ fId, value }) => {
      return axios.post(api.Products.featureValue, {
        feature_id: fId,
        value: { fa: value },
      });
    },
    saveItem: async ({ fId, value }) => {
      return axios.put(api.Products.featureValue, {
        id: fId,
        value: value,
      });
    },
    updateGroup: async ({ id, groups = [] }) => {
      return axios.put(api.Products.features, {
        id: id,
        groups: _.uniq(groups),
      });
    },
    search: ({ search }) => {
      const key = api.Products.featureGroup + "-" + search;
      const req = async () => {
        return await axios
          .get(api.Products.features, {
            params: {
              name__fa: search,
            },
          })
          .then((res) => {
            return {
              status: res.status,
              data: convertFeatures(res.data.data),
            };
          });
      };
      return [key, req];
    },
    productFeature: ({ productId }) => {
      const key = api.Products.productFeature + "-" + productId;
      const req = async () => {
        return await axios
          .get(api.Products.productFeature, {
            params: {
              product_id: productId,
              distinct: true,
            },
          })
          .then((res) => {
            return {
              status: res.status,
              data: res.data.data,
            };
          });
      };
      return [key, req];
      /*onst key = api.Products.productFeature + "-" + productId;
            const req = async () => {
                return await axios.get(api.Products.productFeature, {
                    params: {
                        product_id: productId
                    }
                }).then((res) => {
                    return {
                        status: res.status,
                        data: convertFeatures(res.data.data)
                    }
                })
            };
            return [key, req]*/
    },
    Filters: {
      get: async ({ boxId, ...params } = {}) => {
        return await cp.Filters.get({
          boxId: boxId,
          state: api.Products.features,
          ...params,
        });
      },
    },
  },
  FeaturesGroup: {
    get: ({
      boxId,
      order = {
        asc: true,
        orderBy: "id",
      },
      filters = {},
      page = 1,
      step = featuresDefaultStep,
      ...params
    }) => {
      const ap = api.Products.featureGroup;
      const apiKey = ApiHelper.createSWRKey({
        api: ap,
        order: order,
        box: boxId,
        page,
        step,
        filters,
      });

      const req = async () => {
        return await axios
          .get(ap, {
            params: {
              category_id: boxId,
              p: page,
              s: step,
              o: ApiHelper.convertOrder(order),
              ...DataUtils()
                .TableFilter()
                .convertForSendToServer({ filters: filters }),
              ...params,
            },
          })
          .then((res) => {
            return {
              status: 200,
              data: convertFeaturesGroup(res.data.data),
              pagination: convertPagination(res.data.pagination, page),
            };
          });
      };
      return [apiKey, req];
    },
    getSingle: async ({ fgId, ...params }) => {
      return await axios
        .get(api.Products.featureGroup, {
          params: {
            id: fgId,
          },
        })
        .then((res) => {
          return {
            status: res.status,
            data: convertFeatureGroup(res.data.data),
          };
        });
    },
    save: async ({ fgId, boxId, name, showTitle }) => {
      gcLog("fgId:, on save", { fgId, boxId, name, showTitle });
      return await cp.FeaturesGroup.getSingle({ fgId }).then((res) => {
        const settings = tryIt(
          () => {
            return {
              ...res.data.settings,
              ui: {
                ...res.data.settings.ui,
                show_title: showTitle,
              },
            };
          },
          {
            ...res.data.settings,
            ui: {
              show_title: showTitle,
            },
          }
        );
        const props = {
          id: fgId,
          name: name,
          category_id: boxId,
          settings: settings,
        };
        if (!fgId)
          return axios.post(api.Products.featureGroup, {
            ...props,
          });
        return axios.put(api.Products.featureGroup, {
          ...props,
        });
      });
    },
    updateFeatures: async ({ id, features }) => {
      return axios.put(api.Products.featureGroup, {
        id: id,
        features: _.uniq(features),
      });
    },
    search: ({ boxId, search }) => {
      const key = api.Products.featureGroup + "-" + boxId + "-" + search;
      const req = async () => {
        if (!boxId) return {};
        return await axios.get(api.Products.featureGroup, {
          params: {
            category_id: boxId,
            name__fa: search,
          },
        });
      };
      return [key, req];
    },
  },
  Brands: {
    get: () => {
      const ap = api.Products.brands;
      const req = async ({
        order = {
          asc: true,
          orderBy: "id",
        },
        boxId,
        q,
        filters = {},
        page = 1,
        step = featuresDefaultStep,
        ...params
      }) => {
        return await axios
          .get(ap, {
            params: {
              category_id: boxId,
              q: q ? q : undefined,
              p: page,
              s: step,
              o: ApiHelper.convertOrder(order),
              ...DataUtils()
                .TableFilter()
                .convertForSendToServer({ filters: filters }),
              ...params,
            },
          })
          .then((res) => {
            return {
              status: 200,
              data: convertBrands(res.data.data),
              pagination: convertPagination(res.data.pagination, page),
            };
          });
      };
      return [ap, req];
    },
    update: ({ id, name, permalink, params }) => {
      const data = {
        name: name,
        permalink: permalink,
      };

      if (!id) {
        return axios
          .post(
            api.Products.brands,
            {
              ...data,
            },
            {
              params: {
                ...params,
              },
            }
          )
          .then((res) => {
            return {
              status: res.status,
              data: convertBrand(res.data.data),
            };
          });
      }
      return axios
        .put(
          api.Products.brands,
          {
            id: id,
            ...data,
          },
          {
            params: {
              ...params,
            },
          }
        )
        .then((res) => {
          return {
            status: res.status,
            data: convertBrand(res.data.data),
          };
        });
    },
    Filters: {
      get: async ({ boxId, ...params } = {}) => {
        return await cp.Filters.get({
          boxId: boxId,
          state: api.Products.brands,
          ...params,
        });
      },
    },
  },
  Categories: {
    get: ({
      boxId,
      order = {
        asc: true,
        orderBy: "id",
      },
      filters = {},
      page = 1,
      step = featuresDefaultStep,
      parentId,
      ...params
    }) => {
      const ap = api.Products.categories;
      const apiKey =
        ApiHelper.createSWRKey({
          api: ap,
          order: order,
          filters: filters,
          box: boxId,
          page: page,
          step: step,
        }) + `-${parentId}`;
      const req = async () => {
        if (!boxId) return undefined;
        return await axios
          .get(ap, {
            params: {
              parent_id: parentId || boxId,
              p: page,
              s: step,
              o: ApiHelper.convertOrder(order),
              ...DataUtils()
                .TableFilter()
                .convertForSendToServer({ filters: filters }),
              ...params,
            },
          })
          .then((res) => {
            return {
              status: 200,
              data: convertCategories(res.data.data),
              pagination: convertPagination(res.data.pagination, page),
            };
          });
      };
      return [apiKey, req];
    },
    search: ({ query, ...props }) => {
      return ControllerSite.search({ searchIn: "cat", query: query, ...props });
    },
    getAllCategories: ({ ...params }) => {
      const ap = apiRout + "/admin/categories";
      const req = async ({ boxId, ...params }) => {
        return await axios
          .get(ap, {
            params: {
              id: boxId,
              ...params,
            },
          })
          .then((res) => {
            return {
              status: res.status,
              categories: res.data.data,
            };
          });
      };
      return [ap, req];
    },
    activeCategory: ({ catId, disable }) => {
      return axios.put(api.Products.categories, {
        id: catId,
        disable: disable,
      });
    },
    getCategory: async ({ catId, ...params }) => {
      return await axios
        .get(api.Products.categories, {
          params: {
            id: catId,
            ...params,
          },
        })
        .then((res) => {
          return {
            status: 200,
            data: {
              ...convertCategory(res.data.data),
            },
          };
        });
    },
    updateCategory: async ({
      id,
      boxId,
      parentId,
      name,
      permalink,
      mediaId,
      seoTitle,
      seoDescription,
      ...params
    }) => {
      if (!id) {
        return await axios
          .post(
            api.Products.categories,
            {
              parent_id: parentId || boxId,
              name: name,
              permalink: permalink,
              media_id: mediaId,
              seo_title: seoTitle,
              seo_description: seoDescription,
            },
            {
              params: {
                ...params,
              },
            }
          )
          .then((res) => {
            return {
              status: res.status,
              data: convertCategory(res.data.data),
            };
          });
      }
      return await axios
        .put(
          api.Products.categories,
          {
            id: id,
            parent_id: parentId,
            name: name,
            permalink: permalink,
            media_id: mediaId,
          },
          {
            params: {
              ...params,
            },
          }
        )
        .then((res) => {
          return {
            status: res.status,
            data: convertCategory(res.data.data),
          };
        });
    },
    updateFeaturesGroup: async ({ id, featureGroups, ...params }) => {
      return await axios
        .put(api.Products.categories, {
          id: id,
          feature_groups: _.uniq(featureGroups),
        })
        .then((res) => {
          return {
            status: res.status,
            data: convertCategory(res.data.data),
          };
        });
    },
    Filters: {
      get: async ({ boxId, ...params } = {}) => {
        return await cp.Filters.get({
          boxId: boxId,
          state: api.Products.categories,
          ...params,
        });
      },
    },
  },
  Invoices: {
    get: () => {
      const ap = api.Products.invoices;
      const req = async ({
        order = {
          asc: true,
          orderBy: "id",
        },
        filters = {},
        page = 1,
        step = featuresDefaultStep,
        parentId,
        status,
        onlyBooking,
        ...params
      }) => {
        return await axios
          .get(ap, {
            params: {
              p: page,
              s: step,
              o: ApiHelper.convertOrder(order),
              parent_id: parentId,
              status: status,
              only_booking: onlyBooking ? true : undefined,
              ...DataUtils()
                .TableFilter()
                .convertForSendToServer({ filters: filters }),
              ...params,
            },
          })
          .then((res) => {
            return {
              status: 200,
              data: convertInvoices(res.data.data),
              pagination: convertPagination(res.data.pagination, page),
            };
          });
      };
      return [ap, req];
    },
    put: async ({ id, status, ...params }) => {
      const ap = api.Products.invoices;
      return await axios
        .put(
          ap,
          {
            id: id,
            status: status,
          },
          {
            params: {
              ...params,
            },
          }
        )
        .then((res) => {
          return {
            status: res.status,
            data: res.data.data,
          };
        });
    },
    getSingle: async ({ invoiceId, ...params }) => {
      return await axios
        .get(api.Products.invoices, {
          params: {
            id: invoiceId,
            ...params,
          },
        })
        .then((res) => {
          return {
            status: 200,
            data: convertInvoice(res.data.data),
          };
        });
    },
    Filters: {
      get: async ({ ...params } = {}) => {
        return cp.Filters.get({ state: api.Products.invoices, ...params });
      },
    },
  },
  InvoiceProduct: {
    get: ({
      order = {
        asc: true,
        orderBy: "id",
      },
      boxId,
      filters = {},
      page = 1,
      step = featuresDefaultStep,
      parentId,
      onlyBooking,
      ...params
    }) => {
      const ap = api.Products.invoiceProduct;
      const apiKey =
        ApiHelper.createSWRKey({
          api: ap,
          order: order,
          box: boxId,
          page,
          step,
          filters,
        }) + `onlyBooking-${onlyBooking}`;
      const req = async () => {
        return await axios
          .get(ap, {
            params: {
              category_id: boxId,
              p: page,
              s: step,
              o: ApiHelper.convertOrder(order),
              only_booking: onlyBooking ? true : undefined,
              parent_id: parentId,
              ...DataUtils()
                .TableFilter()
                .convertForSendToServer({ filters: filters }),
              ...params,
            },
          })
          .then((res) => {
            return {
              status: 200,
              data: convertInvoiceProducts(res.data.data),
              pagination: convertPagination(res.data.pagination, page),
            };
          });
      };
      return [apiKey, req];
    },
    update: async ({ id, deliver_status }) => {
      return axios.put(api.Products.invoiceProduct, { id, deliver_status });
    },
  },
  Filters: {
    get: async ({ state = api.Products.main, ...params } = {}) => {
      return await axios
        .get(api.tableFilter + `${state}`, {
          params: {
            ...params,
            category_id: params.boxId,
          },
        })
        .then((res) => {
          return {
            status: 200,
            data: convertTableFilters(res.data.data),
          };
        });
    },
  },
  DiscountCode: {
    get: ({
      storageId,
      order = {
        asc: true,
        orderBy: "id",
      },
      filters = {},
      page = 1,
      step = productDefaultStep,
      startTime: sd,
      endTime: ed,
      used,
      html,
    }) => {
      const ap = api.Products.discountCode;
      const apiKey =
        ApiHelper.createSWRKey({
          api: ap,
          order: order,
          page,
          step,
          filters,
        }) + `-${storageId}-${sd}-${ed}-${used}-${html}`;

      const req = async () => {
        if (!storageId) return {};
        let getProps = {
          storage_id: storageId,
          p: (page = 1),
          s: step,
          o: ApiHelper.convertOrder(order),
          sd: isNumeric(sd) ? _.toInteger(sd / 1000) : undefined,
          ed: isNumeric(ed) ? _.toInteger(ed / 1000) : undefined,
          not_used: !used,
          html: html,
        };
        return await axios
          .get(ap, {
            params: {
              ...getProps,
            },
          })
          .then((res) => {
            let p;
            try {
              p = {
                data: convertDiscountCodes(res.data.data),
                pagination: convertPagination(res.data.pagination, page),
              };
            } catch (e) {
              p = {
                data: res.data,
              };
            }

            return {
              status: 200,
              ...p,
            };
          });
      };
      return [apiKey, req];
    },
    print: async ({
      storageId,
      count: step = 30,
      startTime,
      endTime,
      used,
    }) => {
      return await cp.DiscountCode.get({
        storageId: storageId,
        step: step,
        startTime: startTime,
        endTime: endTime,
        used: used,
        html: true,
      })[1]();
    },
    save: async ({ storageId, prefix, count }) => {
      return await axios.post(api.Products.discountCode, {
        storage_id: _.toInteger(storageId),
        prefix: _.toUpper(prefix),
        count: _.toInteger(count),
      });
    },
  },
};

export default cp;
