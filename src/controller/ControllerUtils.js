import _ from "lodash";
import {filterType} from "../utils/DataUtils";
import {lang} from "../repository";

export const defaultStep = 15;
export default (() => {
    // Create the methods object
    const methods = {};
    methods.convertPagination = (pagination) => {
        return {
            lastPage: pagination.last_page,
            count: pagination.count,
        }
    };
    methods.convertTableFilters = (filters) => {
        const list = [];
        _.forEach(filters, (filter) => {
            list.push(methods.convertTableFilter(filter))
        });
        return list;
    };
    methods.convertTableFilter = (filter) => {
        switch (filter.name) {
            case "box": {
                return {
                    ...filter,
                    type:filterType.single,
                    label: lang.get("mt_box"),
                    filters:methods.convertNamesToLabels(filter.filters)
                }
            }
            case "category": {
                return {
                    ...filter,
                    type: filterType.multi,
                    label: 'دسته‌بندی',
                    filters:methods.convertNamesToLabels(filter.filters)
                }
            }
            default: {
                return {
                    ...filter,
                    type: undefined,
                    label: ''
                }
            }
        }
    };
    methods.convertNamesToLabels = (data) => {
        const list = [];
        _.forEach(data, (d) => {
            list.push(methods.convertNameToLabel(d))
        });
        return list;
    };
    methods.convertNameToLabel = (item) => {
        item.label = item.name;
        item.value = item.name;
        return item;
    };
    return methods;
})();
