import momentJalaali from "moment-jalaali";
import _ from "lodash";
import {activeLang, apiRout, DEBUG, lang, sLang} from "../repository";
import rout from "../router";
import DataUtils, {filterType} from "../utils/DataUtils";
import {cyan, green, grey, lightGreen, lime, red} from "@material-ui/core/colors";
import yellow from "@material-ui/core/colors/yellow";
import {gcError, gcLog, getSafe, tryIt} from "../utils/ObjectUtils";
import {bookingType, featureType, productType, serverFileTypes} from "./type";
import {defaultDescription} from "./defualt";

//region Product
export function convertProducts(products) {
    const list = [];
    _.forEach(products, (product) => {
        try {
            list.push(convertProduct(product))
        } catch (e) {
        }
    });
    return list
}

export function convertProduct(p) {
    const val = {
        ...p,
        id: p.id,
        name: convertMultiLanguage(p.name),
        permalink: p.permalink,
        thumbnail: p.thumbnail,
        disable: p.disable,
        active: !p.disable,
        cities: [],
        link: rout.Product.Single.editProduct(p.id),
        storageLink: rout.Storage.Single.showStorageDashboard({productId: p.id}),
        deadline: _.toInteger(p.deadline),
        location: p.location,
        description: p.description || defaultDescription(),
        shortDescription: convertMultiLanguage(p.short_description || createMultiLanguage()),
        media: convertMultiLanguageObjArray({array: p.media, keys: ['title']}),
        type: convertType(p.type),
        featureGroup: convertFeaturesGroup(p.feature_groups),
        box: p.box ? {
            ...convertMultiLanguageObj({val: p.box, keys: ['name']}),
            link: `/box/${p.box.id}`
        } : undefined,
        categories: p.categories ? convertCategories(p.categories) : undefined,
        breadcrumbs: [],
        shortAddress: convertMultiLanguage(p.short_address || createMultiLanguage()),
        address: convertMultiLanguage(p.address || createMultiLanguage()),
        tags: convertMultiLanguageObjArray({array: p.tags, keys: ['name']}),
        tag_groups: p.tag_groups || [],
        manage: p.manage,
        rate: p.rate,
        brand: p.brand,
        house: p.house,
        date_id: p?.date_id,
        settings: p.settings,
        default_storage_id: p.default_storage_id,
        review: (() => {
            const review = _.isObject(p.review) ? {...p.review} : {};
            if (!_.isArray(review.chats)) {
                review.chats = []
            }
            if (!review.state) {
                review.state = 'ready'
            }
            return review
        })(),
        features: p.features,
        check_review: p.check_review,
        booking_type: bookingType[p.booking_type],
        ...createAndUpdatedAt(p),
    };

    if (p.cities) {
        _.forEach(p.cities, (ci) => {
            val.cities.push({
                ...ci,
                label: ci.name
            })
        })
    }

    if (p.default_storage) {
        val.defaultStorage = convertStorage(p.default_storage)
    }

    if (p.storages) {
        val.storages = _.sortBy(convertStorages(p.storages), [function (o) {
            return o.default === false;
        }]);

        if (!p.defaultStorage && !_.isEmpty(val.storages)) {
            val.defaultStorage = val.storages[0];
        }
    }

    val.properties = p.properties;

    val.details = convertDetails(p.details || createMultiLanguageObject());

    return val
}

export function convertDetails(de = {}) {
    const val = {};
    _.forEach(sLang, (l, key) => {
        if (!de[key]) {
            de[key] = {
                text: "",
                phones: [],
                servingDays: "",
                servingHours: "",
            };
            return;
        }
        const phones = [];
        _.forEach(de[key].phones, (p) => {
            phones.push({
                ...p,
                // icon: p.type === "phone" ? <PhoneIphone/> : <Phone/>
            })
        });
        de[key] = {
            text: de[key].text,
            phones: phones,
            servingDays: de[key].serving_days || de[key].servingDays,
            servingHours: de[key].serving_hours || de[key].servingHours,

        };
    });

    return {
        ...de,
        toServer: (de) => {
            const res = {};
            _.forEach(sLang, (l, key) => {
                res[key] = undefined;

                if (!de[key])
                    return;
                res[key] = {};

                try {
                    if (de[key].text) {
                        res[key].text = de[key].text;
                    }
                    if (de[key].phones) {
                        res[key].phones = de[key].phones;
                    }
                    if (de[key].servingDays) {
                        res[key].serving_days = de[key].servingDays;
                    }
                    if (de[key].servingHours) {
                        res[key].serving_hours = de[key].servingHours;
                    }
                } catch (e) {
                }
            });
            return res
        },
        isEmpty: (de) => {
            let empty = true;
            _.forEach(sLang, (l, key) => {
                if (!de[key])
                    return;
                if (de[key].text) {
                    empty = false;
                    return false;
                }
                if (de[key].phones) {
                    empty = false;
                    return false;
                }
                if (!_.isEmpty(de[key].servingDays) || !_.isEmpty(de[key].servingHours)) {
                    empty = false;
                    return false;
                }
            });
            return empty;
        }
    }
}

export const toServer = {
    details: ({servingDays, servingHours, phones, text}) => {
        const res = {};
        _.forEach(sLang, (l, key) => {
            const data = {};
            let fill = false;
            if (servingDays && servingDays[key]) {
                fill = true;
                data.serving_days = servingDays[key]
            }

            if (servingHours && servingHours[key]) {
                fill = true;
                data.serving_hours = servingHours[key];
            }

            if (phones && phones[key]) {
                fill = true;
                data.phones = phones[key];
            }
            if (text && text[key]) {
                fill = true;
                data.text = text[key];
            }

            if (fill)
                res[key] = data
        });
        return res
    }
}


//region Storages
export function convertStorages(storages) {
    const list = [];
    _.forEach(storages, (st) => {
        list.push(convertStorage(st))
    });
    return list
}

export function convertStorage(st) {
    return {
        storageId: st.id,
        storageTitle: convertMultiLanguage(st.title),
        invoiceTitle: convertMultiLanguage(st.invoice_title),
        invoiceDescription: convertMultiLanguage(st.invoice_description),
        tax: convertTax(st.tax),
        media: st.media,
        default: st.default,
        shipping_cost: st.shipping_cost,
        availableCount: st.available_count,
        availableCountForSale: st.available_count_for_sale,
        unavailable: st.unavailable,
        maxCountForSale: st.max_count_for_sale,
        minCountForSale: st.min_count_for_sale,
        maxShippingTime: st.max_shipping_time,
        minCountAlert: st.min_count_alert | 0,
        soldCount: st.sold_count,
        startPrice: st.start_price,
        discountPrice: st.discount_price,
        discountPercent: st.discount_percent === 0 ? undefined : st.discount_percent,
        finalPrice: st.final_price,
        supplier: st.supplier ? convertUser({user: st.supplier}) : null,
        gender: convertGender(st.gender, "همه"),
        startTime: st.start_time,
        jStartTime: momentJalaali(new Date(st.start_time * 1000)).format('jYYYY/jM/jD'),
        deadline: st.deadline,
        jDeadline: momentJalaali(new Date(st.deadline * 1000)).format('jYYYY/jM/jD'),
        priority: st.priority,
        features: convertFeatures(st.features),
        dimensions: st.dimensions,
        disable: st.disable,
        vip_prices: st.vip_prices,
        package_discount_price: st.package_discount_price,
        booking_cost: st.booking_cost === -1 ? 0 : st.booking_cost,
        least_booking_time: st.least_booking_time === -1 ? 0 : st.least_booking_time,
        settings: st.settings || {}
    }
}

//region Feature
export function convertFeatureValue(features) {
    const output = []
    features.map(it => {
            it.values.map(i => {
                output.push({feature_id: it.feature.id, feature_value_id: i.id, settings: i.setting || {}})
            })
        }
    )


    return output


}

export function convertFeatures(features, isGroup) {
    gcLog("convert list", features)
    const list = [];
    _.forEach(features, (f) => {
        list.push({
            ...convertFeature(isGroup ? f.feature : f),
            sortId: f.id || f.feature.id,
            priority: getSafe(() => f.priority === undefined ? (f.feature.priority === undefined ? 1000 : f.feature.priority) : f.priority, 1000)
        })
    });
    return list
}

export function convertFeature(f) {
    let props = {};
    try {
        props = {
            ...createAndUpdatedAt(f),
            ...f
        }
    } catch (e) {

    }
    try {
        return {
            ...props,
            type: featureType[f.type]
        }
    } catch (e) {
        console.error("convertFeature", e);
        return f
    }
}


export function convertFeaturesGroup(featuresGroup) {
    const list = [];
    _.forEach(featuresGroup, (f) => {
        list.push(convertFeatureGroup(f))
    });
    return list
}

export function convertFeatureGroup(featureGroup) {
    const props = featureGroup;
    try {
        props.features = convertFeatures(featureGroup.features, true)
    } catch (e) {

    }

    return {
        ...props,
        ...createAndUpdatedAt(featureGroup)
    }
}

//endregion Feature
//endregion Storages

//region Properties
export function convertListProperties(properties = {}) {
    const val = {};
    _.forEach(sLang, (l, key) => {
        if (!properties[key]) {
            properties[key] = {
                usageCondition: [],
                property: []
            };
            return;
        }
        properties[key] = {
            usageCondition: convertProperties(properties[key].usage_condition),
            property: convertProperties(properties[key].property)
        }
    });
    return {
        ...properties,
        toServer: (pr) => {
            const res = {};
            _.forEach(sLang, (l, key) => {
                res[key] = {
                    usage_condition: [],
                    property: []
                };

                if (!pr[key])
                    return;
                _.forEach(pr[key].usageCondition, p => {
                    res[key].usage_condition.push({
                        id: p.id,
                        text: p.text,
                        type: p.type,
                        priority: p.priority,
                        icon: p.icon
                    })
                });

                _.forEach(pr[key].property, p => {
                    res[key].property.push({
                        id: p.id,
                        text: p.text,
                        type: p.type,
                        priority: p.priority,
                        icon: p.icon
                    })
                });
            });

            return res
        },
        isEmpty: (pr) => {
            let empty = true;
            if (!pr)
                return empty;
            _.forEach(sLang, (l, key) => {
                if (!pr[key])
                    return;
                if (!_.isEmpty(pr[key].usageCondition) || !_.isEmpty(pr[key].property)) {
                    empty = false;
                    return false
                }
            });
            return empty;
        }
    }
}

export function convertProperties(properties) {
    const high = [];
    const medium = [];
    const low = [];
    _.forEach(properties, (p) => {
        switch (p.priority) {
            case 'high':
                high.push(convertProperty(p));
                break;
            case 'medium':
                medium.push(convertProperty(p));
                break;
            default:
                low.push(convertProperty(p));
                break;
        }
    });
    return [
        ...high,
        ...medium,
        ...low
    ]
}

export function convertProperty(property) {
    const p = property.priority;
    return ({
        ...property,
        isHigh: p === 'high',
        isMedium: p === 'medium',
        isLow: p === 'low',
    });
}

//endregion Properties

//region Brands
export function convertBrands(brands) {
    const list = [];
    _.forEach(brands, (b) => {
        list.push(convertBrand(b))
    });
    return list;
}

export function convertBrand(brand) {
    return {
        id: brand.id,
        name: convertMultiLanguage(brand.name),
        permalink: brand.permalink,
        ...createAndUpdatedAt(brand)
    }
}

//endregion Brands

//region Categories
export function convertCategories(categories) {
    try {
        const list = [];
        _.forEach(categories, (cat) => {
            gcLog("convertCategories",convertCategory(cat))
            list.push(convertCategory(cat))
        });
        return list;

    } catch (e) {
    }

}

export function convertCategory(category) {
    return {
        id: category.id,
        name: {...convertMultiLanguage(category.name),seoTitle:category?.name?.seoTitle,seoDescription:category?.name?.seoDescription},
        media: category.media,
        disable: category.disable,
        permalink: category.permalink,
        link: category.permalink ? rout.Category.Single.editCategory(category.id) : rout.Category.RelationCat.Single.editCategory(category.id),
        parent: category.parent,
        boxId:searchTree(category),
        categoryChildProductCount: category.category_child_product_count,
        productCount: category.product_count,
        feature_groups: convertFeaturesGroup(category.feature_groups),
        childCount: category.child_count,
        child: convertCategories(category.children),
        seoTittle: category.seo_title,
        seoDescription: category.seo_description,
        ...createAndUpdatedAt(category)
    }
}

function searchTree(category){
    if(!category.parent){
        return category.id;
    }else{
        return searchTree(category.parent);
    }
}
//endregion Brands

//region Tax

export const TaxValue = {
    hasNot: 'has_not',
    fromTotalPrice: 'from_total_price',
    fromProfit: "from_profit",
};

export function convertTax(tax = TaxValue.hasNot) {

    return {
        value: tax,
        hasNot: tax === TaxValue.hasNot,
        fromTotalPrice: tax === TaxValue.fromTotalPrice,
        fromProfit: tax === TaxValue.fromProfit,
    }
}

//endregion Tax

export function convertType(type) {
    const res = {
        isService: false,
        isProduct: false,
        isPackage: false,
        isPackageItem: false,
        value: type,
        label: ''
    };
    return productType[type]
}


//endregion Product

//region invoice
export function convertInvoices(invoices) {
    const list = [];
    _.forEach(invoices, inv => {
        list.push(convertInvoice(inv))
    })
    return list
}

export function convertInvoice(invoices) {
    return {
        ...invoices,
        status: {
            key: invoices.status,
            ...invoice_status[invoices.status]
        },
        payed_at: invoices.payed_at ? momentJalaali(new Date(invoices.payed_at * 1000)).format('jYYYY/jM/jD  HH:mm') : undefined,
        o_payed_at: invoices.payed_at,
        ipg: convertIpg(invoices.ipg),
        ...createAndUpdatedAt(invoices)
    }
}

//endregion invoice

//region invoice
export function convertInvoiceProducts(invoicesProducts) {
    const list = [];

    _.forEach(invoicesProducts, inv => {
        list.push(convertInvoiceProduct(inv))

    })
    return list
}

export function convertInvoiceProduct(invoicesProducts) {
    return {
        ...invoicesProducts,
        deliver_status: invoicesProducts.deliver_status ? deliver_status[invoicesProducts.deliver_status] : undefined,
        purchase_date: momentJalaali(new Date(invoicesProducts.purchase_date * 1000)).format('jYYYY/jM/jD  HH:mm'),
        o_purchase_date: invoicesProducts.purchase_date,
        storage: convertStorage(invoicesProducts.storage),
        supplier: invoicesProducts.supplier ? convertUser({user: invoicesProducts.supplier}) : undefined
    }
}

//endregion invoice

//region discount_code

export function convertDiscountCodes(data) {
    try {
        const newList = []
        _.forEach(data, (f) => {
            newList.push(convertDiscountCode(f))
        })
        return newList
    } catch (e) {
        return []
    }

}

export function convertDiscountCode(item) {

    return {
        ...item,
        ...createAndUpdatedAt(item)
    }
}

//endregion discount_code


//region user
export function convertUsers({users}) {
    const list = [];
    try {
        _.forEach(users, (us) => {
            list.push(convertUser({user: us}))
        })
    } catch (e) {
    }
    return list
}

export function convertUser({user: us, boxes = []}) {
    const firstName = us.first_name ? _.startCase(_.toLower(us.first_name)) : '';
    const lastName = us.last_name ? _.startCase(_.toLower(us.last_name)) : '';
    // if (DEBUG)
    //     us.roll = "admin"
    return {
        id: us.id,
        username: us.username,
        firstName: firstName,
        lastName: lastName,
        fullName: (_.isEmpty(firstName) && _.isEmpty(lastName)) ? '' : firstName + " " + lastName,
        phone: us.username,
        email: us.email ? us.email : '',
        walletMoney: us.walletMoney,
        meliCode: us.meli_code ? us.meli_code : '',
        gender: convertGender(us.gender),
        birthday: us.birthday,
        telegram_username: us.telegram_username,
        jBirthday: momentJalaali(us.birthday, "YYYY-M-D").format('jYYYY/jM/jD'),
        avatar: us.avatar || 'https://api.mehrtakhfif.com/static/avatar.jpg',
        shaba: us.shaba ? us.shaba : '',
        isActiveData: (!_.isEmpty(firstName) && !_.isEmpty(lastName) && !_.isEmpty(us.meli_code)),
        isVerify: us.is_verify,
        boxes: boxes,
        depositId: us.deposit_id,
        roll: us.roll,
        settings: us.settings ? {
            ...us.settings,
            fun: DEBUG ? 1 : us.settings.fun
        } : {
            fun: DEBUG ? 1 : undefined
        },
        canSetReview: (() => {
            switch (us.roll) {
                case "superuser":
                case "content_manager":
                    return true
                default:
                    return false
            }
        })(),
        ...createAndUpdatedAt(us)
    }
}

export function convertAddress(ad) {
    let state = null;
    _.forEach(states, (st) => {
        if (st.id === ad.state) {
            state = st;
            return true;
        }
    });

    return {
        id: ad.id,
        postalCode: ad.postal_code,
        phone: ad.phone,
        name: ad.name,
        address: ad.address,
        fullAddress: state.label + ' - ' + ad.city.name + ' - ' + ad.address,
        state: state,
        city: ad.city,
        location: ad.location
    }
}

export const convertGender = (gender, noneLabel = lang.get("gender_none")) => {
    if (!_.isBoolean(gender))
        return {
            value: gender,
            label: noneLabel,
        };
    return {
        value: gender,
        label: lang.get(gender ? "gender_man" : 'gender_woman'),
    }
};

export const states = [
    {
        "label": "یزد",
        "id": 31
    },
    {
        "label": "همدان",
        "id": 30
    },
    {
        "label": "هرمزگان",
        "id": 29
    },
    {
        "label": "مرکزی",
        "id": 28
    },
    {
        "label": "مازندران",
        "id": 27
    },
    {
        "label": "لرستان",
        "id": 26
    },
    {
        "label": "گیلان",
        "id": 25
    },
    {
        "label": "گلستان",
        "id": 24
    },
    {
        "label": "کهکیلویه و بویراحمد",
        "id": 23
    },
    {
        "label": "کرمانشاه",
        "id": 22
    },
    {
        "label": "کرمان",
        "id": 21
    },
    {
        "label": "کردستان",
        "id": 20
    },
    {
        "label": "قم",
        "id": 19
    },
    {
        "label": "قزوین",
        "id": 18
    },
    {
        "label": "فارس",
        "id": 17
    },
    {
        "label": "سیستان و بلوچستان",
        "id": 16
    },
    {
        "label": "سمنان",
        "id": 15
    },
    {
        "label": "زنجان",
        "id": 14
    },
    {
        "label": "خوزستان",
        "id": 13
    },
    {
        "label": "خراسان شمالی",
        "id": 12
    },
    {
        "label": "خراسان رضوی",
        "id": 11
    },
    {
        "label": "خراسان جنوبی",
        "id": 10
    },
    {
        "label": "چهارمحال بختیاری",
        "id": 9
    },
    {
        "label": "تهران",
        "id": 8
    },
    {
        "label": "بوشهر",
        "id": 7
    },
    {
        "label": "ایلام",
        "id": 6
    },
    {
        "label": "البرز",
        "id": 5
    },
    {
        "label": "اصفهان",
        "id": 4
    },
    {
        "label": "اردبیل",
        "id": 3
    },
    {
        "label": "آذربایجان غربی",
        "id": 2
    },
    {
        "label": "آذربایجان شرقی",
        "id": 1
    }
];
//endregion user

//region Global

export const paginationParams = (page, step = 10) => {
    return {
        s: step,
        p: page
    }
};

export const convertPagination = (pagination, activePage) => {
    if (!pagination){
        return {
            lastPage: 0,
            count:0,
            page: 0,
            step: 0,
            hasMoreItems: 0
        }
    }
    return {
        lastPage: pagination.last_page,
        count: pagination.count,
        page: activePage ? activePage : undefined,
        step: Math.ceil(pagination.count / pagination.last_page),
        hasMoreItems: activePage ? activePage + 1 < pagination.last_page : undefined
    }
};

//region Table
export const convertTableFilters = (filters) => {
    const list = [];
    _.forEach(filters, (filter) => {
        list.push(convertTableFilter(filter))
    });
    return list;
};
export const convertTableFilter = (filter) => {
    switch (filter.name) {
        case "box": {
            return {
                ...filter,
                type: filterType.single,
                label: 'باکس',
                filters: convertNamesToLabels(filter.filters)
            }
        }
        case "categories": {
            return {
                ...filter,
                type: filterType.multi,
                label: 'دسته‌بندی',
                filters: convertNamesToLabels(filter.filters)
            }
        }
        case "group_id": {
            return {
                ...filter,
                type: filterType.multi,
                label: 'گروه',
                filters: convertNamesToLabels(filter.filters)
            }
        }
        default: {
            return {
                ...filter,
                type: undefined,
                label: ''
            }
        }
    }
};
export const convertNamesToLabels = (data) => {
    const list = [];
    _.forEach(data, (d) => {
        list.push(convertNameToLabel(d))
    });
    return list;
};
export const convertNameToLabel = (item) => {
    item.label = item.name;
    item.value = item.name;
    return item;
};
//endregion Table

export const convertIpg = (ipg) => {
    try {
        return {
            ...ipg,
            image: `${apiRout}/media/ipg/${ipg.key}.png`
        }
    } catch (e) {
    }
}

//region MultiLanguage
export function convertMultiLanguageObjArray({keys, array, def = null}) {
    try {
        const ar = [];
        _.forEach(array, (v) => {
            ar.push(convertMultiLanguageObj({keys: keys, val: v, def: null}))
        });
        return ar;
    } catch (e) {
        return array;
    }
}

export function convertMultiLanguageObj({keys, val, def = null}) {
    if (val === undefined)
        return def;
    _.forEach(keys, (k) => {
        val[k] = convertMultiLanguage(val[k]);
    });
    return val;
}

export function convertMultiLanguageArray(array, def = null) {
    if (array === undefined)
        return def;
    const ar = [];
    _.forEach(ar, (val) => {
        ar.push(convertMultiLanguage(val, def))
    });
    return ar
}

export function convertMultiLanguage(val, def = createMultiLanguage({})) {
    try {
        if (val === undefined || !val.fa === undefined)
            return def;
        return val;
    } catch (e) {
        return val
    }
}

export function createMultiLanguage({fa = '', en = '', ar = ''} = {}) {
    return {
        fa: fa,
        en: en,
        ar: ar
    }
}

export function createMultiLanguageObject({fa = {}, en = {}, ar = {}} = {}) {
    return {
        fa: fa,
        en: en,
        ar: ar
    }
}

//endregion MultiLanguage

export const createAndUpdatedAt = (data) => {
    try {
        return {
            createdAt: {
                by: {
                    ...data.created_by,
                    link: '/user/' + data.created_by.id
                },
                date: momentJalaali(new Date(data.created_at * 1000)).format('jYYYY/jM/jD  HH:mm'),
                oDate: data.created_at,
            },
            updatedAt: {
                by: {
                    ...data.updated_by,
                    link: '/user/' + data.updated_by.id
                },
                date: momentJalaali(new Date(data.updated_at * 1000)).format('jYYYY/jM/jD  HH:mm'),
                oDate: data.updated_at
            },
        }
    } catch (e) {
        return {
            createdAt: {
                by: {
                    name: "مهربات",
                    link: '/user/' + 1
                },
                date: momentJalaali().format('jYYYY/jM/jD  HH:mm'),
                oDate: "1398-01-01"
            },
            updatedAt: {
                by: {
                    name: "مهربات",
                    link: '/user/' + 1
                },
                date: momentJalaali().format('jYYYY/jM/jD  HH:mm'),
                oDate: "1398-01-01"
            },
        };
    }
}

//region File
export const getMediaTypeId = (type) => {

    if (_.isNumber(type))
        return type;
    let ty = undefined
    _.forEach(serverFileTypes.Image, (s) => {
        if (s.type === type) {
            ty = s;
        }
    })
    return ty ? ty.id : serverFileTypes.Image.Image.id
}


export function convertImage(m) {
    return {
        id: m.id,
        title: m.title,
        image: m.image,
        type: m.type,
        ...createAndUpdatedAt(m)
    }
}

export const defaultIcon = 'default.svg';

export const icons = {
    mehrTakhfif: {
        media: "mehr-takhfif.svg",
        name: createMultiLanguage({fa: "مهرتخفیف"})
    },
    helalAhmar: {
        media: "helal-ahmar.svg",
        name: createMultiLanguage({fa: "هلال احمر"})
    },
}


export function convertIconUrl(iconName) {
    if (!iconName)
        iconName = defaultIcon;
    return `${apiRout}/media/icon/${iconName}`
}

export function convertIconName(iconName) {
    if (!iconName)
        iconName = defaultIcon;
    return _.startCase(iconName.replaceAll(/.+?\\(?=\w+)|\.\w+$|$/g, ""))
}

//endregion File

export const deliver_status = {
    pending: {
        key: "pending",
        label: "درانتظار",
        color: yellow[600],
    },
    packing: {
        key: "packing",
        label: "بسته‌بندی",
        color: cyan[300],
    },
    sending: {
        key: "sending",
        label: "ارسال‌شده",
        color: lime[400],
    },
    delivered: {
        key: "delivered",
        label: "دریافت‌شده",
        color: green[600],
    },
    referred: {
        key: "referred",
        label: "مرجوع‌شده",
        color: red[300],
    },
}

export const invoice_status = {
    pending: {
        key: "pending",
        label: "درانتظار",
        color: yellow[600],
        selectable: false
    },
    sent: {
        key: "sent",
        label: "ارسال ‌شده",
        color: green[400],
        selectable: true
    },
    payed: {
        key: "payed",
        label: "پرداخت ‌شده",
        color: cyan[400],
        selectable: true
    },
    canceled: {
        key: "canceled",
        label: "لغو شده",
        color: red[400],
        selectable: false
    },
    rejected: {
        key: "rejected",
        label: "ّبرگشت خورده",
        color: red[400],
        selectable: false
    },
}


export function generateId(items) {
    let id = 1
    _.forEach(items, s => {
        if (s.id === id) {
            id = s.id + 1
        }
    })
    return id
}

//endregion Global

export const ApiHelper = {
    convertOrder: (order) => {
        let orderBy = order.orderBy;
        if (orderBy === 'index')
            orderBy = 'date';
        return (order.asc ? '' : '-') + orderBy
    },
    errorParams: (statusCode, delay = 0) => {
        if (statusCode === undefined)
            return {};
        return {
            ...ApiHelper.delay(delay),
            error: true,
            status_code: statusCode,
        }
    },
    delay: (delay) => {
        if (delay === undefined)
            return {};
        const s = delay / 1000;
        return {
            delay: s
        }
    },
    convertData: {
        location: (latLong) => {
            if (latLong)
                return [latLong.lat, latLong.lng];
            return null
        },
    },
    createSWRKey: ({api, order, filters, box = '', page, step}) => {
        const orderKey = order ? ((order.asc ? "true" : 'false') + '-' + order.orderBy) : '';
        const filtersKey = filters ? DataUtils().TableFilter().convertForSendToServer({
            filters: filters,
            returnString: true
        }) : '';
        return (api + box + "-" + page + "-" + step + '-' + orderKey + '-' + filtersKey);
    }
};
