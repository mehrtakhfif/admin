export const productType = {
    service: {
        isService: true,
        type: "service",
        label: "سرویس"
    },
    product: {
        isProduct: true,
        type: "product",
        label: "فیزیکی"
    },
    tourism: {
        isTourism: true,
        type: "tourism",
        label: "اقامتگاه"
    },
    package: {
        isPackage: true,
        type: "package",
        label: "پکیج"
    },
    package_item: {
        isPackageItem: true,
        type: "package_item",
        label: "پکیج آیتم"
    },
}

export const featureType = {
    bool: {
        key: 'bool',
        isBool: true,
        activeValue: false,
        typeLabel: 'بله/خیر',
    },
    text: {
        key: 'text',
        isSingle: true,
        activeValue: 1,
        typeLabel: 'متنی',
    },
    selectable: {
        key: 'selectable',
        isMulti: true,
        activeValue: [],
        typeLabel: 'تک انتخابی',
    },
};

export const serverFileTypes = {
    Image: {
        Image: {
            id: 1,
            type: 'image'
        },
        Thumbnail: {
            id: 2,
            type: 'thumbnail',
            width: 600,
            height: 372,
        },
        Media: {
            id: 3,
            type: 'media',
            width: 1280,
            height: 794,
        },
        Slider: {
            id: 4,
            type: 'slider',
            width: 1920,
            height: 504,
        },
        Ads: {
            id: 5,
            type: 'ads',
            // width: 820,
            // height: 300,
        },
        AdsMobile: {
            id: 6,
            type: 'mobile_ads',
            // width: 500,
            // height: 384,
        },
        Category: {
            id: 7,
            type: 'category',
            width: 800,
            height: 500,
        },
        SliderMobile: {
            id: 8,
            type: 'mobile_slider',
            width: 980,
            height: 860,
        },
        Description: {
            id: 9,
            type: 'description',
        },
    },
    SliderMobile: {
        id: 8,
        type: 'mobile_slider',
        width: 980,
        height: 860,
    },
    Video: {
        Audio: {
            id: 100,
            type: 'audio'
        },
    },
    Audio: {
        Audio: {
            id: 200,
            type: 'audio'
        },
    },
};

export const productDescription = {
    description:{
        key:"description",
        label:"توضیحات",
    },
    usage_condition:{
        key:"usage_condition",
        label:"شرایط استفاده"
    },
    how_to_use:{
        key:"how_to_use",
        label:"طریقه مصرف"
    },
    keeping_maintenance:{
        key:"keeping_maintenance",
        label:"شرایط نگهداری"
    },
    regulation:{
        key:"regulation",
        label:"مقررات"
    },
}


export const bookingType = {
    unbookable:{
        key:"unbookable",
        label:"غیرفعال",
    },
    datetime:{
        key:"datetime",
        label:"تاریخ و ساعت",
    },
    range:{
        key:"range",
        label:"بازه",
    }
}
