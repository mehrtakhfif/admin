import React, {useState, useRef} from 'react';
import {useSelector} from 'react-redux'
import Typography from "../base/Typography";
import {activeLang, lang} from "../../repository";
import NoneTextField from "../base/textField/NoneTextField";
import {bookingType as BookingTypeList, productType as productTypeList} from "../../controller/type";
import {
    Box,
    InputLabel,
    MenuItem,
    FormControl,
    Select,
    Button,
    TextField
} from "@material-ui/core"
import MaterialMenu from "@material-ui/core/Menu"
import {gcLog} from "../../utils/ObjectUtils";
import ControllerProduct from "../../controller/ControllerProduct";
import {rout} from "../../router";
import {generatePermalink} from "../base/textField/PermalinkTextField";
import {Redirect} from "react-router-dom";
import {createMultiLanguageObject} from '../../controller/converter';

const useStyles = {
    container: {},
    searchProductName: {},
    main: {},
    formControl: {
        width: "50%",
        display: "flex",
        margin: "32px 8px"
    },
    selectEmpty: {
        marginTop: "16px",
    },
    inputText: {
        width: "-webkit-fill-available",
        margin: "8px 15%"
    },
    textHeader: {alignSelf: "end"},
    productDetail: {width: "50%", justifyContent: "center"},
    saveButton: {background: "#2979FF", color: "#fff", padding: "8px", alignSelf: "flex-end", margin: "8px"}
}


function CreateProduct(props) {
    const nameRef = useRef()
    const boxes = useSelector(state => state.user.boxes);
    const [type, setType] = useState("")
    const [bookingType, setBookingType] = useState(BookingTypeList.unbookable);
    const [selectedBox, setSelectedBox] = useState("");
    const [similarProducts, setSimilarProducts] = useState([]);
    const [anchorEl, setAnchorEl] = useState(null);
    const [redirect,setRedirect] = useState()

    const handleBoxChange = (event) => {
        setSelectedBox(event.target.value);
    };
    const handleTypeChange = (event) => {
        gcLog("handleTypeChange", event.target.value)
        setType(event.target.value);
    }

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };


    const handleClose = (bookingType) => {
        if (bookingType)
            setBookingType(bookingType)
        setAnchorEl(null);
    };

    function searchProductName(e) {
        if (e.target.value.length % 3 == 0) {
            ControllerProduct.Product.searchProduct({query: nameRef.current.value}).then((res) => {
                gcLog("searchProductName res", res)
                if (res.status === 200)
                    setSimilarProducts(res.data.products)
            })
        }
    }


    const handleSave = () => {
        gcLog("handle save", nameRef.current.value)
        const data = {
            name: {'fa':nameRef.current.value},
            type: type,
            booking_type: bookingType.key,
            permalink: generatePermalink(nameRef.current.value),
            description:createMultiLanguageObject({fa: {items: []}, en: {items: []}, ar: {items: []}})
        }
        gcLog("req Data", {selectedBox, data})
        ControllerProduct.Product.createProduct({box: selectedBox, data}).then((res) => {
            gcLog("res", res)
            if (res.status === 200)

            gcLog(rout.Product.Single.ProductsNew.editProduct(res.data.id))
            setRedirect(rout.Product.Single.ProductsNew.editProduct(res.data.id))
        })
    }


    let Booking = <React.Fragment/>
    if (type === "product" || type === "package") {
        Booking = <Box pl={2} display={'flex'} alignItems={'center'}>
            <Typography pl={1}>
                رزرو:
            </Typography>
            <Button aria-controls="select-booking-type-menu" aria-haspopup="true" onClick={handleClick}>
                {bookingType.label}
            </Button>
            <MaterialMenu
                id="select-booking-type-menu"
                anchorEl={anchorEl}
                keepMounted open={Boolean(anchorEl)} onClose={() => {
                handleClose()
            }}>
                {
                    Object.keys(BookingTypeList).map(k => {
                        const it = BookingTypeList[k];
                        return (
                            <MenuItem key={k} onClick={() => handleClose(it)}>
                                {it.label}
                            </MenuItem>
                        )
                    })
                }
            </MaterialMenu>
            <NoneTextField name={"booking_type"} defaultValue={bookingType.key}/>
        </Box>
    }
    if (redirect!==undefined)
    return (
        <Redirect
            to={{
                pathname: redirect
            }}
        />
    )
    return (
        <Box width={1} display={'flex'} flexDirection={'row'}>
            <Box width={2 / 3} style={useStyles.main} display={'flex'} flexDirection={'column'} alignItems={'center'}>
                <Typography variant={"h3"} style={useStyles.textHeader} p={3} fontWeight={'600'}>ایجاد محصول
                    جدید</Typography>

                <Box width={1} className={"enNum"}>
                <TextField
                    name={"name"}
                    label={"نام محصول"}
                    inputRef={nameRef}
                    onChange={searchProductName}
                    style={useStyles.inputText}
                />
                </Box>
                <Box display={"flex"} style={useStyles.productDetail}>
                    <FormControl variant="outlined" style={useStyles.formControl}>
                        <InputLabel id="box">رسته</InputLabel>
                        <Select
                            labelId="box"
                            id="boxSelect"
                            value={selectedBox}
                            onChange={handleBoxChange}
                            label="رسته"
                        >
                            <MenuItem key={"s"} value={""}>
                                <em>هیچی</em>
                            </MenuItem>
                            {boxes.map((b) => (
                                <MenuItem key={b.id} value={b.id}>{b.name[activeLang]}</MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                    <FormControl variant="outlined" style={useStyles.formControl}>
                        <InputLabel id="type">نوع محصول</InputLabel>
                        <Select
                            labelId="type"
                            id="typeSelect"
                            value={type}
                            onChange={handleTypeChange}
                            label={lang.get("type")}>
                            <MenuItem key={"n"} value={""}>
                                <em>هیچی</em>
                            </MenuItem>
                            {Object.keys(productTypeList).map((key, index) => {
                                const item = productTypeList[key];
                                return (
                                    <MenuItem key={item.label} value={item.type}>{item.label}</MenuItem>
                                )
                            })}
                        </Select>
                    </FormControl>
                </Box>
                {Booking}
                <Button style={useStyles.saveButton} onClick={handleSave}>افزوردن محصول</Button>
            </Box>
            <Box width={1 / 3} flex={'column'} p={1}>
                <Typography variant={"h2"} fontWeight={400} p={1} display={similarProducts.length>0?'block':'none'}>محصولات مشابه:</Typography>
                {similarProducts?.map((product)=>(
                    <Typography variant={"body1"} width={"100%"} p={0.5}>{product?.name["fa"]}</Typography>
                    ))
                }
            </Box>
        </Box>
    )
}

CreateProduct.whyDidYouRender = true
export default CreateProduct;

