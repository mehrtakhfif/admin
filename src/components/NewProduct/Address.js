import React, {Fragment, useEffect, useRef, useState} from 'react';
import {
    Box, Button, Card, Collapse, Chip, IconButton, Tooltip,
    ButtonBase,
    Divider,
    Radio,
    Dialog,
    DialogContent,
    DialogContentText,
    DialogActions,
    DialogTitle,
} from '@material-ui/core';
import {makeStyles, useTheme} from '@material-ui/core/styles';
import {blue, cyan, green, grey, orange, red} from '@material-ui/core/colors';
import {
    Add,
    DeleteOutline,
    Close,
    LocationOn,
    LocationOff,
    EditLocation,
    Cancel,
    Edit
} from '@material-ui/icons';
import useSWR, {mutate} from 'swr';
import _ from 'lodash';
import {useSnackbar} from 'notistack';
import {activeLang, lang, siteLang, sLang, theme} from '../../repository';
import {gcError, gcLog, getSafe} from '../../utils/ObjectUtils';
import {convertNameToLabel, createMultiLanguage, states} from '../../controller/converter';
import Typography from '../base/Typography';
import TextField from '../base/textField/TextField';
import BaseButton from '../base/button/BaseButton';
import {UtilsStyle} from '../../utils/Utils';
import ControllerSite from '../../controller/ControllerSite';
import AutoFill from '../base/autoFill/AutoFill';
import MapImage from '../../drawable/image/map.jpg';
import SelectLocation from '../addAddress/SelectLocation';
import icon from '../../drawable/mapImage/marker-icon.png';
import AddAddressMapComponent from '../addAddress/Map';
import L from "leaflet";
import {Map as LeafletMap, Marker, TileLayer} from "react-leaflet";
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox'
import PhoneTextField from "../base/textField/PhoneTextField";

const useStyles = makeStyles((theme) => ({
        
        root: {
            '& > *': {
                margin: theme.spacing(1),
            },
            
        },
        brandContainer: {
                margin: '8px',
                border: '#e2e2e2 solid',
                borderRadius: '8px',
                padding: '8px',
                height: 'fit-content'
            },
        inputText : {margin:'4px'}
    }),
);


const useMapStyles = makeStyles({
    map: {
        '& .leaflet-marker-pane img': {
            top: -41,
            right: -12.5,
            width: 25,
            height: 41
        },
        '& .leaflet-popup': {
            bottom: '22px !important',
        }
    }
});


function Address({...props}) {
    const classes = useStyles();
    const [data, setData] = useState(props.data);
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();
    gcLog('Address', {data});
    
    
    
    function updateProduct(inputData) {
        return props.updateProduct(props.id, inputData);
    }
    
    function handleCityChange(cities) {
        setData({
            ...data,
            cities: cities
        })
        
    }
    
    function handleLocationChange(location) {
        const backup = _.cloneDeep(data)
        setData(s=>({
            ...s,
            location: location,
        }));
        updateProduct({location:location}).catch(e=>{
            setData(backup)
        })
    }
    
    return (
        <Box display={'flex'} width={1} flexDirection={'column'} >
            <ContactInformation/>
            <Cities data={data}
                    onChange={handleCityChange}/>
            {/*{props.data.type==="service"&&*/}
                <Addresses item={data}/>
            {/*}*/}
            {/*{props.data.type==="service"&&*/}
                <MapCo
                    item={data}
                    onChange={handleLocationChange}/>
                    {/*}*/}
        </Box>
    );
    
    function Cities({active, data, onChange, ...props}) {
        const {enqueueSnackbar, closeSnackbar} = useSnackbar();
        const [state, setState] = useState({
            state: undefined,
            city: undefined,
        })
        const [openStateSelectionDialog,setOpenStateSelectionDialog] = useState(false)
        const [selectedCities, setSelectedCities] = useState(data.cities)
        const [selectedCitiesCheckboxTemp, setSelectedCitiesCheckboxTemp] = useState(data.cities)
        const apiKey = `cities-${state.state ? state.state.id : ""}`
        const {data: cities, error: citiesError} = useSWR(apiKey, () => {
            if (!state.state)
                return undefined
            return ControllerSite.cities.get({stateId: state.state.id}).then((res) => {
                // setSelectedCitiesCheckboxTemp(res.data.cities)
                return res.data.cities;
            })
        }, {
            dedupingInterval: 60000 * 20
        })
    
    
        const handleChangeCitySelection = (city) => {
            const index = selectedCitiesCheckboxTemp.findIndex(f=>f.id === city.id)
            gcLog("handleChangeCitySelection",{city,selectedCitiesCheckboxTemp,a:index})
            if (index===-1)
                {
                    setSelectedCitiesCheckboxTemp(s=> s.concat([city]))
                    return
                }
    
            setSelectedCitiesCheckboxTemp(s=> s.filter(item=>item.id!==city.id))
        };
        
        
        function openStateSelection() {
            setOpenStateSelectionDialog(s=>!s)
        }
        
        function handleCloseStateSelectionDialog() {
            setOpenStateSelectionDialog(false)
        }
        function handleSelectDialogAction() {
            const oldState= _.cloneDeep(selectedCities)
            setSelectedCities(selectedCitiesCheckboxTemp)
            handleCloseStateSelectionDialog()
            onChange(selectedCitiesCheckboxTemp)
            let ids = []
            selectedCitiesCheckboxTemp.forEach(item=>ids.push(item.id))
            updateProduct({'cities':ids}).catch(e=>{
                onChange(oldState)
                setSelectedCities(oldState)
            })
    
        }
        
        function handleSelectAllSelectionDialog() {
            setSelectedCitiesCheckboxTemp(cities)
        }
        
        function handleSelectNoneSelectionDialog() {
            setSelectedCitiesCheckboxTemp([])
        }
        
        
        
        return (
            <Box m={2} width={1}>
                <Dialog
                    fullWidth={true}
                    maxWidth={'sm'}
                    open={openStateSelectionDialog}
                    onClose={handleCloseStateSelectionDialog}
                    aria-labelledby="max-width-dialog-title"
                ><DialogTitle id="max-width-dialog-title">انتخاب شهر ها</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            شهر های مورد نظر خورد را انتخاب کنید
                            <Button onClick={handleSelectAllSelectionDialog}>انتخاب همه</Button>
                            <Button onClick={handleSelectNoneSelectionDialog}>حذف انتخاب ها</Button>
                        </DialogContentText>
                            <FormControl component="fieldset" >
                                <FormGroup>
                                    {
                                        cities?.map(city=>{
                                            return <FormControlLabel
                                                key={city.id}
                                                control={<Checkbox checked={selectedCitiesCheckboxTemp.findIndex(i=>i.id===city.id)!==-1} onChange={(event)=>handleChangeCitySelection(city)} name={city.name} />}
                                                label={city.name}
                                            />
                                        })
                                    }
                                </FormGroup>
                             </FormControl>
                        </DialogContent>
                    <DialogActions>
                        <Button onClick={handleCloseStateSelectionDialog} color="primary">
                            بستن
                        </Button>
                        <Button onClick={handleSelectDialogAction} color="primary">
                            انتخاب
                        </Button>
                    </DialogActions>
                </Dialog>
                <Box display={'flex'} width={1} flexDirection={'column'}>
                    <Box display={'flex'} alignItems={'flex-start'} width={1} px={2} py={2}>
                        <Box display={'flex'} flex={1}>
                            <Box width={0.5} pr={1}>
                                <AutoFill
                                    inputId='stateInput'
                                    items={states}
                                    activeItem={state.state}
                                    onActiveItemChanged={(item) => {
                                        setState({
                                            ...state,
                                            state: item,
                                            city: null
                                        });
                                    }}
                                    title={lang.get('state')}
                                    placeholder={lang.get('search_state')}/>
                            </Box>
                            <Box width={0.5} pr={3} alignItems={'center'}>
                                <Button disabled={_.isEmpty(state.state)} onClick={openStateSelection}>انتخاب شهرها</Button>
                            </Box>
                        </Box>
                        {/*<SuccessButton
                            variant={"outlined"}
                            onClick={() => {
                                if (_.findIndex(selectedCities, (cy) => {
                                    return cy.id === state.city.id
                                }) !== -1) {
                                    enqueueSnackbar(`شهر ${state.city.name} تکراری میباشد`,
                                        {
                                            variant: "error",
                                            action: (key) => (
                                                <Fragment>
                                                    <Button onClick={() => {
                                                        closeSnackbar(key)
                                                    }}>
                                                        {lang.get('close')}
                                                    </Button>
                                                </Fragment>
                                            )
                                        });
                                } else {
                                    setState({
                                        ...state,
                                        city: null,
                                    })
                                    
                                    if (!state.city)
                                        return
                                    setSelectedCities([
                                        ...selectedCities,
                                        {
                                            ...state.city,
                                            stateId: state.city.state ,
                                            state: _.find(states, (s) => s.id === state.city.state)
                                        }
                                    ])
                                    
                                }
                                
                            }}
                            style={{
                                marginRight: theme.spacing(1)
                            }}>
                            <Add
                                style={{
                                    marginLeft: theme.spacing(1)
                                }}/>
                            افزودن
                        </SuccessButton>*/}
                    </Box>
                    <Box display={'flex'} flexWrap={'wrap'} maxHeight={'200px'} flex={1} style={{overflowY:'auto'}}>
                        {
                            selectedCities.map(ci => (
                                <Box key={ci.id} p={2}>
                                    <Box display={'flex'} minWidth={150} flexDirection={'column'} p={1} component={Card}
                                         >
                                        <Box display={'flex'} alignItems={'center'}>
                                            <Box display={'flex'} flexDirection={'column'} flex={1}>
                                                <Typography variant={"h6"} fontWeight={500}>
                                                    {ci.name}
                                                </Typography>
                                                <Typography variant={"body1"}>
                                                    استان:
                                                    {" " + states.filter(item=>item["id"]===ci.state)[0]?.label}
                                                </Typography>
                                            </Box>
                                            <Tooltip title={"حذف"}>
                                                <IconButton
                                                    onClick={() => {
                                                        const newList = selectedCities;
                                                        _.remove(newList, (cy) => {
                                                            return cy.id === ci.id
                                                        })
                                                        handleChangeCitySelection(ci)
                                                        handleSelectDialogAction()
                                                        // setSelectedCities([...newList])
                                                    }}>
                                                    <DeleteOutline/>
                                                </IconButton>
                                            </Tooltip>
                                        </Box>
                                    </Box>
                                </Box>
                            ))
                        }
                    </Box>
                </Box>
            </Box>
        )
    }
    
    function Addresses({item, ...props}) {
        const [state, setState] = useState({
            activeLanguage: sLang[siteLang]
        })
        
        function handleShortAddressChange(e) {
            const backup = _.cloneDeep(data)
            setData(s=>({...s,short_address:{[siteLang]:e.target.value}}))
            updateProduct({'short_address':{[siteLang]:e.target.value}}).catch(e=>{
                setData(backup)
            })
        }
        
        function handleAddressChange(e) {
            const backup = _.cloneDeep(data)
            setData(s=>({...s,address:{[siteLang]:e.target.value}}))
            updateProduct({'address':{[siteLang]:e.target.value}}).catch(e=>{
                setData(backup)
            })
        }
        
        return (
            <Box display={'flex'}>
                <Box width={0.3} m={1}>
                    <TextField
                        className={classes.inputText}
                        name={'name'}
                        variant={'outlined'}
                        label={'آدرس کوتاه'}
                        fullWidth={true}
                        multiline={true}
                        // inputRef={nameRef}
                        defaultValue={data.short_address?.[siteLang]||null}
                        onFocusOut={handleShortAddressChange}/>
                </Box>
                <Box width={0.7} m={1}>
                    <TextField
                        className={classes.inputText}
                        name={'name'}
                        variant={'outlined'}
                        label={'آدرس'}
                        fullWidth={true}
                        multiline={true}
                        // inputRef={nameRef}
                        defaultValue={data?.address?.[siteLang]}
                        onFocusOut={handleAddressChange}/>
                </Box>
            </Box>)
        
}

    function ContactInformation() {
        const [details,setDetails] = useState(data?.details?.[siteLang])
        const defaultHours = ['9 الی 21','9 الی 14 و 17 الی 21','']
        const defaultDays = ['با هماهنگی','شنبه تا پنجشنبه بجز روز های تعطیل','همه روزه']

        function handleDetailsChange(text,type){

            setDetails(s=>{
                    const d = {...s,[type==='time'?'hours':'days']:text}
                    updateProduct({'details': {[siteLang]:d}}).catch(e=>{
                        return s
                    })
                    return d
                }
                )
        }

        function addDefaultsHours(text){
            setDetails(s=>{
                const d = {...s,hours:text}
                updateProduct({'details': {[siteLang]:d}}).catch(e=>{
                    return s
                })
                return d})
        }

        function handleOnChange(text,type){
            setDetails(s=>{
                const d = {...s,[type==='time'?'hours':'days']:text}
                return d})

        }

        function addDefaultsDays(text){
            setDetails(s=>{
                const d = {...s,days:text}
                updateProduct({'details': {[siteLang]:d}}).catch(e=>{
                    return s
                })
                return d})
        }

        function handlePhoneChange(text){
            setDetails(s=>{
                const d = {...s,phone:[text.target.value]}
                updateProduct({'details': {[siteLang]:d}}).catch(e=>{
                    return s
                })
                return d})
        }

        return (<Box mt={3} flexDirection={'row'} display={'flex'}>
            <Box width={1/2} m={1}>
                <Box mt={2}>
                    <Typography variant={'body2'} fontWeight={'bold'}>زمان سرویس دهی :</Typography>
                    <Box display={'flex'} flexDirection={'row'}>
                        {
                            defaultHours.map(item=>{
                                return (<Button key={item} onClick={(e)=>addDefaultsHours(item)}>{item}</Button>)
                            })
                        }
                    </Box>
                    <TextField
                        className={classes.inputText}
                        name={'hours'}
                        variant={'outlined'}
                        mb={3}
                        rows={3}
                        value={details?.hours}
                        defaultValue={details?.hours}
                        onChange={(e)=>{handleOnChange(e.target.value,'time')}}
                        multiline={true}
                        fullWidth={true}
                        onFocusOut={(e)=>handleDetailsChange(e.target.value,'time')}
                    />
                </Box>
                <Box mt={3}>
                    <Typography variant={'body2'} fontWeight={'bold'}>روز های سرویس دهی :</Typography>
                    <Box display={'flex'} flexDirection={'row'} >
                        {
                            defaultDays.map(item=>{
                                return (<Button key={item} onClick={(e)=>addDefaultsDays(item)}>{item}</Button>)
                            })
                        }
                    </Box>
                    <TextField
                        className={classes.inputText}
                        name={'days'}
                        variant={'outlined'}
                        rows={3}
                        value={details?.days}
                        onChange={(e)=>{handleOnChange(e.target.value,'days')}}
                        multiline={true}
                        fullWidth={true}
                        onFocusOut={(e)=>handleDetailsChange(e.target.value,'days')}
                    />
                </Box>
            </Box>
            <Box width={1/2} pl={20} pr={20}  dir={'ltr'} flexDirection={'column'} display={'flex'} alignItems={'center'} justifyContent={'center'}>
            <TextField
                className={classes.inputText}
                name={'phone'}
                variant={'outlined'}
                label={'شماره تماس'}
                fullWidth={true}
                defaultValue={details?.phone?.[0]}
                onFocusOut={handlePhoneChange}
            />
            </Box>


        </Box>)
    }

}

export default Address;


//region Map

function MapCo({active, item, onChange, ...props}) {
    const theme = useTheme();
    const {location} = item;
    const [state, setState] = useState({
        showMap: false,
        mapOpen: false,
    });
    
    return (
        <Box m={3} height={'400px'} display={'flex'} position={'relative'} style={{ backgroundImage: state.showMap ? null : `url(${MapImage})`,}}>
                {state.showMap?<Button style={{zIndex: '1000',position: 'absolute'}}  onClick={() => setState({...state, showMap: false})} ><Cancel/></Button>:null}
            <Box  position={'absolute'} bgcolor={"#fff"} style={{top:'0px',bottom:'0px',left: '0px',right:'0px',margin:'auto',width:'fit-content',height:'min-content'}}>
                {<Tooltip title={lang.get(location ? "edit_address" : "add_address")}><Button onClick={() => setState({...state, mapOpen: true})}><Edit/></Button></Tooltip>}
                {!_.isEmpty(location) ?<Tooltip title={lang.get("show_address_on_map")}><Button
                    onClick={() => setState({
                        ...state,
                        showMap: true
                    })}><LocationOn/></Button></Tooltip>:null}
                {!_.isEmpty(location) ?<Tooltip title={'حذف آدرس'}><Button onClick={() => onChange(null)}><LocationOff/></Button></Tooltip>:null}
            </Box>
            {state.showMap ?
                <MapComponent
                    location={[
                        location.lat,
                        location.lng,
                    ]}/> :null}
            <SelectLocation
                open={state.mapOpen}
                title={lang.get(location ? "edit_address" : "add_address")}
                targetLocation={{
                    lat: (location && location.lat) ? location.lat : 37.28049,
                    lng: (location && location.lng) ? location.lng : 49.59051
                }}
                onSelect={(val) => {
                    setState({
                        ...state,
                        mapOpen: false
                    })
                    if (val)
                        onChange(val)
                }}/>
        </Box>
    )
}

let DefaultIcon = L.icon({
    iconUrl: icon,
});
L.Marker.prototype.options.icon = DefaultIcon;
function MapComponent({location, ...props}) {
    const classes = useMapStyles(props);
    return (
        <LeafletMap
            className={classes.map}
            animate={true}
            center={location}
            doubleClickZoom={true}
            boxZoom={true}
            zoomControl={true}
            minZoom={7}
            zoom={14}
            maxZoom={19}
            length={4}
            style={{
                width: '100%',
                height: '100%',
            }}>
            <TileLayer
                attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"/>
            <Marker position={location}/>
        </LeafletMap>
    )
}

const useMapStyle = makeStyles(theme => ({
    mapContainer: {},
    mapSubmitBtn: {
        paddingTop: theme.spacing(2),
        paddingBottom: theme.spacing(2),
        ...UtilsStyle.borderRadius(5),
    },
    
    mapSubmitBtnContainer: {
        position: 'absolute',
        zIndex: 3,
        bottom: 0,
        left: 0,
        right: 0,
        width: '100%',
    },
}));

export function FullScreenMap(props) {
    const {firstLocation, onSubmitClick, onReturnClick} = props;
    const classes = useMapStyle(props);
    const defaultLocation = firstLocation ? firstLocation : {
        lat: 37.28049,
        lng: 49.59051,
    };
    const [map, setMap] = React.useState({
        centerLocation: defaultLocation,
    });
    return (
        <Box display='flex'
             boxShadow={3}
             className={classes.mapContainer}
             flexDirection='column'
             width={1}
             height={1}>
            <AddAddressMapComponent
                centerLocation={map.centerLocation}
                targetLocation={defaultLocation}
                onMoveEnd={(center) => {
                    setMap({
                        ...map,
                        centerLocation: center
                    })
                }}
                style={{
                    zIndex: 2
                }}/>
            <Box className={classes.mapSubmitBtnContainer} display='flex'>
                <Box width="70%">
                    <BaseButton
                        disabled={false}
                        size='large'
                        className={classes.mapSubmitBtn}
                        onClick={() => onSubmitClick(map.centerLocation)}
                        style={{
                            backgroundColor: blue[500],
                            color: "#fff",
                            width: '100%'
                        }}>
                        {lang.get('submit_location')}
                    </BaseButton>
                </Box>
                <Box width='30%'>
                    <BaseButton
                        disabled={false}
                        variant={"text"}
                        size='large'
                        className={classes.mapSubmitBtn}
                        onClick={onReturnClick}
                        style={{
                            backgroundColor: red[500],
                            color: "#fff",
                            width: '100%'
                        }}>
                        {lang.get('return')}
                    </BaseButton>
                </Box>
            </Box>
        </Box>
    )
}

//endregion Map
