import React, {useRef, useState, Fragment} from 'react';
import {productDescription} from '../../controller/type';
import {gcLog, getSafe} from '../../utils/ObjectUtils';
import _ from 'lodash';
import {
    makeStyles,
} from '@material-ui/core/styles';
import {
    Box,
    Typography,
    Button,
} from '@material-ui/core';
import Editor, {htmlToEditorContent} from '../base/editor/Editor';
import {siteLang} from '../../repository';
import {createMultiLanguageObject} from '../../controller/converter';


const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            margin: theme.spacing(1),
        },
    },
    usageConditionDescription: {height: '445px'},
    DescriptionTypography: {
        margin: '8px',
        fontWeight: '400',
    },
}));

function UsageConditions({...props}) {
    const classes = useStyles();
    const [data, setData] = useState(_.isEmpty(props?.data?.description)?{...props.data,description: createMultiLanguageObject({fa: {items: []}, en: {items: []}, ar: {items: []}})}:props.data)
    const [currentDescription, setCurrentDescription] = useState(
        productDescription[Object.keys(productDescription)[0]]);
    const id = props.id;
    const productDescriptionItems = getSafe(()=>data.description[siteLang].items,[])

    function updateProduct(inputData) {
        return props.updateProduct(id, inputData);
    }
    
    function onChange(key, value) {
        // if (value.length < 9,)
        //     return;
        const newD = _.cloneDeep(data);
        const i = _.findIndex(newD.description[siteLang].items, (d) => {
            return d.key === key;
        });
        i === -1 ?
            newD.description[siteLang].items.push(
                {key: key, value: value}) :
            newD.description[siteLang].items[i].value = value;
        const backup = _.cloneDeep(data);
        setData(s => ({...s, ...newD}));
        updateProduct({'description': newD.description}).then(res => {
        
        }).catch((e) => {
            setData(backup);
        });
        
    }
    
    function handleDescriptionChange(key) {
        setCurrentDescription(productDescription[key]);
    }
    
    return (
        <Box m={1}>
            <Box display={'flex'} flexDirection={'row'}>
                {Object.keys(productDescription).map(des => {
                    return <Button onClick={() => handleDescriptionChange(des)}
                                   variant={'contained'}
                                   color={des === currentDescription.key ?
                                       'primary' :
                                       ''}
                                   key={productDescription[des].key}>{productDescription[des].label}</Button>;
                })}
            </Box>
            <Box m={3} className={classes.usageConditionDescription}
                 bgcolor={'#fff'} p={1}>
                <Typography variant={'h6'}
                            className={classes.DescriptionTypography}>{currentDescription.label}</Typography>
                <Editor
                    content={productDescriptionItems.filter(
                        o => o.key === currentDescription.key)[0]?.value}
                    onBlur={(content) => onChange(currentDescription.key,
                        content.trim())}
                />
            </Box>
        </Box>
    );
}

export default UsageConditions;