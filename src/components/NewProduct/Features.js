import React,{useState,useEffect,Fragment} from 'react';
import {gcLog, tryIt} from '../../utils/ObjectUtils';
import {activeLang, lang, siteLang, sLang, webRout} from '../../repository';
import {makeStyles} from '@material-ui/core/styles';
import SelectFeatureValue  from '../Feature/SelectFeatureValue' ;
import {
    Box,
    Typography,
    Button,
    TextField,
    IconButton,
    Collapse}
from '@material-ui/core';
import BaseButton from '../base/button/BaseButton';
import ProductFeatureGroup from '../Feature/ProductFeatureGroup';
import OrderProductFeature from '../../pages/products/OrderProductFeature';
import {featureType} from '../../controller/type';
import DefaultTextField from '../base/textField/DefaultTextField';
import ControllerProduct from '../../controller/ControllerProduct';
import _ from 'lodash'
import {useSnackbar} from "notistack";
import {blue, cyan, green, grey, orange, red} from "@material-ui/core/colors";
import {
    Delete,
    Edit,
} from "@material-ui/icons";
import {convertFeatureValue} from '../../controller/converter';

const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            margin: theme.spacing(1),
        },
    },
}))

function Features({...props}) {
    const classes = useStyles();
    const [data, setData] = useState(props.data);
    gcLog("NewProduct features",{data})
    
    
    useEffect(()=>{
        gcLog("Features useEffect",{data})
    },[data])
    
    
    
    
    function updateProduct(inputData) {
        return props.updateProduct(props.id, inputData);
    }
    
    function handleFeatureChange(features) {
            let sortedFeatures = null
            try {
                sortedFeatures = features.features.sort((a, b) => {
                    const ap = a.priority === undefined ? a.feature.priority : a.priority
                    const bp = b.priority === undefined ? b.feature.priority : b.priority
                
                
                    if (ap === undefined) return 1
                    if (bp === undefined) return -1
                
                    return (ap > bp) ? 1 : -1
                })
            } catch (e) {
                sortedFeatures = features.features
            }
        
            // const backup = _.cloneDeep(data)
            updateProduct({'features':convertFeatureValue(sortedFeatures)}).then(res=>{
                setData(s => ({
                    ...s,
                    features: sortedFeatures
                }))
            })
        
            
    }
    
    return (
        <Box>
            <Feature productId={props.id} item={data}
                      onChange={handleFeatureChange}/>
        </Box>
    );
    
    function sortFeatures(features) {
        const text = [];
        const bool = [];
        const selectable = [];
        _.forEach(features, (f) => {
            switch (f.feature.type.key) {
                case featureType.bool.key: {
                    bool.push(f)
                    break
                }
                case featureType.selectable.key: {
                    selectable.push(f)
                    break
                }
                default: {
                    text.push(f)
                }
            }
        })
        
        return [...text,
            ...bool,
            ...selectable]
    }
    
    function Feature({productId, active, item, onChange, ...props}) {
        const {enqueueSnackbar, closeSnackbar} = useSnackbar();
        const {address, shortAddress} = item;
        const [openOrder, setOpenOrder] = useState(false)
        const [productFeatures, setProductFeatures] = useState(sortFeatures(item.features));
        const [state, setState] = useState({
            activeLanguage: sLang[siteLang]
        })
        const [openSearchFeature, setOpenSearchFeature] = useState(false)
        const [productGroupDialog, setProductGroupDialog] = useState(false)
        const [featureGroupData, setFeatureGroupData] = useState([])
        
        
        // useEffect(() => {
        //     return ()=>onChange({features: productFeatures})
        // }, productFeatures)/
        
        function handleOnClose() {
            setOpenSearchFeature(false)
        }
        
        function NotRemovableAlert(values) {
            values.map(value => (
                enqueueSnackbar(`فیچر ${value.name[siteLang]} در انبار استفاده شده و قابل حذف نمی باشد`,
                    {
                        variant: "error",
                        action: (key) => (
                            <Fragment>
                                <Button onClick={() => {
                                    closeSnackbar(key)
                                }}>
                                    {lang.get('close')}
                                </Button>
                            </Fragment>
                        
                        )
                    })
            ))
            
        }
        
        function handleRemoveFeature(it) {
            const elementsIndex = productFeatures.findIndex(element => element.feature.id === it.feature.id)
            if (elementsIndex !== -1) {
                const notRemovable = _.filter(productFeatures[elementsIndex].values, it => !_.isEmpty(it.storage_id))
                if (notRemovable.length === 0) {
                    const newArray = _.cloneDeep(productFeatures).filter(item => item.feature.id !== it.feature.id)
                    setProductFeatures(sortFeatures(newArray))
                    onChange({features: newArray})
                    return
                }
                const newArray = _.cloneDeep(productFeatures)
                NotRemovableAlert(notRemovable)
                newArray[elementsIndex].values = notRemovable
                setProductFeatures(sortFeatures(newArray))
                onChange({features: newArray})
                
                
            }
        }
        
        function handleSelection(selectedFeature, selectedValues, type = null) {
            
            const elementsIndex = _.isUndefined(productFeatures) ? -1 : productFeatures.findIndex(element => element.feature.id === selectedFeature.id)
            if (elementsIndex !== -1) {
                let newArray = _.cloneDeep([...productFeatures])
                const notRemovable = _.filter(productFeatures[elementsIndex].values, it => !_.isEmpty(it.storage_id))
                let o = notRemovable
                selectedValues.map(value => {
                        const index = _.findIndex(notRemovable, item => item.id === value.id)
                        if (index === -1) {
                            o = o.concat(value)
                        }
                        
                    }
                )
                newArray[elementsIndex] = {...newArray[elementsIndex], values: o}
                setProductFeatures(sortFeatures(newArray))
                setOpenSearchFeature(false)
                onChange({features: newArray})
                return
            }
            
            let newArray = undefined
            setProductFeatures(d => {
                newArray = d.concat([{feature: selectedFeature, values: selectedValues}])
                return sortFeatures(newArray)
            })
            onChange({features: newArray})
            
            setOpenSearchFeature(false)
            
            
        }
        
        function handleGroupSelection(data) {
            gcLog("xasxasxasxasxasxasxasxassxas", {data,productFeatures})
            let tempFeatures = _.cloneDeep(productFeatures)
            data.map(featureValue => {
                
                const fIndex = _.findIndex(productFeatures, f => f.feature.id === featureValue.feature.id)
                if (fIndex === -1) {
                    tempFeatures = tempFeatures.concat(featureValue)
                    // setProductFeatures(d => {
                    //         let o = d.concat(featureValue)
                    //         onChange({features: o})
                    //         setProductGroupDialog(false)
                    //         return sortFeatures(o)
                    //     }
                    // )
                    /* gcLog("kkkkkkkkkkkkkkkkk productFeatures",productFeatures)
    
    
    
    
                     let dd = _.cloneDeep(productFeatures)
                     dd = dd.concat(featureValue)
                     setProductFeatures(dd)
                     gcLog("kkkkkkkkkkkkkkkkk",dd)
                     onChange({features: dd})
                     setProductGroupDialog(false)*/
                    
                    
                    return
                }
                const f = productFeatures[fIndex]
                
                const notRemovable = _.filter(f.values, it => (it.selected))
                
                let o = notRemovable
                
                featureValue.values.map(value => {
                        const index = _.findIndex(notRemovable, item => item.id === value.id)
                        if (index === -1) {
                            o = o.concat(value)
                        }
                        
                    }
                )
                
                f.values = o

                tempFeatures[fIndex] = f
                tempFeatures = tempFeatures.concat(featureValue)
                // setProductFeatures(dd => {
                //     dd[fIndex] = f
                //     onChange({features: dd})
                //     setProductGroupDialog(false)
                //     return sortFeatures(dd)
                // })
                
                
            })

             setProductFeatures(dd => {
                //     dd[fIndex] = f
                    onChange({features: tempFeatures})
                //     setProductGroupDialog(false)
                    return sortFeatures(tempFeatures)
                })

        }
        
        function openGroupFeature() {
            if (productId === undefined) {
                enqueueSnackbar(`برای دیدن گروه دسته بندی اول محصول را ذخیره کنید`,
                    {
                        variant: "error",
                        action: (key) => (
                            <Fragment>
                                <Button onClick={() => {
                                    closeSnackbar(key)
                                }}>
                                    {lang.get('close')}
                                </Button>
                            </Fragment>
                        
                        )
                    })
            }
            setProductGroupDialog(s => !s)
        }
        
        
        return (
            <Box m={2}>
                <Box display={"flex"} m={2}>
                    <BaseButton fullWidth={true}
                                variant={'outlined'}
                                style={{margin: "8px"}}
                                onClick={() => {
                                    setOpenSearchFeature(s => (!s))
                                }}>
                        جست و جوی ویژگی
                    </BaseButton>
                    <BaseButton
                        fullWidth={true}
                        variant={'outlined'}
                        style={{margin: "8px"}}
                        onClick={openGroupFeature}>
                        
                        افزودن ویژگی های دسته بدنی</BaseButton>
                    
                    <BaseButton
                        fullWidth={true}
                        variant={'outlined'}
                        style={{margin: "8px"}}
                        onClick={() => {
                            setOpenOrder(true)
                            
                        }}>
                        مرتبسازی
                    </BaseButton>
                </Box>
                <ProductFeatureGroup
                    open={productGroupDialog}
                    productFeatures={productFeatures}
                    productId={productId}
                    onClose={() => {
                        setProductGroupDialog(s => (!s))
                    }}
                    onDone={handleGroupSelection}/>
                <SelectFeatureValue
                    open={openSearchFeature}
                    onClose={() => {
                        setOpenSearchFeature(s => (!s))
                    }}
                    onDone={handleSelection}/>
                <Box
                    display={'flex'}
                    flex={1}
                    flexDirection={'column'}>
                    {productFeatures ?
                        productFeatures.map(it => (
                            <FeatureItem key={it.feature.id} it={it}
                                         handleSelection={handleSelection}
                                         onRemoveFeature={handleRemoveFeature}/>
                        )) : <React.Fragment/>}
                </Box>
                <OrderProductFeature
                    open={openOrder}
                    productId={productId}
                    onClose={(saved) => {
                        setOpenOrder(false)
                        if (saved)
                            enqueueSnackbar("ذخیره شد. بعد از رفرش قابل مشاهده میباشد.",
                                {
                                    variant: "info",
                                    action: (key) => (
                                        <Fragment>
                                            <Button onClick={() => {
                                                closeSnackbar(key)
                                            }}>
                                                {lang.get('close')}
                                            </Button>
                                        </Fragment>
                                    )
                                });
                    }}/>
            </Box>
        )
    }
    
    function FeatureItem({it, handleSelection, onRemoveFeature, ...props}) {
        const [loading, setLoading] = useState(false)
        const [textFeatureValue, setTextFeatureValue] = useState("")
        const [collapse, setCollapse] = useState(false)
        
        
        let type = tryIt(() => {
            return it.feature.type
        }, "")
        return (
            <Box display={'flex'}
                 style={{borderBottom: "#e2e2e2 2px solid"}} flexDirection={'row'} width={'100%'} alignItems={"center"}>
                <Box display={'flex'} flexDirection={'column'} m={1} >
                <Typography alignItems={"center"}  variant={'h5'}  p={1}>
                    {it.feature.name[siteLang]}
                </Typography>
                    {
                        (_.isObject(it.feature) && it.feature.type) &&
                        <React.Fragment>
                            <Typography variant={"body2"} m={0.5}>
                                ( {(_.isObject(it.feature.type) ? it.feature.type : featureType[it.feature.type]).typeLabel} )
                            </Typography>
                        </React.Fragment>
                    }
                </Box>:
                <Box flex={1} m={1} width={'400px'} display={"flex"} alignItems={"center"} style={{overflowX: 'auto',overflowY:'hidden'}}>
                    {it.values.map(value => (
                        <Box key={value.id}>
                            <Typography justifyContent={"center"} alignItems={"center"} variant={'subtitle1'}
                                        justify={'center'} key={value.id} style={{
                                color: _.isEmpty(value.storage_id) ? "#848383" : "#de3f3f",padding:'10px'
                            }}>
                                {!(collapse && type.key === featureType.text.key) ? `   ${value.name[siteLang]}` : ""}
                            </Typography>
                            <Collapse in={collapse && type.key === featureType.text.key}
                                      style={{display: type.key === featureType.text.key ? "flex" : "none"}}>
                                <Box display={"flex"}
                                     flex={1}
                                     alignItems={"center"}>
                                    <Box display={'flex'} flex={1} alignItems={"center"}
                                         m={2}
                                         style={{
                                             minWidth: "500px"
                                         }}>
                                        <DefaultTextField
                                            name={"search"}
                                            label={'متن ویژگی را وارد کنید'}
                                            multiline={true}
                                            rowsMax={4}
                                            variant={'outlined'}
                                            style={{margin: "10px"}}
                                            defaultValue={it.values[0].name[siteLang]}
                                            onChange={(text) => {
                                                setTextFeatureValue(text)
                                            }}
                                        />
                                        <BaseButton
                                            variant={'outlined'}
                                            loading={loading}
                                            style={{
                                                borderColor: cyan[300],
                                                margin: "12px"
                                            }}
                                            onClick={() => {
                                                // handleCheck(feature.values[0], feature)
                                                
                                                if (textFeatureValue !== "" && textFeatureValue !== it.values[0].name[siteLang]) {
                                                    setLoading(true)
                                                    ControllerProduct.Features.addFeatureValue({
                                                        fId: it.feature.id,
                                                        value: textFeatureValue
                                                    }).then((response) => {
                                                        handleSelection(it.feature, [response.data.data], "text")
                                                        setCollapse(!collapse)
                                                    }).finally(() => {
                                                        setLoading(false)
                                                    })
                                                }
                                            }}>
                                            ذخیره
                                        </BaseButton>
                                    </Box>
                                
                                
                                </Box>
                            </Collapse>
                        </Box>
                    
                    ))}
                
                </Box>
                <Box display={"flex"} flexDirection={"flex-end"}>
                    
                    {
                        <IconButton style={{display: it.feature.type.key === "text" ? "block" : "none"}} onClick={() => {
                            setCollapse(!collapse)
                        }} color="primary" aria-label="upload picture"
                                    component="span">
                            <Edit/>
                        </IconButton>
                    }
                    
                    <IconButton onClick={() => {
                        onRemoveFeature(it)
                    }
                    } color="primary" aria-label="upload picture"
                                component="span">
                        <Delete/>
                    </IconButton>
                
                </Box>
            </Box>
        )
        
    }
    
}

export default Features;