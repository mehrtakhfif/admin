import React, {useRef, useState, Fragment, useEffect} from 'react';
import {gcLog} from '../../utils/ObjectUtils';
import {activeLang, lang, siteLang, webRout} from '../../repository';
import {makeStyles} from '@material-ui/core/styles';
import {
    Box,
    Typography,
    Button,
    TextField,
    InputBase,
    Fab,
    Slide,
    CircularProgress,
    Radio,
    FormLabel,
    RadioGroup,
    FormControlLabel,
    DialogContent,
    DialogActions,
    DialogContentText,
    DialogTitle
} from '@material-ui/core';
import MaterialFormControl from "@material-ui/core/FormControl";
import {useSnackbar} from 'notistack';
import {
    Add,
    AddBoxOutlined,
    AddToPhotosOutlined,
    CancelOutlined,
    CheckCircleOutlineOutlined,
    Close,
    DateRange,
    Delete,
    DeleteOutline,
    DeleteOutlineOutlined,
    Edit,
    ExpandLess,
    ExpandMore, HighlightOff,
    InfoOutlined,
    Map,
    Menu,
    MenuOpen,
    Phone,
    PhoneEnabled,
    PhoneIphone,
    Save,
    SortOutlined,
    Storage,
    Sync,
    VisibilityOffOutlined,
    VisibilityOutlined,
    ShoppingCart,
    RemoveShoppingCart
} from "@material-ui/icons";
import {generatePermalink} from '../base/textField/PermalinkTextField';
import Editor from '../base/editor/Editor';
import CategorySingle, {getActiveParents} from '../../pages/categories/CategorySingle';
import {UtilsStyle} from '../../utils/Utils';
import ComponentError from '../base/ComponentError';
import Collapse from "@material-ui/core/Collapse";
import useSWR, {mutate} from "swr";
import Tooltip from "@material-ui/core/Tooltip";
import ButtonBase from "@material-ui/core/ButtonBase";
import Dialog from "@material-ui/core/Dialog";
import Skeleton from "@material-ui/lab/Skeleton";
import Divider from "@material-ui/core/Divider";
import Checkbox from "@material-ui/core/Checkbox";
import useTheme from "@material-ui/core/styles/useTheme";
import ControllerProduct from "../../controller/ControllerProduct";
import _ from "lodash";
import {gcError, getSafe, tryIt} from "../../utils/ObjectUtils";
import {blue, cyan, green, grey, orange, red} from "@material-ui/core/colors";
import {DEBUG, sLang, theme} from "../../repository";
import {Card} from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import FormContainer from '../base/FormContainer';
import clsx from 'clsx';
import BaseLink from '../base/link/Link';
import rout from '../../router';
import NoneTextField from '../base/textField/NoneTextField';
import {createName} from '../base/textField/TextFieldMultiLanguageContainer';
import MultiTextField from '../base/textField/MultiTextField';
import {convertTableFilter} from "../../controller/converter";
/*const theme = createMuiTheme({
  direction: 'rtl',
  overrides: {
    MuiInput: {
      underline: {
        "&:hover:not($disabled):not($error):not($focused):before": {
          borderBottom: "1px solid rgba(0, 0, 0, 0.42)"
        },
        "&$focused:after": {
          borderBottom: "1px solid rgba(0, 0, 0, 0.20)"
        }
      }
    },
    MuiTypography: {
      h6:
          {
            margin: '8px',
          },

    },
  },
});*/

const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            margin: theme.spacing(1),
        },
    },
    productTitle: {padding: '8px', margin: '8px', width: '-webkit-fill-available'},
    productTitleWarning: {
        padding: '8px',
        margin: '8px',
        width: '-webkit-fill-available',
        'background': '-webkit-linear-gradient(top right, #fcf26d5e, #fafafa)'
    },
    inputText: {padding: '8px', margin: '8px', width: '-webkit-fill-available'},
    description: {},
    typography: {margin: '8px'},
    permalinkButton: {width: '10%'},
    buttonActive: {
        display: 'inline-block',
        backgroundColor: green[600],
        color: '#fff',
        fontSize: '15px',
        '&:hover': {
            backgroundColor: green[700],
        },
    },
    buttonDeActive: {
        display: 'inline-block',
        backgroundColor: red[600],
        color: '#fff',
        fontSize: '15px',
        '&:hover': {
            backgroundColor: red[700],
        },
    },
    wrapper: {
        margin: theme.spacing(1),
        position: 'relative',

    },
    fabProgress: {
        color: '#fff',
        position: 'absolute',
        margin: 'auto',
        top: 0, left: 0, bottom: 0, right: 0,
    },
    categoryContainer:
        {
            margin: '8px',
            border: '#e2e2e2 solid',
            borderRadius: '8px',
            padding: '8px',
        },


    shortDesWarningStyle: {
        padding: '8px', margin: '8px', width: '-webkit-fill-available',
        'background': '-webkit-linear-gradient(top right, #fcf26d5e, #fafafa)',
        '--box-shadow': 'rgba(250,239,24,0.3)',
    },
    shortDesErrorStyle: {
        padding: '8px', margin: '8px', width: '-webkit-fill-available',
        'background': '-webkit-linear-gradient(top right, #fc6d6d8a, #ee505000)',
        '--box-shadow': 'rgba(243,33,33,0.3)',

    }

}));


function Name({...props}) {
    const classes = useStyles();
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();
    const [data, setData] = useState(props.data);
    const [productCategory, setProductCategory] = useState(props.data.categories ?? []);
    const [activateProductState, setActivateProductState] = useState({loading: false, active: !data.disable});
    const ref = useRef();
    const nameRef = useRef();
    const seoTitleRef = useRef();
    const permalinkRef = useRef(data.permalink);
    const [permalink, setPermalink] = useState(undefined);
    const [permalinkWarning, setPermalinkWarning] = useState(data.permalink.length > 70 ? 2 : data.permalink.length > 50 ? 1 : 0);
    const [shortDescriptionWarning, setShortDescriptionWarning] = useState(data.short_description.length > 160 ? 2 : data.short_description.length < 50 ? 1 : 0);
    const [titleWarning, setTitleWarning] = useState(data?.name[siteLang]?.length > 63 ? 1 : 0);
    const shortDescriptionRef = useRef(data.short_description);
    const [nameState, setNameState] = useState(data.name);
    const [openChangePermalinkDialog, setOpenChangePermalinkDialog] = useState(false);
    const FullScreenTransition = React.forwardRef(function Transition(props, ref) {
        return <Slide direction="up" ref={ref} {...props} />;
    });
    const buttonClassname = clsx({
        [classes.buttonActive]: activateProductState.active,
        [classes.buttonDeActive]: !activateProductState.active,
    });

    const shortDescriptionStyle = clsx({
        [classes.shortDesErrorStyle]: shortDescriptionWarning === 2,
        [classes.shortDesWarningStyle]: shortDescriptionWarning === 1,
        [classes.inputText]: shortDescriptionWarning === 0,

    })
    const titleStyle = clsx({
        [classes.productTitleWarning]: titleWarning === 1,
        [classes.productTitle]: titleWarning === 0,

    })

    const id = props.id;

    function updateProduct(inputData) {
        return props.updateProduct(id, inputData);
    }

    function handleNameChange(e) {
        const inputData = nameRef.current.value;

        if (inputData.length > 63) {
            setTitleWarning(1)
        } else {
            setTitleWarning(0)
        }
        gcLog('handleNameChange', {
            data, inputData, d: {
                a: data.name,
                inputData,
                f: !(data.name[siteLang] === inputData || inputData === ''),
            },
        });
        if (!(data.name[siteLang] === inputData || inputData === '')) {
            updateProduct({'name': {[siteLang]: inputData}}).then((res) => {
                setData({...data, name: {...data.name, [siteLang]: inputData}});
                // autoGeneratePermalink(null);

            });
        }
    }

    function handleSeoTitleChange(e) {
        const inputData = seoTitleRef.current.value.trim();
        // if (inputData.length > 63) {
        //     setTitleWarning(1)
        // } else {
        //     setTitleWarning(0)
        // }
        // gcLog('handleNameChange', {
        //     data, inputData, d: {
        //         a: data.name,
        //         inputData,
        //         f: !(data.name[siteLang] === inputData || inputData === ''),
        //     },
        // });
        if (data.seo_title !== inputData) {
            updateProduct({name:{...data.name,seo_title:inputData}}).then((res) => {
                setData({...data, seo_title: inputData});
            });
        }
    }

    function handlePermalinkChange(e) {
        const inputData = permalinkRef.current.value;
        gcLog('handlePermalinkChange', {
            a: data.permalink,
            inputData,
            f: !(data.permalink === inputData || inputData === ''),
        });
        if (!(data.permalink === inputData || inputData === '')) {

            updatePermalink(inputData).then((res) => {
                setData({
                    ...data,
                    permalink: {...data.permalink, [siteLang]: inputData},
                });
            });
        }
    }

    function handleShortDescriptionChange() {
        const inputData = shortDescriptionRef.current.value;
        if (inputData.length > 160) {
            setShortDescriptionWarning(2)
            enqueueSnackbar("توضیحات کوتاه بیشتر از 160 کاراکتر است",
                {
                    variant: "warning",
                    action: (key) => (
                        <Fragment>
                            <Button onClick={() => {
                                closeSnackbar(key)
                            }}>
                                {lang.get('close')}
                            </Button>
                        </Fragment>
                    )
                });
            return
        }
        if (inputData.length < 50) {
            setShortDescriptionWarning(1)
            enqueueSnackbar("توضیحات کوتاه کمتر از 50 کاراکتر است",
                {
                    variant: "warning",
                    action: (key) => (
                        <Fragment>
                            <Button onClick={() => {
                                closeSnackbar(key)
                            }}>
                                {lang.get('close')}
                            </Button>
                        </Fragment>
                    )
                });
        }
        setShortDescriptionWarning(0)

        if (!(data.short_description[siteLang] === inputData)) {
            updateProduct({'short_description': {[siteLang]: inputData}}).then((res) => {
                gcLog("short_description", inputData)
                setData((s) => ({
                    ...s,
                    short_description: {
                        ...s.short_description,
                        [siteLang]: inputData,
                    },
                }));
            });
        }

    }

    function handleDescriptionChange(content) {
        gcLog('handleDescriptionChange',
            {data: data.description[siteLang], content});
        if (!(data.description[siteLang] === content || content === '')) {
            updateProduct({'description': {[siteLang]: content}}).then((res) => {
                setData((s) => ({
                    ...s,
                    description: {...s.description, [siteLang]: content},
                }));
            });

        }

    }

    function autoGeneratePermalink(e) {
        try {
            const permalink = generatePermalink(nameRef.current.value);
            updatePermalink(permalink)
            permalinkRef.current.value = permalink;

        } catch (e) {
        }
    }

    function updatePermalink(permalink) {
        setPermalink(permalink)
        if (activateProductState.active){
            setOpenChangePermalinkDialog(s=>!s)
            return
        }
     acceptChangePermalinkDialog()


    }

    function ChangePermalinkDialog() {
        return <Dialog
            open={openChangePermalinkDialog}
            onClose={closeChangePermalinkDialog}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
            <DialogTitle id="alert-dialog-title">
                {"آیا مایل به تغییر پرمالینک هستید"}
            </DialogTitle>
            <DialogContent>
                <DialogContentText id="alert-dialog-description">
                    تغییر پرمالینک محصول موجب از بین رفتن لینک قدیمی محصول و دسترسی گوگل و باقی صفحات سایت به ان محصول می‌شود. در صورت امکان از تغییر پرمالینک اجتناب کنید.
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={closeChangePermalinkDialog}>منصرفم</Button>
                <Button onClick={acceptChangePermalinkDialog} autoFocus>
                    مطمئنم
                </Button>
            </DialogActions>
        </Dialog>
    }

    function closeChangePermalinkDialog(){
        setOpenChangePermalinkDialog(false)
    }

    function acceptChangePermalinkDialog() {
        if (permalink.length > 70) {
            setPermalinkWarning(2)
        } else if (permalink.length > 55) {
            setPermalinkWarning(1)
        } else {
            setPermalinkWarning(0)
        }
        closeChangePermalinkDialog()
        return updateProduct({'permalink': permalink});

    }

    function handleActiveProduct() {
        setActivateProductState(s => ({...s, loading: true}));
        /*disable :true , activate:false      server : activate   disable:false */
        updateProduct({'disable': activateProductState.active}).then((res) => {
            gcLog('ressssss', res);
            if (res.status === 200 || res.status === 202) {
                setActivateProductState(s => ({...s, active: !s.active}));
            }/*
            if (res.status===250){
                setActivateProductState(s => ({...s, active: s.active}));
            }*/
        }).finally(() => {
            setActivateProductState(s => ({...s, loading: false}));
        });
    }

    function handleCategoryChange(category) {
        // const arr = [...productCategory]
        setProductCategory(arr => (arr.some(o => category.id === o.id) ?
            arr.filter(o => category.id !== o.id) : [...category]))
        let reqData = []
        category.forEach(item => reqData = reqData.concat([item.id]))
        updateProduct({'categories': reqData})

    }

    function onUnavailableClick() {
        const available = !data.available
        setData({...data,available:available})
        updateProduct({'available':available})
    }


    return (
        <Fragment>
            <ChangePermalinkDialog />
            <Box display={'flex'} alignItems={'center'}>
                <Tooltip title={"موجودیت محصول"}>
                <Button  onClick={onUnavailableClick}>
                    <Typography alignItems={"center"} variant={"body1"}>
                        {data.available? <ShoppingCart
                            fontSize={"small"}
                            style={{color: "#539e37"}}
                        />: <RemoveShoppingCart
                            fontSize={"small"}
                            style={{color: "#b34c4c"}}
                        />}

                    </Typography>
                </Button>
                </Tooltip>
                <BaseLink toHref={rout.Storage.Single.showStorageDashboard({productId: id})}>
                    <Typography alignItems={"center"} variant={"body1"}>
                        <Storage
                            fontSize={"small"}
                        />
                    </Typography>
                </BaseLink>
                <Typography variant={'h2'}
                            className={classes.productTitle}>{data.name[siteLang]}</Typography>

                <Typography variant={'h6'} p={8}>وضعیت محصول:</Typography>
                <div className={classes.wrapper}>
                    <Button
                        className={buttonClassname}
                        onClick={handleActiveProduct}
                    >
                        {!activateProductState.loading &&
                        (activateProductState.active ? 'فعال' : 'غیرفعال')}
                    </Button>
                    {activateProductState.loading && <CircularProgress size={34}
                                                                       className={classes.fabProgress}/>}
                </div>
            </Box>
            <Box display={'flex'} flexDirection={'row'}>
                <Box width={0.7}>
                    <Box width={1} className={"enNum"}>
                        <TextField
                            className={titleStyle}
                            name={'name'}
                            variant={'outlined'}
                            label={'نام محصول'}
                            fullWidth={true}
                            inputRef={nameRef}
                            defaultValue={data.name[siteLang]}
                            onFocusOut={handleNameChange}
                            onBlur={handleNameChange}/>
                    </Box>
                    <Box ml={2} mb={2} mr={2} p={1} display={'flex'}
                         alignItems={'center'}
                         bgcolor={permalinkWarning === 0 ? 'rgba(231,231,231,0.27)' : permalinkWarning === 1 ? 'rgba(255,221,103,0.55)' : '#FF8B8B94'}>
                        <Typography variant={'h6'}
                                    fontWeight={'bold'}>پرمالینک: </Typography>
                        <Box className={"enNum"} width={1}>

                        <InputBase
                            inputProps={{'aria-label': 'naked'}}
                            className={classes.inputText}
                            name={'permalink'}
                            variant={'filled'}
                            fullWidth={true}
                            label={'پرمالینک'}
                            inputRef={permalinkRef}
                            defaultValue={data.permalink}
                            onBlur={handlePermalinkChange}
                        />
                        </Box>
                        <Button className={classes.permalinkButton}
                                size={'small'}
                                onClick={autoGeneratePermalink}>ساخت
                            پرمالینک</Button>
                    </Box>
                    <TextField
                        className={titleStyle}
                        name={'name'}
                        variant={'outlined'}
                        label={'سئو تایتل'}
                        fullWidth={true}
                        inputRef={seoTitleRef}
                        defaultValue={data?.name?.seo_title}
                        onFocusOut={handleSeoTitleChange}
                        onBlur={handleSeoTitleChange}/>
                    <Box display={'column'} mt={1}>
                        <Button onClick={()=>{shortDescriptionRef.current.value="خرید اینترنتی ....... از فروشگاه آنلاین مهرتخفیف"}}> خرید اینترنتی ....... از فروشگاه آنلاین مهرتخفیف</Button>
                    </Box>
                    <TextField
                        fullWidth={true}
                        name={'shortDescription'}
                        label={'توضیحات کوتاه'}
                        variant={'outlined'}
                        inputRef={shortDescriptionRef}
                        defaultValue={data.short_description[siteLang]}
                        className={shortDescriptionStyle}
                        multiline={true}
                        rowsMax={5}
                        rows={5}
                        onBlur={handleShortDescriptionChange}
                    />
                    {/*<Box display={'flex'} alignItems={'center'} justifyContent={'space-between'} p={1} ml={2} mb={1} flexDirection={'row'}
                         bgcolor={shortDescriptionWarning === 0 ? 'rgba(231,231,231,0.27)' : shortDescriptionWarning === 1 ? 'rgba(255,221,103,0.55)' : '#FF8B8B94'}>
                        <Button>
                            خرید اینترنتی ----- از فروشگاه آنلاین مهرتخفیف
                        </Button>
                        <Typography>
                            {shortDescriptionRef?.current?.value[siteLang]?.length}
                        </Typography>
                    </Box>*/}
                </Box>
                <Box width={0.3} className={classes.categoryContainer}>
                    <SelectCategory item={productCategory} box={data.category} onChange={handleCategoryChange}/>
                </Box>
            </Box>
            <Box className={classes.categoryContainer}
            >
                <ProductSetting data={data}/>
            </Box>
        </Fragment>
    );


    function updateAdminNote(event) {
        updateProduct({
            'settings': {
                ...data, 'adminNote': event.target.value,
            }
        })

    }


    function ProductSetting({data: da}) {
        const {settings: data} = da
        const [productStatus, setProductStatus] = useState(_.toString(data.productStatus || 0));

        function handleProductStatusUpdate(event) {
            setProductStatus(event.target.value)
            updateProduct({
                'settings': {
                    ...data, 'productStatus': _.toInteger(event.target.value),
                }
            })

        }


        return (
            <Box my={2}>
                <Box display={'flex'} flexDirection={'column'}>
                    <Box p={1}>
                        <NoneTextField
                            name={createName({group: 'settings', name: "productStatus"})}
                            defaultValue={productStatus}/>
                        <MaterialFormControl component="fieldset">
                            <FormLabel component="legend">
                                <Typography variant={"h5"}>
                                    وضعیت محصول
                                </Typography>
                                <Typography variant={'body'}>
                                    این بخش جهت داشتن آمار وضعیت محصول میباشد و به کاربران نشان داده نخواهد شد.
                                    <br/>
                                    درصورتی که محصول به یک حد قابل قبول از اطلاعات رسیده است وضعیت مطلوب را انتخاب کنید.
                                    <br/>
                                    درصورتی که محصول نیاز به هیچ تغییری ندارد و به کامل ترین حالت اطلاعات خود رسیده است
                                    وضعیت کامل را انتخاب کنید.
                                </Typography>
                            </FormLabel>
                            <RadioGroup row aria-label="productStatus" value={productStatus}
                                        onChange={handleProductStatusUpdate}>
                                <FormControlLabel
                                    value="0"
                                    control={<Radio color="primary"/>}
                                    label="ناقص"
                                    labelPlacement="top"/>
                                <FormControlLabel
                                    value="1"
                                    control={<Radio color="primary"/>}
                                    label="مطلوب"
                                    labelPlacement="top"/>
                                <FormControlLabel
                                    value="2"
                                    control={<Radio color="primary"/>}
                                    label="کامل"
                                    labelPlacement="top"/>
                            </RadioGroup>
                        </MaterialFormControl>
                    </Box>
                    <Box p={2}>
                        <TextField
                            label={"یادداشت"}
                            multiline={true}
                            rowsMax={4}
                            fullWidth={true}
                            variant={'outlined'}
                            placeholder={"یک متن برای یاد داشت خودتان بنویسد. (این قسمت به کاربر نشان داده نخواهد شد)"}
                            group={"settings"}
                            name={"adminNote"}
                            defaultValue={data.adminNote || ""}
                            onBlur={updateAdminNote}
                            rows={4}
                        />
                    </Box>
                </Box>
            </Box>
        )
    }


    function SelectCategory({item: da, box, onChange, ...props}) {

        const item = da
        const d = ControllerProduct.Categories.getAllCategories({});
        const apiKey = d[0] + box.id;
        const {data, error} = useSWR(apiKey, () => {
            return d[1]({
                boxId: box.id,
            }).then(res => {
                return res.categories;
            })
        });



        const [state, setState] = useState({
            addDialog: false,
        });

        function getData() {
            mutate(apiKey);
        }

        function handleCategoryChange(cat) {
            const newList = item ? item : [];
            const remove = _.remove(newList, (c) => {
                return c.id === cat.id
            });

            if (_.isEmpty(remove)) {
                newList.push(cat);
            }
            onChange(newList);
            mutate()
        }


        const itemParentsIdList = getParents();

        function getParents() {
            const list = [];
            _.forEach(item, (i) => {
                list.push(i.id)
            });
            return getActiveParents(list, data);
        }

        return (
            !error ?
                data ?
                    <Box>
                        <Box display={'flex'} flexDirection={'column'} width={1}>
                            <Box display={'flex'} justifyContent={'center'} alignItems={'center'}>
                                <Tooltip title={'افزودن'}>
                                    <IconButton onClick={() => setState({
                                        ...state,
                                        addDialog: true
                                    })}
                                                style={{
                                                    marginRight: theme.spacing(0.5)
                                                }}>
                                        <Add/>
                                    </IconButton>
                                </Tooltip>
                                <Typography variant={'h6'} py={2}
                                            style={{
                                                flex: 1,
                                                ...UtilsStyle.disableTextSelection()
                                            }}>
                                    انتخاب دسته
                                </Typography>
                                <Tooltip title={'بروزرسانی'}>
                                    <IconButton onClick={getData}
                                                style={{
                                                    marginLeft: theme.spacing(0.5)
                                                }}>
                                        <Sync/>
                                    </IconButton>
                                </Tooltip>
                            </Box>
                            <Box display={'flex'} flexDirection={'column'} px={2}
                                 style={{
                                     height: 300,
                                     overflowY: 'auto'
                                 }}>{!_.isEmpty(data) &&
                            <CategoryContainer
                                activeItems={item}
                                items={data}
                                activeParents={itemParentsIdList || []}
                                onCategoryChange={handleCategoryChange}/>}
                            </Box>
                            <Dialog fullScreen open={state.addDialog} TransitionComponent={FullScreenTransition}>
                                <CategorySingle
                                    box={box}
                                    onClose={(cat) => {
                                        if (cat) {
                                            handleCategoryChange(cat);
                                            getData();
                                        }
                                        setState({
                                            ...state,
                                            addDialog: false
                                        })
                                    }}/>
                            </Dialog>
                        </Box>
                    </Box> :
                    <Box width={1} py={0.5}>
                        <Skeleton height={90} variant={"rect"}/>
                    </Box> :
                <ComponentError
                    tryAgainFun={getData}/>
        )
    }


    function CategoryContainer({activeItems, items, level = 0, activeParents = [], onCategoryChange, ...props}) {

        const theme = useTheme();
        const darkMode = theme.palette.type === "dark";


        if (!items)
            return <React.Fragment/>



        return (
            <Box width={1} px={1} py={1}
                 boxShadow={1}
                 style={{
                     backgroundColor: level !== 0 ? grey[darkMode ? (800 - (level * 100)) : level * 100] : null,
                     ...UtilsStyle.borderRadius('0 0 5px 5px')
                 }}>
                {
                    items.map((cat, i) => {

                        return(
                            <CategoryItem key={cat.id} index={i} activeItems={activeItems}
                                          activeParents={activeParents}
                                          item={cat} level={level}
                                          onCategoryChange={onCategoryChange}/>
                        )
                    })
                }
            </Box>
        )
    }


    function CategoryItem({activeItems, index, item, level = 0, activeParents, onCategoryChange, ...props}) {

        const [state, setState] = useState({
            open: false,
        });

        const isActive = _.findIndex(activeItems, (i) => i.id === item.id) !== -1;
        const isParent = !isActive && _.findIndex(activeParents, (p) => p.id === item.id) !== -1;

        return (
            <React.Fragment>
                {index !== 0 &&
                <Divider style={{
                    opacity: 0.5
                }}/>}
                <Box width={1} display={'flex'} alignItems={'center'}>
                    <ButtonBase
                        onClick={() => onCategoryChange(item)}
                        style={{
                            cursor: 'pointer',
                            display: 'flex',
                            flexWrap: 'wrap',
                            justifyContent: 'flex-start',
                            flex: 1,
                            ...UtilsStyle.disableTextSelection(),
                        }}>
                        <Box key={item.id} py={1} display={'flex'} flex={'wrap'} alignItems={'center'}>
                            <Checkbox
                                checked={isActive}
                                value={item.id}
                                indeterminate={isParent}
                                name={`category-${item.id}`}
                                inputProps={{'aria-label': `category-${item.id}`}}
                                style={{
                                    color: isActive ? red[400] : isParent ? red[200] : null
                                }}
                            />
                            <Typography variant={'body1'}>
                                {item.name}
                            </Typography>
                        </Box>
                    </ButtonBase>
                    {!_.isEmpty(item.child) &&
                    <Box mx={0.5}>
                        <Tooltip title={"نمایش فرزندان"}>
                            <IconButton onClick={() => setState({
                                ...state,
                                open: !state.open
                            })}>
                                {state.open ? <ExpandLess/> : <ExpandMore/>}
                            </IconButton>
                        </Tooltip>
                    </Box>}
                </Box>
                {!_.isEmpty(item.child) &&
                <Collapse in={state.open}>
                    <CategoryContainer activeItems={activeItems} items={item.child}
                                       activeParents={activeParents}
                                       onCategoryChange={onCategoryChange}
                                       level={level + 1}/>
                </Collapse>
                }
            </React.Fragment>
        )
    }


}

export default Name;