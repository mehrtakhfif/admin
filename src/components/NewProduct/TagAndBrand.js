import React, {Fragment, useEffect, useRef, useState} from 'react';
import {
    Box, Button, Card, Collapse, Chip, IconButton, Tooltip,
    ButtonBase,
    Divider,
    Radio,
} from '@material-ui/core';
import {makeStyles, useTheme} from '@material-ui/core/styles';
import {blue, cyan, green, grey, orange, red} from '@material-ui/core/colors';
import {
    Add,
    CancelOutlined,
    DeleteOutlineOutlined,
    VisibilityOffOutlined,
    VisibilityOutlined,
    Delete
} from '@material-ui/icons';
import useSWR, {mutate} from 'swr';
import _ from 'lodash';
import {useSnackbar} from 'notistack';
import ControllerProduct from '../../controller/ControllerProduct';
import {maxShowableTagCount} from '../../pages/box/themplate/tag/CreateTag';
import {activeLang, lang} from '../../repository';
import {gcError, gcLog} from '../../utils/ObjectUtils';
import {createMultiLanguage} from '../../controller/converter';
import Typography from '../base/Typography';
import SelectTag, {TagItem} from '../../pages/box/themplate/tag/SelectTag';
import TextFieldContainer, {textFieldNewValue} from '../base/textField/TextFieldContainer';
import {createName} from '../base/textField/TextFieldMultiLanguageContainer';
import TextField from '../base/textField/TextField';
import BaseButton from '../base/button/BaseButton';
import PleaseWait from '../base/loading/PleaseWait';
import ComponentError from '../base/ComponentError';
import TagSingle from '../../pages/tags/TagSingle';
import FormContainer from '../base/FormContainer';
import {UtilsStyle} from '../../utils/Utils';
import {EditBrand} from '../../pages/brands/Brands';
import Skeleton from '@material-ui/lab/Skeleton';

const useTagsStyle = makeStyles(theme => ({
    root: {
        backgroundColor: cyan[100],
        color: grey[700],
        '&:focus': {
            backgroundColor: cyan[100],
            color: grey[700],
        },

    },
}));

const useStyles = makeStyles((theme) => ({

        root: {
            '& > *': {
                margin: theme.spacing(1),
            },

        },
        brandContainer:
            {
                margin: '8px',
                border: '#e2e2e2 solid',
                borderRadius: '8px',
                padding: '8px',
                height: 'fit-content'
            },
    }),
);

function TagAndBrand({...props}) {
    const classes = useStyles();
    const [data, setData] = useState(props.data);
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();
    gcLog('NewProduct features', {data});


    function updateProduct(inputData) {
        return props.updateProduct(props.id, inputData);
    }


    function handleTagChange(newData) {
        gcLog("handleTagChange", newData)
        const newTags = [];
        try {
            _.forEach(newData, (t) => {
                newTags.push({
                    "tag_id": t.id,
                    "show": t.show
                },)
            });
        } catch (e) {
            gcError("getSaveProps::newTags", e)
        }
        updateProduct({'tags': newTags}).then(res => {

            setData({
                ...data,
                tags: newData,
            });
        })
    }

    function handleGroupTagChange(newData) {
        gcLog("handleGroupTagChange", newData)

        const newTagGroups = [];
        const backup = _.cloneDeep(data)
        try {
            _.forEach(newData, t => {
                newTagGroups.push(t.id);
            })
        } catch (e) {
            gcError("getSaveProps::GroupTags", e)
        }

        setData({
            ...data,
            tag_groups: [...newData],
        });
        updateProduct({'tag_groups': newTagGroups}).catch(e => {
            gcError("getSaveProps::GroupTags", e)
            setData(backup);
        })
    }

    function handleBrandChange(br) {
        updateProduct({'brand_id': br.id}).then(res => {
            setData({
                ...data,
                brand: br,
            });
        })
    }


    const catId = props?.data?.category?.id

    return (
        <Box m={2} display={'flex'} flexDirection={'row'}>
            <Box width={0.7}>
                <Tags data={data}
                      onChange={handleTagChange}
                      onTagGpAdd={handleGroupTagChange}/>
            </Box>
            <Box width={0.3} className={classes.brandContainer}>
                <SelectBrand
                    data={data}
                    onChange={handleBrandChange}/>
            </Box>
        </Box>

    );


    function SelectBrand({active, data: dd, onChange, ...props}) {
        const {brand: item, category} = dd;
        const theme = useTheme();

        const [state, setState] = useState({
            addDialog: false,
        });
        const [defValue, setDefValue] = useState('');
        const [value, setValue] = useState('');
        const [inputValue, setInputValue] = useState('');
        const d = ControllerProduct.Brands.get();
        const apiKey = d[0] + category.id + '-' + value;
        const {data, error} = useSWR(apiKey, () => {
            return d[1]({
                boxId: category.id,
                q: value,
                step: 100,
                all: true,
            });
        });

        function handleBrandChange(brand) {
            onChange(brand);
        }

        return (
            <Box
                my={0.5}
                openDef={!!item}
                active={active}
                title={lang.get('brand')}>
                {data ?
                    <React.Fragment>
                        <Box display={'flex'} flexDirection={'column'}
                             width={1}>
                            <Box display={'flex'} justifyContent={'center'}
                                 alignItems={'center'} flexWrap={'wrap'}>
                                <Tooltip title={'افزودن'}>
                                    <IconButton
                                        onClick={() => setState({
                                            ...state,
                                            addDialog: true,
                                        })}
                                        style={{
                                            marginRight: theme.spacing(0.5),
                                        }}>
                                        <Add/>
                                    </IconButton>
                                </Tooltip>
                                <Typography variant={'h6'} py={2}
                                            display={'flex'}
                                            justifyContent={'center'}
                                            alignItems={'center'}
                                            style={{
                                                flex: 1,
                                                ...UtilsStyle.disableTextSelection(),
                                            }}>
                                    <Tooltip title={'حذف'}>
                                        <IconButton
                                            onClick={() => handleBrandChange(
                                                null)}
                                            disabled={!item}
                                            style={{
                                                color: red[400],
                                                marginLeft: theme.spacing(0.5),
                                            }}>
                                            <DeleteOutlineOutlined/>
                                        </IconButton>
                                    </Tooltip>
                                    برند: {(item && item.name) ?
                                    item.name[activeLang] :
                                    '------'}
                                </Typography>
                                <Box width={1} px={2} pb={2}>
                                    <TextFieldContainer
                                        name={createName({name: 'tag-name'})}
                                        defaultValue={defValue}
                                        onChange={(val) => {
                                            setInputValue(val);
                                        }}
                                        render={(
                                            ref,
                                            {
                                                name: inputName,
                                                initialize,
                                                valid,
                                                errorIndex,
                                                inputProps,
                                                style,
                                                props
                                            }) => {
                                            return (
                                                <TextField
                                                    {...props}
                                                    variant={'outlined'}
                                                    error={!valid}
                                                    name={inputName}
                                                    inputRef={ref}
                                                    fullWidth
                                                    onKeyPress={(e) => {
                                                        try {
                                                            if (e.key ===
                                                                'Enter') {
                                                                const val = ref.current.getAttribute(
                                                                    textFieldNewValue);
                                                                setValue(val);
                                                                setDefValue(
                                                                    val);
                                                            }
                                                        } catch (e) {
                                                        }
                                                    }}
                                                    endAdornment={(inputValue) &&
                                                    (
                                                        <IconButton
                                                            onClick={() => {
                                                                try {
                                                                    setDefValue(
                                                                        '');
                                                                    setValue(
                                                                        '');
                                                                    setInputValue(
                                                                        '');
                                                                } catch (e) {
                                                                }
                                                            }}>
                                                            <CancelOutlined/>
                                                        </IconButton>
                                                    )}
                                                    label={'جست‌و‌جو برند'}
                                                    placeholder={'بعد تایپ نام برند enter بزنید'}
                                                    style={{
                                                        ...style,
                                                        minWidth: '90%',
                                                    }}
                                                    inputProps={{
                                                        ...inputProps,
                                                    }}/>
                                            );
                                        }}/>
                                </Box>
                            </Box>
                            <Box display={'flex'} flexDirection={'column'}
                                 mx={2}
                                 style={{
                                     height: 300,
                                     overflowY: 'auto',
                                 }}>
                                {(data.data) && data.data.map((b, i) => (
                                    <React.Fragment key={b.id}>
                                        <Divider style={{
                                            opacity: 0.5,
                                        }}/>
                                        <ButtonBase
                                            onClick={() => handleBrandChange(b)}
                                            style={{
                                                cursor: 'pointer',
                                                display: 'flex',
                                                justifyContent: 'flex-start',
                                                ...UtilsStyle.disableTextSelection(),
                                            }}>
                                            <Box key={b.id} py={1}
                                                 display={'flex'} flex={'wrap'}
                                                 alignItems={'center'}>
                                                <Radio
                                                    checked={item && b.id ===
                                                    item.id}
                                                    value="b"
                                                    name="radio-button-demo"
                                                    inputProps={{'aria-label': 'B'}}
                                                />
                                                <Typography variant={'body1'}>
                                                    {b.name[activeLang]}
                                                </Typography>
                                            </Box>
                                        </ButtonBase>
                                    </React.Fragment>
                                ))}
                            </Box>
                        </Box>
                        <EditBrand open={state.addDialog}
                                   onClose={(br) => {
                                       if (br) {
                                           mutate(apiKey);
                                           handleBrandChange(br);
                                       }
                                       setState({
                                           ...state,
                                           addDialog: false,
                                       });
                                   }}/>
                    </React.Fragment> :
                    active ?
                        <Box width={1} py={0.5}>
                            <Skeleton height={90} variant={'rect'}/>
                        </Box> :
                        <React.Fragment/>}
            </Box>
        );
    }

}

const Tags = React.memo(function Tags({data: dd, onChange, onTagGpAdd: onTagGpChange, ...props}) {
    const {tags, category, tag_groups} = dd;
    const theme = useTheme();

    const {enqueueSnackbar, closeSnackbar} = useSnackbar();
    const [setting, setSetting] = useState({
        openAddTagDialog: false,
        dialogValue: '',
        dialogPermalink: '',
        selectGpTag: false,
    });
    const [defValue, setDefValue] = useState('');
    const [inputValue, setInputValue] = useState('');
    const [value, setValue] = useState('');
    const classes = useTagsStyle(props);
    const [data, setData] = useState([])
    const d = ControllerProduct.Product.Tags.search({query: value});
    const apiKey = d[0] + value;
    const {data: da, error} = useSWR(apiKey, () => {
        return d[1]();
    }, {errorRetryCount: 1});
    // const {tags: data} = da ? da.data : [];
    useEffect(() => {
        setData(da ? da.data.tags : [])
    }, [da])
    useEffect(() => {
        setValue('');
        setDefValue('');
    }, []);

    function handleAdd(d, show) {
        try {
            const newTags = tags;
            const newList = [];
            if (show) {
                const i = _.findIndex(tags, (t) => t.id === d.id);
                if (i === -1)
                    return;
                newTags[i] = d;
            } else {
                if (!_.isArray(d))
                    d = [d];
                let notAdded = 0;
                let showTag = 0;
                _.forEach(tags, (t) => {
                    showTag = (t.show ? 1 : 0) + showTag;
                });

                _.forEach(d, (tag) => {
                    const i = _.findIndex(tags, (t) => {
                        return tag.id === t.id;
                    });
                    if (i !== -1) {
                        notAdded = notAdded + 1;
                        return;
                    }
                    if (tag.show) {
                        if (maxShowableTagCount <= showTag) {
                            tag.show = false;
                        }
                        showTag = showTag + 1;
                    }
                    newList.push(tag);
                });
                if (notAdded > 0) {
                    enqueueSnackbar(notAdded + ' انتخاب شده است.',
                        {
                            variant: 'warning',
                            action: (key) => (
                                <Fragment>
                                    <Button onClick={() => {
                                        closeSnackbar(key);
                                    }}>
                                        {lang.get('close')}
                                    </Button>
                                </Fragment>
                            ),
                        });
                }
                if (_.isEmpty(newList))
                    return;
            }

            onChange([...newTags, ...newList]);
        } catch (e) {
            gcError('ProductSingle::Tags::handleAdd', e);
        }
    }

    function handleDelete(d,all=false) {
        if (all){
            onChange([]);
            return
        }
        const newData = tags;
        _.remove(newData, (t) => {
            return t.id === d.id;
        });
        onChange([...newData]);
    }

    function handleAddNewTag() {
        if (!_.isEmpty(data)) {
            const i = _.findIndex(data, (t) => {
                return t.name.fa === value;
            });
            if (i !== -1) {
                enqueueSnackbar('این تگ موجود میباشد.',
                    {
                        variant: 'error',
                        action: (key) => (
                            <Fragment>
                                <Button onClick={() => {
                                    closeSnackbar(key);
                                }}>
                                    {lang.get('close')}
                                </Button>
                            </Fragment>
                        ),
                    });
                return;
            }
        }

        setSetting({
            ...setting,
            openAddTagDialog: true,
            dialogPermalink: '',
            dialogValue: value,
        });

        // enqueueSnackbar('فزوده شد.',
        //     {
        //         variant: "success",
        //         action: (key) => (
        //             <Fragment>
        //                 <Button onClick={() => {
        //                     closeSnackbar(key)
        //                 }}>
        //                     {lang.get('close')}
        //                 </Button>
        //             </Fragment>
        //         )
        //     });
    }

    function handleCloseDialog(save) {

        if (save) {
            if (!setting.dialogPermalink || !setting.dialogValue) {
                enqueueSnackbar('تمام فیلدها را پرکنید.',
                    {
                        variant: 'error',
                        action: (key) => (
                            <Fragment>
                                <Button onClick={() => {
                                    closeSnackbar(key);
                                }}>
                                    {lang.get('close')}
                                </Button>
                            </Fragment>
                        ),
                    });
                return;
            }
            ControllerProduct.Product.Tags.set({
                permalink: setting.dialogPermalink,
                name: createMultiLanguage({fa: setting.dialogValue}),
            }).then(res => {
                res.item.hasActive = true;
                handleAdd(res.item);
                enqueueSnackbar('افزوده شد.',
                    {
                        variant: 'success',
                        action: (key) => (
                            <Fragment>
                                <Button onClick={() => {
                                    closeSnackbar(key);
                                }}>
                                    {lang.get('close')}
                                </Button>
                            </Fragment>
                        ),
                    });
                handleCloseDialog(false);
                mutate(apiKey);
            });
            return;
        }

        setSetting({
            ...setting,
            openAddTagDialog: false,
            dialogPermalink: '',
        });
    }

    function handleRemoveTag(tg) {
        const newList = tag_groups.filter(i=>(i.id != tg.id));
        onTagGpChange(newList);
    }


    const catId = category?.id


    return (
        <React.Fragment>
            <Box>
                <Box display={'flex'} width={1} px={2} py={1}
                     flexDirection={'column'}>
                    {!error ?
                        <Box display={'flex'} justifyContent={'center'}
                             width={1}>
                            <Box width={1} mt={2} mb={2}>

                                <Box display={'flex'}
                                     flexDirection={'column'} mb={3}>
                                    <Box display={'flex'} flexDriection={'row'} alignItems={'center'}>
                                    <Typography variant={'body1'} pb={1}
                                                fontWeight={400}>
                                        تگ‌های فعال:
                                    </Typography>
                                        <IconButton onClick={()=>handleDelete([],true)}>
                                            <Delete/>
                                        </IconButton>
                                    </Box>
                                    <Box display={'flex'} flexWrap={'wrap'}>
                                        {
                                            tags.map((t, i) => (
                                                <Box key={i} ml={2}
                                                     mb={1.5}>
                                                    <Chip
                                                        clickable
                                                        onClick={() => {
                                                            if (!t.show) {
                                                                let showCount = 0;
                                                                _.forEach(
                                                                    tags,
                                                                    t => {
                                                                        showCount = (t.show ?
                                                                            1 :
                                                                            0) +
                                                                            showCount;
                                                                    });
                                                                if (showCount >=
                                                                    maxShowableTagCount) {
                                                                    enqueueSnackbar(
                                                                        'حداکثر تعداد قابل نمایش ' +
                                                                        maxShowableTagCount +
                                                                        ' میباشد.',
                                                                        {
                                                                            variant: 'error',
                                                                            action: (key) => (
                                                                                <Fragment>
                                                                                    <Button
                                                                                        onClick={() => {
                                                                                            closeSnackbar(
                                                                                                key);
                                                                                        }}>
                                                                                        {lang.get(
                                                                                            'close')}
                                                                                    </Button>
                                                                                </Fragment>
                                                                            ),
                                                                        });
                                                                    return;
                                                                }
                                                            }
                                                            handleAdd({
                                                                ...t,
                                                                show: !t.show,
                                                            }, true);
                                                        }}
                                                        className={classes.root}
                                                        label={t.name.fa}
                                                        onDelete={() => handleDelete(
                                                            t)}
                                                        style={{
                                                            backgroundColor: !t.show ?
                                                                cyan[100] :
                                                                green[100],
                                                        }}/>
                                                </Box>
                                            ))
                                        }
                                    </Box>
                                </Box>

                                <Box display={'flex'} width={1}
                                     alignItems={'center'}
                                     justifyContent={'center'}>
                                    <Box flex={1} pr={4}>
                                        <TextFieldContainer
                                            name={createName(
                                                {name: 'tag-name'})}
                                            defaultValue={defValue}
                                            onChange={(val) => {
                                                setInputValue(val);
                                            }}
                                            render={(
                                                ref,
                                                {
                                                    name: inputName,
                                                    initialize,
                                                    valid,
                                                    errorIndex,
                                                    inputProps,
                                                    style,
                                                    props
                                                }) => {
                                                return (
                                                    <TextField
                                                        {...props}
                                                        variant={'outlined'}
                                                        error={!valid}
                                                        name={inputName}
                                                        inputRef={ref}
                                                        fullWidth
                                                        onKeyPress={(e) => {
                                                            try {
                                                                if (e.key ===
                                                                    'Enter') {
                                                                    const val = ref.current.getAttribute(
                                                                        textFieldNewValue);
                                                                    setValue(
                                                                        val);
                                                                    setDefValue(
                                                                        val);
                                                                }
                                                            } catch (e) {
                                                            }
                                                        }}
                                                        endAdornment={(inputValue) &&
                                                        (
                                                            <IconButton
                                                                onClick={() => {
                                                                    try {
                                                                        setDefValue(
                                                                            '');
                                                                        setValue(
                                                                            '');
                                                                        setInputValue(
                                                                            '');
                                                                    } catch (e) {
                                                                    }
                                                                }}>
                                                                <CancelOutlined/>
                                                            </IconButton>
                                                        )}
                                                        label={'جست‌و‌جو تگ'}
                                                        placeholder={'بعد تایپ نام تگ enter بزنید'}
                                                        style={{
                                                            ...style,
                                                            minWidth: '90%',
                                                        }}
                                                        inputProps={{
                                                            ...inputProps,
                                                        }}/>
                                                );
                                            }}/>
                                    </Box>
                                    <Box display={'flex'}
                                         alignItems={'center'}>
                                        <BaseButton
                                            disabled={value.length < 2}
                                            size={'medium'}
                                            style={{
                                                backgroundColor: cyan[300],
                                                color: '#fff',
                                            }}
                                            onClick={handleAddNewTag}>
                                            {lang.get('add')}
                                        </BaseButton>
                                        <Box pl={2} display={'flex'}>
                                            <BaseButton
                                                size={'medium'}
                                                style={{
                                                    backgroundColor: cyan[300],
                                                    color: '#fff',
                                                }}
                                                onClick={() => {
                                                    setSetting({
                                                        ...setting,
                                                        selectGpTag: true,
                                                    });
                                                }}>
                                                گروه تگ‌ها
                                            </BaseButton>
                                        </Box>
                                    </Box>
                                </Box>
                                <Box mt={2}>
                                    {

                                        data ?
                                            <Collapse in={!_.isEmpty(data)}>
                                                <Box display={'flex'}
                                                     flexWrap={'wrap'}
                                                     py={2} px={2}>
                                                    {
                                                        !_.isEmpty(data) &&
                                                        data.map((d, i) => (
                                                            <Box key={i}
                                                                 ml={2}
                                                                 mb={1.5}>
                                                                <Chip
                                                                    label={d.name.fa}
                                                                    disabled={_.findIndex(
                                                                        tags,
                                                                        (t) => {
                                                                            return d.permalink ===
                                                                                t.permalink;
                                                                        }) !==
                                                                    -1}
                                                                    onClick={() => handleAdd(
                                                                        d)}
                                                                    style={{
                                                                        color: d.hasActive ?
                                                                            theme.palette.action.hover :
                                                                            null,
                                                                    }}/>
                                                            </Box>
                                                        ))
                                                    }
                                                </Box>
                                            </Collapse> :
                                            <PleaseWait/>
                                    }
                                </Box>
                            </Box>
                        </Box> :
                        <ComponentError
                            statusCode={(_.isObject(error) &&
                                error.request) ?
                                error.request.status :
                                undefined}
                            tryAgainFun={() => mutate(apiKey)}/>
                    }
                </Box>
            </Box>
            <TagSingle
                open={setting.openAddTagDialog}
                name={value}
                onClose={(item) => {
                    if (item) {
                        item.hasActive = true;
                        handleAdd(item);
                        enqueueSnackbar('افزوده شد.',
                            {
                                variant: 'success',
                                action: (key) => (
                                    <Fragment>
                                        <Button onClick={() => {
                                            closeSnackbar(key);
                                        }}>
                                            {lang.get('close')}
                                        </Button>
                                    </Fragment>
                                ),
                            });
                        handleCloseDialog(false);
                        mutate(apiKey);
                        return;
                    }
                    setSetting({
                        ...setting,
                        openAddTagDialog: false,
                        dialogPermalink: '',
                    });
                }}/>
            <SelectTag open={setting.selectGpTag}
                       boxId={catId}
                       selectable={true}
                       multiSelect={true}
                       onClose={(data) => {
                           if (data) {
                               let i = 0;
                               const newTg = [];
                               _.forEach(data, (it) => {
                                   const index = _.findIndex(tag_groups,
                                       d => {
                                           return d.id === it.id;
                                       });
                                   if (-1 !== index) {
                                       i = i + 1;
                                   } else {
                                       newTg.push(it);
                                   }
                               });
                               if (!_.isEmpty(newTg))
                                   onTagGpChange([
                                       ...tag_groups,
                                       ...newTg,
                                   ]);
                               if (i > 0)
                                   enqueueSnackbar(
                                       i + ' گروه از قبل انتخاب شده است.',
                                       {
                                           variant: 'warning',
                                           action: (key) => (
                                               <Fragment>
                                                   <Button onClick={() => {
                                                       closeSnackbar(key);
                                                   }}>
                                                       {lang.get('close')}
                                                   </Button>
                                               </Fragment>
                                           ),
                                       });
                           }
                           setSetting({
                               ...setting,
                               selectGpTag: false,
                           });
                       }}/>
            {!_.isEmpty(tag_groups) &&
            <Box display={'flex'}
                 flexDirection={'column'} mb={1}>
                <Typography variant={'body1'} pb={1}
                            fontWeight={400}>
                    گروه تگ‌های فعال:
                </Typography>
                <Box display={'flex'} flexWrap={'wrap'}>
                    {
                        tag_groups.map(tg => (
                            <Box width={1 / 3}>
                                <TagItem
                                    key={tg.id}
                                    item={tg}
                                    product={true}
                                    selectable={false}
                                    editable={false}
                                    width={1}
                                    onRemoved={() => handleRemoveTag(tg)}/>
                            </Box>
                        ))
                    }
                </Box>
            </Box>}
        </React.Fragment>
    );
})

export default TagAndBrand;