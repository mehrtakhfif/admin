import { Box, makeStyles, TextField, Typography } from "@material-ui/core";
import ControllerProduct from "../../controller/ControllerProduct";
import React, { useState } from "react";
/* 
{
        "custom_price": {},
        "id": 2,
        "created_at": 1637049281.226308,
        "weekend": 2000,
        "guest": 1000,
        "peak": 3000,
        "weekday": 1000,
        "updated_at": 1637049281.226308
    }


        "capacity": 5,
    "max_capacity": 8,
    "min_reserve_time": 3
*/

const useStyle = makeStyles({
  inputPrice: {
    margin: "24px 24px",
  },
});

export default function HousePrice({ id, data, ...props }) {
  const classes = useStyle();
  const [state, setstate] = useState({
    id: data?.price?.id,
    weekday: data?.price?.weekday,
    guest: data?.price?.guest,
    peak: data?.price?.peak,
    weekend: data?.price?.weekend,
  });

  return (
    <Box display={'flex'} mt={2} justifyContent={'space-around'}>
    <Box width={1/3} display={"flex"} flexDirection={"column"}>
      <Typography variant={'h3'} style={{ fontWeight: 'bold' }}>ظرفیت</Typography>
      <TextField
        className={classes.inputPrice}
        name={"capacity"}
        defaultValue={data?.capacity}
        onBlur={updateProduct}
        label={"ظرفیت"}
      />
      <TextField
        className={classes.inputPrice}
        name={"max_capacity"}
        defaultValue={data?.max_capacity}
        onBlur={updateProduct}
        label={"حداکثر ظرفیت"}
      />
      <TextField
        className={classes.inputPrice}
        name={"min_reserve_time"}
        defaultValue={data?.min_reserve_time}
        onBlur={updateProduct}
        label={"حد اقل زمان رزرو (روز)"}
      />
    </Box>
    <Box width={1/3} display={"flex"} flexDirection={"column"}>
      <Typography variant={'h3'} style={{ fontWeight: 'bold' }}>هزینه</Typography>
      <TextField
        className={classes.inputPrice}
        name={"weekday"}
        defaultValue={state.weekday}
        onChange={handleChange}
        onBlur={updateHousePrice}
        label={"قیمت پایه"}
      />
      <TextField
        className={classes.inputPrice}
        name={"guest"}
        defaultValue={state.guest}
        onChange={handleChange}
        onBlur={updateHousePrice}
        label={"قیمت میهمان"}
      />
      <TextField
        fullwidth
        className={classes.inputPrice}
        name={"weekend"}
        defaultValue={state.weekend}
        onBlur={updateHousePrice}
        onChange={handleChange}
        label={"آخر هفته"}
      />
      <TextField
        className={classes.inputPrice}
        name={"peak"}
        defaultValue={state.peak}
        onChange={handleChange}
        onBlur={updateHousePrice}
        label={"قیمت تعطیلات"}
      />
    </Box>
    </Box>
  );


  function updateProduct(e) {
    const { name, value } = e.target;
    props.updateProduct(id,{[name]:value})
  }

  function handleChange(e) {
  }

  function updateHousePrice(e) {
    const { name, value } = e.target;
    if(state[name]!==parseInt(value)){
    ControllerProduct.HousePrice.update({
      product_id: parseInt(id),
      id: state.id,
      [name]: parseInt(value)
    }).then(res=>{
      
      setstate((s) => {
        if(s.id)
        return { ...s, [name]: parseInt(value)}
        
        return { ...s, [name]: parseInt(value), id: res.data.id}
      
      });
      props.successLoading()
    }).catch(e=>{
      props.errorLoading()
    });
  }
  }
}
