import React, { useState, useEffect, useRef, useCallback } from "react";

import ControllerProduct from "../../controller/ControllerProduct";
import { gcError } from "../../utils/ObjectUtils";
import Lottie from "react-lottie";
import loadingAnimationData from "../../loadingAnimation.json";
import successTickData from "../../success_tick.json";
import errorTickData from "../../error_tick.json";
import errorAnimationData from "../../errorAnimation.json";

const loadingOptions = {
  animationData: loadingAnimationData,
  loop: true,
  autoplay: true,
};
const errorOptions = {
  animationData: errorAnimationData,
  loop: true,
  autoplay: true,
};
const errorsTickOptions = {
  animationData: errorTickData,
  loop: false,
  autoplay: false,
};
const successTickOptions = {
  animationData: successTickData,
  loop: false,
  autoplay: false,
};

export default function Request({
  id,
  onlyFields = [],
  excludeFields = [],
  language,
  children,
  ...props
}) {
  const [data, setData] = useState();
  const [error, setError] = useState();
  const responseLoadingKey = { success: "success", error: "error" };
  const [responseLoading, setResponseLoading] = useState(undefined);

  useEffect(() => {
    ControllerProduct.Product.getProduct(id, onlyFields, excludeFields)[1]()
      .then((res) => {
        setError(false);
        setData(res);
      })
      .catch((e) => {
        gcError(e);
        setError(e);
      });
  }, []);

  const updateProduct = useCallback((id, data) => {
    return ControllerProduct.Product.putProduct({ id, data })
      .then((res) => {
        if (res.status === 202) {
            successLoading()
        } else {
            errorLoading()
        }
        return res;
      })
      .catch((e) => {
        errorLoading()
        return e;
      });
  }, []);

  function errorLoading() {
    setResponseLoading(responseLoadingKey.error);
  }

  function successLoading() {
    setResponseLoading(responseLoadingKey.success);
  }

  function handleAnimationComplete() {
    setResponseLoading(undefined);
  }

  if (error) return <Error messag={error.message} />;

  if (data === undefined) return <Loading />;

  return (
    <div
      className={"RequestMainContent"}
      style={{ position: "relative", height: "100%" }}
    >
      <MemoCm
        id={id}
        updateProduct={updateProduct}
        language={language}
        successLoading={successLoading}
        errorLoading={errorLoading}
        {...props}
        {...data}
      >
        {children}
      </MemoCm>
      <div
        style={{
          display:
            responseLoading === responseLoadingKey.success ? "block" : "none",
          position: "fixed",
          left: 8,
          bottom: 16,
          width: 40,
          height: 40,
        }}
      >
        <Lottie
          options={successTickOptions}
          height={"100%"}
          isStopped={responseLoading !== responseLoadingKey.success}
          isPaused={responseLoading !== responseLoadingKey.success}
          eventListeners={[
            {
              eventName: "complete",
              callback: handleAnimationComplete,
            },
          ]}
          cmplete={handleAnimationComplete}
          width={"100%"}
        />
      </div>
      <div
        style={{
          display:
            responseLoading === responseLoadingKey.error ? "block" : "none",
          position: "fixed",
          left: 8,
          bottom: 16,
          width: 40,
          height: 40,
        }}
      >
        <Lottie
          options={errorsTickOptions}
          height={"100%"}
          isStopped={responseLoading !== responseLoadingKey.error}
          isPaused={responseLoading !== responseLoadingKey.error}
          eventListeners={[
            {
              eventName: "complete",
              callback: handleAnimationComplete,
            },
          ]}
          cmplete={handleAnimationComplete}
          width={"100%"}
        />
      </div>
    </div>
  );
}

const MemoCm = React.memo(function MemoCm({ children, ...props }) {
  return (
    <React.Fragment>
      {React.cloneElement(children, { ...props })}
    </React.Fragment>
  );
});

export function Loading(props) {
  return (
    <div style={{ padding: "100px" }}>
      <Lottie options={loadingOptions} height={400} width={400} />
    </div>
  );
}

export function Error({ message, ...props }) {
  return (
    <div style={{ padding: "100px" }}>
      <Lottie options={errorOptions} height={400} width={400} />
      <p>{message}</p>
    </div>
  );
}
