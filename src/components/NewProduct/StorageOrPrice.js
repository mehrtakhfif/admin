import axios from "axios";
import React, { useEffect, useState } from "react";
import api from "../../controller/api";
import StorageDashboard from "../../pages/storages/StorageDashboard";
import { gcLog } from "../../utils/ObjectUtils";
import HousePrice from "./HousePrice";
import Request, { Loading } from "./Request";

export default function StorageOrPrice({ productId, ...props }) {
  const [state, setState] = useState();

  useEffect(() => {
    axios.get(api.Products.main, { params: { id: productId } }).then((res) => {
      setState(res.data.data.type);
    });
  }, []);

  switch (state) {
    case undefined:
      return <Loading />;

    case "tourism":
      return (
        (<Request
            id={productId}
            onlyFields={["price","capacity","max_capacity","min_reserve_time"]}
            >
          <HousePrice/>
        </Request>)
      );

    default:
      return <StorageDashboard productId={productId} dialogeMode={true} />;
  }
}
