import React, {useState} from 'react';
import {lang, mediaApiRout, siteLang} from '../../repository';
import DraggableList from '../base/draggableList/DraggableList';
import {UtilsStyle} from '../../utils/Utils';
import Img from '../base/img/Img';
import Typography from '../base/Typography';
import UploadItem from '../base/uploader/UploadItem';
import {serverFileTypes} from '../../controller/type';
import {gcLog, getSafe} from '../../utils/ObjectUtils';
import FormContainer from '../base/FormContainer';
import {grey, cyan} from '@material-ui/core/colors';
import {Box, Switch, Button, IconButton, Card, Tooltip} from '@material-ui/core';
import {Delete, Menu, MenuOpen, Image, PermMedia, Sort, Edit} from '@material-ui/icons';
import _ from 'lodash';
import BaseButton from '../base/button/BaseButton';
import {makeStyles} from '@material-ui/core/styles';
import ChangeAltDialog from "../base/img/ChangeAltDialog";

function Media({...props}) {
    const id = props.id;
    const [data, setData] = useState(props.data.media);
    const [thumbnail, setThumbnail] = useState(props.data.thumbnail);
    const [state, setState] = useState({
        sorting: false,
    });
    const [openAltDialog, setOpenAltDialog] = useState(false)
    const [mediaAltData, setMediaAltData] = useState()

    const useStyles = makeStyles((theme) => ({
        root: {
            '& > *': {
                margin: theme.spacing(1),
            },
        },
        sortButton: {
            background: state.sorting ?
                theme.palette.secondary.main :
                '#fff',
            color: state.sorting ?
                '#fff' : '#000'
            ,
        },

    }));
    const classes = useStyles();

    gcLog('Media Log', {...props.data});

    function updateProduct(inputData) {
        return props.updateProduct(id, inputData).then((res) => {
        }).catch((e) => {
            gcLog("error")
        });
    }

    function toggleAltDialog(id,text,type) {
        gcLog("toggleAltDialog id", {id,text})
        setMediaAltData({id:id,text:text,type:type})
        setOpenAltDialog(s => !s)
    }

    function handleAltDialogClose(id,text,type){
        gcLog("handleAltDialogClose", {id, text, type})
        if (text===undefined||id===undefined){
            toggleAltDialog(undefined,undefined)
            return
        }
        switch (type){
            case 'media':{
                const newData = _.cloneDeep(data)
                var index = newData.findIndex((c=> c.id === id))
                newData[index] = {...newData[index],title:{[siteLang]:text}}
                setData(newData)
                toggleAltDialog(undefined,undefined)
                break
            }
            case 'thumbnail':{
                setThumbnail(s=>({...s ,title: {[siteLang]:text}}))
                toggleAltDialog(undefined,undefined)
                break
            }
            default :{
                toggleAltDialog(undefined,undefined)
            }
        }
    }

    function onSelectMedia(media, sorted = false) {
        let oldData = [...data]
        setData([...media])
        if (sorted) {
            let sortedListIds = []
            media.forEach((item) => {
                sortedListIds.push(item.id)
            })
            updateProduct({'media': sortedListIds}).then((res) => {
            }).catch((e) => {
                gcLog("eroooor", e)
                setData(oldData)
            })
            return
        }
        if (!_.isEqual(data, media)) {
            let orderedList = [...media]
            orderedList.sort((a, b) => (a.priority > b.priority) ? 1 : ((b.priority > a.priority) ? -1 : 0))
            let sortedListIds = []
            orderedList.forEach((item) => {
                sortedListIds.push(item.id)
            })

            gcLog('Media OnSet', {oa: orderedList, b: media, cId: sortedListIds});
            updateProduct({'media': sortedListIds}).then((res) => {
            }).catch((e) => {
                gcLog("eroooor", e)
                setData(oldData)
            })
        }

    }

    function onSelectThumbnail(th) {
        if (!_.isEqual(thumbnail, th)) {

            gcLog('Media onSelectThumbnail', {th});
            updateProduct({'thumbnail_id': th.id}).then((res) => {
                setThumbnail(th)
            })
        }

    }

    function toggleSort() {
        setState({
            ...state,
            sorting: !state.sorting,
        });
    }


    gcLog("asfljkaskjfkjaskjf",props)

    const catId = props.data.category.id


    return (
        <Box display={'flex'} flexDirection={'column'}>
            <Box display={'flex'}>
                <UploadItem
                    boxId={catId}
                    src={thumbnail?.image|| ''}
                    type={serverFileTypes.Image.Thumbnail.type}
                    boxWidth={'auto'}
                    width={serverFileTypes.Image.Thumbnail.width}
                    height={serverFileTypes.Image.Thumbnail.height}
                    tooltipTitle={'تامبنیل'}
                    buttonIcon={<Image fontSize={'large'}/>}
                    onSelected={onSelectThumbnail}/>

                <UploadItem
                    boxId={catId}
                    holderHeight={'auto'}
                    multiSelect={true}
                    boxWidth={'auto'}
                    tooltipTitle={'مدیا'}
                    buttonIcon={<PermMedia fontSize={'large'}/>}
                    type={serverFileTypes.Image.Media.type}
                    width={serverFileTypes.Image.Media.width}
                    height={serverFileTypes.Image.Media.height}
                    onSelected={(files) => {
                        _.forEach(data, (d) => {
                            _.remove(files, (f) => {
                                return f.id === d.id;
                            });
                        });
                        onSelectMedia([...data, ...files]);
                    }}/>
                <Tooltip title={'مرتب سازی مدیا'}>
                    <IconButton
                        disabled={!data || data.length < 2}
                        onClick={toggleSort}><Sort fontSize={'large'}/></IconButton>
                </Tooltip>
            </Box>
            {thumbnail ?
                <Box display={'flex'} width={1} height={'130px'} p={3}
                     bgcolor={'white'} alignItems={'center'}>
                    <Box width={'80%'} display={'flex'} flexDirection={'row'} alignItems={'center'}
                         justifyContent={'left'}>
                        <Typography variant={'subtitle2'}>{thumbnail?.title[siteLang]}</Typography>
                        <Box mx={2}>
                            <Tooltip title={"ویرایش آلت"}><Button onClick={() => toggleAltDialog(thumbnail.id,thumbnail?.title[siteLang],'thumbnail')}><Edit
                                fontSize={'small'}/></Button></Tooltip>
                        </Box>
                    </Box>
                    <Img width={'auto'} height={'inherit'}
                         src={thumbnail?.image}/>
                </Box> : <React.Fragment/>}
            <Box title={lang.get('media')} boxProps={{flexWrap: 'wrap'}} m={1}>
                {state.sorting ?
                    <Box width={1}>
                        <DraggableList
                            items={data}
                            onItemsChange={(items) => {
                                onSelectMedia(items, true);
                            }}
                            rootStyle={(dragging) => {
                                return {
                                    backgroundColor: dragging ?
                                        null :
                                        grey[200],
                                    ...UtilsStyle.transition(500),
                                };
                            }}
                            render={(item, props, {isDraggingItem, ...p}) => (
                                <Box key={item.id}
                                     display={'flex'}
                                     width={1}
                                     {...props}>
                                    <Box display={'flex'} alignItems={'center'}
                                         py={1}>
                                        <Box px={2} py={1}>
                                            {isDraggingItem ?
                                                <MenuOpen/> :
                                                <Menu/>
                                            }
                                        </Box>
                                        <Img maxWidth={70} minHeight={'unset'}
                                             src={item.image}/>
                                        <Typography pr={2} pl={1}
                                                    variant={'body1'}
                                                    fontWeight={600}
                                                    style={{
                                                        justifyContent: 'center',
                                                    }}>
                                            {item.title?.[siteLang]}
                                        </Typography>
                                    </Box>
                                </Box>
                            )}
                        />
                    </Box> :
                    <Box width={1} display={'flex'} flexWrap={'wrap'}>
                        {data?.map(m => (
                            <MediaItem key={m.id}
                                       item={m}
                                       toggleAltDialog={toggleAltDialog}
                                       onRemove={() => {
                                           const newList = [...data];
                                           _.remove(newList,
                                               d => d.id === m.id);
                                           onSelectMedia(newList);
                                       }}/>
                        ))}
                    </Box>}

            </Box>
            <ChangeAltDialog data={mediaAltData} open={openAltDialog} handleClose={handleAltDialogClose}/>
        </Box>
    );
}

function MediaItem({item, onRemove, toggleAltDialog, ...props}) {
    return (
        <Box px={2} py={2} width={1 / 3} position={'relative'}>
            <Box component={Card} display={'flex'} flexDirection={'column'}>
                <Img zoomable={true} src={item?.image}/>
                <Box display={'flex'} flexWrap={'wrap'} py={1} px={1}
                     alignItems={'center'}>
                    <Box bgcolor={'white'} m={1} position={'absolute'} style={{left:'16px',top:'20px',borderRadius:'8px'}}>
                        <IconButton onClick={() => onRemove()}>
                            <Delete fontSize={'small'}/>
                        </IconButton>
                    </Box>
                    <Box display={'flex'} flexDirection={'row'} width={1}>
                        <Typography  variant={'body1'} fontWeight={600} style={{
                            flex:1,
                            alignItems:'center'
                        }}>
                            {item?.title?.fa}
                        </Typography>
                        {/*todo multi language problem */}
                        <Tooltip title={"ویرایش alt"}><Button onClick={() => toggleAltDialog(item?.id,item?.title?.fa,'media')}><Edit
                            fontSize={'small'}/></Button></Tooltip>
                    </Box>
                </Box>
            </Box>
        </Box>
    );
}

export default Media;