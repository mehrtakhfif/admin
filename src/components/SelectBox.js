import React, {useEffect} from "react";
import {useSelector} from "react-redux";
import Box from "@material-ui/core/Box";
import BaseButton from "./base/button/BaseButton";
import {activeLang, DEBUG} from "../repository";
import Typography from "./base/Typography";
import {useTheme} from "@material-ui/core";
import _ from "lodash"
import {gcLog} from "../utils/ObjectUtils";


export default function SelectBox({onBoxSelected, ...props}) {
    const {boxes} = useSelector(state => state.user);
    const theme = useTheme();
    useEffect(() => {
        // if (DEBUG) {
        //     onBoxSelected(_.find(boxes, b => b.id === 3))
        // }
    }, [])
    useEffect(() => {
        if (boxes.length === 1) {
            onBoxSelected(boxes[0])
        }
    });

    return (
        <Box display={'flex'} flexDirection={'column'} p={3}>
            <Typography variant={'h5'} pt={1} pb={3}>
                لطفا یک باکس را انتخاب کنید:
            </Typography>
            <Box display={'flex'} flexWrap={'wrap'}>
                {boxes.map((b) => (
                    <Box py={2} pl={2} key={b.id}>
                        <BaseButton onClick={() => {
                            gcLog("BaseButton onClick",b)
                            return onBoxSelected(b)
                        }}
                                    style={{
                                        color: '#fff',
                                        backgroundColor: theme.palette.primary.main
                                    }}>
                            <Typography variant={'body1'} color={"#fff"}>
                                {b.name[activeLang]}
                            </Typography>
                        </BaseButton>
                    </Box>
                ))}
            </Box>
        </Box>
    )
}
