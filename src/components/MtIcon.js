import React from "react";
import Icon from "@material-ui/core/Icon";
import PropTypes from 'prop-types'


function MtIcon({icon, fontSize, color,hasChildPath,...props}) {



    return (
        <Icon
            className={`mt ${icon}`}
            fontSize={fontSize}
            {...props}
            style={{
                color: color,
                ...props.style
            }}>
            {
                hasChildPath&&
                <React.Fragment>
                    <span className="path1"/>
                    <span className="path2"/>
                    <span className="path3"/>
                </React.Fragment>
            }
        </Icon>
    )
}

//region propTypes


MtIcon.propTypes = {
    icon: PropTypes.string,
    color: PropTypes.any,
    hasChildPath:PropTypes.bool,
    fontSize: PropTypes.oneOf(["small", "default", "large"])
}

export default MtIcon
//endregion propTypes
