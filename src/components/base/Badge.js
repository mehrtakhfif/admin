import React from "react";
import Badge from "@material-ui/core/Badge";
import {makeStyles} from "@material-ui/core/styles";


const useStyles = makeStyles(theme => (
    {
        customBadge: props => ({
            color: props.color,
            backgroundColor: props.backgroundColor,
        })
    }));


function BadgeComponent({color = '#000', backgroundColor='transparent', badgeContent, ...props}) {
    const classes = useStyles({color: color, backgroundColor: backgroundColor, ...props});
    return (
        <Badge
            classes={{badge: classes.customBadge}}
            badgeContent={badgeContent} {...props}>
            {props.children}
        </Badge>
    )
}

export default (BadgeComponent);
