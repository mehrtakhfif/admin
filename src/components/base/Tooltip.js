import React from "react";
import Tooltip from "@material-ui/core/Tooltip";

export default function({title, disable, ...props}) {
    return (
        (!disable && title) ?
            <Tooltip title={title}>
                {props.children}
            </Tooltip> :
            <React.Fragment>
                {props.children}
            </React.Fragment>
    )
}
