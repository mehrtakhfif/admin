import React from "react";
import {UtilsStyle} from "../../../utils/Utils";
import ButtonBase from "@material-ui/core/ButtonBase";
import Typography from "../Typography";
import {Link} from "react-router-dom";

export default function ({toHref, border, backgroundColor, color, variant = 'body1', ...props}) {
    return (
        <ButtonBase
            {...props}
            style={{
                backgroundColor: backgroundColor,
                ...UtilsStyle.borderRadius(5),
                ...props.style,
            }}>
            <Typography variant={variant} color={color} p={1}>
                <Link to={toHref} style={{
                    color:color
                }}>
                    {props.children}
                </Link>
            </Typography>
        </ButtonBase>
    )
}
