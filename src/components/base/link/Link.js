import React from 'react'
import PropTypes from "prop-types";
import {Link as MLink, makeStyles, useTheme} from "@material-ui/core";
import clsx from "clsx";
import {Link as RouterLink} from "react-router-dom";
import Box from "../Typography";
import {gcLog} from "../../../utils/ObjectUtils";


const useStyles = makeStyles(theme => ({
    root: props => ({
        color: props.color,
        '& *': {
            color: props.color,
        },
        "&:hover": {
            color: props.hoverColor,
            '& *': {
                color: props.hoverColor,
            }
        },
    }),
    clearLink: {
        color: 'unset',
        '&:hover': {
            opacity: 1,
            color: 'unset',
        },
        '&:hover *': {
            opacity: 1,
            color: 'unset',
        }
    }
}));

export default function Link({color, hoverColor, hasHover, className, cursorIsPointer = true, toHref,target, linkStyle, ...props}) {
    const theme = useTheme();
    if (!hoverColor)
        hoverColor = theme.palette.secondary.main;
    const classes = useStyles({
        color: color,
        hoverColor: hoverColor,
        hasHover: hasHover,
        ...props
    });

    return (
        <Box className={clsx(className, classes.root)}
             component={toHref ? 'div' : 'a'}
             no_hover={!hasHover ? 'true' : 'false'}
             target={target}
             {...props}
             style={{
                 cursor: cursorIsPointer ? 'pointer' : null,
                 ...props.style
             }}>
            {toHref ?
                <MLink className={classes.clearLink} no_hover={!hasHover ? 'true' : 'false'} to={{pathname: toHref}}
                       style={{...linkStyle}}
                       target={target}
                       component={RouterLink}>
                    {props.children}
                </MLink>
                :
                props.children
            }
        </Box>
    )
}
Link.defaultProps = {
    color: 'unset',
    hasHover: true
};
Link.propTypes = {
    color: PropTypes.string,
    hoverColor: PropTypes.string,
    hasHover: PropTypes.bool
};
