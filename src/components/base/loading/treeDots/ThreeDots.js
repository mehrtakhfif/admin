import React from 'react';

export default function Loading() {
    return (
        <div aria-hidden="true" className="loadingDots">
            <span className="css-1ojr9sz-LoadingDot"/>
            <span className="css-12a6fd1-LoadingDot"/>
            <span className="css-19d8ryt-LoadingDot"/>
        </div>
    )
}
