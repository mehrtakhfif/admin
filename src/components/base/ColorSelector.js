import React, {useEffect} from "react";
import Box from "@material-ui/core/Box";
import Popover from "@material-ui/core/Popover";
import {
    red,
    pink,
    purple,
    deepPurple,
    indigo,
    blue,
    lightBlue,
    cyan,
    teal,
    green,
    lightGreen,
    lime,
    yellow,
    amber,
    orange,
    deepOrange,
    brown,
    grey,
    blueGrey
} from "@material-ui/core/colors";
import {UtilsStyle} from "../../utils/Utils";
import ButtonBase from "@material-ui/core/ButtonBase";
import Typography from "./Typography";

const defItem = [100, 200, 300, 400, 500, 600, 700, 800, 900, 'A100', 'A200', 'A400', 'A700'];
const defColor = [
    red,
    pink,
    purple,
    deepPurple,
    indigo,
    blue,
    lightBlue,
    cyan,
    teal,
    green,
    lightGreen,
    lime,
    yellow,
    amber,
    orange,
    deepOrange,
    brown,
    grey,
    blueGrey
];

export default function ColorSelector({id, anchorEl, onSelect, ...props}) {


    return (
        <Popover
            id={id}
            open={Boolean(anchorEl)}
            anchorEl={anchorEl}
            onClose={() => onSelect()}
            anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'left',
            }}
            transformOrigin={{
                vertical: 'top',
                horizontal: 'left',
            }}>
            <Box minWidth={350} display={'flex'} flexWrap={'wrap'}>
                {
                    defColor.map((it) => (
                        <ColorItem key={it} color={it} onSelect={onSelect}/>
                    ))
                }
            </Box>
        </Popover>
    )
}

function ColorItem({color, onSelect, ...props}) {
    const [anchorEl, setAnchorEl] = React.useState(null);


    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const handleSelect = (color) => {
        handleClose()
        onSelect(color)
    }

    const open = Boolean(anchorEl);
    const id = open ? 'color-picker-item-popover' : undefined;


    return (
        <React.Fragment>
            <Box p={1}>
                <ButtonBase
                    aria-describedby={id}
                    onClick={handleClick}>
                    <Box
                        style={{
                            backgroundColor: color[300],
                            width: 30,
                            height: 30,
                            ...UtilsStyle.borderRadius(5)
                        }}/>
                </ButtonBase>
            </Box>
            <Popover
                id={id}
                open={Boolean(anchorEl)}
                anchorEl={anchorEl}
                onClose={handleClose}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'left',
                }}>
                <Box minWidth={350} display={'flex'} flexWrap={'wrap'}>

                    {
                        defItem.map((it) => (
                            <Box
                                display={'flex'}
                                flexDirection={'column'}
                                alignItems={'center'}
                                justifyContent={'center'}
                                key={`${color}-${it}`}
                                p={1}>
                                <ButtonBase
                                    aria-describedby={id}
                                    onClick={() => handleSelect(color[it])}>
                                    <Box
                                        style={{
                                            backgroundColor: color[it],
                                            width: 30,
                                            height: 30,
                                            ...UtilsStyle.borderRadius(5)
                                        }}/>
                                </ButtonBase>
                                <Typography pt={1}
                                            variant={'caption'}>
                                    {it}
                                </Typography>
                            </Box>
                        ))
                    }
                </Box>
            </Popover>
        </React.Fragment>
    )
}


export function ColorSelectorInput({color, onSelect, ...props}) {
    const [anchorEl, setAnchorEl] = React.useState(null);
    useEffect(() => {
        if (!color)
            onSelect(grey[900])
    }, [])


    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const open = Boolean(anchorEl);
    const id = open ? 'color-picker-popover' : undefined;

    return (
        <React.Fragment>
            <ButtonBase
                aria-describedby={id}
                onClick={handleClick}>
                <Box
                    {...props}
                    style={{
                        minWidth: 70,
                        minHeight: 30,
                        backgroundColor: color || cyan[300],
                        ...UtilsStyle.borderRadius(5),
                        ...props.style,
                    }}/>
            </ButtonBase>
            <ColorSelector
                id={id}
                anchorEl={anchorEl}
                onSelect={(color) => {
                    handleClose()
                    if (color) {
                        onSelect(color)
                    }
                }}/>
        </React.Fragment>
    )
}
