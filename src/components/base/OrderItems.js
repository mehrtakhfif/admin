import React, {Fragment, useState} from "react";
import Box from "@material-ui/core/Box";
import BaseDialog from "../../components/base/BaseDialog";
import Typography from "../../components/base/Typography";
import Backdrop from "@material-ui/core/Backdrop";
import CircularProgress from "@material-ui/core/CircularProgress";
import {cyan, grey} from "@material-ui/core/colors";
import {useSnackbar} from "notistack";
import Button from "@material-ui/core/Button";
import {lang} from "../../repository";
import _ from 'lodash'
import DraggableList from "../../components/base/draggableList/DraggableList";
import {UtilsStyle} from "../../utils/Utils";
import SuccessButton from "./button/buttonVariant/SuccessButton";
import ControllerSite from "../../controller/ControllerSite";
import ComponentError from "./ComponentError";
import BaseButton from "./button/BaseButton";
import {gcLog} from "../../utils/ObjectUtils";


export default function OrderItems ({open, model, header = "مرتبسازی", helperText, data, renderItem, onDataChange, onRetry, onClose, ...props}) {
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();

    const [loading, setLoading] = useState(false)

    function handleSave() {
        const ids = []
        _.forEach(data, d => {
            ids.push(d.sortId || d.id)
        })
        setLoading(true)
        ControllerSite.Order.set({model, ids}).then(res => {
            onClose(true)
        }).finally(() => {
            setLoading(false)
        })
    }

    function showSnack(text, variant = "error") {
        enqueueSnackbar(text,
            {
                variant: variant,
                action: (key) => (
                    <Fragment>
                        <Button onClick={() => {
                            closeSnackbar(key)
                        }}>
                            {lang.get('close')}
                        </Button>
                    </Fragment>
                )
            });
    }


    const el = (
        <React.Fragment>
            {
                _.isArray(data) &&
                <Box p={2} display={'flex'} flexDirection={'column'}>
                    {helperText &&
                    <Typography variant={"caption"} pr={0.5}>
                        {helperText}
                    </Typography>
                    }
                    <DraggableList
                        items={data}
                        onItemsChange={(items) => {
                            onDataChange(items)
                        }}
                        rootStyle={(dragging) => {
                            return {
                                backgroundColor: dragging ? null : grey[200],
                                ...UtilsStyle.transition(500)
                            }
                        }}
                        render={(item, props, {isDraggingItem, ...p}) => {
                            const {id} = item;
                            return (
                                <Box key={id}
                                     width={1}
                                     {...props}>
                                    {renderItem(item, {isDraggingItem, ...p})}
                                </Box>
                            )
                        }}/>
                    <Box display={'flex'} pt={2}>
                        <SuccessButton variant={"outlined"} onClick={handleSave}>
                            ذخیره
                        </SuccessButton>
                        {
                            onClose&&
                        <Box ml={2}>
                            <BaseButton
                                variant={"outlined"}
                                onClick={()=>onClose(false)}>
                                لغو
                            </BaseButton>
                        </Box>
                        }
                    </Box>
                </Box>
            }
            {
                data === false &&
                <ComponentError
                    tryAgainFun={onRetry}/>
            }
            <Backdrop
                open={loading || (open !== undefined && data === true)}
                style={{
                    zIndex: '9999'
                }}>
                <CircularProgress color="inherit"
                                  style={{
                                      color: cyan[300],
                                      width: 100,
                                      height: 100
                                  }}/>
            </Backdrop>
        </React.Fragment>
    )

    return (
        open === undefined ?
            el :
            <BaseDialog
                open={open}
                header={header}
                onClose={() => onClose(false)}>
                {el}
            </BaseDialog>
    )
}
