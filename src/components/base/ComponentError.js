import React from "react";
import Box from "@material-ui/core/Box";
import {lang} from "../../repository";
import BaseButton from "../base/button/BaseButton";
import Typography from "./Typography";

export default function ComponentError({
                                           message = lang.get("er_render_check_internet_connection"),
                                           tryAgainLabel = lang.get("try_again"),
                                           tryAgainFun,
                                           statusCode,
                                           ...props
                                       }) {
    return (
        <Box width={1} display='flex' px={2} py={2}
             flexDirection='column'
             justifyContent='center'
             alignItems={'center'}>
            {statusCode === 403 ?
                <Typography pt={1} pb={2} variant={'h6'}
                            style={{
                                lineHeight: 1.7,
                                textAlign: 'center',
                                display: 'flex',
                                justifyContent: 'center'
                            }}>
                    دسترسی شما به این بخش محدود شده است.
                </Typography>:
            <React.Fragment>
                <Typography pt={1} pb={2} variant={'h6'}
                            style={{
                                lineHeight: 1.7,
                                textAlign: 'center',
                                display: 'flex',
                                justifyContent: 'center'
                            }}>
                    {message}
                </Typography>
                <Box display={'flex'}>
                    <BaseButton
                        onClick={tryAgainFun}>
                        {tryAgainLabel}
                    </BaseButton>
                </Box>
            </React.Fragment>}
        </Box>
    )
}
