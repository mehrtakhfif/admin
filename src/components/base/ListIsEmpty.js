import React from "react";
import {grey} from "@material-ui/core/colors";
import Skeleton from "@material-ui/lab/Skeleton";
import Box from "@material-ui/core/Box";
import {isClient} from "../../repository";
import Typography from "./Typography";

export default function ListIsEmpty({fullPage = true, variant = 'h3', fontWeight = 400, color = grey[700], textRootStyle, textStyle, skeletonStyle, ...props}) {
    let style = {};
    if (fullPage && isClient())
        style = {
            height: window.innerHeight - 200,
        };
    return (
        <Box
            px={{
                xs: 0.2,
                md: 2,
                lg: 4
            }} py={{
            xs: 1,
            md: 2,
            lg: 4
        }}
            width={1}
            display={'flex'}
            flexDirection={'column'}
            alignItems={'center'}
            justifyContent={'center'}
            {...props}
            style={{
                ...style,
                ...props.style,
            }}>
            <Typography variant={variant} fontWeight={fontWeight} color={color}
                        style={{
                            position: 'absolute',
                            right: 0,
                            left: 0,
                            textAlign: 'center',
                            display: 'flex',
                            justifyContent: 'center',
                            ...textStyle,
                            ...textRootStyle
                        }}>
                چیزی پیدا نشد 😟
            </Typography>
            <Skeleton variant={"rect"} style={{
                width: '100%',
                height: '100%',
                ...skeletonStyle
            }}/>
        </Box>
    )
}
