import React from "react";
import {Typography} from "@material-ui/core";
import {theme} from "../../repository";
import _ from 'lodash';
import {makeStyles} from "@material-ui/core/styles";
import useTheme from "@material-ui/core/styles/useTheme";
import {gcLog} from "../../utils/ObjectUtils";


const useTypographyStyles = makeStyles(theme => ({
    typographyRoot: (props => ({
        '&:hover': {
            color: props.hoverColor
        }
    }))
}));
export default function ({
                             component = 'div', fontSize, variant, fontWeight, children, isText,
                             color,
                             width,
                             hoverColor = null,
                             display = 'flex',
                             flexDirection,
                             alignItems,
                             justifyContent,
                             whiteSpace=false,
                             ...props
                         }) {
    const theme = useTheme();

    const classes = useTypographyStyles({hoverColor: hoverColor});
    props = generateStyle(props);

    return (
        <Typography
            component={component}
            className={classes.typographyRoot}
            variant={variant}
            {...props}
            style={{
                width:width,
                color: color ? color : theme.palette.text.primary,
                fontWeight: fontWeight,
                fontSize: fontSize,
                display: display,
                alignItems: alignItems,
                flexDirection:flexDirection,
                justifyContent:justifyContent,
                whiteSpace: whiteSpace ? 'pre-wrap':undefined,
                ...props.style
            }}>
            {children}
        </Typography>
    )
}

function generateStyle(props) {
    const {p, px, py, pl, pr, pt, pb, m, mx, my, ml, mr, mt, mb, ...newProps} = props;
    const s = props.style ? props.style : {};
    //padding
    s.paddingLeft = getValue(pl || px || p);
    s.paddingRight = getValue(pr || px || p);
    s.paddingTop = getValue(pt || py || p);
    s.paddingBottom = getValue(pb || py || p);
    //margin
    s.marginLeft = getValue(ml || mx || m);
    s.marginRight = getValue(mr || mx || m);
    s.marginTop = getValue(mt || my || m);
    s.marginBottom = getValue(mb || my || m);


    return {
        ...newProps,
        style: {
            ...s
        }
    }
}

function getValue(v) {
    if (_.isNumber(v))
        return theme.spacing(v);
    return v
}
