import React from "react";
import {UtilsStyle} from "../../../utils/Utils";
import ButtonBase from "@material-ui/core/ButtonBase";
import Typography from "../Typography";

export default function ButtonText({buttonProps={}, ...props}) {
    return (
        <ButtonBase
            {...buttonProps}
            style={{
                ...UtilsStyle.borderRadius(5),
                ...buttonProps.style
            }}>
            <Typography p={1}{...props}>
                    {props.children}
            </Typography>
        </ButtonBase>
    )
}
