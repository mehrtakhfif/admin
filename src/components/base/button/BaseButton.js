import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import {green} from "@material-ui/core/colors";
import Button from "@material-ui/core/Button";
import CircularProgress from "@material-ui/core/CircularProgress";
import PropTypes from "prop-types";
import {useTheme} from "@material-ui/styles";

const useStyles = makeStyles(theme => ({
    buttonProgress: {
        color: green[500],
        position: 'absolute',
        top: '50%',
        left: '50%',
    },
}));

export default function BaseButton({id, loading, loadingStyle = {}, disabled, children, ...props}) {
    const theme = useTheme();
    const classes = useStyles();
    const superSmall = (props.size === 'superSmall');
    if (superSmall)
        props.size = 'small';
    return (
        <Button
            id={id}
            disabled={(loading || disabled)}
            {...props}
            style={{
                backgroundColor: props.variant === "contained" ? theme.palette.action.selected : null,
                borderColor: props.variant === "outlined" ? theme.palette.action.selected : null,
                padding: superSmall ? '3px 5px' : null,
                fontSize: superSmall ? '0.6125rem' : null,
                minWidth: superSmall ? 45 : null,
                ...props.style
            }}>
            {children}
            {loading && <CircularProgress
                className={classes.buttonProgress}
                size={superSmall ? 18 : 24}
                style={{
                    marginTop: superSmall ? -9 : -12,
                    marginLeft: superSmall ? -9 : -12,
                    ...loadingStyle
                }}/>}
        </Button>
    )
}
BaseButton.defaultProps = {
    size: 'medium',
    variant: "contained"
};

BaseButton.propTypes = {
    id: PropTypes.string,
    onClick: PropTypes.func,
    variant: PropTypes.oneOf(['text', 'contained', 'outlined']),
    size: PropTypes.oneOf(['superSmall', 'small', 'medium', 'large']),
    loading: PropTypes.bool,
    disabled: PropTypes.bool
};


