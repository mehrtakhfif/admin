import React from 'react';
import Green from '@material-ui/core/colors/green'
import BaseButton from "../BaseButton";
import PropTypes from "prop-types";
import {UtilsStyle} from "../../../../utils/Utils";

export default function SuccessButton({style, ...props}) {
    return (
        <BaseButton
            {...props}
            style={{
                color: !props.variant ? Green[50] : undefined,
                backgroundColor: !props.variant ? Green["A700"] : null,
                borderColor:Green["A700"],
                ...UtilsStyle.widthFitContent(),
                '&:hover': {
                    backgroundColor:  !props.variant ? Green[500] : null,
                },
                ...style,
            }}/>
    )
}
SuccessButton.propTypes = {
    id: PropTypes.string,
    onClick: PropTypes.func,
    variant: PropTypes.oneOf(['contained', 'outlined']),
    size: PropTypes.oneOf(['small', 'medium', 'large']),
    loading: PropTypes.bool,
    disabled: PropTypes.bool
};





