import React from "react";
import {isClient} from "../../repository";
import LazyLoad from 'react-lazyload';


export default function ClientLazyLoad({lzHeight,lzPlaceholder,lzOffset=0,lzOnce=false,...props}) {
    return (
        <React.Fragment>
            {isClient() ?
                <LazyLoad height={lzHeight} offset={lzOffset} once={lzOnce} placeholder={lzPlaceholder} >
                    {props.children}
                </LazyLoad> :
                props.children
            }
        </React.Fragment>
    )
}
