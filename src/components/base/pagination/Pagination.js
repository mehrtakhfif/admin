import React from "react";
import Box from "@material-ui/core/Box";
import {UtilsStyle} from "../../../utils/Utils";
import {cyan, grey} from "@material-ui/core/colors";
import {makeStyles} from "@material-ui/styles";
import Pagination from "react-paginate";
import {ChevronLeft, ChevronRight} from "@material-ui/icons";

const usePaginationStyles = makeStyles(theme => ({
    paginationRoot: ({disable, ...props}) => ({
        ...UtilsStyle.disableTextSelection(),
        '&>ul': {
            display: 'flex',
            listStyleType: 'none',
            direction: 'ltr',
        },
        '& li': {
            cursor: disable ? 'progress' : 'pointer',
            '& a': {
                pointerEvents: disable ? 'none' : 'all',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                marginRight: theme.spacing(0.5),
                marginLeft: theme.spacing(0.5),
                paddingRight: theme.spacing(0.5),
                paddingLeft: theme.spacing(0.5),
                minWidth: 40,
                minHeight: 40,
                ...UtilsStyle.transition(),
                ...UtilsStyle.borderRadius(5),
            }
        }
    }),
    paginationItem: {
        '& a': {
            fontSize: theme.typography.h5.fontSize,
            fontWeight: 400,
            '&:hover': {
                backgroundColor: grey[300],
                ...UtilsStyle.transition()
            }
        }
    },
    paginationActiveItem: {
        '& a': {
            cursor: 'default',
            backgroundColor: cyan[300],
            color: '#fff',
            '&:hover': {
                backgroundColor: cyan[400],
                ...UtilsStyle.transition()
            }
        }
    }
}));

export default function ({activePage, pages, pageRangeDisplayed = 5, disable = false, onPageChange, ...props}) {
    const classes = usePaginationStyles({disable, ...props});

    return (
        <Box
            className={classes.paginationRoot}
            display={'flex'}
            justifyContent={'center'}
            alignItems={'center'}
            {...props}>
            {props.children}
            {pages &&
            <Pagination
                initialPage={activePage}
                pageRangeDisplayed={pageRangeDisplayed}
                pageCount={pages}
                pageClassName={classes.paginationItem}
                activeClassName={classes.paginationActiveItem}
                disableInitialCallback={disable}
                previousLabel={
                    <Box>
                        <ChevronLeft/>
                    </Box>}
                nextLabel={
                    <Box>
                        <ChevronRight/>
                    </Box>}
                onPageChange={(pages) => {
                    if (!disable)
                        onPageChange(pages.selected)
                }}/>
            }
        </Box>
    )
}

