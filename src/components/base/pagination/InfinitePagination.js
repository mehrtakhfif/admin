import React from "react";
import Box from "@material-ui/core/Box";
import InfiniteScroll from "react-infinite-scroller";


export default function InfinitePagination({pageStart = 0, hasMore, loader = null, onPageChange, ...props}) {
    return (
        <Box {...props}>
            <InfiniteScroll
                pageStart={pageStart}
                loadMore={onPageChange}
                loader={loader}
                hasMore={hasMore}
                style={{
                    width: '100%'
                }}>
                {props.children}
            </InfiniteScroll>
        </Box>
    )
}
