import Collapse from "@material-ui/core/Collapse";
import BoxWithTopBorder from "./textHeader/BoxWithTopBorder";
import {Card} from "@material-ui/core";
import React from "react";

export default function FormContainer({title, active = true, open, activeLanguageValue, activeLanguage, onLanguageChange, onOpenClick, ...props}) {
    return (
        <Collapse in={active} style={{width: '100%'}}>
            <BoxWithTopBorder component={Card} open={open} display={'flex'}
                              activeLanguageValue={activeLanguageValue}
                              activeLanguage={activeLanguage}
                              title={title} mt={2}
                              width={1}
                              onLanguageChange={onLanguageChange}
                              onOpenClick={onOpenClick}
                              {...props}>
                {active &&
                <Collapse in={open}>
                    {props.children}
                </Collapse>
                }
            </BoxWithTopBorder>
        </Collapse>
    )
}
