import React from "react";
import SyncableTextField from "./SyncableTextField";
import {lang} from "../../../repository";
import TextFieldContainer from "./TextFieldContainer";
import {gcLog} from "../../../utils/ObjectUtils";


export default function PermalinkTextField({syncedRef, name, variant = "outlined", onRefChange, defaultValue, inputProps = {}}) {
    let inputRef = null;

    return (
        <TextFieldContainer
            name={name}
            actived={true}
            errorPatterns={['^[A-Za-z\u0591-\u07FF\uFB1D-\uFDFD\uFE70-\uFEFC][A-Za-z0-9-_ \u0591-\u07FF\uFB1D-\uFDFD\uFE70-\uFEFC]*$']}
            onChangeDelay={1000}
            onChange={(value) => {
                inputRef.current.setValue(generatePermalink(value))
            }}
            render={(ref, {name, initialize, valid, errorIndex, setValue}) => {
                inputRef = ref;
                if (onRefChange)
                    onRefChange(ref);
                return (
                    <SyncableTextField
                        name={name}
                        variant={variant}
                        fullWidth
                        syncedRef={syncedRef}
                        inputRef={ref}
                        required
                        label={lang.get('permalink')}
                        renderSynced={(text) => {
                            return generatePermalink(text)
                        }}
                        defaultValue={defaultValue}
                        {...inputProps}/>
                )
            }}
        />
    )
}


export function generatePermalink(text) {

    text = text.replaceAll(/[۰]/gm,"0")
    text = text.replaceAll(/[۱]/gm,"1")
    text = text.replaceAll(/[۲]/gm,"2")
    text = text.replaceAll(/[۳]/gm,"3")
    text = text.replaceAll(/[۴]/gm,"4")
    text = text.replaceAll(/[۵]/gm,"5")
    text = text.replaceAll(/[۶]/gm,"6")
    text = text.replaceAll(/[۷]/gm,"7")
    text = text.replaceAll(/[۸]/gm,"8")
    text = text.replaceAll(/[۹]/gm,"9")


    text = text.replaceAll(/[ _]+/gm, '-').toLowerCase();
    text = text.replaceAll(/[^-0-9a-zA-zاآبپتثجچحخدذرزژسشصضطظعغفقکگلمنوهی]|[\/\\'\^_`]/gm, "");
    text = text.replaceAll(/[-]+/gm, '-').toLowerCase();
    text = text.replaceAll(/[ _]+/gm, '-');
    return text.toLowerCase()
}
