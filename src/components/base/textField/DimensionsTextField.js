import _ from "lodash";
import {createName} from "./TextFieldMultiLanguageContainer";
import TextFieldContainer from "./TextFieldContainer";
import React from "react";
import TextField from "./TextField";
import ReactNumberFormat from "react-number-format";

export default function ({group, array, name, value, required, label, onChange, textFieldProps = {}, ...props}) {
    let inputRef = null;

    return (
        <TextFieldContainer
            defaultValue={_.toString(value)}
            name={createName({group: group, array: array, name: name})}
            type={'number'}
            onChangeDelay={undefined}
            onChange={(d, el) => {
                try {
                    onChange(d, el)
                } catch (e) {
                }
            }}
            returnValue={(value) => {
                return value.replace("g ", "").trimAll();
            }}
            render={(ref, {name, initialize, valid, errorText, inputProps, setValue, props}) => {
                inputRef = ref;
                return (
                    <TextField
                        error={!valid}
                        variant="outlined"
                        helperText={errorText}
                        name={name}
                        inputRef={ref}
                        placeholder={"g ---"}
                        fullWidth
                        required={required}
                        defaultValue={props.defaultValue}
                        label={label}
                        {...textFieldProps}
                        InputProps={{
                            ...textFieldProps.InputProps,
                            inputComponent: NumberFormat,
                        }}
                    />
                )
            }}/>
    )
}


function NumberFormat(props) {
    const {inputRef, onChange, ...other} = props;

    return (
        <ReactNumberFormat
            {...other}
            getInputRef={inputRef}
            isAllowed={(values) => {
                const {value, floatValue} = values;

                if (typeof floatValue === 'undefined' || typeof value === 'undefined') {
                    return true;
                }
                if (value.match('\\.')) {
                    return false
                }
                return value.charAt(0) !== '-';
            }}
            thousandSeparator={" "}
            isNumericString
            prefix="g "
            style={{
                direction: 'ltr'
            }}
        />
    );
}
