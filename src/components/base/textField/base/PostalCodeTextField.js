import React from 'react';
import {checkHasErrorPatternReturnBool} from "../../../../utils/Checker";
import PropTypes from 'prop-types';
import {lang} from "../../../../repository";
import _ from "lodash"
import BaseTextField from "./BaseTextField";

export default function PostalCodeTextField(props) {
    let newProps = props;
    if (_.isEmpty(props.label)) {
        newProps = {
            ...newProps,
            label: lang.get("postal_code")
        }
    }
    if (_.isEmpty(props.errorMessage)) {
        newProps = {
            ...newProps,
            errorMessage: lang.get('er_postal_code_structure')
        }
    }

    return (
        <BaseTextField
            {...newProps}
            dir='ltr'
            pattern={props.pattern}
            errorPattern={(text) => {
                return (text.replace(/\D/g, '').length >= 10 || !checkHasErrorPatternReturnBool('^\\d{5}[ -]?\\d{5}$', text))
            }}
            label={lang.get("postal_code")}/>
    )
}

PostalCodeTextField.defaultProps = {
    label: '',
    placeholder: '415XX-1XX35',
    icon: {},
    type: 'text',
    pattern: '^\\d{0,5}[ -]?\\d{0,5}$',
    validatable: false,
    maxLength: 11
};

PostalCodeTextField.propTypes = {
    inputName: PropTypes.string,
    label: PropTypes.string,
    isLabelRtl: PropTypes.bool,
    value: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    validatable: PropTypes.bool,
    hasValid: PropTypes.bool,
    isRequired: PropTypes.bool,
    isRequiredErrorMessage: PropTypes.string,
    hasError: PropTypes.bool,
    errorMessage: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
    dir: PropTypes.oneOf(['ltr', 'rtl']),
    icon: PropTypes.object,
    variant: PropTypes.oneOf(['standard', 'outline', 'outlineV2']),
    loading: PropTypes.bool,
    disabled: PropTypes.bool,
    maxLength: PropTypes.number,
    onTextChange: PropTypes.func,
    onTextFocusChange: PropTypes.func,
    style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};
