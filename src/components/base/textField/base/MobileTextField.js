import React from 'react';
import {isMobileNumber} from "../../../../utils/Checker";
import PropTypes from 'prop-types';
import {lang} from "../../../../repository";
import _ from "lodash"
import BaseTextField from "./BaseTextField";

export default function MobileTextField(props) {
    let newProps = props;
    if (_.isEmpty(props.label)) {
        newProps = {
            ...newProps,
            label: lang.get('mobile_number')
        }
    }
    if (_.isEmpty(props.errorMessage)) {
        newProps = {
            ...newProps,
            errorMessage: lang.get("er_verify_mobile")
        }
    }

    return (
        <BaseTextField
            {...newProps}
            maxLength={11}
            pattern="^[0-9][0-9]*$"
            errorPattern={(text) => {
                return isMobileNumber(text)
            }}
        />
    )
}

MobileTextField.defaultProps = {
    label: '',
    placeholder: '09*********',
    icon: {},
};

MobileTextField.propTypes = {
    inputName: PropTypes.string,
    label: PropTypes.string,
    isLabelRtl: PropTypes.bool,
    value: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    isRequired: PropTypes.bool,
    validatable: PropTypes.bool,
    isRequiredErrorMessage: PropTypes.string,
    hasError: PropTypes.bool,
    errorMessage: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
    dir: PropTypes.oneOf(['ltr', 'rtl']),
    variant: PropTypes.oneOf(['standard', 'outline', 'outlineV2']),
    icon: PropTypes.object,
    loading: PropTypes.bool,
    disabled: PropTypes.bool,
    maxLength: PropTypes.number,
    onTextChange: PropTypes.func,
    onTextFocusChange: PropTypes.func,
    style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};
