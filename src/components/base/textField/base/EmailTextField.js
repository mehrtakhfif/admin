import React from 'react';
import {isEmail} from "../../../../utils/Checker";
import PropTypes from 'prop-types';
import {lang} from "../../../../repository";
import _ from "lodash"
import BaseTextField from "./BaseTextField";

export default function EmailTextField(props) {
    let newProps = props;
    if (_.isEmpty(props.label)) {
        newProps = {
            ...newProps,
            label: lang.get('email')
        }
    }
    if (_.isEmpty(props.errorMessage)) {
        newProps = {
            ...newProps,
            errorMessage: lang.get("er_verify_email")
        }
    }

    return (
        <BaseTextField
            {...newProps}
            errorPattern={(text) => {

                return isEmail(text)
            }}
        />
    )
}

EmailTextField.defaultProps = {
    label: '',
    placeholder: 'example@email.com',
    icon: {},
};

EmailTextField.propTypes = {
    inputName: PropTypes.string,
    label: PropTypes.string,
    isLabelRtl: PropTypes.bool,
    value: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    isRequired: PropTypes.bool,
    validatable: PropTypes.bool,
    isRequiredErrorMessage: PropTypes.string,
    hasError: PropTypes.bool,
    errorMessage: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
    dir: PropTypes.oneOf(['ltr', 'rtl']),
    variant: PropTypes.oneOf(['standard', 'outline', 'outlineV2']),
    icon: PropTypes.object,
    loading: PropTypes.bool,
    disabled: PropTypes.bool,
    maxLength: PropTypes.number,
    onTextChange: PropTypes.func,
    onTextFocusChange: PropTypes.func,
    style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};
