import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import {lang} from "../../../../repository";
import _ from "lodash"
import BaseTextField from "./BaseTextField";
import {UtilsFormat} from "../../../../utils/Utils";

export default function PriceTextField(props) {
    const [value, setValue] = useState(UtilsFormat.numberToMoney(_.toString(props.value)));
    const [timer, setTimer] = useState(null);
    let newProps = props;
    if (_.isEmpty(props.label)) {
        newProps = {
            ...newProps,
            label: lang.get('قیمت')
        }
    }
    if (_.isEmpty(props.errorMessage)) {
        newProps = {
            ...newProps,
            errorMessage:"ساختار مبلغ اشتباه است"
        }
    }

    useEffect(() => {
        clearTimeout(timer);
        setTimer(setTimeout(() => {
            props.onTextChange(UtilsFormat.moneyToNumber(value));
        }, 1000))

        return () => {
            clearTimeout(timer);
        }
    }, [value]);

    return (
        <BaseTextField
            {...newProps}
            dir={'ltr'}
            selfHandleValueChanger={undefined}
            value={value}
            onTextChange={(text) => {
                if (Number.MAX_SAFE_INTEGER < _.toInteger(UtilsFormat.moneyToNumber(text)))
                    return;
                setValue(UtilsFormat.numberToMoney(UtilsFormat.moneyToNumber(text)))
            }}
            pattern="^[0-9][0-9,]*$"
            errorPattern="^\d+(,\d{3})*$"/>
    )
}


PriceTextField.propTypes = {
    inputName: PropTypes.string,
    label: PropTypes.string,
    isLabelRtl: PropTypes.bool,
    placeholder: PropTypes.string,
    isRequired: PropTypes.bool,
    validatable: PropTypes.bool,
    isRequiredErrorMessage: PropTypes.string,
    hasError: PropTypes.bool,
    errorMessage: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
    dir: PropTypes.oneOf(['ltr', 'rtl']),
    variant: PropTypes.oneOf(['standard', 'outline', 'outlineV2']),
    icon: PropTypes.object,
    loading: PropTypes.bool,
    disabled: PropTypes.bool,
    maxLength: PropTypes.number,
    onTextChange: PropTypes.func,
    onTextFocusChange: PropTypes.func,
    style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};
