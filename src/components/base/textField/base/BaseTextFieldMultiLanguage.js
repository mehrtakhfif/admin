import React, {useState} from "react";
import BaseTextField from "./BaseTextField";
import PropTypes from "prop-types";
import {getActiveLanguage, sLang} from "../../../../repository";
import Box from "@material-ui/core/Box";
import {FiberManualRecord} from "@material-ui/icons";
import IconButton from "@material-ui/core/IconButton";
import {green, orange, red} from "@material-ui/core/colors";
import Typography from "../../Typography";
import {UtilsStyle} from "../../../../utils/Utils";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import {hasActive} from "../../multiLanguageContainer/MultiLanguageContainer";

export default function BaseTextFieldMultiLanguage({
                                                       value,
                                                       onTextChange,
                                                       style,
                                                       textFieldStyle,
                                                       autoFocus = false,
                                                       ...props
                                                   }) {
    const [anchorEl, setAnchorEl] = React.useState(null);
    const [state, setState] = useState({
        activeLang: getActiveLanguage(),
        initial: false,
    });

    const hA = hasActive(value);
    return (
        <Box display={'flex'} width={1}
             alignItems={'center'}
             style={{...style}}>
            <BaseTextField
                {...props}
                autoFocus={autoFocus}
                value={value[state.activeLang.key]}
                onTextChange={(text) => {
                    const newVal ={};
                    newVal[state.activeLang.key] = text;
                    onTextChange({
                        ...value,
                        ...newVal})
                }}
                style={{
                    ...textFieldStyle
                }}/>
            <Box pr={1} display={'flex'} flexDirection={'column'}>
                <Typography variant={'caption'} display={'flex'}
                            justifyContent={'center'}
                            style={{...UtilsStyle.disableTextSelection()}}>
                    {state.activeLang.shortLabel}
                </Typography>
                <IconButton size={"small"} onClick={(e) => {
                    setAnchorEl(e.currentTarget)
                }}>
                    <FiberManualRecord
                        fontSize={'small'}
                        style={{
                            color: hA === 1 ? green[300] : hA === -1 ? red[300] : orange[300]
                        }}/>
                </IconButton>
                <Menu
                    anchorEl={anchorEl}
                    keepMounted
                    open={Boolean(anchorEl)}
                    onClose={() => setAnchorEl(null)}>
                    {
                        Object.keys(sLang).map((keyName, index) => {
                            const l = sLang[keyName];
                            return (
                                <MenuItem
                                    key={index}
                                    onClick={() => {
                                        setAnchorEl(null)
                                        setState({
                                            ...state,
                                            activeLang: l
                                        })
                                    }}>
                                    <Box display={'flex'}>
                                        <FiberManualRecord
                                            fontSize={'small'}
                                            style={{
                                                color: hasActive(value, l) === 1 ? green[300] : red[300]
                                            }}/>
                                        <Typography variant={'caption'} display={'flex'}
                                                    pr={0.5}
                                                    justifyContent={'center'}
                                                    style={{...UtilsStyle.disableTextSelection()}}>
                                            {l.label}
                                        </Typography>
                                    </Box>
                                </MenuItem>
                            )
                        })
                    }
                </Menu>
            </Box>
        </Box>
    )
}


BaseTextFieldMultiLanguage.propTypes = {
    inputName: PropTypes.string,
    label: PropTypes.string,
    isLabelRtl: PropTypes.bool,
    value: PropTypes.object,
    pattern: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
    placeholder: PropTypes.string,
    isRequired: PropTypes.bool,
    isRequiredErrorMessage: PropTypes.string,
    optionalLabel: PropTypes.string,
    validatable: PropTypes.bool,
    hasValid: PropTypes.bool,
    hasError: PropTypes.bool,
    errorMessage: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
    errorPattern: PropTypes.oneOfType([PropTypes.func, PropTypes.string, PropTypes.array]),
    dir: PropTypes.oneOf(['ltr', 'rtl']),
    icon: PropTypes.object,
    loading: PropTypes.bool,
    disabled: PropTypes.bool,
    maxLength: PropTypes.number,
    multiline: PropTypes.bool,
    rows: PropTypes.number,
    type: PropTypes.oneOf(['text', 'password', 'number', 'date', 'email', 'file', 'search']),
    variant: PropTypes.oneOf(['standard', 'standardWhitOutButtonLine', 'outline', 'outlineV2']),
    onTextChange: PropTypes.func,
    onTextFocusChange: PropTypes.func,
    style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};
