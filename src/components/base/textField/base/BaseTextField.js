import React, {createRef, useEffect, useState} from 'react';
import {checkHasErrorPattern, checkPattern as checkerCheckPattern, isLtr} from "../../../../utils/Checker";
import makeStyles from "@material-ui/core/styles/makeStyles";
import PropTypes from 'prop-types';
import {dir as SiteDir, lang, theme} from "../../../../repository";
import _ from "lodash"
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControl from "@material-ui/core/FormControl";
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import InputAdornment from "@material-ui/core/InputAdornment";
import ThreeDots from "../../loading/treeDots/ThreeDots";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import {UtilsKeyboardKey} from "../../../../utils/Utils";
import {useTheme} from "@material-ui/styles";

const useStyles3 = makeStyles(theme => (
    {
        myInput: props => ({
            "&>label": {
                color: theme.palette.text.secondary,
                "&[data-shrink=true]": {
                    fontWeight: "200"
                },
            },
            "& .Mui-disabled": {
                backgroundColor: 'transparent !important'
            },
            '& div[class*="MuiOutlinedInput-root"]': {
                "& legend": {
                    textAlign: 'right'
                }
            },
            '& div[class*="MuiInput-formControl"]:before': {
                display: 'block'
            },
            '& div[class*="MuiInput-formControl"]:after': {
                display: 'block'
            },
            '& input': {
                color: theme.palette.text.primary,
                ...props.inputStyle,
                '&>div::before': {
                    borderColor: theme.palette.text.secondary,
                },
                '&>div': {
                    "&>fieldset": {
                        borderColor: props.borderColor ? props.borderColor : null
                    },
                    '&:hover>fieldset': {
                        borderColor: props.borderColor ? `${props.borderColor} !important` : null
                    }
                }
            },
            validateStyle: {
                '&[isvalide*="true"]': {
                    '&>label': {
                        color: theme.palette.success.main,
                    },
                    '&>div::before': {
                        borderColor: theme.palette.success.main,
                    },
                    "& fieldset": props => ({
                        borderColor: props.borderColor ? props.borderColor : 'unset'
                    }),
                },
            },
        })
    }));


const useRtlStyle = makeStyles({
    myInput: {
        "&>label": {
            transformOrigin: 'top right !important',
            right: 0,
            left: 'auto',
        }
    }
});

const useLtrStyle = makeStyles({
    myInput: {
        "&>label": {
            transformOrigin: 'top left !important',
            right: 'auto',
            left: 0,
            "&[data-shrink=true]": {},
        }
    }
});


const useStandardWhitOutButtonLineStyle = makeStyles({
    myInput: {
        "&>label": {
            '& div[class*="MuiInput-formControl"]:before': {
                display: 'none'
            },
            '& div[class*="MuiInput-formControl"]:after': {
                display: 'none'
            },
        }
    }
});

const useAllOutlineRtlStyle = makeStyles({
    myInput: {
        "&>label": {
            paddingRight: 5,
            paddingLeft: 0,
            transform: "translate(0px, 21px) scale(1)",
        }
    },
    validateStyle: {
        '&[isvalide*="true"]': {
            "& fieldset": {
                borderColor: theme.palette.success.main + " !important"
            },
        },
    },
});


const useAllOutlineLtrStyle = makeStyles({
    myInput: {
        "&>label": {
            paddingRight: 0,
            paddingLeft: 5,
            transform: "translate(0px, 21px) scale(1)",
        }
    }
});

const useOutlineStyle = makeStyles({
    myInput: {
        "&>label": {
            "&[data-shrink=true]": {
                transform: "translate(-14px, -0.5px) scale(0.75)",
            },
        }
    }
});

const useOutlineV2Style = makeStyles(theme => ({
    myInput: {
        marginTop: theme.spacing(6),
        "&>label": {
            "&[data-shrink=true]": {
                transform: "translate(0, -22.5px) scale(1)",
                fontWeight: "400"
            },
        }
    }
}));


export default function BaseTextField({
                                          onTextChange,
                                          onTextFocusChange,
                                          onTextChangeFinished,
                                          value = '',
                                          label,
                                          isLabelRtl,
                                          isRequiredErrorMessage,
                                          placeholder,
                                          isRequired,
                                          hasValid,
                                          validatable,
                                          pattern,
                                          errorMessage,
                                          hasError,
                                          typingWaitInterval = 1000,
                                          variant = "standard",
                                          optionalLabel,
                                          errorPattern,
                                          maxLength,
                                          loading,
                                          icon,
                                          multiline,
                                          rows,
                                          type,
                                          inputName,
                                          autoFocus = false,
                                          disabled,
                                          dir,
                                          style,
                                          inputRootStyle = {},
                                          inputStyle = {},
                                          className,
                                          borderColor,
                                          ...props
                                      }) {
    const theme = useTheme();
    const inputRef = createRef();
    const isStandard = variant === "standard" || variant === 'standardWhitOutButtonLine';
    const isOutline = variant === "outline";
    const isOutlineV2 = variant === "outlineV2";
    const [labelWidth, setLabelWidth] = React.useState(0);
    const [initialize, setInitialize] = React.useState(false);
    const [changeHandlerTimer, setChangeHandlerTimer] = React.useState(false);


    //region Props
    if (isLabelRtl === undefined) {
        // props = {
        //     ...props,
        // }
        isLabelRtl = SiteDir === "rtl"
    }
    if (isRequiredErrorMessage === undefined) {
        // props = {
        //     ...props,
        // }
        isRequiredErrorMessage = lang.get("er_fill_input")
    }
    //endregion Props

    //region Variable

    useEffect(() => {
        return
        if (isOutline || isOutlineV2)
            setLabelWidth(labelRef.current.offsetWidth + (labelRef.current.offsetWidth * 0.35));
    }, []);


    const renderVale = value;


    const labelRef = React.useRef(null);

    useEffect(() => {
        return;
        if (!initialize) {
            setInitialize(true);
            return
        }
        clearTimeout(changeHandlerTimer);
        setChangeHandlerTimer(setTimeout(() => {
            onChangeHandler(value, false);
        }), 1000);

    }, [value]);


    const [state, setState] = useState({
        inputDir: getDefaultDir(),
        hasError: null,
        hasErrorAttr: (_.isEmpty(value) && isRequired),
        hasValid: null,
        timer: null
    });


    function getDefaultDir() {
        return SiteDir
        if (!_.isEmpty(renderVale)) {
            return isLtr(renderVale) ? "ltr" : "rtl";
        }
        if (!_.isEmpty(placeholder)) {
            return isLtr(placeholder) ? "ltr" : "rtl";
        }
        return SiteDir;
    }

    useEffect(() => {
        return;
        if (value.length !== 1)
            return;

        let dir = null;
        if (type === "number" || type === "password" || type === "email") {
            dir = "ltr";
        }

        if (dir === null) {
            const d = (!_.isEmpty(value)) ? isLtr(value) : (!_.isEmpty(placeholder) ? isLtr(placeholder) : SiteDir === 'ltr');
            dir = d ? "ltr" : "rtl";
        }

        setState({
            ...state,
            inputDir: dir
        });
    }, [value]);

    //endregion Variable

    //region handler
    const onChange = event => {
        const text = event.target.value;
        onTextChange(text);
        //region patternCheck
        if ((maxLength && text.length > maxLength) || (pattern && !_.isEmpty(text) && !checkPattern(text)))
            return;
        //endregion patternCheck

        let newState = {
            ...state,
            hasError: null,
            hasValid: null
        };
        //region CheckError Only For Attr
        newState = {
            ...newState,
            hasErrorAttr: checkError(errorPattern, text)
        };
        //endregion CheckError Only For Attr
        if (onTextChange !== undefined) {
            onTextChange(text);
        }
        if (onTextChangeFinished !== undefined) {
            clearTimeout(state.timer);
            // requestNewListDebounce(value);
            newState = {
                ...newState,
                timer: setTimeout(() => onTextChangeFinished(text), typingWaitInterval)
            }
        }
        setState(newState);
    };

    function onChangeHandler(text, callParent = true) {

//region patternCheck
        if (pattern && !_.isEmpty(text) && !checkPattern(text))
            return;
        //endregion patternCheck
        let newState = {
            ...state,
            hasError: null,
            hasValid: null
        };
        //region maxLengthCheck
        if (maxLength) {
            if (text.length > maxLength) {
                text = text.slice(0, maxLength);
                return
            }
        }
        //endregion maxLengthCheck
        //region LtrOrRtl
        const ltr = (!_.isEmpty(text)) ? isLtr(text) : (!_.isEmpty(placeholder) ? isLtr(placeholder) : SiteDir === 'ltr');
        newState = {
            ...newState,
            inputDir: ltr ? "ltr" : 'rtl'
        };
        //endregion LtrOrRtl
        //region CheckError Only For Attr
        newState = {
            ...newState,
            hasErrorAttr: checkError(errorPattern, text)
        };
        //endregion CheckError Only For Attr
        setState(newState);
        if (callParent && undefined !== onTextChange)
            onTextChange(text);
    }

    const onFocusChange = event => {
        return
        try {
            const text = event.target.value;
            let newState = {...state};
            //region ErrorCheck
            const checkErrorCo = checkError(errorPattern, text);
            newState = {
                ...newState,
                hasError: checkErrorCo,
                hasValid: !checkErrorCo
            };
            //endregion ErrorCheck
            setState(newState);
            if (onTextFocusChange !== undefined)
                onTextFocusChange(event.target.value)
        } catch (e) {
        }
    };

    function handleKeyDown(e) {
        return
        if (onTextChangeFinished && e.keyCode === UtilsKeyboardKey.ENTER_KEY) {
            clearTimeout(state.timer);
            onTextChangeFinished(renderVale)
        }
    }

    function onFocusChangeHandler(text, callParent = true) {

        try {
            let newState = {...state};
            //region ErrorCheck
            const checkErrorCo = checkError(errorPattern, text);
            newState = {
                ...newState,
                hasError: checkErrorCo,
                hasValid: !checkErrorCo
            };
            //endregion ErrorCheck
            setState(newState);
            if (callParent && onTextFocusChange !== undefined)
                onTextFocusChange(text)
        } catch (e) {
        }
    }

    //region handlerHelper

    /**
     *  Check Error
     *
     * pattern can regex or function or list of regex and functions
     * text for check pattern
     *
     *
     *
     * NOTE: Text Pattern first check pattern has available then check if pattern is regex check or pattern has function run function
     *
     */
    function checkError(pattern, text) {
        return false
        if (_.isEmpty(text)) {
            if (isRequired)
                return -1;
            return state.hasError
        }
        if (hasError === undefined && pattern) {
            const check = checkHasErrorPattern(pattern, text);
            return check !== -1 ? check : null
        }
        return state.hasError
    }


    /**
     *  Check Pattern
     *
     * text for check pattern
     *
     *
     * NOTE: Text Pattern first check pattern has available then check if pattern is regex check or pattern has function run function
     *
     */
    function checkPattern(text) {
        return true
        try {
            return pattern && checkerCheckPattern(pattern, text)
        } catch (e) {
            console.error("BaseTextField::checkPattern", e)
        }
        return true
    }

    //endregion handlerHelper

    //endregion handler

    //region ClassName

    const classes = useStyles3({
        inputStyle: inputStyle,
        isLabelRtl: isLabelRtl,
        isStandard: isStandard,
        isStandardWhitOutButtonLine: variant === 'standardWhitOutButtonLine',
        isOutline: isOutline,
        isOutlineV2: isOutlineV2,
        borderColor: borderColor,
    });
    const rtlClasses = useRtlStyle();
    const ltrClasses = useLtrStyle();
    const standardWhitOutButtonLineClasses = useStandardWhitOutButtonLineStyle();
    const allOutlineRtlClasses = useAllOutlineRtlStyle();
    const allOutlineLtrClasses = useAllOutlineLtrStyle();
    const outlineClasses = useOutlineStyle();
    const outlineV2Classes = useOutlineV2Style();

    const classesName = [classes.myInput];

    if (isLabelRtl)
        classesName.push(isLabelRtl ? rtlClasses.myInput : ltrClasses.myInput);
    if (isOutline || isOutlineV2) {
        classesName.push(isLabelRtl ? allOutlineRtlClasses.myInput : allOutlineLtrClasses.myInput);
        classesName.push(isOutline ? outlineClasses.myInput : outlineV2Classes.myInput);
    } else {
        classesName.push(standardWhitOutButtonLineClasses.myInput);

    }

    if (className) {
        classesName.push(className)
    }
    if (hasValid || (!_.isEmpty(renderVale) && validatable && !hasErrorFun())) {
        classesName.push(classes.validateStyle)
    }
    let elemntProp = {};
    if (renderVale !== undefined) {
        elemntProp = {
            ...elemntProp,
            value: renderVale,
        }
    }
    // if (maxLength) {
    //     elemntProp = {
    //         ...elemntProp,
    //         inputProps: {
    //             ...elemntProp.inputProps,
    //             maxLength: maxLength,
    //         }
    //     }
    // }
    //endregion ClassName

    //region Functions
    function hasErrorFun() {
        return !(disabled || loading) && (hasError || (hasError === undefined && state.hasError !== null))
    }

    function hasValidFun() {
        return (disabled || loading) && (hasValid || (hasValid === undefined && state.hasValid !== null))
    }

    function getErrorMessage() {
        if (state.hasError !== -1) {
            if (_.isString(errorMessage)) {
                return errorMessage
            }
            if (_.isArray(errorMessage)) {
                return errorMessage[state.hasError]
            }
        }
        return isRequiredErrorMessage
    }

    function getInputDir() {
        return state.inputDir
    }

    //endregion Functions


    return (
            <FormControl
                         error={hasErrorFun()}
                         isvalide={state.hasValid ? 'true' : 'false'}
                         {...((state.hasErrorAttr) ? {haserror: ""} : null)}
                         disabled={disabled || loading}
                         variant={isStandard ? "standard" : "outlined"}
                         style={{
                             ...style,
                             paddingTop: 5
                         }}>
                {
                    <InputLabel ref={labelRef}
                                htmlFor={isStandard ? "component-standard" : "component-outlined"}>{label}{(optionalLabel !== "" && !isRequired) &&
                    <span style={{
                        fontSize: '0.7rem',
                        top: -4
                    }}>
                {(optionalLabel) ? optionalLabel : ' (' + lang.get('optional') + ')'}</span>}
                    </InputLabel>
                }
                {
                    isStandard ? (
                        <Input
                            name={inputName ? inputName : label}
                            {...elemntProp}
                            dir={getInputDir()}
                            onChange={onChange}
                            onKeyDown={handleKeyDown}
                            type={type}
                            value={renderVale}
                            autoFocus={autoFocus}
                            variant="outlined"
                            inputRef={inputRef}
                            onBlur={onFocusChange}
                            placeholder={placeholder}
                            multiline={multiline}
                            rows={rows}
                            startAdornment={
                                loading ? (
                                    <InputAdornment position="start">
                                        <ThreeDots/>
                                    </InputAdornment>
                                ) : null
                            }
                            error={hasErrorFun()}/>
                    ) : (isOutline || isOutlineV2) ? (
                        <OutlinedInput
                            name={inputName ? inputName : label}
                            {...elemntProp}
                            dir={getInputDir()}
                            onChange={onChange}
                            onKeyDown={handleKeyDown}
                            type={type}
                            value={renderVale}
                            autoFocus={autoFocus}
                            variant="outlined"
                            inputRef={inputRef}
                            onBlur={onFocusChange}
                            placeholder={placeholder}
                            multiline={multiline}
                            labelWidth={isOutline ? labelWidth : 0}
                            rows={rows}
                            startAdornment={
                                loading ? (
                                    <InputAdornment position="start">
                                        <ThreeDots/>
                                    </InputAdornment>
                                ) : null
                            }
                            error={hasErrorFun()}/>
                    ) : null
                }
                {
                    (hasErrorFun()) &&
                    <FormHelperText>{getErrorMessage()}</FormHelperText>
                }
            </FormControl>
    )
}

BaseTextField.defaultProps = {
    label: '',
    icon: {},
    type: 'text',
};

BaseTextField.propTypes = {
    inputName: PropTypes.string,
    label: PropTypes.string,
    isLabelRtl: PropTypes.bool,
    value: PropTypes.string,
    pattern: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
    placeholder: PropTypes.string,
    isRequired: PropTypes.bool,
    isRequiredErrorMessage: PropTypes.string,
    optionalLabel: PropTypes.string,
    validatable: PropTypes.bool,
    hasValid: PropTypes.bool,
    hasError: PropTypes.bool,
    errorMessage: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
    errorPattern: PropTypes.oneOfType([PropTypes.func, PropTypes.string, PropTypes.array]),
    dir: PropTypes.oneOf(['ltr', 'rtl']),
    icon: PropTypes.object,
    loading: PropTypes.bool,
    disabled: PropTypes.bool,
    maxLength: PropTypes.number,
    multiline: PropTypes.bool,
    rows: PropTypes.number,
    type: PropTypes.oneOf(['text', 'password', 'number', 'date', 'email', 'file', 'search']),
    variant: PropTypes.oneOf(['standard', 'standardWhitOutButtonLine', 'outline', 'outlineV2']),
    onTextChange: PropTypes.func,
    onTextFocusChange: PropTypes.func,
    style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};
