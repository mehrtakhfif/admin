import React from 'react';
import PropTypes from 'prop-types';
import {lang} from "../../../../repository";
import _ from "lodash"
import BaseTextField from "./BaseTextField";

export default function AddressTextField(props) {
    let newProps = props;
    if (_.isEmpty(props.label)) {
        newProps = {
            ...newProps,
            label: lang.get("post_address")
        }
    }
    if (_.isEmpty(props.placeholder)) {
        newProps = {
            ...newProps,
            placeholder: lang.get('post_address_placeholder')
        }
    }
    if (_.isEmpty(props.errorMessage)) {
        newProps = {
            ...newProps,
            errorMessage: lang.get('er_address_is_to_short')
        }
    }

    return (
        <BaseTextField
            {...newProps}
            pattern={props.pattern}
            multiline={true}/>
    )
}

AddressTextField.defaultProps = {
    label: '',
    placeholder: '',
    icon: {},
    type: 'text',
    pattern: '^((\\S)+[ ]?[\\n]*)*$',
    errorPattern: '(([^ -_]+)[\\n -]+){2}.+',
    validatable: false,
    rows: 4,
};

AddressTextField.propTypes = {
    inputName: PropTypes.string,
    label: PropTypes.string,
    isLabelRtl: PropTypes.bool,
    value: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    rows: PropTypes.number,
    validatable: PropTypes.bool,
    hasValid: PropTypes.bool,
    isRequired: PropTypes.bool,
    isRequiredErrorMessage: PropTypes.string,
    hasError: PropTypes.bool,
    errorMessage: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
    dir: PropTypes.oneOf(['ltr', 'rtl']),
    icon: PropTypes.object,
    variant: PropTypes.oneOf(['standard', 'outline', 'outlineV2']),
    loading: PropTypes.bool,
    disabled: PropTypes.bool,
    maxLength: PropTypes.number,
    onTextChange: PropTypes.func,
    onTextFocusChange: PropTypes.func,
    style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};
