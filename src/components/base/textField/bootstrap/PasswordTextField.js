import React, {useState} from 'react';
import {lang} from "../../../../repository";
import BaseTextField from "./BaseTextField";

export function verify(text) {
    return !(text.length > 0 && text.length < 6)
}

export default function PasswordTextField({
                                              onTextChange = null,
                                              onTextFocusChange = null,
                                              defaultPassword = "",
                                              title = lang.get("password"),
                                              hint = lang.get("password_hint"),
                                              errorMessage = lang.get("er_password"),
                                              disabled = false,
                                              style = {}
                                          }) {

    const [password, setPassword] = useState(defaultPassword);
    const [error, setError] = useState(false);
    const onChange = text => {
        setPassword(text);
        if (null !== onTextChange)
            onTextChange(text);
        setError(false);
    };

    const onFocusChange = text => {
        setError(!verify(text));
        if (null !== onTextFocusChange)
            onTextFocusChange(text)
    };

    return (
        <BaseTextField text={password}
                       onTextChange={onChange}
                       onTextFocusChange={onFocusChange}
                       hasError={error}
                       title={title}
                       type='password'
                       dir="ltr"
                       inputName="password"
                       errorMessage={errorMessage}
                       hint={hint}
                       disabled={disabled}
                       style={style}>
        </BaseTextField>
    )
}
