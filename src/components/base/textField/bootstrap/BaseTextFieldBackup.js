import React, {useState} from 'react';
import InputLabel from "@material-ui/core/InputLabel";
import {fade, withStyles} from '@material-ui/core/styles';
import InputBase from '@material-ui/core/InputBase';
import FormControl from '@material-ui/core/FormControl';
import Typography from "@material-ui/core/Typography";
import createMuiTheme from "@material-ui/core/styles/createMuiTheme";
import {isLtr} from "../../../../utils/Checker";
import {UtilsStyle} from "../../../../utils/Utils";

const BootstrapInput = withStyles(theme => ({
    root: {
        width: "100%",
        'label + &': {
            marginTop: theme.spacing(3),
        },
    },
    input: {
        direction: 'inherit',
        ...UtilsStyle.borderRadius(4),
        position: 'relative',
        backgroundColor: theme.palette.common.white,
        border: '1px solid #ced4da',
        width: '100%',
        padding: '10px 12px',
        transition: theme.transitions.create(['border-color', 'box-shadow']),
        '&:focus': {
            boxShadow: `${fade(theme.palette.primary.main, 0.25)} 0 0 0 0.2rem`,
            borderColor: theme.palette.primary.main,
        },
    },
    error: {
        '& input': {
            boxShadow: `${fade(theme.palette.error.main, 0.25)} 0 0 0 0.2rem`,
            borderColor: theme.palette.error.main,
        }
    }
}))(InputBase);

export default function BaseTextField({
                                          onTextChange = null,
                                          onTextFocusChange = null,
                                          text = "",
                                          title = "",
                                          hint = "",
                                          errorMessage = null,
                                          hasError = false,
                                          type = "text"
                                      }) {
    let theme = createMuiTheme();
    const [ltr, setLtr] = useState(setDefaultDir());

    function setDefaultDir() {
        if (text !== "") {
            return isLtr(text);
        }
        if (hint !== "") {
            return isLtr(hint);
        }
        return true;
    }

    const onChange = event => {
        const text = event.target.value;
        const ltr = (text.length > 0) ? isLtr(text) : isLtr(hint);
        setLtr(ltr);
        if (null !== onTextChange)
            onTextChange(text);
    };

    const onFocusChange = event => {
        if (onTextFocusChange !== null)
            onTextFocusChange(event.target.value)
    };

    return (
        <FormControl style={{width: "100%", margin: "0 auto"}}>
            <InputLabel shrink htmlFor="bootstrap-input"
                        component='label'
                        style={{left: isLtr(title) ? "0" : "auto", color: "#656565"}}>
                <Typography variant="h5">
                    {title}
                </Typography>
            </InputLabel>
            <BootstrapInput error={hasError}
                            classes={{error: 'error'}}
                            value={text}
                            onChange={onChange}
                            onBlur={onFocusChange}
                            type={type}
                            placeholder={hint}
                            style={{
                                direction: ltr ? "ltr" : "rtl",
                            }}/>
            {
                (hasError && errorMessage) && (
                    <Typography
                        style={{
                            marginTop: theme.spacing(1),
                            color: theme.palette.error.main
                        }}
                        variant="body2"
                        align="center">
                        {errorMessage}
                    </Typography>
                )
            }
        </FormControl>
    )
}
