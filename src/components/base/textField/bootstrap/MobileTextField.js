import React, {useState} from 'react';
import {lang} from "../../../../repository";
import {isMobileNumber} from '../../../../utils/Checker'
import BaseTextField from "./BaseTextField";

export function verify(text) {
    return (text.length > 0 && isMobileNumber(text))
}

export default function MobileTextField({
                                            onTextChange = null,
                                            onTextFocusChange = null,
                                            defaultMobileNumber = "",
                                            title = lang.get("mobile_number"),
                                            hint = "09*********",
                                            errorMessage = lang.get("er_mobile_number"),
                                            inputName = "phone",
                                            disabled = false,
                                            style = {}
                                        }) {
    const [mobile, setMobile] = useState(defaultMobileNumber);
    const [error, setError] = useState(false);

    const onChange = text => {
        if (text.length <= 11) {
            setMobile(text);
            if (null !== onTextChange)
                onTextChange(text)
        }
        setError(false);
    };

    const onFocusChange = text => {
        if (text.length > 0)
            setError(!isMobileNumber(text));
        if (null !== onTextFocusChange)
            onTextFocusChange(text)
    };

    return (
        <BaseTextField text={mobile}
                       onTextChange={onChange}
                       onTextFocusChange={onFocusChange}
                       hasError={error}
                       title={title}
                       type="number"
                       inputName={inputName}
                       errorMessage={errorMessage}
                       hint={hint}
                       disabled={disabled}
                       style={style}/>
    )
}
