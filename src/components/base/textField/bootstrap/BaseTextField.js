import React, {useState} from 'react';
import {fade, withStyles} from '@material-ui/core/styles';
import InputBase from '@material-ui/core/InputBase';
import {isLtr} from "../../../../utils/Checker";
import makeStyles from "@material-ui/core/styles/makeStyles";
import PropTypes from 'prop-types';
import Box from "@material-ui/core/Box";
import {UtilsStyle} from "../../../../utils/Utils";
import {theme} from "../../../../repository";

const BootstrapInput = withStyles(theme => ({
    root: {
        width: "100%",
        'label + &': {
            marginTop: theme.spacing(3),
        },
    },
    input: {
        direction: 'inherit',
        ...UtilsStyle.borderRadius(4),
        position: 'relative',
        backgroundColor: theme.palette.common.white,
        border: '1px solid #ced4da',
        width: '100%',
        padding: '10px 12px',
        transition: theme.transitions.create(['border-color', 'box-shadow']),
        '&:focus': {
            boxShadow: `${fade(theme.palette.primary.main, 0.25)} 0 0 0 0.2rem`,
            borderColor: theme.palette.primary.main,
        },

    },
    error: {
        '& input': {
            boxShadow: `${fade(theme.palette.error.main, 0.25)} 0 0 0 0.2rem`,
            borderColor: theme.palette.error.main,
        }
    }
}))(InputBase);

export default function BaseTextField(props) {
    const {onTextChange, onTextFocusChange, text, title, hint, errorMessage, hasError, icon, type, inputName, disabled, dir, style} = props;

    const useStyles = makeStyles(theme => ({
        input: {
            '& input': {
                "&::-webkit-input-placeholder": {
                    direction: isLtr(hint) ? "ltr" : "rtl",
                },
                "&:-ms-input-placeholder": {
                    direction: isLtr(hint) ? "ltr" : "rtl",
                },
                "&:-moz-placeholder": {
                    direction: isLtr(hint) ? "ltr" : "rtl",
                }
            }
        }
    }));
    const classes = useStyles();

    const [ltr, setLtr] = useState(setDefaultDir());

    function setDefaultDir() {
        if (text !== "") {
            return isLtr(text);
        }
        if (hint !== "") {
            return isLtr(hint);
        }
        return true;
    }

    const onChange = event => {
        const text = event.target.value;
        const ltr = (text.length > 0) ? isLtr(text) : isLtr(hint);
        setLtr(ltr);
        if (null !== onTextChange)
            onTextChange(text);
    };

    const onFocusChange = event => {
        if (onTextFocusChange !== null)
            onTextFocusChange(event.target.value)
    };

    return (
        <div style={style}>
            <Box
                fontSize='subtitle1.fontSize'
                component="h5"
                style={{paddingBottom: theme.spacing(1)}}>
                {title}
            </Box>
            <BootstrapInput error={hasError}
                            classes={{error: 'error'}}
                            className={classes.input}
                            value={text}
                            onChange={onChange}
                            onBlur={onFocusChange}
                            type={type}
                            placeholder={hint}
                            disabled={disabled}
                            name={inputName ? inputName : text}
                            style={{
                                direction: dir ? dir : ltr ? "ltr" : "rtl",
                            }}/>
            {
                <Box
                    style={{
                        marginTop: theme.spacing(1),
                        color: theme.palette.error.main,
                        display: (hasError && errorMessage) ? "block" : "none"
                    }}
                    fontSize="caption.fontSize"
                    component='span'
                    align={isLtr(errorMessage) ? "left" : "right"}>
                    {errorMessage}
                </Box>
            }
        </div>
    )
}


BaseTextField.defaultProps = {
    text: '',
    title: '',
    hint: '',
    icon: {},
    type: 'text',
};

BaseTextField.propTypes = {
    onTextChange: PropTypes.func,
    onTextFocusChange: PropTypes.func,
    text: PropTypes.string,
    placeholder: PropTypes.string,
    errorMessage: PropTypes.string,
    hasError: PropTypes.bool,
    icon: PropTypes.object,
    type: PropTypes.oneOf(['text', 'password', 'number', 'date', 'email', 'file', 'search']),
    inputName: PropTypes.string,
    dir: PropTypes.oneOf(['ltr', 'rtl']),
    style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};
