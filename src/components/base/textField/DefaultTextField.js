import React from "react";
import TextField from "./TextField";
import TextFieldContainer from "./TextFieldContainer";
import Box from "@material-ui/core/Box";


export default function DefaultTextField({innerRef,name, defaultValue=null, variant, label, multiline, rows, rowsMax, required, type,onFocusOut, textFieldProps = {}, ...props}) {

    return (
        <TextFieldContainer
            name={name}
            defaultValue={defaultValue}
            onChangeDelay={600}
            type={type}
            render={(ref, {props}) => (
                <TextField
                    {...props}
                    variant={variant}
                    inputRef={ref}
                    name={name}
                    multiline={multiline}
                    rows={multiline ? rows : undefined}
                    rowsMax={multiline ? rowsMax : undefined}
                    fullWidth={true}
                    label={label}
                    required={required}
                    onFocusOut={onFocusOut}
                    {...textFieldProps}
                    style={{
                        ...textFieldProps.style
                    }}/>
            )}
            {...props}
            {...props.containerProps}
        />
    )
}
