import React, {useRef, useState} from "react";
import PropTypes from "prop-types";
import {getActiveLanguage, sLang} from "../../../repository";
import Box from "@material-ui/core/Box";
import {FiberManualRecord} from "@material-ui/icons";
import IconButton from "@material-ui/core/IconButton";
import _ from 'lodash';
import {green, orange, red} from "@material-ui/core/colors";
import Typography from "../Typography";
import {UtilsStyle} from "../../../utils/Utils";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import TextFieldContainer from "./TextFieldContainer";
import {hasActive as hasHasActive} from '../multiLanguageContainer/MultiLanguageContainer'


function hasActive(ref, lang) {
    if (!ref.current)
        return;
    const value = {};
    _.forEach(ref.current.getElementsByTagName('input'), function (el) {
        value[el.lang] = el.value;
    });
    return hasHasActive(value, lang)
}

export function createName({group, array, name, langKey}) {
    if (group) {
        return `${group}~~~${createName({array: array, name: name, langKey: langKey})}~~~`
    }
    let n = array ? `${array}___${name}` : name;
    if (langKey)
        n = n + "-lang-" + langKey;
    return n + (array ? '___' : "")
}

export function createInputProps({langKey}) {
    return {
        lang: langKey,
    }
}


export default function TextFieldMultiLanguageContainer({
                                                            group,
                                                            array,
                                                            name,
                                                            actived,
                                                            errorPatterns = [],
                                                            renderGlobalErrorText,
                                                            render,
                                                            checkInterval = 1000,
                                                            style,
                                                            ...props
                                                        }) {
    const [anchorEl, setAnchorEl] = React.useState(null);
    const [state, setState] = useState({
        activeLang: getActiveLanguage(),
        initial: false,
    });
    const ref = useRef();

    const hA = hasActive(ref);
    return (
        <Box ref={ref} display={'flex'} width={1}
             alignItems={'center'}
             style={{...style}}>
            {Object.keys(sLang).map((langKey) => {
                const lang = sLang[langKey];
                return (
                    <TextFieldContainer
                        key={langKey}
                        name={createName({group: group, array: array, name: name, langKey: lang.key})}
                        errorPatterns={errorPatterns}
                        renderGlobalErrorText={renderGlobalErrorText}
                        checkInterval={checkInterval}
                        active={actived}
                        render={(ref, props) => {
                            return render(ref, {
                                ...props,
                                lang: lang,
                                inputProps: {
                                    ...props.inputProps,
                                    ...createInputProps({langKey: lang.key})
                                },
                                style: {
                                    ...props.style,
                                    display: state.activeLang.key !== lang.key ? "none" : null,
                                },
                                props: {
                                    ...props.props,
                                    containerProps: {
                                        style: {
                                            display: state.activeLang.key !== lang.key ? "none" : null,
                                        }
                                    },
                                }
                            })
                        }}/>
                )
            })}
            <Box pl={1} display={'flex'} flexDirection={'column'}>
                <Typography variant={'caption'} display={'flex'}
                            justifyContent={'center'}
                            style={{...UtilsStyle.disableTextSelection()}}>
                    {state.activeLang.shortLabel}
                </Typography>
                <IconButton size={"small"} onClick={(e) => {
                    setAnchorEl(e.currentTarget)
                }}>
                    <FiberManualRecord
                        fontSize={'small'}
                        style={{
                            color: hA === 1 ? green[300] : hA === -1 ? red[300] : orange[300]
                        }}/>
                </IconButton>
                <Menu
                    anchorEl={anchorEl}
                    keepMounted
                    open={Boolean(anchorEl)}
                    onClose={() => setAnchorEl(null)}>
                    {
                        Object.keys(sLang).map((keyName, index) => {
                            const l = sLang[keyName];
                            return (
                                <MenuItem
                                    key={index}
                                    onClick={() => {
                                        setAnchorEl(null)
                                        setState({
                                            ...state,
                                            activeLang: l
                                        })
                                    }}>
                                    <Box display={'flex'}>
                                        <FiberManualRecord
                                            fontSize={'small'}
                                            style={{
                                                color: hasActive(ref, l) === 1 ? green[300] : red[300]
                                            }}/>
                                        <Typography variant={'caption'} display={'flex'}
                                                    pr={0.5}
                                                    justifyContent={'center'}
                                                    style={{...UtilsStyle.disableTextSelection()}}>
                                            {l.label}
                                        </Typography>
                                    </Box>
                                </MenuItem>
                            )
                        })
                    }
                </Menu>
            </Box>
        </Box>
    )
}


TextFieldMultiLanguageContainer.propTypes = {
    inputName: PropTypes.string,
    label: PropTypes.string,
    isLabelRtl: PropTypes.bool,
    value: PropTypes.object,
    pattern: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
    placeholder: PropTypes.string,
    isRequired: PropTypes.bool,
    isRequiredErrorMessage: PropTypes.string,
    optionalLabel: PropTypes.string,
    validatable: PropTypes.bool,
    hasValid: PropTypes.bool,
    hasError: PropTypes.bool,
    errorMessage: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
    errorPattern: PropTypes.oneOfType([PropTypes.func, PropTypes.string, PropTypes.array]),
    dir: PropTypes.oneOf(['ltr', 'rtl']),
    icon: PropTypes.object,
    loading: PropTypes.bool,
    disabled: PropTypes.bool,
    maxLength: PropTypes.number,
    multiline: PropTypes.bool,
    rows: PropTypes.number,
    type: PropTypes.oneOf(['text', 'password', 'number', 'date', 'email', 'file', 'search']),
    variant: PropTypes.oneOf(['standard', 'standardWhitOutButtonLine', 'outline', 'outlineV2']),
    onTextChange: PropTypes.func,
    onTextFocusChange: PropTypes.func,
    style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};
