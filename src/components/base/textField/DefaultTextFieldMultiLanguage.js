import React from "react";
import TextField from "./TextField";
import TextFieldContainer, {errorList} from "./TextFieldContainer";
import {siteLang} from "../../../repository";
import TextFieldMultiLanguageContainer from "./TextFieldMultiLanguageContainer";
import {getSafe} from "../../../utils/ObjectUtils";


export default function DefaultTextFieldMultiLanguage ({group, name, defaultValue, label,onChange,style, variant, required,onFocusOut, multiline = false, rows, rowsMax = 3, containerProps = {}, textFieldProps = {}, ...props}) {


    return (
        <TextFieldMultiLanguageContainer
            group={group}
            name={name}
            render={(ref, {lang: inputLang, name: inputName, initialize, valid, errorIndex, setValue, inputProps, style, props}) => {
                return (
                    <TextField
                        {...props}
                        onChange={onChange}
                        error={!valid}
                        name={inputName}
                        multiline={multiline}
                        onFocusOut={onFocusOut}
                        rows={multiline ? rows : undefined}
                        rowsMax={multiline ? rowsMax : undefined}
                        defaultValue={getSafe(()=>defaultValue[inputLang.key],"")}
                        fullWidth
                        helperText={errorList[errorIndex]}
                        inputRef={ref}
                        required={required && inputLang.key === siteLang}
                        label={label}
                        style={style}
                        inputProps={{
                            ...inputProps
                        }}/>
                )
            }}/>
    )
}
