import _ from "lodash";
import {createName} from "./TextFieldMultiLanguageContainer";
import TextFieldContainer, {errorList} from "./TextFieldContainer";
import React from "react";
import TextField from "./TextField";
import ReactNumberFormat from "react-number-format";

const phoneErrorList = [];
export default function ShabaTextField({group, name, defaultValue, required, label = "شماره شبا", isMobile: isM, onChange, ...props}) {
    let mobileRef = null;


    return (
        <TextFieldContainer
            defaultValue={defaultValue ? _.split(defaultValue, "IR")[1] : ''}
            name={createName({group: group, name: name})}
            type={'text'}
            onChangeDelay={undefined}
            errorPatterns={phoneErrorList}
            returnValue={(value) => {
                return value.trimAll()
            }}
            render={(ref, {name, initialize, valid, errorIndex, inputProps, setValue, props}) => {
                mobileRef = ref;
                return (
                    <TextField
                        {...props}
                        error={!valid}
                        variant="outlined"
                        name={name}
                        inputRef={ref}
                        fullWidth
                        helperText={errorList[errorIndex]}
                        required={required}
                        label={label}
                        placeholder={'IR 5505676***** **** **2 98001'}
                        onChange={(e) => {

                        }}
                        InputLabelProps={{
                            shrink: (ref && ref.current && ref.current.value) ? true : undefined,
                        }}
                        InputProps={{
                            inputComponent: MobileFormat,
                        }}

                    />
                )
            }}/>
    )
}

function MobileFormat(props) {
    const {inputRef, onChange, ...other} = props;
    return (
        <ReactNumberFormat
            {...other}
            getInputRef={inputRef}
            thousandSeparator
            isNumericString
            prefix="IR "
            format="IR ############ #### ### #####" mask=" _"
            style={{
                direction: 'ltr'
            }}
        />
    );
}
