import TextFieldContainer from "./TextFieldContainer";
import React from "react";
import TextField from "./TextField";


export default function ({name,defaultValue,containerProps={},textFieldProps={},...props}) {
    return(
        <TextFieldContainer
            name={name}
            defaultValue={defaultValue}
            render={(ref, {props}) => (
                <TextField
                    {...props}
                    inputRef={ref}
                    name={name}
                    {...textFieldProps}
                    style={{
                        display: 'none',
                        ...textFieldProps.style
                    }}/>
            )}
            {...containerProps}
        />
    )
}
