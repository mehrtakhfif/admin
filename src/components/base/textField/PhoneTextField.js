import _ from "lodash";
import {createName} from "./TextFieldMultiLanguageContainer";
import TextFieldContainer from "./TextFieldContainer";
import React from "react";
import TextField from "./TextField";
import ReactNumberFormat from "react-number-format";

const phoneErrorList = [];
export default function ({group, name, defaultValue, required, label="شماره موبایل ", onChange, ...props}) {
    let mobileRef = null;

    return (
            <TextFieldContainer
                defaultValue={_.toString(defaultValue)}
                name={createName({group: group, name: name})}
                type={'text'}
                onChangeDelay={undefined}
                errorPatterns={phoneErrorList}
                returnValue={(value) => {
                    return value.trimAll()
                }}
                render={(ref, {name, initialize, valid, errorText, inputProps, setValue, props}) => {
                    mobileRef = ref;
                    return (
                        <TextField
                            error={!valid}
                            variant="outlined"
                            helperText={errorText}
                            name={name}
                            inputRef={ref}
                            fullWidth
                            placeholder={"0900 000 0000"}
                            required={required}
                            defaultValue={props.defaultValue}
                            label={label}
                            onChange={(e) => {

                            }}
                            InputLabelProps={{
                                shrink: (ref && ref.current && ref.current.value) ? true : undefined,
                            }}
                            InputProps={{
                                inputComponent: MobileFormat,
                            }}

                        />
                    )
                }}/>
    )
}

function PhoneFormat(props) {
    const {inputRef, onChange, ...other} = props;
    console.log("pppppppppppsfafa", props)
    return (
        <ReactNumberFormat
            {...other}
            getInputRef={inputRef}
            thousandSeparator
            isNumericString
            format="### ########" mask="_"
            style={{
                direction: 'ltr'
            }}
        />
    );
}

function MobileFormat(props) {
    const {inputRef, onChange, ...other} = props;
    return (
        <ReactNumberFormat
            {...other}
            getInputRef={inputRef}
            thousandSeparator
            isNumericString
            format="#### ### ####" mask=" _"
            style={{
                direction: 'ltr'
            }}
        />
    );
}
