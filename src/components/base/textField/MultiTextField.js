import React from "react";
import TextFieldContainer from "./TextFieldContainer";
import TextField from "./TextField";
import {createName} from "./TextFieldMultiLanguageContainer";


export default function ({rows=4,group, name, defaultValue, required, label,variant="outlined",placeholder, onChange, ...props}) {

    return (
        <TextFieldContainer
            defaultValue={defaultValue}
            name={createName({group: group, name: name})}
            type={'text'}
            onChangeDelay={undefined}
            render={(ref, {name, initialize, valid, errorText, inputProps, setValue, props}) => (
                <TextField
                    error={!valid}
                    variant={variant}
                    helperText={errorText}
                    name={name}
                    inputRef={ref}
                    fullWidth
                    multiline
                    rows={rows}
                    placeholder={placeholder}
                    required={required}
                    defaultValue={props.defaultValue}
                    label={label}
                />
            )}/>
    )
}
