import React, {useEffect, useRef, useState} from "react";
import {Editor} from "react-draft-wysiwyg";
import {makeStyles} from "@material-ui/styles";
import Box from "@material-ui/core/Box";
import htmlToDraft from 'html-to-draftjs';
import {ContentState, convertToRaw, EditorState} from 'draft-js';
import draftToHtml from 'draftjs-to-html';
import TextField from "@material-ui/core/TextField";
import TextFieldContainer from "../TextFieldContainer";
import {tryIt} from "../../../../utils/ObjectUtils";

const useStyles = makeStyles(theme => ({
    editorRoot: (({minHeight}) => ({
        '&>div.rdw-editor-wrapper': {
            display: 'flex',
            flexDirection: 'column',
            '&>.toolbarClassName': {
                direction: 'rtl',
                paddingRight: 100,
            },
            '&>.editorClassName': {
                minHeight: minHeight,
                background: '#fff',
                padding: theme.spacing(1)
            }
        }
    }))
}));

export default function ({name, value, minHeight = 300, ...props}) {
    const classes = useStyles({minHeight: minHeight});
    const [state, setState] = useState({
        uploadedImages: []
    });
    const [html, setHtml] = useState();
    const [timer, setTimer] = useState();
    const ref = useRef();

    useEffect(() => {
        setHtml(htmlToEditorContent(value))
    }, [value]);


    return (
        <Box className={classes.editorRoot}{...props}
             style={{...props.style, color: "#000",overflow:"hidden"}}>
            <TextFieldContainer
                name={name}
                render={(ref, {name}) => {
                    useEffect(() => {
                        clearTimeout(timer);
                        try {
                            setTimer(setTimeout(() => {
                                tryIt(() => {
                                    ref.current.setValue(editorContentToHtml(html));
                                })
                            }, 3000))
                        } catch (e) {
                        }
                        return () => {
                            clearTimeout(timer)
                        }
                    }, [html]);
                    return (
                        <TextField name={name} inputRef={ref} style={{display: 'none'}}/>
                    )
                }}/>
            <Editor
                editorState={html}
                toolbarClassName="toolbarClassName"
                wrapperClassName="wrapperClassName"
                editorClassName="editorClassName"
                onEditorStateChange={(h) => setHtml(h)}
                toolbar={{
                    image: {
                        previewImage: true,
                        uploadCallback: (file) => {
                            // long story short, every time we upload an image, we
                            // need to save it to the state so we can get it's data
                            // later when we decide what to do with it.

                            // Make sure you have a uploadImages: [] as your default state
                            let uploadedImages = state.uploadedImages;

                            const imageObject = {
                                file: file,
                                localSrc: URL.createObjectURL(file),
                            };

                            uploadedImages.push(imageObject);

                            setState({
                                ...state,
                                imageObject
                            });


                            return new Promise(
                                (resolve, reject) => {
                                    resolve({data: {link: 'http://localhost:3000/static/media/box4.cb3abcff.png'}});
                                }
                            );


                            // We need to return a promise with the image src
                            // the img src we will use here will be what's needed
                            // to preview it in the browser. This will be different than what
                            // we will see in the index.md file we generate.
                            return new Promise(
                                (resolve, reject) => {
                                    resolve({data: {link: imageObject.localSrc}});
                                }
                            );
                        }
                    }
                }}/>
        </Box>
    )
}

export function htmlToEditorContent(html) {
    const contentBlock = htmlToDraft(html);
    if (contentBlock) {
        const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
        return EditorState.createWithContent(contentState);
    }
    return null
}

export function editorContentToHtml(htmlContent) {
    return draftToHtml(convertToRaw(htmlContent.getCurrentContent()))
}
