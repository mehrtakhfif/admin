import React from "react";
import {createName} from "../TextFieldMultiLanguageContainer";
import Editor from "./Editor";
import Box from "@material-ui/core/Box";
import {sLang} from "../../../../repository";


export default function ({name, group, value, activeLang = sLang.fa, minHeight = 300, ...props}) {


    return (
        <Box width={1}>
            <Editor name={createName({group:group,name:name,langKey:activeLang.key})} value={value[activeLang.key]} minHeight={minHeight}/>
        </Box>
    )
}
