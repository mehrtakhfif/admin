import {errorList} from "./TextFieldContainer";
import React from "react";
import TextField from "./TextField";
import TextFieldMultiLanguageContainer from "./TextFieldMultiLanguageContainer";


export default function ({name, defaultValue, containerProps = {}, textFieldProps = {}, ...props}) {
    return (
        <TextFieldMultiLanguageContainer
            name={name}
            style={{display:'none'}}
            render={(ref, {lang: inputLang, name: inputName, initialize, valid, errorIndex, setValue, inputProps, style, props}) => {
                return (
                    <TextField
                        {...props}
                        error={!valid}
                        variant="outlined"
                        name={inputName}
                        defaultValue={defaultValue[inputLang.key]}
                        fullWidth
                        helperText={errorList[errorIndex]}
                        inputRef={ref}
                        label={''}
                        style={style}
                        inputProps={{
                            ...inputProps
                        }}/>
                )
            }}/>
    )
}
