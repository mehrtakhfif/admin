import React, {useEffect, useState} from "react";
import Box from "@material-ui/core/Box";
import Editor, {editorContentToHtml, htmlToEditorContent} from "./Editor";
import _ from 'lodash';
import {sLang} from "../../../repository";

export default function ({name,value, activeLang = 'fa', onContentChange, ...props}) {
    const [state, setState] = useState({
        activeLang: activeLang,
        value: null
    });
    const [timer, setTimer] = useState(null);

    useEffect(() => {
        const pr = {};
        _.forEach(sLang, (v) => {
            pr[v.key] = htmlToEditorContent((value && value[v.key]) ? value[v.key] : "");
        });
        setState({
            ...state,
            value: pr
        })
    }, []);

    useEffect(() => {

        clearInterval(timer);
        setTimer(setTimeout(()=>{
            const val ={};
            _.forEach(state.value,(v,key)=>{
               val[key] =  editorContentToHtml(v)
            });
            onContentChange(val)
        },1000));


        return () => {
            clearTimeout(timer);
        }
    }, [state.value]);

    return (
        <Box>
            {state.value &&
            <Editor
                name={name}
                content={state.value[state.activeLang]}
                onContentChange={(content) => {

                    const val = state.value;
                    val[state.activeLang] = content;
                    setState({
                        ...state,
                        value:{
                            ...val
                        }
                    })
                }}
                {...props}/>
            }
        </Box>
    )
}
