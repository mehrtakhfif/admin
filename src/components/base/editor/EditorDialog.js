import React, {useRef} from "react";
import Editor from "./Editor";
import BaseDialog from "../BaseDialog";
import Box from "@material-ui/core/Box";
import SuccessButton from "../button/buttonVariant/SuccessButton";
import BaseButton from "../button/BaseButton";
import FormController from "../formController/FormController";
import {tryIt} from "../../../utils/ObjectUtils";


export default function ({open, value, boxId, onClose, ...props}) {
    const ref = useRef();

    function handleSave() {
        tryIt(() => {
            onClose(ref.current.serialize().editorDialog)
        })
    }

    return (
        <BaseDialog open={open} maxWidth={"xl"} onClose={() => onClose()}>
            <FormController innerref={ref} display={'flex'} py={2} px={3} flexDirection={'column'} width={1}>
                <Editor
                    name={"editorDialog"}
                    content={value}
                    boxId={boxId}
                    {...props}/>
                <Box display={'flex'} pt={2}>
                    <SuccessButton onClick={handleSave}>
                        ذخیره
                    </SuccessButton>
                    <Box px={2}>
                        <BaseButton variant={"outlined"} onClick={() => onClose()}>
                            لغو
                        </BaseButton>
                    </Box>
                </Box>
            </FormController>
        </BaseDialog>
    )


}
