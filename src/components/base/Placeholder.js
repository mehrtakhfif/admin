import React from "react";


export default function Placeholder({...props}) {
    return (

        <div
            className={'mt-place-holder'}
            style={{
                position: 'absolute !important',
                bottom: 0,
                display: 'none !important'
            }}>
            {
                props.children?props.children:"holder"
            }
        </div>
    )
}
