import React from 'react';
import Box from "@material-ui/core/Box";

function PlaceHolder(props) {
    return (
        <Box>
            <div className="lds-grid">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
        </Box>
    )
}

export default PlaceHolder;

