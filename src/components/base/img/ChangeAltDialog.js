import React,{useState} from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import {gcLog} from "../../../utils/ObjectUtils";
import axios from "axios";
import ControllerSite from "../../../controller/ControllerSite";

function ChangeAltDialog({data,open,handleClose,...props}) {
    const [altText,setAltText] = useState(data?.text)

    async function handleChangeAlt() {
    ControllerSite.Media.mediaAlt({id:data.id, text:altText}).then((res) => {
            if (res.status === 200){
                handleClose(data.id,altText,data.type)
            }
        })
    }


    return (
        <div>
            <Dialog open={open} onClose={handleClose} fullWidth={true} maxWidth={'md'} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">تغیر متن alt</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        متن جدید alt را وارد کنید
                    </DialogContentText>
                    <TextField
                        autoFocus
                        margin="dense"
                        id="name"
                        label=""
                        type="alt"
                        fullWidth
                        defaultValue={data?.text}
                        value={altText}
                        onChange={(e)=>{setAltText(e.target.value)}}
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        بستن
                    </Button>
                    <Button onClick={handleChangeAlt} color="primary">
                        ذخیره
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}

export default ChangeAltDialog;