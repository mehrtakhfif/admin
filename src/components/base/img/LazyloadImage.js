import * as React from "react";
import {useEffect, useState} from "react";
import {isClient} from "../../../repository";
import Box from "@material-ui/core/Box";

// Only initialize it one time for the entire application


function LazyImage({alt, src, placeholder = true, placeholderSrc, srcset, sizes, width = '100%', height, ...props}) {

    const [init, setInit] = useState(-1);

    useEffect(() => {
        if (isClient())
            document.lazyLoadInstance.update();
    });

    useEffect(() => {
        if (init === -1){
            setInit(1);
            return
        }
        setInit(0);

        // if (placeholderSrc){
        //     src = placeholderSrc.replace("-ph","-has-ph")
        //     src = placeholderSrc.replace("-ph","-has-ph")
        //
        // }
    }, [src]);

    return (
            init === 0 ?
            <Box>
                <span style={{color:'transparent',opacity:0}}>place</span>
                {setTimeout(()=>setInit(1),500)}
            </Box>:
            <img
                alt={alt}
                className="lazy"
                src={(placeholder && placeholderSrc) ? placeholderSrc : src}
                data-src={placeholder ? src : ''}
                data-srcset={srcset}
                data-sizes={sizes}
                width={width}
                height={height}
                {...props}
            />
    )
}

export default LazyImage;
