import React from "react";
import Box from "@material-ui/core/Box";
import Card from "@material-ui/core/Card";
import Typography from "../Typography";
import {InfoOutlined} from "@material-ui/icons";
import {grey, orange, red} from "@material-ui/core/colors";
import {Tooltip} from "@material-ui/core";
import Popper from "@material-ui/core/Popper";
import Fade from "@material-ui/core/Fade";
import IconButton from "@material-ui/core/IconButton";
import _ from 'lodash';

export default function ({items = [], tooltipProps = {}, placement = 'left', ...props}) {
    const [anchorEl, setAnchorEl] = React.useState(null);
    const handleClick = (event) => {
        setAnchorEl(anchorEl ? null : event.currentTarget);
    };
    const open = Boolean(anchorEl);
    const id = open ? 'transitions-popper' : undefined;

    return (
        <Box {...props}>
            <Tooltip title={"توضیحات"}>
                <IconButton onClick={handleClick}>
                    <InfoOutlined/>
                </IconButton>
            </Tooltip>
            <Popper id={id} open={open} anchorEl={anchorEl} placement={placement} transition>
                {({TransitionProps}) => (
                    <Fade {...TransitionProps} timeout={350}>
                        <Box component={Card} display={'flex'} my={1} mx={1} {...tooltipProps}
                             style={{...tooltipProps.style}}>
                            <Box p={1} display={'flex'} flexDirection={'column'}>
                                {items.map((item, index) => {
                                    const {icon, iconProps, text, textProps, textVariant, importance, isH, isM, isL} = item;
                                    const itemStyle = getStyle(item);

                                    return (
                                        <Typography key={index} variant={textVariant} py={0.3}
                                                    alignItems={'center'}
                                                    color={itemStyle.textColor}
                                                    fontWeight={itemStyle.fontWight}
                                                    {...textProps}>
                                            {icon &&
                                            <Box display={'flex'} ml={0.5}
                                                 {...iconProps}
                                                 style={{
                                                     color:itemStyle.iconColor,
                                                     ...iconProps.style
                                                 }}>
                                                {icon}
                                            </Box>
                                            }
                                            {text}
                                        </Typography>
                                    )
                                })}
                            </Box>
                        </Box>
                    </Fade>
                )}
            </Popper>
        </Box>
    )
}

function getStyle(item) {
    const {icon, iconProps, text, textProps, textVariant, importance, isH, isM, isL} = item;

    if (isH) {
        return {
            textColor: red[700],
            iconColor: red[600],
            fontWight: 600,
        }
    }
    if (isM) {
        return {
            textColor: orange[700],
            iconColor: orange[600],
            fontWight: 400,
        }
    }
    return {
        textColor: grey[800],
        iconColor: grey[600],
    }
}


export const createInfoItem = ({
                                   icon = <InfoOutlined fontSize={"small"}/>,
                                   iconProps = {
                                       style: {
                                           color: grey[600]
                                       }
                                   },
                                   text,
                                   textVariant = 'body2',
                                   textProps = {},
                                   importance = "h" | "m" | 'l'
                               }) => {
    importance = _.lowerCase(importance);
    if (!(importance === "h" || importance === "m"))
        importance = "l";
    const isH = importance === "h";
    const isM = importance === "m";
    const isL = !(isH || isM);
    return {
        icon,
        iconProps,
        text,
        textVariant,
        textProps,
        importance,
        isH,
        isM,
        isL
    }
};
