import React from "react";
import {DragDropContext, Draggable, Droppable} from "react-beautiful-dnd";
import _ from 'lodash'

// fake data generator
const getItems = count =>
    Array.from({length: count}, (v, k) => k).map(k => ({
        id: `item-${k}`,
        content: `item ${k}`
    }));

// a little function to help us with reordering the result
const reorder = (list, startIndex, endIndex) => {
    const result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);

    return result;
};

const grid = 8;

const getItemStyle = (isDragging, draggableStyle) => ({
    userSelect: "none",
    ...draggableStyle
});

const getListStyle = isDraggingOver => ({
    background: isDraggingOver ? "lightblue" : "lightgrey",
});

export default function ({idKey='id' , items, render, onItemsChange, rootStyle, ...props}) {

    function onDragEnd(result) {
        // dropped outside the list
        if (!result.destination) {
            return;
        }

        const newItems = reorder(
            items,
            result.source.index,
            result.destination.index
        );

        onItemsChange(newItems)
    }

    return (
        <DragDropContext onDragEnd={onDragEnd}>
            <Droppable droppableId="droppable">
                {(provided, snapshot) => (
                    <div
                        {...provided.droppableProps}
                        ref={provided.innerRef}
                        style={rootStyle ? rootStyle(!snapshot.isDraggingOver) : {}}>
                        {items.map((item, index) => (
                            <Draggable key={item[idKey]} draggableId={_.toString(item[idKey])} index={index}>
                                {(provided, snapshot) => (
                                    <div>
                                        {render(item, {
                                            ref: provided.innerRef,
                                            ...provided.draggableProps,
                                            ...provided.dragHandleProps,
                                            style: {
                                                ...getItemStyle(
                                                    snapshot.isDragging,
                                                    provided.draggableProps.style
                                                )
                                            }
                                        }, {
                                            index: index,
                                            ...provided.draggableProps,
                                            isDraggingItem: snapshot.isDragging,
                                        })}
                                    </div>
                                )}
                            </Draggable>
                        ))}
                        {provided.placeholder}
                    </div>
                )}
            </Droppable>
        </DragDropContext>
    )
}
