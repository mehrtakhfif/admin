import React from "react";
import Box from "@material-ui/core/Box";
import {makeStyles, useTheme} from "@material-ui/core";
import {UtilsStyle} from "../../../utils/Utils";
import {sLang} from "../../../repository";
import Divider from "@material-ui/core/Divider";
import Typography from "../Typography";
import IconButton from "@material-ui/core/IconButton";
import {ExpandLess, ExpandMore, FiberManualRecord} from "@material-ui/icons";
import {green, orange, red} from "@material-ui/core/colors";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import {hasActive} from "../multiLanguageContainer/MultiLanguageContainer";
import _ from 'lodash'
import ButtonBase from "@material-ui/core/ButtonBase";

const style = makeStyles(theme => ({
    root: props => ({
        ...UtilsStyle.transition(500),
        "&:before": {
            content: "''",
            width: '100%',
            height: props.borderStork,
            backgroundColor: props.borderColor,
            ...UtilsStyle.transition(500),
        }
    }),
}));


export default function BoxWithTopBorder({
                              title,
                              borderStork = 4,
                              borderColor,
                              open,
                              activeLanguageValue: value,
                              activeLanguage,
                              onLanguageChange,
                              onOpenClick,
                              ...props
                          }) {
    const theme = useTheme();
    if (!borderColor)
        borderColor = theme.palette.secondary.main;

    const classes = style({
        borderColor: borderColor,
        borderStork: borderStork,
        ...props,
    });

    const [anchorEl, setAnchorEl] = React.useState(null);
    const hA = value ? hasActive(value) : null;
    if (value && !_.isArray(value))
        value = [value];

    const handleClick = event => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = React.useCallback(() => {
        setAnchorEl(null);
    }, []);

    return (
        <Box display={'flex'}
             flexDirection={'column'}
             className={classes.root}
             {...props}>
            {title &&
            <>
                <Box py={1.5} px={1} display={'flex'}
                     alignItems={'center'}
                     onClick={(e) => open !== undefined ? onOpenClick(e, !open) : null}
                     style={{
                         cursor: open !== undefined ? 'pointer' : null
                     }}>
                    {!_.isEmpty(value) ?
                        <ButtonBase>
                            <Box display={'flex'} alignItems={'center'} mr={1}
                                 py={0.2} px={0.5}
                                 onClick={(e) => {
                                     try {
                                     handleClick(e);
                                     e.stopPropagation();
                                     }catch (e) {

                                     }
                                 }}
                                 style={{
                                     backgroundColor: theme.palette.action.selected,
                                     ...UtilsStyle.borderRadius(5)
                                 }}>
                                <Typography variant={'caption'} pt={'2px'} pl={0.3} fontWeight={500}>
                                    {activeLanguage.shortLabel}
                                </Typography>
                                <FiberManualRecord
                                    fontSize={'small'}
                                    style={{
                                        color: hA === 1 ? green[300] : hA === -1 ? red[300] : orange[300]
                                    }}/>
                            </Box>
                        </ButtonBase> : null}
                    <Typography variant={'h6'}
                                style={{
                                    flex: 1
                                }}>
                        {title}
                    </Typography>
                    {open !== undefined ?
                        <Box px={2}>
                            <IconButton size={'small'}>
                                {!open ?
                                    <ExpandMore/> :
                                    <ExpandLess/>}
                            </IconButton>
                        </Box>
                        : null}
                </Box>
                <Divider/>
            </>}
            {props.children}
            {value &&
            <LanguageMenu anchorEl={anchorEl} value={value} onLanguageChange={(l) => {
                if (l) {
                    onLanguageChange(l)
                }
                handleClose();
            }}/>
            }
        </Box>
    )
}

// export default React.memo(BoxWithTopBorder, (pp, np) => {
//     return !(!_.isEqual(pp.children, np.children) || !_.isEqual(pp.title, np.title) || !_.isEqual(pp.borderStork, np.borderStork) ||
//         !_.isEqual(pp.borderColor, np.borderColor) || !_.isEqual(pp.open, np.open) ||
//         !_.isEqual(pp.activeLanguageValue, np.activeLanguageValue) || !_.isEqual(pp.activeLanguage, np.activeLanguage))
// })


const LanguageMenu = React.memo(function LanguageMenu({anchorEl, value, onLanguageChange, ...props}) {
    return (
        !_.isEmpty(value) &&
        <Menu
            anchorEl={anchorEl}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={() => onLanguageChange()}>
            {
                Object.keys(sLang).map((keyName, index) => {
                    const l = sLang[keyName];
                    return (
                        <MyMenuItem
                            key={keyName}
                            onClick={(e) => {
                                try {
                                onLanguageChange(l);
                                e.stopPropagation();

                                }catch (e) {

                                }
                            }}>
                            <Box display={'flex'}>
                                <FiberManualRecord
                                    fontSize={'small'}
                                    style={{
                                        color: hasActive(value, l) === 1 ? green[300] : red[300]
                                    }}/>
                                <Typography variant={'caption'} display={'flex'}
                                            pr={0.5}
                                            justifyContent={'center'}
                                            style={{...UtilsStyle.disableTextSelection()}}>
                                    {l.label}
                                </Typography>
                            </Box>
                        </MyMenuItem>
                    )
                })
            }
        </Menu>
    )
}, (pp, np) => {
    return !(!_.isEqual(pp.value, np.value) || !_.isEqual(pp.anchorEl, np.anchorEl))
});

const MyMenuItem = React.memo(MenuItem);
