import React from "react";
import Box from "@material-ui/core/Box";
import Typography from "../Typography";
import {makeStyles} from "@material-ui/core/styles";

const styles = makeStyles(theme => (
    {
        textHeader: {
            marginTop: 0,
            marginBottom: 0,
            position: 'relative',
            display: 'inline-block',
            '&:before,&:after': {
                content: '""',
                position: 'absolute',
                height: '1px',
                backgroundColor: 'red',
                top: '50%',
                transform: 'translate(0, -50%)',
                [theme.breakpoints.up('md')]: {
                    width: '150px',
                },
                [theme.breakpoints.down('sm')]: {
                    width: '150px',
                },
                [theme.breakpoints.down('xs')]: {
                    width: '50px',
                },
            },
            '&:before': {
                left: '100%',
                marginLeft: '15px',
            },
            '&:after': {
                right: '100%',
                marginRight: '15px',
            }
        },
    }));

function TextHeader({variant = 'h6', component = 'h6', typographyStyle = {}, ...props}) {
    const classes = styles(props);
    return (
        <Box textAlign='center' display="flex" justifyContent={'center'}  {...props}>
            <Typography
                className={classes.textHeader}
                variant={'h6'}
                fontWeight={600}
                component={component}
                style={{
                    ...typographyStyle,
                }}>
                {props.children}
            </Typography>
        </Box>
    )
}

export default TextHeader;



