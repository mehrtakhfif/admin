import React from "react";
import {cyan} from "@material-ui/core/colors";
import Box from "@material-ui/core/Box";
import {UtilsStyle} from "../../../utils/Utils";

export default function Gem({color = cyan[500], scale = 7, ...props}) {
    return (
        <Box component={'span'} style={{
            backgroundColor: color,
            width: scale,
            height: scale,
            MozTransform: 'scale(1) rotate(45deg)',
            WebkitTransform: 'scale(1) rotate(45deg)',
            OTransform: 'scale(1) rotate(45deg)',
            MsTransform: 'scale(1) rotate(45deg)',
            transform: 'scale(1) rotate(45deg)',
            ...UtilsStyle.borderRadius(2)
        }} {...props}/>
    )
}
