import React from "react";
import {ReactComponent as Gem} from '../../../drawable/svg/gem.svg';
import {cyan, grey} from "@material-ui/core/colors";
import {makeStyles} from "@material-ui/core";

const style = makeStyles((theme) => ({
    root: ({borderColor, gemColor, ...props}) => ({
        '& #border': {
            stroke: borderColor
        },
        '& #square': {
            fill: gemColor
        }
    }),
}));

export default function Gem2({borderColor = grey[700], gemColor = cyan[500], ...props}) {
    const classes = style({borderColor, gemColor, ...props});
    return (
        <Gem className={classes.root} {...props}/>
    )
}
