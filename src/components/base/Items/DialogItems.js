import React, {useState} from "react";
import Box from "@material-ui/core/Box";
import ItemCard from "./ItemCard";

export default function DialogItems({minWidth = 1 / 3, item,dialogProps, ...props}) {
    const [open, setOpen] = useState(false)
    console.log("DialogItems",open);
    return (
        <Box minWidth={minWidth} {...props}>
            <ItemCard item={item} onClick={() => setOpen(true)}/>
            {item.dialog({open: open, onClose: () => setOpen(false),...dialogProps})}
        </Box>
    )
}
