import React from "react";
import Box from "@material-ui/core/Box";
import BaseButton from "../button/BaseButton";
import {Card} from "@material-ui/core";
import Typography from "../Typography";
import {grey} from "@material-ui/core/colors";


export function createItemCard({icon, title, dialog, background = grey[100], darkBackground = grey[700],...p}) {
    return {icon, title, dialog, background, darkBackground: darkBackground || background,...p}
}



export default function ItemCard({item, onClick, ...props}) {
    return (
        <Box p={2} {...props}>
            <BaseButton
                onClick={onClick}
                style={{
                    margin: 0,
                    padding: 0,
                    width: "100%"
                }}>
                <Box component={Card} width={1} py={4} px={2} display={'flex'}
                     alignItems={'center'} justifyContent={'center'}
                     flexDirection={'column'}
                     style={{
                         background: item.background,
                     }}>
                    {item.icon}
                    <Typography variant={'h6'} pt={2} px={3} color={item.color}>
                        {item.title}
                    </Typography>
                </Box>
            </BaseButton>
        </Box>
    )
}
