import React from "react";
import Box from "@material-ui/core/Box";
import {sLang} from "../../../repository";
import MenuItem from "@material-ui/core/MenuItem";
import {FiberManualRecord} from "@material-ui/icons";
import {green, orange, red} from "@material-ui/core/colors";
import {UtilsStyle} from "../../../utils/Utils";
import _ from "lodash";
import Menu from "@material-ui/core/Menu";
import IconButton from "@material-ui/core/IconButton";
import Typography from "../Typography";
import Tooltip from "@material-ui/core/Tooltip";


export function hasActive(value, lan = null) {
    if (!value)
        return
    if (_.isArray(value))
        return hasActiveArray(value, lan);

    if (lan) {
        return Boolean(value[lan.key]) ? 1 : -1;
    }
    let len = 0;
    let active = 0;
    if (!value)
        return
    _.forEach(sLang, (l, key) => {
            len = len + 1;
            if (value[key]) {
                if (_.isObject(value[key])) {
                    len = len - 1;
                    _.forEach(value[key], v => {
                        len = len + 1;
                        if (!_.isEmpty(v)) {
                            active = active + 1;
                            return false
                        }
                    });
                    return
                }
                active = active + 1;
            }
        }
    );
    return active === 0 ? -1 : active < len ? 0 : 1
}

function hasActiveArray(value, lan = null) {
    let res = -2;
    _.forEach(value, (v) => {
        const a = hasActive(v, lan);
        if (a === -1) {
            res = res > -1 ? 0 : -1;
            return;
        }
        if (a === 0) {
            res = 0;
            return;
        }
        if (a === 1) {
            res = (res === -2 || res === 1) ? 1 : 0;
        }
    });
    return res;
}

export default function ({value, activeLanguage, onLanguageChange, ...props}) {
    const [anchorEl, setAnchorEl] = React.useState(null);
    const hA = hasActive(value);
    return (
        <Box display={'flex'} width={1} position={'relative'}{...props}>
            <Box display={'flex'} alignItems={'center'} position={'absolute'} style={{top: 0}}>
                <Tooltip title={activeLanguage.shortLabel}>
                    <IconButton size={"small"}
                                onClick={(e) => {
                                    setAnchorEl(e.currentTarget)
                                }}>
                        <FiberManualRecord
                            fontSize={'small'}
                            style={{
                                color: hA === 1 ? green[300] : hA === -1 ? red[300] : orange[300]
                            }}/>
                    </IconButton>
                </Tooltip>
                <Menu
                    anchorEl={anchorEl}
                    keepMounted
                    open={Boolean(anchorEl)}
                    onClose={() => setAnchorEl(null)}>
                    {
                        Object.keys(sLang).map((keyName, index) => {
                            const l = sLang[keyName];
                            return (
                                <MenuItem
                                    key={index}
                                    onClick={() => {
                                        setAnchorEl(null)
                                        onLanguageChange(l)
                                    }}>
                                    <Box display={'flex'}>
                                        <FiberManualRecord
                                            fontSize={'small'}
                                            style={{
                                                color: hasActive(value, l) === 1 ? green[300] : red[300]
                                            }}/>
                                        <Typography variant={'caption'} display={'flex'}
                                                    pr={0.5}
                                                    justifyContent={'center'}
                                                    style={{...UtilsStyle.disableTextSelection()}}>
                                            {l.label}
                                        </Typography>
                                    </Box>
                                </MenuItem>
                            )
                        })
                    }
                </Menu>
            </Box>
            {props.children}
        </Box>
    )
}
