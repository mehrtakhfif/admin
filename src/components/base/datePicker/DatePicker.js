import React, {useEffect, useState} from "react";
import Box from "@material-ui/core/Box";
import {Dialog, useTheme} from "@material-ui/core";
import {Calendar} from "react-modern-calendar-datepicker";
import momentJalaali from "moment-jalaali";
import moment from "moment";
import JDate from "../../../utils/JDate";
import {TimePicker,} from '@material-ui/pickers';
import BaseButton from "../button/BaseButton";

export function convertTimestampDate(timeStamp, withTime = false) {
    try {
        if (!timeStamp)
            throw "timestamp in undefined";
        const j = JDate.timeStampToJalali(timeStamp);

        let props = {
            year: j.jYear(),
            month: j.jMonth() + 1,
            day: j.jDate(),
        };
        if (withTime)
            props = {
                ...props,
                hours: j.hours(),
                minutes: j.minutes(),
                seconds: j.seconds(),
            };
        return props
    } catch (e) {
        return undefined
    }
}

export function convertDateToTimestamp(date) {
    try {
        const d = {
            ...convertTimestampDate(Date.now()),
            ...date
        };
        d.month = d.month - 1;
        let moment = null;
        if (d.hours) {
            moment = momentJalaali(`${d.year}/${d.month + 1}/${d.day} ${d.hours}:${d.minutes}:${d.seconds}`, 'jYYYY/jM/jD HH:mm:ss');
        } else {
            moment = momentJalaali(`${d.year}/${d.month + 1}/${d.day}`, 'jYYYY/jM/jD');
        }
        return moment.unix()
    } catch (e) {
        return Date.now();
    }
}

export default function DatePicker({open, onClose, date, minimumDate, maximumDate, selectTime, calendarProps = {}, ...props}) {
    const theme = useTheme();
    const [state, setState] = useState({
        showTime: false
    });
    const [dateState, setDateState] = useState();
    const [timeState, setTimeState] = useState();

    function handleDateChange(date) {
        setDateState(date);
        if (selectTime) {
            setState({
                ...state,
                showTime: true
            });
            return
        }
            handleSave(date)
    }

    function handleTimeChange(date) {
        setTimeState(date);
    }

    function handleSave(date) {
        const pr = date ? date : dateState;

        if (selectTime) {
            const time = moment(timeState);
            pr.hours = time.hour();
            pr.minutes = time.minute();
            pr.seconds = 0;
        }

        onClose(convertDateToTimestamp(pr))
    }

    useEffect(() => {
        if (open) {
            setState({...state, showTime: false});
            // setDateState(undefined);
        }
    }, [open]);


    return (
        <Dialog open={open}
                onClose={() => {
                    onClose()
                }}>
            <Box>
                {!state.showTime ?
                    <Calendar
                        value={convertTimestampDate(date)}
                        onChange={handleDateChange}
                        minimumDate={convertTimestampDate(minimumDate)}
                        maximumDate={convertTimestampDate(maximumDate)}
                        shouldHighlightWeekends
                        locale="fa"
                    />
                    :
                    <Box dir={'ltr'}
                         className={'timePicker'}>
                        <TimePicker
                            autoOk
                            variant="static"
                            openTo="hours"
                            value={timeState}
                            onChange={handleTimeChange}
                        />
                        <Box px={2} py={2}>
                            <BaseButton variant={"text"}
                                        onClick={()=>handleSave()}
                                        style={{
                                            color: theme.palette.secondary.dark,
                                            marginRight: theme.spacing(1)
                                        }}>
                                ذخیره
                            </BaseButton>
                            <BaseButton variant={"text"}
                                        onClick={() => onClose()}
                                        style={{
                                            color: theme.palette.secondary.dark,
                                        }}>
                                لفو
                            </BaseButton>
                        </Box>
                    </Box>}
            </Box>
        </Dialog>
    )
}
