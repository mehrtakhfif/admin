import React, {useEffect, useState} from "react";
import Box from "@material-ui/core/Box";
import ButtonBase from "@material-ui/core/ButtonBase";
import {UtilsStyle} from "../../../utils/Utils";
import Typography from "../Typography";
import JDate from "../../../utils/JDate";
import DatePicker from "./DatePicker";
import _ from "lodash";
import LocalStorageUtils from "../../../utils/LocalStorageUtils";
import {DEBUG} from "../../../repository";
import {gcLog} from "../../../utils/ObjectUtils";

export default function DatePickerStartEndPanel({key, onChange,maximumDate = Date.now() / 1000, ...props}) {
    const [open, setOpen] = useState(undefined);
    const [startTime, setStartTime] = useState(_.toInteger(LocalStorageUtils.get(key + "startTime", (((new Date().setHours(0, 0, 0)) - (60 * 60 * 24 * 7 * 1000)) - (DEBUG ? 1000000000 : 0)))));
    const [endTime, setEndTime] = useState(_.toInteger(LocalStorageUtils.get(key + "endTime", Date.now())));

    useEffect(() => {
        onChange(startTime, endTime)
    }, [startTime, endTime])


    return (
        <Box display={'flex'} {...props}>
            <Box width={1 / 2} display={'flex'} justifyContent={"center"}>
                <ButtonBase
                    onClick={() => {
                        setOpen(1)
                    }}
                    style={{
                        cursor: "pointer",
                        ...UtilsStyle.borderRadius(5)
                    }}>
                    <Box display={'flex'} p={1} alignItems={'center'} flexDirection={'column'}>
                        <Typography pb={0.5} variant={"body2"}>
                            تاریخ شروع
                        </Typography>
                        <Typography pt={0.5} variant={"h6"} dir={"ltr"}>
                            {JDate.timestampFormat(startTime / 1000, "jYYYY/jM/jD HH:mm")}
                        </Typography>
                    </Box>
                </ButtonBase>
            </Box>
            <Box width={1 / 2} display={'flex'} justifyContent={"center"}>
                <ButtonBase
                    onClick={() => {
                        setOpen(2)
                    }}
                    style={{
                        cursor: "pointer",
                        ...UtilsStyle.borderRadius(5)
                    }}>
                    <Box display={'flex'} p={1} alignItems={'center'} flexDirection={'column'}>
                        <Typography pb={0.5} variant={"body2"}>
                            تاریخ پایان
                        </Typography>
                        <Typography pt={0.5} variant={"h6"} dir={"ltr"}>
                            {JDate.timestampFormat(endTime / 1000, "jYYYY/jM/jD HH:mm")}
                        </Typography>
                    </Box>
                </ButtonBase>
            </Box>
            <DatePicker
                open={_.isNumber(open)}
                maximumDate={maximumDate}
                selectTime={true}
                date={(open === 1 ? startTime : endTime) / 1000}
                onClose={(t) => {

                    if (t) {
                        t = _.toInteger(t * 1000)
                        if (open === 1) {
                            LocalStorageUtils.set(key + "startTime", t)
                            setStartTime(t)
                        } else {
                            LocalStorageUtils.set(key + "endTime", t)
                            setEndTime(t)
                        }
                    }
                    setOpen(undefined)
                }}/>
        </Box>
    )
}
