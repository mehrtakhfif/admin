import React from "react";
import Box from "@material-ui/core/Box";
import Typography from "./Typography";
import {Card} from "@material-ui/core";
import Collapse from "@material-ui/core/Collapse";
import BaseButton from "./button/BaseButton";

export default class ErrorBoundary extends React.Component {
    state = {error: null, errorInfo: null, open: false};

    componentDidCatch(error, errorInfo) {
        if (this.state.error)
            return
        this.setState({
            ...this.state,
            error: error,
            errorInfo: errorInfo
        });
        try {
            this.props.onError()
        } catch (e) {
        }
    }

    render() {
        if (this.state.error) {
            return (
                <Box width={1} py={2} display={'flex'} flexDirection={'column'}>
                    <Box width={1} display={'flex'} alignitems={'center'} jusifyContent={'center'}>
                        <Typography pb={2} variant={"h6"}>
                            مشکلی در رندر این کامپوننت پیش آمده
                            <br/>
                            لطفا با پشتیبانی تماس حاصل فرمایید
                        </Typography>
                        <BaseButton onClick={() => {
                            this.setState({
                                ...this.state,
                                open: !this.state.open,
                            });
                        }}>
                            {this.state.open ? "پنهان کردن" : "نمایش ارور"}
                        </BaseButton>
                    </Box>
                    <Collapse in={this.state.open}>
                        <Box p={1}>
                            <Box component={Card}>
                                <details style={{whiteSpace: "pre-wrap"}}>
                                    {this.state.error && this.state.error.toString()}
                                    <br/>
                                    {this.state.errorInfo.componentStack}
                                </details>
                            </Box>
                        </Box>
                    </Collapse>
                </Box>
            );
        }

        return this.props.children;
    }
}
