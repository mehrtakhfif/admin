import React, {useState} from "react";
import Day from "./Day";
import Box from "@material-ui/core/Box";


export const monthsName = [
    'فروردین',
    'اردیبهشت',
    'خرداد',
    'تیر',
    'مرداد',
    'شهریور',
    'مهر',
    'آبان',
    'آذر',
    'دی',
    'بهمن',
    'اسفند',
];

export const dayName = [
    'شنبه',
    'یک‌شنبه',
    'دو‌شنبه',
    'سه‌شنبه',
    'جهار‌شنبه',
    'پنج‌شنبه',
    'جعه',
];
export default function Month2({jalali, ...props}) {
    const daysInLastMonth = jalali.clone().subtract(1, 'months').daysInMonth();
    const daysInMonth = jalali.daysInMonth();
    const monthFirstDay = jalali.clone().startOf('jMonth');
    const monthLastDay = jalali.clone().endOf('jMonth');
    const [dayRange, setDayRange] = useState({
        startDay: null,
        endDay: null
    });
    const [activeHoverDay, setActiveHoverDay] = useState(null);

    const days = [];
    const startDayOnWeek = monthFirstDay.day() + 1;


    for (let i = 1; i <= daysInMonth + startDayOnWeek; i++) {
        const {startDay, endDay} = dayRange;
        const day = (i > startDayOnWeek) ? i - startDayOnWeek : daysInLastMonth - startDayOnWeek + i;
        const isDisable = i <= startDayOnWeek;
        // const isActive = isDisable && (startDay !== null && startDay <= day) && ((endDay === null && activeHoverDay !== null && day <= activeHoverDay) || endDay >= day);
        const inForwardHoveRange = (startDay && !endDay && activeHoverDay) && (startDay < activeHoverDay && activeHoverDay >= day && startDay <= day);
        const inBackHoveRange = (startDay && !endDay && activeHoverDay) && (startDay > activeHoverDay && activeHoverDay <= day && startDay >= day);
        const inRange = startDay && endDay && (day >= startDay && day <= endDay);
        const isActive = !isDisable && (startDay === day || inForwardHoveRange || inBackHoveRange || inRange);

        days.push(
            <Day key={i}
                 day={day}
                 disable={isDisable}
                 active={isActive}
                 onDayClick={() => {
                     if (startDay === null || endDay !== null) {
                         setDayRange( {
                             ...dayRange,
                             startDay: day,
                             endDay: null
                         });
                     } else if (startDay < day) {
                         setDayRange({
                             ...dayRange,
                             endDay: day
                         });
                     } else{
                         const endDay = dayRange.startDay;
                         setDayRange({
                             ...dayRange,
                             startDay: day,
                             endDay: endDay
                         });
                     }

                 }}
                 onDayHover={(enter) => {
                     setActiveHoverDay(enter ? day : null)
                 }}/>
        )
    }
    return (
        <Box display={'flex'} flexDirection={'column'} justifyContent={'center'} alignItems={'center'}>
            <Box width={1} display={'flex'}>
                {dayName.map((label, index) => (
                    <WeekLabel key={index}
                               label={label}/>
                ))}
            </Box>
            <Box width={1} display={'flex'} flexWrap={'wrap'}>
                {days}
            </Box>
        </Box>
    )
}


function WeekLabel({label}) {
    return (
        <Box width={1 / 7} pt={0.5}
             py={1} display={'flex'}
             alignItems={'center'} justifyContent={'center'}
             fontSize={'2.5vw'}>
            {label}
        </Box>
    )
}
