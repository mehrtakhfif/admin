import React, {useEffect, useState} from "react";
import Box from "@material-ui/core/Box";
import JDate from "../../../utils/JDate";
import Typography from "../Typography";
import Month from "./Month";
import _ from 'lodash'
import {withWidth} from "@material-ui/core";
import {isWidthUp} from "@material-ui/core/withWidth";
import {ArrowBackIos, ArrowForwardIos} from "@material-ui/icons";
import {lang} from "../../../pages/BaseSite";
import {UtilsStyle} from "../../../utils/Utils";

function Datepicker({
                        startDateTimeStamp = 1614698357,
                        endDateTimeStamp = 1625321957,
                        ...props
                    }) {

    //region variable
    const startDate = JDate.timeStampToJalali(startDateTimeStamp);
    const endDate = JDate.timeStampToJalali(endDateTimeStamp);
    // const daysInMonth = startDate.daysInMonth();
    //endregion variable
    //region state
    const [state, setState] = useState({
        disableDays: []
    });

    const [dayRange, setDayRange] = useState({
        activeMonthState: 0,
        startDay: null,
        endDay: null,
    });
    const [dayHover, setDayHover] = useState({
        activeHoverDay: null
    });
    //endregion state

    //region useEffect
    useEffect(() => {
        const newState = {...state};
        const disableDays = [];
        disableDays.push({
            startDate: JDate.timeStampToJalali(1578096000),
            endDate: JDate.timeStampToJalali(1578182400)
        });
        disableDays.push({
            startDate: JDate.timeStampToJalali(1579132800),
            endDate: JDate.timeStampToJalali(1579305600)
        });
        disableDays.push({
            startDate: JDate.timeStampToJalali(1580630764),
            endDate: JDate.timeStampToJalali(1581235564)
        });
        disableDays.push({
            startDate: JDate.timeStampToJalali(1617269916),
            endDate: JDate.timeStampToJalali(1617788316)
        });

        // Start and End date
        disableDays.push({
            startDate: startDate.clone().startOf('jMonth'),
            endDate: startDate.clone().subtract(1, 'day')
        });
        disableDays.push({
            startDate: endDate.clone().add(1,'day'),
            endDate: endDate.clone().endOf('jMonth')
        });


        setState({
            ...newState,
            disableDays: disableDays
        })
    }, []);
    //endregion useEffect

    //endregion variable

    //region handler
    function handlerItemClick({day, isDisable}) {
        const {startDay, endDay} = dayRange;
        if ((startDay && endDay) || (startDay && startDay.isSame(day))) {
            setDayRange({
                ...dayRange,
                startDay: null,
                endDay: null
            });
            return
        }
        if (isDisable)
            return;
        if (startDay === null || endDay !== null) {
            setDayRange({
                ...dayRange,
                startDay: day,
                endDay: null
            });
        } else if (startDay < day) {
            setDayRange({
                ...dayRange,
                endDay: day
            });
        } else if (!startDay.isSame(day, 'day')) {
            const endDay = dayRange.startDay;
            setDayRange({
                ...dayRange,
                startDay: day,
                endDay: endDay
            });
        }
    }

    function handleOnHoverItem(enter, {day, isDisable}) {
        const {startDay} = dayRange;
        if (startDay) {
            let disable = false;
            _.forEach(state.disableDays, ({startDate, endDate}) => {
                if (JDate.inRange(day, startDate, endDate)) {
                    disable = true;
                    return false;
                }
            });
            setDayHover({
                ...dayHover,
                activeHoverDay: disable ? null : day
            });
            return
        }
        setDayHover({
            ...dayHover,
            activeHoverDay: enter ? day : null
        });
    }


    //endregion handler
    let m1 = startDate.clone().add(dayRange.activeMonthState, 'month');
    let m2 = isWidthUp('md', props.width) ? startDate.clone().add(dayRange.activeMonthState + 1, 'month') : null;

    let hasBefore = true;
    let hasNext = true;
    if (startDate.isSameOrAfter(m1, 'month')) {
        hasBefore = false
    }
    if (endDate.isSame(m1.clone().add(1, 'month'), 'month')) {
        hasNext = false
    }
    return (
        <Box display={'flex'} flexDirection={'column'} position={'relative'}>
            <Box display={'flex'} position={'absolute'} style={{
                left: 0,
                right: 0
            }}>

                <Box display={'flex'} width={1 / 2}
                     alignItems={'center'}

                     style={{
                         ...UtilsStyle.disableTextSelection()
                     }}>
                    {hasBefore &&
                    <Box display={'flex'}
                         alignItems={'center'}
                         onClick={() => {
                             if (!hasBefore)
                                 return;
                             setDayRange({
                                 ...dayRange,
                                 activeMonthState: dayRange.activeMonthState - 1
                             })
                         }}
                         style={{
                             cursor: 'pointer',
                         }}>
                        <ArrowForwardIos/>
                        <Typography variant={'h5'} pr={0.8}>
                            {lang.get("back")}
                        </Typography>
                    </Box>
                    }
                </Box>
                <Box display={'flex'} width={1 / 2}
                     justifyContent={'flex-end'}
                     alignItems={'center'}
                     style={{
                         ...UtilsStyle.disableTextSelection()
                     }}>
                    {hasNext &&
                    <Box
                        display={'flex'}
                        alignItems={'center'}
                        onClick={() => {
                            if (!hasNext)
                                return;
                            setDayRange({
                                ...dayRange,
                                activeMonthState: dayRange.activeMonthState + 1
                            })
                        }}
                        style={{
                            cursor: 'pointer',
                        }}>
                        <Typography variant={'h5'} pl={0.5}>
                            {lang.get("next")}
                        </Typography>
                        <ArrowBackIos/>
                    </Box>
                    }
                </Box>
            </Box>
            <Box display={'flex'}>
                <Box display={'flex'}
                     flexDirection={'column'}
                     alignItems={'center'}
                     ml={{
                         xs: 0,
                         md: 1
                     }}>
                    <Typography mb={2} variant={'h5'} fontWeight={400}>
                        {m1.locale('fa').format('jMMMM - jYYYY')}
                    </Typography>
                    <Month
                        date={m1}
                        disableDays={state.disableDays}
                        activeRange={{
                            startDay: dayRange.startDay,
                            endDay: dayRange.endDay,
                            activeHoverDay: dayHover.activeHoverDay
                        }}
                        onDayClick={handlerItemClick}
                        onDayHover={handleOnHoverItem}/>
                </Box>
                {m2 &&
                <Box display={'flex'}
                     flexDirection={'column'}
                     alignItems={'center'}
                     mr={{
                         xs: 0,
                         md: 1
                     }}>
                    <Typography mb={2} variant={'h5'} fontWeight={400}>
                        {m2.format('jMMMM - jYYYY')}
                    </Typography>
                    <Month
                        date={m2}
                        disableDays={state.disableDays}
                        activeRange={{
                            startDay: dayRange.startDay,
                            endDay: dayRange.endDay,
                            activeHoverDay: dayHover.activeHoverDay
                        }}
                        onDayClick={handlerItemClick}
                        onDayHover={handleOnHoverItem}/>
                </Box>
                }
            </Box>
        </Box>
    )
}

export default withWidth()(Datepicker)
