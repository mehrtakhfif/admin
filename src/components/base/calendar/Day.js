import React from "react";
import Box from "@material-ui/core/Box";
import {makeStyles} from "@material-ui/core";
import {cyan, grey, red} from "@material-ui/core/colors";
import {UtilsFormat, UtilsStyle} from "../../../utils/Utils";
import Typography from "../Typography";
import pholiday from "pholiday";
import clsx from "clsx";


const sp = {
    fill: {
        background: grey[700],
        color: grey[400],
        priceColor: grey[400],
        hoverBackground: grey[700],
        hoverColor: grey[400],
    },
    disable: {
        background: grey[500],
        color: grey[300],
        priceColor: grey[300],
        hoverBackground: grey[500],
        hoverColor: grey[300],
    },
    active: {
        background: cyan[400],
        color: grey[200],
        priceColor: grey[200],
        hoverBackground: cyan[400],
        hoverColor: grey[200],
    },
    default: {
        background: grey[200],
        color: grey[900],
        priceColor: grey[900],
        hoverBackground: grey[300],
        hoverColor: grey[900],
    },
    holiday: {
        background: grey[200],
        color: red[600],
        priceColor: grey[900],
        hoverBackground: grey[300],
        hoverColor: red[600],
    }
};
const useDayStyles = makeStyles({
    dayRoot: {
        '&:before': {
            content: '""',
            float: 'left',
            paddingTop: '100%'
        }
    },
    dayItem: {
        cursor: 'pointer',
        ...UtilsStyle.borderRadius(2),
        ...UtilsStyle.disableTextSelection(),
        ...UtilsStyle.transition(600),
        backgroundColor: sp.default.background,
        '& b': {
            color: sp.default.color,
        },
        '& span': {
            color: sp.default.priceColor
        },
        '&:hover': {
            ...UtilsStyle.transition(600),
            backgroundColor: sp.default.hoverBackground,
            '& b': {
                color: sp.default.hoverColor,
            }
        },
        '&.active': {
            backgroundColor: sp.active.background,
            // ...UtilsStyle.borderRadius(active === 2 ? `0 20% 20% 0` : active === 3 ? `20% 0 0 20%` : 2),
            '& b': {
                color: sp.active.color,
            },
            '& span': {
                color: sp.active.priceColor
            },
            '&:hover': {
                backgroundColor: sp.active.hoverBackground,
                '& b': {
                    color: sp.active.hoverColor,
                }
            }
        },
        '&.disable': {
            backgroundColor: sp.disable.background,
            '& b': {
                color: sp.disable.color,
            },
            '& span': {
                color: sp.disable.priceColor
            },
            '&:hover': {
                backgroundColor: sp.disable.hoverBackground,
                '& b': {
                    color: sp.disable.hoverColor,
                }
            }
        },
        '&.holiday': {
            backgroundColor: sp.holiday.background,
            '& b': {
                color: sp.holiday.color,
            },
            '& span': {
                color: sp.holiday.priceColor
            },
            '&:hover': {
                backgroundColor: sp.holiday.hoverBackground,
                '& b': {
                    color: sp.holiday.hoverColor,
                }
            }
        },
        '&.fill': {
            backgroundColor: sp.fill.background,
            '& b': {
                color: sp.fill.color,
            },
            '& span': {
                color: sp.fill.priceColor
            },
            '&:hover': {
                backgroundColor: sp.fill.hoverBackground,
                '& b': {
                    color: sp.fill.hoverColor,
                }
            }
        },
        '&.fill, &.disable':{
            cursor: 'no-drop',
        }
    },
    itemBorder: (({active, ...props}) => ({
        '&.active': {
            ...UtilsStyle.borderRadius(active === 2 ? `0 20% 20% 0` : active === 3 ? `20% 0 0 20%` : 2),
        }
    }))
});
export default function Day({day, active = false, fill = false, disable = false, hidden = false, onDayClick, onDayHover, ...props}) {
    const isHoliday = pholiday(day).isHoliday();
    const classes = useDayStyles({active, ...props});
    const dayClass = fill ? 'fill' : disable ? 'disable' : active ? 'active' : isHoliday ? 'holiday' : '';
    return (
        <Box className={classes.dayRoot}
             p={{
                 xs: 0.2,
                 md: 0.5
             }}
             onMouseEnter={() => onDayHover && onDayHover(true)}
             onMouseLeave={() => onDayHover && onDayHover(false)}
             width={1 / 7}>
            {!hidden &&
            <Box className={clsx(classes.dayItem, classes.itemBorder, dayClass)}
                 width={1}
                 height={1}
                 display={'flex'}
                 flexDirection={'column'}
                 alignItems={'center'}
                 justifyContent={'center'}
                 onClick={onDayClick}>
                <Typography
                    display={'flex'}
                    flexDirection={'column'}
                    alignItems={'center'}
                    variant={'subtitle1'}
                    component={'b'}
                    pb={{
                        xs: 0,
                        md: 0.5,
                        lg: 1
                    }}>
                    {day.format('jD')}
                </Typography>
                <Typography
                    fontWeight={200}
                    component={'span'}
                    variant={'subtitle2'}>
                    {UtilsFormat.numberToMoney(550000)}
                </Typography>
            </Box>
            }
        </Box>
    )
}
