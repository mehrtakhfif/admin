import React from "react";
import Day from "./Day";
import Box from "@material-ui/core/Box";
import _ from 'lodash';
import Typography from "../Typography";
import JDate from "../../../utils/JDate";


export const dayName = [
    'شنبه',
    'یک‌شنبه',
    'دو‌شنبه',
    'سه‌شنبه',
    'چهار‌شنبه',
    'پنج‌شنبه',
    'جمعه',
];
export default function Month({date, disableDays = [], activeRange, onDayClick, onDayHover, ...props}) {

    const daysInMonth = JDate.daysInMonth(date);
    const monthFirstDay = date.clone().startOf('jMonth');


    let timer = null;

    function onDayHoverHandler(enter, props) {
        clearTimeout(timer);
        timer = setTimeout(() => {
            onDayHover(enter, props)
        }, 500)
    }

    //region createDays

    const startDayOnWeek = monthFirstDay.day() + 1;
    const days = [];
    //region fakeDays
    for (let i = 0; i < startDayOnWeek; i++) {
        const day = monthFirstDay.clone().subtract(startDayOnWeek - i, 'day');
        days.push(<Day key={i}
                       day={day}
                       hidden={true}
                       disable={true}/>)
    }
    //endregion fakeDays
    //region days
    for (let i = 0; i < daysInMonth; i++) {
        const {startDay, endDay, activeHoverDay} = activeRange;
        const day = monthFirstDay.clone().add(i, 'day');

        //region CheckDayIsDisable
        let fill = false;
        let disable = false;
        _.forEach(disableDays, ({startDate, endDate}) => {
            if (JDate.inRange(day, startDate, endDate)) {
                fill = true;
                disable = true;
                return false;
            }
            if (startDay && !endDay) {
                if (day.isAfter(startDate) && startDay.isBefore(startDate)) {
                    disable = true;
                    return false;
                }
                if (day.isBefore(startDate) && startDay.isAfter(startDate)) {
                    disable = true;
                    return false;
                }
            }
        });
        //endregion CheckDayIsDisable

        //region dayIsActive
        let isActive = false;
        if (!disable) {
            const inForwardHoveRange = (startDay && !endDay && activeHoverDay) && (startDay < activeHoverDay && activeHoverDay >= day && startDay <= day);
            const inBackHoveRange = (startDay && !endDay && activeHoverDay) && (startDay > activeHoverDay && activeHoverDay <= day && startDay >= day);
            const inRange = startDay && endDay && (day >= startDay && day <= endDay);
            isActive = (day.isSame(startDay, 'day') || inForwardHoveRange || inBackHoveRange || inRange) ? 1 : 0;
        }

        if (isActive) {
            if (startDay && endDay) {
                if (startDay.isSame(day, 'day')) {
                    isActive = 2;
                } else if (endDay.isSame(day, 'day')) {
                    isActive = 3;
                }
            } else if (startDay && startDay.isSame(day, 'day')) {
                isActive = (activeHoverDay === null || startDay.isSame(activeHoverDay, 'day')) ? 1 : startDay.isBefore(activeHoverDay, 'day') ? 2 : 3;
            } else if (endDay && endDay.isSame(day, 'day')) {
                isActive = (endDay.isSameOrBefore(activeHoverDay, 'day')) ? 3 : 2;
            } else if (startDay && activeHoverDay && activeHoverDay.isSame(day, 'day')) {
                isActive = activeHoverDay.isSameOrBefore(startDay, 'day') ? 2 : 3;
            }
        }

        //endregion dayIsActive


        days.push(
            <Day key={i + daysInMonth}
                 day={day}
                 fill={fill}
                 disable={disable}
                 active={isActive}
                 onDayClick={() => onDayClick({
                     day: day,
                     isDisable: disable
                 })}
                 onDayHover={(enter) => onDayHoverHandler(enter, {
                     day: day,
                     isDisable: disable
                 })}/>
        )
    }
    //endregion days
    //endregion createDays


    return (
        <Box display={'flex'} flexDirection={'column'} justifyContent={'center'} alignItems={'center'} {...props}>
            <Box width={1} display={'flex'}>
                {dayName.map((label, index) => (
                    <WeekLabel key={index}
                               label={label}/>
                ))}
            </Box>
            <Box width={1} display={'flex'} flexWrap={'wrap'}>
                {days}
            </Box>
        </Box>
    )
}


function WeekLabel({label}) {
    return (
        <Typography width={1 / 7} pt={0.5}
                    py={1} display={'flex'}
                    alignItems={'center'} justifyContent={'center'}
                    variant={'h6'}>
            {label}
        </Typography>
    )
}
