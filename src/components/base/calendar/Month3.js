import React, {useState} from "react";
import Day from "./Day";
import Box from "@material-ui/core/Box";
import _ from 'lodash';
import Typography from "../Typography";
import JDate from "../../../utils/JDate";


export const dayName = [
    'شنبه',
    'یک‌شنبه',
    'دو‌شنبه',
    'سه‌شنبه',
    'چهار‌شنبه',
    'پنج‌شنبه',
    'جمعه',
];
export default function Month3({jalali, disables = [], ...props}) {
    // const daysInLastMonth = jalali.clone().subtract(1, 'months').daysInMonth();
    const daysInMonth = jalali.daysInMonth();
    const monthFirstDay = jalali.clone().startOf('jMonth');
    // const monthLastDay = jalali.clone().endOf('jMonth');
    const disableDays = [];
    _.forEach(disables, (item, index) => {
        disableDays.push({
            startDate: JDate.timeStampToJalali(item.startDate),
            endDate: JDate.timeStampToJalali(item.endDate)
        });
    });
    const [dayRange, setDayRange] = useState({
        startDay: null,
        endDay: null
    });
    const [activeHoverDay, setActiveHoverDay] = useState(null);
    const days = [];
    const startDayOnWeek = monthFirstDay.day() + 1;

    //region createDays
    //region fakeDays
    for (let i = 0; i < startDayOnWeek; i++) {
        const day = monthFirstDay.clone().subtract(startDayOnWeek - i, 'day');
        days.push(<Day key={i}
                       day={day}
                       hidden={true}
                       disable={true}/>)
    }
    //endregion fakeDays
    //region days
    for (let i = 0; i < daysInMonth - 1; i++) {
        const {startDay, endDay} = dayRange;
        const day = monthFirstDay.clone().add(i, 'day');

        //region CheckDayIsDisable
        let disable = false;
        _.forEach(disableDays, ({startDate, endDate}) => {
            if (JDate.inRange(day, startDate, endDate)) {
                disable = true;
                return false;
            }
            if (startDay) {
                if (day.isAfter(startDate) && startDay.isBefore(startDate)) {
                    disable = true;
                    return false;
                }
                if (day.isBefore(startDate) && startDay.isAfter(startDate)) {
                    disable = true;
                    return false;
                }
            }
        });
        //endregion CheckDayIsDisable

        //region dayIsActive
        const inForwardHoveRange = (startDay && !endDay && activeHoverDay) && (startDay < activeHoverDay && activeHoverDay >= day && startDay <= day);
        const inBackHoveRange = (startDay && !endDay && activeHoverDay) && (startDay > activeHoverDay && activeHoverDay <= day && startDay >= day);
        const inRange = startDay && endDay && (day >= startDay && day <= endDay);
        let isActive = (day.isSame(startDay, 'day') || inForwardHoveRange || inBackHoveRange || inRange) ? 1 : 0;

        if (isActive) {
            if (startDay && endDay) {
                if (startDay.isSame(day, 'day')) {
                    isActive = 2;
                } else if (endDay.isSame(day, 'day')) {
                    isActive = 3;
                }
            } else if (startDay && startDay.isSame(day, 'day')) {
                isActive = (activeHoverDay === null || startDay.isSame(activeHoverDay, 'day')) ? 1 : startDay.isBefore(activeHoverDay, 'day') ? 2 : 3;
            } else if (endDay && endDay.isSame(day, 'day')) {
                isActive = (endDay.isSameOrBefore(activeHoverDay, 'day')) ? 3 : 2;
            } else if (startDay && activeHoverDay && activeHoverDay.isSame(day, 'day')) {
                isActive = activeHoverDay.isSameOrBefore(startDay, 'day') ? 2 : 3;
            }
        }
        //endregion dayIsActive

        //region handler
        function handlerItemClick() {
            if ((startDay && endDay) || (startDay && startDay.isSame(day))) {
                setDayRange({
                    ...dayRange,
                    startDay: null,
                    endDay: null
                });
                return
            }
            if (disable)
                return;
            if (startDay === null || endDay !== null) {
                setDayRange({
                    ...dayRange,
                    startDay: day,
                    endDay: null
                });
            } else if (startDay < day) {
                setDayRange({
                    ...dayRange,
                    endDay: day
                });
            } else if (!startDay.isSame(day, 'day')) {
                const endDay = dayRange.startDay;
                setDayRange({
                    ...dayRange,
                    startDay: day,
                    endDay: endDay
                });
            }
        }

        function handleOnHoverItem(enter) {

            if (startDay) {
                let disable = false;
                _.forEach(disableDays, ({startDate, endDate}) => {
                    if (JDate.inRange(day, startDate, endDate)) {
                        disable = true;
                        return false;
                    }
                });
                setActiveHoverDay(disable ? null : day);
                return
            }
            setActiveHoverDay(enter ? day : null)
        }

        //endregion handler

        days.push(
            <Day key={i + daysInMonth}
                 day={day}
                 disable={disable}
                 active={isActive}
                 onDayClick={handlerItemClick}
                 onDayHover={handleOnHoverItem}/>
        )
    }
    //endregion days
    //endregion createDays

    return (
        <Box display={'flex'} flexDirection={'column'} justifyContent={'center'} alignItems={'center'}>
            <Box width={1} display={'flex'}>
                {dayName.map((label, index) => (
                    <WeekLabel key={index}
                               label={label}/>
                ))}
            </Box>
            <Box width={1} display={'flex'} flexWrap={'wrap'}>
                {days}
            </Box>
        </Box>
    )
}


function WeekLabel({label}) {
    return (
        <Typography width={1 / 7} pt={0.5}
                    py={1} display={'flex'}
                    alignItems={'center'} justifyContent={'center'}
                    variant={'h6'}>
            {label}
        </Typography>
    )
}
