import React from "react";
import MTBreadcrumbs from '@material-ui/core/Breadcrumbs'
import Link from "./link/Link";
import Box from "@material-ui/core/Box";
import {grey} from "@material-ui/core/colors";
import _ from 'lodash'

export function Breadcrumbs({data, ...props}) {
    return (
        <div>
            <MTBreadcrumbs aria-label="product breadcrumb" separator="›" {...props} >
                {
                    data.map((item, index) => (
                        <Item item={item} key={index}/>)
                    )
                }
            </MTBreadcrumbs>
        </div>
    )
}

function Item({item, ...props}) {
    console.log("iteeem",item)
    return (
        <>
            {
                item.link ?
                    <Link color="inherit" href={item.link} {...props}>
                        {item.name}
                    </Link>
                    :
                    <Box fontWeight="fontWeightRegular"
                         fontSize='body1.fontSize'
                         component='h3'
                         {...props}
                         style={{
                             color: grey[800]
                         }}>
                        {_.truncate(item.name,35)}
                    </Box>
            }
        </>
    )
}
