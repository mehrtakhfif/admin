import React, {useCallback, useEffect, useState} from "react";
import Box from "@material-ui/core/Box";
import PropTypes from 'prop-types';
import clsx from 'clsx';
import {lighten, makeStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '../../base/Typography';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import FilterListIcon from '@material-ui/icons/FilterList';
import _ from "lodash";
import Skeleton from "@material-ui/lab/Skeleton";
import {activeLang, lang, theme} from "../../../repository";
import Collapse from "@material-ui/core/Collapse";
import {UtilsStyle} from "../../../utils/Utils";
import Filters from "./Filters";
import Link from "../link/Link";
import {ArrowDropDown, ClearAll, FirstPage, LastPage, NavigateBefore, NavigateNext} from "@material-ui/icons";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import ButtonBase from "@material-ui/core/ButtonBase";
import ButtonLink from "../link/ButtonLink";
import {cyan} from "@material-ui/core/colors";
import {useSelector} from "react-redux";
import BaseButton from "../button/BaseButton";
import {gcLog, isElement, isNumeric} from "../../../utils/ObjectUtils";

const useStyles = makeStyles(theme => ({
    root: {},
    paper: {
        width: '100%',
        marginBottom: theme.spacing(2),
        overflowX: 'auto',
    },
    table: {},
    visuallyHidden: {
        border: 0,
        clip: 'rect(0 0 0 0)',
        height: 1,
        margin: -1,
        overflow: 'hidden',
        padding: 0,
        position: 'absolute',
        top: 20,
        width: 1,
    },
    tablePagination: {
        '&>div': {
            '&>div[class*="MuiTablePagination-selectRoot"]': {
                marginLeft: theme.spacing(4),
                marginRight: theme.spacing(1)
            },
            '&>div[class*="MuiTablePagination-actions"]': {
                marginRight: theme.spacing(4),
                marginLeft: theme.spacing(1)
            }
        }
    }
}));


export default function (props) {
    return (
        _.isArray(props.data) ?
            <Ta {...props}/> :
            <React.Fragment/>
    )
}

function Ta({
                cookieKey,
                headers,
                title,
                data,
                filters,
                loading,
                rowsPerPage,
                activePage,
                rowCount,
                lastPage,
                orderType,
                activeBox,
                showActionPanel = true,
                showBox = true,
                addNewButton,
                onRowPerPageChange,
                onActivePageChange,
                onFilterChange,
                onApplyFilterClick,
                onResetFilterClick,
                onChangeOrder,
                onChangeBox,
                ...props
            }) {

    //region var
    const {orderBy} = orderType;
    const order = orderType.asc ? 'asc' : 'desc';
    const classes = useStyles();
    //region state
    const [selected, setSelected] = React.useState([]);
    const [filterState, setFilterState] = React.useState({
        open: true
    });
    const [activeData, setActiveData] = React.useState({
        data: [],
        selected: []
    });
    //endregion state
    //endregion var

    //region useEffect

    useEffect(() => {
        //activeData
        const ad = data;
        //activeSelectedData
        const asd = [];
        if (_.isEmpty(ad) || !ad[0].id)
            return;
        _.forEach(ad, (i) => {
            const res = _.findIndex(selected, (s) => {
                return s === i.id.label;
            });
            if (res !== -1)
                asd.push(i);
        });

        setActiveData({
            ...activeData,
            data: ad,
            selected: asd,
        })
    }, [selected, activePage, data]);


    useEffect(() => {
        setFilterState({
            ...filterState,
            open: false
        });
    }, [selected]);
    //endregion useEffect

    //region handler

    const handleRequestSort = (event, property) => {
        onChangeOrder({
            asc: (orderBy !== property) || order !== 'asc',
            orderBy: property
        })
    };

    const handleUnSelectAllActiveClick = event => {
        // if (event.target.checked) {
        //     const newSelecteds = [];
        //     _.forEach(data, (newItem) => {
        //         newSelecteds.push(newItem.index)
        //     });
        //     setSelected([...newSelecteds]);
        //     return;
        // }
        setSelected([])
    };

    const handleSelectAllActiveClick = event => {
        const newSelecteds = selected;
        if (event.target.checked) {
            _.forEach(activeData.data, (newItem) => {
                const res = _.findIndex(newSelecteds, (s) => {
                    return newItem.id === s
                });
                if (res === -1) {
                    newSelecteds.push(newItem.id.label)
                }
            });
            setSelected([...newSelecteds]);
            return;
        }
        _.forEach(activeData.data, (da) => {
            _.remove(newSelecteds, (ns) => {
                return ns === da.id.label
            })
        });
        setSelected([...newSelecteds])
    };

    const handleClick = (event, id) => {
        const selectedIndex = selected.indexOf(id);
        let newSelected = [];

        if (selectedIndex === -1) {
            newSelected = newSelected.concat(selected, id);
        } else if (selectedIndex === 0) {
            newSelected = newSelected.concat(selected.slice(1));
        } else if (selectedIndex === selected.length - 1) {
            newSelected = newSelected.concat(selected.slice(0, -1));
        } else if (selectedIndex > 0) {
            newSelected = newSelected.concat(
                selected.slice(0, selectedIndex),
                selected.slice(selectedIndex + 1),
            );
        }
        setSelected(newSelected);
    };

    const handleChangePage = (event, newPage) => {
        if (!_.isNumber(newPage) || newPage < 0 || newPage + 1 > lastPage)
            return;
        onActivePageChange(newPage);
    };

    const handleChangeRowsPerPage = (e, rowPerPage) => {
        if (!_.isNumber(rowPerPage))
            return;
        onRowPerPageChange(rowPerPage);
    };

    //endregion handler

    //region renderData
    const isSelected = (name) => {
        return false
        return selected.indexOf(name) !== -1
    }
    //endregion renderData

    return (
        <Box width={1} {...props}>
            {
                _.isArray(data) ?
                    (
                        <Box display={'flex'} width={1} flexDirection={'column'}>
                            <HeadPanel showBox={showBox} activeBox={activeBox} addNewButton={addNewButton}
                                       onChangeBox={onChangeBox}/>
                            <Paper className={classes.paper}>
                                {
                                    (title || filters) &&
                                    <EnhancedTableToolbar
                                        allNumSelected={selected.length}
                                        numSelected={activeData.selected.length}
                                        title={title}
                                        open={filterState.open}
                                        handleFilterIconClick={(open) => {
                                            setFilterState({
                                                ...filterState,
                                                open: open
                                            })
                                        }}
                                        handleUnSelectAllClick={handleUnSelectAllActiveClick}/>
                                }
                                {filters &&
                                <Collapse in={filterState.open}>
                                    <Box width={1} py={2} px={2} display={'flex'} justifyContent={'center'}
                                         alignItems={'center'}>
                                        <Filters filters={filters}
                                                 loading={loading}
                                                 onItemChange={onFilterChange}
                                                 onResetFilterClick={onResetFilterClick}
                                                 onApplyFilterClick={onApplyFilterClick}/>
                                    </Box>
                                </Collapse>}
                                <TableContainer style={{maxHeight: 500}}>
                                    <Table
                                        className={classes.table}
                                        stickyHeader
                                        aria-label="sticky table"
                                        size={'medium'}
                                        style={{
                                            width: "auto",
                                            tableLayout: "auto",
                                            minWidth: window.innerWidth - 100
                                        }}>
                                        <EnhancedTableHead
                                            classes={classes}
                                            numSelected={activeData.selected.length}
                                            order={order}
                                            showActionPanel={showActionPanel}
                                            orderBy={orderBy}
                                            loading={loading}
                                            data={headers}
                                            onSelectAllClick={handleSelectAllActiveClick}
                                            onRequestSort={handleRequestSort}
                                            rowCount={activeData.data.length}/>
                                        <TableBody>
                                            {
                                                data.map((row, index) => {
                                                    return (
                                                        <React.Fragment key={index}>
                                                            {(!loading && row.id) &&
                                                            <RowItem
                                                                index={index}
                                                                key={index}
                                                                handleClick={handleClick}
                                                                headers={headers}
                                                                isSelected={isSelected}
                                                                loading={loading}
                                                                row={row}/>
                                                            }
                                                        </React.Fragment>
                                                    );
                                                })}
                                        </TableBody>
                                    </Table>
                                </TableContainer>
                                {showActionPanel &&
                                <Pagination
                                    loading={loading}
                                    lastPage={lastPage}
                                    activePage={activePage}
                                    rowCount={rowCount}
                                    onChangePage={handleChangePage}
                                    rowsPerPage={rowsPerPage}
                                    onRowsPerPageChange={handleChangeRowsPerPage}/>
                                }
                            </Paper>
                        </Box>
                    ) :
                    <Skeleton variant={"rect"} height={300}/>
            }
        </Box>
    )
}

Ta.propTypes = {
    headers: PropTypes.array.isRequired,
    title: PropTypes.oneOfType([
        PropTypes.string.isRequired,
        PropTypes.object.isRequired,
    ]),
    data: PropTypes.oneOfType([
        PropTypes.array,
        PropTypes.bool,
    ]),
    filters: PropTypes.oneOfType([
        PropTypes.array,
        PropTypes.bool,
    ]),
    loading: PropTypes.bool.isRequired,
    rowsPerPage: PropTypes.number.isRequired,
    activePage: PropTypes.number.isRequired,
    lastPage: PropTypes.number.isRequired,
    orderType: PropTypes.object.isRequired,
    showActionPanel: PropTypes.bool.isRequired,
    addNewButton: PropTypes.object,
    onRowPerPageChange: PropTypes.func.isRequired,
    onActivePageChange: PropTypes.func.isRequired,
    onFilterChange: PropTypes.func.isRequired,
    onApplyFilterClick: PropTypes.func.isRequired,
    onResetFilterClick: PropTypes.func.isRequired,
    onChangeOrder: PropTypes.func.isRequired,
    onChangeBox: PropTypes.func,
    // order: PropTypes.oneOf(['asc', 'desc']).isRequired,
    // orderBy: PropTypes.string.isRequired,
    rowCount: PropTypes.number.isRequired,
};

//region DataGenerator
export const headerItemTemplate = {
    id: createTableHeader({id: "id", type: 'num', label: 'ID', hasPadding: false}),
    actions: createTableHeader({
        id: "actions",
        type: 'str',
        label: 'عملیات',
        sortable: false,
        hasPadding: false,
        style: {width: 60}
    }),
    name: createTableHeader({id: "name", type: 'str', label: 'نام'}),
    createdAt: createTableHeader({id: "created_at", type: 'str', label: 'تاریخ ساخت', dir: 'ltr'}),
    createdBy: createTableHeader({id: "created_by", type: 'str', label: 'ساخت توسط'}),
    updatedAt: createTableHeader({id: "updated_at", type: 'str', label: 'تاریخ بروزرسانی', dir: 'ltr'}),
    updatedBy: createTableHeader({id: "updated_by", type: 'str', label: 'بروزرسانی توسط'}),
    idAction: () => [headerItemTemplate.id, headerItemTemplate.actions],
    idActionName: () => [...headerItemTemplate.idAction(), headerItemTemplate.name],
    createByUpdatedBy: () => [headerItemTemplate.updatedAt, headerItemTemplate.updatedBy, headerItemTemplate.createdAt, headerItemTemplate.createdBy],
};

export function createTableHeader({
                                      id,
                                      type = 'bool' || 'num' || 'str',
                                      label,
                                      hasPadding = true,
                                      dir,
                                      sortable = true,
                                      onClick,
                                      style,
                                      ...props
                                  }) {
    return {
        id: id,
        type: type,
        label: label,
        hasPadding: hasPadding,
        dir: dir,
        sortable: sortable,
        onClick: onClick,
        style,
        ...props
    };
};

//endregion DataGenerator

function RowItem({index, handleClick, isSelected, loading, headers, row}) {
    const [isItemSelected, setIsItemSelected] = useState();
    const labelId = `enhanced-table-checkbox-${index}`;

    useEffect(() => {
        setIsItemSelected(row.id ? isSelected(row.id.label) : false)
    }, [row]);

    return (
        <TableRow
            hover
            onClick={event => handleClick(event, row.id.label)}
            role="checkbox"
            aria-checked={isItemSelected}
            tabIndex={-1}
            key={index}
            selected={isItemSelected}>
            <TableCell padding="checkbox">
                <Checkbox
                    checked={isItemSelected}
                    disabled={loading}
                    inputProps={{'aria-labelledby': labelId}}/>
            </TableCell>
            {
                headers.map((h, i) => {
                    const item = row[h.id];
                    return (
                        <React.Fragment>
                            {_.isObject(item) &&
                            <TableCell key={i} component="th"
                                       id={labelId}
                                       scope="row"
                                       padding={h.hasPadding ? "none" : "checkbox"}
                                       align="left"
                                       style={{
                                           direction: h.dir,
                                           ...UtilsStyle.widthFitContent()
                                       }}>
                                <Box pl={2}>
                                    {item.label}
                                </Box>
                            </TableCell>}
                        </React.Fragment>
                    )
                })
            }
        </TableRow>
    )
}

function HeadPanel({showBox, addNewButton, activeBox, onChangeBox, ...props}) {
    const [boxSelector, setBoxSelector] = useState();
    const {boxes} = useSelector(state => state.user);

    function handleCloseBoxSelector(e, box) {
        setBoxSelector(null);
        if (onChangeBox)
            onChangeBox(e, box)
    }

    const box = _.find(showBox ? boxes : [], (b) => {
        return b.id === activeBox
    });

    return (
        <Box display={'flex'} mb={2} width={1} flexWrap={'wrap'} alignItems={'center'}>
            {
                isElement(addNewButton) ?
                    addNewButton
                    :
                    _.isObject(addNewButton) ?
                        addNewButton.link ?
                            <ButtonLink
                                backgroundColor={cyan[400]}
                                color={'#fff'}
                                toHref={addNewButton.link}>
                                {addNewButton.label}
                            </ButtonLink> :
                            addNewButton.onClick ?
                                <BaseButton
                                    onClick={(e) => {
                                        try {
                                            addNewButton.onClick();
                                            e.stopPropagation();
                                        } catch (e) {

                                        }
                                    }}
                                    style={{
                                        backgroundColor: cyan[400],
                                        color: '#fff'
                                    }}>
                                    {addNewButton.label}
                                </BaseButton> : null : null}
            {(boxes && !_.isEmpty(boxes)) &&
            <Box mr={2}>
                {box &&
                <Box display={'flex'} alignItems={'center'}>
                    <Typography variant={'body1'} mr={1.5} ml={0.5}>
                        {lang.get('active_box')}:
                    </Typography>
                    <ButtonBase
                        onClick={(e) => setBoxSelector(e.currentTarget)}
                        style={{
                            border: `1px solid ${cyan[200]}`,
                            ...UtilsStyle.borderRadius(5),
                            ...UtilsStyle.disableTextSelection(),
                        }}>
                        <Box display={'flex'} alignItems={'center'}
                             py={0.75}
                             px={1}
                             style={{current: 'pointer'}}>
                            <Typography variant={'body1'} ml={0.5}>
                                {box.name[activeLang]}
                            </Typography>
                            <ArrowDropDown/>
                        </Box>
                    </ButtonBase>
                </Box>}
                <Menu
                    id="simple-menu"
                    anchorEl={boxSelector}
                    keepMounted
                    open={Boolean(boxSelector)}
                    onClose={(e) => {
                        handleCloseBoxSelector(e)
                    }}>
                    {
                        boxes.map((b, index) => (
                            <MenuItem key={index}
                                      onClick={(e) => handleCloseBoxSelector(e, b)}><Typography>{b.name[activeLang]}</Typography></MenuItem>
                        ))
                    }
                </Menu>
            </Box>}
        </Box>
    )
}

function Pagination({activePage, lastPage, loading, rowCount, rowsPerPage, onChangePage, onRowsPerPageChange, ...props}) {
    const [pageSelector, setPageSelector] = useState(null);
    const [rowsPerPageSelector, setRowsPerPageSelector] = useState(null);
    const rowsPerPageOptions = [5, 10, 25, 50, 100];

    function handleClosePageSelector(e, page) {
        setPageSelector(null);
        onChangePage(e, page)
    }

    function handleCloseRowsPerPageSelector(e, rowPerPage) {
        setRowsPerPageSelector(null);
        onRowsPerPageChange(e, rowPerPage)
    }

    return (
        <Box display={'flex'} flexWrap={'wrap'}
             flexDirection={'row-reverse'} alignItems={'center'}
             px={3} py={0.5}>
            <Box display={'flex'} ml={2}>
                <Box display={'flex'} alignItems={'center'}>
                    <Typography variant={'body2'}>
                        {lang.get("go_to_page")}:
                    </Typography>
                    <ButtonBase disabled={loading} onClick={(e) => setPageSelector(e.currentTarget)}>
                        <Typography variant={'body2'} pr={1}>
                            {activePage + 1}
                        </Typography>
                        <ArrowDropDown/>
                    </ButtonBase>
                </Box>
                <IconButton
                    aria-label="first page"
                    disabled={loading || activePage <= 0}
                    onClick={(e) => onChangePage(e, 0)}>
                    <LastPage/>
                </IconButton>
                <IconButton aria-label="before page"
                            disabled={loading || activePage <= 0}
                            onClick={(e) => onChangePage(e, activePage - 1)}>
                    <NavigateNext/>
                </IconButton>
                <IconButton aria-label="next page"
                            disabled={loading || activePage + 1 >= lastPage}
                            onClick={(e) => onChangePage(e, activePage + 1)}>
                    <NavigateBefore/>
                </IconButton>
                <IconButton aria-label="last page"
                            disabled={loading || activePage + 1 >= lastPage}
                            onClick={(e) => onChangePage(e, lastPage - 1)}>
                    <FirstPage/>
                </IconButton>
            </Box>
            <Typography variant={'body2'} mr={1}>
                {`${lang.get("total_count")}: ${rowCount}` + "  |  " + `${lang.get("page")} ${activePage + 1} از ${lastPage}`}
            </Typography>
            <Box p={2} display={'flex'} alignItems={'center'}>
                <Typography variant={'body2'}>
                    {lang.get("count_in_page")}:
                </Typography>
                <ButtonBase onClick={(e) => setRowsPerPageSelector(e.currentTarget)} disabled={loading}>
                    <Typography variant={'body2'} pr={1}>
                        {rowsPerPage}
                    </Typography>
                    <ArrowDropDown/>
                </ButtonBase>
            </Box>
            <Menu
                id="simple-menu"
                anchorEl={pageSelector || rowsPerPageSelector}
                keepMounted
                open={Boolean(pageSelector || rowsPerPageSelector)}
                onClose={(e) => {
                    if (pageSelector) {
                        handleClosePageSelector(e);
                        return
                    }
                    handleCloseRowsPerPageSelector(e);
                }}>
                {pageSelector ? [...Array(lastPage)].map((i, index) => (
                        <MenuItem key={index} onClick={(e) => handleClosePageSelector(e, index)}>{index + 1}</MenuItem>
                    )) :
                    rowsPerPageOptions.map((o, index) => (
                        <MenuItem key={index} onClick={(e) => handleCloseRowsPerPageSelector(e, o)}>{o}</MenuItem>
                    ))}
            </Menu>
        </Box>
    )
}

function EnhancedTableHead(props) {
    const {data, loading, classes, showActionPanel, onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort} = props;

    return (
        <TableHead>
            <TableRow>
                <TableCell padding="checkbox">
                    <Checkbox
                        indeterminate={numSelected > 0 && numSelected < rowCount}
                        checked={rowCount > 0 && numSelected === rowCount}
                        disabled={loading}
                        onChange={onSelectAllClick}
                        inputProps={{'aria-label': 'select all desserts'}}
                    />
                </TableCell>
                {data.map(headCell => (
                    <TableCell
                        key={headCell.id}
                        align={'left'}
                        padding={headCell.id === 'id' ? "checkbox" : "none"}
                        sortDirection={orderBy === headCell.id ? order : false}
                        style={{
                            direction: 'ltr',
                            ...headCell.style
                        }}>
                        <TableSortLabel
                            disabled={loading}
                            hideSortIcon={!headCell.sortable}
                            active={orderBy === headCell.id}
                            direction={orderBy === headCell.id ? order : 'asc'}
                            onClick={(e) => {
                                if (!headCell.sortable)
                                    return;
                                onRequestSort(e, headCell.id);
                            }}
                            style={{
                                width: 'max-content',
                                paddingRight: !headCell.sortable ? theme.spacing(2) : 0
                            }}>
                            {headCell.label}
                        </TableSortLabel>
                    </TableCell>
                ))}
            </TableRow>
        </TableHead>
    );
}

EnhancedTableHead.propTypes = {
    classes: PropTypes.object.isRequired,
    numSelected: PropTypes.number.isRequired,
    onRequestSort: PropTypes.func.isRequired,
    onSelectAllClick: PropTypes.func.isRequired,
    order: PropTypes.oneOf(['asc', 'desc']).isRequired,
    orderBy: PropTypes.string.isRequired,
    rowCount: PropTypes.number.isRequired,
};

const useToolbarStyles = makeStyles(theme => ({
    root: {
        paddingLeft: theme.spacing(2),
        paddingRight: theme.spacing(1),
    },
    highlight:
        theme.palette.type === 'light'
            ? {
                color: theme.palette.secondary.main,
                backgroundColor: lighten(theme.palette.secondary.light, 0.85),
            }
            : {
                color: theme.palette.text.primary,
                backgroundColor: theme.palette.secondary.dark,
            },
    title: {
        flex: '1 1 100%',
    },
}));

const EnhancedTableToolbar = props => {
    const classes = useToolbarStyles();
    const {allNumSelected, numSelected, title, open, handleFilterIconClick, handleUnSelectAllClick} = props;

    return (
        <Toolbar
            className={clsx(classes.root, {
                [classes.highlight]: allNumSelected > 0,
            })}>
            {allNumSelected > 0 ? (
                <Box display={'flex'}
                     flexDirection={'column'}
                     width={1}
                     style={{...UtilsStyle.disableTextSelection()}}>
                    <Typography className={classes.title} color="inherit" variant="subtitle1">
                        {`${lang.get("selected")}: ${allNumSelected}`}
                    </Typography>
                    {numSelected > 0 ? (
                        <Typography className={classes.title} color="inherit" variant="subtitle2" pt={0.3}>
                            {`${lang.get("selected_in_page")}: ${numSelected}`}
                        </Typography>
                    ) : null}

                </Box>
            ) : (
                <Typography className={classes.title} variant="h5" id="tableTitle"
                            style={{...UtilsStyle.disableTextSelection()}}>
                    {title}
                </Typography>
            )}

            {allNumSelected > 0 ? (
                <Tooltip title="لغو تمام انتخاب شدها">
                    <IconButton aria-label="un select" onClick={handleUnSelectAllClick}>
                        <ClearAll/>
                    </IconButton>
                </Tooltip>
            ) : (
                <Tooltip title={lang.get(open ? 'closing_filter_list' : 'opening_filter_list')}>
                    <IconButton aria-label="filter list" onClick={() => handleFilterIconClick(!open)}>
                        <FilterListIcon style={{
                            transform: `rotateX(${open ? 180 : 0}deg)`,
                            ...UtilsStyle.transition(300)
                        }}/>
                    </IconButton>
                </Tooltip>
            )}
        </Toolbar>
    );
};

EnhancedTableToolbar.propTypes = {
    allNumSelected: PropTypes.number.isRequired,
    numSelected: PropTypes.number.isRequired,
    title: PropTypes.oneOfType([
        PropTypes.string.isRequired,
        PropTypes.object.isRequired,
    ]),
    open: PropTypes.bool.isRequired,
    handleFilterIconClick: PropTypes.func.isRequired,
};

