import React, {useEffect, useState} from "react";
import Box from "@material-ui/core/Box";
import ComponentError from "../../base/ComponentError";
import {lang} from "../../../repository";
import MultiSelectAutoFill from '../autoFill/MultiSelectAutoFill'
import {cyan, grey, orange} from "@material-ui/core/colors";
import {UtilsStyle} from "../../../utils/Utils";
import Typography from "../Typography";
import {useTheme} from "@material-ui/core";
import BaseButton from "../button/BaseButton";
import {FilterList, RotateLeft} from "@material-ui/icons";
import {filterType} from "../../../utils/DataUtils";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Switch from "@material-ui/core/Switch";
import _ from 'lodash'
import AutoFill from "../autoFill/AutoFill";
import TextFieldContainer from "../textField/TextFieldContainer";
import {createName} from "../textField/TextFieldMultiLanguageContainer";
import TextField from "../textField/TextField";
import {gcLog} from "../../../utils/ObjectUtils";

export default function ({filters, loading, onItemChange, onResetFilterClick, onApplyFilterClick, ...props}) {
    const theme = useTheme();
    const [filterState, setFilterState] = useState({
        data: {
            defFilters: [],
            boolFilters: [],
        }
    });
    useEffect(() => {
        if (!_.isArray(filters))
            return;
        const listOne = [];
        const listTwo = []
        _.forEach(filters, (fi) => {
            if (fi.type !== filterType.bool) {
                listOne.push(fi);
                return;
            }
            listTwo.push(fi);
        });

        setFilterState({
            ...filterState,
            data: {
                ...filterState.data,
                defFilters: listOne,
                boolFilters: listTwo,
            }
        });
    }, [filters]);
    const [init, setInit] = useState({})

    const {defFilters, boolFilters} = filterState.data;
    return (
        <Box width={1}
             p={2}
             mb={1}>
            {_.isArray(filters) ?
                <Box display={'flex'} width={1} flexDirection={'column'}>
                    <Typography variant={'body1'} pt={2} pb={1} style={{...UtilsStyle.disableTextSelection()}}>
                        {lang.get('filtering')}:
                    </Typography>
                    <Box width={1} display={'flex'} flexWrap={'wrap'}
                         alignItems={'flex-start'}>
                        {defFilters.map((fi, index) => (
                            <ItemContainer key={index}>
                                {
                                    fi.type === filterType.text ?
                                        <Box width={'80%'}>
                                            <TextFieldContainer
                                                name={createName({name: _.toString(index), group: 'tableFilter'})}
                                                defaultValue={fi.value}
                                                onChange={(val) => {
                                                    if (!init[index]) {
                                                        const nInit = init;
                                                        init[index] = true
                                                        setInit({...nInit})
                                                        return
                                                    }
                                                    onItemChange({
                                                        item: fi,
                                                        value: val
                                                    })
                                                }}
                                                onChangeDelay={500}
                                                render={(ref, {name: inputName, initialize, valid, errorIndex, setValue, inputProps, style, props}) => {
                                                    return (
                                                        <TextField
                                                            {...props}
                                                            error={!valid}
                                                            name={inputName}
                                                            inputRef={ref}
                                                            fullWidth
                                                            label={fi.label}
                                                            placeholder={fi.placeholder}
                                                            style={{
                                                                ...style,
                                                                minWidth: '90%'
                                                            }}
                                                            inputProps={{
                                                                ...inputProps,
                                                            }}/>
                                                    )
                                                }}/>
                                        </Box>
                                        :
                                        (fi.type === filterType.multi) ?
                                            <MultiSelectAutoFill
                                                isMulti={fi.type === filterType.multi}
                                                inputId={fi.label}
                                                title={fi.label}
                                                items={fi.data}
                                                activeItems={fi.value}
                                                placeholder={fi.placeholder}
                                                width={'80%'}
                                                onActiveItemChanged={(item) => {
                                                    onItemChange({
                                                        item: fi,
                                                        value: item
                                                    })
                                                }}/>
                                            : fi.type === filterType.single ?
                                            <AutoFill
                                                inputId={fi.label}
                                                title={fi.label}
                                                items={fi.data}
                                                activeItem={fi.value}
                                                placeholder={fi.placeholder}
                                                width={'80%'}
                                                onActiveItemChanged={(item) => {
                                                    onItemChange({
                                                        item: fi,
                                                        value: item
                                                    })
                                                }}/> : null}
                            </ItemContainer>
                        ))}
                        {boolFilters.map((fi, index) => (
                            <Box key={index} display={'flex'} flexWrap={'wrap'}
                                 pt={1}
                                 ml={2}
                                 alignItems={'center'}>
                                <FormControlLabel
                                    control={
                                        <Switch checked={fi.value}
                                                onChange={(e) => {
                                                    onItemChange({
                                                        item: fi,
                                                        value: !fi.value
                                                    })

                                                }}
                                                value={fi.label}/>
                                    }
                                    label={fi.label}
                                    labelPlacement="top"/>
                            </Box>
                        ))}
                    </Box>
                    <Box display={'flex'} flexDirection={'row-reverse'} mt={3.5} ml={2} flexWrap={'wrap'}>
                        <BaseButton
                            disabled={loading}
                            onClick={onApplyFilterClick}
                            style={{
                                marginRight: theme.spacing(1.5),
                                backgroundColor: cyan[400],
                                color: '#fff'
                            }}>
                            {lang.get("apply_filters")}
                            <FilterList style={{marginRight: theme.spacing(0.25)}}/>
                        </BaseButton>
                        <BaseButton
                            variant={'outlined'}
                            disabled={loading}
                            onClick={onResetFilterClick}
                            style={{
                                borderColor: orange[400],
                                color: grey[600]
                            }}>
                            {lang.get("reset")}
                            <RotateLeft style={{marginRight: theme.spacing(0.25)}}/>
                        </BaseButton>
                    </Box>
                </Box> :
                <ComponentError
                    message={lang.get('er_problem_call_support')} tryAgainLabel={null}/>}
        </Box>
    )
}

//region Component
//region ItemContainer
function ItemContainer(props) {
    return (
        <Box display='flex'
             justifyContent={['center', 'start', 'start']}
             width={['100%', '50%', '50%']}
             mt={2}
             mb={1}
             {...props}>
            {props.children}
        </Box>
    )
}

//endregion ItemContainer
//endregion Component


//region DataGenerator
export function createFilter({
                                 id,
                                 type = filterType.bool || filterType.text || filterType.multi || filterType.single,
                                 label,
                                 placeholder = "انتخاب...",
                                 data = null,
                                 value = null,
                                 defaultValue = null,
                             }) {
    if (value === null && defaultValue !== null) {
        value = defaultValue;
    }
    return {id, type, label, placeholder, data, value, defaultValue};
}

export function createFilterData({id, label}) {
    return {id, label};
}

//endregion DataGenerator