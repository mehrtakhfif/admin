import React from "react";
import Box from "@material-ui/core/Box";


export default function ({onRightClick, ...props}) {
    return (
        <Box
            {...props}
            onContextMenu={(e) => {
                if (e.type !== 'contextmenu') {
                    return
                }
                onRightClick(e)
                e.preventDefault();
                return false
            }}>
            {props.children}
        </Box>
    )
}
