import React from "react";
import RightClickHandler from "./RightClickHandler";
import Menu from "@material-ui/core/Menu";
import MaterialMenuItem from "@material-ui/core/MenuItem";
import Box from "@material-ui/core/Box";
import Link from "../link/Link";
import BaseButton from "../button/BaseButton";
import Typography from "../Typography";
import ButtonBase from "@material-ui/core/ButtonBase";

export function MenuItem({href, onClick, ...props}) {

    return (
        href ?
            <Link linkStyle={{minWidth: '100%'}} toHref={href} {...props}>
                {props.children}
            </Link> :
            onClick ?
                <ButtonBase onClick={onClick}
                            {...props}
                            style={{
                                display: 'flex',
                                ...props.style,
                            }}>
                    {props.children}
                </ButtonBase> :
                <React.Fragment>
                    {props.children}
                </React.Fragment>
    )
}

export default function ({items = [], ...props}) {
    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <RightClickHandler
            onRightClick={handleClick}
            {...props}>
            {props.children}
            <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}>
                {
                    items.map((it, i) =>
                        <MaterialMenuItem
                            key={i} onClick={handleClose}
                            style={{
                                display: 'block !important',
                                width: "100%"
                            }}>
                            <Box width={1}>
                                {it}
                            </Box>
                        </MaterialMenuItem>
                    )
                }
            </Menu>
        </RightClickHandler>
    )
}
