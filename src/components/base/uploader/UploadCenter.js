import React, {Fragment, useCallback, useEffect, useRef, useState} from "react";
import Box from "@material-ui/core/Box";
import {cyan, red} from "@material-ui/core/colors";
import {UtilsStyle} from "../../../utils/Utils";
import _ from "lodash";
import {Card, CircularProgress, ThemeProvider, useTheme} from "@material-ui/core";
import BaseButton from "../button/BaseButton";
import TableContainer from "@material-ui/core/TableContainer";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import Img from "../img/Img";
import Tooltip from "@material-ui/core/Tooltip";
import IconButton from "@material-ui/core/IconButton";
import {Check, Close} from "@material-ui/icons";
import {useDropzone} from "react-dropzone";
import Button from "@material-ui/core/Button";
import {colors, lang, siteLang, theme} from "../../../repository";
import {useSnackbar} from "notistack";
import ImagePlaceholder from "../../../drawable/fileType/image-placeholder.jpg";
import LinearProgress from "@material-ui/core/LinearProgress";
import Typography from "../Typography";
import ControllerSite from "../../../controller/ControllerSite";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import {errorList, notValidErrorTextField} from "../textField/TextFieldContainer";
import TextFieldMultiLanguageContainer from "../textField/TextFieldMultiLanguageContainer";
import FormControl from "../formController/FormController";
import TextField from "../textField/TextField";
import {createMultiLanguage} from "../../../controller/converter";
import {gcLog} from "../../../utils/ObjectUtils";

export default function UploadCenter({
                             boxId,
                             maxUploadCount,
                             uploadFileTypes,
                             onSelected,
                             imageSize,
                             type = 'image',
                             ...props
                         }) {
    const theme = useTheme();
    const ref = useRef();
    const [state, setState] = useState({
        key: Math.round(Math.random() * 1000),
        files: [],
        check: [],
        uploading: false,
        uploadedFile: []
    });
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();
    const [uploadProgress, setUploadProgress] = useState(0);
    const [title, setTitle] = useState({
        title: {},
        open: false,
    });

    const hasFile = (!_.isEmpty(state.files) || !_.isEmpty(state.uploadedFile));
    const onDrop = useCallback((acceptedFiles) => {
        if (acceptedFiles && acceptedFiles.length > maxUploadCount) {
            enqueueSnackbar(`حداکثر تعداد آپلود: ${maxUploadCount}`,
                {
                    variant: "error",
                    action: (key) => (
                        <Fragment>
                            <Button onClick={() => {
                                closeSnackbar(key)
                            }}>
                                {lang.get('close')}
                            </Button>
                        </Fragment>
                    )
                });
            return;
        }

        let typeCheck = true;
        _.forEach(acceptedFiles, (f) => {
            _.forEach(uploadFileTypes, (ut) => {
                if (!typeCheck)
                    return false;
                const check = !!f.type.match(ut);
                if (!check) {
                    typeCheck = false;
                    return false;
                }
            });
        });
        if (!typeCheck) {
            enqueueSnackbar(`فایل غیر مجاز میباشد.`,
                {
                    variant: "error",
                    action: (key) => (
                        <Fragment>
                            <Button onClick={() => {
                                closeSnackbar(key)
                            }}>
                                {lang.get('close')}
                            </Button>
                        </Fragment>
                    )
                });
            return;
        }
        setState({
            ...state,
            files: acceptedFiles,
            check: [],
        })
    }, []);
    const {getRootProps, getInputProps, isDragActive, inputRef} = useDropzone({onDrop});
    const rootProps = _.isEmpty(state.files) ? getRootProps() : {};


    useEffect(() => {
        if (_.isEmpty(state.files))
            return;

        _.forEach(state.files, (f, index) => {
            if (imageSize) {
                getImageSize(f, ({width, height}) => {
                    const check = state.check;
                    check[index] = !(imageSize.width && imageSize.height) ||!((width && width !== imageSize.width) || (height && height !== imageSize.height));
                    setState({
                        ...state,
                        check: [...check]
                    });
                })
                return
            }
            const check = state.check;
            check[index] = true;
            setState({
                ...state,
                check: [...check]
            });
        });
        return;

        //Todo: check line
        // uploadImage();
    }, [state.files]);

    useEffect(() => {
        if (state.files.length !== state.check.length)
            return;
        let hasUndefined = false;
        let hasFalse = false;
        _.forEach(state.check, (c) => {
            if (c === undefined) {
                hasUndefined = true;
                return false;
            }
            if (c === false) {
                hasFalse = true;
                return false;
            }
        });
        if (hasUndefined)
            return;
        if (hasFalse) {
            enqueueSnackbar(`عکس باید در اندازه ( ${imageSize.width} * ${imageSize.height} ) باشد.`,
                {
                    variant: "error",
                    action: (key) => (
                        <Fragment>
                            <Button onClick={() => {
                                closeSnackbar(key)
                            }}>
                                {lang.get('close')}
                            </Button>
                        </Fragment>
                    )
                });
            resetState();
            return;
        }
        if (!_.isEmpty(state.files)) {
            uploadImage()
        }
    }, [state.check]);

    function uploadImage() {

        setUploadProgress(0);
        if (_.isEmpty(state.files)) {
            resetState();
            setTitle({
                ...title,
                open: false
            });
            return
        }

        if (!state.uploading)
            setState({
                ...state,
                uploading: true,
            });

        getTitle();
    }

    function getTitle() {
        setTitle({
            ...title,
            open: true
        })
    }

    function closeTitleDialog(submit) {
        if (submit) {
            let errorText = "";
            try {
                if (ref.current.hasError()) {
                    errorText = "فرم مشکل دارد";
                    try {
                        const el = ref.current.getErrorElement();
                        const t = el.getAttribute(notValidErrorTextField);
                        errorText = t ? t : errorText;
                        el.focus();
                    } catch (e) {
                    }
                    reqCancel(errorText);
                    return;
                }
            } catch (e) {

            }
            uploadImageAfterSetTitle()
        }
        setTitle({
            ...title,
            open: false
        })
    }


    function reqCancel(text = null) {
        if (text)
            enqueueSnackbar(text,
                {
                    variant: "error",
                    action: (key) => (
                        <Fragment>
                            <Button onClick={() => {
                                closeSnackbar(key)
                            }}>
                                {lang.get('close')}
                            </Button>
                        </Fragment>
                    )
                });

        setState({
            loading: false
        })
    }

    function uploadImageAfterSetTitle() {
        try {
            const {name} = ref.current.serialize();

            ControllerSite.Media.upload(
                {
                    boxId: boxId,
                    file: state.files[0],
                    type: type,
                    title: name,
                    onUploadProgress: (progressEvent) => {
                        setUploadProgress(Math.round((progressEvent.loaded * 100) / progressEvent.total));
                    }
                }
            ).then(res => {
                const newState = state;
                newState.files.splice(0, 1);
                newState.uploadedFile.push(res.data.media);
                setState({
                    ...newState
                });

                setTimeout(() => {
                    setTitle({
                        ...title,
                        title: name
                    });
                }, 1000)
                uploadImage();
            }).catch(() => {
                enqueueSnackbar(`آپلود یک عکس به مشکل برخورده است.`,
                    {
                        variant: "error",
                        action: (key) => (
                            <Fragment>
                                <Button onClick={() => {
                                    closeSnackbar(key)
                                }}>
                                    {lang.get('close')}
                                </Button>
                            </Fragment>
                        )
                    });
                const newState = state;
                newState.files.splice(0, 1);
                setState({
                    ...newState
                });
            });
        } catch (e) {

        }
    }

    function resetState(nState) {
        setState({
            ...state,
            files: [],
            check: [],
            uploading: false,
            ...nState
        })
    }

    return (
        <Box
            display={'flex'}
            alignItems={!hasFile ? 'stretch' : 'unset'}
            flexDirection={!hasFile ? 'row' : 'column'}
            px={3}
            py={2}
            mb={4}
            height={1}>
            <Box width={1}
                 display={'flex'}
                 py={!hasFile ? 0 : 2}
                 alignItems={!hasFile ? 'stretch' : 'unset'}
                 {...rootProps}>
                <input name={`image-selector-${state.key}`} {...getInputProps()} />
                <Box px={7} py={8} display={'flex'}
                     width={1}
                     height={1}
                     minHeight={250}
                     alignItems={'center'}
                     justifyContent={'center'}
                     flexDirection={'column'}
                     style={{
                         background: theme.palette.action.selected,
                         ...UtilsStyle.borderRadius(!hasFile ? 0 : 5)
                     }}>
                    {
                        _.isEmpty(state.files) ?
                            isDragActive ?
                                <Typography variant={'h4'} fontWeight={700} mt={5} color={theme.palette.text.secondary}
                                            style={{...UtilsStyle.disableTextSelection()}}>
                                    فایل را رها کنید.
                                </Typography> :
                                <React.Fragment>
                                    <BaseButton variant={"outlined"}>
                                        <Typography variant={'h6'} py={2} px={3.5}>
                                            برای آپلود فایل کلیک کنید
                                        </Typography>
                                    </BaseButton>
                                    <Typography variant={'h6'} mt={5} color={theme.palette.text.primary}
                                                style={{...UtilsStyle.disableTextSelection()}}>
                                        یا فایل را در اینجا رها کنید.
                                    </Typography>
                                </React.Fragment> :
                            <Box>
                                {
                                    state.uploading ?
                                        <Box>
                                            در حال آپلود...
                                        </Box> :
                                        <CircularProgress/>
                                }
                            </Box>
                    }
                </Box>
            </Box>
            {
                !_.isEmpty(state.files) ?
                    <Box display={'flex'} flexDirection={'column'}>
                        {state.files.map((m, i) => (
                            <UploadImageItem key={i} file={m}
                                             uploadProgress={i === 0 ? uploadProgress : undefined}/>
                        ))}
                    </Box> : null
            }
            {
                (!_.isEmpty(state.uploadedFile)) ?
                    <TableContainer component={Card}
                                    style={{
                                        overflow: 'initial'
                                    }}>
                        <Table aria-label="simple table">
                            <TableHead>
                                <TableRow>
                                    <TableCell align="center" padding={"checkbox"}>id</TableCell>
                                    <TableCell align="center" padding={"checkbox"}>عکس</TableCell>
                                    <TableCell align="center" padding={"checkbox"}>عملیات</TableCell>
                                    <TableCell align="right">تایتل</TableCell>
                                    <TableCell align="right">زمان ساخت</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {state.uploadedFile.map((f, index) => {
                                    const size = 70;
                                    return (
                                        <TableRow key={index}>
                                            <TableCell align="right">
                                                {index + 1}
                                            </TableCell>
                                            <TableCell align="right">
                                                <Box px={3}>
                                                    <Img width={size} minHeight={1}
                                                         src={f.image}
                                                         display='flex'
                                                         imgStyle={{
                                                             width: size,
                                                             height: 'auto',
                                                             overflow: 'auto',
                                                             alignItems: 'center',
                                                             ...UtilsStyle.borderRadius(5)
                                                         }}/>
                                                </Box>
                                            </TableCell>
                                            <TableCell align="center">
                                                <Box display={'flex'} alignItems={'center'} px={3}>
                                                    <Box pl={1}>
                                                        <Tooltip title="انتخاب">
                                                            <IconButton
                                                                onClick={() => {
                                                                    onSelected(f)
                                                                }}>
                                                                <Check style={{
                                                                    color: cyan[500]
                                                                }}/>
                                                            </IconButton>
                                                        </Tooltip>
                                                    </Box>
                                                    <Tooltip title="حذف">
                                                        <IconButton size={'small'}>
                                                            <Close fontSize={"small"}
                                                                   style={{
                                                                       color: red[200]
                                                                   }}/>
                                                        </IconButton>
                                                    </Tooltip>
                                                </Box>
                                            </TableCell>
                                            <TableCell align="right">
                                                {f.title.fa}
                                            </TableCell>
                                            <TableCell align="right">{f.createdAt.date}</TableCell>
                                        </TableRow>
                                    )
                                })}
                            </TableBody>
                        </Table>
                    </TableContainer> : null
            }
            <Dialog
                open={title.open && !_.isEmpty(state.files)}
                onClose={closeTitleDialog}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description">
                <Box display={'flex'} flexDirection={'column'} alignItems={'center'} py={2} px={2}>
                    <Typography variant={'h6'} mb={2} style={{width: '100%'}}>
                        آپلود
                    </Typography>
                    {(state.files && state.files[0]) &&
                    <ShowFileImage file={state.files[0]}/>
                    }
                    <FormControl minWidth={500} mt={2} name={"uploadCenter"} innerref={ref}>
                        <TextFieldMultiLanguageContainer
                            name={"name"}
                            render={(ref, {lang: inputLang, name: inputName, initialize, valid, errorIndex, setValue, inputProps, style, props}) => {
                                let title = createMultiLanguage();
                                try {
                                    title = state.uploadedFile[state.uploadedFile.length - 1].title;
                                } catch (e) {
                                }
                                return (
                                    <TextField
                                        {...props}
                                        error={!valid}
                                        variant="outlined"
                                        name={inputName}
                                        defaultValue={title[inputLang.key]}
                                        fullWidth
                                        helperText={errorList[errorIndex]}
                                        inputRef={ref}
                                        requestFocus={true}
                                        required={inputLang.key === siteLang}
                                        label={'نام'}
                                        style={style}
                                        inputProps={{
                                            ...inputProps
                                        }}/>
                                )
                            }}/>
                    </FormControl>
                </Box>
                <DialogActions>
                    <BaseButton
                        onClick={() => {
                            setTimeout(() => {
                                closeTitleDialog(true)
                            }, 1000)
                        }}
                        style={{
                            marginLeft: theme.spacing(1.5),
                            backgroundColor: colors.success.main,
                            color: '#fff'
                        }}>
                        ذخیره
                    </BaseButton>
                    <BaseButton
                        variant={'text'}
                        onClick={() => {
                            closeTitleDialog();
                            resetState();
                        }}
                        style={{
                            color: colors.danger.main
                        }}>
                        لغو
                    </BaseButton>
                </DialogActions>
            </Dialog>
        </Box>
    )
}

function ShowFileImage({file}) {
    const [src, setSrc] = useState(ImagePlaceholder);
    useEffect(() => {
        const reader = new FileReader();
        reader.onload = function (e) {
            setSrc(e.target.result);
        };
        reader.readAsDataURL(file);
    }, [file]);

    return (
        <Img
            width={250}
            src={src}
            display='flex'
            imgStyle={{
                width: 250,
                height: 'auto',
                overflow: 'auto',
                alignItems: 'center',
                ...UtilsStyle.borderRadius(5)
            }}/>
    )
}


function UploadImageItem({file, uploadProgress = 0, ...props}) {
    const [src, setSrc] = useState(ImagePlaceholder);
    useEffect(() => {
        const reader = new FileReader();
        reader.onload = function (e) {
            setSrc(e.target.result);
        };
        reader.readAsDataURL(file);
    }, [file]);
    const size = 100;
    return (
        <Box display={'flex'} alignItems='center' my={1}>
            <Img width={size} minHeight={1}
                 src={src}
                 display='flex'
                 imgStyle={{
                     width: size,
                     height: 'auto',
                     overflow: 'auto',
                     alignItems: 'center',
                     ...UtilsStyle.borderRadius(5)
                 }}/>
            <Typography variant={'h6'} pr={5} display={'flex'} style={{flex: 1, flexDirection: 'column'}}>
                {!file.title ? 'درحال آپلود' : (file.title && file.title['fa']) ? file.title['fa'] : 'نامشخص'}
                <ThemeProvider
                    theme={{
                        ...theme,
                        direction: 'ltr'
                    }}>
                    <Box style={{direction: 'ltr'}} pt={1.5}>
                        <LinearProgress variant="determinate" value={uploadProgress}/>
                    </Box>
                </ThemeProvider>
            </Typography>
        </Box>
    )
}

function getImageSize(file, callBack) {
    if (!callBack)
        return
    try {
        let img = new Image();
        img.src = window.URL.createObjectURL(file);
        img.onload = () => callBack({width: img.width, height: img.height});
    } catch (e) {

    }
}
