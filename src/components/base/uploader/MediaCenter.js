import React, {Fragment, useEffect, useRef, useState} from "react";
import Box from "@material-ui/core/Box";
import Img from "../img/Img";
import ControllerSite from "../../../controller/ControllerSite";
import _ from 'lodash'
import {cyan, green, red} from "@material-ui/core/colors";
import {UtilsStyle} from "../../../utils/Utils";
import ButtonBase from "@material-ui/core/ButtonBase";
import BaseButton from "../button/BaseButton";
import StickyBox from "react-sticky-box";
import {Card, useTheme} from "@material-ui/core";
import {lang, siteLang, theme} from "../../../repository";
import Button from "@material-ui/core/Button";
import {useSnackbar} from "notistack";
import {useDispatch} from "react-redux";
import Typography from "../Typography";
import {makeStyles} from "@material-ui/styles";
import PleaseWait from "../loading/PleaseWait";
import useSWR, {mutate, useSWRPages} from "swr";
import ComponentError from "../../base/ComponentError";
import TextFieldMultiLanguageContainer, {createName} from "../textField/TextFieldMultiLanguageContainer";
import {errorList, notValidErrorTextField} from "../textField/TextFieldContainer";
import FormControl from "../formController/FormController";
import TextField from "../textField/TextField";
import Checkbox from "@material-ui/core/Checkbox";
import SuccessButton from "../button/buttonVariant/SuccessButton";

export default function ({boxId = null, type, selectable = true, multiSelect = false, onSelected, ...props}) {
    const [state, setState] = useState({
        uniqId: Math.round(Math.random() * 1000),
        data: [],
        pagination: {
            hasMoreItems: true,
            lastPage: 0,
            items: 0,
            page: 0,
        },
        itemSelected: false
    });
    const ref = useRef();
    const [activeItem, setActiveItem] = useState({
        item: null
    });


    const d = ControllerSite.Media.media();
    const {
        pages,
        isLoadingMore,
        isReachingEnd,
        loadMore,
        pageSWRs,
        ...f
    } = useSWRPages(
        `media-center-${boxId}-${type}`,
        ({offset, withSWR}) => {
            const {data, error} = withSWR(
                // eslint-disable-next-line react-hooks/rules-of-hooks
                useSWR(d[0] + `media-center-${boxId}-${type}` + (offset || 1), () => {
                    return d[1]({page: offset || 1, boxId: boxId, type: type})
                })
            );

            if (error) {
                return <ComponentError
                    statusCode={(_.isObject(error) && error.request) ? error.request.status : undefined}
                    tryAgainFun={() => mutate(d[0])}/>
            }

            if (!data) {
                return (
                    <PleaseWait fullPage={false}
                                style={{
                                    width: '100%',
                                    height: 200
                                }}/>)
            }

            if (_.isEmpty(data.data)) {
                return (
                    <Box display={'flex'} justifyContent={'center'} height={400} width={1}>
                        <Typography variant={'h3'} alignItems={'center'}>
                            {lang.get("list_is_empty")}
                        </Typography>
                    </Box>
                )
            }

            return data.data.map((item, index) =>
                <Item key={item.id} item={item}
                      isMulti={multiSelect}
                      width={activeItem.item ? 0.25 : 0.2}
                      onClick={(ref) => onActiveItemChange(item, ref)}/>
            )
        },
        // get next page's offset from the index of current page
        (SWR, index) => {
            if (SWR.data && SWR.data.pagination.lastPage <= index + 1) return null;
            return (index + 2)
        },
        [
            onActiveItemChange,
            boxId,
            type
        ]
    );

    //DEPS deps useSWR
    function onActiveItemChange(item, ref) {
        if (state.itemSelected) {
            ref.current.click();
            return
        }


        setActiveItem({
            ...activeItem,
            item: item
        })
    }

    useEffect(() => {
        // if (_.isEmpty(state.data[state.pagination.page]))
        //     requestData(state.pagination.page)
    }, []);

    function requestData(page) {
        // ControllerSite.Media.media({
        //     boxId: boxId,
        //     type: type,
        //     page: page + 1
        // }).then((res) => {
        //     const newData = state.data;
        //     newData[page] = res.data.data;
        //     setState({
        //         data: [...newData],
        //         pagination: {
        //             ...res.data.pagination,
        //             page: page + 1
        //         }
        //     })
        // })
    }

    function handleClosePanel() {
        setActiveItem({
            ...activeItem,
            item: null
        })
    }

    function handleSelected(item) {
        if (item) {
            return onSelected(multiSelect ? [item] : item)
        }
        const res = [];
        try {
            _.forEach(pageSWRs, (({data}) => {
                _.forEach(data.data, (d) => {
                    _.forEach(ref.current.serialize().selected, (val) => {
                        if (d.id === _.toInteger(Object.keys(val)[0])) {
                            res.push(d);
                            return false;
                        }
                    })
                });
            }));
        } catch (e) {
        }
        onSelected(res)
    }

    return (
        <Box display={'flex'} flexDirection={'column'}>
            {state.itemSelected &&
            <Box my={2} px={2} display={'flex'}>
                <Typography variant={'body2'} alignItems={'center'}>
                    {state.itemSelected} عکس انتخاب شده است.
                </Typography>
                <SuccessButton
                    onClick={() => handleSelected()}
                    style={{
                        marginRight: theme.spacing(1),
                        marginLeft: theme.spacing(1)
                    }}>
                    انتخاب گزینه‌های فعال
                </SuccessButton>
                <BaseButton
                    variant={"text"}
                    onClick={() => {
                        const form = ref.current;
                        for (let i = 0; i < form.elements.length; i++) {
                            const element = form.elements[i];
                            const type = element.type;
                            const nodeName = element.nodeName.toLowerCase();
                            if (nodeName === "input" && type === "checkbox" && element.checked) {
                                element.click()
                            }
                        }
                    }}
                    style={{
                        marginRight: theme.spacing(1),
                        marginLeft: theme.spacing(1)
                    }}>
                    لغو انتخاب‌ها
                </BaseButton>
            </Box>
            }
            <Box display={'flex'} {...props}>
                <FormControl innerref={ref} name={"mediaMultiSelect" + state.uniqId} display={'flex'} flexWrap={'wrap'}
                             flex={1}
                             onChangeInterval={0}
                             onChange={() => {
                                 try {
                                     if (!multiSelect)
                                         return;
                                     const selected = ref.current.serialize().selected;
                                     if (selected) {
                                         setActiveItem({
                                             item:null
                                         });
                                         setState({
                                             ...state,
                                             itemSelected: selected.length
                                         });
                                         return
                                     }
                                     setState({
                                         ...state,
                                         itemSelected: false
                                     })
                                 } catch (e) {

                                 }
                             }}
                             style={{
                                 ...UtilsStyle.transition(300),
                             }}>
                    {pages}
                    {
                        !isReachingEnd &&
                        <Box width={1} mt={2} mb={1} display={'flex'} justifyContent={'center'}>
                            <ButtonBase onClick={() => loadMore()}>
                                <Typography px={2} py={1} variant={'h6'}>
                                    {lang.get("show_more")}
                                </Typography>
                            </ButtonBase>
                        </Box>
                    }
                </FormControl>
                {(!state.itemSelected && activeItem.item) &&
                <Box>
                    <StickyBox offsetTop={80} offsetBottom={20}>
                        <Box minWidth={350}
                             px={1}
                             pt={2}
                             size={{
                                 height: 'fit-content'
                             }}>
                            <SidePanel
                                item={activeItem.item}
                                selectable={selectable}
                                onSelected={handleSelected}
                                onChange={(item) => {
                                    let index = null;
                                    _.forEach(state.data, (d, i) => {
                                        if (index)
                                            return false;
                                        _.forEach(d, (m, mI) => {
                                            if (m.id === item.id) {
                                                index = {
                                                    dataIndex: i,
                                                    mediaIndex: mI
                                                };
                                                return false
                                            }
                                        })
                                    });
                                    if (!index)
                                        return;
                                    const newData = state.data;
                                    newData[index.dataIndex][index.mediaIndex] = item;
                                    setState({
                                        ...state,
                                        data: [...newData]
                                    })
                                }}
                                onClose={handleClosePanel}/>
                        </Box>
                    </StickyBox>
                </Box>
                }
            </Box>
        </Box>

    )
}

function SidePanel({item, selectable, onSelected, onChange, onClose,}) {
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();
    const dispatch = useDispatch();
    const ref = useRef();

    const [state, setState] = useState({
        item: item,
    });

    useEffect(() => {
        setState({
            ...state,
            item: item
        });


    }, [item]);

    function updateImage() {
        let errorText = "";
        if (ref.current.hasError()) {
            errorText = "فرم مشکل دارد";
            try {
                const el = ref.current.getErrorElement();
                const t = el.getAttribute(notValidErrorTextField);
                errorText = t ? t : errorText;
                el.focus();
            } catch (e) {
            }
            reqCancel(errorText);
            return;
        }
        const {name} = ref.current.serialize();

        ControllerSite.Media.update({imageId: state.item.id, title: name}).then((res) => {
            enqueueSnackbar(`با موفقیت بروزرسانی شد.`,
                {
                    variant: "success",
                    action: (key) => (
                        <Fragment>
                            <Button onClick={() => {
                                closeSnackbar(key)
                            }}>
                                {lang.get('close')}
                            </Button>
                        </Fragment>
                    )
                });
            onChange(res.data.media);
            // dispatch(stopLoading({uniqueIds: uId}));
        }).catch(() => {
            // dispatch(stopLoading({uniqueIds: uId}));
        })
    }

    function reqCancel(text = null) {
        if (text)
            enqueueSnackbar(text,
                {
                    variant: "error",
                    action: (key) => (
                        <Fragment>
                            <Button onClick={() => {
                                closeSnackbar(key)
                            }}>
                                {lang.get('close')}
                            </Button>
                        </Fragment>
                    )
                });

        setState({
            loading: false
        })
    }

    const i = state.item;
    return (
        <Box
            width={1}
            component={Card}
            px={1.5}
            py={1.5}
            display={'flex'}
            flexDirection={'column'}
            alignItems={'center'}
            justifyContent={'center'}
            style={{
                ...UtilsStyle.transition(300),
            }}>
            <Img
                mb={1}
                src={item.image}
                imgStyle={{
                    width: '100%',
                    height: 'auto',
                    ...UtilsStyle.borderRadius(5)
                }}
                style={{
                    width: 250,
                    height: 'auto'
                }}/>
            <FormControl minWidth={200} width={1} pt={2} name={"editUploadCenter"} innerref={ref}>
                <TextFieldMultiLanguageContainer
                    name={"name"}
                    render={(ref, {lang: inputLang, name: inputName, initialize, valid, errorIndex, setValue, inputProps, style, props}) => {
                        return (
                            <TextField
                                {...props}
                                error={!valid}
                                variant="outlined"
                                name={inputName}
                                defaultValue={i.title[inputLang.key]}
                                fullWidth
                                helperText={errorList[errorIndex]}
                                inputRef={ref}
                                required={inputLang.key === siteLang}
                                label={'نام'}
                                style={style}
                                inputProps={{
                                    ...inputProps
                                }}/>
                        )
                    }}/>
            </FormControl>
            <Box display={'flex'} mt={2} width={1} flexWrap={'wrap'}>
                {selectable &&
                <BaseButton
                    onClick={() => {
                        if (onSelected)
                            onSelected(item)
                    }}
                    style={{
                        marginBottom: theme.spacing(1),
                        marginLeft: theme.spacing(0.75),
                        backgroundColor: green[300],
                        color: '#fff',
                    }}>
                    {lang.get("select")}
                </BaseButton>
                }
                <BaseButton
                    onClick={() => {
                        setTimeout(() => {
                            updateImage()
                        }, 1000)
                    }}
                    style={{
                        marginBottom: theme.spacing(1),
                        marginLeft: theme.spacing(0.75),
                        backgroundColor: cyan[300],
                        color: '#fff',
                    }}>
                    {lang.get("save")}
                </BaseButton>
                <BaseButton
                    onClick={() => {
                        onClose()
                    }}
                    style={{
                        marginBottom: theme.spacing(1),
                        marginLeft: theme.spacing(0.75),
                        backgroundColor: red[200],
                        color: '#fff',
                    }}>
                    {lang.get("delete")}
                </BaseButton>
                <BaseButton
                    variant={"text"}
                    size={"small"}
                    onClick={() => {
                        onClose()
                    }}
                    style={{
                        marginBottom: theme.spacing(1),
                        marginLeft: theme.spacing(0.75),
                    }}>
                    {lang.get("close")}
                </BaseButton>
            </Box>
        </Box>
    )
}

const useStyles = makeStyles({
    mediaCenterItemRoot: {
        position: 'relative',
        '& .mediaCenterItemTitle': {
            opacity: 0,
            position: 'absolute',
            bottom: 5,
            backgroundColor: cyan[300],
            ...UtilsStyle.borderRadius(5),
            ...UtilsStyle.transition(),
        },
        '&:hover .mediaCenterItemTitle': {
            opacity: 1,
        }
    }
});

function Item({item, isMulti = false, onClick, ...props}) {
    const theme = useTheme();
    const ref = useRef();
    const classes = useStyles(props);
    return (
        <Box width={0.25} p={1}
             display={'flex'}
             className={classes.mediaCenterItemRoot}
             alignItems={'center'} justifyContent={'center'}
             {...props}
             style={{
                 ...UtilsStyle.transition(300),
                 ...props.style,
             }}>
            <Box component={Card}
                 width={1} boxShadow={3}
                 display={'flex'}
                 alignItems={'center'} justifyContent={'center'}
                 style={{
                     position: 'relative',
                 }}>
                <ButtonBase
                    onClick={() => {
                        onClick(ref)
                    }}
                    style={{
                        width: '100%',
                    }}>
                    <Img
                        src={item ? item.image : ''}
                        imgStyle={{
                            width: '100%',
                            height: 'auto',
                            overflow: 'hidden',
                        }}
                        style={{
                            width: '100%',
                            height: '100%',
                        }}/>
                </ButtonBase>
                {(item && item.title && item.title.fa) &&
                <Typography className={'mediaCenterItemTitle'}
                            component={'span'} variant={"body1"}
                            color={'#fff'}
                            px={1.5}
                            py={0.7}>
                    {item.title.fa}
                </Typography>
                }

                {isMulti &&
                <Box
                    style={{
                        position: 'absolute',
                        top: 5,
                        left: 4,
                        background: 'rgba(171, 171, 171, 0.5)',
                        ...UtilsStyle.borderRadius('50%')
                    }}>
                    <Checkbox
                        inputRef={ref}
                        name={createName({array: "selected", name: item.id})}
                        color="primary"
                        inputProps={{'aria-label': 'secondary checkbox'}}
                    />
                </Box>}
            </Box>
        </Box>
    )
}
