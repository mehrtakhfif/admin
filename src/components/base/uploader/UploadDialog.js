import React, {useEffect, useState} from "react";
import Slide from "@material-ui/core/Slide";
import Dialog from "@material-ui/core/Dialog";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import {Close} from "@material-ui/icons";
import Box from "@material-ui/core/Box";
import Typography from "../Typography";
import {makeStyles} from "@material-ui/core";
import ButtonBase from "@material-ui/core/ButtonBase";
import {UtilsStyle} from "../../../utils/Utils";
import {cyan, grey} from "@material-ui/core/colors";
import PropTypes from "prop-types";
import {useSnackbar} from "notistack";
import MediaCenter from "./MediaCenter";
import UploadCenter from "./UploadCenter";

const useStyles = makeStyles(theme => ({
    appBar: {
        position: 'relative',
    },
    title: {
        marginLeft: theme.spacing(2),
        flex: 1,
    },
}));
const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

export default function UploadDialog({
                                         boxId,
                                         open,
                                         uploadFileTypes = ['image.*'],
                                         imageSize,
                                         multiSelect=false,
                                         maxUploadCount,
                                         onSelected,
                                         fileType = 'image',
                                         type = 'image',
                                         onClose,
                                         ...props
                                     }) {
    const classes = useStyles();
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();

    const [setting, setSetting] = useState({
        activeTab: 0
    });


    useEffect(() => {
    }, [open]);


    function closeHandler(e) {
        onClose(e)
    }

    return (
        <Dialog fullScreen open={open} onClose={closeHandler} TransitionComponent={Transition}>
            <AppBar className={classes.appBar}>
                <Toolbar>
                    <Box edge="start" display='flex' alignItems={'center'} onClick={closeHandler}>
                        <IconButton color="inherit" aria-label="close">
                            <Close/>
                        </IconButton>
                        <Typography variant="h6" color={'#fff'} pr={1} style={{cursor: 'pointer'}}>
                            انصراف
                        </Typography>
                    </Box>
                </Toolbar>
                <Box display={'flex'}>
                    <Box mr={1}>
                        <ButtonBase onClick={() =>
                            setSetting({
                                ...setting,
                                activeTab: 0
                            })}>
                            <Typography
                                variant={'body1'}
                                py={1} px={1}
                                color={setting.activeTab === 0 ? grey[100] : grey[800]}
                                style={{
                                    backgroundColor: setting.activeTab === 0 ? cyan[400] : grey[100],
                                    ...UtilsStyle.borderRadius("5px 5px 0 0")
                                }}>
                                آپلود
                            </Typography>
                        </ButtonBase>
                    </Box>
                    <Box mr={1.5}>
                        <ButtonBase onClick={() => setSetting({
                            ...setting,
                            activeTab: 1
                        })}>
                            <Typography variant={'body1'} py={1} px={1.5}
                                        color={setting.activeTab === 1 ? grey[100] : grey[800]}
                                        style={{
                                            backgroundColor: setting.activeTab === 1 ? cyan[400] : grey[100],
                                            ...UtilsStyle.borderRadius("5px 5px 0 0")
                                        }}>
                                مدیا سنتر
                            </Typography>
                        </ButtonBase>
                    </Box>
                </Box>
            </AppBar>
            {setting.activeTab === 0 ?
                <UploadCenter
                    boxId={boxId}
                    uploadFileTypes={uploadFileTypes}
                    maxUploadCount={maxUploadCount}
                    type={type}
                    imageSize={imageSize}
                    onSelected={(file)=>{
                        if (multiSelect){
                            onSelected([file]);
                            return
                        }
                        onSelected(file);
                    }}/> :
                setting.activeTab === 1 ?
                    <Box>
                        <MediaCenter boxId={boxId} type={type}
                                     multiSelect={multiSelect}
                                     onSelected={onSelected}/>
                    </Box> : null}
        </Dialog>
    );
}

UploadDialog.propTypes = {
    uploadFileTypes: PropTypes.arrayOf(PropTypes.string),
    fileType: PropTypes.oneOf(['image', 'audio']),
};



