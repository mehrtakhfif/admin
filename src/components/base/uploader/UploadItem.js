import React, {useState} from 'react';
import {Box, IconButton} from '@material-ui/core';
import Img from '../img/Img';
import UploadImagePlaceholder
  from '../../../drawable/image/UploadImagePlaceholder.jpg';
import {grey} from '@material-ui/core/colors';
import {UtilsStyle} from '../../../utils/Utils';
import ButtonBase from '@material-ui/core/ButtonBase';
import FullScreenDialog from './UploadDialog';
import PropTypes from 'prop-types';
import Icon from '../../icon/Icon';
import Tooltip from '../Tooltip';

export default function UploadItem({
                                     src,
                                     boxId,
                                     maxUploadCount,
                                     uploadFileTypes = ['image.*'],
                                     onClose,
                                     width ,
                                     height ,
                                     boxWidth = 1,
                                     multiSelect = false,
                                     holderWidth,
                                     holderHeight,
                                     fileType = 'image',
                                     type = 'image',
                                     buttonIcon,
                                     tooltipTitle=undefined,
                                     onSelected,
                                     ...props
                                   }) {

  const [state, setState] = useState({
    open: false,
  });

  function openDialog() {
    setState({
      ...state,
      open: true,
    });
  }

  return (
      <Box display={'flex'}
           width={boxWidth}
           justifyContent={'center'}
           alignItems={'center'}
           flexDirection={'column'}>
        {buttonIcon === undefined ?
            <ButtonBase
                onClick={openDialog}
                style={{width: '100%'}}>
              <Img src={src ? src : UploadImagePlaceholder}
                   minWidth={40}
                   minHeight={40}
                   maxWidth={holderWidth ? holderWidth : width}
                   maxHeight={holderHeight ? holderHeight : height}
                   imgStyle={{
                     width: '100%',
                     maxWidth: width,
                     height: 'auto',
                     border: `1px solid ${grey[400]}`,
                     ...UtilsStyle.borderRadius(5),
                   }}
                   style={{
                     justifyContent: 'center',
                   }}/> </ButtonBase>
            : <Tooltip title={tooltipTitle}><IconButton onClick={openDialog}>{buttonIcon}</IconButton></Tooltip>}

        <FullScreenDialog
            open={state.open}
            boxId={boxId}
            imageSize={{
              width: width,
              height: height,
            }}
            maxUploadCount={maxUploadCount}
            uploadFileTypes={uploadFileTypes}
            fileType={fileType}
            type={type}
            multiSelect={multiSelect}
            onSelected={(file) => {
              onSelected(file);
              setState({
                ...state,
                open: false,
              });
            }}
            onClose={(e) => {
              setState({
                ...state,
                open: false,
              });
              if (onClose)
                onClose(e);
            }}/>
      </Box>
  );
}

UploadItem.propTypes = {
  uploadFileTypes: PropTypes.arrayOf(PropTypes.string),
  fileType: PropTypes.oneOf(['image', 'audio']),
};
