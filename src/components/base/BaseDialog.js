import React from "react";
import Dialog from "@material-ui/core/Dialog";
import Slide from "@material-ui/core/Slide";
import Box from "@material-ui/core/Box";
import IconButton from "@material-ui/core/IconButton";
import {Close} from "@material-ui/icons";
import {tryIt,isElement} from "../../utils/ObjectUtils";
import _ from "lodash";
import Typography from "./Typography";

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

export default function BaseDialog({
                             open,
                             header = false,
                             fullScreen=false,
                             onClose,
                             ...props
                         }) {

    return (
        <Dialog open={open}
                fullWidth={true}
                fullScreen={fullScreen}
                TransitionComponent={Transition}
                keepMounted
                onClose={() => {
                    tryIt(() => {
                        onClose()
                    })
                }}
                {...props}>
            <Box display={'flex'} flexDirection={'column'} p={2}>
                {header &&
                <Box display={'flex'} alignItems={'center'} p={2}>
                    <IconButton
                        onClick={() => {
                            tryIt(() => {
                                onClose()
                            })
                        }}>
                        <Close/>
                    </IconButton>
                    {
                        isElement(header) ?
                            header :
                            (_.isString(header)) &&
                            <Typography variant={"h6"} px={2}>
                                {header}
                            </Typography>

                    }
                </Box>
                }
                {props.children}
            </Box>
        </Dialog>
    )
}
