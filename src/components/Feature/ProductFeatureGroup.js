import React, {useEffect, useState, Fragment} from "react";
import Box from "@material-ui/core/Box";
import {UtilsStyle} from "../../utils/Utils";
import Typography from "../base/Typography";
import PleaseWait from "../base/loading/PleaseWait";
import BaseDialog from "../base/BaseDialog";
import {Close} from "@material-ui/icons"
import {lang, siteLang} from "../../repository";
import useSWR, {mutate} from 'swr'
import IconButton from "@material-ui/core/IconButton";
import {
    Checkbox,
    Button,
    FormGroup,
    FormControlLabel,
    useTheme,
    Collapse,
    RadioGroup,
    FormLabel,
    Radio
} from "@material-ui/core"
import {useSnackbar} from "notistack";
import BaseButton from "../base/button/BaseButton";
import {cyan} from "@material-ui/core/colors"
import _ from "lodash"
import {gcLog} from "../../utils/ObjectUtils";
import DefaultTextField from "../base/textField/DefaultTextField";
import {makeStyles} from '@material-ui/core/styles';
import Switch from '@material-ui/core/Switch';
import Paper from '@material-ui/core/Paper';
import Grow from '@material-ui/core/Grow';
import {convertFeature, convertFeatureGroup, convertFeaturesGroup} from "../../controller/converter";
import FormControl from "../base/formController/FormController";
import MaterialFormController from "@material-ui/core/FormControl"
import ControllerProduct from "../../controller/ControllerProduct";


export default function ProductFeatureGroup({
                                                open = true,
                                                onSelect,
                                                onClose,
                                                onDone,
                                                productFeatures,
                                                productId,
                                                ...props
                                            }) {
    const theme = useTheme()
    const [checkedValues, setCheckedValues] = useState([])
    const [featureTextValue, setFeatureTextValue] = useState("")
    // const [featureGroups, setFeatureGroups] = useState([])
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();
    const d = ControllerProduct.Features.getFeatureGroup(productId);
    const apiKey = d[0] + productId;
    const {data: featureGroups, error} = useSWR(apiKey, () => {
        return d[1]().then(res => {
            const featureGroupTemp = convertFeaturesGroup(res.data.feature_groups)
            setCheckedValues(getSelectedFromFeatureGroups(featureGroupTemp))
            return featureGroupTemp
        })
    });
    gcLog("ProductFeatureGroup data error", {featureGroups, error})

    function getSelectedFromFeatureGroups(featureGroups) {
        let o = []
        featureGroups.map(group => (group.features.map(feature => {
            const selected = feature.values.filter(value => !_.isEmpty(value.storage_id))
            if (selected.length !== 0) {
                o = o.concat([{feature: feature, values: selected}])
            }
        })))
        return o
    }


    function handleCheck(selectedValue, selectedFeature) {


        const fIndex = _.findIndex(checkedValues, f => f.feature.id === selectedFeature.id)

        if (fIndex === -1) {
            setCheckedValues(d => {
                    return d.concat({feature: selectedFeature, values: [selectedValue]})
                }
            )
            return
        }
        const f = checkedValues[fIndex]
        f.values = (_.findIndex(f.values, value => value.id === selectedValue.id) !== -1)
            ? f.values.filter(d => (d.id !== selectedValue.id))
            : f.feature.type.key === "bool" ? f.values = [selectedValue] : f.values.concat([selectedValue])

        const dd = _.cloneDeep(checkedValues)
        dd[fIndex] = f
        setCheckedValues(dd)

    }

    return (
        <Box display={"flex"} alignItems="center" justifyContent="center">
            <BaseDialog open={open}
                        maxWidth={"md"}>
                {
                    <Box display={'block'} flexDirection={'column'}>
                        <Box display={'flex'} py={2}>
                            <Box display={'flex'} flex={1}>
                                <BaseButton style={{margin: "8px"}} variant={'outlined'} onClick={() => {
                                    let o = []
                                    featureGroups.map(group => {
                                        group.features.map(feature => (
                                            o = o.concat([{
                                                feature: feature,
                                                values: feature.type.key === "bool" ? [feature.values[0]] : feature.values
                                            }])
                                        ))
                                    })
                                    setCheckedValues(o)
                                }}>انتخاب همه</BaseButton>
                                <BaseButton style={{margin: "8px", color: theme.palette.text.secondary}}
                                            variant={'outlined'} onClick={() => {

                                    setCheckedValues(s => (getSelectedFromFeatureGroups(featureGroups)))
                                }}>پاک کردن انتخاب ها</BaseButton>
                            </Box>
                            <IconButton variant={"text"} onClick={() => {
                                // setCheckedValues(getSelectedFromFeatureGroups(productFeatures))
                                onClose()
                            }}>
                                <Close/>
                            </IconButton>
                        </Box>
                        <Box style={{maxHeight: "400px", overflow: "scroll", overflowX: "hidden"}}>
                            {
                                !error ?
                                    _.isArray(featureGroups) ?
                                        !_.isEmpty(featureGroups) ?
                                            featureGroups.map(it => {
                                                return (
                                                    <FeatureGroupItem key={it.id} data={it} handleCheck={handleCheck}
                                                                      checkedValues={checkedValues}/>
                                                )
                                            }) :
                                            <p style={{textAlign: "center", fontSize: "24px"}}> هیچ گروه فیچری برای دسته
                                                بندی محصول انتخاب نشده است </p> :
                                        <PleaseWait/> :
                                    <h1>`${error.toString()}`</h1>
                            }
                        </Box>

                    </Box>
                }
                <Typography style={{textAlign: "right"}}
                            p={1}
                            color={theme.palette.error.main}
                            variant={"caption"}> فیچر های استفاده شده در انبار قابل حذف نمیباشند </Typography>
                <BaseButton
                    style={{margin: "16px", backgroundColor: cyan[300], color: "#fff"}}
                    onClick={() => {
                        onDone(checkedValues)
                    }}
                >تمام</BaseButton>
            </BaseDialog>
        </Box>
    )

}

function FeatureGroupItem({data, handleCheck, checkedValues, ...props}) {

    return (<Box>
        <Box display={'flex'} flexDirection={'row'} alignItems={'center'} justifyContent={'center'} position={'relative'}>
            <Typography justifyContent={"center"} variant={'h5'} p={"10px"} m={"8px"}>
                {data.name[siteLang]}
            </Typography>
           <Box position={'absolute'} style={{right:'8px'}}>
               <Typography justifyContent={"center"} variant={'h6'} p={"10px"} m={"8px"}>
                   شناسه : {data.id}
               </Typography>
           </Box>
        </Box>
        <Box>
            {
                data && data.features ?
                    <Box display={'flex'} flexDirection={'column'} width={"100%"} height={"100%"} p={"20px"}
                         style={{
                             padding: "8px",
                         }}>
                        {data.features.map(feature => (
                                <Box key={feature.id} display={"flex"} alignItems={"center"}>
                                    <Typography style={{textAlign: "center"}}
                                                p={"2px"}
                                                variant={"subtitle2"}>{feature.name[siteLang]} </Typography>
                                    <Typography style={{textAlign: "center"}}
                                                p={"2px"}
                                                variant={"caption"}> ({feature.type.typeLabel}) : </Typography>
                                    <OutputFeature feature={feature} handleCheck={handleCheck}
                                                   checkedValues={checkedValues}/>
                                </Box>

                            )
                        )}

                    </Box>
                    : <React.Fragment/>

            }


        </Box>


    </Box>)
}

function OutputFeature({feature, handleCheck, checkedValues}) {

    return feature.type.key === "selectable" ?
        <FormGroup row style={{padding: "8px"}}>
            {
                feature.values.map(it => (
                        <FormControlLabel
                            key={it.id}
                            style={{
                                border: "1px solid #ededed",
                                padding: "0px 15px 0px 15px",
                                margin: "5px",
                                ...UtilsStyle.borderRadius(5)
                            }}

                            control={<Checkbox disabled={!_.isEmpty(it.storage_id)} checked={(() => {
                                const f = _.find(checkedValues, f => f.feature.id === feature.id)
                                if (!f)
                                    return false
                                return (_.findIndex(f.values, i => (i.id === it.id)) !== -1)
                            })()}
                                               onChange={(e) => {
                                                   handleCheck(it, feature)
                                               }}
                                               name={"selectValue"}/>}
                            label={it.name[siteLang]}
                            labelPlacement="start"
                        />
                    )
                )
            }
        </FormGroup> :
        feature.type.key === "text" ?
            <TextFeatureSelection feature={feature} handleCheck={handleCheck} checkedValues={checkedValues}/> :
            <FormGroup row style={{
                border: "1px solid #ededed",
                margin: "5px",
                ...UtilsStyle.borderRadius(5)
            }}>
                {
                    feature.values.map(it => (
                        <FormControlLabel
                            key={it.id}
                            style={{
                                padding: "0px 15px 0px 15px",
                                margin: "1px"
                            }}
                            control={<Checkbox checked={(() => {
                                const f = _.find(checkedValues, f => f.feature.id === feature.id)
                                if (!f)
                                    return false
                                return (_.findIndex(f.values, i => (i.id === it.id)) !== -1)
                            })()}
                                               onChange={(e) => {
                                                   handleCheck(it, feature)
                                               }}
                                               name={"selectValue"}/>}
                            label={it.name[siteLang]}
                            labelPlacement="start"
                        />
                    ))
                }
            </FormGroup>

}

function TextFeatureSelection({feature, handleCheck, checkedValues}) {


    return (
        <Box display={"flex"} flex={1} alignItems={"center"}>
            <FormControlLabel
                control={<Checkbox checked={(() => {
                    const f = _.find(checkedValues, f => f.feature.id === feature.id)
                    if (!f)
                        return false
                    return (_.findIndex(f.values, i => (i.id === feature.values[0].id)) !== -1)
                })()}
                                   onChange={(e) => {
                                       handleCheck(feature.values[0], feature)
                                   }}
                                   name={"selectValue"}/>}
                label={feature?.values[0]?.name?.[siteLang]}
                labelPlacement={"start"}
            />
        </Box>
    )
}
