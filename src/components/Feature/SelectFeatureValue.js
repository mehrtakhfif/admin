import React, {useEffect, useState, Fragment} from "react";
import Box from "@material-ui/core/Box";
import ControllerProduct from "../../controller/ControllerProduct";
import DefaultTextField from "../base/textField/DefaultTextField";
import {UtilsStyle} from "../../utils/Utils";
import ButtonBase from "@material-ui/core/ButtonBase";
import Typography from "../base/Typography";
import PleaseWait from "../base/loading/PleaseWait";
import ComponentError from "../base/ComponentError";
import BaseDialog from "../base/BaseDialog";
import {convertFeatures} from "../../controller/converter";
import useSWR, {mutate} from "swr";
import {Close} from "@material-ui/icons"
import {lang, siteLang} from "../../repository";
import {gcError, gcLog} from "../../utils/ObjectUtils";
import IconButton from "@material-ui/core/IconButton";
import Dialog from "@material-ui/core/Dialog";
import SelectFeature from "../../pages/features/SelectFeature";
import {Checkbox, Button, FormGroup, FormControlLabel, useTheme} from "@material-ui/core"
import {SuccessSnackWhitClose} from "../../utils/Snack";
import {useSnackbar} from "notistack";
import BaseButton from "../base/button/BaseButton";
import {cyan} from "@material-ui/core/colors"
import _ from "lodash"


export default function SelectFeatureValue({open = false, onClose, onDone, ...props}) {
    const theme = useTheme()
    const [search, setSearch] = useState("");
    const [featureTextValue,setFeatureTextValue] = useState("")
    const [featureValue, setFeatureValue] = useState(null)
    const [finalFeature, SetFinalFeature] = useState([])
    const [openSelect, setOpenSelect] = useState(false)
    const [loading, setLoading] = useState(false)

    const d = open? ControllerProduct.Features.search({search: search}) : [null,null]
    const {data, error} = useSWR(...d)
    const [checkedValues, setCheckedValues] = useState([]);
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();

    useEffect(() => {
        setSearch("")
        setFeatureTextValue("")
        setCheckedValues([])
        setFeatureValue([])
        SetFinalFeature([])
        setOpenSelect(false)
    }, [open])

    // gcLog("SelectFeatureValue",{featureTextValue})

    // useEffect(()=>{
    //     return ()=> setOpenSelect(false)
    // },[])

    function handleCheck(e, it,type = null) {
        setCheckedValues(type==="bool"?[it]:checkedValues.includes(it)
            ? checkedValues.filter(c => c !== it)
            : checkedValues.concat([it])
        );
    }


    function handleSelect(e, item) {

        if (item.values.length === 0) {
            enqueueSnackbar('هیج مقداری برای این ویژگی ساخته نشده است',
                {
                    variant: "info",
                    action: (key) => (
                        <Fragment>
                            <Button onClick={() => {
                                closeSnackbar(key)
                            }}>
                                {lang.get('close')}
                            </Button>
                        </Fragment>

                    )
                });
            return
        }
        setFeatureValue(item)
        setOpenSelect(true)

    }

    function handleDone({type, data,...props}) {

        switch (type) {
            case "selectable" :
                onDone(data, checkedValues,type)
                setOpenSelect(false)
                break
            case "text" :
                setLoading(true)
                gcLog("sadoiiafoOFJKDES",{fId:data.id,value:props.featureTextValue})
                ControllerProduct.Features.addFeatureValue({fId:data.id,value:props.featureTextValue}).then((response)=>{
                    onDone(data,[response.data.data],type)

                }).finally(()=>{
                    setLoading(false)
                    setOpenSelect(false)
                })
                break
            case "bool" :
                onDone(data,checkedValues,type)
                setOpenSelect(false)
                break

        }

    }

    function outputFeature(featureData) {
        let output = undefined
        switch (!_.isEmpty(featureData)&&featureData.type.key) {
            case "selectable" :
                output = (
                    featureData && featureData.values ?
                        <Box display={'flex'} flexDirection={'column'} alignItems={"center"} justifyContent={"center"}  width={"100%"} height={"100%"} p={"20px"}
                             style={{
                                 padding: "20px",
                             }}>
                            <Typography style={{marginBottom: "20px"}} variant={"h5"}>مقدار های ویژگی را انتخاب کنید
                                ‌:</Typography>
                            <FormGroup row style={{marginTop: "20px"}}>
                                {featureData.values.map(it => (
                                    <FormControlLabel
                                        key={it.id}
                                        style={{
                                            border: "1px solid #ededed",
                                            padding: "0px 15px 0px 15px",
                                            margin: "5px",
                                            ...UtilsStyle.borderRadius(5)
                                        }}
                                        control={<Checkbox checked={checkedValues.includes(it)}
                                                           onChange={(e) => handleCheck(e, it,"")}
                                                           name={"selectValue"}/>}
                                        label={it.name[siteLang]}
                                        labelPlacement="start"
                                    />
                                ))}
                            </FormGroup>
                            <BaseButton
                                variant={'outlined'}
                                style={{
                                    borderColor: cyan[300],
                                    margin: "30px auto 5px auto",
                                    width: "40%"
                                }}
                                onClick={() => handleDone({type:"selectable",data: featureData})}>
                                تمام
                            </BaseButton>
                        </Box> :
                        <React.fragment/>
                )
                break
            case "text" :
                output = (
                    <Box display={'flex'} flexDirection={'column'} width={"100%"} height={"100%"} p={"20px"}
                         style={{
                             padding: "20px",
                         }}>
                        <DefaultTextField
                            name={"search"}
                            label={'متن ویژگی را وارد کنید'}
                            multiline={true}
                            rowsMax={4}
                            variant={'outlined'}
                            defaultValue={featureData.values[0].name[siteLang]}
                            style={{padding: "5px", margin: "10px"}}
                            onChange={(text) => {
                                setFeatureTextValue(text)
                            }}
                        />
                        <BaseButton
                            variant={'outlined'}
                            loading={loading}
                            style={{
                                borderColor: cyan[300],
                                margin: "30px auto 5px auto",
                                width: "40%"
                            }}
                            onClick={() => {
                                handleDone({type:"text",data:featureData,featureTextValue})
                            }}>
                            تمام
                        </BaseButton>
                    </Box>
                )
                break
            case "bool" :
                output = (
                    <Box display={'flex'} flexDirection={'column'} alignItems={"center"} justifyContent={"center"} height={"100%"} p={"20px"}
                         style={{
                             padding: "20px",
                         }}>
                        <Typography style={{marginBottom: "20px"}} variant={"h5"}>مقدار های ویژگی را انتخاب کنید
                            ‌:</Typography>
                    <FormGroup row style={{
                    border: "1px solid #ededed",
                    margin: "5px",
                    ...UtilsStyle.borderRadius(5)
                }}>
                    {
                        featureData.values.map(it => (
                            <FormControlLabel
                                key={it.id}
                                style={{
                                    padding: "0px 15px 0px 15px",
                                    margin: "1px"
                                }}
                                control={<Checkbox checked={checkedValues.includes(it)}
                                                   onChange={(e) => {
                                                       handleCheck(e,it,"bool")
                                                   }}
                                                   name={"selectValue"}/>}
                                label={it.name[siteLang]}
                                labelPlacement="start"
                            />
                        ))
                    }
                </FormGroup>
                <BaseButton
                    variant={'outlined'}
                    loading={loading}
                    style={{
                        borderColor: cyan[300],
                        margin: "30px auto 5px auto",
                        width: "40%"
                    }}
                    onClick={() => {
                        handleDone({type:"bool",data:featureData})
                    }}>
                    تمام
                </BaseButton>
            </Box>
                )
                break
        }
        return output
    }
    return (
        <Box display={"flex"} alignItems="center" justifyContent="center">
            {openSelect ?
                <Box display={"flex"} justifyContent="center" width={"60%"} height={"45%"} style={{
                    color: theme.palette.text.primary,
                    backgroundColor: theme.palette.background.primary,
                }}>
                    {
                        outputFeature(featureValue)
                    }
                </Box>
                : <BaseDialog open={open}>
                    {
                        <Box display={'block'} flexDirection={'column'}>
                            <Box display={'flex'} justifyContent={"flex-end"} py={2}>
                                <IconButton variant={"text"} onClick={() => onClose()}>
                                    <Close/>
                                </IconButton>
                            </Box>
                            <Box py={2} px={4}>
                                <DefaultTextField
                                    name={"search"}
                                    label={'جست و جو'}
                                    defaultValue={""}
                                    variant={'outlined'}
                                    onChange={(text) => {
                                        setSearch(text)
                                    }}
                                />
                            </Box>
                            <Box style={{maxHeight: "400px", overflow: "scroll", overflowX: "hidden"}}>
                                {

                                    !error ?
                                        data && data ?
                                            <Box display={'flex'} flexDirection={'column'}>
                                                {data.data.map(it => (
                                                    <Box key={it.id}
                                                         style={{
                                                             ...UtilsStyle.borderRadius(5),
                                                             paddingRight: "10px",
                                                             width: "100%"
                                                         }}>
                                                        <ButtonBase
                                                            onClick={() => {
                                                                handleSelect(null, it)
                                                            }}
                                                            style={{
                                                                width: "100%",
                                                                ...UtilsStyle.borderRadius(5)
                                                            }}>
                                                            <Box display={'flex'} flexDirection={'column'}
                                                                 style={{
                                                                     // border: `1px solid ${grey[300]}`,
                                                                     ...UtilsStyle.borderRadius(5)
                                                                 }}>
                                                                <Typography variant={'h6'}
                                                                            alignItems={"center"}
                                                                            style={{
                                                                                textAlign: "center",
                                                                                justifyContent: "center",
                                                                                paddingRight: "20px",
                                                                                paddingLeft: "20px",
                                                                                width: 250,
                                                                                height: 50,
                                                                                color: theme.palette.text.primary,
                                                                                backgroundColor: theme.palette.background.primary,
                                                                            }}>
                                                                    {it.name[siteLang]}
                                                                    <Typography pr={0.8} variant={"body2"}>
                                                                        {it.type.typeLabel}
                                                                    </Typography>
                                                                </Typography>
                                                            </Box>
                                                        </ButtonBase>
                                                    </Box>
                                                ))}
                                            </Box> :
                                            <PleaseWait/>
                                        : <ComponentError tryAgainFun={() => mutate(d[0])}/>
                                }
                            </Box>
                        </Box>
                    }
                </BaseDialog>}
        </Box>
    )
}
