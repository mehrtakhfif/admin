import React, {useEffect, useState} from "react";
import Box from "@material-ui/core/Box";
import ControllerProduct from "../../controller/ControllerProduct";
import {siteLang} from "../../repository";
import Typography from "../base/Typography";
import {UtilsStyle} from "../../utils/Utils";
import {Checkbox, FormControlLabel, FormGroup, useTheme} from "@material-ui/core";
import _ from 'lodash';
import {useSnackbar} from "notistack";
import useSWR from "swr";
import BaseButton from "../base/button/BaseButton";
import {Delete} from "@material-ui/icons";
import IconButton from "@material-ui/core/IconButton";
import FormContainer from "../base/FormContainer";
import SaveDialog from "../dialog/SaveDialog";
import Icon from "../icon/Icon";
import {convertFeaturesGroup} from "../../controller/converter";


export default function StorageFeatures({boxId, productId, storageId, item: it, onCheck, defaultFeatures, checkedValues, ...props}) {
    //TODO: Add FeaturesPercent and send to server
    //TODO: Select default value


    const theme = useTheme();
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();

    const data = storageId === undefined ? defaultFeatures : it

    const [state, setState] = useState({
        open: false,
        selectFeatures: false,
        removeDialog: false,
    });
    const [item, setItem] = useState([]);

    // const d = ControllerProduct.Features.productFeature({productId})
    //
    // const {data, error} = useSWR(...d);

    // const [productFeatures, setProductFeatures] = useState([
    //     {
    //         feature: "",
    //         values: [],
    //         id: 83,
    //         settings: {}
    //     }])


    useEffect(() => {
        setItem(it);
    }, [it]);

   /* useEffect(() => {
        if (storageId===undefined){
           setData( )
        }
    }, [data]);*/

    /*
        function handleCheck(e, it) {
            setCheckedValues(checkedValues.includes(it)
                ? checkedValues.filter(c => c !== it)
                : checkedValues.concat([it])
            );
        }*/

    function FeatureItem({it, ...props}) {

        return (
            <Box display={'flex'} flexDirection={'row'} width={"100%"}>
                <Typography justifyContent={"center"} variant={'h5'} p={"10px"} m={"auto"}
                            style={{minWidth: "fit-content", textAlign: "center"}}>
                    {it.feature.name[siteLang]} :
                </Typography>
                <FormGroup row style={{width: "100%", margin: "5px"}}>
                    {it.values.map(value => {
                        return (
                            <FormControlLabel
                                key={value.id}
                                style={{
                                    border: "1px solid #ededed",
                                    padding: "0px 15px 0px 15px",
                                    margin: "5px",
                                    ...UtilsStyle.borderRadius(5)
                                }}
                                control={<Checkbox checked={_.findIndex(checkedValues, (f) => {
                                    return f.id === value.id
                                }) !== -1}
                                                   onChange={(e) => onCheck({value})}
                                                   name={"selectValue"}/>}
                                label={value.name[siteLang]}
                                labelPlacement="start"
                            />
                        )
                    })}
                </FormGroup>
                <Box m={"auto"} display={"flex"} flexDirection={"flex-end"}>
                    <IconButton
                        //     onClick={() => ({
                        //     const elementsIndex = productFeatures.findIndex(element => element.feature === it.feature)
                        //
                        //     if (elementsIndex !== -1) {
                        //         setProductFeatures(productFeatures.filter(item => item.feature !== it.feature))
                        //         onChange({features: productFeatures})
                        //     })
                        //
                        // }}
                        color="primary" aria-label="upload picture"
                        component="span">
                        <Delete/>
                    </IconButton>
                </Box>
            </Box>)
    }

    return (
        <FormContainer title={"فیچر"} open={state.open} active={true}
                       mt={4}
                       justifyContent={'center'}
                       onOpenClick={(e, open) => {
                           setState({
                               ...state,
                               open: open
                           })
                       }}>
            <BaseButton
                variant={'outlined'}
                onClick={() => {
                    setState({
                        ...state,
                        selectFeatures: true
                    })
                }}
                fullWidth={true}
                style={{
                    color: theme.palette.text.primary.main,
                    margin: "10px",
                    display: "none",
                    border: `1px solid ${theme.palette.primary.main}`
                }}>
                افزودن فیچر جدید
            </BaseButton>
            {
                data && data.length !== 0 ?
                    <React.Fragment>
                        {
                            data.map(pf => (
                                <FeatureItem key={it.id} it={pf}/>
                            ))}
                        {
                            // data.data.map(pf => {
                            //     return (
                            //         <Box width={"100%"} display={"flex"} flexDirection={"row"}>
                            //                     <Typography variant={'h5'} p={1} alignItems={"center"}>
                            //                         {pf.feature.name[siteLang]}
                            //                     </Typography>
                            //                     <FormGroup row style={{marginTop: "20px"}}>
                            //                         {pf.values.map(it => (
                            //                             <FormControlLabel
                            //                                 key={it.id}
                            //                                 style={{
                            //                                     border: "1px solid #ededed",
                            //                                     padding: "0px 15px 0px 15px",
                            //                                     margin: "5px",
                            //                                     ...UtilsStyle.borderRadius(5)
                            //                                 }}
                            //                                 control={<Checkbox checked={checkedValues.includes(it)||it.selected}
                            //                                                    onChange={(e) => handleCheck(e, it)}
                            //                                                    name={"selectValue"}/>}
                            //                                 label={it.name[siteLang]}
                            //                                 labelPlacement="start"
                            //                             />
                            //                         ))}
                            //                     </FormGroup>
                            //             <Box mx={1} display={"flex"} flexDirection={"flex-end"}>
                            //                 <IconButton
                            //                     onClick={() => {
                            //                         setState({
                            //                             ...state,
                            //                             removeDialog: pf
                            //                         })
                            //                     }}>
                            //                     <DeleteForeverOutlined/>
                            //                 </IconButton>
                            //             </Box>
                            // </Box>
                            //     )
                            // })
                        }
                    </React.Fragment> :
                    <Typography justifyContent={"center"} variant={'h5'} p={"10px"} m={"auto"}
                                style={{minWidth: "fit-content", textAlign: "center"}}>
                        هیچ فیچری انتخاب نشده
                    </Typography>


            }
            {<SaveDialog open={Boolean(state.removeDialog)}
                         text={(
                             state.removeDialog &&
                             <Box display={'flex'} flexDirection={'column'} p={1}>
                                 <Box display={'flex'} pb={2}>
                                     <Icon width={45} showSkeleton={false} minHeight={45}
                                           alt={state.removeDialog.iconName}
                                           src={state.removeDialog.iconLink}/>
                                     <Typography variant={'body1'} fontWeight={500} p={1}>
                                         {state.removeDialog.name[siteLang]}
                                     </Typography>
                                 </Box>
                                 <Typography variant={'h6'}>
                                     آیا از حذف این فیچر اطمینان دارید؟
                                 </Typography>
                             </Box>
                         )}
                         submitText={"بله اطمینان دارم"}
                         cancelText={"خیر"}
                         onSubmit={() => {
                             const newItem = item;
                             _.remove(newItem, (i) => {
                                 return i.id === state.removeDialog.id;
                             });
                             setItem([...newItem]);
                             setState({
                                 ...state,
                                 removeDialog: false
                             })
                         }}
                         onCancel={() => {
                             setState({
                                 ...state,
                                 removeDialog: false
                             })
                         }}/>}
        </FormContainer>
    )
}
