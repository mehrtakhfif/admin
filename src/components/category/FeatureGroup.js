import React,{useState} from "react";
import _ from "lodash";
import ControllerProduct from "../../controller/ControllerProduct";
import BaseButton from "../base/button/BaseButton";
import SelectFeaturesGroup, {Item as FeatureGroupItem} from "../../pages/featuresGroup/SelectFeaturesGroup";
import {Box,Card} from "@material-ui/core"
import {green} from "@material-ui/core/colors"

export default function FeatureGroup({boxId, catId, featureGroups: fg, ...props}) {
    const [featureGroups, setFeatureGroups] = useState(fg || []);
    const [open, setOpen] = useState(false)
    const [state, setState] = useState({
        loading: false
    })

    function handleOnSelected(items) {
        setOpen(false)
        if (!items)
            return;
        setState({
            ...state,
            loading: true
        })
        const newList = [];
        _.forEach([...featureGroups], (i) => newList.push(i.id))
        update(newList.concat(items))
    }

    function handlerRemove(item) {
        if (!item)
            return;
        setState({
            ...state,
            loading: true
        })
        const fg = (() => {
            const fgId = []
            _.forEach(featureGroups, f => fgId.push(f.id))
            return fgId
        })()
        _.remove(fg, (id) => id === item.id)

        update(fg)
    }

    function update(list) {
        setState({
            ...state,
            loading: true
        })

        ControllerProduct.Categories.updateFeaturesGroup({id: catId, featureGroups: list}).finally(() => {
            ControllerProduct.Categories.getCategory({catId: catId}).then((res) => {

                setFeatureGroups(res.data.feature_groups)
            }).finally(() => {
                setState({
                    ...state,
                    loading: false
                })
            })
        })
    }

    if (catId) return <Box py={2} width={1} display={'flex'}>
        <Box width={1} component={Card} p={2} display={'flex'} flexDirection={'column'}>
            <Box>
                <BaseButton
                    variant={"outlined"}
                    onClick={() => {
                        setOpen(true)
                    }}
                    style={{
                        borderColor: green[300]
                    }}>
                    افزودن گروه فیچر جدید
                </BaseButton>
            </Box>
            <Box display={'flex'} pt={2} flexWrap={'wrap'}>
                {featureGroups.map(f => (
                    <FeatureGroupItem
                        key={f.id}
                        onRemove={() => handlerRemove(f)}
                        item={f} width={1 / 3}/>
                ))}
            </Box>
        </Box>
        <SelectFeaturesGroup
            boxId={boxId}
            multiSelect={true}
            open={open}
            onSelect={handleOnSelected}/>
    </Box>

    return null


}
