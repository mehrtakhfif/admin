import React,{useState,useEffect} from "react";
import _ from "lodash";
import BaseButton from "../base/button/BaseButton";
import Typography from "../base/Typography";
import {activeLang} from "../../repository";
import {UtilsStyle} from "../../utils/Utils";
import Link from "../base/link/Link";
import {getActiveParents} from "../../pages/categories/CategorySingle";
import {useTheme,Box,Card,Collapse,Divider} from "@material-ui/core"
import {cyan,grey,red,orange} from "@material-ui/core/colors"
import {PersonAddOutlined,DeleteOutlined,CloseOutlined,PeopleAltOutlined,NavigateBefore} from "@material-ui/icons"
import axios from 'axios'
import { api } from './../../controller/api'
import { Button } from '@material-ui/core'
import { gcLog } from "../../utils/ObjectUtils";



export default function TreeViewComponent({catId, rootCategory, categories, activeCategory, onActiveCategoryChange, ...props}) {
    const theme = useTheme();
    
    const [open, setOpen] = useState(false)
    const [data,setData] = useState(categories)
    const [parents, setParents] = useState(initParent())
    const [parentId, setParentId] = useState(activeCategory?activeCategory?.parent?.id:rootCategory.id)
    

    function initParent() {
        if (!(activeCategory && activeCategory.parent))
            return null;
        const parent = [];
        let ac = activeCategory;
        while (ac) {
            parent.push(ac);
            if (ac.parent) {
                ac = ac.parent;
                continue;
            }
            ac = false
        }
        _.reverse(parent);
        return parent;
    }
    
    
    useEffect(() => {
        axios.get(api.Products.categories,{params:{parent_id:parentId}}).then(res => {
                setData(res.data.data)
        })
                return () => {
            
        }
    }, [parentId])

    function parentIdChange(id){
        setParentId(id?id:activeCategory.parent.id)
    }
    
    
    const activeParents = (activeCategory && data) ? getActiveParents(activeCategory.id, data) : []

    return (
        <Box display={'flex'} width={1} flexDirection={'column'}>
            <Box component={Card} px={3} py={2}>
                <Box display={'flex'} pb={3} alignItems={'center'} flexWrap={'wrap'}>
                    {
                        parents &&
                        <BaseButton
                            variant={'outlined'}
                            onClick={() => {
                                setParents(null)
                                // setData(categories)
                                parentIdChange()
                            }}
                            style={{
                                borderColor: cyan[300],
                                marginLeft: theme.spacing(1)
                            }}>
                            <PersonAddOutlined style={{color: cyan[700], marginLeft: theme.spacing(1)}}/>
                            بازنشانی لیست
                        </BaseButton>
                    }
                    {
                        activeCategory &&
                        <BaseButton
                            variant={'outlined'}
                            onClick={() => {
                                onActiveCategoryChange(null)
                            }}
                            style={{
                                borderColor: red[300],
                                marginLeft: theme.spacing(1)
                            }}>
                            <DeleteOutlined style={{color: red[700], marginLeft: theme.spacing(0.5)}}/>
                            حذف والد
                        </BaseButton>
                    }
                    <BaseButton
                        variant={"outlined"}
                        onClick={() => {
                            setOpen(s=>!s)
                        }}
                        style={{
                            borderColor: orange[300]
                        }}>
                        {open ?
                            <React.Fragment>
                                <CloseOutlined style={{color: orange[700], marginLeft: theme.spacing(0.5)}}/>
                                بستن
                            </React.Fragment> :
                            <React.Fragment>
                                <PeopleAltOutlined style={{color: orange[700], marginLeft: theme.spacing(0.5)}}/>
                                انتخاب والد
                            </React.Fragment>}
                    </BaseButton>
                    <Box>
                        {
                            activeCategory &&
                            <Typography variant={'h6'} fontWeight={600} py={2} pr={3}>
                                والد: {activeCategory.name[activeLang]}
                            </Typography>
                        }
                    </Box>
                </Box>
                <Collapse in={open}>
                    <Box display={'flex'} flexDirection={'column'}
                         style={{
                             border: `1px solid ${grey[300]}`,
                             ...UtilsStyle.borderRadius(5)
                         }}>
                        {parents &&
                        <Box display={'flex'} flexWrap={'wrap'} alignItems={'center'} pt={2} px={2}>
                            <Link variant={'h6'}
                                  pb={2}
                                  color={grey[700]}
                                  onClick={() => {
                                        setParents(null)
                                        // setData(categories)
                                        parentIdChange()
                                  }}>
                                همه
                            </Link>
                            {parents?.map((p, i) => (
                                <React.Fragment key={i}>
                                    <NavigateBefore style={{
                                        marginRight: theme.spacing(1),
                                        marginLeft: theme.spacing(1),
                                        marginBottom: theme.spacing(2)
                                    }}/>
                                    {
                                        parents.length !== i + 1 ?
                                            <Link variant={'h6'}
                                                  pb={2}
                                                  color={grey[700]}
                                                  onClick={() => {
                                                      const newParent = [];
                                                      _.forEach(parents, (pr) => {
                                                          newParent.push(pr);
                                                          if (p.id === pr.id)
                                                              return false;
                                                      });
                                                      
                                                    setParents(newParent)
                                                    // setData(p.child)
                                                    parentIdChange(p.id)
                                                  }}>
                                                {p.name[activeLang]}
                                            </Link> :
                                            <Typography variant={'h6'}
                                                        pb={2}
                                                        color={grey[400]}
                                                        style={{
                                                            cursor: 'default'
                                                        }}>
                                                {p?.name?.[activeLang]}
                                            </Typography>
                                    }

                                </React.Fragment>
                            ))}
                        </Box>}
                        <Box maxHeight={300} overflow={'auto'}>
                        {data && data.map((c, index) => {
                            const isActive = _.findIndex(activeParents, (p) => p.id === c.id) !== -1;
                            return (
                                <React.Fragment key={c.id}>
                                    <Box display={'flex'} alignItems={'center'}
                                         px={2}
                                         style={{
                                            //  backgroundColor: catId === c.id ? theme.palette.action.disabledBackground : (activeCategory && c.id === activeCategory.id) ? cyan[100] : isActive ? orange[50] : index % 2 !== 0 ? theme.palette.action.disabledBackground : null
                                         }}>
                                        <Typography py={2} variant={'h6'}
                                                    fontWeight={400}
                                                    style={{
                                                        flex: 1
                                                    }}>
                                            {c?.name?.[`${activeLang}`]?c?.name?.[`${activeLang}`]:c?.name}
                                        </Typography>
                                        <Box display={'flex'} py={1}>
                                            { c.child_count!== 0 &&
                                            <Button
                                                // disabled={(activeCategory && c.id === activeCategory.id) || catId === c.id}
                                                onClick={() => {
                                                    const newParents = parents ? parents : [];
                                                    newParents.push(c);
                                                    setParents(newParents)
                                                    parentIdChange(c.id)
                                                    // setData(c.child)
                                                }}
                                                style={{
                                                    borderColor: cyan[300],
                                                    marginLeft: theme.spacing(1)
                                                }}>
                                                نمایش فرزندان
                                            </Button>}
                                            <Button
                                                disabled={(activeCategory && c.id === activeCategory.id) || catId === c.id}
                                                onClick={() => onActiveCategoryChange(c)}>
                                                انتخاب
                                            </Button>
                                        </Box>
                                    </Box>
                                    {data.length !== index + 1 &&
                                    <Divider/>
                                    }
                                </React.Fragment>
                            )
                        })}
                        </Box>
                    </Box>
                </Collapse>
            </Box>
        </Box>
    )
}

