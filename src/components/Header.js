import React, {useState} from 'react';
import clsx from 'clsx';
import {makeStyles, MuiThemeProvider} from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Box from "@material-ui/core/Box";
import {
    Category,
    Dashboard,
    Dvr,
    EmojiFlags, Event,
    ExpandLess,
    ExpandMore,
    ExtensionOutlined,
    FlipCameraAndroidOutlined,
    Group,
    InboxOutlined,
    Language,
    Menu,
    NightsStay,
    ReceiptOutlined,
    ShoppingCartOutlined,
    Storage,
    WbSunny
} from "@material-ui/icons";
import {UtilsStyle} from "../utils/Utils";
import _ from 'lodash'
import {DEBUG, lang, theme} from "../repository";
import rout from "../router";
import Link from "./base/link/Link";
import LinearProgress from "@material-ui/core/LinearProgress";
import {useDispatch, useSelector} from "react-redux";
import Collapse from "@material-ui/core/Collapse";
import Tooltip from "@material-ui/core/Tooltip";
import Switch from "@material-ui/core/Switch";
import {setLiteMode, setSiteMode} from "../redux/actions/BaseAction";
import createPalette from "@material-ui/core/styles/createPalette";
import storage from "../storage";
import Icon from "@material-ui/core/Icon";
import BaseDialog from "./base/BaseDialog";
import Typography from "./base/Typography";
import BaseButton from "./base/button/BaseButton";
import Img from "./base/img/Img";
import {cyan, yellow} from "@material-ui/core/colors";

const drawerWidth = 240;
const menuHeight = 70;

const headerUseStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
    },
    headerUseAppBar: {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    headerUseAppBarShift: {
        '&[class*="MuiAppBar-root"]': {
            marginLeft: drawerWidth,
            width: `calc(100% - ${drawerWidth}px)`,
            transition: theme.transitions.create(['width', 'margin'], {
                easing: theme.transitions.easing.sharp,
                duration: theme.transitions.duration.enteringScreen,
            }),
        }
    },
    menuButton: {
        marginRight: 36,
    },
    toolbar: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: theme.spacing(0, 1),
        ...theme.mixins.toolbar,
    },
    content: {},
    listItem: ({open}) => ({
        '&>div': {
            width: '100%',
            '&>a': {
                display: 'flex',
                width: '100%',
                flexDirection: open ? 'row-reverse' : 'row',
                '&>div:first-child': {
                    justifyContent: open ? 'flex-end' : 'flex-start',
                    ...UtilsStyle.transition()
                },
                '&>div:nth-child(2)': {
                    display: open ? 'flex' : 'none',
                    textAlign: open ? 'right' : 'left'
                }
            }
        }
    })
}));

export default function Header({...props}) {
    const dispatch = useDispatch();

    const [open, setOpen] = React.useState(storage.Base.Drawer.getOpen);
    const classes = headerUseStyles({open: open});
    const {axios, ...u} = useSelector(state => state);
    const {isLogin} = u.user;
    const {darkMode, lite} = u.base;

    const handleDrawerOpen = () => {
        storage.Base.Drawer.setOpen(true);
        setOpen(true);
    };

    const handleDrawerClose = () => {
        storage.Base.Drawer.setOpen(false);
        setOpen(false);
    };

    return (
        isLogin ?
            <div className={classes.root}>
                <CssBaseline/>
                <AppBar
                    position="fixed"
                    className={clsx(classes.headerUseAppBar, {
                        [classes.headerUseAppBarShift]: open,
                    })}
                    style={{
                        height: menuHeight
                    }}>
                    <Toolbar>
                        <Box display={'flex'} width={1}>
                            <IconButton onClick={handleDrawerOpen} edge="start" color="inherit" aria-label="menu">
                                <Menu/>
                            </IconButton>
                            <Box display={'flex'} flex={1} justifyContent={'flex-end'}>
                                <Telegram/>
                                <Email/>
                                <Box display={'flex'} alignItems={'center'}>
                                    <Box ml={1} display={'flex'} alignItems={'center'}>
                                        {!darkMode ?
                                            <WbSunny style={classes.icon}/> :
                                            <NightsStay/>
                                        }
                                    </Box>
                                    <MuiThemeProvider
                                        theme={{
                                            ...theme,
                                            palette: createPalette({
                                                primary: {
                                                    ...theme.palette.primary
                                                },
                                                secondary: {
                                                    light: "#fff",
                                                    main: "#fff",
                                                    dark: "#fff"
                                                }
                                            }),
                                        }}>
                                        <Switch
                                            checked={darkMode}
                                            onChange={() => dispatch(setSiteMode(!darkMode))}/>
                                    </MuiThemeProvider>
                                </Box>
                                <Box ml={1} display={'flex'} alignItems={'center'}>
                                    <Tooltip title={!lite ? "فعال کردن نسخه سبک" : "غیرفعال کردن نسخه سبک"}>
                                        <IconButton
                                            onClick={() => {
                                                dispatch(setLiteMode(!lite))
                                            }}>
                                            <Icon
                                                className={"fad fa-circle"}
                                                style={{
                                                    color: lite ? cyan[400] : yellow[100]
                                                }}/>
                                        </IconButton>
                                    </Tooltip>
                                </Box>
                            </Box>
                        </Box>
                    </Toolbar>
                    {!lite && axios?.loading < axios?.totalRequest ?
                        <LinearProgress style={{
                            backgroundColor: '#fff',
                        }}/> : null}
                </AppBar>
                <Box display='flex'>
                    <Drawers hasOpen={open} open={open} onClose={handleDrawerClose} {...props}/>
                    <Box component={'main'} className={classes.content} style={{
                        width: window.innerWidth - 85
                    }}>
                        <div className={classes.toolbar}/>
                        {props.children}
                    </Box>
                </Box>
            </div> :
            <React.Fragment>
                {props.children}
            </React.Fragment>
    );
}

//region Drawers

const useMainDrawersStyle = makeStyles(theme => ({
    minHeaderDrawer: {
        width: drawerWidth,
        flexShrink: 0,
        whiteSpace: 'nowrap',
    },
    minHeaderDrawerOpen: {
        width: drawerWidth,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    minHeaderDrawerClose: {
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        overflowX: 'hidden',
        width: theme.spacing(7) + 1,
        [theme.breakpoints.up('sm')]: {
            width: theme.spacing(9) + 1,
        },
    },
}))

function Drawers({open, hasOpen, onClose, ...props}) {
    const {roll: userRole} = useSelector(state => state.user)
    const classes = useMainDrawersStyle();
    const [hoverState, setHoverState] = React.useState({});
    const [childrenState, setChildrenState] = React.useState({});
    return (
        <Drawer
            variant="permanent"
            className={clsx(classes.minHeaderDrawer, {
                [classes.minHeaderDrawerOpen]: hasOpen,
                [classes.minHeaderDrawerClose]: !hasOpen,
            })}
            classes={{
                paper: clsx({
                    [classes.minHeaderDrawerOpen]: hasOpen,
                    [classes.minHeaderDrawerClose]: !hasOpen,
                }),
            }}>
            <Box className={classes.toolbar}
                 display={'flex'}
                 alignItems={'center'}
                 style={{
                     minHeight: menuHeight
                 }}>
                <IconButton onClick={onClose}>
                    {theme.direction === 'rtl' ? <ChevronRightIcon/> : <ChevronLeftIcon/>}
                </IconButton>
            </Box>
            <Divider/>
            {
                !_.isEmpty(menuItems[userRole]) &&
                menuItems[userRole].map((items, index) => (
                    <React.Fragment key={index}>
                        {index !== 0 ? <Divider/> : null}
                        {
                            items.map(({label, icon, rout, children}, index) => (
                                <Box key={label} display={'flex'} flexDirection={'column'}>
                                    <Tooltip title={lang.get(label)} disableHoverListener={hasOpen}>
                                        <Box display={'flex'} alignItems={'center'}>
                                            <ListItem button key={label}
                                                      className={classes.listItem}
                                                      onMouseEnter={(e) => {
                                                          if (!children || childrenState[label])
                                                              return;
                                                          const state = hoverState;
                                                          state[label] = true;
                                                          setHoverState({...state});
                                                      }}
                                                      onMouseLeave={() => {
                                                          if (!children || childrenState[label])
                                                              return;
                                                          const state = hoverState;
                                                          state[label] = false;
                                                          setHoverState({...state});
                                                      }}
                                                      style={{
                                                          padding: 0,
                                                          display: 'flex',
                                                          width: '100%'
                                                      }}>
                                                <Link toHref={rout()}
                                                      width={1}
                                                      linkStyle={{
                                                          display: 'flex',
                                                          width: '100%',
                                                          paddingRight: theme.spacing(2),
                                                          paddingLeft: theme.spacing(2),
                                                          paddingTop: theme.spacing(1),
                                                          paddingBottom: theme.spacing(1),
                                                          flexDirection: 'row-reverse'
                                                      }}
                                                      style={{
                                                          textAlign: 'right',
                                                          display: 'flex',
                                                          width: '100%',
                                                      }}>
                                                    {(!hasOpen || ((!children || !hoverState[label]) && !childrenState[label])) && icon ?
                                                        <ListItemIcon style={{
                                                            display: 'flex',
                                                            flexDirection: ' row-reverse',
                                                        }}>{icon}</ListItemIcon> : null
                                                    }
                                                    {hasOpen && <ListItemText primary={lang.get(label)}/>}
                                                </Link>
                                                {
                                                    (hasOpen && children && (hoverState[label] || childrenState[label])) ? (
                                                        <IconButton
                                                            size={"small"}
                                                            aria-label={label}
                                                            onClick={() => {
                                                                const state = childrenState;
                                                                state[label] = !_.isBoolean(state[label]) ? true : state[label] !== true;
                                                                setChildrenState({...state})
                                                            }}
                                                            style={{
                                                                marginLeft: theme.spacing(2),
                                                            }}>
                                                            {
                                                                !childrenState[label] ?
                                                                    <ExpandMore/> :
                                                                    <ExpandLess/>
                                                            }
                                                        </IconButton>
                                                    ) : null
                                                }
                                            </ListItem>
                                        </Box>
                                    </Tooltip>
                                    {(hasOpen && children) ? (
                                        <Collapse in={childrenState[label]}>
                                            <Box display={'flex'} alignItems={'center'} flexDirection={'column'} pr={1}>
                                                {
                                                    children.map(({label: labelCh, icon: iconCh, rout: routCh}, indexCh) => (
                                                        <ListItem key={indexCh + 'child'} button
                                                                  className={classes.listItem}>
                                                            <Link toHref={routCh()}
                                                                  linkStyle={{width: '100%'}}
                                                                  style={{width: '100%', textAlign: 'right'}}>
                                                                {
                                                                    iconCh ?
                                                                        <ListItemIcon>{iconCh}</ListItemIcon>
                                                                        : null
                                                                }
                                                                <ListItemText primary={lang.get(labelCh)}/>
                                                            </Link>
                                                        </ListItem>
                                                    ))
                                                }
                                            </Box>
                                            <Divider/>
                                        </Collapse>
                                    ) : null}
                                </Box>
                            ))
                        }
                    </React.Fragment>
                ))
            }

        </Drawer>
    )
}

//endregion Drawers

function Telegram() {

    const user = useSelector(state => state.user)


    const [open, setOpen] = useState(DEBUG ? false : (!user.telegram_username));

    function onClose() {
        setOpen(false)
    }


    return (
        <Box px={1} display={'flex'} alignItems={'center'}>
            {
                !user.telegram_username ?
                    <IconButton
                        className={DEBUG ? undefined : 'shake'}
                        onClick={() => {
                            setOpen(true)
                        }}>
                        <Icon className="fab fa-telegram-plane" style={{color: "#fff"}}/>
                    </IconButton> :
                    <React.Fragment>
                        <Tooltip
                            title={_.isNaN(_.toNumber(user.telegram_username)) ? `${user.telegram_username}@` : user.telegram_username}>
                            <Box className={'shakeHover'} px={2}>
                                <Img src={user.avatar}
                                     width={35} height={35}
                                     minHeight={1}
                                     style={{
                                         ...UtilsStyle.borderRadius('100%')
                                     }}/>
                            </Box>
                        </Tooltip>
                    </React.Fragment>
            }
            <BaseDialog open={open} onClose={onClose}>
                <Box p={3} display={'flex'} flexDirection={'column'}>
                    <Typography pb={4} variant={'h3'} style={{textAlign: 'center', lineHeight: 1.7}}>
                        فیلتر شکن خود را روشن کنید و دگمه زیر را بفشارید
                    </Typography>
                    <Box display={'flex'} justifyContent={'center'}>
                        <BaseButton
                            onClick={() => {
                                window.open('https://api.mehrtakhfif.com/admin/tg_login', '_self',);
                            }}>
                            <Box display={'flex'}>
                                <Icon fontSize={"large"} className="fab fa-telegram" style={{color: "#2FA6DE"}}/>
                                <Typography pr={1} variant={'body1'} alignItems={'center'}>
                                    عضویت در ربات مهرتخفیف
                                </Typography>
                            </Box>
                        </BaseButton>
                    </Box>
                    <Typography pt={5} variant={'h6'} style={{textAlign: 'center'}}>
                        برای اطلاع رسانی از فروش محصولات و اخبار سایت عضو ربات مهرتخفیف شوید.
                    </Typography>
                </Box>
            </BaseDialog>
        </Box>
    )
}

function Email() {
    const user = useSelector(state => state.user)
    const pattern = /.*@mehrtakhfif\.com/g;

    return (
        DEBUG || pattern.test(user.email) ?
            <React.Fragment>
                <Box px={1} display={'flex'} alignItems={'center'}>
                    <Link toHref={"https://box.mehrtakhfif.com/mail/"} target={'_blank'}>
                        <Tooltip title={"ایمیل کاری"}>
                            <IconButton>
                                <Icon className={"fas fa-envelope"}
                                      style={{
                                          color: '#fff',
                                          width: '40px !important',
                                          height: '40px !important'
                                      }}/>
                            </IconButton>
                        </Tooltip>
                    </Link>
                </Box>
                <Box px={1} display={'flex'} alignItems={'center'}>
                    <Link toHref={"https://box.mehrtakhfif.com/cloud/"} target={'_blank'}>
                        <Tooltip title={"فضای ابری"}>
                            <IconButton>
                                <Icon className={"fas fa-cloud"}
                                      style={{
                                          color: '#fff',
                                          minWidth: 40,
                                          minHeight: 40
                                      }}/>
                            </IconButton>
                        </Tooltip>
                    </Link>
                </Box>
            </React.Fragment> :
            <React.Fragment/>
    )
}


//region items

const items = {
    dashboard: {
        label: 'dashboard',
        icon: <Dashboard/>,
        rout: () => rout.Main.dashboard
    },
    products: {
        label: 'products',
        icon: <Dvr/>,
        rout: () => rout.Product.rout,
        children: [
            {
                label: 'add_product',
                rout: () => rout.Product.Single.createNewProduct,
            },
            {
                label: 'show_all_products',
                rout: () => rout.Product.rout,
            },
        ]
    },
    review_products: {
        label: 'review_products',
        icon: <FlipCameraAndroidOutlined/>,
        rout: () => rout.Product.Review.rout,
    },
    storage: {
        label: 'storages',
        icon: <Storage/>,
        rout: () => rout.Storage.rout,
        children: [
            {
                label: 'show_all_storage',
                rout: () => rout.Storage.rout,
            },
        ]
    },
    category: {
        label: 'categories',
        icon: <Category/>,
        rout: () => rout.Category.rout,
        children: [
            {
                label: 'add_category',
                rout: () => rout.Category.Single.createNewProduct,
            },
            {
                label: 'show_all_categories',
                rout: () => rout.Category.rout,
            },
        ]
    },
    features: {
        label: 'features',
        icon: <ExtensionOutlined/>,
        rout: () => rout.Feature.rout,
        children: [
            {
                label: 'add_features',
                rout: () => rout.Feature.Single.createNewProduct,
            },
            {
                label: 'show_all_features',
                rout: () => rout.Feature.rout,
            },
        ]
    },
    featuresGroup: {
        label: 'feature_group',
        icon: <Icon className={"fal fa-layer-group"}/>,
        rout: () => rout.FeatureGroup.rout,
        children: [
            {
                label: 'add_features',
                rout: () => rout.FeatureGroup.Single.createNewProduct,
            },
            {
                label: 'show_all_features',
                rout: () => rout.FeatureGroup.rout,
            },
        ]
    },
    brands: {
        label: 'brands',
        icon: <EmojiFlags/>,
        rout: () => rout.Brand.rout,
    },
    supplier: {
        label: 'supplier',
        icon: <Group/>,
        rout: () => rout.Supplier.rout,
        children: [
            {
                label: 'add_supplier',
                rout: () => rout.Supplier.Single.createNewProduct,
            },
            {
                label: 'show_all_supplier',
                rout: () => rout.Supplier.rout,
            },
        ]
    },
    box: {
        label: 'mt_box',
        icon: <InboxOutlined/>,
        rout: () => rout.Box.Dashboard.rout,
        children: [
            {
                label: 'templates',
                rout: () => rout.Box.Templates.rout,
            },
        ]
    },
    invoice: {
        label: 'invoice',
        icon: <ReceiptOutlined/>,
        rout: () => rout.Invoice.rout,
        children: [
            {
                label: 'show_invoice',
                rout: () => rout.Invoice.Single.show,
            },
        ]
    },
    invoiceProduct: {
        label: 'invoice_product',
        icon: <ShoppingCartOutlined/>,
        rout: () => rout.InvoiceProduct.rout,

    },
    site: {
        label: 'site_settings',
        icon: <Language/>,
        rout: () => rout.Site.Dashboard.rout
    },
    accountants: {
        label: 'accountants',
        icon: <Icon className={"far fa-calculator"}/>,
        rout: () => rout.Accountants.rout
    },
    booking: {
        label: 'booking',
        icon: <Event/>,
        rout: () => rout.Booking.rout,
    },
    house: {},
    residentType: {}
};

const superuserItem = [
    [
        items.dashboard,
        items.products,
        items.review_products,
        items.category,
        items.storage,
        items.brands,
        items.supplier,
    ],
    [
        items.box,
        items.invoice,
        items.invoiceProduct,
        items.booking,
        items.accountants,
        items.features,
        items.featuresGroup,
        items.site
    ]
]

const contentManagerItem = [
    [
        items.dashboard,
        items.products,
        items.review_products,
        items.category,
        items.storage,
        items.brands,
        items.supplier,
    ],
    [
        items.box,
        items.invoice,
        items.features,
        items.featuresGroup,
        items.site
    ]
]

const adminItem = [
    [
        items.dashboard,
        items.products,
        items.review_products,
        items.category,
        items.storage,
        items.brands,
        items.supplier,
    ],
    [
        items.box,
        items.invoiceProduct,
        items.featuresGroup,
        items.booking,
    ]
];
const admin2Item = [
    [
        items.dashboard,
        items.products,
        items.review_products,
        items.category,
        items.storage,
        items.brands,
        items.supplier,
    ],
    [
        items.box,
        items.featuresGroup,
    ]
];

const supportItem = [
    [
        items.dashboard,
        items.products,
        items.category,
        items.storage,
        items.brands,
        items.supplier,
    ],
    [
        items.box,
        items.invoice,
        items.invoiceProduct,
        items.features,
        items.featuresGroup,
    ]
];

const designer = [
    [
        items.dashboard,
        items.products,
        items.review_products,
        items.category,
        items.storage,
    ],
    [
        items.site,
    ]
];

const post = [
    [
        items.dashboard,
        items.invoice,
    ],
    []
];

const accountants = [
    [
        items.dashboard,
        items.products,
        items.storage,
        items.invoice,
        items.accountants,
    ]
];


const menuItems = {
    admin: adminItem,
    admin2: admin2Item,
    support: supportItem,
    content_manager: contentManagerItem,
    superuser: superuserItem,
    designer: designer,
    accountants: accountants,
    ha_accountants: [
        [
            items.dashboard,
            items.invoice,
        ]
    ],
    post: post
};

//endregion items


//region Hidden

// export function ShowSM({...props}) {
//     return (
//         <Hidden lgUp={true}>
//             {props.children}
//         </Hidden>
//     )
// }
//
// export function HiddenSM({...props}) {
//     return (
//         <Hidden mdDown={true}>
//             {props.children}
//         </Hidden>
//     )
// }

//endregion Hidden
