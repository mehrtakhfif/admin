import {grey} from "@material-ui/core/colors";
import React, {useEffect, useState} from "react";
import moment from "moment";
import _ from "lodash";
import Box from "@material-ui/core/Box";
import {lang} from "../repository";
import Typography from "./base/Typography";


export default function Timer({time, textVariant = 'h6', textColor = grey[200], itemPadding = 0.5, ...props}) {
    const [state, setState] = useState({
        duration: moment.duration((time - moment().unix()) * 1000, 'milliseconds')
    });
    useEffect(() => {
        const interval = setInterval(() => {
            setState({
                ...state,
                duration: moment.duration((time - moment().unix()) * 1000, 'milliseconds')
            });
        }, 1000);
        return () => clearInterval(interval);
    }, []);

    function serializeTime(val) {
        const v = _.toString(val);
        return v.length === 1 ? '0' + v : v;
    }

    const du = state.duration;
    const days = serializeTime(Math.floor(du.asDays()));
    const hours = serializeTime(du.hours());
    const minutes = serializeTime(du.minutes());
    const seconds = serializeTime(du.seconds());

    return (
        <Box display={'flex'} alignItems={'flex-start'} justifyContent={'center'}
             dir={'ltr'}
             {...props}
             style={{color: '#f1f1f1', ...props.style}}>
            <TimerItem time={days} title={lang.get('day')} textVariant={textVariant} textColor={textColor}
                       itemPadding={itemPadding}/>
            <Typography variant={textVariant} color={textColor} px={0.2}>
                :
            </Typography>
            <TimerItem time={hours} title={lang.get('hour')} textVariant={textVariant} textColor={textColor}
                       itemPadding={itemPadding}/>
            <Typography variant={textVariant} color={textColor} px={0.2}>
                :
            </Typography>
            <TimerItem time={minutes} title={lang.get('minute')} textVariant={textVariant} textColor={textColor}
                       itemPadding={itemPadding}/>
            <Typography variant={textVariant} color={textColor} px={0.2}>
                :
            </Typography>
            <TimerItem time={seconds} title={lang.get('second')} textVariant={textVariant} textColor={textColor}
                       itemPadding={itemPadding}/>
        </Box>
    )
}

function TimerItem({time, title, textVariant, textColor, itemPadding, ...props}) {
    return (
        <Box display={'flex'} flexDirection={'column'} alignItems={'center'}
             justifyContent={'flex-end'} px={itemPadding}>
            <Typography variant={textVariant} color={textColor}>
                {time}
            </Typography>
            <Typography variant={'caption'} color={textColor}>
                {title}
            </Typography>
        </Box>
    )
}
