import React, { Fragment, useEffect, useState } from "react";
import BaseDialog from "../base/BaseDialog";
import { useSnackbar } from "notistack";
import CircularProgress from "@material-ui/core/CircularProgress";
import { cyan, green, red } from "@material-ui/core/colors";
import Backdrop from "@material-ui/core/Backdrop";
import useSWR, { mutate } from "swr";
import ControllerProduct from "../../controller/ControllerProduct";
import _ from "lodash";
import storage from "../../storage";
import { activeLang, cookieVersion, lang, theme } from "../../repository";
import ComponentError from "../base/ComponentError";
import Box from "@material-ui/core/Box";
import Table, {
  createTableHeader,
  headerItemTemplate,
} from "../base/table/Table";
import { createFilter } from "../base/table/Filters";
import DataUtils, { filterType } from "../../utils/DataUtils";
import rout, { siteRout } from "../../router";
import Tooltip from "@material-ui/core/Tooltip";
import IconButton from "@material-ui/core/IconButton";
import Link from "../base/link/Link";
import {
  CopyrightOutlined,
  Delete,
  DeleteOutlined,
  FiberManualRecord,
  FileCopyOutlined,
  VisibilityOutlined,
} from "@material-ui/icons";
import Typography from "../base/Typography";
import { gcLog, tryIt } from "../../utils/ObjectUtils";
import { Button, Icon } from "@material-ui/core";
import BaseButton from "../base/button/BaseButton";
import { Util } from "leaflet/dist/leaflet-src.esm";
import { Utils } from "../../utils/Utils";
import { style } from "@material-ui/system";
import axios from "axios";
import api from "../../controller/api";

const CookieKey = "DISCOUNT_CODE";

const headers = [
  ...headerItemTemplate.idAction(),
  createTableHeader({ id: "code", type: "str", label: "کد", sortable: false }),
  createTableHeader({
    id: "invoice_id",
    type: "str",
    label: "خریداری‌شده",
    sortable: true,
  }),
  headerItemTemplate.createdBy,
  headerItemTemplate.createdAt,
];
const filters = [
  createFilter({
    id: "name__fa",
    type: filterType.text,
    label: "نام",
    value: "",
  }),
];

const filterButton = [
  {
    key: "all",
    label: "همه",
    color: cyan[400],
  },
  {
    key: "not_purchased",
    label: "خرید نشده",
    color: red[400],
  },
  {
    key: "purchased",
    label: "خریدشده",
    color: green[400],
  },
];

export default function ({
  open,
  sId: storageId,
  boxPrefix,
  onClose,
  ...props
}) {
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const [tableData, setTableData] = useState();
  const [state, setState] = useState((oldState) => ({
    ...oldState,
    order: storage.PageSetting.getOrder(CookieKey),
    pagination: {
      rowPerPage: storage.PageSetting.getRowPerPage(cookieVersion),
      page: storage.PageSetting.getPage(cookieVersion),
    },
  }));
  //region state
  const [filtersState, setFiltersState] = useState({
    data: filters,
    resetFilters: () => {
      setFiltersState({
        ...filtersState,
        data: DataUtils()
          .TableFilter()
          .resetFilter({ filters: filtersState.data }),
      });
    },
  });

  //endregion state

  const d = ControllerProduct.DiscountCode.get({
    storageId: storageId,
    order: state.order,
    page: state.pagination.page,
    step: state.pagination.rowPerPage,
  });

  const reqToken = d[0] + (open ? "-true" : "-false");

  const { data, error } = useSWR(reqToken, () => {
    if (!open) return undefined;
    return d[1]();
  });

  useEffect(() => {
    if (open) mutate(d[0]);
  }, [open]);

  useEffect(() => {
    setTableData(
      (data && data.data
        ? data.data
        : [...Array(state.pagination.rowPerPage)]
      ).map((code, i) => {
        const singleRout = tryIt(() => {
          return code.invoice_id
            ? rout.Invoice.Single.showInvoice(code.invoice_id)
            : undefined;
        });
        return code
          ? {
              id: {
                label: code.id,
              },
              actions: {
                label: (
                  <Box display={"flex"} alignItems={"center"} pl={1}>
                    <Tooltip title={"نمایش فاکتور"}>
                      <span>
                        <IconButton
                          size={"small"}
                          disabled={!Boolean(singleRout)}
                          onClick={(e) => {
                            e.stopPropagation();
                          }}
                          style={{
                            marginRight: theme.spacing(0.5),
                            marginLeft: theme.spacing(0.5),
                          }}
                        >
                          {singleRout ? (
                            <Link
                              toHref={singleRout}
                              linkStyle={{ display: "flex" }}
                              hoverColor={theme.palette.primary.main}
                            >
                              <VisibilityOutlined
                                style={{ fontSize: 20, padding: 2 }}
                              />
                            </Link>
                          ) : (
                            <VisibilityOutlined
                              style={{ fontSize: 20, padding: 2 }}
                            />
                          )}
                        </IconButton>
                      </span>
                    </Tooltip>
                    <Tooltip title={"حذف"}>
                      <span>
                        <IconButton
                          size={"small"}
                          onClick={(e) => {
                            e.stopPropagation();
                            axios
                              .delete(api.Products.discountCode, {
                                params: { ids: code.id },
                              })
                              .then((res) => {
                                mutate(reqToken);
                                enqueueSnackbar("حذف شد", {
                                  variant: "warning",
                                  anchorOrigin: {
                                    vertical: "bottom",
                                    horizontal: "right",
                                  },
                                  action: (key) => (
                                    <Fragment>
                                      <Button
                                        onClick={() => {
                                          closeSnackbar(key);
                                        }}
                                      >
                                        {lang.get("close")}
                                      </Button>
                                    </Fragment>
                                  ),
                                });
                              });
                          }}
                          style={{
                            marginRight: theme.spacing(0.5),
                            marginLeft: theme.spacing(0.5),
                          }}
                        >
                          <DeleteOutlined />
                        </IconButton>
                      </span>
                    </Tooltip>
                    <Tooltip title={"کپی"}>
                      <span>
                        <IconButton
                          size={"small"}
                          onClick={(e) => {
                            e.stopPropagation();
                            navigator.clipboard.writeText(code.code);
                            enqueueSnackbar("کپی شد.", {
                              variant: "info",
                              anchorOrigin: {
                                vertical: "bottom",
                                horizontal: "right",
                              },
                              action: (key) => (
                                <Fragment>
                                  <Button
                                    onClick={() => {
                                      closeSnackbar(key);
                                    }}
                                  >
                                    {lang.get("close")}
                                  </Button>
                                </Fragment>
                              ),
                            });
                          }}
                          style={{
                            marginRight: theme.spacing(0.5),
                            marginLeft: theme.spacing(0.5),
                          }}
                        >
                          <FileCopyOutlined />
                        </IconButton>
                      </span>
                    </Tooltip>
                  </Box>
                ),
              },
              code: {
                label: (
                  <Typography variant={"h6"} justifyContent={"center"}>
                    {code.code}
                  </Typography>
                ),
              },
              invoice_id: {
                label: (() => {
                  const sold = Boolean(code.invoice_id);
                  return (
                    <Tooltip title={sold ? "خرید شده است" : "خرید نشده است"}>
                      <FiberManualRecord
                        className={
                          "fa " + (sold ? "fa-check-circle" : "fa-times-circle")
                        }
                        fontSize={"default"}
                        style={{ color: sold ? green[300] : red[300] }}
                      />
                    </Tooltip>
                  );
                })(),
              },
              created_by: {
                label: (
                  <Typography variant={"h6"} justifyContent={"center"}>
                    {code.createdAt.by.name}
                  </Typography>
                ),
              },
              created_at: {
                label: (
                  <Typography variant={"h6"} justifyContent={"center"}>
                    {code.createdAt.date}
                  </Typography>
                ),
              },
            }
          : {};
      })
    );
  }, [data]);

  //region handler
  function handleFilterChange({ item, value }) {
    setState((oldState) => ({
      ...oldState,
      pagination: {
        ...state.pagination,
        page: 0,
      },
    }));
    setFiltersState((oldState) => ({
      ...oldState,
      data: DataUtils()
        .TableFilter()
        .filterItemChange({
          filters: filtersState.data,
          item: item,
          value: value,
        }),
    }));
  }

  function handleApplyFilter() {
    setState((oldState) => ({
      ...oldState,
      filters: _.cloneDeep(filtersState.data),
    }));
  }

  function handleResetFilter() {
    filtersState.resetFilters();
  }

  function handleChangeOrder(order) {
    storage.PageSetting.setOrder(CookieKey, {
      order: order,
    });
    setState((oldState) => ({
      ...oldState,
      order: {
        ...order,
      },
    }));
  }

  function handleChangeBox(e, box) {
    if (!box || state.activeBox === box.id) return;
    storage.PageSetting.setActiveBox(CookieKey, { activeBox: box.id });
    filtersState.resetFilters();
    setState({
      ...state,
      activeBox: box.id,
      filters: {},
      pagination: {
        ...state.pagination,
        page: 0,
      },
    });
  }

  //endregion handler
  const pg = state.pagination;

  function handleFilterButtonClick(item) {}

  return (
    <BaseDialog
      maxWidth={"xl"}
      open={open}
      header={"نمایش کدها"}
      onClose={onClose}
    >
      {open ? (
        error ? (
          <ComponentError tryAgainFun={() => mutate(d[0])} />
        ) : data ? (
          <Box width={1} display={"flex"} flexDirection={"column"}>
            <Box pb={1} display={"flex"}>
              {filterButton.map((it) => (
                <Box p={1} key={it.key}>
                  <BaseButton
                    variant={"outlined"}
                    onClick={() => handleFilterButtonClick(it)}
                    style={{ borderColor: it.color }}
                  >
                    {it.label}
                  </BaseButton>
                </Box>
              ))}
            </Box>
            <Table
              headers={headers}
              data={tableData}
              loading={false}
              activePage={pg.page}
              rowsPerPage={pg.rowPerPage}
              lastPage={data ? data.pagination.lastPage : 0}
              rowCount={data ? data.pagination.count : 0}
              orderType={state.order}
              onActivePageChange={(page) => {
                setState({
                  ...state,
                  pagination: {
                    ...state.pagination,
                    page: page,
                  },
                });
                // requestData({page: e})
              }}
              onRowPerPageChange={(rowPerPage) => {
                storage.PageSetting.setRowPerPage(CookieKey, {
                  rowPerPage: rowPerPage,
                });
                setState({
                  ...state,
                  pagination: {
                    ...state.pagination,
                    rowPerPage: rowPerPage,
                  },
                });
              }}
              onApplyFilterClick={handleApplyFilter}
              onResetFilterClick={handleResetFilter}
              onChangeOrder={handleChangeOrder}
              onChangeBox={handleChangeBox}
              alignItems={"center"}
              justifyContent={"center"}
            />
          </Box>
        ) : (
          <Backdrop
            open={!data}
            style={{
              zIndex: "9999",
            }}
          >
            <CircularProgress
              color="inherit"
              style={{
                color: cyan[300],
                width: 100,
                height: 100,
              }}
            />
          </Backdrop>
        )
      ) : (
        <React.Fragment />
      )}
    </BaseDialog>
  );
}
