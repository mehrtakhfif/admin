import React, {Fragment, useRef, useState} from "react";
import Box from "@material-ui/core/Box";
import BaseDialog from "../base/BaseDialog";
import Typography from "../base/Typography";
import DefaultTextField from "../base/textField/DefaultTextField";
import _ from "lodash"
import Button from "@material-ui/core/Button";
import FormControl from "../base/formController/FormController";
import {gcLog, toNumberSafe, tryIt} from "../../utils/ObjectUtils";
import SuccessButton from "../base/button/buttonVariant/SuccessButton";
import {useSnackbar} from "notistack";
import {lang} from "../../repository";
import CircularProgress from "@material-ui/core/CircularProgress";
import {cyan} from "@material-ui/core/colors";
import Backdrop from "@material-ui/core/Backdrop";
import ControllerProduct from "../../controller/ControllerProduct";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import {UtilsStyle} from "../../utils/Utils";
import BaseButton from "../base/button/BaseButton";
import {useTheme} from "@material-ui/core";
import DatePickerStartEndPanel from "../base/datePicker/DatePickerStartEndPanel";
import LocalStorageUtils from "../../utils/LocalStorageUtils";
import Checkbox from "@material-ui/core/Checkbox";


const printDiscountCode = "printDiscountCode"
export default function ({open, sId, boxPrefix, onClose, ...props}) {
    const ref = useRef();
    const theme = useTheme();
    const [loading, setLoading] = useState(false);
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();
    const [used, setUsed] = React.useState("not_used");
    const [startTime, setStartTime] = useState(undefined);
    const [endTime, setEndTime] = useState(undefined);
    const [showDate, setShowDate] = useState(LocalStorageUtils.get("showDate" + printDiscountCode + sId, "false") === "true");

    const handleChange = (event) => {
        setUsed(event.target.value);
    };

    function handlePrintClick() {
        setLoading(true)
        try {
            const props = {};
            const {count} = ref.current.serialize()
            props.count = toNumberSafe(count)
            if (props.count < 1) {
                reqCancel("لطفا تعداد را وارد کنید");
                throw ""
            }
            props.used = used === "used"
            if (showDate) {
                props.startTime = startTime
                props.endTime = endTime
            }


            LocalStorageUtils.set("count" + printDiscountCode + sId, props.count)

            ControllerProduct.DiscountCode.print({
                storageId: sId,
                ...props
            }).then(res => {
                window.open(res.data.url)
            }).finally(() => {
                setLoading(false)
            })
        } catch (e) {
            setLoading(false)
        }
    }

    function reqCancel(text = null) {
        if (text)
            enqueueSnackbar(text,
                {
                    variant: "error",
                    action: (key) => (
                        <Fragment>
                            <Button onClick={() => {
                                closeSnackbar(key)
                            }}>
                                {lang.get('close')}
                            </Button>
                        </Fragment>
                    )
                });
    }

    return (
        <BaseDialog open={open && sId} header={"چاپ کد"} onClose={onClose}>
            {
                !loading &&
                <Box display={'flex'} flexDirection={"column"}>
                    <FormControl innerref={ref} p={2} display={'flex'} flexWrap={'wrap'}>
                        <Box width={0.5} px={1} pb={0.5}>
                            <DefaultTextField
                                name={"count"}
                                label={"تعداد"}
                                defaultValue={LocalStorageUtils.get("count" + printDiscountCode + sId, 50)}
                                variant={"outlined"}
                                type={"number"}
                                required={true}/>
                        </Box>
                        <Box width={0.5} p={1} display={'flex'} flexDirection={'column'}
                             style={{
                                 border: `1px solid ${cyan[300]}`,
                                 ...UtilsStyle.borderRadius(5)
                             }}>
                            <Typography variant={"h6"} pb={2}>
                                انتخاب وضعیت:
                            </Typography>
                            <Box display={'flex'}>
                                <Box display={'flex'} flexDirection={'column'}
                                     px={0.5}
                                     alignItems={'center'} justifyContent={'center'}>
                                    <Typography variant={"body1"} pb={0.5}>
                                        خرید نشده
                                    </Typography>
                                    <Radio
                                        checked={used === 'not_used'}
                                        onChange={handleChange}
                                        value="not_used"
                                        inputProps={{'aria-label': 'not_purchased'}}
                                    />
                                </Box>
                                <Box display={'flex'} flexDirection={'column'}
                                     px={0.5}
                                     alignItems={'center'} justifyContent={'center'}>
                                    <Typography variant={"body1"} pb={0.5}>
                                        خریدشده
                                    </Typography>
                                    <Radio
                                        checked={used === 'used'}
                                        onChange={handleChange}
                                        value="used"
                                        inputProps={{'aria-label': 'purchased'}}
                                    />
                                </Box>
                            </Box>
                        </Box>
                        <Box display={'flex'} justifyContent={showDate ? 'center' : undefined} width={1} pt={2}>
                            <Box px={1}>
                                زمان:
                                <Checkbox
                                    checked={showDate}
                                    onChange={() => {
                                        const newD = !showDate
                                        setShowDate(newD)
                                        LocalStorageUtils.set("showDate" + printDiscountCode + sId, newD ? "true" : "false")
                                    }}
                                    inputProps={{'aria-label': 'primary checkbox'}}
                                />
                            </Box>
                            {
                                showDate &&
                                <DatePickerStartEndPanel
                                    width={2 / 3}
                                    key={printDiscountCode + sId}
                                    onChange={(startTime, endTime) => {
                                        setStartTime(startTime)
                                        setEndTime(endTime)
                                    }}/>
                            }
                        </Box>
                        <Box display={'flex'} justifyContent={'center'} width={1} pt={2}>
                            <BaseButton
                                onClick={handlePrintClick}
                                style={{
                                    width: '30%',
                                    backgroundColor: theme.palette.success.main
                                }}>
                                <Typography variant={"body1"} color={"#fff"}>
                                    پرینت
                                </Typography>
                            </BaseButton>
                        </Box>
                    </FormControl>
                </Box>
            }
            <Backdrop open={loading}
                      style={{
                          zIndex: '9999'
                      }}>
                <CircularProgress color="inherit"
                                  style={{
                                      color: cyan[300],
                                      width: 100,
                                      height: 100
                                  }}/>
            </Backdrop>
        </BaseDialog>
    )
}
