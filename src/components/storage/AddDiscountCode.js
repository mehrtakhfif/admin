import React, {Fragment, useRef, useState} from "react";
import Box from "@material-ui/core/Box";
import BaseDialog from "../base/BaseDialog";
import Typography from "../base/Typography";
import DefaultTextField from "../base/textField/DefaultTextField";
import _ from "lodash"
import Button from "@material-ui/core/Button";
import FormControl from "../base/formController/FormController";
import {gcLog, tryIt} from '../../utils/ObjectUtils';
import SuccessButton from "../base/button/buttonVariant/SuccessButton";
import {useSnackbar} from "notistack";
import {lang} from "../../repository";
import CircularProgress from "@material-ui/core/CircularProgress";
import {cyan} from "@material-ui/core/colors";
import Backdrop from "@material-ui/core/Backdrop";
import ControllerProduct from "../../controller/ControllerProduct";

export default function AddDiscountCode({open, sId:storageId, boxPrefix, onClose,dialogProps, ...props}) {
    const ref = useRef();
    const [loading, setLoading] = useState(false);
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();
    function generateCode() {
        tryIt(() => {
            const error = ref.current.hasError()
            if (error) {
                reqCancel(`فیلد ` + error + " را پرکنید.");
                return
            }
            const {prefix, len, count} = ref.current.serialize()
            if (_.toNumber(len) > 7){
                reqCancel("طول رشته نباید بیشتر از 7 باشد.")
                return;
            }
            setLoading(true)
            ControllerProduct.DiscountCode.save({
                storageId:storageId,
                prefix: (boxPrefix ? (boxPrefix + "-") : "") + prefix,
                count
            }).then(res => {
                onClose(res)
            }).finally(() => {
                setLoading(false)
            })
        })
    }




    function reqCancel(text = null) {
        if (text)
            enqueueSnackbar(text,
                {
                    variant: "error",
                    action: (key) => (
                        <Fragment>
                            <Button onClick={() => {
                                closeSnackbar(key)
                            }}>
                                {lang.get('close')}
                            </Button>
                        </Fragment>
                    )
                });
    }

    return (
        <BaseDialog open={open} header={"افزودن کدتخفیف جدید"} onClose={onClose}>
            {
                (open && (!storageId || (storageId && boxPrefix))) ?
                    <FormControl innerref={ref} p={2} display={'flex'} flexDirection={'column'}>
                        <Box display={'flex'} flexWrap={'wrap'}>
                            <Box p={1} width={1}>
                                <DefaultTextField
                                    name={"prefix"}
                                    variant={'outlined'}
                                    required={true}
                                    label={"پیشوند"}
                                    defaultValue={null}
                                    textFieldProps={{
                                        endAdornment: boxPrefix ? (
                                            <Typography variant={"h6"}>
                                                -{_.upperCase(boxPrefix)}
                                            </Typography>
                                        ) : null
                                    }}/>
                            </Box>
                            <Box p={1}>
                                <DefaultTextField
                                    type={"number"}
                                    name={"count"}
                                    required={true}
                                    variant={'outlined'}
                                    label={"تعداد"}/>
                            </Box>
                            <Box width={1} display={'flex'}>
                                <Box width={0.5}>
                                    <SuccessButton
                                        onClick={generateCode}>
                                        تولید کد
                                    </SuccessButton>
                                </Box>
                            </Box>
                        </Box>
                    </FormControl> :
                    <Box p={2} display={'flex'} flexDirection={'column'} alignItems={'center'}
                         justifyContent={'center'}>
                        <Typography variant={"h6"}>
                            مشکلی در بدست آوردن پیشوند باکس پیش آمده.
                        </Typography>
                        <Typography pt={2} variant={"body2"}>
                            لطفا با پشتیبان تماس حاصل فرمایید.
                        </Typography>
                    </Box>
            }
            <Backdrop open={loading}
                      style={{
                          zIndex: '9999'
                      }}>
                <CircularProgress color="inherit"
                                  style={{
                                      color: cyan[300],
                                      width: 100,
                                      height: 100
                                  }}/>
            </Backdrop>
        </BaseDialog>
    )
}
