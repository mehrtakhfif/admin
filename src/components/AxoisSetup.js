import React, {Fragment, useEffect} from "react";
import {useSnackbar} from "notistack";
import {useDispatch} from "react-redux";
import axios from "axios";
import Button from "@material-ui/core/Button";
import {adminApiRout, DEBUG, lang} from "../repository";
import {finishLoading, startLoading} from "../redux/actions/AxiosAction";
import _ from 'lodash';
import {UtilsEncrypt, UtilsParser, UtilsStyle} from "../utils/Utils";
import CookiesUtils from "../utils/CookiesUtils";
import moment from 'moment'
import {generateServerKey} from "../sv";
import {gcLog, tryIt} from "../utils/ObjectUtils";
import Box from "@material-ui/core/Box";
import IconButton from "@material-ui/core/IconButton";
import {Close} from "@material-ui/icons";
import {cyan, grey, orange, red} from "@material-ui/core/colors";

axios.defaults.baseURL = adminApiRout;
axios.defaults.withCredentials = true;

export default function AxiosSetup(props) {
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();
    const dispatch = useDispatch();

    useEffect(() => {
        //region setup
        axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
        axios.defaults.headers['admin'] = 'true';
        axios.defaults.withCredentials = true;

        // axios.defaults.headers.post['Access-Control-Allow-Headers'] = 'Content-Type, Authorization';
        //endregion
        //region request
        axios.interceptors.request.use(function (config) {
            dispatch(startLoading());

            try {
                switch (_.upperCase(config.method)) {
                    case "POST":
                    case "PUT":
                    case "PATCH": {
                        const cs = CookiesUtils.getServer({key: "csrf_cookie"});
                        if (cs) {
                            const t = moment().utc().format('YYYY-MM-DD-HH-mm');
                            config.headers['X-CSRF-Token'] = UtilsEncrypt.SHA.S224(generateServerKey(cs, t));
                            break;
                        }
                    }
                    default: {

                    }
                }
            } catch (e) {
            }

            if (!(config && config.params))
                return config;
            if (DEBUG && !config.params.delay) {
                // config.params.delay = 1;
            }
            return config;
        }, function (error) {
            dispatch(finishLoading());
            return Promise.reject(error);
        });
        //endregion
        //region response
        axios.interceptors.response.use(function (response) {
            dispatch(finishLoading());
            try {
                let variant = "success";
                let message = undefined;
                let children = undefined
                let duration = 7000
                tryIt(() => {
                    variant = response.data.variant || "success"
                })
                tryIt(() => {
                    message = response.data.message
                })
                tryIt(() => {
                    children = response.data.html
                })
                tryIt(() => {
                    duration = response.data.duration || duration
                })
                if (response.status === 201 ) {
                    message = message || "با موفقیت ذخیره شد.";
                } else if (response.status === 250) {
                    message = message || "محصول غیر فعال شد";
                }

                if (message)
                showSnackBar(message, children, variant, duration)
            } catch (e) {
            }
            return response
        }, function (error) {
            dispatch(finishLoading());
            try {
                let variant = "error";
                let message = undefined;
                let children = undefined;
                let duration = 7000;

                tryIt(() => {
                    variant = error.response.data.variant || variant
                })
                tryIt(() => {
                    message = error.response.data.message
                })
                tryIt(() => {
                    children = error.response.data.html
                })
                tryIt(() => {
                    duration = error.response.data.duration
                })

                tryIt(() => {
                    if (!error.response.status || error.response.status === 500) {
                        message = message || lang.get("er_server_error");
                    }
                    message = error.response.data.type ? `error: ${error.response.data.type} ${error.response.data.field}` : message;
                })

                showSnackBar(message, children, variant, duration)
            } catch (e) {
            }


            return Promise.reject(error);
        });
        //endregion


    }, []);


    function showSnackBar(message, children, variant, duration) {
        if (!(message || children))
            return
        duration = (duration < 7000) ? 7000 : duration;
        enqueueSnackbar(message,
            {
                variant: variant,
                autoHideDuration: duration,
                children: ((key) => {
                    if (!children) {
                        return undefined
                    }
                    setTimeout(() => {
                        closeSnackbar(key)
                    }, duration)
                    return (
                        <Box
                            display={'flex'}
                            p={2}
                            style={{
                                backgroundColor: variant === " success" ? grey[400] : variant === " info" ? cyan[300] : variant === "warning" ? orange[300] : red[300],
                                ...UtilsStyle.borderRadius("10px")
                            }}>
                            <Box width={0.75} style={{color: "#fff"}}>
                                {UtilsParser.html(`<div>${children}</div>`)}
                            </Box>
                            <Box width={0.25}>
                                <IconButton size={"small"} onClick={() => closeSnackbar(key)}>
                                    <Close fontSize={"small"} style={{color: "#fff"}}/>
                                </IconButton>
                            </Box>
                        </Box>
                    )
                })(),
                action: (key) => (
                    <Fragment>
                        <Button onClick={() => {
                            closeSnackbar(key)
                        }}>
                            {lang.get('close')}
                        </Button>
                    </Fragment>
                )
            });
    }


    return (
        <>
            {props.children}
        </>
    )
}
