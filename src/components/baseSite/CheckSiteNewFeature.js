import React, {Fragment, useEffect, useState} from "react";
import storage from "../../storage";
import BaseDialog from "../base/BaseDialog";
import {DEBUG, newFeatures} from "../../repository";
import Box from "@material-ui/core/Box";
import BaseButton from "../base/button/BaseButton";
import {cyan, grey} from "@material-ui/core/colors";
import Typography from "../base/Typography";
import Divider from "@material-ui/core/Divider";
import {Card} from "@material-ui/core";
import _ from "lodash"
import Button from "@material-ui/core/Button";
import {useSnackbar} from "notistack";
import {useSelector} from "react-redux";
import {gcLog} from "../../utils/ObjectUtils";


const funEmoji = [
    '😀', '😄', '😁', '😆', '😇', '🤪', '😊', '😉'
]

export default function CheckSiteNewFeature ({...props}) {
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();
    const {isLogin, settings} = useSelector(state => state.user);
    const fun = (_.isObject(settings) ? settings.fun : false)
    const [newV, setNewV] = useState(!storage.Base.SiteNewFeature.getChecked())
    const [disableClose, setDisableClose] = useState(false)
    const [showDialog, setShowDialog] = useState(false)

    function handleAccepted(force) {
        if (!force && fun === 99) {
            BehsadDialog()
            return
        }
        storage.Base.SiteNewFeature.setChecked()
        setNewV(false)
    }


    function getFunEmoji() {
        try {
            return fun ? funEmoji[Math.floor(Math.random() * funEmoji.length)] : ""
        } catch (e) {
        }
    }

    useEffect(() => {
        try {
            document.getElementById('root').onclick = () => {
                return !showDialog
            }
        } catch (e) {
        }
    }, [showDialog])

    useEffect(() => {
        setShowDialog(Boolean(newV && isLogin));
    }, [newV, isLogin])


    function BehsadDialog() {
        const text = "من برده شما هستم و تشکر میکنم."
        const person = prompt("من بهزاد میرزایی از آرین و میلاد تشکر میکنم. متن پیام:", text);

        if (person == null || person !== text) {
            BehsadDialog()
        } else {
            handleAccepted(true)
            enqueueSnackbar("خوش امدی برده 😎",
                {
                    variant: "success",
                    action: (key) => (
                        <Fragment>
                            <Button onClick={() => {
                                closeSnackbar(key)
                            }}>
                                بستن
                            </Button>
                        </Fragment>
                    )
                });
        }
    }

    return (
        <React.Fragment>
            {props.children}
            <BaseDialog open={showDialog}>
                <Box display={'flex'} flexDirection={'column'} py={2} px={2}>
                    <Typography variant={"h5"} fontWeight={400} pb={2} color={cyan[500]}>
                        فهرست تغییرات پنل ادمین
                    </Typography>
                    <Box p={2} component={Card} display={'flex'} height={400} flexDirection={'column'}
                         style={{overflowY: "scroll"}}>
                        {
                            newFeatures(fun).map(({v, features}, i) => (
                                <Box key={v} display={'flex'} flexDirection={'column'} pb={2}>
                                    <Typography dir={'ltr'} variant={"h6"} fontWeight={500} alignItems={'center'}
                                                px={0.5}
                                                pb={0.5}>
                                        V<Typography pr={0.5} fontWeight={500} variant={"body2"}>
                                        ersion
                                    </Typography> {v}
                                    </Typography>
                                    <Divider/>
                                    <Box component={'ul'} display={'flex'} flexDirection={'column'} pt={1.5}>
                                        {
                                            features.map((t, i) => (
                                                _.isString(t) ?
                                                    <Typography key={i} display={"list-item"} component={"li"}
                                                                variant={"h6"}
                                                                pb={1}>
                                                        {t}
                                                    </Typography> :
                                                    <React.Fragment key={i}/>
                                            ))
                                        }
                                    </Box>
                                </Box>
                            ))
                        }
                    </Box>
                    <Box display={'flex'} pt={3} px={2}>
                        <Box>
                            <BaseButton
                                id={"accept_button_dialog"}
                                variant={"outlined"}
                                onClick={()=>handleAccepted()}
                                style={{
                                    borderColor: cyan[300]
                                }}>
                                باتشکر {fun ? disableClose ? " 😁" : " 😊" : ""}
                            </BaseButton>
                        </Box>
                        <Box px={2}>
                            <BaseButton
                                variant={"outlined"}
                                disabled={disableClose}
                                onClick={() => {
                                    if (settings.fun) {
                                        setDisableClose(true)
                                        enqueueSnackbar("تشکر کن " + getFunEmoji(),
                                            {
                                                variant: "info",
                                                action: (key) => (
                                                    <Fragment>
                                                        <Button onClick={() => {
                                                            closeSnackbar(key)
                                                        }}>
                                                            بستن
                                                        </Button>
                                                    </Fragment>
                                                )
                                            });
                                        return
                                    }
                                    handleAccepted()
                                }}>
                                بستن
                            </BaseButton>
                        </Box>
                    </Box>
                </Box>
            </BaseDialog>
        </React.Fragment>
    )
}

