import React, {createRef, useEffect, useState} from "react";
import {makeStyles} from "@material-ui/styles";
import L from "leaflet";
import icon from "../../drawable/mapImage/marker-icon.png";
import iconShadow from "../../drawable/mapImage/marker-shadow.png";
import Box from "@material-ui/core/Box";
import {Map as LeafletMap, TileLayer} from "react-leaflet";
import TextFieldContainer from "../base/textField/TextFieldContainer";
import TextField from "../base/textField/TextField";
import {lang, theme} from "../../repository";
import {Close, TextFields} from "@material-ui/icons";
import BaseButton from "../base/button/BaseButton";
import IconButton from "@material-ui/core/IconButton";
import {UtilsStyle} from "../../utils/Utils";
import Typography from "../base/Typography";
import Dialog from "@material-ui/core/Dialog";
import {blue, red} from "@material-ui/core/colors";
import Toolbar from "@material-ui/core/Toolbar";
import AppBar from "@material-ui/core/AppBar";
import _ from 'lodash';

const useSelectLocationStyle = makeStyles(theme => ({
    mapContainer: {},
    mapSubmitBtn: {
        paddingTop: theme.spacing(2),
        paddingBottom: theme.spacing(2),
        ...UtilsStyle.borderRadius(5),
    },
    mapSubmitBtnContainer: {
        position: 'absolute',
        zIndex: 999,
        bottom: 0,
        left: 0,
        right: 0,
        width: '100%',
    },
    mapRoot: {
        minHeight: 300,
        height: '100%'
    }
}))
export default function SelectLocation({open, title = lang.get("edit_address"), targetLocation: tl, onSelect, ...props}) {
    const classes = useSelectLocationStyle(props);
    const [state, setState] = useState({
        zoom: 14,
        textField: true
    });
    const [lng, setLng] = useState()
    const [lat, setLat] = useState()
    const [tfLo, setTfLo] = useState(tl);
    const ref = createRef();


    const [targetLocation, setTargetLocation] = useState(tl);

    useEffect(() => {
        try {
            ref.current.leafletElement.flyTo([lat, lng], state.zoom)
        } catch (e) {
        }
    }, [lat, lng])

    useEffect(() => {
        if (state.textField) {
            setTfLo({
                lng: lng,
                lat: lat
            })
        }
    }, [state.textField])

    //region Function
    function onZoomEnd(e) {
        setState({
            ...state,
            zoom: e.target._zoom
        })
    }

    function getLocationNumber(val) {
        try {
            return _.toString(val).substr(0, 8)
        } catch (e) {
            return ""
        }
    }

    //endregion Function

    //region Marker
    L.Marker.prototype.options.icon = L.icon({
        iconUrl: icon,
    });
    L.Control.prototype.options.icon = L.icon({
        iconUrl: icon,
        shadowUrl: iconShadow,
    });
    //endregion Marker

    return (
        <Dialog
            open={open}
            fullScreen
            aria-labelledby="address_edit_alert-dialog-title"
            aria-describedby="address_edit_alert-dialog-description"
            onClose={() => {
                onSelect()
            }}>
            <AppBar style={{position: 'relative'}}>
                <Toolbar
                    style={{
                        display: 'flex',
                        flexDirection: 'row-reverse',
                    }}>
                    <IconButton edge="start" color="inherit"
                                onClick={() => onSelect()}
                                aria-label="close">
                        <Close/>
                    </IconButton>
                    <Typography variant="h6" ml={2} color={"#fff"}>
                        {title}
                    </Typography>
                </Toolbar>
            </AppBar>
            <Box display='flex'
                 boxShadow={3}
                 className={classes.mapContainer}
                 flexDirection='column'
                 width={1}
                 height={1}>
                <Box width="100%"
                     className={classes.mapRoot}
                     style={{
                         position: 'relative',
                         ...props.style
                     }}>
                    <Box
                        display={'flex'}
                        p={2}
                        style={{
                            position: 'absolute',
                            background: '#ffffffcc',
                            zIndex: 999,
                            top: 5,
                            right: 5,
                            ...UtilsStyle.borderRadius(5)
                        }}>
                        {state.textField ?
                            <Box
                                display={'flex'}
                                alignItems={'center'}
                                width={450}>
                                <IconButton
                                    onClick={() => setState({
                                        ...state,
                                        textField: false
                                    })}
                                    style={{
                                        marginRight: theme.spacing(1)
                                    }}>
                                    <Close/>
                                </IconButton>
                                <Box px={1} width={0.5}>
                                    <TextFieldContainer
                                        name={"lng"}
                                        defaultValue={tfLo.lng}
                                        dir={"ltr"}
                                        type={"number"}
                                        onChange={(val) => {
                                            // setLng(val)
                                            if (!val)
                                                return
                                            setLng(_.toNumber(val))

                                            // setTargetLocation({
                                            //     ...targetLocation,
                                            //     lng:_.toNumber(val)
                                            // })
                                        }}
                                        render={(ref, {name: inputName, initialize, valid, errorIndex, setValue, inputProps, style, props}) => {
                                            return (
                                                <TextField
                                                    {...props}
                                                    error={!valid}
                                                    name={inputName}
                                                    inputRef={ref}
                                                    fullWidth
                                                    defaultValue={tfLo.lng}
                                                    label={'lng'}
                                                    style={{
                                                        ...style,
                                                        minWidth: '90%'
                                                    }}
                                                    inputProps={{
                                                        ...inputProps,
                                                    }}/>
                                            )
                                        }}/>
                                </Box>
                                <Box width={0.5} px={1}>
                                    <TextFieldContainer
                                        name={"lat"}
                                        defaultValue={tfLo.lat}
                                        dir={"ltr"}
                                        type={"number"}
                                        onChange={(val) => {
                                            if (!val)
                                                return
                                            setLat(_.toNumber(val))
                                        }}
                                        render={(ref, {name: inputName, initialize, valid, errorIndex, setValue, inputProps, style, props}) => {
                                            return (
                                                <TextField
                                                    {...props}
                                                    error={!valid}
                                                    name={inputName}
                                                    inputRef={ref}
                                                    fullWidth
                                                    defaultValue={tfLo.lat}
                                                    label={'lat'}
                                                    style={{
                                                        ...style,
                                                        minWidth: '90%'
                                                    }}
                                                    inputProps={{
                                                        ...inputProps,
                                                    }}/>
                                            )
                                        }}/>
                                </Box>
                            </Box> :
                            <IconButton
                                onClick={() => {
                                    setState({
                                        ...state,
                                        textField: true
                                    })
                                }}>
                                <TextFields/>
                            </IconButton>}
                        <Box display={'flex'} flexDirection={'column'} alignItems={"flex-end"} pl={1}>
                            <Typography variant={"body2"} py={0.5}>
                                lat: {getLocationNumber(targetLocation.lat)}
                            </Typography>
                            <Typography variant={"body2"} py={0.5}>
                                lng: {getLocationNumber(targetLocation.lng)}
                            </Typography>
                        </Box>
                    </Box>
                    <LeafletMap
                        ref={ref}
                        animate={true}
                        className={classes.root}
                        center={targetLocation}
                        doubleClickZoom={true}
                        boxZoom={true}
                        zoomControl={true}
                        minZoom={7}
                        maxZoom={19}
                        onMoveEnd={(e) => {
                            setTargetLocation(e.target.getCenter())
                            // onMoveEnd(e.target.getCenter())
                        }}
                        onZoomend={onZoomEnd}
                        length={4}
                        zoom={state.zoom}
                        style={{
                            height: '100%',
                        }}>
                        <TileLayer
                            attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"/>
                    </LeafletMap>
                    <img src={icon}
                         alt='Map Marker'
                         style={{
                             position: 'absolute',
                             top: '50%',
                             zIndex: '9999',
                             left: '50%',
                             width: 25,
                             marginLeft: -12.5,
                             marginTop: -41
                         }}/>
                </Box>
                <Box className={classes.mapSubmitBtnContainer} display='flex'>
                    <Box width="70%">
                        <BaseButton
                            disabled={false}
                            size='large'
                            className={classes.mapSubmitBtn}
                            onClick={() => onSelect(targetLocation)}
                            style={{
                                backgroundColor: blue[500],
                                color: "#fff",
                                width: '100%',
                                borderRadius: 0
                            }}>
                            {lang.get('submit_location')}
                        </BaseButton>
                    </Box>
                    <Box width='30%'>
                        <BaseButton
                            disabled={false}
                            variant={"text"}
                            size='large'
                            className={classes.mapSubmitBtn}
                            onClick={() => {
                                onSelect()
                            }}
                            style={{
                                backgroundColor: red[500],
                                color: "#fff",
                                width: '100%',
                                borderRadius: 0
                            }}>
                            {lang.get('return')}
                        </BaseButton>
                    </Box>
                </Box>
            </Box>
        </Dialog>
    )
}
