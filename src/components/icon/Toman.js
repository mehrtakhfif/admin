import React from "react";
import Icon from "./Icon";
import {convertIconUrl} from "../../controller/converter";
import {theme} from "../../repository";


export default function ({color, ...props}) {

    return (
        <Icon
            {...props}
            width={18}
            alt={"toman"}
            color={color}
            src={convertIconUrl("toman.svg")}
            style={{
                marginRight: theme.spacing(0.3)
            }}/>
    )
}
