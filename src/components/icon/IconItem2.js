import React, {useEffect, useState} from "react";
import ButtonBase from "@material-ui/core/ButtonBase";
import {UtilsStyle} from "../../utils/Utils";
import NoneTextField from "../base/textField/NoneTextField";
import Tooltip from "../base/Tooltip";
import {convertIconUrl} from "../../controller/converter";
import IconSelector from "./IconSelector";
import {useTheme} from "@material-ui/core";
import Icon from "./Icon";


export default function ({name, width = 60, height = 60, type = "all", iconName: ic, title, onSelect, ...props}) {
    const theme = useTheme();
    const [iconName, setIconName] = useState(ic);
    const [open, setOpen] = useState(false);
    const [state, setState] = useState({
        tooltipDisable: false
    });

    useEffect(() => {
        setIconName(ic)
    }, [ic]);

    const el = (
        <ButtonBase
            onClick={(e) => {
                try {
                    e.stopPropagation();
                    setOpen(true)
                } catch (e) {

                }
            }}
            style={{
                padding: theme.spacing(1),
                border: `1px solid ${theme.palette.text.secondary}`,
                ...UtilsStyle.borderRadius(5)
            }}>
            <Icon
                width={width}
                height={height}
                minHeight={height}
                src={convertIconUrl(iconName)}
                alt={iconName}
                showSkeleton={false}
                {...props}
                style={{
                    ...props.style,
                }}
            />
        </ButtonBase>);
    return (
        <React.Fragment>
            {name &&
            <NoneTextField
                name={name}
                defaultValue={iconName}/>
            }
            {title ?
                <Tooltip title={title} disable={state.tooltipDisable}>
                    <span>
                        {el}
                    </span>
                </Tooltip>
                : el}
            <IconSelector
                open={open}
                type={type}
                onSelect={(item) => {
                    setOpen(false);
                    setState({
                        ...state,
                        tooltipDisable: true
                    });
                    if (onSelect)
                        onSelect(item);
                    setTimeout(() => {
                        setState({
                            ...state,
                            tooltipDisable: false
                        });
                    }, 1500);
                    if (item)
                        setIconName(item.name)
                }}/>
        </React.Fragment>
    )
}
