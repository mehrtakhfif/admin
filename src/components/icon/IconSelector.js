import React, {useEffect, useState} from "react";
import {Dialog, useTheme} from "@material-ui/core";
import ControllerSite from "../../controller/ControllerSite";
import BaseButton from "../base/button/BaseButton";
import Transition from "../base/Transition";
import Box from "@material-ui/core/Box";
import Typography from "../base/Typography";
import IconButton from "@material-ui/core/IconButton";
import {Close} from "@material-ui/icons";
import Tooltip from "../base/Tooltip";
import PleaseWait from "../base/loading/PleaseWait";
import ButtonBase from "@material-ui/core/ButtonBase";
import {UtilsStyle} from "../../utils/Utils";
import _ from 'lodash';
import PropTypes from "prop-types";
import useSWR, {mutate} from "swr";
import ComponentError from "../base/ComponentError";
import Icon from "@material-ui/core/Icon";
import {ColorSelectorInput} from "../base/ColorSelector";
import MtIcon from "../MtIcon";
import {gcLog} from "../../utils/ObjectUtils";

function IconSelector({open, type: ty = "all", onSelect, ...props}) {
    const [type, setType] = useState(ty);
    const [color, setColor] = useState()
    const theme = useTheme();
    const {data, error} = useSWR('font-icons-' + type, () => {
        return ControllerSite.Icon.get({type: type}).then(res => {
            return res.data
        })
    });

    useEffect(() => {
        if (type === ty)
            return;
        setType(type);
    }, [ty]);


    function getStyle(t) {
        return {
            backgroundColor: type === t ? theme.palette.primary.main : undefined,
            borderColor: type === t ? undefined : theme.palette.primary.main,
            marginRight: theme.spacing(1),
            marginLeft: theme.spacing(1),
            color: type === t ? '#fff' : theme.palette.primary.main

        }
    }

    return (
        <Dialog fullScreen={true} open={open} TransitionComponent={Transition}>
            <Box my={2} display={'flex'} flexDirection={'column'}>
                <Box display={'flex'} alignItems={'center'} px={2}
                     style={{
                         position: 'sticky',
                         top: 0
                     }}>
                    <Tooltip title={"بستن"}>
                        <IconButton color="inherit" aria-label="close" onClick={() => onSelect()}>
                            <Close/>
                        </IconButton>
                    </Tooltip>
                    <ColorSelectorInput
                        mx={1}
                        color={color}
                        onSelect={(color) => {
                            setColor(color)
                        }}/>
                    <Typography variant={'h6'} mr={2}>
                        انتخاب آیکون
                    </Typography>
                    <Box mr={2} flexWrap={'wrap'}>
                        <BaseButton
                            onClick={() => setType("all")}
                            variant={type !== "all" ? "outlined" : undefined}
                            style={getStyle("all")}>
                            همه
                        </BaseButton>
                        <BaseButton
                            onClick={() => setType("feature")}
                            variant={type !== "feature" ? "outlined" : undefined}
                            style={getStyle("feature")}>
                            فیچر
                        </BaseButton>
                        <BaseButton
                            onClick={() => setType("boom-gardi")}
                            variant={type !== "boom-gardi" ? "outlined" : undefined}
                            style={getStyle("boom-gardi")}>
                            بومگردی
                        </BaseButton>
                    </Box>
                </Box>
                <Box display={'flex'} px={2} flexWrap={"wrap"} justifyContent={'center'}>
                    {!error ?
                        data ?
                            data.map(ic => {
                                const ln = ic.types.length
                                const multi = ln > 1
                                return (
                                    <Box key={ic.name} display={'flex'} flexWrap={'wrap'}
                                         minWidth={ln / 12}
                                         p={multi ? 1 : 0}>
                                        <Box
                                            width={1}
                                            display={'flex'} flexWrap={'wrap'}
                                            style={{
                                                border: multi ? `1px solid ${theme.palette.secondary.main}` : undefined,
                                                ...UtilsStyle.borderRadius(5),
                                            }}>
                                            {
                                                ic.types.map(type => (
                                                    <Box key={`${ic.name}-${type}`}
                                                         width={1 / ic.types.length}
                                                         display={'flex'}
                                                         alignItems={'center'}
                                                         justifyContent={'center'}
                                                         py={3} px={1}>
                                                        <ButtonBase
                                                            onClick={() => {
                                                                onSelect({
                                                                    ...ic,
                                                                    type,
                                                                    color
                                                                }, type)
                                                            }}
                                                            style={{
                                                                margin: theme.spacing(1),
                                                                border: `1px solid ${theme.palette.text.secondary}`,
                                                                ...UtilsStyle.borderRadius(5),
                                                            }}>
                                                            <Box style={{
                                                                alignItems: 'center',
                                                                justifyContent: 'center',
                                                                display: 'flex',
                                                                padding: '12px',
                                                            }}>
                                                                <MtIcon
                                                                    fontSize={"large"}
                                                                    // lassName={`${ic.name}`}
                                                                    icon={`${ic.name}`}
                                                                    style={{
                                                                        color: color
                                                                    }}/>
                                                            </Box>
                                                        </ButtonBase>
                                                    </Box>
                                                ))
                                            }
                                        </Box>
                                    </Box>
                                )
                            }) :
                            <PleaseWait/> :
                        <ComponentError
                            statusCode={(_.isObject(error) && error.request) ? error.request.status : undefined}
                            tryAgainFun={() => mutate(type)}/>
                    }
                </Box>
            </Box>
        </Dialog>
    )
}

IconSelector.prototype = {
    type: PropTypes.oneOf(["all", "feature", "boom-gardi"]),
};
export default IconSelector;

