import React, {useEffect, useState} from "react";
import ButtonBase from "@material-ui/core/ButtonBase";
import {UtilsStyle} from "../../utils/Utils";
import Tooltip from "../base/Tooltip";
import IconSelector from "./IconSelector";
import {useTheme} from "@material-ui/core";
import _ from "lodash"
import Icon from "@material-ui/core/Icon";


export default function ({width = 60, height = 60, type = "all", icon: ic, onSelect, ...props}) {
    const theme = useTheme();
    const [icon, setIcon] = useState(ic);
    const [open, setOpen] = useState(false);
    const [state, setState] = useState({
        tooltipDisable: false
    });

    useEffect(() => {
        setIcon(ic)
    }, [ic]);


    const elIcon = (
        <Icon
            width={width}
            height={height}
            {...props}
            className={icon ? `${icon.type} ${icon.name}` : "fal fa-square-full"}
            style={{
                color: icon ? icon.color : "#000",
                width: width,
                height: height,
                ...props.style,
            }}/>
    )

    const el = (
        _.isFunction(onSelect) ?
            <ButtonBase
                onClick={(e) => {
                    try {
                        e.stopPropagation();
                        setOpen(true)
                    } catch (e) {
                    }
                }}
                style={{
                    padding: theme.spacing(1),
                    border: `1px solid ${theme.palette.text.secondary}`,
                    ...UtilsStyle.borderRadius(5)
                }}>
                {elIcon}
            </ButtonBase> :
            elIcon);
    return (
        <React.Fragment>
            {(icon && icon.title) ?
                <Tooltip title={icon.title} disable={state.tooltipDisable}>
                    <span>
                        {el}
                    </span>
                </Tooltip>
                : el}
            <IconSelector
                open={open}
                type={type}
                onSelect={(item) => {
                    setOpen(false);
                    setState({
                        ...state,
                        tooltipDisable: true
                    });
                    if (onSelect)
                        onSelect(item);
                    setTimeout(() => {
                        setState({
                            ...state,
                            tooltipDisable: false
                        });
                    }, 1500);
                    if (item)
                        setIcon(item)
                }}/>
        </React.Fragment>
    )
}
