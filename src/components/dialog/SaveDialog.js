import React from "react";
import useTheme from "@material-ui/core/styles/useTheme";
import Box from "@material-ui/core/Box";
import {green} from "@material-ui/core/colors";
import DialogActions from "@material-ui/core/DialogActions";
import BaseDialog from "../base/BaseDialog";
import ButtonText from "../base/button/ButtonText";
import {isElement} from "../../utils/ObjectUtils";

export default function SaveDialog({
                                       open,
                                       text = "ذخیره محصول",
                                       submitText = "ذخیره",
                                       cancelText = "لغو",
                                       onSubmit,
                                       onCancel,
                                       closable = true
                                   }) {
    const theme = useTheme();
    const isDarkMode = theme.palette.type === "dark";

    return (
        <BaseDialog
            open={open}
            onClose={() => {
                if (closable || onCancel)
                    onCancel(false)
            }}
            aria-labelledby="save-dialog-slide-title"
            aria-describedby="save-dialog-slide-description">
            <Box dipslya={'flex'} flexDirection={'column'} py={2} px={4}>
                {text}
                <DialogActions>
                    {
                        isElement(submitText) ?
                            submitText :
                            <ButtonText
                                variant={"body1"}
                                color={isDarkMode ? green[200] : green[700]}
                                onClick={onSubmit}>
                                {submitText}
                            </ButtonText>
                    }
                    <ButtonText
                        variant={'body2'}
                        onClick={onCancel}
                        style={{
                            marginLeft: theme.spacing(0.5),
                            marginRight: theme.spacing(0.5),
                        }}>
                        {cancelText}
                    </ButtonText>
                </DialogActions>
            </Box>
        </BaseDialog>
    );
}
