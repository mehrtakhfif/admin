import React from "react";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import {Close} from "@material-ui/icons";
import AppBar from "@material-ui/core/AppBar";


export default function ({onClose}) {
    return (
        <AppBar style={{position: 'sticky',top:0}}>
            <Toolbar
                style={{
                    display: 'flex',
                    flexDirection: 'row-reverse',
                }}>
                <IconButton edge="start" color="inherit"
                            onClick={() => onClose('location', {mapOpen: false})}
                            aria-label="close">
                    <Close/>
                </IconButton>
            </Toolbar>
        </AppBar>
    )
}
