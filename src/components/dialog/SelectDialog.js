import React, {useEffect} from "react";
import useTheme from "@material-ui/core/styles/useTheme";
import Box from "@material-ui/core/Box";
import {green} from "@material-ui/core/colors";
import DialogActions from "@material-ui/core/DialogActions";
import BaseDialog from "../base/BaseDialog";
import ButtonText from "../base/button/ButtonText";
import _ from "lodash"
import {Item} from "../../pages/box/BoxDashboard";
import Typography from "../base/Typography";


export function createItem({label, rout, backgroundColor, ...props}) {
    return {label, rout, backgroundColor, ...props}
}

export default function SelectDialog({
                                         open,
                                         text = "یک گزینه را انتخاب کنید.",
                                         items = [],
                                         closeText = "بستن",
                                         closable = true,
                                         onClose,
                                     }) {
    const theme = useTheme();
    useEffect(() => {
        try {
            if (_.isEmpty(items))
                throw "";
            if (items.length === 1)
                onClose(items[0]);
        } catch (e) {
            onClose()
        }
    }, [])

    return (
            <Box dipslya={'flex'} flexDirection={'column'} py={2} px={4}>
                <Typography variant={"h6"}>
                    {text}
                </Typography>
                <Box display={'flex'} flexWrap={'wrap'}>
                    {items.map((it, i) => (
                        <Item key={i} label={it.label} rout={it.rout}
                              backgroundColor={it.backgroundColor}
                              onClick={() => {
                                  onClose(it)
                              }}/>
                    ))}
                </Box>
                <DialogActions>
                    <ButtonText
                        variant={'body2'}
                        onClick={() => onClose()}
                        style={{
                            marginLeft: theme.spacing(0.5),
                            marginRight: theme.spacing(0.5),
                        }}>
                        {closeText}
                    </ButtonText>
                </DialogActions>
            </Box>
    );
}
