import React, {useEffect} from "react";
import Box from "@material-ui/core/Box";
import {InfoOutlined} from "@material-ui/icons";
import Typography from "../base/Typography";
import {useTheme} from "@material-ui/core";
import {green} from "@material-ui/core/colors";
import DialogActions from "@material-ui/core/DialogActions";
import ButtonText from "../base/button/ButtonText";
import BaseDialog from "../base/BaseDialog";

export default React.memo(function CacheFindDialog({
                                                       open,
                                                       text = (
                                                           <Box display={'flex'} alignItems={'center'}>
                                                               <InfoOutlined style={{marginLeft: 8}}/>
                                                               اطلاعاتی از پیش ذخیره شده برای این بخش یافت شد.
                                                               <br/>
                                                               آیا تمایل دارید که اطلاعات بازیابی شود؟
                                                           </Box>),
                                                       submitText = "بله بازیابی شود",
                                                       cancelText = "خیر",
                                                       onSubmit,
                                                       onCancel
                                                   }) {

    const theme = useTheme();
    const isDarkMode = theme.palette.type === "dark";

    useEffect(() => {
        if (open)
            onCancel()
    }, [open])

    return (
        <BaseDialog open={open}
                    onClose={onCancel}>
            <Box dipslya={'flex'} flexDirection={'column'} py={2} px={4}>
                <Typography variant={'body1'} pb={2} color={theme.palette.secondary.dark}>
                    {text}
                </Typography>
                <DialogActions>
                    <ButtonText variant={'body1'} px={2} color={isDarkMode ? green[200] : green[700]} onClick={onSubmit}
                                buttonProps={{style: {marginLeft: theme.spacing(0.5)}}}>
                        {submitText}
                    </ButtonText>
                    <ButtonText variant={'body2'} onClick={onCancel}>
                        {cancelText}
                    </ButtonText>
                </DialogActions>
            </Box>
        </BaseDialog>
    )
})

