import React from 'react';
import {Box} from '@material-ui/core'

function RouterLoading(props) {
    const style = React.useMemo(()=>({
        position: "absolute",
        top: "50%",
        left: "50%",
        transform: "translate(-50%, -50%)"
    }))
    return (
        <Box
            style={style}>
            <div className="lds-dual-ring"/>
        </Box>
    )
}

export default RouterLoading;