import React, {Fragment, useEffect, useRef, useState} from "react";
import Box from "@material-ui/core/Box";
import Typography from "../../components/base/Typography";
import ControllerUser from "../../controller/ControllerUser";
import ShabaTextField from "../../components/base/textField/ShabaTextField";
import SuccessButton from "../../components/base/button/buttonVariant/SuccessButton";
import BaseButton from "../../components/base/button/BaseButton";
import {useSnackbar} from "notistack";
import Button from "@material-ui/core/Button";
import {lang, theme} from "../../repository";
import rout from "../../router";
import Dialog from "@material-ui/core/Dialog";
import _ from 'lodash'
import FormControl from "../../components/base/formController/FormController";
import PhoneTextField from "../../components/base/textField/PhoneTextField";
import TextFieldContainer, {errorList} from "../../components/base/textField/TextFieldContainer";
import TextField from "../../components/base/textField/TextField";
import SaveDialog from "../../components/dialog/SaveDialog";
import {orange} from "@material-ui/core/colors";
import {Redirect} from "react-router-dom";
import MultiTextField from "../../components/base/textField/MultiTextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Switch from "@material-ui/core/Switch";
import {useSelector} from "react-redux";

export default function SupplierSingle({open, userId, onClose, ...props}) {
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();
    const {roll} = useSelector(state => state.user);
    const isSuperuser = (roll === "superuser");

    const ref = useRef();
    let uId = undefined;
    try {
        uId = userId ? userId : props.match.params.userId;
    } catch (e) {
    }

    const [state, setState] = useState({
        data: !uId,
        loading: false,
        saveDialog: false,
        closeDialog: false,
        redirect: false
    });
    const [data, setData] = useState(getInitialData());

    function getInitialData() {
        return {
            username: "",
            firstName: "",
            lastName: "",
            shaba: "",
            settings: {}
        }
    }

    useEffect(() => {
        if (!uId) {
            setData(getInitialData());
            return;
        }
        ControllerUser.Supplier.getOne({userId: uId}).then((res => {
            const d = res.data;
            setData({
                ...data,
                username: d.username,
                firstName: d.firstName,
                lastName: d.lastName,
                shaba: d.shaba,
                settings: d.settings
            });
        }))
    }, []);

    useEffect(() => {
        if (!uId || !data.username)
            return;
        setState({...state, data: true});
    }, [data]);


    function handleSave() {
        const props = getRequestProps();
        if (!props)
            return;
        setState({
            ...state,
            loading: true
        });

        ControllerUser.Supplier.set(props).then(res => {
            if (onClose) {
                setState({
                    ...state,
                    loading: false
                });
                onClose(res.data);
                return
            }
            setState({
                ...state,
                loading: false,
                redirect: true
            })
        }).catch(e => {
            setState({
                ...state,
                loading: false
            });
        })
    }


    function getRequestProps() {
        let errorText = "";
        if (ref.current.hasError()) {
            errorText = "فرم مشکل دارد";
            // try {
            //     const el = ref.current.getErrorElement();
            //     const t = el.getAttribute(notValidErrorTextField);
            //     errorText = t ? t : errorText;
            //     el.focus();
            // } catch (e) {
            // }
            reqCancel(errorText);
            return;
        }
        const {username, firstName, lastName, shaba, details} = ref.current.serialize();


        return {
            userId: uId,
            username: username,
            firstName: firstName,
            lastName: lastName,
            shaba: shaba,
            details: details
        }
    }

    function reqCancel(text = null) {
        if (text)
            enqueueSnackbar(text,
                {
                    variant: "error",
                    action: (key) => (
                        <Fragment>
                            <Button onClick={() => {
                                closeSnackbar(key)
                            }}>
                                {lang.get('close')}
                            </Button>
                        </Fragment>
                    )
                });

        setState({
            ...state,
            loading: false
        })
    }


    const el = (
        state.data &&
        <Box display={'flex'} flexDirection={'column'} p={2}>
            <Typography variant={'h6'} fontWeight={600} pb={2}>
                تامین کننده:
            </Typography>
            <FormControl innerref={ref} display={'flex'} flexWrap={'wrap'}>
                <Box p={2} minWidth={300}>
                    <TextFieldContainer
                        name={"firstName"}
                        render={(ref, {name: inputName, initialize, valid, errorIndex, setValue, inputProps, style, props}) => {
                            return (
                                <TextField
                                    {...props}
                                    error={!valid}
                                    variant="outlined"
                                    name={inputName}
                                    defaultValue={data.firstName}
                                    fullWidth
                                    helperText={errorList[errorIndex]}
                                    inputRef={ref}
                                    required
                                    label={"نام"}
                                    style={style}
                                    inputProps={{
                                        ...inputProps
                                    }}/>
                            )
                        }}/>
                </Box>
                <Box p={2} minWidth={300}>
                    <TextFieldContainer
                        name={"lastName"}
                        render={(ref, {name: inputName, initialize, valid, errorIndex, setValue, inputProps, style, props}) => {
                            return (
                                <TextField
                                    {...props}
                                    error={!valid}
                                    variant="outlined"
                                    name={inputName}
                                    defaultValue={data.lastName}
                                    fullWidth
                                    helperText={errorList[errorIndex]}
                                    inputRef={ref}
                                    required
                                    label={"نام خانوادگی "}
                                    style={style}
                                    inputProps={{
                                        ...inputProps
                                    }}/>
                            )
                        }}/>
                </Box>

                <Box p={2} minWidth={300}>
                    <PhoneTextField
                        required
                        defaultValue={data.username}
                        name={"username"}/>
                </Box>
                <Box p={2} minWidth={350}>
                    <ShabaTextField name={"shaba"} defaultValue={data.shaba} required/>
                </Box>

                <Box p={2} minWidth={600}>
                    <MultiTextField
                        label={"توضیحات اضافه"}
                        name={"details"}
                        rows={5}
                        defaultValue={data.settings.supplier_details || ""}/>
                </Box>
            </FormControl>
            <Box display={'flex'} pt={2}>
                <SuccessButton variant={"outlined"}
                               loading={state.loading}
                               onClick={() => {
                                   if (!getRequestProps())
                                       return;
                                   setState({...state, saveDialog: true})
                               }}>
                    ذخیره
                </SuccessButton>
                {onClose &&
                <BaseButton variant={"text"}
                            loading={state.loading}
                            onClick={() => {
                                setState({...state, closeDialog: true})
                            }} style={{marginRight: theme.spacing(1)}}>
                    بستن
                </BaseButton>
                }
            </Box>
            <SaveDialog open={state.saveDialog}
                        text={(
                            <Box display={'flex'} flexDirection={'column'} px={3}>
                                <Typography variant={'h6'} fontWeight={400} pb={2}>
                                    ذخیره تامین‌کننده
                                </Typography>
                                <Typography variant={'body1'}>
                                    آیا از ذخیره این تامین‌کننده اطمینان دارید؟
                                </Typography>
                                <Typography variant={'caption'} color={theme.palette.text.secondary} pt={0.5}>
                                <span style={{color: orange[300], fontWeight: 600, paddingLeft: theme.spacing(0.3)}}>
                                    توجه:
                                </span>
                                    در صورت ذخیره، اطلاعات تغییریافته قابل بازگشت نمیباشد.
                                </Typography>
                            </Box>)}
                        onSubmit={() => {
                            setState({...state, saveDialog: false});
                            handleSave();
                        }}
                        onCancel={() => setState({...state, saveDialog: false})}/>
            <SaveDialog open={state.closeDialog}
                        submitText={"بله"}
                        cancelText={"خیر"}
                        text={<Typography variant={"body1"}>
                            آیا از لغو تغییرات اطمینان دارید؟
                        </Typography>}
                        onSubmit={() => {
                            setState({...state, closeDialog: false});
                            onClose()
                        }}
                        onCancel={() => {
                            setState({...state, closeDialog: false})
                        }}/>
        </Box>
    );

    return (
        <React.Fragment>
            {(onClose && _.isBoolean(open)) ?
                <Dialog open={open} onClose={onClose}>
                    {el}
                </Dialog> :
                state.data &&
                <React.Fragment>
                    {state.redirect ?
                        <Redirect
                            to={{
                                pathname: rout.Supplier.rout
                            }}/> : <React.Fragment>
                            {el}
                        </React.Fragment>}
                </React.Fragment>}

        </React.Fragment>
    )
}
