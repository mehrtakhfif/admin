import React, {useState} from "react";
import ControllerUser from "../../controller/ControllerUser";
import useSWR, {mutate} from "swr";
import Box from "@material-ui/core/Box";
import ButtonBase from "@material-ui/core/ButtonBase";
import {Card} from "@material-ui/core";
import Dialog from "@material-ui/core/Dialog/Dialog";
import BaseButton from "../../components/base/button/BaseButton";
import Skeleton from "@material-ui/lab/Skeleton";
import ComponentError from "../../components/base/ComponentError";
import Img from "../../components/base/img/Img";
import userAvatar from "../../drawable/svg/userAvatar.svg";
import {UtilsStyle} from "../../utils/Utils";
import {Person, Phone} from "@material-ui/icons";
import {cyan} from "@material-ui/core/colors";
import {theme} from "../../repository";
import Typography from "../../components/base/Typography";
import SuccessButton from "../../components/base/button/buttonVariant/SuccessButton";
import SupplierSingle from "./SupplierSingle";
import TextFieldContainer, {errorList} from "../../components/base/textField/TextFieldContainer";
import TextField from "../../components/base/textField/TextField";
import _ from "lodash";

export default function ({open, onClose, ...props}) {

    return (
        <Dialog open={open} maxWidth={"lg"} onClose={() => onClose()}>
            {open &&
            <Supplier onClose={onClose}/>
            }
            <React.Fragment/>
        </Dialog>
    )
}

function Supplier({onClose, ...props}) {
    const [value, setValue] = useState("");
    const [state, setState] = useState({
        add: false,
    });

    const d = ControllerUser.Supplier.search({query: value});
    const {data, error} = useSWR(d[0] + value, () => {
        return d[1]()
    });


    const cardWidth = 300;
    return (
        <React.Fragment>
            <Box display={'flex'} flexDirection={'column'} width={(cardWidth * 2) + 16} px={1} py={3}>
                <Box px={2} display={'flex'} width={1} alignItems={'center'}>
                    <Box flex={1} pr={2}>
                        <TextFieldContainer
                            name={'selectSupplier'}
                            active={false}
                            onChangeDelay={1000}
                            onChange={(value) => {
                                setValue(value)
                            }}
                            render={(ref, {name, initialize, valid, errorIndex, setValue, inputProps, style, props}) => {
                                return (
                                    <TextField
                                        {...props}
                                        error={!valid}
                                        name={name}
                                        variant="outlined"
                                        fullWidth
                                        requestFocus={true}
                                        inputRef={ref}
                                        helperText={errorList[errorIndex]}
                                        defaultValue={""}
                                        label={"نام یا شماره موبایل تامین کننده"}
                                        style={style}
                                        inputProps={{
                                            ...inputProps
                                        }}/>
                                )
                            }}/>
                    </Box>
                    <SuccessButton variant={"outlined"}
                                   onClick={() => setState({
                                       ...state,
                                       add: true
                                   })}
                                   style={{
                                       marginRight: theme.spacing(0.5)
                                   }}>
                        افزودن
                    </SuccessButton>
                    <BaseButton variant={"text"} onClick={() => onClose()}
                                style={{
                                    marginRight: theme.spacing(1)
                                }}>
                        بستن
                    </BaseButton>
                </Box>
                {!error ? data ?
                    <Box display={'flex'} flexWrap={'wrap'} mt={2}>
                        {data.data.suppliers.map(su => (
                            <Box minWidth={cardWidth} p={1} key={su.id}>
                                <ButtonBase
                                    onClick={() => {
                                        onClose(su)
                                    }}
                                    style={{width: '100%'}}>
                                    <Box component={Card} width={1} p={1}>
                                        <UserItem justifyContent={'unset'} user={su}/>
                                    </Box>
                                </ButtonBase>
                            </Box>
                        ))}
                    </Box> :
                    <Box display={'flex'} flexWrap={'wrap'}>
                        <Box p={1} width={cardWidth}>
                            <Skeleton height={150}/>
                        </Box>
                        <Box p={1} width={cardWidth}>
                            <Skeleton height={150}/>
                        </Box>
                    </Box> :
                    <ComponentError
                        statusCode={(_.isObject(error) && error.request) ? error.request.status : undefined}
                        tryAgainFun={() => mutate(d[0])}/>}
            </Box>
            <SupplierSingle open={state.add}

                            onClose={(su) => {
                                setState({...state, add: false});
                                if (su)
                                    onClose(su)
                            }}/>
        </React.Fragment>
    )
}

export function UserItem({showTitle = false, user, ...props}) {
    return (
        <Box display={'flex'} flexWrap={'wrap'} alignItems={'center'}
             justifyContent={'center'} {...props}>
            <Img src={(user && user.avatar) ? user.avatar : userAvatar}
                 style={{
                     width: 90,
                     height: 'auto',
                     ...UtilsStyle.borderRadius('100%')
                 }}/>
            <Box display={'flex'} flexDirection={'column'} mr={2}>
                {showTitle &&
                <Typography variant={"h6"} my={1}>
                    {_.isString(showTitle) ? showTitle : "تامین کننده:"}
                </Typography>}
                <Typography mr={showTitle ? 1 : 0} variant={"body1"} my={0.5}>
                    <Person
                        style={{
                            color: cyan[400],
                            marginLeft: theme.spacing(1)
                        }}/>
                    {(user && user.fullName) ? user.fullName : 'نامشخص'}
                </Typography>
                <Typography mr={showTitle ? 1 : 0} variant={"h6"} my={0.5}>
                    <Phone
                        style={{
                            direction: 'ltr',
                            color: cyan[400],
                            marginLeft: theme.spacing(1)
                        }}/>
                    {user ? user.username : "09---------"}
                </Typography>
            </Box>
        </Box>
    )
}
