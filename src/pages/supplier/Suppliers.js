import React, {useEffect, useState} from "react";
import Box from "@material-ui/core/Box";
import Table, {createTableHeader, headerItemTemplate} from "../../components/base/table/Table";
import {createFilter} from "../../components/base/table/Filters";
import DataUtils, {filterType} from "../../utils/DataUtils";
import {useDispatch} from "react-redux";
import _ from "lodash";
import storage from "../../storage";
import ControllerUser from "../../controller/ControllerUser";
import {ApiHelper} from "../../controller/converter";
import useSWR, {mutate} from "swr";
import Tooltip from "@material-ui/core/Tooltip";
import {lang, theme} from "../../repository";
import IconButton from "@material-ui/core/IconButton";
import rout from "../../router";
import {EditOutlined, FiberManualRecordTwoTone} from "@material-ui/icons";
import Link from "../../components/base/link/Link";
import {UtilsStyle} from "../../utils/Utils";
import ComponentError from "../../components/base/ComponentError";
import {green, red} from "@material-ui/core/colors";
import Gem2 from "../../components/base/icon/Gem2";
import {gcLog} from "../../utils/ObjectUtils";

const headers = [
    headerItemTemplate.id,
    createTableHeader({id: "username", type: 'str', label: 'نام‌کاربری'}),
    headerItemTemplate.name,
    createTableHeader({id: "is_verify", type: 'str', label: 'وضعیت'}),
    createTableHeader({id: "shaba", type: 'str', label: 'شبا'}),
    ...headerItemTemplate.createByUpdatedBy(),
];

const filters = [
    createFilter({id: 'name__fa', type: filterType.text, label: 'نام', value: ''}),
];
const CookieKey = "suppliers";

export default function ({...props}) {
    //region var
    const dispatch = useDispatch();

    //region state
    const [filtersState, setFiltersState] = useState({
        data: filters,
        resetFilters: () => {
            setFiltersState({
                ...filtersState,
                data: DataUtils().TableFilter().resetFilter({filters: filtersState.data}),
            })
        }
    });
    const [state, setState] = useState({
        filters: _.cloneDeep(filters),
        order: storage.PageSetting.getOrder(CookieKey),
        pagination: {
            rowPerPage: storage.PageSetting.getRowPerPage(CookieKey),
            page: 0
        }
    });
    //endregion state
    //endregion var

    //region useEffect
    useEffect(() => {
        requestFilters();
    }, []);

    useEffect(() => {
        storage.PageSetting.setPage(CookieKey, {page: state.pagination.page});
    }, [state.pagination.page]);
    //endregion useEffect

    //region requests
    const d = ControllerUser.Supplier.get();
    const apiKey = ApiHelper.createSWRKey({
        api: d[0],
        order: state.order,
        filters: state.filters,
        page: state.pagination.page,
        step: state.pagination.rowPerPage
    });
    const {data: dd, error} = useSWR(apiKey, () => {
        return d[1]({
            page: state.pagination.page + 1,
            step: state.pagination.rowPerPage,
            order: state.order,
            filters: state.filters,
        })
    });




    function requestFilters() {
        ControllerUser.Supplier.Filters.get({boxId: state.activeBox}).then((res) => {
            const newList = _.isArray(filtersState.data) && !_.isEmpty(filtersState.data) ? filtersState.data : filters;
            _.forEach(res.data, (fi) => {
                const data = createFilter({id: fi.name, type: fi.type, label: fi.label, data: fi.filters, value: null});
                switch (fi.name) {
                    case 'category': {
                        data.value = [];
                        break;
                    }
                    default: {
                    }
                }

                const index = _.findIndex(newList, (l) => l.id === data.id);
                newList[index !== -1 ? index : newList.length] = data;
            });
            setFiltersState({
                ...filtersState,
                data: [...newList]
            })
        }).catch((e) => {
            setFiltersState({
                ...filtersState,
                data: false
            })
        });
    }

    //endregion requests

    //region handler
    function handleFilterChange({item, value}) {
        setState({
            ...state,
            pagination: {
                ...state.pagination,
                page: 0
            }
        })
        setFiltersState({
            ...filtersState,
            data: DataUtils().TableFilter().filterItemChange({filters: filtersState.data, item: item, value: value})
        });
    }

    function handleApplyFilter() {
        setState({
            ...state,
            filters: _.cloneDeep(filtersState.data)
        })
    }

    function handleResetFilter() {
        filtersState.resetFilters()
    }

    function handleChangeOrder(order) {
        storage.PageSetting.setOrder(CookieKey, {order: order});
        setState({
            ...state,
            order: {
                ...order
            }
        });
    }

    //endregion handler


    //region renderData
    const pg = state.pagination;

    const tableData = (dd && dd.data ? dd.data : [...Array(state.pagination.rowPerPage)]).map((pr, i) => {
        const link = pr ? rout.Supplier.Single.editSupplier(pr.id) : "";

        return (
            (
                pr ? {
                    id: {
                        label: pr.id,
                    },
                    username: {
                        label: pr.username,
                        link: link
                    },
                    name: {
                        label: pr.fullName,
                        link: link
                    },
                    is_verify: {
                        label: (
                            <Box display={'flex'} pl={2} style={{...UtilsStyle.disableTextSelection()}}>
                                <Tooltip title={pr.isVerify ? "فعال" : "غیرفعال"}>
                                    <Box component={'span'}>
                                        <FiberManualRecordTwoTone style={{color:pr.isVerify ? green[300] : red[300]}}/>
                                    </Box>
                                </Tooltip>
                            </Box>),
                    },
                    shaba: {
                        label: pr.shaba,
                    },
                    created_at: {
                        label: pr.createdAt.date,
                    },
                    created_by: {
                        label: pr.createdAt.by.name,
                        link: pr.createdAt.by.link,
                    },
                    updated_at: {
                        label: pr.updatedAt.date,
                    },
                    updated_by: {
                        label: pr.updatedAt.by.name,
                        link: pr.updatedAt.by.link,
                    },
                } : {}
            )
        )
    });
    //endregion renderData


    return (
        <Box
            my={2}
            px={2}>
            {(!error) ?
                <Table headers={headers}
                       cookieKey={CookieKey}
                       title={"تامین کننده"}
                       data={tableData}
                       filters={filtersState.data}
                       onFilterChange={handleFilterChange}
                       loading={false}
                       activePage={pg.page}
                       showBox={false}
                       rowsPerPage={pg.rowPerPage}
                       lastPage={dd ? dd.pagination.lastPage : 0}
                       rowCount={dd ? dd.pagination.count : 0}
                       orderType={state.order}
                       activeBox={state.activeBox}
                       addNewButton={{
                           label: lang.get('add_new_supplier'),
                           link: rout.Supplier.Single.createNewSupplier,
                       }}
                       onActivePageChange={(page) => {
                           setState({
                               ...state,
                               pagination: {
                                   ...state.pagination,
                                   page: page
                               }
                           })
                           // requestData({page: e})
                       }}
                       onRowPerPageChange={(rowPerPage) => {
                           storage.PageSetting.setRowPerPage(CookieKey, {rowPerPage: rowPerPage});
                           setState({
                               ...state,
                               pagination: {
                                   ...state.pagination,
                                   rowPerPage: rowPerPage
                               }
                           })
                       }}
                       onApplyFilterClick={handleApplyFilter}
                       onResetFilterClick={handleResetFilter}
                       onChangeOrder={handleChangeOrder}/> :
                <Box>
                    <ComponentError
                        statusCode={(_.isObject(error) && error.request) ? error.request.status : undefined}
                        tryAgainFun={() => mutate(apiKey)}/>
                </Box>
            }
        </Box>
    )
}
