import React, {Fragment, useEffect, useRef, useState} from "react";
import Box from "@material-ui/core/Box";
import {Card, Dialog, useTheme, Icon, FormControlLabel, Switch} from "@material-ui/core";
import {useSnackbar} from "notistack";
import {convertFeatures, convertFeaturesGroup, createMultiLanguage} from "../../controller/converter";
import ControllerProduct from "../../controller/ControllerProduct";
import PleaseWait from "../../components/base/loading/PleaseWait";
import ComponentError from "../../components/base/ComponentError";
import Typography from "../../components/base/Typography";
import {featureType} from "../../controller/type";
import BaseButton from "../../components/base/button/BaseButton";
import _ from "lodash";
import Transition from "../../components/base/Transition";
import StickyBox from "react-sticky-box";
import SuccessButton from "../../components/base/button/buttonVariant/SuccessButton";
import DefaultTextFieldMultiLanguage from "../../components/base/textField/DefaultTextFieldMultiLanguage";
import FormController from "../../components/base/formController/FormController";
import {cyan} from "@material-ui/core/colors";
import {UtilsData, UtilsRouter, UtilsStyle} from "../../utils/Utils";
import {lang, siteLang} from "../../repository";
import ButtonBase from "@material-ui/core/ButtonBase";
import BaseDialog from "../../components/base/BaseDialog";
import Backdrop from "@material-ui/core/Backdrop";
import CircularProgress from "@material-ui/core/CircularProgress";
import IconButton from "@material-ui/core/IconButton";
import {DeleteOutlineOutlined} from "@material-ui/icons";
import Button from "@material-ui/core/Button";
import axios from "axios";
import rout from "../../router";
import SelectFeaturesGroup, {Item as FeatureGroupItem} from "../featuresGroup/SelectFeaturesGroup";
import DialogAppBar from "../../components/dialog/DialogAppBar";
import {gcLog, tryIt} from "../../utils/ObjectUtils";
import FeatureSort from "./FeatureSort";
import api from "../../controller/api";
import IconSelector from '../../components/icon/IconSelector';
import MtIcon from "../../components/MtIcon";


export default function ({featureId, open, onClose, ...props}) {
    const theme = useTheme();
    const ref = useRef();
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();
    const fId = (() => {
        try {
            return featureId || props.match.params.fId
        } catch (e) {
            return undefined
        }
    })();
    const [state, setState] = useState({
        error: false,
        loading: false
    });
    const [showDuplicate, setShowDuplicate] = useState(false);
    const [data, setData] = useState(!fId ? initData() : {});
    const [sorting, setSorting] = useState(false)
    const [iconDialog, setIconDialog] = useState(false)
    const [icon, setIcon] = useState(data?.settings?.icon)
    const [showGrid, setShowGrid] = useState(data?.settings?.showGrid)
    const [hideName, setHideName] = useState(data?.settings?.hideName)
    const [duplicate, setDuplicate] = useState(false)
    const [timer, setTimer] = useState()


    useEffect(() => {
        if (duplicate) {
            enqueueSnackbar("یک فیچر با این نام وجود دارد",
                {
                    variant: "warning",
                    action: (key) => (
                        <Fragment>
                            <Button onClick={() => {
                                closeSnackbar(key)
                            }}>
                                {lang.get('close')}
                            </Button>
                        </Fragment>
                    )
                });
            setDuplicate(o => false)
        }

    }, [duplicate])

    function initData() {
        return {
            name: createMultiLanguage(),
            type: null,
            value: [],
            lastValue: [],
            groups: []
        }
    }

    function getData(setNewValue) {
        setState({
            ...state,
            loading: true
        })
        ControllerProduct.Features.getSingle({fId: fId}).then((res) => {
            if (setNewValue) {
                setData({
                    ...data,
                    lastValue: [...res.data.values]
                })
            } else {
                setShowGrid(res.data.settings?.ui?.showGrid)
                setHideName(res.data.settings?.ui?.hideName)
                setIcon(res.data.settings?.ui?.icon)
                setData({
                    ...res.data,
                    name: res.data.name,
                    type: res.data.type,
                    value: [],
                    lastValue: res.data.values,
                    groups: convertFeaturesGroup(res.data.groups),
                    settings: res.data.settings
                });
            }
            setState({
                ...state,
                loading: false,
                error: false
            })
        }).catch((e) => {
            setState({
                ...state,
                error: true,
                loading: false
            })
        })
    }

    const handleSave = () => {
        try {
            if (ref.current.hasError()) {
                enqueueSnackbar("لطفا تمام فیلد های اجباری را پرکنید.",
                    {
                        variant: "error",
                        action: (key) => (
                            <Fragment>
                                <Button onClick={() => {
                                    closeSnackbar(key)
                                }}>
                                    {lang.get('close')}
                                </Button>
                            </Fragment>
                        )
                    });
                return
            }
            setState({
                ...state,
                loading: true
            })
            const {name, value} = ref.current.serialize()
            const val = []
            _.forEach(value, v => {
                if (_.isEmpty(v[siteLang]))
                    return
                val.push(v)
            })
            ControllerProduct.Features.save({
                id: fId,
                name: name,
                values: val,
                type: data.type.key,
                settings: {ui: {showGrid: showGrid, hideName: hideName, icon: icon}}
            }).then(res => {
                if (res.status === 200) {
                    setShowDuplicate(res.data.data)
                    return;
                }

                if (_.isBoolean(open)) {
                    onClose()
                    return
                }

                if (!fId) {
                    redirect(res.data.id)
                    return;
                }
                window.location.reload()
            }).finally(() => {
                setState({
                    ...state,
                    loading: false
                })
            })
        } catch (e) {

            setState({
                ...state,
                loading: true
            })
        }
    }


    function redirect(id) {
        UtilsRouter.goTo(props.history, {routUrl: rout.Feature.Single.editFeature(id)})
        window.location.reload()
    }


    const handleAddNewItem = (itemsCount = 1, returnValue) => {
        try {
            const newValues = data.value || [];
            for (let i = 0; i < itemsCount; i++) {
                newValues.push({
                    id: UtilsData.createId(newValues),
                    value: createMultiLanguage()
                })
            }
            if (returnValue)
                return newValues
            setData({
                ...data,
                value: [...newValues]
            })
        } catch (e) {
        }
    }

    const removeItem = (item) => {
        try {
            const {value} = ref.current.serialize();
            const newValue = [];
            _.forEach(value, (v, key) => {
                if (item.id === _.toNumber(key))
                    return;
                newValue.push({
                    id: _.toNumber(key),
                    value: v
                })
            })
            setData({
                ...data,
                value: [...newValue]
            })
        } catch (e) {
        }
    }

    const handleChangeFeature = () => {
        setState({
            ...state,
            loading: true
        })
        getData(true)
    }

    useEffect(() => {
        if (fId)
            getData()
    }, [])


    const addItemEl = ({...props}) => (
        (data && data.type && data.type.key === featureType.selectable.key) ?
            <Box display={'flex'} flexDirection={'column'} {...props}>
                <BaseButton
                    variant={"outlined"}
                    onClick={() => handleAddNewItem()}
                    style={{
                        borderColor: cyan[500]
                    }}>
                    افزودن آیتم جدید
                </BaseButton>
                <BaseButton
                    variant={"outlined"}
                    onClick={() => {
                        setSorting(true)
                    }}
                    style={{borderColor: cyan[400]}}>
                    مرتبسازی
                </BaseButton>
                <FeatureSort
                    open={sorting}
                    featureId={fId}
                    onClose={(updated) => {
                        getData()
                        setSorting(false);
                    }}/>
            </Box> :
            <React.Fragment/>
    )

    function handleTitleChange(event) {
        const {value} = event.target

        clearTimeout(timer)
        setTimer(setTimeout(() => {
            setDuplicate(o => false)
            if (value.length % 3 === 0) {
                axios.get(api.Products.features, {
                    params: {
                        name__fa: value
                    }
                }).then((res) => {
                    if (res.status === 200) {
                        if (res.data.data.filter(item => item.name[siteLang] === value && item.type === data.type.key).length !== 0) {
                            setDuplicate(o => true)
                        }
                    }
                })
            }
        }, 2000))
    }

    const el = (
        <Box py={3}>
            {
                !state.error ?
                    (!_.isEmpty(data)) ?
                        !data.type ?
                            <SelectType
                                onSelect={(type) => {
                                    if (!data.type) {
                                        setData({
                                            ...initData(),
                                            type: type,
                                            value: handleAddNewItem((type.key === featureType.selectable.key || type.key === featureType.bool.key) ? 2 : 1, true)
                                        })
                                    }
                                }}/>
                            :
                            <React.Fragment>
                                <Box display={'flex'}>
                                    <FormController innerref={ref} width={'80%'} display={'flex'}
                                                    flexDirection={'column'}
                                                    p={2}>
                                        <Box component={Card} p={2}>
                                            <DefaultTextFieldMultiLanguage
                                                label={"نام"}
                                                required={true}
                                                name={"name"}
                                                defaultValue={data.name}
                                                variant={"outlined"}

                                                onChange={(event) => handleTitleChange(event)}
                                            />
                                            {
                                                !_.isEmpty(data.value) &&
                                                <Box my={2}
                                                     p={2}
                                                     style={{
                                                         border: `1px solid ${cyan[300]}`,
                                                         ...UtilsStyle.borderRadius(5)
                                                     }}>

                                                    {data.value.map((v, i) => (
                                                        <Box id={v.id} py={2} display={'flex'}>
                                                            <DefaultTextFieldMultiLanguage
                                                                group={"value"}
                                                                label={`آیتم- ${i + 1}`}
                                                                multiline={data.type.key === featureType.text.key}
                                                                rowsMax={4}
                                                                required={!fId && i <= 1}
                                                                defaultValue={v.value}
                                                                name={v.id}/>
                                                            <Box py={1} px={2}>
                                                                <IconButton
                                                                    disabled={(!fId && i <= 1)}
                                                                    onClick={() => removeItem(v)}>
                                                                    <DeleteOutlineOutlined/>
                                                                </IconButton>
                                                            </Box>
                                                        </Box>
                                                    ))}
                                                </Box>
                                            }
                                            {
                                                !_.isEmpty(data.lastValue) &&
                                                <Box my={2} p={2}
                                                     display={'flex'}
                                                     flexWrap={'wrap'}
                                                     style={{
                                                         border: `1px solid ${cyan[300]}`,
                                                         ...UtilsStyle.borderRadius(5)
                                                     }}>
                                                    {data.lastValue.map((v, i) => (

                                                        <FeatureItem key={v.id} item={v}
                                                                     type={data.type}
                                                                     onChange={handleChangeFeature}/>
                                                    ))}
                                                </Box>
                                            }
                                        </Box>
                                        <FeatureGroup fId={fId} groups={data.groups}/>
                                    </FormController>
                                    <Box width={'20%'} display={'flex'} p={2} flexDirection={'column'}>

                                        <StickyBox offsetTop={180} offsetBottom={20}>
                                            <Box display={'flex'} component={Card} justifyContent={'center'}
                                                 alignItems={'center'}
                                                 width={1}
                                                 flexDirection={'column'}>

                                                {icon?.name && <Box p={2}><MtIcon
                                                    fontSize={'large'}
                                                    icon={`${icon?.name}`}
                                                    style={{
                                                        color: icon?.color,
                                                        fontSize:'75px'
                                                    }}/>
                                                    </Box>}


                                                <BaseButton
                                                    variant={'outlined'}
                                                    onClick={() => {
                                                        setIconDialog(true)
                                                    }}
                                                    style={{borderColor: cyan[400]}}
                                                >
                                                    انتخاب آیکون
                                                </BaseButton>
                                                {addItemEl({pt: 2, mx: '8px'})}
                                                <Box p={2}>
                                                    <FormControlLabel
                                                        control={<Switch checked={showGrid} onChange={(event) => {
                                                            setShowGrid(event.target.checked)
                                                        }} name="gilad"/>}
                                                        label="گرید :"
                                                        labelPlacement="start"
                                                    />
                                                    <FormControlLabel
                                                        control={<Switch checked={hideName} onChange={(event) => {
                                                            setHideName(event.target.checked)
                                                        }} name="gilad"/>}
                                                        label="عدم نمایش نام :"
                                                        labelPlacement="start"
                                                    />
                                                </Box>
                                                <SuccessButton
                                                    onClick={handleSave}>
                                                    ذخیره
                                                </SuccessButton>
                                            </Box>
                                        </StickyBox>
                                    </Box>
                                </Box>
                                <Backdrop open={state.loading}
                                          style={{
                                              zIndex: '9999'
                                          }}>
                                    <CircularProgress color="inherit"
                                                      style={{
                                                          color: cyan[300],
                                                          width: 100,
                                                          height: 100
                                                      }}/>
                                </Backdrop>
                            </React.Fragment>
                        :
                        <PleaseWait/>
                    :
                    <ComponentError tryAgainFun={() => getData()}/>
            }
            <BaseDialog
                open={Boolean(showDuplicate)}
                header={false}
                onClose={() => {
                    setShowDuplicate(false)
                }}>
                <Box display={'flex'} flexDirection={'column'} p={3}>
                    <Typography variant={"h6"} fontWeight={500}>
                        یک فیچر با همین اسم وجود دارد.
                    </Typography>
                    <Box display={'flex'} pt={2}>
                        <SuccessButton
                            variant={"outlined"}
                            onClick={() => {
                                tryIt(() => {
                                    redirect(showDuplicate.id)
                                })
                            }}>
                            ویرایش فیچر
                        </SuccessButton>
                        <Box mx={1}>
                            <BaseButton
                                variant={"text"}
                                onClick={() => {
                                    setShowDuplicate(false)
                                }}>
                                انصراف
                            </BaseButton>
                        </Box>
                    </Box>
                </Box>
            </BaseDialog>
        </Box>
    )

    return (
        <React.Fragment>
            <IconSelector
                open={iconDialog}
                // type={type}
                onSelect={(item) => {
                    setIconDialog(false);
                    setIcon(item)
                    gcLog("IconSelector onSelect item", item)
                }}/>
            {
                (_.isBoolean(open) && onClose) ?
                    <Dialog fullScreen={true} open={open} ransitionComponent={Transition}>
                        <DialogAppBar onClose={() => onClose()}/>
                        {el}
                    </Dialog> :
                    el
            }
        </React.Fragment>
    )
}


function SelectType(
{
    onSelect,
...
    props
}
)
{
    const theme = useTheme();

    return (
        <Box display={'flex'} flexDirection={'column'}>
            <Typography variant={'h6'} fontWeight={600} py={1}>
                انتخاب نوع فیچر:
            </Typography>
            <Box display={'flex'} flexWrap={'wrap'}>
                {Object.keys(featureType).map((key, index) => {
                    const f = featureType[key];
                    return (
                        <Box m={1} key={f.key}>
                            <BaseButton
                                onClick={() => onSelect(f)}
                                style={{
                                    color: '#fff',
                                    backgroundColor: theme.palette.primary.main
                                }}>
                                {f.typeLabel}
                            </BaseButton>
                        </Box>
                    )
                })}
            </Box>
        </Box>
    )
}

function FeatureItem(
{
    item, type, onChange
}
)
{
    const ref = useRef();
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();

    const [open, setOpen] = useState(false)
    const [state, setState] = useState({
        loading: false
    })

    const handleClose = () => {
        setState({
            ...state,
            loading: false
        })
        setOpen(false)
    }

    const handleSave = () => {
        try {
            if (ref.current.hasError()) {
                enqueueSnackbar("لطفا تمام فیلد های اجباری را پرکنید.",
                    {
                        variant: "error",
                        action: (key) => (
                            <Fragment>
                                <Button onClick={() => {
                                    closeSnackbar(key)
                                }}>
                                    {lang.get('close')}
                                </Button>
                            </Fragment>
                        )
                    });
                return
            }
            setState({
                ...state,
                loading: true
            })
            const {name} = ref.current.serialize()
            ControllerProduct.Features.saveItem({fId: item.id, value: name}).finally(() => {
                handleClose()
                onChange()
            })
        } catch (e) {
            setState({
                ...state,
                loading: false
            })
        }
    }
    return (
        <React.Fragment>
            <Box p={2}>
                <ButtonBase
                    onClick={() => setOpen(true)}>
                    <Box component={Card} display={'flex'} flexDirection={'column'} p={2}>
                        <Typography variant={"h6"} whiteSpace={true}>
                            {item.name[siteLang]}
                        </Typography>
                    </Box>
                </ButtonBase>
            </Box>
            <BaseDialog open={open} onClose={handleClose}>
                <FormController name={`feature-item-${item.id}`} innerref={ref} display={'flex'}
                                flexDirection={'column'} py={3} px={2}>
                    <DefaultTextFieldMultiLanguage
                        name={'name'}
                        defaultValue={item.name}
                        required={true}
                        multiline={type.key === featureType.text.key}
                        rowsMax={4}
                        label={"نام"}/>
                    <Box pt={2}>

                        <SuccessButton onClick={handleSave}>
                            ذخیره
                        </SuccessButton>
                    </Box>
                </FormController>
            </BaseDialog>
            <Backdrop open={state.loading}
                      style={{
                          zIndex: '9999'
                      }}>
                <CircularProgress color="inherit"
                                  style={{
                                      color: cyan[300],
                                      width: 100,
                                      height: 100
                                  }}/>
            </Backdrop>
        </React.Fragment>
    )
}

function FeatureGroup(
{
    fId, groups
:
    gp = [],
...
    props
}
)
{
    const theme = useTheme();
    const [state, setState] = useState({
        loading: false
    })
    const [groups, setGroups] = useState(gp)
    const [open, setOpen] = useState(false)

    function handleAddFeaturesGroup(gp) {
        if (!gp) {
            setOpen(false)
            return;
        }
        setState({
            ...state,
            loading: true
        })

        const items = [];
        _.forEach(groups, (gp) => {
            items.push(gp.id);
        })
        items.push(gp.id)
        update(items)
    }

    function handleRemoveItem(gp) {
        const newList = [];
        _.forEach(groups, i => {
            if (i.id !== gp.id) newList.push(i.id)
        })
        // _.remove(newList, it => it.id === gp.id)
        update(newList)
    }

    function update(items) {
        ControllerProduct.Features.updateGroup({id: fId, groups: items}).finally(() => {
            setOpen(false)
            ControllerProduct.Features.getSingle({fId: fId}).then(res => {
                setGroups(res.data.groups)
            }).finally(() => {
                setState({
                    ...state,
                    loading: false
                })
            })
        })
    }

    return (
        fId ?
            <Box py={2}>
                {
                    _.isEmpty(groups) ?
                        <Box display={'flex'}>
                            <BaseButton variant={"outlined"}
                                        onClick={() => {
                                            setOpen(true)
                                        }}
                                        style={{borderColor: cyan[400]}}>
                                افزودن به گروه فیچر
                            </BaseButton>
                        </Box> :
                        <Box component={Card}
                             display={'flex'}
                             flexWrap={'wrap'}
                             p={2}>
                            <Box display={'flex'} width={1}>
                                <BaseButton variant={"outlined"}
                                            onClick={() => {
                                                setOpen(true)
                                            }}
                                            style={{borderColor: cyan[400]}}>
                                    افزودن به گروه فیچر
                                </BaseButton>
                            </Box>
                            {
                                groups.map(gp => (
                                    <Box maxWidth={1 / 3} key={gp.id} p={2}>
                                        <FeatureGroupItem item={gp} onRemove={() => handleRemoveItem(gp)}/>
                                    </Box>
                                ))
                            }
                        </Box>}
                <SelectFeaturesGroup
                    open={open}
                    autoHandleBox={true}
                    onSelect={handleAddFeaturesGroup}/>
                <Backdrop open={state.loading}
                          style={{
                              zIndex: '9999'
                          }}>
                    <CircularProgress color="inherit"
                                      style={{
                                          color: cyan[300],
                                          width: 100,
                                          height: 100
                                      }}/>
                </Backdrop>
            </Box> :
            <React.Fragment/>
    )
}
