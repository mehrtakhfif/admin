


import React, {useEffect, useState} from "react";
import useSWR, {mutate} from 'swr'
import _ from "lodash";
import ControllerProduct from "../../controller/ControllerProduct";
import Box from "@material-ui/core/Box";
import {Card, Dialog, useTheme} from "@material-ui/core";
import Transition from "../../components/base/Transition";
import ComponentError from "../../components/base/ComponentError";
import PleaseWait from "../../components/base/loading/PleaseWait";
import ButtonBase from "@material-ui/core/ButtonBase";
import Icon from "../../components/icon/Icon";
import Typography from "../../components/base/Typography";
import IconButton from "@material-ui/core/IconButton";
import {CancelOutlined, Close} from "@material-ui/icons";
import {siteLang} from "../../repository";
import Divider from "@material-ui/core/Divider";
import TextField from "../../components/base/textField/TextField";
import TextFieldContainer from "../../components/base/textField/TextFieldContainer";


export default function ({open, onSelect, boxId, ...props}) {
    const theme = useTheme();
    const d = ControllerProduct.Features.get();
    const apiKey = d[0] + boxId;
    const [filter, setFilter] = useState("");
    const {data:orgData, error} = useSWR(apiKey, () => {
        return d[1]({
            boxId: boxId,
            all: true
        }).then(res => {
            return res.data
        })
    });
    const [data, setData] = useState(orgData);

    useEffect(() => {
        if (!orgData)
            return;
        if (!filter) {
            setData([...orgData]);
            return;
        }

        const newList = [];
        _.forEach(orgData, (d) => {
            if (d.name.fa.includes(filter))
                newList.push(d)
        });
        setData([...newList]);
    }, [orgData, filter]);

    return (
        <Dialog open={open} maxWidth={"xl"} fullWidth={true} scroll={"body"} transitionDuration={Transition}
                onClose={() => {
                    onSelect()
                }}>
            <Box width={1} p={2} display={'flex'} flexDirection={'column'}>
                <Box display={'flex'} alignItems={'center'}>
                    <IconButton onClick={() => onSelect()}>
                        <Close/>
                    </IconButton>
                    <Typography px={2} variant={'h6'}>
                        انتخاب فیچر
                    </Typography>
                </Box>
                {!error ?
                    data ?
                        <Box display={'flex'} flexDirection={'column'} alignItems={'center'} justifyContent={'center'}>
                            <Box width={4 / 5} pb={3}>
                                <TextFieldContainer
                                    onChangeDelay={300}
                                    onChange={(v) => setFilter(v)}
                                    render={(ref,) => {
                                        return (
                                            <TextField
                                                variant="outlined"
                                                fullWidth
                                                inputRef={ref}
                                                endAdornment={filter && (
                                                    <IconButton onClick={() => setFilter("")}>
                                                        <CancelOutlined/>
                                                    </IconButton>
                                                )}
                                                label={'جست‌و‌جو نام فیچر'}
                                                style={{
                                                    flex: 1,
                                                    paddingLeft: theme.spacing(1.5)
                                                }}/>
                                        )
                                    }}
                                />
                            </Box>
                            <Box width={1} display={'flex'} flexWrap={'wrap'}>
                                {data.map(d => {
                                    return (
                                        <Box key={d.id} p={1}>
                                            <ButtonBase onClick={() => {
                                                onSelect(d)
                                            }}>
                                                <Box component={Card} display={'flex'} p={2}>
                                                    <Icon width={100} height={100} minHeight={100} showSkeleton={false}
                                                          src={d.iconLink} alt={d.iconName}/>
                                                    <Box display={'flex'} flexDirection={'column'} pl={1.5}>
                                                        <Typography variant={"h5"} fontWeight={500} pb={1}>
                                                            {d.name[siteLang]}
                                                        </Typography>
                                                        <Divider/>
                                                        <Box display={'flex'} pt={1} flexWrap={'wrap'} maxWidth={400}>
                                                            {d.value.map((v, i) => {
                                                                return (
                                                                    <Typography key={"fv"+v.id} variant={"body1"}>
                                                                        {i > 0 && ", "} {v.name[siteLang] ? v.name[siteLang] : "----"}
                                                                    </Typography>
                                                                )
                                                            })}
                                                        </Box>
                                                    </Box>
                                                </Box>
                                            </ButtonBase>
                                        </Box>
                                    )
                                })}
                            </Box>
                        </Box> :
                        <PleaseWait/>
                    :
                    <ComponentError
                        statusCode={(_.isObject(error) && error.request) ? error.request.status : undefined}
                        tryAgainFun={() => mutate(apiKey)}/>}
            </Box>
        </Dialog>
    )
}
