import React, {useEffect, useState} from "react";
import Table, {createTableHeader, headerItemTemplate} from "../../components/base/table/Table";
import {useSelector} from "react-redux";
import storage from "../../storage";
import {createFilter} from "../../components/base/table/Filters";
import DataUtils, {filterType} from "../../utils/DataUtils";
import ControllerProduct from "../../controller/ControllerProduct";
import Box from "@material-ui/core/Box";
import {ApiHelper} from "../../controller/converter";
import useSWR, {mutate} from "swr";
import _ from "lodash";
import Tooltip from "@material-ui/core/Tooltip";
import {activeLang, lang, theme} from "../../repository";
import IconButton from "@material-ui/core/IconButton";
import rout from "../../router";
import {EditOutlined} from "@material-ui/icons";
import Link from "../../components/base/link/Link";
import ComponentError from "../../components/base/ComponentError";
import Typography from "../../components/base/Typography";
import Icon from "../../components/icon/Icon";
import {gcLog} from "../../utils/ObjectUtils";
import Autocomplete from "@material-ui/lab/Autocomplete";
import TextField from "../../components/base/textField/TextField";

const headers = [
    ...headerItemTemplate.idActionName(),
    createTableHeader({id: "type", type: 'str', label: 'نوع'}),
    createTableHeader({id: "values_count", type: 'str', label: 'تعداد آیتم‌ها',sortable:false}),
    ...headerItemTemplate.createByUpdatedBy(),
];
const filters = [
    createFilter({id: 'name__fa', type: filterType.text, label: 'نام', value: ''}),
];
const CookieKey = "features";

export default function ({...props}) {
    //region var
    const {boxes} = useSelector(state => state.user);

    //region state
    const [state, setState] = useState({
        filters: _.cloneDeep(filters),
        activeBox: storage.PageSetting.getActiveBox(CookieKey, boxes),
        order: storage.PageSetting.getOrder(CookieKey),
        pagination: {
            rowPerPage: storage.PageSetting.getRowPerPage(CookieKey),
            page: 0
        }
    });
    const [comboBoxData, setComboBoxData] = useState([{name: "در حال بارگذاری", id: 0}]);
    const [comboBox, setComboBox] = useState();
    const [filtersState, setFiltersState] = useState({
            data: filters,
            resetFilters: () => {
                setFiltersState({
                    ...filtersState,
                    data: DataUtils().TableFilter().resetFilter({filters: filtersState.data}),
                })
            }

        }
    );

    //endregion state
    //endregion var

    //region useEffect
    useEffect(() => {
        if (!state.activeBox)
            return;
        requestFilters();
    }, [state.activeBox]);
    //endregion useEffect

    //region requests

    const d = ControllerProduct.Features.get();
    const apiKey = ApiHelper.createSWRKey({
        api: d[0],
        order: state.order,
        filters: state.filters,
        box: state.activeBox,
        page: state.pagination.page,
        step: state.pagination.rowPerPage
    })
    const {data, error} = useSWR(apiKey, () => {
        return d[1]({
            boxId: state.activeBox,
            page: state.pagination.page + 1,
            step: state.pagination.rowPerPage,
            order: state.order,
            filters: state.filters,
        })
    });


    // const dg = ControllerProduct.Features.getGroup();
    // const apiGroupKey = ApiHelper.createSWRKey({api:dg[0],order:state.order,filters:state.filters,box:state.activeBox,page:state.pagination.page,step:state.pagination.rowPerPage})
    // const {groupData, groupError} = useSWR(apiGroupKey, () => {
    //     return d[1]({
    //         boxId: state.activeBox,
    //         page: state.pagination.page + 1,
    //         step: state.pagination.rowPerPage,
    //         order: state.order,
    //         filters: state.filters,
    //     })
    // });


    function requestFilters() {
        ControllerProduct.Features.Filters.get({boxId: state.activeBox}).then((res) => {
            const newList = _.isArray(filtersState.data) && !_.isEmpty(filtersState.data) ? filtersState.data : filters;
            _.forEach(res.data, (fi) => {
                const data = createFilter({id: fi.name, type: fi.type, label: fi.label, data: fi.filters, value: null});
                switch (fi.name) {
                    case 'category': {
                        data.value = [];
                        break;
                    }
                    case 'group_id' : {
                        data.value = [];
                        setComboBoxData(fi.filters)
                        break
                    }
                    default: {
                    }
                }

                const index = _.findIndex(newList, (l) => l.id === data.id);
                newList[index !== -1 ? index : newList.length] = data;

            });
            setComboBoxData(newList[1].data)
            setFiltersState({
                    ...filtersState,
                    data: [...newList]
                }
            )
        }).catch((e) => {
            setFiltersState({
                ...filtersState,
                data: false
            })
        });
    }

    //endregion requests

    //region handler

    function handleComboChange(e, value) {
        setComboBox(value)

        const index = filtersState.data[1].data.findIndex(x => x.id ===value.id);
        handleFilterChange({item:filtersState.data[1].data,value:filtersState.data[1].data[index]})

        // handleFilterChange()
        handleApplyFilter()
    }

    function ComboBox() {
        return (
            <Autocomplete
                id="searchFeatureGroup"
                options={comboBoxData}
                onChange={handleComboChange}
                getOptionLabel={(option) => option.name}
                style={{width: 300, margin: 8}}
                value={comboBox}
                renderInput={(params) => <TextField {...params} label="جست و جوی گروه..." variant="outlined"/>}
            />
        );
    }


    function handleFilterChange({item, value}) {
        setState({
            ...state,
            pagination: {
                ...state.pagination,
                page: 0
            }
        })
        setFiltersState({
            ...filtersState,
            data: DataUtils().TableFilter().filterItemChange({filters: filtersState.data, item: item, value: value})
        });
    }

    function handleApplyFilter() {
        setState({
            ...state,
            filters: _.cloneDeep(filtersState.data)
        })
    }

    function handleResetFilter() {
        filtersState.resetFilters()
    }

    function handleChangeOrder(order) {
        storage.PageSetting.setOrder(CookieKey, {order: order});
        setState({
            ...state,
            order: {
                ...order
            }
        });
    }

    function handleChangeBox(e, box) {
        if (!box || state.activeBox === box.id)
            return;
        storage.PageSetting.setActiveBox(CookieKey, {activeBox: box.id});
        filtersState.resetFilters();
        setState({
            ...state,
            activeBox: box.id,
            pagination: {
                ...state.pagination,
                page: 0
            }
        });
    }

    //endregion handler

    //region renderData
    const pg = state.pagination;
    const tableData = (data && data.data ? data.data : [...Array(state.pagination.rowPerPage)]).map((fe, i) => (
        fe ? {
            id: {
                label: fe.id,
            },
            actions: {
                label: <Box display={'flex'} alignItems={'center'} pl={1}>
                    <Tooltip title={lang.get('edit')}>
                        <IconButton size={"small"}
                                    style={{
                                        marginRight: theme.spacing(0.5),
                                        marginLeft: theme.spacing(0.5)
                                    }}>
                            <Link toHref={rout.Feature.Single.editFeature(fe.id)} linkStyle={{display: 'flex'}}
                                  hoverColor={theme.palette.primary.main}>
                                <EditOutlined style={{fontSize: 20, padding: 2}}/>
                            </Link>
                        </IconButton>
                    </Tooltip>
                </Box>
            },
            name: {
                label: fe.name[activeLang],
            },
            type: {
                label: <Box display={'flex'} alignItems={'center'}>
                    <Typography mr={2} variant={"body1"}>
                        {fe.type.typeLabel}
                    </Typography>
                </Box>
            },
            values_count:{
              label:  <Box display={'flex'} alignItems={'center'}>
                  <Typography mr={2} variant={"body1"}>
                      {fe.values.length}
                  </Typography>
              </Box>
            },
            created_at: {
                label: fe.createdAt.date,
            },
            created_by: {
                label: fe.createdAt.by.name,
                link: fe.createdAt.by.link,
            },
            updated_at: {
                label: fe.updatedAt.date,
            },
            updated_by: {
                label: fe.updatedAt.by.name,
                link: fe.updatedAt.by.link,
            },
        } : {}
    ));


    //endregion renderData
    return (

        <Box my={2} px={2}>
            {(!error) ?
                <React.Fragment>
                    <Table headers={headers}
                           cookieKey={CookieKey}
                           title={lang.get("features")}
                           data={tableData}
                           filters={filtersState.data}
                           onFilterChange={handleFilterChange}
                           loading={false}
                           activePage={pg.page}
                           rowsPerPage={pg.rowPerPage}
                           lastPage={data ? data.pagination.lastPage : 0}
                           rowCount={data ? data.pagination.count : 0}
                           orderType={state.order}
                           activeBox={state.activeBox}
                           addNewButton={{
                               label: lang.get('add_new_features'),
                               link: rout.Feature.Single.createNewProduct,
                           }}
                           onActivePageChange={(page) => {
                               setState({
                                   ...state,
                                   pagination: {
                                       ...state.pagination,
                                       page: page
                                   }
                               })
                               // requestData({page: e})
                           }}
                           onRowPerPageChange={(rowPerPage) => {
                               storage.PageSetting.setRowPerPage(CookieKey, {rowPerPage: rowPerPage});
                               setState({
                                   ...state,
                                   pagination: {
                                       ...state.pagination,
                                       rowPerPage: rowPerPage
                                   }
                               })
                           }}
                           onApplyFilterClick={handleApplyFilter}
                           onResetFilterClick={handleResetFilter}
                           onChangeOrder={handleChangeOrder}
                           onChangeBox={handleChangeBox}/>
                </React.Fragment> :
                <Box>
                    <ComponentError
                        statusCode={(_.isObject(error) && error.request) ? error.request.status : undefined}
                        tryAgainFun={() => mutate(apiKey)}/>
                </Box>}
        </Box>
    )
}
