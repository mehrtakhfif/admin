import React, {Fragment, useEffect, useRef, useState} from "react";
import Box from "@material-ui/core/Box";
import {Card, Dialog, useTheme} from "@material-ui/core";
import {createMultiLanguage, defaultIcon} from "../../controller/converter";
import ControllerProduct from "../../controller/ControllerProduct";
import Backdrop from "@material-ui/core/Backdrop/Backdrop";
import error from "../../language/fa/error";
import PleaseWait from "../../components/base/loading/PleaseWait";
import ComponentError from "../../components/base/ComponentError";
import BaseButton from "../../components/base/button/BaseButton";
import Typography from "../../components/base/Typography";
import TextFieldMultiLanguageContainer from "../../components/base/textField/TextFieldMultiLanguageContainer";
import {errorList, notValidErrorTextField} from "../../components/base/textField/TextFieldContainer";
import {lang, siteLang} from "../../repository";
import TextField from "../../components/base/textField/TextField";
import FormControl from "../../components/base/formController/FormController";
import _ from "lodash";
import {UtilsData, UtilsStyle} from "../../utils/Utils";
import Tooltip from "../../components/base/Tooltip";
import {DeleteOutline} from "@material-ui/icons";
import IconButton from "@material-ui/core/IconButton";
import StickyBox from "react-sticky-box";
import SuccessButton from "../../components/base/button/buttonVariant/SuccessButton";
import SaveDialog from "../../components/dialog/SaveDialog";
import {orange} from "@material-ui/core/colors";
import Button from "@material-ui/core/Button";
import {useSnackbar} from "notistack";
import Transition from "../../components/base/Transition";
import SelectBox from "../../components/SelectBox";
import {featureType} from "../../controller/type";
import {gcLog} from "../../utils/ObjectUtils";
import SelectFeaturesGroup from "../featuresGroup/SelectFeaturesGroup";
import ButtonBase from "@material-ui/core/ButtonBase";

export default function ({open, onClose, ...props}) {

    const theme = useTheme();
    const ref = useRef();
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();
    const fId = props.match.params.fId;
    const [state, setState] = useState({
        error: false,
        loading: false,
        saveDialog: false,
        closeDialog: false,
    });
    const [openGroup, setOpenGroup] = useState(false);
    const [group, setGroup] = useState(undefined);
    const [data, setData] = useState(false);
    const [box, setBox] = useState()

    useEffect(() => {
        if (!fId) {
            setData(initData());
            return;
        }
        getData()
    }, [fId]);


    function initData() {
        return {
            name: createMultiLanguage(),
            type: null,
            icon: defaultIcon,
            value: [
                getNewValue(1),
                getNewValue(2)
            ]
        }
    }

    function getData() {
        setState({
            ...state,
            error: false,
            loading: true
        });
        ControllerProduct.Features.getSingle({fId: fId}).then((res) => {
            setData(res.data);
            setState({
                ...state,
                error: false,
                loading: false
            });
        }).catch((e) => {
            setState({
                ...state,
                error: true,
                loading: false
            })
        })
    }

    function getNewValue(id) {
        return {
            id: id || UtilsData.createId(data.value || []),
            name: createMultiLanguage()
        }
    }

    function handleSaveFeature() {
        const props = getRequestProps();
        return;
        if (!props)
            return;
        const {boxId, type, name, icon, value} = props;
        console.log("fsafasfasfasfasf", props)
        ControllerProduct.Features.save({
            fId: fId,
            boxId: boxId,
            type: type,
            name: name,
            iconName: icon,
            value: value
        }).then((res) => {
        })
    }

    function getRequestProps() {
        let errorText = "";
        if (fId && !data.type) {
            errorText = "نوع فیچر را انتخاب کنید."
        }
        if (!errorText && ref.current.hasError()) {
            errorText = "فرم مشکل دارد";
            try {
                const el = ref.current.getErrorElement();
                const t = el.getAttribute(notValidErrorTextField);
                if (t) {
                    errorText = t
                }
                el.focus();
            } catch (e) {
            }
        }
        if (errorText) {
            reqCancel(errorText);
            return;
        }
        const props = ref.current.serialize();

        const value = [];
        _.forEach(props.value, (v) => {
            const props = v;
            delete props.serializeKey;
            value.push(props)
        });

        return {
            type: fId ? undefined : data.type.key,
            name: props.name,
            value: value
        };
    }

    function reqCancel(text = null) {
        if (text)
            enqueueSnackbar(text,
                {
                    variant: "error",
                    action: (key) => (
                        <Fragment>
                            <Button onClick={() => {
                                closeSnackbar(key)
                            }}>
                                {lang.get('close')}
                            </Button>
                        </Fragment>
                    )
                });

        setState({
            loading: false
        })
    }

    const featureItem = (data && data.type && !data.type.isBool) &&
        <BaseButton
            onClick={() => {
                setData({
                    ...data,
                    value: [
                        ...data.value,
                        getNewValue()
                    ]
                })
            }}
            style={{
                color: '#fff',
                backgroundColor: theme.palette.primary.main
            }}>
            افزودن فیچر جدید
        </BaseButton>;


    const el = (
        <Box py={2} px={2} display={'flex'} width={1}>
            {
                !state.error ?
                    data ?
                        (!fId && !data.box) ?
                            <SelectBox onBoxSelected={(b) => {
                                setData({
                                    ...data,
                                    box: b
                                })
                            }}/> :
                            !data.type ?
                                <SelectType
                                    onSelect={(type) => {
                                        setData({
                                            ...data,
                                            type: type
                                        })
                                    }}/> :
                                <Box display={'flex'} width={1}>
                                    <Box displya={'flex'} minWidth={'80%'}>
                                        <FormControl name={"featureSingle"} innerref={ref} display={'flex'} width={1}
                                                     pb={2}
                                                     flexDirection={'column'}>
                                            <Typography variant={'body1'} my={2}>
                                                نوع فیچر: {data.type.typeLabel}
                                            </Typography>
                                            <Box width={1} display={'flex'} alignItems={'center'}>
                                                <Box mr={2} minWidth={200} px={2}>
                                                    {
                                                        group ?
                                                            <Box display={'flex'}>
                                                                <ButtonBase
                                                                    onClick={() => {
                                                                        setOpenGroup(true)
                                                                    }}
                                                                    style={{
                                                                        ...UtilsStyle.borderRadius(5)
                                                                    }}>
                                                                    <Box display={'flex'} px={1} alignItems={'center'}>
                                                                        <Typography variant={'body1'} pl={1}>
                                                                            گروه:
                                                                        </Typography>
                                                                        <Typography variant={'h6'}>
                                                                            {group.name[siteLang]}
                                                                        </Typography>
                                                                    </Box>
                                                                </ButtonBase>
                                                                <Box pl={0.5}>
                                                                    <IconButton
                                                                        onClick={() => setGroup(undefined)}>
                                                                        <DeleteOutline/>
                                                                    </IconButton>
                                                                </Box>
                                                            </Box> :
                                                            <SuccessButton
                                                                variant={"outlined"}
                                                                onClick={() => {
                                                                    setOpenGroup(true)
                                                                }}>
                                                                انتخاب گروه
                                                            </SuccessButton>
                                                    }
                                                </Box>
                                                <Box flex={1}>
                                                    <TextFieldMultiLanguageContainer
                                                        name={"name"}
                                                        render={(ref, {lang: inputLang, name: inputName, initialize, valid, errorIndex, setValue, inputProps, style, props}) => {
                                                            return (
                                                                <TextField
                                                                    {...props}
                                                                    error={!valid}
                                                                    variant="outlined"
                                                                    name={inputName}
                                                                    fullWidth
                                                                    defaultValue={data.name[inputLang.key]}
                                                                    helperText={errorList[errorIndex]}
                                                                    inputRef={ref}
                                                                    required={inputLang.key === siteLang}
                                                                    label="نام"
                                                                    inputProps={{
                                                                        ...inputProps
                                                                    }}/>
                                                            )
                                                        }}/>
                                                </Box>
                                            </Box>
                                            <Box display={'flex'} mt={3} p={2} flexDirection={'column'} component={Card}
                                                 width={1}>
                                                <Box mb={1}>
                                                    {featureItem}
                                                </Box>
                                                <Box display={'flex'} flexDirection={'column'}>
                                                    {
                                                       false&&data.value.map((v, i) => (
                                                            <Box key={v.id} my={2}>
                                                                <TextFieldMultiLanguageContainer
                                                                    array={"value"}
                                                                    name={v.id}
                                                                    render={(ref, {lang: inputLang, name: inputName, initialize, valid, errorIndex, setValue, inputProps, style, props}) => {
                                                                        return (
                                                                            <TextField
                                                                                {...props}
                                                                                error={!valid}
                                                                                variant="outlined"
                                                                                name={inputName}
                                                                                fullWidth
                                                                                endAdornment={(
                                                                                    (i > 1) &&
                                                                                    <Tooltip title='حذف'>
                                                                                        <IconButton
                                                                                            onClick={() => {
                                                                                                const val = data.value;
                                                                                                _.remove(val, va => {
                                                                                                    return va.id === v.id
                                                                                                });
                                                                                                setData({
                                                                                                    ...data,
                                                                                                    value: val
                                                                                                })
                                                                                            }}>
                                                                                            <DeleteOutline/>
                                                                                        </IconButton>
                                                                                    </Tooltip>
                                                                                )}
                                                                                required={i <= 1 && inputLang.key === siteLang}
                                                                                defaultValue={v.name[inputLang.key]}
                                                                                helperText={errorList[errorIndex]}
                                                                                inputRef={ref}
                                                                                label="نام"
                                                                                inputProps={{
                                                                                    ...inputProps
                                                                                }}/>
                                                                        )
                                                                    }}/>
                                                            </Box>
                                                        ))
                                                    }
                                                </Box>
                                            </Box>
                                        </FormControl>
                                    </Box>
                                    <Box width={'20%'} pl={2}>
                                        <StickyBox offsetTop={180} offsetBottom={20}>
                                            <Box display={'flex'} component={Card} py={2} width={1}
                                                 flexDirection={'column'}>
                                                <Box display={'flex'} justifyContent={'center'} mb={1}>
                                                    {featureItem}
                                                </Box>
                                                <Box display={'flex'} alignItems={'center'}
                                                     justifyContent={'center'} mt={1}>
                                                    <SuccessButton
                                                        loading={state.loading}
                                                        onClick={() => {
                                                            setTimeout(() => {
                                                                if (!getRequestProps())
                                                                    return;
                                                                setState({...state, saveDialog: true})
                                                            }, 1200)
                                                        }}>
                                                        {lang.get('save')}
                                                    </SuccessButton>
                                                    {onClose &&
                                                    <BaseButton
                                                        disabled={state.loading}
                                                        onClick={() => setState({...state, closeDialog: true})}
                                                        variant={"text"}
                                                        style={{marginRight: theme.spacing(1)}}>
                                                        {lang.get('close')}
                                                    </BaseButton>
                                                    }
                                                </Box>
                                            </Box>
                                        </StickyBox>
                                    </Box>
                                    <SaveDialog open={state.saveDialog}
                                                text={(
                                                    <Box display={'flex'} flexDirection={'column'} px={3}>
                                                        <Typography variant={'h6'} fontWeight={400} pb={2}>
                                                            ذخیره فیچر
                                                        </Typography>
                                                        <Typography variant={'body1'}>
                                                            آیا از ذخیره این فیچر اطمینان دارید؟
                                                        </Typography>
                                                        {
                                                            fId &&
                                                            <Typography variant={"body2"} color={orange[300]}>
                                                                توجه: به هیچ وجه توانایی مرتب‌سازی و جابه‌جایی آیتم های
                                                                فیچر در آینده را ندارید.
                                                                <br/>
                                                                در صورت نداشتن اطمینان بازبینی کنید.
                                                            </Typography>
                                                        }
                                                        <Typography variant={'caption'}
                                                                    color={theme.palette.text.secondary}
                                                                    pt={0.5}>
                                <span style={{color: orange[300], fontWeight: 600, paddingLeft: theme.spacing(0.3)}}>
                                    توجه:
                                </span>
                                                            در صورت ذخیره، اطلاعات تغییریافته قابل بازگشت نمیباشد.
                                                        </Typography>
                                                    </Box>)}
                                                onSubmit={() => {
                                                    setState({...state, saveDialog: false});
                                                    handleSaveFeature();
                                                }}
                                                onCancel={() => setState({...state, saveDialog: false})}/>
                                    <SaveDialog open={state.closeDialog}
                                                submitText={"بله"}
                                                cancelText={"خیر"}
                                                text={<Typography variant={"body1"}>
                                                    آیا از لغو تغییرات اطمینان دارید؟
                                                </Typography>}
                                                onSubmit={() => {
                                                    setState({...state, closeDialog: false});
                                                    onClose();
                                                }
                                                }
                                                onCancel={() => {
                                                    setState({...state, closeDialog: false})
                                                }}/>
                                    <SelectFeaturesGroup
                                        boxId={box ? box.id : undefined}
                                        open={openGroup}
                                        onSelect={(fe) => {
                                            setOpenGroup(false)
                                            if (!fe)
                                                return;
                                            setGroup(fe)
                                        }}
                                        onBoxSelect={(box) => setBox(box)}/>
                                </Box> :
                        <PleaseWait/> :
                    <ComponentError
                        statusCode={(_.isObject(error) && error.request) ? error.request.status : undefined}
                        tryAgainFun={getData}/>
            }
            <Backdrop open={state.loading} style={{zIndex: 99999999}}/>
        </Box>
    );

    return (
        <React.Fragment>
            {
                (_.isBoolean(open) && onClose) ?
                    <Dialog fullScreen={true} open={open} ransitionComponent={Transition}>
                        {el}
                    </Dialog> :
                    el
            }
        </React.Fragment>
    )
}

function SelectType({onSelect, ...props}) {
    const theme = useTheme();

    return (
        <Box display={'flex'} flexDirection={'column'}>
            <Typography variant={'h6'} fontWeight={600} py={1}>
                انتخاب نوع فیچر:
            </Typography>
            <Box display={'flex'} flexWrap={'wrap'}>
                {Object.keys(featureType).map((key, index) => {
                    const f = featureType[key];
                    return (
                        <Box m={1} key={f.key}>
                            <BaseButton
                                onClick={() => onSelect(f)}
                                style={{
                                    color: '#fff',
                                    backgroundColor: theme.palette.primary.main
                                }}>
                                {f.typeLabel}
                            </BaseButton>
                        </Box>
                    )
                })}
            </Box>
        </Box>
    )
}
