import React, {useEffect, useState} from "react";
import BaseDialog from "../../components/base/BaseDialog";
import Box from "@material-ui/core/Box";
import {gcLog} from "../../utils/ObjectUtils";
import ControllerProduct from "../../controller/ControllerProduct";
import Backdrop from "@material-ui/core/Backdrop";
import CircularProgress from "@material-ui/core/CircularProgress";
import {cyan, grey} from "@material-ui/core/colors";
import Typography from "../../components/base/Typography";
import {siteLang} from "../../repository";
import DraggableList from "../../components/base/draggableList/DraggableList";
import {Menu, MenuOpen} from "@material-ui/icons";
import Img from "../../components/base/img/Img";
import {UtilsStyle} from "../../utils/Utils";
import SuccessButton from "../../components/base/button/buttonVariant/SuccessButton";
import BaseButton from "../../components/base/button/BaseButton";
import {useTheme} from "@material-ui/core";
import _ from "lodash"
import ComponentError from "../../components/base/ComponentError";

export default function ({open, featureId, onClose, ...props}) {
    const theme = useTheme()
    const [data, setData] = useState()
    const [error, setError] = useState(false)
    const [loading, setLoading] = useState(false)


    useEffect(() => {
        getData()
    }, [featureId, open])

    function getData() {
        if (!open)
            return
        setLoading(true)
        setError(false)
        setData(undefined)
        ControllerProduct.Features.getSingle({fId: featureId}).then((res) => {
            setData({
                name: res.data.name,
                values: res.data.values
            });
        }).catch((e) => {
            setError(true)
        }).finally(() => {
            setLoading(false)
        })
    }

    function handleSave() {
        const ids = [];
        _.forEach(data.values, v => {
            ids.push(v.id)
        })
        setLoading(true)
        ControllerProduct.Features.sort({ids: ids}).then(res => {
            onClose(true)
        }).finally(() => {
            setLoading(false)
        })
    }

    return (
        <BaseDialog open={Boolean(open && featureId)} onClose={onClose}>
            {
                error ?
                    <ComponentError
                        tryAgainFun={getData}/> :
                    data ?
                        <Box display={'flex'} flexDirection={'column'} p={3}>
                            <Typography pb={2} variant={"h6"}>
                                {data.name[siteLang]}
                            </Typography>
                            <Box>
                                <DraggableList
                                    items={data.values}
                                    onItemsChange={(items) => {
                                        setData({
                                            ...data,
                                            values: items
                                        })
                                    }}
                                    rootStyle={(dragging) => {
                                        return {
                                            backgroundColor: dragging ? null : grey[200],
                                            ...UtilsStyle.transition(500)
                                        }
                                    }}
                                    render={(item, props, {isDraggingItem, ...p}) => (
                                        <Box key={item.id}
                                             display={'flex'}
                                             width={1}
                                             {...props}>
                                            <Box display={'flex'} alignItems={'center'}
                                                 py={1}>
                                                <Box px={2} py={1}>
                                                    {isDraggingItem ?
                                                        <MenuOpen/> :
                                                        <Menu/>
                                                    }
                                                </Box>
                                                <Typography pr={2} pl={1} variant={'body1'} fontWeight={600}
                                                            style={{
                                                                justifyContent: 'center',
                                                            }}>
                                                    {item.name[siteLang]}
                                                </Typography>
                                            </Box>
                                        </Box>
                                    )}/>
                            </Box>
                            <Box display={'flex'} pt={2}>
                                <SuccessButton variant={"outlined"}
                                               onClick={handleSave}>
                                    ذخیره
                                </SuccessButton>
                                <Box px={2}>
                                    <BaseButton
                                        variant={"outlined"}
                                        onClick={() => {
                                            onClose(false)
                                        }}
                                        style={{
                                            borderColor: theme.palette.error.main
                                        }}>
                                        لغو
                                    </BaseButton>
                                </Box>
                            </Box>
                        </Box> :
                        <React.Fragment/>
            }
            <Backdrop open={Boolean(open && loading)}
                      style={{
                          zIndex: '9999'
                      }}>
                <CircularProgress color="inherit"
                                  style={{
                                      color: cyan[300],
                                      width: 100,
                                      height: 100
                                  }}/>
            </Backdrop>
        </BaseDialog>
    )

}
