import React, {useEffect, useState} from "react";
import BaseDialog from "../../components/base/BaseDialog";
import Box from "@material-ui/core/Box";
import DefaultTextField from "../../components/base/textField/DefaultTextField";
import ControllerProduct from "../../controller/ControllerProduct";
import useSWR, {mutate} from "swr";
import {gcLog} from "../../utils/ObjectUtils";
import PleaseWait from "../../components/base/loading/PleaseWait";
import ComponentError from "../../components/base/ComponentError";
import {Card, Typography, useTheme} from "@material-ui/core";
import {siteLang} from "../../repository";
import BaseButton from "../../components/base/button/BaseButton";
import IconButton from "@material-ui/core/IconButton";
import {DeleteOutline, DeleteOutlineOutlined, ExpandLessOutlined, ExpandMoreOutlined} from "@material-ui/icons";
import Select from "@material-ui/core/Select";
import Checkbox from "@material-ui/core/Checkbox";
import Collapse from "@material-ui/core/Collapse";
import _ from "lodash"
import ButtonBase from "@material-ui/core/ButtonBase";
import SuccessButton from "../../components/base/button/buttonVariant/SuccessButton";
import {grey} from "@material-ui/core/colors";
import {UtilsStyle} from "../../utils/Utils";


export default function SelectFeature({open, multiSelect = false, onSelect, ...props}) {
    const theme = useTheme();
    const [search, setSearch] = useState("");

    const d = ControllerProduct.Features.search({search})
    const {data, error} = useSWR(...d);

    const [selected, setSelected] = useState([]);

    useEffect(() => {
        setSelected([])
    }, [open])



    return (
        <BaseDialog open={open} maxWidth={"lg"} onClose={() => onSelect()}>
            <Box p={2} width={1}>
                <Box display={'flex'}>
                    <Box minWidth={1 / 3}>
                        <DefaultTextField
                            name={"search"}
                            label={"جستجو"}
                            variant={'outlined'}
                            onChange={(val) => {
                                setSearch(val)
                            }}/>
                    </Box>
                    {
                        multiSelect &&
                        <React.Fragment>
                            <Box px={2} display={'flex'} flexDirection={'column'}>
                                <Typography variant={"caption"} justifyContent={'center'}
                                            style={{
                                                textAlign: 'center'
                                            }}>
                                    تعداد انتخاب شده ها:
                                </Typography>
                                <Typography pt={1} variant={"h6"} justifyContent={'center'}
                                            style={{
                                                textAlign: 'center'
                                            }}>
                                    {selected.length}
                                </Typography>
                            </Box>
                            <Box px={2}>
                                <SuccessButton
                                    onClick={() => {
                                        onSelect(selected)
                                    }}>
                                    انتخاب
                                </SuccessButton>
                            </Box>
                            <Box px={2}>
                                <BaseButton
                                    variant={"outlined"}
                                    onClick={() => {
                                        onSelect()
                                    }}
                                    style={{
                                        borderColor: theme.palette.error.main
                                    }}>
                                    بسته
                                </BaseButton>
                            </Box>
                        </React.Fragment>
                    }
                </Box>
                <Box width={1} py={2}
                     maxHeight={400} display={'flex'} flexWrap={'wrap'}
                     style={{
                         overflowY: 'scroll'
                     }}>
                    {
                        !error ?
                            data ?
                                <React.Fragment>
                                    {
                                        data.data.map(d => (
                                            <Item
                                                kye={d.id}
                                                checked={_.findIndex(selected, (it) => it === d.id) !== -1}
                                                multiSelect={multiSelect}
                                                item={d}
                                                onSelect={() => {
                                                    if (!multiSelect) {
                                                        onSelect(d)
                                                        return;
                                                    }
                                                    const newList = selected;
                                                    const index = _.findIndex(newList, (it) => it === d.id)
                                                    if (index === -1) {
                                                        setSelected([...newList, d.id])
                                                        return
                                                    }
                                                    newList.splice(index, 1)
                                                    setSelected([...newList])
                                                }}/>
                                        ))
                                    }
                                </React.Fragment> :
                                <PleaseWait/> :
                            <ComponentError tryAgainFun={() => mutate(d[0])}/>
                    }
                </Box>
            </Box>
        </BaseDialog>
    )
}


export function Item({item, checked, multiSelect, onSelect, onRemove, ...props}) {
    const theme = useTheme();
    const [open, setOpen] = useState(false);

    const el = (
        <Box component={Card} width={1} display={'flex'} flexDirection={'column'} p={1}>
            <Box display={'flex'}>
                <Box display={'flex'} flexDirection={'column'} flex={1}>
                    <Typography variant={"h6"}>
                        {item.name[siteLang] ? item.name[siteLang] : "-----"}
                    </Typography>
                    <Typography variant={"caption"}>
                        نوع:
                        {item.type.typeLabel ? item.type.typeLabel : "----"}
                    </Typography>
                </Box>
                <Box minWidth={'20%'}
                     display={'flex'}
                     flexDirection={'column'}
                     justifyContent={'center'}
                     px={1}>
                    {
                        (multiSelect) &&
                        <Box pb={0.5}
                             onClick={(e) => {
                                 e.stopPropagation()
                             }}>
                            <Checkbox
                                size={"small"}
                                checked={checked}
                                onChange={(e) => {
                                    onSelect(item)
                                }}
                                inputProps={{'aria-label': 'primary checkbox'}}
                            />
                        </Box>
                    }
                    {
                        onRemove &&
                        <Box display={'none'} justifyContent={'center'}>
                            <IconButton
                                size={"small"}
                                onClick={onRemove}>
                                <DeleteOutline fontSize={"small"} style={{color: theme.palette.error.main}}/>
                            </IconButton>
                        </Box>
                    }
                    {
                        !_.isEmpty(item.values) &&
                        <Box display={'flex'} justifyContent={'center'}>
                            <IconButton
                                size={"small"}
                                onClick={(e) => {
                                    e.stopPropagation()
                                    setOpen(!open)
                                }}>
                                {
                                    open ?
                                        <ExpandLessOutlined fontSize={"small"}/> :
                                        <ExpandMoreOutlined fontSize={"small"}/>

                                }
                            </IconButton>
                        </Box>
                    }
                </Box>
            </Box>
            {
                !_.isEmpty(item.values) &&
                <Collapse in={open}>
                    <Box display={'flex'} flexWrap={'wrap'} pt={1}>
                        {
                            item.values.map(it => (
                                <Box key={it.id} p={0.5}>
                                    <Typography variant={"body2"}
                                                style={{
                                                    padding: theme.spacing(0.5),
                                                    border: `1px solid ${grey[400]}`,
                                                    ...UtilsStyle.borderRadius(5)
                                                }}>
                                        {it.name != null ? it.name[siteLang] : "--"}
                                    </Typography>
                                </Box>
                            ))
                        }
                    </Box>
                </Collapse>
            }
        </Box>
    )
    return (
        <Box p={1} width={1 / 4}>
            {
                onSelect ?
                    <ButtonBase onClick={(e) => {
                        onSelect(item)
                        e.stopPropagation()
                    }}
                                style={{width: '100%'}}>
                        {el}
                    </ButtonBase> :
                    el
            }
        </Box>
    )
}
