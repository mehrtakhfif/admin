import React, {useEffect, useState} from "react";
import Box from "@material-ui/core/Box";
import {Dialog, useTheme} from "@material-ui/core";
import Transition from "../../../../components/base/Transition";
import SelectBox from "../../../../components/SelectBox";
import useSWR, {mutate} from "swr";
import ControllerBox from "../../../../controller/ControllerBox";
import {siteLang} from "../../../../repository";
import BaseButton from "../../../../components/base/button/BaseButton";
import Typography from "../../../../components/base/Typography";
import {
    Add, DeleteOutlined,
    EditOutlined,
    KeyboardArrowDown,
    KeyboardArrowUp, RemoveOutlined,
    ThumbUpOutlined,
    VisibilityOutlined
} from "@material-ui/icons";
import _ from "lodash";
import PleaseWait from "../../../../components/base/loading/PleaseWait";
import ComponentError from "../../../../components/base/ComponentError";
import CreateTag from "./CreateTag";
import ControllerProduct from "../../../../controller/ControllerProduct";
import CircularProgress from "@material-ui/core/CircularProgress";
import {cyan} from "@material-ui/core/colors";
import Backdrop from "@material-ui/core/Backdrop/Backdrop";
import IconButton from "@material-ui/core/IconButton";
import Collapse from "@material-ui/core/Collapse";
import Chip from "@material-ui/core/Chip";
import ListIsEmpty from "../../../../components/base/ListIsEmpty";
import Skeleton from "@material-ui/lab/Skeleton";
import {gcLog} from "../../../../utils/ObjectUtils";
import ErrorBoundary from "../../../../components/base/ErrorBoundary";
import Checkbox from "@material-ui/core/Checkbox";
import SuccessButton from "../../../../components/base/button/buttonVariant/SuccessButton";

export default function ({open, boxId: bId, editable, selectable, multiSelect, onClose}) {

    const [boxId, setBoxId] = useState(bId ? bId : undefined);

    useEffect(() => {
        if (!bId && !boxId)
            return;
        setBoxId(bId)
    }, [bId, open]);

    useEffect(() => {
        if (!open)
            setBoxId(undefined)
    }, [open]);

    return (
        <Dialog open={open} maxWidth={"xl"} fullWidth={true}
                transitionDuration={Transition}
                onClose={() => onClose()}>
            {boxId ?
                <TG boxId={boxId} editable={editable} selectable={selectable}
                    multiSelect={multiSelect}
                    onClose={onClose}/> :
                <SelectBox onBoxSelected={(box) => {
                    setBoxId(box.id)
                }}/>}
        </Dialog>
    )
}


function TG({boxId, editable, selectable, multiSelect, onClose, ...props}) {
    const theme = useTheme();
    const apiKey = `setting-${boxId}`;
    const [state, setState] = useState({
        open: false,
        loading: false
    })
    const [selected, setSelected] = useState([]);
    const {data, error} = useSWR(apiKey, () => {
        return ControllerBox.TagGroup.get({boxId: boxId}).then(res => {
            return res.data.data
        })
    });

    useEffect(() => {
        try {

        } catch (e) {

        }
    }, [data])

    function handleSave() {
        const res = [];
        _.forEach(selected, id => {
            const item = _.find(data, (d) => d.id === id)
            if (!item)
                return;
            res.push(item)
        })
        onClose(res)
    }

    return (
        <Box>
            {!error ?
                _.isArray(data) ?
                    <Box display={'flex'} flexDirection={'column'}>
                        {
                            selectable &&
                            <Box display={'flex'} flexDirection={"row-reverse"}
                                 style={{
                                     position: 'absolute',
                                     top: 10,
                                     left: 30,
                                     zIndex:999
                                 }}>
                                <SuccessButton
                                    size={"large"}
                                    disabled={_.isEmpty(selected)}
                                    onClick={handleSave}>
                                    ذخیره
                                    {
                                        !_.isEmpty(selected) ?
                                            <Typography variant={"h6"} px={1} fontWeight={600} color={"#fff"}>
                                                {selected.length}
                                            </Typography>:0
                                    }
                                    گروه
                                </SuccessButton>
                            </Box>
                        }
                        <Box display={'flex'} p={2}
                             boxShadow={3}
                             pt={selectable ? 7 : 2}
                             flexWrap={'wrap'}
                             style={{
                                 backgroundColor: theme.palette.background.paper,
                                 position: 'sticky',
                                 top: 0,
                                 zIndex: 10
                             }}>
                            {editable &&
                            <Box width={1 / 4} p={2}>
                                <BaseButton
                                    variant={"outlined"}
                                    onClick={() => {
                                        setState({
                                            ...state,
                                            open: true
                                        })
                                    }}>
                                    <Typography py={1} alignItems={"center"} variant={"h6"}>
                                        <Add style={{
                                            marginLeft: theme.spacing(1)
                                        }}/>
                                        افزودن گروه تگ جدید
                                    </Typography>
                                </BaseButton>
                            </Box>}
                            {!_.isEmpty(data) ? data.map(d => (
                                    <TagItem
                                        item={d}
                                        onSelect={selectable ? (check) => {
                                            if (check) {
                                                setSelected([
                                                    ...selected,
                                                    d.id
                                                ])
                                                return
                                            }
                                            const newList = _.cloneDeep(selected);
                                            _.remove(newList, (id) => id === d.id)
                                            setSelected(newList)
                                        } : undefined}
                                        selected={_.findIndex(selected, id => id === d.id) !== -1}
                                        editable={editable}
                                        multiSelect={multiSelect}
                                        loading={state.loading}
                                        setState={(newState) => setState({
                                            ...state,
                                            ...newState
                                        })}
                                        onClose={onClose}/>
                                )) :
                                editable ? null : <ListIsEmpty/>}
                            {
                                Boolean(state.open) &&
                                <ErrorBoundary>
                                    <CreateTag
                                        boxId={boxId}
                                        open={Boolean(state.open)}
                                        tgId={_.isObject(state.open) ? state.open.id : false}
                                        onClose={(save) => {
                                            try {
                                                mutate(apiKey)
                                                setState({
                                                    ...state,
                                                    open: false
                                                })
                                            } catch (e) {
                                            }
                                        }}/>
                                </ErrorBoundary>
                            }
                        </Box>
                    </Box> :
                    <PleaseWait/> :
                <ComponentError
                    statusCode={(_.isObject(error) && error.request) ? error.request.status : undefined}
                    tryAgainFun={() => {
                        mutate(apiKey)
                    }}/>}
            <Backdrop open={state.loading}
                      style={{
                          zIndex: '999999'
                      }}>
                <CircularProgress color="inherit"
                                  style={{
                                      color: cyan[300],
                                      width: 100,
                                      height: 100
                                  }}/>
            </Backdrop>
        </Box>)
}


export function TagItem({item: d, loading,product=false, setState, editable, multiSelect, selected, onSelect, onRemoved, onClose, ...props}) {
    const theme = useTheme();
    const [setting, setSetting] = useState({
        showTags: false,
        req: false
    })

    const apiKey = `tag-${d.id}-${setting.req}`
    const {data, error} = useSWR(apiKey, () => {
        if (!setting.req) {
            return undefined
        }
        if (product){
            return ControllerBox.TagGroup.getTags({id:d.id}).then(res=>res.data.tags)
        }
        return ControllerProduct.Product.Tags.getList({tags: d.tags}).then(res => res.data)
    })

    return (
        <Box width={1 / 4} key={d.id} p={2} {...props}>
            <BaseButton
                variant={"outlined"}
                onClick={() => {
                    if (loading)
                        return;
                    if (onSelect) {
                        onSelect(!selected)
                        return;
                    }
                    if (product)
                        return;
                    setState({
                        loading: true
                    })
                    if (editable) {
                        setState({
                            open: d
                        })
                        return;
                    }
                    ControllerProduct.Product.Tags.getList({tags: d.tags}).then(res => {
                        setState({
                            loading: false
                        })
                        onClose({
                            ...d,
                            tags: res.data
                        })
                    })

                }}
                style={{
                    width: "100%",
                    position: 'relative'
                }}>
                <Box display={'flex'} flexDirection={'column'} width={1}>
                    <Typography py={1} alignItems={"center"} variant={"h6"}>
                        {editable ?
                            <EditOutlined
                                style={{
                                    marginLeft: theme.spacing(1)
                                }}/> :
                            <ThumbUpOutlined
                                style={{
                                    marginLeft: theme.spacing(1)
                                }}/>}
                        {d.name[siteLang]}
                    </Typography>
                    <Box display={'flex'} width={'100%'} alignItems={'center'}>
                        <Box flex={1} display={'flex'} alignItems={'center'}>
                            {product?  <Typography variant={"body1"}>
                                مشاهده تگ ها
                            </Typography>:<div>

                            <Typography variant={"body1"}>
                                تعداد تگ‌ها:
                            </Typography>
                            <Typography px={1} variant={"h6"}>
                                {_.isArray(d.tags) ? d.tags.length : 0}
                            </Typography>
                            </div>}
                        </Box>
                        <IconButton
                            onClick={(e) => {
                                e.stopPropagation();
                                setSetting({
                                    ...setting,
                                    req: true,
                                    showTags: !setting.showTags
                                })
                            }}>
                            {setting.showTags ? <KeyboardArrowUp/> : <KeyboardArrowDown/>}
                        </IconButton>
                        {onRemoved &&
                        <IconButton
                            onClick={(e) => {
                                e.stopPropagation();
                                onRemoved(e)
                            }}>
                            <DeleteOutlined/>
                        </IconButton>}
                    </Box>
                    <Collapse in={setting.showTags}>
                        <Box display={'flex'} width={1} flexWrap={'wrap'}>
                            {!error ?
                                data ? data.map(t => (
                                        <Box p={0.25}>
                                            <Chip
                                                avatar={t.show ? <VisibilityOutlined/> : undefined}
                                                label={t.name[siteLang]}
                                                variant="outlined"/>
                                        </Box>
                                    )) :
                                    <Box width={1} height={100}>
                                        <Skeleton variant={"rect"} style={{
                                            width: '100%',
                                            height: '100%',
                                        }}/>
                                    </Box> :
                                <ComponentError
                                    statusCode={(_.isObject(error) && error.request) ? error.request.status : undefined}
                                    tryAgainFun={(e) => {
                                        e.stopPropagation()
                                        mutate(apiKey)
                                    }}/>}
                        </Box>
                    </Collapse>
                </Box>
                {
                    onSelect &&
                    <Checkbox
                        checked={selected}
                        onChange={(e) => {
                            onSelect(e.target.checked)
                            e.stopPropagation()
                        }}
                        style={{
                            position: 'absolute',
                            top: 0,
                            left: 0
                        }}
                    />}
            </BaseButton>
        </Box>
    )
}
