import React, {Fragment, useEffect, useRef, useState} from "react";
import Dialog from "@material-ui/core/Dialog";
import Transition from "../../../../components/base/Transition";
import Box from "@material-ui/core/Box";
import StickyBox from "react-sticky-box";
import {lang, siteLang} from "../../../../repository";
import Typography from "../../../../components/base/Typography";
import BaseButton from "../../../../components/base/button/BaseButton";
import {makeStyles, useTheme} from "@material-ui/core";
import {createName} from "../../../../components/base/textField/TextFieldMultiLanguageContainer";
import Card from "@material-ui/core/Card";
import _ from 'lodash';
import TextField from "../../../../components/base/textField/TextField";
import TextFieldContainer, {textFieldNewValue} from "../../../../components/base/textField/TextFieldContainer";
import {createMultiLanguage} from "../../../../controller/converter";
import SuccessButton from "../../../../components/base/button/buttonVariant/SuccessButton";
import SaveDialog from "../../../../components/dialog/SaveDialog";
import {cyan, green, grey, orange} from "@material-ui/core/colors";
import FormController from "../../../../components/base/formController/FormController";
import Button from "@material-ui/core/Button";
import {useSnackbar} from "notistack";
import ControllerBox from "../../../../controller/ControllerBox";
import SelectBox from "../../../../components/SelectBox";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import {CancelOutlined, Close, VisibilityOffOutlined, VisibilityOutlined} from "@material-ui/icons";
import CircularProgress from "@material-ui/core/CircularProgress";
import Backdrop from "@material-ui/core/Backdrop/Backdrop";
import ControllerProduct from "../../../../controller/ControllerProduct";
import useSWR, {mutate} from "swr";
import Chip from "@material-ui/core/Chip";
import Collapse from "@material-ui/core/Collapse";
import PleaseWait from "../../../../components/base/loading/PleaseWait";
import ComponentError from "../../../../components/base/ComponentError";
import TagSingle from "../../../tags/TagSingle";
import Tooltip from "../../../../components/base/Tooltip";
import {gcLog} from "../../../../utils/ObjectUtils";
import ErrorBoundary from "../../../../components/base/ErrorBoundary";


export const maxShowableTagCount = 10

export default function ({boxId, open, tgId, onClose, ...props}) {
    const theme = useTheme();
    const ref = useRef();
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();
    const [items, setItems] = useState(tgId ? undefined : []);
    const [state, setState] = useState({
        saveDialog: false,
        boxId: boxId,
        loading: Boolean(tgId)
    });
    const [name, setName] = useState("")

    useEffect(() => {
        if (!open) {
            setItems([]);
            setState({
                ...state,
                saveDialog: false,
                boxId: boxId,
                loading: false
            })
            return;
        }

        if (tgId) {
            return ControllerBox.TagGroup.get({id: tgId}).then(res => {
                setItems([
                    ...res.data.data.tags
                ])
                setName(res.data.data.name[siteLang])
            }).finally(() => {
                setState({
                    ...state,
                    saveDialog: false,
                    boxId: boxId,
                    loading: false
                })
            })
        }
        setState({
            ...state,
            saveDialog: false,
            boxId: boxId,
            loading: false
        })


        // const tagsId = [];
        // _.forEach(it.tags, t => {
        //     tagsId.push(t.id)
        // })
        // ControllerProduct.Product.Tags.getList({tags: it.tags}).then(res => {
        //     setItems(res.data);
        //     setState({
        //         ...state,
        //         saveDialog: false,
        //         boxId: boxId,
        //         loading: false
        //     })
        // })
    }, [open]);

    function getSaveProps() {
        try {
            if (ref.current.hasError()) {
                reqCancel("نام گروه تگ هارا وارد کنید")
                return false;
            }
        } catch (e) {
            reqCancel(lang.get("er_problem_call_support"))
            return false
        }
        return {
            name: ref.current.serialize().name
        }
    }

    function reqCancel(text = null) {
        if (text)
            enqueueSnackbar(text,
                {
                    variant: "error",
                    action: (key) => (
                        <Fragment>
                            <Button onClick={() => {
                                closeSnackbar(key)
                            }}>
                                {lang.get('close')}
                            </Button>
                        </Fragment>
                    )
                });
    }

    function handleSave() {
        const props = getSaveProps();
        if (!getSaveProps()) {
            return
        }

        const list = [];
        _.forEach(items, (t) => {
            list.push({
                tag_id: t.id,
                show: Boolean(t.show)
            })
        })


        return ControllerBox.TagGroup.save(boxId, {
            tgId: tgId,
            name: createMultiLanguage({fa: props.name}),
            tagsId: list
        }).finally(() => {
            setState({
                ...state,
                saveDialog: false,
                loading: false
            });
        })
    }


    return (
        <Dialog open={Boolean(open)} fullScreen={true} transitionDuration={Transition}>
            <ErrorBoundary>
                <Toolbar>
                    <Box edge="start" display='flex' alignItems={'center'}
                         onClick={() => {
                             if (onClose)
                                 onClose()
                         }}>
                        <IconButton color="inherit" aria-label="close">
                            <Close/>
                        </IconButton>
                        <Typography variant="h6" pr={1} style={{cursor: 'pointer'}}>
                            انصراف
                        </Typography>
                    </Box>
                </Toolbar>
            </ErrorBoundary>

            <ErrorBoundary>
                {
                    items ?
                        (state.boxId) ?
                            <FormController innerref={ref} display={'flex'} p={2}>

                                <ErrorBoundary>
                                    <Box display={'flex'} width={'80%'}>
                                        <Box width={1}>
                                            <Tags tags={items}
                                                  onChange={(t) => {
                                                      setItems([...t])
                                                  }}/>
                                        </Box>
                                    </Box>
                                </ErrorBoundary>
                                <Box display={'flex'} flexDirection={'column'} width={'20%'}>
                                    <ErrorBoundary>
                                        <StickyBox offsetTop={120} offsetBottom={20}>
                                            <Box p={2}>
                                                <Box component={Card} p={2}>
                                                    <Box py={2} display={'flex'} justifyContent={'center'}>
                                                        <TextFieldContainer
                                                            name={createName({name: "name"})}
                                                            defaultValue={name}
                                                            onChangeDelay={500}
                                                            render={(ref, {name: inputName, initialize, valid, errorIndex, setValue, inputProps, style, props}) => {
                                                                return (
                                                                    <TextField
                                                                        {...props}
                                                                        error={!valid}
                                                                        variant={'outlined'}
                                                                        name={inputName}
                                                                        inputRef={ref}
                                                                        fullWidth
                                                                        required={true}
                                                                        label={"نام"}
                                                                        placeholder={"نام گروه تگ‌ها"}
                                                                        style={{
                                                                            ...style,
                                                                            minWidth: '90%'
                                                                        }}
                                                                        inputProps={{
                                                                            ...inputProps,
                                                                        }}/>
                                                                )
                                                            }}/>
                                                    </Box>
                                                    <Box display={'flex'} justifyContent={'center'}>
                                                        <SuccessButton
                                                            variant={"outlined"}
                                                            onClick={() => {
                                                                if (getSaveProps())
                                                                    setState({
                                                                        ...state,
                                                                        saveDialog: true,
                                                                        loading: true
                                                                    })
                                                            }}>
                                                            ذخیره
                                                        </SuccessButton>
                                                    </Box>
                                                </Box>
                                            </Box>
                                        </StickyBox>
                                    </ErrorBoundary>
                                </Box>
                            </FormController> :
                            <ErrorBoundary>
                                <SelectBox
                                    onBoxSelected={(b) => {
                                        setState({
                                            ...setState,
                                            boxId: b.id
                                        })
                                    }}/>
                            </ErrorBoundary> :
                        <PleaseWait/>
                }
            </ErrorBoundary>
            <ErrorBoundary>
                <SaveDialog
                    open={state.saveDialog}
                    text={(
                        <Box display={'flex'} flexDirection={'column'} px={3}>
                            <Typography variant={'h6'} fontWeight={400} pb={2}>
                                ذخیره گروه تگ ها
                            </Typography>
                            <Typography variant={'body1'}>
                                آیا از ذخیره این گروه اطمینان دارید؟
                            </Typography>
                            <Typography variant={'caption'} color={theme.palette.text.secondary} pt={0.5}>
                                <span style={{color: orange[300], fontWeight: 600, paddingLeft: theme.spacing(0.3)}}>
                                    توجه:
                                </span>
                                در صورت ذخیره، اطلاعات تغییریافته قابل بازگشت نمیباشد.
                            </Typography>
                        </Box>)}
                    onSubmit={() => {
                        setState({...state, saveDialog: false});
                        handleSave()
                    }}
                    onCancel={() => setState({...state, loading: false, saveDialog: false})}/>
            </ErrorBoundary>
            <Backdrop open={state.loading}
                      style={{
                          zIndex: '999999'
                      }}>
                <CircularProgress color="inherit"
                                  style={{
                                      color: cyan[300],
                                      width: 100,
                                      height: 100
                                  }}/>
            </Backdrop>
        </Dialog>
    )
}

const useTagsStyle = makeStyles(theme => ({
    root: {
        backgroundColor: cyan[100],
        color: grey[700],
        '&:focus': {
            backgroundColor: cyan[100],
            color: grey[700],
        },
    }
}));

function Tags({tags, onChange, ...props}) {
    const theme = useTheme();

    const {enqueueSnackbar, closeSnackbar} = useSnackbar();
    const [setting, setSetting] = useState({
        openAddTagDialog: false,
        dialogValue: '',
        dialogPermalink: '',
    });
    const [defValue, setDefValue] = useState("");
    const [value, setValue] = useState("");
    const [inputValue, setInputValue] = useState("")

    const classes = useTagsStyle(props);

    const d = ControllerProduct.Product.Tags.search({query: value});
    const apiKey = d[0] + value;
    const {data: da, error} = useSWR(apiKey, () => {
        return d[1]()
    });
    const {tags: data} = da ? da.data : [];

    useEffect(() => {
        setValue("");
        setDefValue("");
    }, []);

    function handleAdd(d) {
        const i = _.findIndex(tags, (t) => {
            return d.permalink === t.permalink;
        })
        if (i !== -1 && d.show === tags[i].show) {
            enqueueSnackbar('این تک انتخاب شده است.',
                {
                    variant: "error",
                    action: (key) => (
                        <Fragment>
                            <Button onClick={() => {
                                closeSnackbar(key)
                            }}>
                                {lang.get('close')}
                            </Button>
                        </Fragment>
                    )
                });
            return
        }
        if (!_.isBoolean(d.show)) {
            d.show = false;
        }
        const newTags = [...tags];
        newTags[i !== -1 ? i : tags.length] = d;
        onChange([...newTags]);
    }

    function handleDelete(d) {
        const newData = tags;
        _.remove(newData, (t) => {
            return t.id === d.id
        });
        onChange([...newData])
    }

    function handleAddNewTag() {
        if (!_.isEmpty(data)) {
            const i = _.findIndex(data, (t) => {
                return t.name.fa === value
            });
            if (i !== -1) {
                enqueueSnackbar('این تگ موجود میباشد.',
                    {
                        variant: "error",
                        action: (key) => (
                            <Fragment>
                                <Button onClick={() => {
                                    closeSnackbar(key)
                                }}>
                                    {lang.get('close')}
                                </Button>
                            </Fragment>
                        )
                    });
                return;
            }
        }

        setSetting({
            ...setting,
            openAddTagDialog: true,
            dialogPermalink: '',
            dialogValue: value
        })
    }

    function handleCloseDialog(save) {
        if (save) {
            if (!setting.dialogPermalink || !setting.dialogValue) {
                enqueueSnackbar('تمام فیلدها را پرکنید.',
                    {
                        variant: "error",
                        action: (key) => (
                            <Fragment>
                                <Button onClick={() => {
                                    closeSnackbar(key)
                                }}>
                                    {lang.get('close')}
                                </Button>
                            </Fragment>
                        )
                    });
                return;
            }
            ControllerProduct.Product.Tags.set({
                permalink: setting.dialogPermalink,
                name: createMultiLanguage({fa: setting.dialogValue})
            }).then(res => {
                res.item.hasActive = true;
                handleAdd(res.item);
                enqueueSnackbar('افزوده شد.',
                    {
                        variant: "success",
                        action: (key) => (
                            <Fragment>
                                <Button onClick={() => {
                                    closeSnackbar(key)
                                }}>
                                    {lang.get('close')}
                                </Button>
                            </Fragment>
                        )
                    });
                handleCloseDialog(false)
                mutate(apiKey);
            });
            return;
        }


        setSetting({
            ...setting,
            openAddTagDialog: false,
            dialogPermalink: ''
        })
    }

    return (
        <React.Fragment>
            <Box display={'flex'} width={1} px={2} py={1} flexDirection={'column'}>
                {!error ?
                    <Box display={'flex'} justifyContent={'center'}>
                        <Box width={0.7} display={'flex'} flexDirection={'column'} mt={2} mb={2}>
                            <Box display={'flex'} flexDirection={'column'} mb={3}>
                                <Typography variant={'body1'} pb={1} fontWeight={400}>
                                    تگ‌های فعال:
                                </Typography>
                                <Box display={'flex'} flexWrap={'wrap'}>
                                    {
                                        tags.map((t, i) => (
                                            <Box key={i} ml={2} mb={1.5}>
                                                <Tooltip title={t.show ? "عدم نمایش" : "نمایش دادن"}>
                                                    <Chip
                                                        avatar={t.show ? <VisibilityOutlined/> :
                                                            <VisibilityOffOutlined/>}
                                                        clickable
                                                        onClick={() => {
                                                            if (!t.show) {
                                                                let showCount = 0
                                                                _.forEach(tags, t => {
                                                                    showCount = (t.show ? 1 : 0) + showCount
                                                                })
                                                                if (showCount >= maxShowableTagCount) {
                                                                    enqueueSnackbar("حداکثر تعداد قابل نمایش " + maxShowableTagCount + " میباشد.",
                                                                        {
                                                                            variant: "error",
                                                                            action: (key) => (
                                                                                <Fragment>
                                                                                    <Button onClick={() => {
                                                                                        closeSnackbar(key)
                                                                                    }}>
                                                                                        {lang.get('close')}
                                                                                    </Button>
                                                                                </Fragment>
                                                                            )
                                                                        });
                                                                    return
                                                                }
                                                            }
                                                            handleAdd({
                                                                ...t,
                                                                show: !t.show
                                                            })
                                                        }}
                                                        className={classes.root}
                                                        label={t.name.fa}
                                                        onDelete={() => handleDelete(t)}
                                                        style={{
                                                            backgroundColor: !t.show ? cyan[100] : green[100]
                                                        }}/>
                                                </Tooltip>
                                            </Box>
                                        ))
                                    }
                                </Box>
                            </Box>
                            <Box display={'flex'} width={1} alignItems={'center'} justifyContent={'center'}>
                                <Box width={1} pr={4}>
                                    <TextFieldContainer
                                        name={createName({name: "tag-name"})}
                                        defaultValue={defValue}
                                        onChange={(val) => {
                                            setInputValue(val)
                                        }}
                                        render={(ref, {name: inputName, initialize, valid, errorIndex, inputProps, style, props}) => {
                                            return (
                                                <TextField
                                                    {...props}
                                                    variant={'outlined'}
                                                    error={!valid}
                                                    name={inputName}
                                                    inputRef={ref}
                                                    fullWidth
                                                    onKeyPress={(e) => {
                                                        try {
                                                            if (e.key === 'Enter') {
                                                                const val = ref.current.getAttribute(textFieldNewValue)
                                                                setValue(val)
                                                                setDefValue(val)
                                                            }
                                                        } catch (e) {
                                                        }
                                                    }}
                                                    endAdornment={(inputValue) && (
                                                        <IconButton onClick={() => {
                                                            try {
                                                                setDefValue("");
                                                                setValue("");
                                                                setInputValue("");
                                                            } catch (e) {
                                                            }
                                                        }}>
                                                            <CancelOutlined/>
                                                        </IconButton>
                                                    )}
                                                    label={'جست‌و‌جو تگ'}
                                                    placeholder={'نام تگ'}
                                                    style={{
                                                        ...style,
                                                        minWidth: '90%'
                                                    }}
                                                    inputProps={{
                                                        ...inputProps,
                                                    }}/>
                                            )
                                        }}/>
                                </Box>
                                <Box display={'flex'}>
                                    <BaseButton
                                        disabled={value.length < 2}
                                        size={"medium"}
                                        style={{
                                            backgroundColor: cyan[300],
                                            color: '#fff'
                                        }}
                                        onClick={handleAddNewTag}>
                                        {lang.get("add")}
                                    </BaseButton>
                                </Box>
                            </Box>
                            <Box mt={2} component={Card}>
                                {
                                    data ?
                                        <Collapse in={!_.isEmpty(data)}>
                                            <Box display={'flex'} flexWrap={'wrap'} py={2} px={2}>
                                                {
                                                    !_.isEmpty(data) && data.map((d, i) => (
                                                        <Box key={i} ml={2} mb={1.5}>
                                                            <Chip
                                                                label={d.name.fa}
                                                                disabled={_.findIndex(tags, (t) => {
                                                                    return d.permalink === t.permalink;
                                                                }) !== -1}
                                                                onClick={() => handleAdd(d)}
                                                                style={{
                                                                    color: d.hasActive ? theme.palette.action.hover : null
                                                                }}/>
                                                        </Box>
                                                    ))
                                                }
                                            </Box>
                                        </Collapse> :
                                        <PleaseWait style={{height: "100%", width: '100%'}}/>
                                }
                            </Box>
                        </Box>
                    </Box> :
                    <ComponentError
                        statusCode={(_.isObject(error) && error.request) ? error.request.status : undefined}
                        tryAgainFun={() => mutate(apiKey)}/>
                }
            </Box>
            <TagSingle
                open={setting.openAddTagDialog}
                name={value}
                onClose={(item) => {
                    if (item) {
                        item.hasActive = true;
                        handleAdd(item);
                        enqueueSnackbar('افزوده شد.',
                            {
                                variant: "success",
                                action: (key) => (
                                    <Fragment>
                                        <Button onClick={() => {
                                            closeSnackbar(key)
                                        }}>
                                            {lang.get('close')}
                                        </Button>
                                    </Fragment>
                                )
                            });
                        handleCloseDialog(false);
                        mutate(apiKey)
                        return
                    }
                    setSetting({
                        ...setting,
                        openAddTagDialog: false,
                        dialogPermalink: ''
                    })
                }}/>
        </React.Fragment>
    )
}
