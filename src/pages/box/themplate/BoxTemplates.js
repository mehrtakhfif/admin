import React, {useState} from "react";
import Box from "@material-ui/core/Box";
import BaseButton from "../../../components/base/button/BaseButton";
import Link from "../../../components/base/link/Link";
import rout from "../../../router";
import Typography from "../../../components/base/Typography";
import {useTheme} from "@material-ui/core";
import SelectProperty from "./property/SelectProperty";
import SelectTag from "./tag/SelectTag";


export default function ({...props}) {
    const theme = useTheme();
    const [state, setState] = useState({
        tags: false
    });
    return (
        <Box p={2}>
            <Item label={"تگ‌ها"}
                  onClick={() => {
                      setState({
                          ...state,
                          tags: true
                      })
                  }}/>
            <Item label={"رفتن به داشبورد"} rout={rout.Box.Dashboard.rout}/>
            <SelectTag
                open={state.tags}
                editable={true}
                onClose={() => {
                    setState({
                        ...state,
                        tags: false
                    })
                }}/>
        </Box>
    )
}


function Item({rout, label, onClick}) {
    const theme = useTheme();

    return (
        <BaseButton
            onClick={onClick}
            style={{
                margin: theme.spacing(1),
                backgroundColor: theme.palette.primary.main
            }}>
            <Link toHref={rout}>
                <Box display={'flex'} minHeight={150} minWidth={200}
                     alignItems={'center'} justifyContent={"center"}>
                    <Typography variant={'h4'} fontWeight={500} p={2} color={"#fff"}>
                        {label}
                    </Typography>
                </Box>
            </Link>
        </BaseButton>
    )
}
