import React, {Fragment, useEffect, useRef, useState} from "react";
import Dialog from "@material-ui/core/Dialog";
import Transition from "../../../../components/base/Transition";
import Box from "@material-ui/core/Box";
import StickyBox from "react-sticky-box";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import {lang, siteLang, sLang} from "../../../../repository";
import Typography from "../../../../components/base/Typography";
import BaseButton from "../../../../components/base/button/BaseButton";
import {useTheme} from "@material-ui/core";
import IconItem from "../../../../components/icon/IconItem";
import TextFieldMultiLanguageContainer, {createName} from "../../../../components/base/textField/TextFieldMultiLanguageContainer";
import Card from "@material-ui/core/Card";
import NoneTextField from "../../../../components/base/textField/NoneTextField";
import _ from 'lodash';
import TextField from "../../../../components/base/textField/TextField";
import {errorList, notValidErrorTextField} from "../../../../components/base/textField/TextFieldContainer";
import {createMultiLanguage} from "../../../../controller/converter";
import {UtilsData} from "../../../../utils/Utils";
import SuccessButton from "../../../../components/base/button/buttonVariant/SuccessButton";
import SaveDialog from "../../../../components/dialog/SaveDialog";
import {cyan, orange} from "@material-ui/core/colors";
import FormControl from "../../../../components/base/formController/FormController";
import Button from "@material-ui/core/Button";
import {useSnackbar} from "notistack";
import ControllerBox from "../../../../controller/ControllerBox";
import SelectBox from "../../../../components/SelectBox";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import {Close, DeleteOutline} from "@material-ui/icons";
import CircularProgress from "@material-ui/core/CircularProgress";
import Backdrop from "@material-ui/core/Backdrop/Backdrop";
import {gcError} from "../../../../utils/ObjectUtils";

export default function ({boxId, open, item, saveToServer = true, onClose, ...props}) {
    const theme = useTheme();
    const ref = useRef();
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();

    const [state, setState] = useState({
        saveDialog: false,
        boxId: boxId,
        loading: false
    });

    const [def, setDef] = useState({
        icon: 'default-property.svg',
        priority: 'high',
    });

    const [items, setter] = useState([]);

    useEffect(() => {
        if (!open) {
            setItems([]);
        }
    }, [open])


    useEffect(() => {
        if (!item || !open)
            return;
        setItems([{
            ...item,
            text: createMultiLanguage({fa: item.text})
        }]);
    }, [item, open]);

    function setItems(items) {
        _.reverse(items.sort((a, b) => a.id - b.id))
        setter(items)
    }

    function getSaveProps() {
        try {
            let errorText = undefined;
            if (ref.current.hasError()) {
                errorText = "فرم مشکل دارد";
                try {
                    const el = ref.current.getErrorElement();
                    const t = el.getAttribute(notValidErrorTextField);
                    errorText = t ? t : errorText;
                    el.focus();
                } catch (e) {
                }
            }
            if (errorText) {
                reqCancel(errorText);
                return
            }

            const data = {};

            _.forEach(ref.current.serialize(), (v) => {
                _.forEach(sLang, (l) => {
                    if (!_.isObject(data[l.key]))
                        data[l.key] = {}
                    if (!_.isArray(data[l.key].items)) {
                        data[l.key].items = [];
                    }
                    data[l.key].items.push({
                        id: item ? item.id : undefined,
                        templateId: item ? item.templateId : undefined,
                        icon: v.icon,
                        priority: v.priority,
                        text: v.text[l.key]
                    })
                });
            });
            return data;
        } catch (e) {
            gcError("CreateProperty::getSaveProps", e)
        }
    }

    function saveData() {
        if (item) {
            if (!saveToServer) {
                setTimeout(() => {
                    setState({
                        ...state,
                        saveDialog: false,
                        loading: false
                    });
                    onClose(getSaveProps());
                }, 1500);
                return;
            }

            ControllerBox.Settings.Template.Property.update(state.boxId, {
                lastItem: item,
                newItem: getSaveProps()
            }).then((res) => {
                if (onClose)
                    onClose(true);
                setState({
                    ...state,
                    saveDialog: false,
                    loading: false
                });
            }).catch(() => {
                setState({
                    ...state,
                    saveDialog: false,
                    loading: false
                });
            });
            return
        }

        if (!saveToServer) {
            setTimeout(() => {
                setState({
                    ...state,
                    saveDialog: false,
                    loading: false
                });
                onClose(getSaveProps());
            }, 1500);
            return;
        }


        ControllerBox.Settings.Template.Property.save(
            state.boxId,
            {
                property: getSaveProps()
            }).then((res => {
            if (onClose)
                onClose(true);
            setState({
                ...state,
                saveDialog: false,
                loading: false
            });
        })).catch(() => {
            setState({
                ...state,
                saveDialog: false,
                loading: false
            });
        });
    }

    function reqCancel(text = null) {
        if (text)
            enqueueSnackbar(text,
                {
                    variant: "error",
                    action: (key) => (
                        <Fragment>
                            <Button onClick={() => {
                                closeSnackbar(key)
                            }}>
                                {lang.get('close')}
                            </Button>
                        </Fragment>
                    )
                });


    }

    return (
        <Dialog open={Boolean(open)} fullScreen={true} transitionDuration={Transition}>
            <Toolbar>
                <Box edge="start" display='flex' alignItems={'center'} onClick={() => {
                    if (onClose)
                        onClose()
                }}>
                    <IconButton color="inherit" aria-label="close">
                        <Close/>
                    </IconButton>
                    <Typography variant="h6" pr={1} style={{cursor: 'pointer'}}>
                        انصراف
                    </Typography>
                </Box>
            </Toolbar>
            {(!saveToServer || state.boxId) ?
                <Box display={'flex'} p={2}>
                    <Box display={'flex'} flexDirection={'column'} width={'80%'}>
                        {!item && <Box display={'flex'} alignItems={'flex-end'}>
                            <Box px={2}>
                                <IconItem
                                    title={"انتخاب آیکون"}
                                    iconName={def.icon}
                                    height={40}
                                    width={40}
                                    onSelect={(icon) => {
                                        if (!icon)
                                            return;
                                        setDef({
                                            ...def,
                                            icon: icon.name
                                        })
                                    }}/>
                            </Box>
                            <Box display={'flex'} flexDirection={'column'} px={2} minWidth={135}>
                                <Typography variant={'subtitle2'} fontWeight={300}>
                                    اولویت:
                                </Typography>
                                <Select
                                    labelId="priority-icon"
                                    value={def.priority}
                                    onChange={(event) => {
                                        setDef({
                                            ...def,
                                            priority: event.target.value
                                        })
                                    }}
                                    style={{
                                        marginTop: theme.spacing(1),
                                    }}>
                                    <MenuItem value={'high'}>
                                        بالا
                                    </MenuItem>
                                    <MenuItem value={'medium'}>
                                        متوسط
                                    </MenuItem>
                                    <MenuItem value={'low'}>
                                        پایین
                                    </MenuItem>
                                </Select>
                            </Box>
                            <Box px={2}>
                                <BaseButton
                                    variant={"outlined"}
                                    onClick={() => {
                                        setItems([...items, {
                                            id: UtilsData.createId(items),
                                            type: def.type,
                                            icon: def.icon,
                                            priority: def.priority,
                                            text: createMultiLanguage()
                                        }])
                                    }}
                                    style={{
                                        borderColor: theme.palette.primary.main
                                    }}>
                                    ساختن
                                </BaseButton>
                            </Box>
                        </Box>}
                        <FormControl innerref={ref} display={'flex'} flexDirection={'column'}>
                            {!_.isEmpty(items) &&
                            <Box mt={4} mx={2}>
                                <Box component={Card} p={2} display={'flex'} flexDirection={'column'}>
                                    {items.map((it, i) => {
                                        if (!it.id)
                                            it.id = i + 1;
                                        return (
                                            <Box key={it.id} display={'flex'} alignItems={'center'} py={2}>
                                                {it.id &&
                                                <Typography variant={"h5"} fontWeight={300}
                                                            style={{
                                                                minWidth: 40
                                                            }}>
                                                    {it.id} )
                                                </Typography>
                                                }
                                                <Box px={2}>
                                                    <IconItem
                                                        title={"انتخاب آیکون"}
                                                        iconName={it.icon}
                                                        name={createName({group: it.id, name: 'icon'})}
                                                        height={40}
                                                        width={40}/>
                                                </Box>
                                                <Box display={'flex'} flexDirection={'column'} px={2} minWidth={135}>
                                                    <NoneTextField
                                                        defaultValue={it.priority}
                                                        name={createName({
                                                            group: it.id,
                                                            name: 'priority'
                                                        })}/>
                                                    <Typography variant={'subtitle2'} fontWeight={300}>
                                                        اولویت:
                                                    </Typography>
                                                    <Select
                                                        labelId="priority-icon"
                                                        value={it.priority}
                                                        onChange={(event) => {
                                                            const obj = {
                                                                ...it,
                                                                priority: event.target.value
                                                            };
                                                            const newItems = items;
                                                            newItems[i] = obj;
                                                            setItems([...newItems]);
                                                        }}>
                                                        <MenuItem value={'high'}>
                                                            بالا
                                                        </MenuItem>
                                                        <MenuItem value={'medium'}>
                                                            متوسط
                                                        </MenuItem>
                                                        <MenuItem value={'low'}>
                                                            پایین
                                                        </MenuItem>
                                                    </Select>
                                                </Box>
                                                <Box display={'flex'} flexDirection={'column'} px={2} flex={1}>
                                                    <TextFieldMultiLanguageContainer
                                                        group={it.id}
                                                        name={"text"}
                                                        render={(ref, {lang: inputLang, name: inputName, initialize, valid, errorIndex, setValue, inputProps, style, props}) => {
                                                            return (
                                                                <TextField
                                                                    {...props}
                                                                    error={!valid}
                                                                    name={inputName}
                                                                    defaultValue={it.text[inputLang.key]}
                                                                    fullWidth
                                                                    helperText={errorList[errorIndex]}
                                                                    inputRef={ref}
                                                                    required={inputLang.key === siteLang}
                                                                    label={"متن"}
                                                                    style={style}
                                                                    inputProps={{
                                                                        ...inputProps
                                                                    }}/>
                                                            )
                                                        }}/>
                                                </Box>
                                                {!item &&
                                                <Box px={0.5}>
                                                    <IconButton
                                                        onClick={() => {
                                                            const newItems = items;
                                                            _.remove(newItems, (item) => {
                                                                return item.id === it.id
                                                            });
                                                            setItems([...newItems]);
                                                        }}>
                                                        <DeleteOutline/>
                                                    </IconButton>
                                                </Box>}
                                            </Box>
                                        )
                                    })}
                                </Box>
                            </Box>
                            }
                        </FormControl>
                    </Box>
                    {!_.isEmpty(items) && <Box display={'flex'} flexDirection={'column'} width={'20%'}>
                        <StickyBox offsetTop={120} offsetBottom={20}>
                            <Box p={2}>
                                <Box component={Card} p={2}>
                                    <Box display={'flex'} justifyContent={'center'}>
                                        <SuccessButton
                                            variant={"outlined"}
                                            onClick={() => {
                                                if (getSaveProps())
                                                    setState({
                                                        ...state,
                                                        saveDialog: true
                                                    })
                                            }}>
                                            {item ? "بروزرسانی" : 'ذخیره'}
                                        </SuccessButton>
                                    </Box>
                                </Box>
                            </Box>
                        </StickyBox>
                    </Box>}
                    <SaveDialog
                        open={Boolean(state.saveDialog)}
                        text={(
                            <Box display={'flex'} flexDirection={'column'} px={3}>
                                <Typography variant={'h6'} fontWeight={400} pb={2}>
                                    {item ? "بروزرسانی" : "ذخیره"}
                                    {" "}
                                    ویژگی
                                </Typography>
                                <Typography variant={'body1'}>
                                    آیا از ذخیره این ویژگی اطمینان دارید؟
                                </Typography>
                                <Typography variant={'caption'} color={theme.palette.text.secondary} pt={0.5}>
                                <span style={{color: orange[300], fontWeight: 600, paddingLeft: theme.spacing(0.3)}}>
                                    توجه:
                                </span>
                                    در صورت ذخیره، اطلاعات تغییریافته قابل بازگشت نمیباشد.
                                </Typography>
                            </Box>)}
                        onSubmit={() => {
                            saveData();
                            setState({
                                ...state,
                                saveDialog: false,
                                loading: true
                            })
                        }}
                        onCancel={() => {
                            setState({
                                ...state,
                                saveDialog: false
                            })
                        }}/>
                </Box> :
                <SelectBox
                    onBoxSelected={(b) => {
                        setState({
                            ...setState,
                            boxId: b.id
                        })
                    }}/>
            }
            <Backdrop open={state.loading}
                      style={{
                          zIndex: '999999'
                      }}>
                <CircularProgress color="inherit"
                                  style={{
                                      color: cyan[300],
                                      width: 100,
                                      height: 100
                                  }}/>
            </Backdrop>
        </Dialog>
    )
}
