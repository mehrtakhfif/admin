import React, {useEffect, useState} from "react";
import {Card, Dialog, useTheme} from "@material-ui/core";
import useSWR, {mutate} from "swr";
import {useSelector} from "react-redux";
import _ from 'lodash'
import Box from "@material-ui/core/Box";
import CreateProperty from "./CreateProperty";
import ControllerBox, {defProperty} from "../../../../controller/ControllerBox";
import {siteLang, sLang} from "../../../../repository";
import SelectBox from "../../../../components/SelectBox";
import Transition from "../../../../components/base/Transition";
import PleaseWait from "../../../../components/base/loading/PleaseWait";
import ComponentError from "../../../../components/base/ComponentError";
import BaseButton from "../../../../components/base/button/BaseButton";
import Typography from "../../../../components/base/Typography";
import Icon from "../../../../components/icon/Icon";
import {convertIconName, convertIconUrl} from "../../../../controller/converter";
import {UtilsStyle} from "../../../../utils/Utils";
import Checkbox from "@material-ui/core/Checkbox";
import ButtonBase from "@material-ui/core/ButtonBase";
import {Add, Delete, Edit, SelectAllOutlined} from "@material-ui/icons";
import IconButton from "@material-ui/core/IconButton";
import Backdrop from "@material-ui/core/Backdrop/Backdrop";
import CircularProgress from "@material-ui/core/CircularProgress";
import {cyan} from "@material-ui/core/colors";
import SaveDialog from "../../../../components/dialog/SaveDialog";

export default function ({open, add, edit, select, multi, removable, boxId: bId, onClose}) {
    const {boxes} = useSelector(state => state.user);

    const [boxId, setBoxId] = useState(bId ? bId : undefined);

    useEffect(() => {
        if (!bId && !boxId)
            return;
        setBoxId(bId)
    }, [bId, open]);

    useEffect(() => {
        if (!open)
            setBoxId(undefined)
    }, [open]);

    return (
        <Dialog open={open} maxWidth={"xl"} fullWidth={true} transitionDuration={Transition}
                onClose={() => onClose()}>
            {boxId ?
                <CM boxId={boxId} edit={edit} add={add} select={select} multi={multi} removable={removable}
                    onClose={onClose}/> :
                <SelectBox onBoxSelected={(box) => {
                    setBoxId(box.id)
                }}/>}
        </Dialog>
    )
}

function CM({boxId, add, edit, select, multi, removable, onClose, ...props}) {
    const theme = useTheme();
    const apiKey = `setting-${boxId}`;
    const [state, setState] = useState({
        add: false
    });
    const {data, error} = useSWR(apiKey, () => {
        return ControllerBox.Settings.Template.get(boxId).then((res) => {
            return (res.data.property && res.data.property[siteLang]) ? res.data.property[siteLang] : {
                items: []
            }
        })
    });

    const [checkedList, setCheckedList] = useState([]);

    return (
        <Box>
            {!error ?
                data ?
                    <Box display={'flex'} flexDirection={'column'}>
                        <Box display={'flex'} p={2}
                             boxShadow={3}
                             style={{
                                 backgroundColor: theme.palette.background.paper,
                                 position: 'sticky',
                                 top: 0,
                                 zIndex: 10
                             }}>
                            <Box display={'flex'} flex={1} justifyContent={'flex-end'}>
                                {add &&
                                <BaseButton
                                    onClick={() => {
                                        setState({
                                            ...state,
                                            add: true
                                        })
                                    }}
                                    style={{
                                        backgroundColor: theme.palette.primary.main,
                                        marginLeft: theme.spacing(1),
                                        marginRight: theme.spacing(1),
                                    }}>
                                    <Typography variant={'body1'}
                                                display={'flex'}
                                                alignItems={'center'}
                                                color={"#fff"}>
                                        افزودن
                                        <Add style={{

                                            marginLeft: theme.spacing(0.5),
                                            marginRight: theme.spacing(0.5),
                                        }}/>
                                    </Typography>
                                </BaseButton>}
                                {(multi && select) &&
                                <BaseButton
                                    disabled={_.isEmpty(checkedList)}
                                    onClick={() => {
                                        const props = {};
                                        _.forEach(sLang, ({key}) => {
                                            props[key] = _.cloneDeep(defProperty);
                                        });
                                        _.forEach(checkedList, (p) => {
                                            _.forEach(props, (pr, lang) => {
                                                if (!_.isArray(props[lang].items)) {
                                                    props[lang].items = []
                                                }

                                                if (lang === siteLang) {
                                                    props[lang].items.push(p);
                                                } else {
                                                    props[lang].items.push({
                                                        ...p,
                                                        text: ""
                                                    });
                                                }
                                            });
                                        });
                                        onClose(props);
                                    }}
                                    style={{
                                        backgroundColor: theme.palette.primary.main,
                                        marginLeft: theme.spacing(1),
                                        marginRight: theme.spacing(1),
                                    }}>
                                    <Typography variant={'body1'}
                                                display={'flex'}
                                                alignItems={'center'}
                                                color={"#fff"}>
                                        انتخاب گزینه های علامت گذاری‌شده
                                        <SelectAllOutlined
                                            style={{
                                                marginLeft: theme.spacing(0.5),
                                                marginRight: theme.spacing(0.5),
                                            }}/>
                                    </Typography>
                                </BaseButton>}

                            </Box>
                        </Box>
                        <Box display={'flex'} flexWrap={'wrap'} p={2}>
                            {data.items.map((p, i) => {
                                return (
                                    <Item key={i}
                                          boxId={boxId}
                                          item={p} multi={multi}
                                          onEdit={edit ? () => {
                                              mutate(apiKey)
                                          } : undefined}
                                          select={select}
                                          onRemove={removable ? () => {
                                              mutate(apiKey)
                                          } : undefined}
                                          checked={_.findIndex(checkedList, (c) => c === p) !== -1}
                                          onSelected={(checked) => {
                                              if (multi) {
                                                  const newList = checkedList;
                                                  if (checked) {
                                                      newList.push(p)
                                                  } else {
                                                      _.remove(newList, (n) => {
                                                          return n === p;
                                                      })
                                                  }
                                                  setCheckedList([...newList]);
                                                  return;
                                              }
                                              onClose(p)
                                          }}/>
                                )
                            })}
                        </Box>
                        <CreateProperty open={state.add}
                                        boxId={boxId}
                                        saveToServer={true}
                                        onClose={() => {
                                            setState({
                                                ...state,
                                                add: false
                                            });
                                            mutate(apiKey);
                                        }}/>
                    </Box> :
                    <PleaseWait/> :
                <ComponentError
                    statusCode={(_.isObject(error) && error.request) ? error.request.status : undefined}
                    tryAgainFun={() => {
                        mutate(apiKey)
                    }}/>}
        </Box>)
}


function Item({boxId, item: p, checked = false, multi, select, onEdit, onSelected, onRemove}) {
    const [check, setCheck] = useState(checked);
    const [state, setState] = useState({
        loading: false,
        remove: false,
        edit: false
    });


    function handleRemove() {
        setState({
            ...state,
            loading: true,
            remove: false
        });
        ControllerBox.Settings.Template.Property.remove(boxId, {item: p}).then(() => {
            setState({
                ...state,
                loading: false,
                remove: false
            });
            onRemove()
        }).catch(() => {
            setState({
                ...state,
                loading: false,
                remove: false
            });
        })
    }


    const el = (
        <Box display={'flex'}>
            <Box px={2} py={2}>
                <Icon width={100} height={100}
                      alt={convertIconName(p.icon)}
                      src={convertIconUrl(p.icon)}
                      style={{
                          ...UtilsStyle.borderRadius(5)
                      }}/>
            </Box>
            <Box display={'flex'} flexDirection={'column'} p={2} py={2} minWidth={200} maxWidth={400}>
                <Box display={'flex'} flex={1} justifyContent={'flex-end'}>
                    <Box display={'flex'}>
                        {onEdit &&
                        <Box display={'flex'} alignItems={'center'} mx={0.5}>
                            <IconButton
                                size={"small"}
                                onClick={(e) => {
                                    e.stopPropagation();
                                    setState({
                                        ...state,
                                        edit: true
                                    })
                                }}>
                                <Edit fontSize={"small"}/>
                            </IconButton>
                        </Box>}
                        {onRemove &&
                        <Box display={'flex'} alignItems={'center'} mx={0.5}>
                            <IconButton
                                size={"small"}
                                onClick={(e) => {
                                    e.stopPropagation();
                                    setState({
                                        ...state,
                                        remove: true
                                    })
                                }}
                                style={{
                                    ...UtilsStyle.widthFitContent()
                                }}>
                                <Delete fontSize={"small"}/>
                            </IconButton>
                        </Box>
                        }
                        {(multi && select) &&
                        <Box display={'flex'} alignItems={'center'} mx={0.5}>
                            <Checkbox
                                checked={check}
                                onChange={(event) => {
                                    onSelected(event.target.checked);
                                    setCheck(event.target.checked);
                                    event.stopPropagation();
                                }}
                                onClick={(e) => {
                                    e.stopPropagation();
                                }}
                                style={{
                                    ...UtilsStyle.widthFitContent()
                                }}/>
                        </Box>
                        }
                    </Box>
                </Box>
                <Typography variant={'body2'}>
                    اولویت:
                    {p.priority === "high" ? "بالا" : p.priority === "medium" ? "متوسط" : "پایین"}
                </Typography>
                <Typography variant={'body1'} pt={1}>
                    {p.text}
                </Typography>
            </Box>
        </Box>
    );

    return (
        <Box p={2}>
            <Box display={'flex'} component={Card}
                 style={{
                     position: 'relative'
                 }}>
                {
                    select ?
                        <ButtonBase
                            onClick={(e) => {
                                if (multi) {
                                    const ch = !check;
                                    setCheck(Boolean(ch));
                                    onSelected(ch);
                                    e.stopPropagation();
                                    return;
                                }
                                onSelected();
                            }}>
                            {el}
                        </ButtonBase>
                        :
                        <React.Fragment>
                            {el}
                        </React.Fragment>
                }

            </Box>
            <SaveDialog
                open={state.remove}
                text={
                    <Typography variant={"h6"} p={2}>
                        آیا از حذف این ویژگی اطمینان دارید؟
                    </Typography>}
                cancelText={"بله"}
                submitText={"خیر"}
                closable={false}
                onCancel={() => {
                    handleRemove();
                }}
                onSubmit={() => {
                    setState({
                        ...state,
                        remove: false
                    });
                }}/>
            <CreateProperty open={state.edit} boxId={boxId} item={p}
                            onClose={(e) => {
                                setState({
                                    ...state,
                                    edit: false
                                });
                                if (e)
                                    onEdit()
                            }}/>
            <Backdrop open={state.loading}
                      style={{
                          zIndex: '9999'
                      }}>
                <CircularProgress color="inherit"
                                  style={{
                                      color: cyan[300],
                                      width: 100,
                                      height: 100
                                  }}/>
            </Backdrop>
        </Box>
    )
}
