import React from "react";
import Box from "@material-ui/core/Box";
import BaseButton from "../../components/base/button/BaseButton";
import Link from "../../components/base/link/Link";
import rout from "../../router";
import Typography from "../../components/base/Typography";
import {useTheme} from "@material-ui/core";
import {orange} from "@material-ui/core/colors";


export default function ({...props}) {
    const theme = useTheme();

    return (
        <Box p={2} display={'flex'} flexWrap={'wrap'}>
            <Item label={"محصول ویژه"} rout={rout.Box.SpecialProduct.rout} backgroundColor={orange[800]}/>
            <Item label={"رفتن به قالب های آماده"} rout={rout.Box.Templates.rout}/>
        </Box>
    )
}

export function Item({label, rout, onClick, backgroundColor, ...props}) {
    const theme = useTheme();
    backgroundColor = backgroundColor || theme.palette.primary.main

    return (
        <Container>
            <BaseButton
                onClick={onClick}
                style={{
                    backgroundColor: backgroundColor
                }}>
                <Link toHref={rout}>
                    <Box display={'flex'} minHeight={150} minWidth={200}
                         alignItems={'center'} justifyContent={"center"}>
                        <Typography variant={'h4'} fontWeight={500} p={2} color={"#fff"}>
                            {label}
                        </Typography>
                    </Box>
                </Link>
            </BaseButton>
        </Container>
    )
}

function Container({...props}) {
    return (
        <Box p={2}>
            {props.children}
        </Box>
    )
}
