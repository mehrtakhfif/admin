import {theme} from "../../../repository";
import BaseDialog from "../../../components/base/BaseDialog";
import ButtonText from "../../../components/base/button/ButtonText";
import React, {Fragment, useEffect, useRef, useState} from "react";
import Box from "@material-ui/core/Box";
import {lang, siteLang} from "../../../repository";
import {green} from "@material-ui/core/colors";
import DialogActions from "@material-ui/core/DialogActions";
import TextField from "../../../components/base/textField/TextField";
import DatePickerStartEndPanel from "../../../components/base/datePicker/DatePickerStartEndPanel";
import axios from "axios";
import api from "../../../controller/api";
import {gcLog} from "../../../utils/ObjectUtils";
import en from "../../../language/en";
import _ from "lodash";
import JDate from "../../../utils/JDate";

export default function TimeSelectionDialog({open, onClose}) {
    const isDarkMode = theme.palette.type === "dark";
    const [name, setName] = useState();
    const [startTime, setStartTime] = useState();
    const [endTime, setEndTime] = useState();
    const [times, setTimes] = useState()
    const [selectedTimes, setSelectedTimes] = useState()

    const handleChange = (event) => {
        setName(event.target.value);
    };

    function onSubmit() {
        if (!name || !startTime || !endTime) {
            return
        }
        const params = {
            "title": name,
            "start_date": startTime / 1000,
            "end_date": endTime / 1000
        }
        axios.post(api.Products.dateRange, params, (res) => {
            gcLog("Response from server", res)
            onClose()
        })
    }

    useEffect(() => {
        axios.get(api.Products.dateRange).then((res) => {
            gcLog("server response get", res)
            setTimes(res?.data?.data)
        })
    }, [])


/*    function onTimSelection(item) {
        const old = _.cloneDeep(selectedTimes)

        if(!old.includes(item)){          //checking weather olday contain the id
            old.push(item);               //adding to olday because value doesnt exists
        }else{
            old.splice(old.indexOf(item), 1);  //deleting
        }

        setSelectedTimes(old)

    }*/

    return (
        <BaseDialog
            open={open}
            onClose={() => {
                onClose(false)
            }}
            aria-labelledby="save-dialog-slide-title"
            aria-describedby="save-dialog-slide-description">
            <Box dipslya={'flex'} flexDirection={'column'} py={2} px={4}>
                ایجاد زمانبندی
                <Box p={2}>
                    <TextField fullWidth label="نام" id="name" value={name}
                               onChange={handleChange}/>
                </Box>
                <DatePickerStartEndPanel
                    maximumDate={null}
                    onChange={(startTime, endTime) => {
                        setStartTime(startTime)
                        setEndTime(endTime)
                    }}/>

                <DialogActions>
                    {
                        <ButtonText
                            variant={"body1"}
                            color={isDarkMode ? green[200] : green[700]}
                            onClick={() => onSubmit(name, startTime, endTime)}>
                            ایجاد
                        </ButtonText>
                    }
                    <ButtonText
                        variant={'body2'}
                        onClick={onClose}
                        style={{
                            marginLeft: theme.spacing(0.5),
                            marginRight: theme.spacing(0.5),
                        }}>
                        انصراف
                    </ButtonText>
                </DialogActions>
                <Box maxHeight={'200px'} style={{overflow: 'overlay'}}>
                    {times?.map(item => (<Box key={item.id} display={'flex'} flexDirection={'row'} justifyContent={'space-around'} alignItems={'center'}
                                              style={{borderBottom: "solid 1px #e8e8e8"}}>
                        <h3 style={{width:'50%'}}>{item?.title}</h3>
                        <p style={{width:'25%'}}>{JDate.timestampFormat(item?.start_date, "jYYYY/jM/jD HH:mm").toString()}</p>
                        <p style={{width:'25%'}}>{JDate.timestampFormat(item?.end_date, "jYYYY/jM/jD HH:mm").toString()}</p>
                    </Box>))}
                </Box>
            </Box>
        </BaseDialog>
    );
}
