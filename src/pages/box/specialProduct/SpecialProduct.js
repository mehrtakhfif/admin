import React, {Fragment, useEffect, useRef, useState} from "react";
import {useSelector} from "react-redux";
import Box from "@material-ui/core/Box";
import SelectBox from "../../../components/SelectBox";
import BaseButton from "../../../components/base/button/BaseButton";
import {Add, CenterFocusWeak, DeleteOutlineOutlined, EditOutlined} from "@material-ui/icons";
import {lang, siteLang, theme} from "../../../repository";
import {useTheme} from "@material-ui/core";
import useSWR, {mutate} from "swr";
import ControllerProduct from "../../../controller/ControllerProduct";
import _ from "lodash";
import ComponentError from "../../../components/base/ComponentError";
import PleaseWait from "../../../components/base/loading/PleaseWait";
import Typography from "../../../components/base/Typography";
import Card from "@material-ui/core/Card";
import Img from "../../../components/base/img/Img";
import {UtilsStyle} from "../../../utils/Utils";
import IconButton from "@material-ui/core/IconButton";
import SaveDialog from "../../../components/dialog/SaveDialog";
import BaseDialog from "../../../components/base/BaseDialog";
import {createMultiLanguage} from "../../../controller/converter";
import {productType, serverFileTypes} from "../../../controller/type";
import SearchSelectStorage from "../../storages/SearchSelectStorage";
import {createName} from "../../../components/base/textField/TextFieldMultiLanguageContainer";
import TextField from "../../../components/base/textField/TextField";
import TextFieldContainer from "../../../components/base/textField/TextFieldContainer";
import FormController from "../../../components/base/formController/FormController";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import UploadItem from "../../../components/base/uploader/UploadItem";
import {cyan} from "@material-ui/core/colors";
import Collapse from "@material-ui/core/Collapse";
import Tooltip from "../../../components/base/Tooltip";
import Button from "@material-ui/core/Button";
import {useSnackbar} from "notistack";
import TimeSelectionDialog from "./TimeSelectionDialog";
import api from "../../../controller/api";
import {gcLog} from "../../../utils/ObjectUtils";
import axios from "axios";
import JDate from "../../../utils/JDate";


export default function ({...props}) {
    const theme = useTheme();
    const [box, setBox] = useState({});
    const [openTimeDialog, setOpenTimeDialog] = useState(false);
    const [state, setState] = useState({
        deleteItem: undefined,
        addDialog: false,
        editDialog: false,
    });

    const apiKey = "specialProduct-" + box.id;
    const {data, error} = useSWR(apiKey, () => {
        if (_.isEmpty(box)) {
            return undefined
        }
        return ControllerProduct.Products.SpecialProducts.get({boxId: box.id})
    })

    function removeItem(item) {
        ControllerProduct.Products.SpecialProducts.remove({removeItem: item.id}).then(res => {
            mutate(apiKey)
        })
    }


    if (_.isEmpty(box)) return (
        <SelectBox onBoxSelected={(box) => {
            setBox(box)
        }}/>
    )

    if (error) return (<ComponentError
        statusCode={(_.isObject(error) && error.request) ? error.request.status : undefined}
        tryAgainFun={() => mutate(apiKey)}/>)

    if (data) return (
        <Box mt={2}>
            <Box m={4}>
                <Button onClick={()=>setOpenTimeDialog(s=>!s)}>
                    ایجاد زمانبندی
                </Button>
            </Box>
            <Box display={'flex'} flexDirection={'row'} flexWrap={'wrap'}>
                <Container width={1 / 3}>
                    <BaseButton
                        onClick={() => {
                            setState({
                                ...state,
                                addDialog: true
                            })
                        }}
                        style={{
                            width: '100%',
                            height: '100%',
                            padding: theme.spacing(2)
                        }}>
                        <Add style={{
                            color: theme.palette.text.primary,
                            width: 100,
                            height: 'auto'
                        }}/>
                    </BaseButton>
                </Container>
                {data.data.map(d => (
                    <Container key={d.id} width={1 / 3} p={2}>
                        <Box component={Card} p={2} display={'flex'}>
                            <Box p={2} width={1 / 3}>
                                <Img src={d.thumbnail}
                                     alt={d.name[siteLang]}
                                     minHeight={1}
                                     style={{
                                         ...UtilsStyle.borderRadius(5)
                                     }}/>
                            </Box>
                            <Box display={'flex'} flexDirection={'column'} flex={1}>
                                <Box display={'flex'} alignItems={'center'}>
                                    <Box flex={1}>
                                        <Typography variant={'body1'} fontWeight={400}>
                                            {d.name[siteLang]}
                                        </Typography>
                                    </Box>
                                    <Box pl={1} display={'flex'} flexDirection={'column'}>
                                        <IconButton
                                            onClick={() => {
                                                setState({
                                                    ...state,
                                                    deleteItem: d
                                                })
                                            }}>
                                            <DeleteOutlineOutlined/>
                                        </IconButton>
                                        <IconButton
                                            onClick={() => {
                                                setState({
                                                    ...state,
                                                    editDialog: d
                                                })
                                            }}>
                                            <EditOutlined/>
                                        </IconButton>
                                    </Box>
                                </Box>
                                <Typography variant={"body2"} pt={1}>
                                    {d.product.defaultStorage.storageTitle[siteLang]}
                                </Typography>
                            </Box>
                        </Box>
                    </Container>
                ))}
                <SaveDialog open={Boolean(state.deleteItem)}
                            submitText={"خیر"}
                            cancelText={"بله"}
                            text={<Typography variant={"body1"}>
                                آیا از حذف این آیتم اطمینان دارید؟
                            </Typography>}
                            onSubmit={() => {
                                setState({...state, deleteItem: undefined})
                            }}
                            onCancel={(byButton) => {
                                if (byButton)
                                    removeItem(_.cloneDeep(state.deleteItem))
                                setState({...state, deleteItem: undefined})
                            }}/>
                <SelectSpecialProduct
                    open={state.addDialog}
                    box={box}
                    onClose={(added) => {
                        if (added) {
                            mutate(apiKey)
                        }
                        setState({
                            ...state,
                            addDialog: false
                        })
                    }}/>
                <EditSpecialProduct
                    box={box}
                    open={Boolean(state.editDialog)}
                    item={state.editDialog}
                    onClose={(edited) => {
                        if (edited) {
                            mutate(apiKey)
                        }
                        setState({
                            ...state,
                            editDialog: false
                        })
                    }}/>
                <         TimeSelectionDialog
                    open={openTimeDialog}
                    onClose={() => {
                        setOpenTimeDialog(false)
                        mutate(apiKey)
                    }}
                />
            </Box>

        </Box>
    )


    return <PleaseWait/>

    /*  return (
          <Box display={'flex'} p={2}>
              {!_.isEmpty(box) ?
                  <Box width={1} display={'flex'} flexWrap={'wrap'}>
                      {
                          !error ?
                              data ?
                                  <React.Fragment>
                                      <Container width={1 / 3}>
                                          <BaseButton
                                              onClick={() => {
                                                  setState({
                                                      ...state,
                                                      addDialog: true
                                                  })
                                              }}
                                              style={{
                                                  width: '100%',
                                                  height: '100%',
                                                  padding: theme.spacing(2)
                                              }}>
                                              <Add style={{
                                                  color: theme.palette.text.primary,
                                                  width: 100,
                                                  height: 'auto'
                                              }}/>
                                          </BaseButton>
                                      </Container>
                                      {data.data.map(d => (
                                          <Container key={d.id} width={1 / 3} p={2}>
                                              <Box component={Card} p={2} display={'flex'}>
                                                  <Box p={2} width={1 / 3}>
                                                      <Img src={d.thumbnail}
                                                           alt={d.name[siteLang]}
                                                           minHeight={1}
                                                           style={{
                                                               ...UtilsStyle.borderRadius(5)
                                                           }}/>
                                                  </Box>
                                                  <Box display={'flex'} flexDirection={'column'} flex={1}>
                                                      <Box display={'flex'} alignItems={'center'}>
                                                          <Box flex={1}>
                                                              <Typography variant={'body1'} fontWeight={400}>
                                                                  {d.name[siteLang]}
                                                              </Typography>
                                                          </Box>
                                                          <Box pl={1} display={'flex'} flexDirection={'column'}>
                                                              <IconButton
                                                                  onClick={() => {
                                                                      setState({
                                                                          ...state,
                                                                          deleteItem: d
                                                                      })
                                                                  }}>
                                                                  <DeleteOutlineOutlined/>
                                                              </IconButton>
                                                              <IconButton
                                                                  onClick={() => {
                                                                      setState({
                                                                          ...state,
                                                                          editDialog: d
                                                                      })
                                                                  }}>
                                                                  <EditOutlined/>
                                                              </IconButton>
                                                          </Box>
                                                      </Box>
                                                      <Typography variant={"body2"} pt={1}>
                                                          {d.product.defaultStorage.storageTitle[siteLang]}
                                                      </Typography>
                                                  </Box>
                                              </Box>
                                          </Container>
                                      ))}
                                  </React.Fragment> :
                                  <PleaseWait/> :
                              <ComponentError
                                  statusCode={(_.isObject(error) && error.request) ? error.request.status : undefined}
                                  tryAgainFun={() => mutate(apiKey)}/>
                      }
                  </Box> :
                  <SelectBox onBoxSelected={(box) => {
                      setBox(box)
                  }}/>}
              <SaveDialog open={Boolean(state.deleteItem)}
                          submitText={"خیر"}
                          cancelText={"بله"}
                          text={<Typography variant={"body1"}>
                              آیا از حذف این آیتم اطمینان دارید؟
                          </Typography>}
                          onSubmit={() => {
                              setState({...state, deleteItem: undefined})
                          }}
                          onCancel={(byButton) => {
                              if (byButton)
                                  removeItem(_.cloneDeep(state.deleteItem))
                              setState({...state, deleteItem: undefined})
                          }}/>
              <SelectSpecialProduct
                  open={state.addDialog}
                  box={box}
                  onClose={(added) => {
                      if (added) {
                          mutate(apiKey)
                      }
                      setState({
                          ...state,
                          addDialog: false
                      })
                  }}/>
              <EditSpecialProduct
                  box={box}
                  open={Boolean(state.editDialog)}
                  item={state.editDialog}
                  onClose={(edited) => {
                      if (edited) {
                          mutate(apiKey)
                      }
                      setState({
                          ...state,
                          editDialog: false
                      })
                  }}/>
          </Box>
      )*/
}

function Container({...props}) {
    return (
        <Box p={2} {...props}>
            {props.children}
        </Box>
    )
}


function EditSpecialProduct({box, item, open, onClose, ...props}) {
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();

    const ref = useRef();
    const [state, setState] = useState({
        product: undefined,
        selectTitleTemplate: undefined,
    })
    const [selectedDateTime, setSelectedDateTime] = useState()

    const [defData, setDefData] = useState({
        title: "",
        thumbnail: undefined,
    })

    useEffect(() => {
        if (!item)
            return;
        setState({
            ...state,
            product: item.product
        })
        setDefData({
            ...defData,
            title: item.name[siteLang],
            thumbnail: item.thumbnail
        })
        setSelectedDateTime(item.date_id)
    }, [item])


    function handleTitleTemplateClose(title) {
        if (title) {
            setDefData({
                ...defData,
                title: title
            })
        }

        setState({
            ...state,
            selectTitleTemplate: undefined
        })
    }

    function handleSave() {
        try {
            ControllerProduct.Products.SpecialProducts.save({
                id: item.id,
                boxId: box.id,
                storageId: state.product.defaultStorage.storageId,
                thumbnail: defData.thumbnail.id,
                name: createMultiLanguage({fa: ref.current.serialize().title}),
                date_id:selectedDateTime
            }).then(res => {
                onClose(true)
            })
        } catch (e) {

        }

    }

    const [times, setTimes] = React.useState()
    useEffect(() => {
        axios.get(api.Products.dateRange,{params:{o: "-id"}}).then((res) => {
            setTimes(res?.data?.data)
        })
    }, [])

    function updateDateTime(date) {
        setSelectedDateTime(date.id)
    }

    return (
        <BaseDialog open={open}
                    maxWidth={"md"}
                    onClose={() => {
                        onClose()
                    }}
                    style={{
                        width: '100%'
                    }}>
            <FormController innerref={ref} display={'flex'} width={1} flexDirection={'column'} p={2}>
                {
                    state.product &&
                    <React.Fragment>
                        <Box display={'flex'} flexWrap={'wrap'}>
                            <Box display={'flex'} flexDirection={'column'}
                                 alignItems={'center'} justifyContent={'center'}>
                                <UploadItem
                                    boxId={box.id}
                                    width={serverFileTypes.Image.Thumbnail.width / 2}
                                    height={serverFileTypes.Image.Thumbnail.height / 2}
                                    type={serverFileTypes.Image.Thumbnail.type}
                                    src={defData.thumbnail ? defData.thumbnail.image : ""}
                                    onSelected={(thumbnail) => {
                                        setDefData({
                                            ...defData,
                                            thumbnail: thumbnail
                                        })
                                    }}/>
                                <Collapse in={!_.isEqual(defData.thumbnail.image, state.product.thumbnail.image)}>
                                    <Box mt={2}>
                                        <BaseButton
                                            variant={"outlined"}
                                            onClick={() => {
                                                setDefData({
                                                    ...defData,
                                                    thumbnail: state.product.thumbnail
                                                })
                                            }}
                                            style={{
                                                borderColor: cyan[300]
                                            }}>
                                            انتخاب تامنیل پیشفرض
                                        </BaseButton>
                                    </Box>
                                </Collapse>
                            </Box>
                            <Box flex={1} display={'flex'} flexDirection={'column'}>
                                <Box px={2}>
                                    <TextFieldContainer
                                        name={createName({name: "title"})}
                                        defaultValue={defData.title}
                                        render={(ref, {
                                            name: inputName,
                                            initialize,
                                            valid,
                                            errorIndex,
                                            setValue,
                                            inputProps,
                                            style,
                                            props
                                        }) => {
                                            return (
                                                <TextField
                                                    {...props}
                                                    variant={'outlined'}
                                                    error={!valid}
                                                    name={inputName}
                                                    inputRef={ref}
                                                    required={true}
                                                    fullWidth
                                                    endAdornment={(
                                                        <Tooltip title={"نام های پیشفرض"}>
                                                            <IconButton
                                                                onClick={(event) => {
                                                                    setState({
                                                                        ...state,
                                                                        selectTitleTemplate: event.currentTarget
                                                                    })
                                                                }}>
                                                                <CenterFocusWeak/>
                                                            </IconButton>
                                                        </Tooltip>
                                                    )}
                                                    label={'جست‌و‌جو محصول'}
                                                    placeholder={'نام محصول (حداقل 3 کارکتر)'}
                                                    style={{
                                                        ...style,
                                                        minWidth: '90%'
                                                    }}
                                                    inputProps={{
                                                        ...inputProps,
                                                    }}/>
                                            )
                                        }}/>
                                    <Menu
                                        anchorEl={state.selectTitleTemplate}
                                        keepMounted
                                        open={Boolean(state.selectTitleTemplate)}
                                        onClose={() => {
                                            handleTitleTemplateClose()
                                        }}>
                                        <MenuItem
                                            onClick={() => handleTitleTemplateClose(state.product.name[siteLang])}>{state.product.name[siteLang]}</MenuItem>
                                        <MenuItem
                                            onClick={() => handleTitleTemplateClose(state.product.defaultStorage.storageTitle[siteLang])}>{state.product.defaultStorage.storageTitle[siteLang]}</MenuItem>
                                    </Menu>
                                </Box>
                                <Box m={2}>
                                    <Box maxHeight={'200px'} style={{overflow: 'overlay'}}>
                                        {times?.map(time => (
                                            <Box key={time.id} bgcolor={selectedDateTime===time.id?'rgba(189,206,150,0.3)':undefined} display={'flex'} flexDirection={'row'} justifyContent={'space-around'} onClick={()=>updateDateTime(time)}
                                                 alignItems={'center'} style={{borderBottom: "solid 1px #e8e8e8",paddingRight:'8px'}}>
                                                <h3 style={{width: '50%'}}>{time?.title}</h3>
                                                <p style={{width: '25%'}}>{JDate.timestampFormat(time?.start_date, "jYYYY/jM/jD HH:mm").toString()}</p>
                                                <p style={{width: '25%'}}>{JDate.timestampFormat(time?.end_date, "jYYYY/jM/jD HH:mm").toString()}</p>
                                            </Box>))}
                                    </Box>
                                </Box>
                                <Box m={2}>
                                    <BaseButton
                                        variant={"outlined"}
                                        onClick={handleSave}
                                        style={{
                                            borderColor: cyan[300]
                                        }}>
                                        ذخیره
                                    </BaseButton>
                                    <BaseButton
                                        variant={"text"}
                                        onClick={() => onClose()}
                                        style={{
                                            marginRight: theme.spacing(1)
                                        }}>
                                        لغو
                                    </BaseButton>
                                </Box>
                            </Box>
                        </Box>
                    </React.Fragment>
                }
            </FormController>
        </BaseDialog>
    )
}

function SelectSpecialProduct({box, open, onClose, ...props}) {
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();

    return (
        <SearchSelectStorage
            open={open}
            boxId={box.id}
            types={[
                productType.product.type,
                productType.service.type,
                productType.package.type,
            ]}
            onSelect={(item) => {
                try {
                    if (!item) {
                        onClose()
                        return
                    }
                    ControllerProduct.Products.SpecialProducts.save({
                        boxId: box.id,
                        storageId: item.defaultStorage.storageId
                    }).then(() => {
                        onClose(true)
                    }).catch(() => onClose())
                } catch (e) {
                    enqueueSnackbar("مشکل در ذخیره",
                        {
                            variant: "error",
                            action: (key) => (
                                <Fragment>
                                    <Button onClick={() => {
                                        closeSnackbar(key)
                                    }}>
                                        {lang.get('close')}
                                    </Button>
                                </Fragment>
                            )
                        });
                    onClose()
                }
            }}/>
    )
}

