import React, {useEffect} from 'react';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import {jssPreset, MuiThemeProvider, StylesProvider} from '@material-ui/core/styles';
import {ThemeProvider} from '@material-ui/styles';
import Box from '@material-ui/core/Box'
import BaseSite from "./BaseSite";
import {darkTheme, dir, theme} from "../repository";
import LazyLoad from "vanilla-lazyload"
import CssBaseline from "@material-ui/core/CssBaseline";
import {useSelector} from "react-redux";
import {MuiPickersUtilsProvider} from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import rtl from 'jss-rtl';
import {create} from 'jss';


document.getElementsByTagName("body")[0].setAttribute("dir", dir);


// Configure JSS
const jss = create({plugins: [...jssPreset().plugins, rtl()]});
export default function BaseSiteContainer(props) {
    useEffect(() => {
        if (!document.lazyLoadInstance)
            document.lazyLoadInstance = new LazyLoad({
                elements_selector: ".lazy"
            });
    }, []);

    const {darkMode} = useSelector(state => state.base);

    useEffect(() => {
        if (darkMode) {
            document.getElementsByTagName("body")[0].setAttribute("dark-mode", "true");
            return
        }
        document.getElementsByTagName("body")[0].removeAttribute("dark-mode");

    }, [darkMode]);

    const th = darkMode ? darkTheme : theme;

    return (
        <Box dir={dir} className={dir} style={{width: '100%', maxWidth: 'unset'}}>
            <StylesProvider jss={jss}>
                <MuiThemeProvider theme={th}>
                    <ThemeProvider theme={th}>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <CssBaseline/>
                            <BaseSite/>
                        </MuiPickersUtilsProvider>
                    </ThemeProvider>
                </MuiThemeProvider>
            </StylesProvider>
        </Box>
    )
}

