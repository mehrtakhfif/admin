import React from "react";
import {Card, Dialog} from "@material-ui/core";
import Transition from "../../components/base/Transition";
import useSWR from "swr";
import ControllerUser from "../../controller/ControllerUser";
import Box from "@material-ui/core/Box";
import PleaseWait from "../../components/base/loading/PleaseWait";
import ButtonBase from "@material-ui/core/ButtonBase";
import Icon from "../../components/icon/Icon";
import {convertIconUrl} from "../../controller/converter";
import Typography from "../../components/base/Typography";
import {siteLang} from "../../repository";
import IconButton from "@material-ui/core/IconButton";
import {Close} from "@material-ui/icons";


export default function ({open, onSelect, ...props}) {

    const {data, error} = useSWR("select-vip", () => {
        return ControllerUser.Vip.Type.get().then(res => {
            return res.data.data
        })
    });

    return (
        <Dialog open={open} maxWidth={"xl"} fullWidth={true} scroll={"body"} transitionDuration={Transition}
                onClose={() => {
                    onSelect()
                }}>
            <Box width={1} p={2} display={'flex'} flexDirection={'column'}>
                <Box display={'flex'} alignItems={'center'}>
                    <IconButton onClick={() => onSelect()}>
                        <Close/>
                    </IconButton>
                    <Typography px={2} variant={'h6'}>
                        انتخاب گروه‌های ویژه
                    </Typography>
                </Box>
            {data ? (
                <Box display={'flex'} flexWrap={'wrap'}>
                    {data.map(v => (
                        <Box key={v.id} p={2}>
                            <ButtonBase onClick={() => {
                                onSelect(v)
                            }}>
                                <Box component={Card} display={'flex'} px={1.5} alignItems={'center'}>
                                    <Box py={2} px={2}>
                                        <Icon width={50} height={50} color={false} src={convertIconUrl(v.media)}/>
                                    </Box>
                                    <Typography variant={'h6'} fontWeight={400}>
                                        {v.name[siteLang]}
                                    </Typography>
                                </Box>
                            </ButtonBase>
                        </Box>
                    ))}
                </Box>) : <PleaseWait/>}
            </Box>
        </Dialog>
    )
}
