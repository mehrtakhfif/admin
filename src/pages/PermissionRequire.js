import React from 'react';
import {useSelector} from 'react-redux'
import {DEBUG_AUTH} from "../repository";
import rout from "../router";
import {Redirect, Route} from "react-router-dom"
import _ from 'lodash'
import {gcLog} from "../utils/ObjectUtils";

const prItem = {
    superuser: [],
    content_manager: [
        rout.Product.Review.Single.rout,
        rout.Feature.rout,
        rout.Feature.Single.rout,
        rout.FeatureGroup.rout,
        rout.FeatureGroup.Single.rout,
    ],
    support: [
        rout.Site.Dashboard.rout,
        rout.Site.rout,
        rout.Site.Ads.rout,
        rout.Invoice.rout,
        rout.Invoice.Single.show,
        rout.InvoiceProduct.rout,
        rout.Feature.rout,
        rout.Feature.Single.rout,
        rout.FeatureGroup.rout,
        rout.FeatureGroup.Single.rout,
    ],
    admin: [
        rout.Product.rout,
        rout.Product.Single.rout,
        rout.Product.Single.Refresh.rout,
        rout.Category.rout,
        rout.Category.Single.rout,
        rout.Storage.rout,
        rout.Storage.Single.rout,
        rout.Box.rout,
        rout.Box.Dashboard.rout,
        rout.Box.SpecialProduct.rout,
        rout.Box.Templates.rout,
        rout.InvoiceProduct.rout,
        rout.Booking.rout,
        rout.Brand.rout,
        rout.Supplier.rout,
        rout.Supplier.Single.rout,
        rout.Tags.rout,
        rout.Tags.Single.rout,
        rout.FeatureGroup.rout,
        rout.FeatureGroup.Single.rout,
    ],
    admin2: [
        rout.Product.rout,
        rout.Product.Single.rout,
        rout.Product.Single.Refresh.rout,
        rout.Category.rout,
        rout.Category.Single.rout,
        rout.Storage.rout,
        rout.Storage.Single.rout,
        rout.Box.rout,
        rout.Box.Dashboard.rout,
        rout.Box.SpecialProduct.rout,
        rout.Box.Templates.rout,
        rout.Brand.rout,
        rout.Supplier.rout,
        rout.Supplier.Single.rout,
        rout.Tags.rout,
        rout.Tags.Single.rout,
        rout.FeatureGroup.rout,
        rout.FeatureGroup.Single.rout,
    ],
    designer: [
        rout.Product.rout,
        rout.Product.Single.rout,
        rout.Category.rout,
        rout.Category.Single.rout,
        rout.Storage.rout,
        rout.Storage.Single.rout,
        rout.Site.rout,
        rout.Site.Ads.rout,
        rout.Site.Slider.rout,
        rout.Site.Dashboard.rout,
    ],
    accountants: [
        rout.Product.rout,
        rout.Product.Single.rout,
        rout.Storage.rout,
        rout.Storage.Single.rout,
        rout.Accountants.rout
    ],
    ha_accountants: [
        rout.Invoice.rout,
        rout.Invoice.Single.show
    ],
    post: [
        rout.Invoice.rout,
        rout.Invoice.Single.show
    ]
}


function PermissionRequire({component: Component, path, children,...rest}) {
    const isLogin = useSelector(state => state.user.isLogin);
    const roll = useSelector(state => state.user.roll);
    const permission = true ||checkPermission({path: path, roll: roll});

    gcLog("safasfasf",{isLogin,roll})

    return (
        <Route
            {...rest}
            path={path}
            render={props =>
                DEBUG_AUTH || isLogin ? (
                    permission ?
                        // <div>
                        //     {React.cloneElement(children, {...props})}
                        // </div>:
                        <React.Fragment>
                            <Component {...props} />
                        </React.Fragment> :
                        <Redirect
                            to={{
                                pathname: rout.Main.dashboard
                            }}
                        />
                ) : (
                    <Redirect
                        to={{
                            pathname: rout.Main.index,
                            state: {
                                from: props.location,
                            }
                        }}
                    />
                )
            }
        />
    );
}

export default PermissionRequire;


function checkPermission({roll, path}) {
    let pr = [rout.Main.dashboard]
    switch (roll) {
        case "superuser": {
            return true
        }
        case "admin": {
            pr = [
                ...pr,
                ...prItem.admin,
                ...prItem.admin2
            ]
            break;
        }
        case "admin2": {
            pr = [
                ...pr,
                ...prItem.admin2
            ]
            break
        }
        case "content_manager": {
            pr = [
                ...pr,
                ...prItem.admin,
                ...prItem.content_manager,
                ...prItem.support,
            ]
            break
        }
        case "support": {
            pr = [
                ...pr,
                ...prItem.content_manager,
                ...prItem.admin,
                ...prItem.support,
            ]
            break
        }
        case "designer": {
            pr = [
                ...pr,
                ...prItem.designer,
            ]
            break;
        }
        case "accountants": {
            pr = [
                ...pr,
                ...prItem.accountants,
                ...prItem.ha_accountants,
            ]
            break;
        }
        case "ha_accountants": {
            pr = [
                ...pr,
                ...prItem.ha_accountants,
            ]
            break;
        }
        case "post": {
            pr = [
                ...pr,
                ...prItem.post,
            ]
        }
        default: {
        }
    }
    let haveP = false;
    _.forEach(pr, p => {
        if (haveP)
            return false
        haveP = _.includes(path, p)
    })
    return haveP
}
