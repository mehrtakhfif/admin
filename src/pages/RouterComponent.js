import React,{Suspense} from 'react';
import CheckSiteNewFeature from "../components/baseSite/CheckSiteNewFeature";
import Header from "../components/Header";
import AxiosSetup from "../components/AxoisSetup";
import {makeStyles} from "@material-ui/styles";
import {useTheme,Box} from "@material-ui/core";
import {BrowserRouter as Router, Redirect, Route, Switch} from "react-router-dom"
import {useDispatch, useSelector} from "react-redux"
import {SnackbarProvider} from "notistack";
import {SWRConfig} from "swr";
import RoutsSwitch from "./RoutsSwitch";
import RouterLoading from "./RouterLoading";

const useStyles = makeStyles({});

function RouterComponent(props) {
    const classes = useStyles();
    const dispatch = useDispatch();
    const theme = useTheme();
    const {baseSite, dialog, ...state} = useSelector(state => state);
    const isDarkMode = theme.palette.type === "dark";
    const routerStyle = React.useMemo(()=>({zIndex: "2"}))


    return (
        <Router style={routerStyle}>
            <SWRConfig
                value={{
                    refreshInterval: 0,
                    revalidateOnFocus: false,
                    dedupingInterval: 2000,
                    errorRetryInterval: 5000,
                    errorRetryCount: 2
                }}>
                <Box>
                    <SnackbarProvider maxSnack={3}
                                      autoHideDuration={7000}
                                      preventDuplicate={true}
                                      anchorOrigin={{
                                          vertical: 'top',
                                          horizontal: 'center',
                                      }}>
                        <CheckSiteNewFeature>
                            <Header {...props}>
                                <AxiosSetup>
                                    <Suspense fallback={<RouterLoading/>}>
                                        <RoutsSwitch/>
                                    </Suspense>
                                </AxiosSetup>
                            </Header>
                        </CheckSiteNewFeature>
                    </SnackbarProvider>
                </Box>
            </SWRConfig>
        </Router>
    )
}

export default  React.memo(RouterComponent)