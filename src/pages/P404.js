import React from 'react';
import Box from "@material-ui/core/Box";


export default function P404() {

    return (
        <Box display="flex" py={[5, 5, 8]} style={{fontSize: "750%"}}>
            404
        </Box>
    )
}


