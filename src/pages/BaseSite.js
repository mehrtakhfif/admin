import React, {Suspense} from "react";
import {useDispatch, useSelector} from "react-redux"
import Box from "@material-ui/core/Box"
import {DEBUG_AUTH, dir, webRout} from "../repository";
import Typography from "../components/base/Typography";
import packageJson from "../../package.json";
import RouterComponent from "./RouterComponent";

function BaseSite(props) {
    const {isLogin} = useSelector(state => state.user);
    return (
        <Box dir={dir}>
            <RouterComponent {...props}/>
            {
                isLogin &&
                <Typography variant={"caption"}
                            style={{
                                position: 'fixed',
                                bottom: 5,
                                left: 20
                            }}>
                    V {packageJson.version}
                </Typography>
            }
        </Box>
    )
}

export default BaseSite


