import React, {Fragment, useEffect, useRef, useState} from "react";
import Box from "@material-ui/core/Box";
import useTheme from "@material-ui/core/styles/useTheme";
import {useSnackbar} from "notistack";
import LocalStorageUtils from "../../utils/LocalStorageUtils";
import ControllerProduct from "../../controller/ControllerProduct";
import _ from "lodash";
import ComponentError from "../../components/base/ComponentError";
import {gcError, gcLog, getSafe, tryIt} from "../../utils/ObjectUtils";
import ErrorBoundary from "../../components/base/ErrorBoundary";
import FormController from "../../components/base/formController/FormController";
import FormControl from "../../components/base/formController/FormController";
import {Prompt} from "react-router-dom";
import Backdrop from "@material-ui/core/Backdrop";
import CircularProgress from "@material-ui/core/CircularProgress";
import {blue, cyan, green, grey, orange, red} from "@material-ui/core/colors";
import StickyBox from "react-sticky-box";
import SaveDialog from "../../components/dialog/SaveDialog";
import TextFieldMultiLanguageContainer, {
    createInputProps,
    createName
} from "../../components/base/textField/TextFieldMultiLanguageContainer";
import TextField from "../../components/base/textField/TextField";
import TextFieldContainer, {errorList, textFieldNewValue} from "../../components/base/textField/TextFieldContainer";
import {activeLang, DEBUG, lang, siteLang, sLang, theme, webRout} from "../../repository";
import {
    convertIconUrl,
    createMultiLanguage,
    createMultiLanguageObject,
    states,
    toServer
} from "../../controller/converter";
import {bookingType, featureType, productDescription, productType, serverFileTypes,} from "../../controller/type";
import PermalinkTextField from "../../components/base/textField/PermalinkTextField";
import BaseButton from "../../components/base/button/BaseButton";
import NoneTextField from "../../components/base/textField/NoneTextField";
import Typography from "../../components/base/Typography";
import rout, {siteRout} from "../../router";
import Button from "@material-ui/core/Button";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import PleaseWait from "../../components/base/loading/PleaseWait";
import {Card, makeStyles} from "@material-ui/core";
import Img from "../../components/base/img/Img";
import IconButton from "@material-ui/core/IconButton";
import {
    Add,
    AddBoxOutlined,
    AddToPhotosOutlined,
    CancelOutlined,
    CheckCircleOutlineOutlined,
    Close,
    DateRange,
    Delete,
    DeleteOutline,
    DeleteOutlineOutlined,
    Edit,
    ExpandLess,
    ExpandMore, HighlightOff,
    InfoOutlined,
    Map,
    Menu,
    MenuOpen,
    Phone,
    PhoneEnabled,
    PhoneIphone,
    Save,
    SortOutlined,
    Storage,
    Sync,
    VisibilityOffOutlined,
    VisibilityOutlined
} from "@material-ui/icons";
import BaseFormContainer from "../../components/base/FormContainer";
import UploadItem from "../../components/base/uploader/UploadItem";
import SelectBox from "../../components/SelectBox";
import Switch from "@material-ui/core/Switch";
import DraggableList from "../../components/base/draggableList/DraggableList";
import {UtilsData, UtilsRouter, UtilsStyle} from "../../utils/Utils";
import Editor, {editorContentToHtml, htmlToEditorContent} from "../../components/base/editor/Editor";
import MultiTextField from "../../components/base/textField/MultiTextField";
import MapImage from "../../drawable/image/map.jpg";
import SelectLocation from "../../components/addAddress/SelectLocation";
import L from "leaflet";
import icon from "../../drawable/mapImage/marker-icon.png";
import {Map as LeafletMap, Marker, TileLayer} from "react-leaflet";
import AddAddressMapComponent from "../../components/addAddress/Map";
import PropTypes, {bool} from "prop-types";
import Collapse from "@material-ui/core/Collapse";
import Select from "@material-ui/core/Select";
import MaterialMenu from "@material-ui/core/Menu"
import MenuItem from "@material-ui/core/MenuItem";
import useSWR, {mutate} from "swr";
import {maxShowableTagCount} from "../box/themplate/tag/CreateTag";
import Chip from "@material-ui/core/Chip";
import TagSingle from "../tags/TagSingle";
import SelectTag, {TagItem} from "../box/themplate/tag/SelectTag";
import Gem2 from "../../components/base/icon/Gem2";
import CreateProperty from "../box/themplate/property/CreateProperty";
import SelectProperty from "../box/themplate/property/SelectProperty";
import Tooltip from "@material-ui/core/Tooltip";
import Icon from "../../components/icon/Icon";
import BaseDialog from "../../components/base/BaseDialog";
import SuccessButton from "../../components/base/button/buttonVariant/SuccessButton";
import ButtonBase from "@material-ui/core/ButtonBase";
import ControllerSite from "../../controller/ControllerSite";
import AutoFill from "../../components/base/autoFill/AutoFill";
import BaseLink from "../../components/base/link/Link";
import CategorySingle, {getActiveParents} from "../categories/CategorySingle";
import Dialog from "@material-ui/core/Dialog";
import Skeleton from "@material-ui/lab/Skeleton";
import Divider from "@material-ui/core/Divider";
import Checkbox from "@material-ui/core/Checkbox";
import Slide from "@material-ui/core/Slide";
import {EditBrand} from "../brands/Brands";
import MaterialFormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import CacheFindDialog from "../../components/dialog/CacheFindDialog";
import {useSelector} from "react-redux";
import {defaultDescription, defaultProperties} from "../../controller/defualt";
import DefaultTextField from "../../components/base/textField/DefaultTextField";
import SelectFeatureValue from "../../components/Feature/SelectFeatureValue";
import ProductFeatureGroup from "../../components/Feature/ProductFeatureGroup";
import Ordertems from "../../components/base/OrderItems";
import OrderProductFeature from "./OrderProductFeature";
import ProductReviewSingle, {ReviewItems} from "./ProductReviewSingle";
import DefaultTextFieldMultiLanguage from "../../components/base/textField/DefaultTextFieldMultiLanguage";

export default function ({...props}) {
    //region var
    const {canSetReview} = useSelector(state => state.user);
    const theme = useTheme();
    const ref = useRef();
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();
    const pId = props.match.params.pId;

    //endregion var

    //region state
    const [state, setState] = useState({
        refreshCache: 1,
        error: false,
    });
    const [cacheFind, setCacheFind] = useState(undefined);
    const [setting, setSetting] = useState({
        loading: false,
        saveDialog: false
    });
    const [data, setData] = useState(pId ? false : {
        id: undefined,
        box: undefined,
        name: createMultiLanguage(),
        permalink: "",
        type: undefined,
        media: [],
        description: defaultDescription(),
        shortDescription: createMultiLanguage(),
        address: createMultiLanguage(),
        shortAddress: createMultiLanguage(),
        location: undefined,
        details: createMultiLanguageObject(),
        properties: defaultProperties(),
        cities: [],
        tags: [],
        tag_groups: [],
        features: [],
        settings: {},
    });
    const [activeEl, setActiveEl] = useState({
        cities: {
            active: false,
            label: 'city'
        },
        address: {
            active: false,
        },
        mapCo: {
            active: false,
            label: 'map'
        },
        details: {
            active: false,
        },
        properties: {
            active: false,
        },
        brand: {
            active: false,
        },
    });
    const [cacheTimer, setCacheTimer] = useState(null);
    //endregion state

    //region function
    function getData() {
        ControllerProduct.Product.get({pId: pId}).then((res) => {
                const data = _.cloneDeep(res.data);
                setData({
                    ...data,
                    description: !_.isEmpty(data.description) ? data.description : defaultDescription(),
                    properties: !_.isEmpty(data.properties) ? data.properties : defaultProperties()
                });

                setActiveEl({
                    ...activeEl,
                    brand: {
                        ...activeEl.brand,
                        active: !_.isEmpty(data.brand)
                    },
                    properties: {
                        ...activeEl.properties,
                        active: !_.isEmpty(data.properties) && !_.isEmpty(data.properties.data) && !_.isEmpty(data.properties.data[siteLang])
                    },
                    details: {
                        ...activeEl.details,
                        active: !_.isEmpty(data.details) && !_.isEmpty(data.details[siteLang]) &&
                            (
                                !_.isEmpty(data.details[siteLang].phones) ||
                                !_.isEmpty(data.details[siteLang].servingDays) ||
                                !_.isEmpty(data.details[siteLang].servingHours) ||
                                !_.isEmpty(data.details[siteLang].text)
                            )
                    },
                    mapCo: {
                        ...activeEl.mapCo,
                        active: !_.isEmpty(data.location)
                    },
                    cities: {
                        ...activeEl.cities,
                        active: !_.isEmpty(data.cities)
                    },
                    address: {
                        ...activeEl.address,
                        active: !_.isEmpty(data.address)
                    }
                })

                setCacheFind(!!LocalStorageUtils.get(getCacheKey()))
            }
        ).catch((e) => {
            setState({
                ...state,
                error: (e && e.response && e.response.status) ? e.response.status : 11
            })
        })
    }

    const getCacheKey = () => {
        return `productSingle-${pId ? pId : -1}`
    };

    function getSaveProps() {
        try {
            const form = ref.current.serialize();
            const props = {
                id: data.id,
                category_id: data.category_id,
                booking_type: (_.isEmpty(form.booking_type) ? undefined : form.booking_type),
                name: form.name,
                permalink: form.permalink,
                thumbnail_id: data.thumbnail ? data.thumbnail.id : undefined,
                features: data.features,
                settings: {
                    ...data.settings,
                    adminNote: form.settings.adminNote,
                    productStatus: _.toInteger(form.settings.productStatus)
                },
                description: (() => {
                    const params = {}
                    _.forEach(Object.keys(productDescription), k => {
                        params[k] = form[k];
                    })
                    return defaultDescription(params)
                })(),
                shortDescription: form.short_description,
            }

            if (!props.name || _.isEmpty(props.name[siteLang])) {
                reqCancel("نام محصول را وارد کنید")
                return
            }

            if (_.isEmpty(props.permalink)) {
                reqCancel("پرمالینک محصول را وارد کنید")
                return
            }

            if (!pId) {
                try {
                    if (_.isEmpty(form.product_type))
                        throw ""
                    props.type = form.product_type
                } catch (e) {
                    reqCancel("نوع محصول را انتخاب کنید")
                    return
                }
            }


            const newCategory = [];
            try {
                _.forEach(data.categories, (cat) => {
                    newCategory.push(cat.id);
                });
            } catch (e) {
                gcError("getSaveProps::newCategory", e)
            }

            const newMedia = [];
            try {
                _.forEach(data.media, (m) => {
                    newMedia.push(m.id);
                });
            } catch (e) {
                gcError("getSaveProps::newMedia", e)
            }

            const newTags = [];
            try {
                _.forEach(data.tags, (t) => {
                    newTags.push({tag_id: t.id, show: Boolean(t.show)})
                });
            } catch (e) {
                gcError("getSaveProps::newTags", e)
            }

            const newTagGroups = [];
            try {
                _.forEach(data.tag_groups, t => {
                    newTagGroups.push(t.id);
                })
            } catch (e) {
                gcError("getSaveProps::GroupTags", e)
            }

            if (activeEl.brand.active) {
                try {
                    props.brand_id = data.brand.id;
                } catch (e) {
                    reqCancel("در انتخاب برند مشکلی پیش آمده.")
                    return
                }
            }

            if (activeEl.cities.active) {
                const ci = Object.keys(form.cities)
                props.cities = []
                _.forEach(ci, (c) => props.cities.push(_.toNumber(c)))
            }

            if (activeEl.address.active) {
                props.address = form.address
                props.short_address = form.shortAddress
            }

            if (activeEl.mapCo.active) {
                props.location = data.location
            }

            if (activeEl.details.active) {
                const param = {
                    phones: {},
                    text: {}
                };
                _.forEach(sLang, (l, key) => {
                    if (!data.details[key])
                        return;
                    param.phones[key] = data.details[key].phones;
                    param.text[key] = data.details[key].text;
                });

                props.details = toServer.details({
                    servingHours: form.details.servingHours,
                    servingDays: form.details.servingDays,
                    phones: param.phones,
                    text: param.text,
                });
            }

            if (activeEl.properties.active) {
                props.properties = data.properties
            }

            return {
                ...props,
                categories: newCategory,
                media: newMedia,
                tags: newTags,
                tag_groups: newTagGroups
            }
        } catch (e) {
            reqCancel(lang.get("er_error"))
            gcError("ProductSingle::getSaveProps", e)

        }
    }

    function saveProduct(first) {
        if (first) {
            if (!getSaveProps()) {
                return;
            }
            setSetting({
                ...setting,
                saveDialog: true
            })
            return
        }
        setSetting({
            ...setting,
            loading: true,
            saveDialog: false,
        });
        setTimeout(() => {
            const params = getSaveProps();
            if (!params)
                return

            ControllerProduct.Product.save(params).then((res) => {


                LocalStorageUtils.remove(getCacheKey());

                const set = {
                    ...setting,
                    saveDialog: false,
                    loading: false,
                };
                setState({
                    ...state,
                    checkRedirect: false
                })
                try {
                    if (!pId)
                        UtilsRouter.goTo(props.history, {routUrl: rout.Product.Single.Refresh.create(pId ? pId : res.data.id)})
                } catch (e) {
                    gcError("ProductSingle::saveProduct::saveProduct", e)
                }
                setSetting({...set});
            }).catch((e) => {
                reqCancel('مشکلی در ذخیره محصول پیش آمده است.');
                setSetting({
                    ...setting,
                    saveDialog: false,
                    loading: false
                })
            });
        }, 5000)
    }

    function reqCancel(text = null) {
        if (text)
            enqueueSnackbar(text,
                {
                    variant: "error",
                    action: (key) => (
                        <Fragment>
                            <Button onClick={() => {
                                closeSnackbar(key)
                            }}>
                                {lang.get('close')}
                            </Button>
                        </Fragment>
                    )
                });

        setSetting({
            ...setting,
            saveDialog: false,
            loading: false
        })

    }

    const handleSyncCache = () => {
        const props = LocalStorageUtils.get(getCacheKey());
        if (!_.isEmpty(props) && props.description.data) {
            setData({...props})
        }
        setTimeout(() => {
            setCacheFind(false)
        }, 1500)
    };
    const handleCloseCache = () => {
        LocalStorageUtils.remove(getCacheKey());
        setCacheFind(false)
    }

    const handleSidePanelAction = (review) => {
        if (review) {
            setData({
                ...data,
                check_review_local: true
            })
        }
        setTimeout(() => {
            saveProduct(true)
        }, review ? 1000 : 0)
    }
    //endregion function

    //region useEffect
    useEffect(() => {
        if (pId) {
            if (data) {
                UtilsRouter.goTo(props.history, {routUrl: rout.Product.Single.Refresh.create(pId)})
            }
            getData()
            return
        }
        if (LocalStorageUtils.get(getCacheKey())) {
            setCacheFind(false)
        } else {
            setCacheFind(false)
        }
    }, [pId]);

    useEffect(() => {
        if (!data || cacheFind || cacheFind === undefined)
            return;
        clearTimeout(cacheTimer);

        setCacheTimer(setTimeout(() => {

            const props = {
                ...data
            };
            try {
                const form = ref.current.serialize();
                if (form.name) {
                    props.name = form.name;
                }
                if (form.permalink) {
                    props.permalink = form.permalink;
                }
                if (form.address) {
                    props.address = form.address
                }
                if (form.shortAddress) {
                    props.shortAddress = form.shortAddress
                }
                if (form.details) {
                    _.forEach(sLang, (l, key) => {
                        if (form.details.servingDays[key]) {
                            props.details[key].servingDays = form.details.servingDays[key];
                        }
                        if (form.details.servingHours) {
                            props.details[key].servingHours = form.details.servingHours[key];
                        }
                        if (form.details.phone) {
                            props.details[key].phone = form.details.phone[key];
                        }
                    });
                }
            } catch (e) {
            }
            LocalStorageUtils.set(getCacheKey(), props)
        }, 100))
    }, [state.refreshCache, data])
    //endregion useEffect

    return (
        <ErrorBoundary>
            <Prompt
                when={state.checkRedirect}
                message={location => `آیا اطمینان دارید که میخواهید این صفحه را ترک کنید؟`}/>
            <Box display={'flex'} width={1}>
                {
                    !state.error ?
                        (cacheFind !== undefined &&
                            (!cacheFind)) || DEBUG ?
                            // true ?
                            data !== false ?
                                !state.error ?
                                    data.box ?
                                        <React.Fragment>
                                            <FormController innerref={ref} name={"product-single"}
                                                            display={'flex'} width={1}
                                                            py={2}
                                                            flexWrap={'wrap'}
                                                            onChange={() => {
                                                                try {
                                                                    setState({
                                                                        ...state,
                                                                        refreshCache: state.refreshCache + 1
                                                                    })
                                                                } catch (e) {

                                                                }
                                                                //TODO: refresh cache
                                                            }}>
                                                {
                                                     data.review&&
                                                    <ReviewShow data={data.review}/>
                                                }
                                                <Box width={5 / 7} px={2}>
                                                    <Box display={'flex'} alignItems={'center'}
                                                         flexWrap={'wrap'}
                                                         pb={2}>
                                                        <Typography variant={'body2'} pl={1}>
                                                            {lang.get('box')}: {data.box.name[activeLang]}
                                                        </Typography>
                                                        {pId &&
                                                        <BaseLink toHref={data.storageLink}>
                                                            <Typography alignItems={"center"} variant={"body1"}>
                                                                <Storage
                                                                    fontSize={"small"}
                                                                    style={{
                                                                        marginLeft: theme.spacing(1)
                                                                    }}/>
                                                                رفتن به انبار
                                                            </Typography>
                                                        </BaseLink>
                                                        }
                                                    </Box>
                                                    <Name item={data}/>
                                                    <ProductType item={data}/>
                                                    <Media item={data}
                                                           onChange={(media) => {
                                                               setData({
                                                                   ...data,
                                                                   media: [...media]
                                                               })
                                                           }}/>
                                                    <ShortDescription
                                                        boxId={data.box.id}
                                                        item={data.shortDescription}
                                                        title={lang.get("short_description")}
                                                        name={"short_description"}
                                                        onChange={(newData) => {
                                                            // setData({
                                                            //     ...data,
                                                            //     shortDescription: newData
                                                            // })
                                                        }}/>

                                                    {
                                                        data.description.data[siteLang].items.map(s => (
                                                            <Description
                                                                boxId={data.box.id}
                                                                name={s.key}
                                                                key={s.key}
                                                                onRemove={
                                                                    s.key !== productDescription.description.key ?
                                                                        () => {
                                                                            const newData = _.cloneDeep(data);
                                                                            _.remove(newData.description.data[siteLang].items, (t) => {
                                                                                return t.key === s.key
                                                                            })
                                                                            setData({
                                                                                ...newData
                                                                            })
                                                                        } : undefined
                                                                }
                                                                title={productDescription[s.key].label}
                                                                item={createMultiLanguage({
                                                                    fa: (() => {
                                                                        return _.find(data.description.data[siteLang].items, (d) => {
                                                                            return d.key === s.key
                                                                        }).value
                                                                    })()
                                                                })}
                                                                onChange={(newData) => {
                                                                    const newD = _.cloneDeep(data.description);
                                                                    const i = _.findIndex(newD.data[siteLang].items, (d) => {
                                                                        return d.key === s.key;
                                                                    })
                                                                    if (i === -1)
                                                                        return;
                                                                    newD.data[siteLang].items[i].value = newData[siteLang]
                                                                    setData({
                                                                        ...data,
                                                                        description: {...newD}
                                                                    })
                                                                }}/>
                                                        ))
                                                    }


                                                    <Features productId={data.id} item={data}
                                                              onChange={(features) => {
                                                                  let sortedFeatures = null
                                                                  try {
                                                                      sortedFeatures = features.features.sort((a, b) => {
                                                                          const ap = a.priority === undefined ? a.feature.priority : a.priority
                                                                          const bp = b.priority === undefined ? b.feature.priority : b.priority


                                                                          if (ap === undefined) return 1
                                                                          if (bp === undefined) return -1

                                                                          return (ap > bp) ? 1 : -1
                                                                      })
                                                                  } catch (e) {
                                                                      sortedFeatures = features.features
                                                                  }

                                                                  setData(s => ({
                                                                      ...s,
                                                                      features: sortedFeatures
                                                                  }))
                                                              }}/>

                                                    <Cities data={data}
                                                            active={activeEl.cities.active}
                                                            onChange={(cities) => {
                                                                setData({
                                                                    ...data,
                                                                    cities: cities
                                                                })
                                                            }}/>
                                                    <Address active={activeEl.address.active} item={data}/>
                                                    <MapCo
                                                        active={activeEl.mapCo.active}
                                                        item={data}
                                                        onChange={(location) => {
                                                            setData({
                                                                ...data,
                                                                location: location
                                                            })
                                                        }}/>
                                                    <Details
                                                        active={activeEl.details.active}
                                                        data={data}
                                                        onChange={(newDetails) => {
                                                            setData({
                                                                ...data,
                                                                details: {...newDetails}
                                                            })
                                                        }}/>
                                                    <Properties active={activeEl.properties.active} data={data}
                                                                onChange={(newData) => {
                                                                    setData({
                                                                        ...data,
                                                                        properties: newData
                                                                    })
                                                                }}/>
                                                    <Tags data={data}
                                                          onChange={(newData) => {
                                                              setData({
                                                                  ...data,
                                                                  tags: newData
                                                              })
                                                          }}
                                                          onTagGpAdd={(newData) => {
                                                              setData({
                                                                  ...data,
                                                                  tag_groups: [...newData]
                                                              })
                                                          }}/>
                                                    <SaveDialog open={setting.saveDialog === true}
                                                                text={(
                                                                    <Box display={'flex'} flexDirection={'column'}
                                                                         px={3}>
                                                                        <Typography variant={'h6'} fontWeight={400}
                                                                                    pb={2}>
                                                                            ذخیره محصول
                                                                        </Typography>
                                                                        <Typography variant={'body1'}>
                                                                            آیا از ذخیره این محصول اطمینان دارید؟
                                                                        </Typography>
                                                                        <Typography variant={'caption'}
                                                                                    color={theme.palette.text.secondary}
                                                                                    pt={0.5}>
                                <span style={{color: orange[300], fontWeight: 600, paddingLeft: theme.spacing(0.3)}}>
                                    توجه:
                                </span>
                                                                            در صورت ذخیره، اطلاعات تغییریافته قابل
                                                                            بازگشت
                                                                            نمیباشد.
                                                                        </Typography>
                                                                    </Box>)}
                                                                onCancel={() => {
                                                                    setSetting({
                                                                        ...setting,
                                                                        saveDialog: false,
                                                                    })
                                                                }}
                                                                onSubmit={(save) => {
                                                                    setSetting({
                                                                        ...setting,
                                                                        saveDialog: false,
                                                                        loading: save
                                                                    })
                                                                    saveProduct()
                                                                }}/>
                                                </Box>
                                                <Box width={2 / 7} pr={3}>
                                                    <StickyBox offsetTop={80} offsetBottom={20}>
                                                        <Box display={'flex'} flexDirection={'column'}>
                                                            <SidePanelActions
                                                                data={data}
                                                                onSaveClick={handleSidePanelAction}/>
                                                            <ActiveProduct id={data.id} productDisable={data.disable}/>
                                                            {
                                                                pId &&
                                                                <Review data={data}/>
                                                            }
                                                            <Thumbnail data={data} onChange={(thumbnail) => {
                                                                setData({
                                                                    ...data,
                                                                    thumbnail: thumbnail
                                                                })
                                                            }}/>
                                                            <SelectCategory data={data}
                                                                            onChange={(cat) => {
                                                                                setData({
                                                                                    ...data,
                                                                                    categories: cat
                                                                                })
                                                                            }}/>
                                                            <SelectBrand
                                                                data={data}
                                                                active={activeEl.brand.active}
                                                                onChange={(br) => {
                                                                    setData({
                                                                        ...data,
                                                                        brand: br
                                                                    })
                                                                }}/>
                                                            <Actions
                                                                data={data}
                                                                onChange={(data) => {
                                                                    setData({...data})
                                                                }}/>
                                                            <PanelFeatures
                                                                data={activeEl}
                                                                onChange={(elKey) => {
                                                                    const newEl = activeEl;
                                                                    newEl[elKey].active = !newEl[elKey].active;
                                                                    setActiveEl({
                                                                        ...newEl
                                                                    })
                                                                }}/>
                                                            <ProductSetting data={data}/>
                                                            <SidePanelActions
                                                                data={data}
                                                                onSaveClick={handleSidePanelAction}/>
                                                        </Box>
                                                    </StickyBox>
                                                </Box>
                                            </FormController>
                                            <Backdrop open={setting.loading}
                                                      style={{
                                                          zIndex: '9999'
                                                      }}>
                                                <CircularProgress color="inherit"
                                                                  style={{
                                                                      color: cyan[300],
                                                                      width: 100,
                                                                      height: 100
                                                                  }}/>
                                            </Backdrop>
                                        </React.Fragment>
                                        :
                                        <SelectBox
                                            onBoxSelected={(box) =>
                                                setData({
                                                    ...data,
                                                    box: box
                                                })}/>
                                    :
                                    <ComponentError
                                        tryAgainFun={getData}
                                        statusCode={state.error}/>
                                :
                                <PleaseWait/>
                            :
                            <CacheFindDialog
                                open={Boolean(cacheFind)}
                                onSubmit={handleSyncCache}
                                onCancel={handleCloseCache}/>
                        :
                        <ComponentError tryAgainFun={getData}/>
                }
            </Box>
        </ErrorBoundary>
    )
}


function ReviewShow(data) {

    // if (data===undefined)
    //     return <React.Fragment/>

    let requestReview = data.state === "request_review";

    return (
        (data && data.state && data.state !== "ready") ?
            <Box width={1} m={2}
                 p={2}
                 style={{
                     backgroundColor: !requestReview ? orange['A400'] : red['A200'],
                     ...UtilsStyle.borderRadius(5)
                 }}>
                <Typography pt={1} variant={'h6'} color={"#fff"}
                            alignItems={"center"}>

                    {
                        !requestReview ?
                            <React.Fragment>
                                <CheckCircleOutlineOutlined
                                    style={{
                                        marginLeft: theme.spacing(1)
                                    }}/>
                                درخواست بازبینی با موفقیت ثبت شد و پس از بازبینی محصول
                                فعال میشود.
                            </React.Fragment> :
                            <React.Fragment>
                                <HighlightOff
                                    style={{
                                        marginLeft: theme.spacing(1)
                                    }}/>
                                این محصول نیاز به بازبینی دارد.
                            </React.Fragment>
                    }
                </Typography>
            </Box> :
            <React.Fragment/>
    )
}

//region SidePanel
function Thumbnail({data, onChange}) {
    return (
        <FormContainer
            my={0.5}
            openDef={!!data.thumbnail}
            title={lang.get("thumbnail_image")}>
            <Box px={3} py={1.5} display={'flex'} flexWrap={'wrap'} alignitems={'center'} justifyContent={'center'}>
                <UploadItem
                    boxId={data.box.id}
                    src={(data.thumbnail) ? data.thumbnail.image : ''}
                    width={serverFileTypes.Image.Thumbnail.width}
                    height={serverFileTypes.Image.Thumbnail.height}
                    type={serverFileTypes.Image.Thumbnail.type}
                    onSelected={(file) => {
                        onChange(file);
                    }}/>
            </Box>
        </FormContainer>

    )
}

function SidePanelActions({data, onSaveClick, ...props}) {
    const theme = useTheme();

    return (
        <Box my={0.5} {...props}>
            <BaseButton size={'small'}
                        onClick={() => onSaveClick()}
                        style={{
                            backgroundColor: cyan[300],
                            color: '#fff',
                            width: '100%',
                        }}>
                <Typography color={'#fff'} py={1.5} variant={'h6'}
                            display={'flex'} alignItems={'center'}
                            style={{
                                display: 'flex',
                                alignItems: 'center'
                            }}>
                    <Save fontSize={"large"} style={{paddingRight: theme.spacing(1)}}/>
                    ذخیره
                </Typography>
            </BaseButton>
        </Box>
    )
};

function ActiveProduct({id, productDisable}) {
    const [disable, setDisable] = useState(productDisable);
    const [state, setState] = useState({
        loading: false
    });
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();

    useEffect(() => {
        setDisable(productDisable);
    }, [productDisable])


    function handleActiveStorage() {
        setState({
            ...state,
            loading: true
        });

        ControllerProduct.Product.activeProduct({pId: id, disable: !disable}).then(res => {
            if (res.status === 202) {
                setDisable(!disable)
            }
            setState({
                ...state,
                loading: false
            });
        }).catch(() => {
            setState({
                ...state,
                loading: false
            });
        })
    }

    return (
        <React.Fragment>
            {(id) &&
            <Box width={1} my={0.5}>
                <Box component={Card} display={'flex'} alignItems={'center'} justifyContent={'center'} p={1}>
                    <Box display={'flex'} justifyContent={'center'} width={1}>
                        <FormControlLabel
                            control={
                                <Switch
                                    checked={!disable}
                                    onChange={handleActiveStorage}
                                    color="primary"/>
                            }
                            label={<Typography
                                variant={'body2'}>فعال سازی محصول</Typography>}
                            labelPlacement={'top'}/>
                    </Box>
                </Box>
            </Box>}
        </React.Fragment>
    )
};

const styles = {
    review : {padding:"16px",margin:"8px",background:"#fff4e5",color:"#663c00"}
}

function Review({data: da, ...props}) {
    const [data, setData] = useState(da.review)
    const [open, setOpen] = useState(false)

    function updateData() {
        setData(undefined)
        ControllerProduct.Product.get({pId: da.id}).then(res => {
            setData(res.data.review)
        })
    }

    return <React.Fragment>
        {(data && _.isArray(data.chats) && !_.isEmpty(data.chats)) ?
        <FormContainer component={Card}
                       openDef={false}
                       title={"بازبینی (برای ویرایش کلیک کنید)"}
                       my={0.5}>
            <Box
                display={'flex'}
                flexDirection={'column'}
                width={1}
                onClick={() => {
                    setOpen(true)
                }}
                style={{
                    cursor: 'pointer'
                }}>
                <ReviewItems maxHeight={300} items={data.chats}/>
            </Box>
        </FormContainer> :
        <BaseButton
            style={styles.review}
            onClick={() => {setOpen(true)}}>بازبینی محصول</BaseButton>}
        <ProductReviewSingle
            productId={da.id}
            open={open}
            onClose={(e) => {
                tryIt(() => {
                    e.stopPropagation()
                })
                updateData()
                setOpen(false)
            }}/>
    </React.Fragment>
}

function Review2({data, ...props}) {
    const {roll} = useSelector(state => state.user);
    const isContentManager = roll === "content_manager" || roll === "superuser";
    return (
        isContentManager ?
            <FormContainer
                title={"بازبینی محصول"}
                my={0.5}>
                <Box px={3} py={1.5}
                     display={'flex'}
                     width={1}>
                    <MultiTextField
                        label={"دلیل بازبینی محصول"}
                        placeholder={"در صورت پر بودن این قسمت محصول به بازبینی تبدیل میشود."}
                        rows={4}
                        name={"review"}
                        defaultValue={getSafe(() => data.review, "")}
                    />
                </Box>
            </FormContainer> :
            <React.Fragment/>
    )
}


//region Category

const FullScreenTransition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

function SelectCategory({data: da, onChange, ...props}) {
    const theme = useTheme();
    const {categories: item, box} = da;

    const d = ControllerProduct.Categories.getAllCategories({});
    const apiKey = d[0] + box.id;
    const {data, error} = useSWR(apiKey, () => {
        return d[1]({
            boxId: box.id,
        }).then(res => {
            return res.categories;
        })
    });
    const [state, setState] = useState({
        addDialog: false,
    });

    function getData() {
        mutate(apiKey);
    }

    function handleCategoryChange(cat) {
        const newList = item ? item : [];
        const remove = _.remove(newList, (c) => {
            return c.id === cat.id
        });

        if (_.isEmpty(remove)) {
            newList.push(cat);
        }
        onChange(newList);
        mutate()
    }


    const itemParentsIdList = getParents();

    function getParents() {
        const list = [];
        _.forEach(item, (i) => {
            list.push(i.id)
        });
        return getActiveParents(list, data);
    }

    return (
        !error ?
            data ?
                <FormContainer title={lang.get("category")}
                               openDef={_.isEmpty(item)}
                               my={0.5}>
                    <Box display={'flex'} flexDirection={'column'} width={1}>
                        <Box display={'flex'} justifyContent={'center'} alignItems={'center'}>
                            <Tooltip title={'افزودن'}>
                                <IconButton onClick={() => setState({
                                    ...state,
                                    addDialog: true
                                })}
                                            style={{
                                                marginRight: theme.spacing(0.5)
                                            }}>
                                    <Add/>
                                </IconButton>
                            </Tooltip>
                            <Typography variant={'h6'} py={2} display={'flex'} justifyContent={'center'}
                                        alignItems={'center'}
                                        style={{
                                            flex: 1,
                                            ...UtilsStyle.disableTextSelection()
                                        }}>
                                انتخاب دسته
                            </Typography>
                            <Tooltip title={'بروزرسانی'}>
                                <IconButton onClick={getData}
                                            style={{
                                                marginLeft: theme.spacing(0.5)
                                            }}>
                                    <Sync/>
                                </IconButton>
                            </Tooltip>
                        </Box>
                        <Box display={'flex'} flexDirection={'column'} px={2}
                             style={{
                                 height: 300,
                                 overflowY: 'auto'
                             }}>
                            <CategoryContainer
                                activeItems={item}
                                items={data}
                                activeParents={itemParentsIdList ? itemParentsIdList : []}
                                onCategoryChange={handleCategoryChange}/>
                        </Box>
                        <Dialog fullScreen open={state.addDialog} TransitionComponent={FullScreenTransition}>
                            <CategorySingle
                                box={box}
                                onClose={(cat) => {
                                    if (cat) {
                                        handleCategoryChange(cat);
                                        getData();
                                    }
                                    setState({
                                        ...state,
                                        addDialog: false
                                    })
                                }}/>
                        </Dialog>
                    </Box>
                </FormContainer> :
                <Box width={1} py={0.5}>
                    <Skeleton height={90} variant={"rect"}/>
                </Box> :
            <ComponentError
                tryAgainFun={getData}/>
    )
}

function CategoryContainer({activeItems, items, level = 0, activeParents = [], onCategoryChange, ...props}) {

    const theme = useTheme();
    const darkMode = theme.palette.type === "dark";
    return (
        items ?
            <Box width={1} px={1} py={1}
                 boxShadow={1}
                 style={{
                     backgroundColor: level !== 0 ? grey[darkMode ? (800 - (level * 100)) : level * 100] : null,
                     ...UtilsStyle.borderRadius('0 0 5px 5px')
                 }}>
                {
                    items.map((cat, i) => (
                        <CategoryItem key={cat.id} index={i} activeItems={activeItems}
                                      activeParents={activeParents}
                                      item={cat} level={level}
                                      onCategoryChange={onCategoryChange}/>
                    ))
                }
            </Box> :
            <React.Fragment/>
    )
}

function CategoryItem({activeItems, index, item, level = 0, activeParents, onCategoryChange, ...props}) {
    const [state, setState] = useState({
        open: false,
    });

    const isActive = _.findIndex(activeItems, (i) => i.id === item.id) !== -1;
    const isParent = !isActive && _.findIndex(activeParents, (p) => p.id === item.id) !== -1;

    return (
        <React.Fragment>
            {index !== 0 &&
            <Divider style={{
                opacity: 0.5
            }}/>}
            <Box width={1} display={'flex'} alignItems={'center'}>
                <ButtonBase
                    onClick={() => onCategoryChange(item)}
                    style={{
                        cursor: 'pointer',
                        display: 'flex',
                        flexWrap: 'wrap',
                        justifyContent: 'flex-start',
                        flex: 1,
                        ...UtilsStyle.disableTextSelection(),
                    }}>
                    <Box key={item.id} py={1} display={'flex'} flex={'wrap'} alignItems={'center'}>
                        <Checkbox
                            checked={isActive}
                            value={item.id}
                            indeterminate={isParent}
                            name={`category-${item.id}`}
                            inputProps={{'aria-label': `category-${item.id}`}}
                            style={{
                                color: isActive ? red[400] : isParent ? red[200] : null
                            }}
                        />
                        <Typography variant={'body1'}>
                            {item.name[activeLang]}
                        </Typography>
                    </Box>
                </ButtonBase>
                {!_.isEmpty(item.child) &&
                <Box mx={0.5}>
                    <Tooltip title={"نمایش فرزندان"}>
                        <IconButton onClick={() => setState({
                            ...state,
                            open: !state.open
                        })}>
                            {state.open ? <ExpandLess/> : <ExpandMore/>}
                        </IconButton>
                    </Tooltip>
                </Box>}
            </Box>
            {!_.isEmpty(item.child) &&
            <Collapse in={state.open}>
                <CategoryContainer activeItems={activeItems} items={item.child}
                                   activeParents={activeParents}
                                   onCategoryChange={onCategoryChange}
                                   level={level + 1}/>
            </Collapse>
            }
        </React.Fragment>
    )
}

//endregion Category

function SelectBrand({active, data: dd, onChange, ...props}) {
    const {brand: item, box} = dd;
    const theme = useTheme();

    const [state, setState] = useState({
        addDialog: false,
    });
    const [defValue, setDefValue] = useState("")
    const [value, setValue] = useState("")
    const [inputValue, setInputValue] = useState("")
    const d = ControllerProduct.Brands.get();
    const apiKey = d[0] + box.id + "-" + value;
    const {data, error} = useSWR(apiKey, () => {
        return d[1]({
            boxId: box.id,
            q: value,
            step: 100,
            all: true
        })
    });

    function handleBrandChange(brand) {
        onChange(brand);
    }

    return (
        <FormContainer
            my={0.5}
            openDef={!!item}
            active={active}
            title={lang.get("brand")}>
            {data ?
                <React.Fragment>
                    <Box display={'flex'} flexDirection={'column'} width={1}>
                        <Box display={'flex'} justifyContent={'center'} alignItems={'center'} flexWrap={'wrap'}>
                            <Tooltip title={'افزودن'}>
                                <IconButton
                                    onClick={() => setState({
                                        ...state,
                                        addDialog: true
                                    })}
                                    style={{
                                        marginRight: theme.spacing(0.5)
                                    }}>
                                    <Add/>
                                </IconButton>
                            </Tooltip>
                            <Typography variant={'h6'} py={2} display={'flex'} justifyContent={'center'}
                                        alignItems={'center'}
                                        style={{
                                            flex: 1,
                                            ...UtilsStyle.disableTextSelection()
                                        }}>
                                <Tooltip title={'حذف'}>
                                    <IconButton onClick={() => handleBrandChange(null)}
                                                disabled={!item}
                                                style={{
                                                    color: red[400],
                                                    marginLeft: theme.spacing(0.5)
                                                }}>
                                        <DeleteOutlineOutlined/>
                                    </IconButton>
                                </Tooltip>
                                برند: {(item && item.name) ? item.name[activeLang] : '------'}
                            </Typography>
                            <Box width={1} px={2} pb={2}>
                                <TextFieldContainer
                                    name={createName({name: "tag-name"})}
                                    defaultValue={defValue}
                                    onChange={(val) => {
                                        setInputValue(val)
                                    }}
                                    render={(ref, {name: inputName, initialize, valid, errorIndex, inputProps, style, props}) => {
                                        return (
                                            <TextField
                                                {...props}
                                                variant={'outlined'}
                                                error={!valid}
                                                name={inputName}
                                                inputRef={ref}
                                                fullWidth
                                                onKeyPress={(e) => {
                                                    try {
                                                        if (e.key === 'Enter') {
                                                            const val = ref.current.getAttribute(textFieldNewValue)
                                                            setValue(val)
                                                            setDefValue(val)
                                                        }
                                                    } catch (e) {
                                                    }
                                                }}
                                                endAdornment={(inputValue) && (
                                                    <IconButton onClick={() => {
                                                        try {
                                                            setDefValue("");
                                                            setValue("");
                                                            setInputValue("");
                                                        } catch (e) {
                                                        }
                                                    }}>
                                                        <CancelOutlined/>
                                                    </IconButton>
                                                )}
                                                label={'جست‌و‌جو برند'}
                                                placeholder={'بعد تایپ نام برند enter بزنید'}
                                                style={{
                                                    ...style,
                                                    minWidth: '90%'
                                                }}
                                                inputProps={{
                                                    ...inputProps,
                                                }}/>
                                        )
                                    }}/>
                            </Box>
                        </Box>
                        <Box display={'flex'} flexDirection={'column'} mx={2}
                             style={{
                                 height: 300,
                                 overflowY: 'auto'
                             }}>
                            {(data.data) && data.data.map((b, i) => (
                                <React.Fragment key={b.id}>
                                    <Divider style={{
                                        opacity: 0.5
                                    }}/>
                                    <ButtonBase
                                        onClick={() => handleBrandChange(b)}
                                        style={{
                                            cursor: 'pointer',
                                            display: 'flex',
                                            justifyContent: 'flex-start',
                                            ...UtilsStyle.disableTextSelection(),
                                        }}>
                                        <Box key={b.id} py={1} display={'flex'} flex={'wrap'} alignItems={'center'}>
                                            <Radio
                                                checked={item && b.id === item.id}
                                                value="b"
                                                name="radio-button-demo"
                                                inputProps={{'aria-label': 'B'}}
                                            />
                                            <Typography variant={'body1'}>
                                                {b.name[activeLang]}
                                            </Typography>
                                        </Box>
                                    </ButtonBase>
                                </React.Fragment>
                            ))}
                        </Box>
                    </Box>
                    <EditBrand open={state.addDialog}
                               onClose={(br) => {
                                   if (br) {
                                       mutate(apiKey);
                                       handleBrandChange(br)
                                   }
                                   setState({
                                       ...state,
                                       addDialog: false
                                   })
                               }}/>
                </React.Fragment> :
                active ?
                    <Box width={1} py={0.5}>
                        <Skeleton height={90} variant={"rect"}/>
                    </Box> :
                    <React.Fragment/>}
        </FormContainer>
    )
}

function PanelFeatures({data, onChange, ...props}) {
    return (
        <FormContainer component={Card}
                       openDef={true}
                       title={"امکانات"}
                       my={0.5}>
            <Box px={3} py={1.5} display={'flex'} flexWrap={'wrap'}>
                {
                    Object.keys(data).map((elKey, i) => {
                        const el = data[elKey];
                        return (
                            <React.Fragment key={elKey}>
                                {(el && _.isBoolean(el.active)) &&
                                <ItemContainer width={0.5} justifyContent={'center'}>
                                    <FormControlLabel
                                        control={
                                            <Switch
                                                checked={el.active}
                                                onChange={() => {
                                                    onChange(elKey)
                                                }}
                                                value={elKey}
                                                color="primary"/>
                                        }
                                        label={<Typography
                                            variant={'body2'}>{lang.get(el.label ? el.label : elKey)}</Typography>}
                                        labelPlacement={'top'}/>
                                </ItemContainer>}
                            </React.Fragment>
                        )
                    })
                }
            </Box>
        </FormContainer>
    )
}

function ProductSetting({data: da}) {
    const {settings: data} = da
    const [productStatus, setProductStatus] = useState(_.toString(data.productStatus || 0));


    return (
        <FormContainer my={0.5} title={"تنظیمات"}>
            <Box display={'flex'} flexDirection={'column'}>
                <Box p={2}>
                    <NoneTextField
                        name={createName({group: 'settings', name: "productStatus"})}
                        defaultValue={productStatus}/>
                    <MaterialFormControl component="fieldset">
                        <FormLabel component="legend">
                            <Typography pb={0.75} variant={"body1"}>
                                وضعیت محصول
                            </Typography>
                            <Typography pb={2} variant={'caption'}>
                                این بخش جهت داشتن آمار وضعیت محصول میباشد و به کاربران نشان داده نخواهد شد.
                                <br/>
                                درصورتی که محصول به یک حد قابل قبول از اطلاعات رسیده است وضعیت مطلوب را انتخاب کنید.
                                <br/>
                                درصورتی که محصول نیاز به هیچ تغییری ندارد و به کامل ترین حالت اطلاعات خود رسیده است
                                وضعیت کامل را انتخاب کنید.
                            </Typography>
                        </FormLabel>
                        <RadioGroup row aria-label="productStatus" value={productStatus}
                                    onChange={(event) => {
                                        setProductStatus(event.target.value)
                                    }}>
                            <FormControlLabel
                                value="0"
                                control={<Radio color="primary"/>}
                                label="ناقص"
                                labelPlacement="top"/>
                            <FormControlLabel
                                value="1"
                                control={<Radio color="primary"/>}
                                label="مطلوب"
                                labelPlacement="top"/>
                            <FormControlLabel
                                value="2"
                                control={<Radio color="primary"/>}
                                label="کامل"
                                labelPlacement="top"/>
                        </RadioGroup>
                    </MaterialFormControl>
                </Box>
                <Box p={2}>
                    <MultiTextField
                        label={"یادداشت"}
                        placeholder={"یک متن برای یاد داشت خودتان بنویسد. (این قسمت به کاربر نشان داده نخواهد شد)"}
                        group={"settings"}
                        name={"adminNote"}
                        defaultValue={data.adminNote || ""}
                        rows={4}
                    />
                </Box>
            </Box>
        </FormContainer>
    )
}

function Actions({data, onChange}) {

    return (
        <FormContainer my={0.5} title={"دیگر عملیات"}>
            <Box display={'flex'} flexDirection={'column'}>
                <Box p={2} display={'flex'} flexWrap={'wrap'}>
                    {
                        Object.keys(productDescription).map(k => {
                            const item = productDescription[k]
                            return (
                                <Box p={1} key={k}>
                                    <BaseButton
                                        disabled={
                                            Boolean(_.find(data.description.data[siteLang].items, (i) => {
                                                return i.key === item.key
                                            }))}
                                        onClick={() => {
                                            try {
                                                onChange({
                                                    ...data,
                                                    description: {
                                                        ...data.description,
                                                        data: {
                                                            ...data.description.data,
                                                            fa: {
                                                                ...data.description.data.fa,
                                                                items: [
                                                                    ...data.description.data.fa.items,
                                                                    {
                                                                        key: item.key,
                                                                        value: ""
                                                                    }
                                                                ]
                                                            }
                                                        }
                                                    }
                                                })
                                            } catch (e) {
                                            }
                                        }}
                                        style={{
                                            backgroundColor: cyan[400],
                                            color: "#fff"
                                        }}>
                                        {item.label}
                                    </BaseButton>
                                </Box>
                            )
                        })
                    }
                </Box>
            </Box>
        </FormContainer>
    )
}

//endregion SidePanel

function Name({item}) {
    const theme = useTheme();
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();
    const {id, name, permalink: pr} = item;
    const [nameRef, setNameRef] = useState();
    const [permalinkRef, setPermalinkRef] = useState();
    const [state, setState] = useState({
        edit: !Boolean(item.id),
        loading: false
    })
    const [permalink, setPermalink] = useState(pr | "");

    useEffect(() => {
        setPermalink(pr || "")
    }, [pr])

    function onSaveHandler() {
        try {
            if (!state.edit) {
                setState({
                    ...state,
                    edit: true
                });
                return;
            }
            const permalink = permalinkRef.current.value

            if (_.isEmpty(permalink)) {
                enqueueSnackbar(`لظفا فیلد ${lang.get('permalink')} را پر کنید.`,
                    {
                        variant: "error",
                        action: (key) => (
                            <Fragment>
                                <Button onClick={() => {
                                    closeSnackbar(key)
                                }}>
                                    {lang.get('close')}
                                </Button>
                            </Fragment>
                        )
                    });
                return;
            }

            if (id) {
                setState({
                    ...state,
                    loading: true
                });
                ControllerProduct.Product.updatePermalink({pId: id, permalink: permalink}).then((res) => {
                    setPermalink(permalink);
                    setState({
                        ...state,
                        loading: false,
                        edit: false
                    });
                }).catch((e) => {
                    setState({
                        ...state,
                        loading: false
                    });
                });
                return;
            }
            setPermalink(permalink)
            setState({
                ...state,
                edit: false
            })
        } catch (e) {
            enqueueSnackbar(lang.get("er_error"),
                {
                    variant: "error",
                    action: (key) => (
                        <Fragment>
                            <Button onClick={() => {
                                closeSnackbar(key)
                            }}>
                                {lang.get('close')}
                            </Button>
                        </Fragment>
                    )
                });
        }
    }


    return (
        <Box display={'flex'} flexDirection={'column'}>
            <ItemContainer mt={0}>
                <TextFieldMultiLanguageContainer
                    name={"name"}
                    render={(ref, {lang: inputLang, name: inputName, initialize, valid, errorIndex, setValue, inputProps, style, props}) => {
                        if (inputLang.key === "fa")
                            setNameRef(ref)
                        return (
                            <TextField
                                {...props}
                                error={!valid}
                                variant="outlined"
                                name={inputName}
                                defaultValue={name[inputLang.key]}
                                fullWidth
                                helperText={errorList[errorIndex]}
                                inputRef={ref}
                                required={inputLang.key === siteLang}
                                label={lang.get('product_name')}
                                style={style}
                                inputProps={{
                                    ...inputProps
                                }}/>
                        )
                    }}/>
            </ItemContainer>
            <Box display={'flex'} alignItems={state.edit ? 'flex-end' : 'center'} width={'100%'}>
                {
                    (!state.edit) ?
                        <ItemContainer width={"auto"}>
                            <NoneTextField
                                name={"permalink"}
                                defaultValue={permalink}/>
                            <Typography variant={'subtitle1'}>
                                <Box display={'flex'} alignItems={'center'}>
                                    {lang.get("permalink")}:
                                    <Typography component={item.id ? 'a' : 'span'}
                                                variant={'subtitle1'}
                                                color={theme.palette.text.disabled}
                                                mr={1}
                                                target={'_blank'}
                                                dir={'ltr'}
                                                href={webRout + siteRout.Product.Single.create({permalink: permalink})}>
                                        {webRout + siteRout.Product.Single.create({permalink: permalink})}
                                    </Typography>
                                </Box>
                            </Typography>
                        </ItemContainer> :
                        <ItemContainer>
                            <PermalinkTextField
                                name={"permalink"}
                                variant={"standard"}
                                defaultValue={item.permalink}
                                syncedRef={nameRef}
                                onRefChange={ref => setPermalinkRef(ref)}/>
                        </ItemContainer>
                }
                <BaseButton
                    variant={'outlined'}
                    size={"superSmall"}
                    fontWeight={300}
                    loading={state.loading}
                    onClick={onSaveHandler}
                    style={{
                        border: `1px solid ${state.loading ? grey[400] : cyan[300]}`,
                        marginRight: theme.spacing(1.5),
                    }}>
                    {lang.get(!state.edit ? 'edit' : 'submit')}
                </BaseButton>
            </Box>
        </Box>
    )
}

function ProductType({item}) {
    const {id, type: ty, booking_type} = item;
    const theme = useTheme();
    const [type, setType] = useState(ty || {})
    useEffect(() => {
        setType(ty || {})
    }, [ty])


    return (
        <ItemContainer flexWrap={'wrap'} alignItems={'center'}>
            <Typography variant={'body1'} pl={2}>
                {lang.get("type")}:
            </Typography>
            <NoneTextField name={"product_type"} defaultValue={type.type}/>
            {id ?
                <Typography variant={'body1'} fontWeight={600} pr={1}>
                    {type.label}
                </Typography> :
                (
                    <RadioGroup
                        row aria-label="position" name="position"
                        value={type.value}
                        onChange={(e, type) => {
                            setType(productType[type])
                        }}>
                        {
                            Object.keys(productType).map((key, index) => {
                                const item = productType[key];
                                return (
                                    <FormControlLabel
                                        key={key}
                                        value={item.type}
                                        control={<Radio color="primary"/>}
                                        label={<Typography color={theme.palette.text.primary}>{item.label}</Typography>}
                                        labelPlacement="end"
                                    />
                                )
                            })
                        }
                    </RadioGroup>
                )}
            <BookingType type={type} bookingType={booking_type}/>
        </ItemContainer>
    )
}

export function BookingType({type, bookingType: bType, dontCheckType, onChange, ...props}) {

    const [anchorEl, setAnchorEl] = React.useState(null);
    const [bkType, setBkType] = useState(bType || bookingType.unbookable);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = (bookingType) => {
        if (bookingType)
            setBkType(bookingType)
        if (onChange)
            onChange(bookingType)
        setAnchorEl(null);
    };

    return (
        (dontCheckType || (type && (type.isProduct || type.isPackage))) ?
            <Box pl={2} display={'flex'} alignItems={'center'} {...props}>
                <Typography pl={1}>
                    رزرو:
                </Typography>
                <Button aria-controls="select-booking-type-menu" aria-haspopup="true" onClick={handleClick}>
                    {bkType.label}
                </Button>
                <MaterialMenu
                    id="select-booking-type-menu"
                    anchorEl={anchorEl}
                    keepMounted
                    open={Boolean(anchorEl)}
                    onClose={() => {
                        handleClose()
                    }}>
                    {
                        Object.keys(bookingType).map(k => {
                            const it = bookingType[k];
                            return (
                                <MenuItem key={k} onClick={() => handleClose(it)}>
                                    {it.label}
                                </MenuItem>
                            )
                        })
                    }
                </MaterialMenu>
                <NoneTextField name={"booking_type"} defaultValue={bkType.key}/>
            </Box> :
            <React.Fragment/>
    )
}

//region Media

function Media({item, onChange}) {

    const {box, media} = item;
    const [state, setState] = useState({
        sorting: false
    })

    return (
        <FormContainer title={lang.get("media")} boxProps={{flexWrap: 'wrap'}}>
            <Box py={2} px={2} width={1}>
                {lang.get("sorting")}: <Switch value={state.sorting} disabled={!media || media.length < 2}
                                               onChange={(v) => {
                                                   setState({
                                                       ...state,
                                                       sorting: !state.sorting
                                                   })
                                               }}/>
            </Box>
            {state.sorting ?
                <Box width={1}>
                    <DraggableList
                        items={media}
                        onItemsChange={(items) => {
                            onChange(items);
                        }}
                        rootStyle={(dragging) => {
                            return {
                                backgroundColor: dragging ? null : grey[200],
                                ...UtilsStyle.transition(500)
                            }
                        }}
                        render={(item, props, {isDraggingItem, ...p}) => (
                            <Box key={item.id}
                                 display={'flex'}
                                 width={1}
                                 {...props}>
                                <Box display={'flex'} alignItems={'center'}
                                     py={1}>
                                    <Box px={2} py={1}>
                                        {isDraggingItem ?
                                            <MenuOpen/> :
                                            <Menu/>
                                        }
                                    </Box>
                                    <Img maxWidth={70} minHeight={'unset'} src={item.image}/>
                                    <Typography pr={2} pl={1} variant={'body1'} fontWeight={600}
                                                style={{
                                                    justifyContent: 'center',
                                                }}>
                                        {item.title.fa}
                                    </Typography>
                                </Box>
                            </Box>
                        )}
                    />
                </Box> :
                <React.Fragment>
                    {media.map(m => (
                        <MediaItem key={m.id}
                                   item={m}
                                   onRemove={() => {
                                       const newList = [...media];
                                       _.remove(newList, d => d.id === m.id);
                                       onChange(newList);
                                   }}/>
                    ))}
                    <Box width={1 / 3} px={2} py={2} display={'flex'} alignItems={'center'}
                         justifyContent={'center'}>
                        <UploadItem
                            boxId={box.id}
                            holderWidth={200}
                            holderHeight={'auto'}
                            multiSelect={true}
                            width={serverFileTypes.Image.Media.width}
                            height={serverFileTypes.Image.Media.height}
                            type={serverFileTypes.Image.Media.type}
                            onSelected={(files) => {
                                _.forEach(media, (d) => {
                                    _.remove(files, (f) => {
                                        return f.id === d.id;
                                    })
                                })
                                onChange([...media, ...files])
                            }}/>
                    </Box>
                </React.Fragment>}

        </FormContainer>
    )
}


function MediaItem({item, onRemove, ...props}) {
    const image = getSafe(() => item.media.image, item.image)
    return (
        <Box px={2} py={2} width={1 / 3}>
            <Box component={Card} display={'flex'} flexDirection={'column'}>
                <Img zoomable={true} src={image}/>
                <Box display={'flex'} flexWrap={'wrap'} py={1} px={1} alignItems={'center'}>
                    <Box pr={1}>
                        <IconButton onClick={() => onRemove()}>
                            <Delete fontSize={"small"}/>
                        </IconButton>
                    </Box>
                    <Typography variant={'body1'} fontWeight={600} style={{
                        justifyContent: 'center',
                    }}>
                        {item.title.fa}
                    </Typography>
                </Box>
            </Box>
        </Box>
    )
}

//endregion Media

//region Features

function sortFeatures(features) {
    const text = [];
    const bool = [];
    const selectable = [];
    _.forEach(features, (f) => {
        switch (f.feature.type.key) {
            case featureType.bool.key: {
                bool.push(f)
                break
            }
            case featureType.selectable.key: {
                selectable.push(f)
                break
            }
            default: {
                text.push(f)
            }
        }
    })

    return [...text,
        ...bool,
        ...selectable]
}

function Features({productId, active, item, onChange, ...props}) {
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();
    const {address, shortAddress} = item;
    const [openOrder, setOpenOrder] = useState(false)
    const [productFeatures, setProductFeatures] = useState(sortFeatures(item.features));
    const [state, setState] = useState({
        activeLanguage: sLang[siteLang]
    })
    const [openSearchFeature, setOpenSearchFeature] = useState(false)
    const [productGroupDialog, setProductGroupDialog] = useState(false)
    const [featureGroupData, setFeatureGroupData] = useState([])


    // useEffect(() => {
    //     return ()=>onChange({features: productFeatures})
    // }, productFeatures)/

    function handleOnClose() {
        setOpenSearchFeature(false)
    }

    function NotRemovableAlert(values) {
        values.map(value => (
            enqueueSnackbar(`فیچر ${value.name[siteLang]} در انبار استفاده شده و قابل حذف نمی باشد`,
                {
                    variant: "error",
                    action: (key) => (
                        <Fragment>
                            <Button onClick={() => {
                                closeSnackbar(key)
                            }}>
                                {lang.get('close')}
                            </Button>
                        </Fragment>

                    )
                })
        ))

    }

    function handleRemoveFeature(it) {
        const elementsIndex = productFeatures.findIndex(element => element.feature.id === it.feature.id)
        if (elementsIndex !== -1) {
            const notRemovable = _.filter(productFeatures[elementsIndex].values, it => !_.isEmpty(it.storage_id))
            if (notRemovable.length === 0) {
                const newArray = _.cloneDeep(productFeatures).filter(item => item.feature.id !== it.feature.id)
                setProductFeatures(sortFeatures(newArray))
                onChange({features: newArray})
                return
            }
            const newArray = _.cloneDeep(productFeatures)
            NotRemovableAlert(notRemovable)
            newArray[elementsIndex].values = notRemovable
            setProductFeatures(sortFeatures(newArray))
            onChange({features: newArray})


        }
    }

    function handleSelection(selectedFeature, selectedValues, type = null) {

        const elementsIndex = _.isUndefined(productFeatures) ? -1 : productFeatures.findIndex(element => element.feature.id === selectedFeature.id)
        if (elementsIndex !== -1) {
            let newArray = _.cloneDeep([...productFeatures])
            const notRemovable = _.filter(productFeatures[elementsIndex].values, it => !_.isEmpty(it.storage_id))
            let o = notRemovable
            selectedValues.map(value => {
                    const index = _.findIndex(notRemovable, item => item.id === value.id)
                    if (index === -1) {
                        o = o.concat(value)
                    }

                }
            )
            newArray[elementsIndex] = {...newArray[elementsIndex], values: o}
            setProductFeatures(sortFeatures(newArray))
            setOpenSearchFeature(false)
            onChange({features: newArray})
            return
        }

        let newArray = undefined
        setProductFeatures(d => {
            newArray = d.concat([{feature: selectedFeature, values: selectedValues}])
            return sortFeatures(newArray)
        })
        onChange({features: newArray})

        setOpenSearchFeature(false)


    }

    function handleGroupSelection(data) {
        data.map(featureValue => {

            const fIndex = _.findIndex(productFeatures, f => f.feature.id === featureValue.feature.id)
            if (fIndex === -1) {
                setProductFeatures(d => {
                        let o = d.concat(featureValue)
                        onChange({features: o})
                        setProductGroupDialog(false)
                        return sortFeatures(o)
                    }
                )
                /* gcLog("kkkkkkkkkkkkkkkkk productFeatures",productFeatures)




                 let dd = _.cloneDeep(productFeatures)
                 dd = dd.concat(featureValue)
                 setProductFeatures(dd)
                 gcLog("kkkkkkkkkkkkkkkkk",dd)
                 onChange({features: dd})
                 setProductGroupDialog(false)*/


                return
            }
            const f = productFeatures[fIndex]

            //todo add selected to the values and show snack bar
            const notRemovable = _.filter(f.values, it => (it.selected))

            let o = notRemovable

            featureValue.values.map(value => {
                    const index = _.findIndex(notRemovable, item => item.id === value.id)
                    if (index === -1) {
                        o = o.concat(value)
                    }

                }
            )

            f.values = o

            setProductFeatures(dd => {
                dd[fIndex] = f
                onChange({features: dd})
                setProductGroupDialog(false)
                return sortFeatures(dd)
            })


        })
    }

    function openGroupFeature() {
        if (productId === undefined) {
            enqueueSnackbar(`برای دیدن گروه دسته بندی اول محصول را ذخیره کنید`,
                {
                    variant: "error",
                    action: (key) => (
                        <Fragment>
                            <Button onClick={() => {
                                closeSnackbar(key)
                            }}>
                                {lang.get('close')}
                            </Button>
                        </Fragment>

                    )
                })
        }
        setProductGroupDialog(s => !s)
    }


    return (
        <FormContainer
            active={active}
            title={"ویژگی"}
            name={"feature"}
            activeLanguageValue={[address, shortAddress]}
            activeLanguage={state.activeLanguage}
            onLanguageChange={(lang) => {
                setState({
                    ...state,
                    activeLanguage: lang
                })
            }}
            boxProps={{
                flexDirection: 'column'
            }}>
            <Box display={"flex"}>
                <BaseButton fullWidth={true}
                            variant={'outlined'}
                            style={{margin: "8px"}}
                            onClick={() => {
                                setOpenSearchFeature(s => (!s))
                            }}>
                    جست و جوی ویژگی
                </BaseButton>
                <BaseButton
                    fullWidth={true}
                    variant={'outlined'}
                    style={{margin: "8px"}}
                    onClick={openGroupFeature}>

                    افزودن ویژگی های دسته بدنی</BaseButton>

                <BaseButton
                    fullWidth={true}
                    variant={'outlined'}
                    style={{margin: "8px"}}
                    onClick={() => {
                        setOpenOrder(true)

                    }}>
                    مرتبسازی
                </BaseButton>
            </Box>
            {item.featureGroup &&
            <ProductFeatureGroup
                open={productGroupDialog}
                productFeatures={productFeatures}
                productId={productId}
                onClose={() => {
                    setProductGroupDialog(s => (!s))
                }}
                onDone={handleGroupSelection}/>}
            <SelectFeatureValue
                open={openSearchFeature}
                onClose={() => {
                    setOpenSearchFeature(s => (!s))
                }}
                onDone={handleSelection}/>
            <Box
                display={'flex'}
                flexDirection={'column'}>
                {productFeatures ?
                    productFeatures.map(it => (
                        <FeatureItem key={it.feature.id} it={it}
                                     handleSelection={handleSelection}
                                     onRemoveFeature={handleRemoveFeature}/>
                    )) : <React.Fragment/>}
            </Box>
            <OrderProductFeature
                open={openOrder}
                productId={productId}
                onClose={(saved) => {
                    setOpenOrder(false)
                    if (saved)
                        enqueueSnackbar("ذخیره شد. بعد از رفرش قابل مشاهده میباشد.",
                            {
                                variant: "info",
                                action: (key) => (
                                    <Fragment>
                                        <Button onClick={() => {
                                            closeSnackbar(key)
                                        }}>
                                            {lang.get('close')}
                                        </Button>
                                    </Fragment>
                                )
                            });
                }}/>
        </FormContainer>
    )
}

function FeatureItem({it, handleSelection, onRemoveFeature, ...props}) {
    const [loading, setLoading] = useState(false)
    const [textFeatureValue, setTextFeatureValue] = useState("")
    const [collapse, setCollapse] = useState(false)


    let type = tryIt(() => {
        return it.feature.type
    }, "")
    return (
        <Box display={'flex'} flexDirection={'row'} flex={1} alignItems={"center"} width={"100%"}>
            <Typography width={'30%'} alignItems={"center"} variant={'h5'} p={1}>
                {it.feature.name[siteLang]}
                {
                    (_.isObject(it.feature) && it.feature.type) &&
                    <React.Fragment>
                        <Typography variant={"body2"} px={0.5}>
                            ( {(_.isObject(it.feature.type) ? it.feature.type : featureType[it.feature.type]).typeLabel} )
                        </Typography>:
                    </React.Fragment>
                }
            </Typography>
            <Box width={'65%'} display={"flex"} alignItems={"center"} style={{overflow: 'auto'}}>
                {it.values.map(value => (
                    <React.Fragment key={value.id}>
                        <Typography justifyContent={"center"} alignItems={"center"} variant={'subtitle1'}
                                    justify={'center'} key={value.id} p={"10px"} style={{
                            color: _.isEmpty(value.storage_id) ? "#848383" : "#de3f3f"
                        }}>
                            {!(collapse && type.key === featureType.text.key) ? `   ${value.name[siteLang]}` : ""}
                        </Typography>
                        <Collapse in={collapse && type.key === featureType.text.key}
                                  style={{display: type.key === featureType.text.key ? "flex" : "none"}}>
                            <Box display={"flex"}
                                 flex={1}
                                 alignItems={"center"}>
                                <Box display={'flex'} flex={1} alignItems={"center"}
                                     m={2}
                                     style={{
                                         minWidth: "500px"
                                     }}>
                                    <DefaultTextField
                                        name={"search"}
                                        label={'متن ویژگی را وارد کنید'}
                                        multiline={true}
                                        rowsMax={4}
                                        variant={'outlined'}
                                        style={{margin: "10px"}}
                                        defaultValue={it.values[0].name[siteLang]}
                                        onChange={(text) => {
                                            setTextFeatureValue(text)
                                        }}
                                    />
                                    <BaseButton
                                        variant={'outlined'}
                                        loading={loading}
                                        style={{
                                            borderColor: cyan[300],
                                            margin: "12px"
                                        }}
                                        onClick={() => {
                                            // handleCheck(feature.values[0], feature)

                                            if (textFeatureValue !== "" && textFeatureValue !== it.values[0].name[siteLang]) {
                                                setLoading(true)
                                                ControllerProduct.Features.addFeatureValue({
                                                    fId: it.feature.id,
                                                    value: textFeatureValue
                                                }).then((response) => {
                                                    handleSelection(it.feature, [response.data.data], "text")
                                                    setCollapse(!collapse)
                                                }).finally(() => {
                                                    setLoading(false)
                                                })
                                            }
                                        }}>
                                        ذخیره
                                    </BaseButton>
                                </Box>


                            </Box>
                        </Collapse>
                    </React.Fragment>

                ))}

            </Box>
            <Box width={'5%'} display={"flex"} flexDirection={"flex-end"}>

                {
                    <IconButton style={{display: it.feature.type.key === "text" ? "block" : "none"}} onClick={() => {
                        setCollapse(!collapse)
                    }} color="primary" aria-label="upload picture"
                                component="span">
                        <Edit/>
                    </IconButton>
                }

                <IconButton onClick={() => {
                    onRemoveFeature(it)
                }
                } color="primary" aria-label="upload picture"
                            component="span">
                    <Delete/>
                </IconButton>

            </Box>
        </Box>
    )

}


//endregion

function ShortDescription({boxId, name, title, item: description, onRemove, onChange}) {

    return (
        <ItemContainer>
            <FormContainer
                width={1}
                title={title}
                style={{
                    overflow: 'inherit'
                }}>
                <Box display={'flex'} flexDirection={'column'} width={1} height={"150px"} m={1}>
                    {onRemove &&
                    <Box p={2}>
                        <BaseButton
                            onClick={() => {
                                onRemove()
                            }}
                            style={{
                                backgroundColor: theme.palette.error.main,
                                color: '#fff'
                            }}>
                            حذف
                        </BaseButton>
                    </Box>}
                    <DefaultTextFieldMultiLanguage
                        boxId={boxId}
                        name={name}
                        rowsMax={8}
                        multiline={true}
                        rows={8}
                        onChange={onChange}
                        defaultValue={description}/>
                </Box>
            </FormContainer>
        </ItemContainer>
    )
}

function Description({boxId, name, title, item: description, onRemove, onChange}) {

    return (
        <ItemContainer>
            <FormContainer
                width={1}
                title={title}
                style={{
                    overflow: 'inherit'
                }}>
                <Box display={'flex'} flexDirection={'column'}>
                    {onRemove &&
                    <Box p={2}>
                        <BaseButton
                            onClick={() => {
                                onRemove()
                            }}
                            style={{
                                backgroundColor: theme.palette.error.main,
                                color: '#fff'
                            }}>
                            حذف
                        </BaseButton>
                    </Box>}
                    <Editor
                        boxId={boxId}
                        name={name}
                        content={description[siteLang] || ""}/>
                </Box>
            </FormContainer>
        </ItemContainer>
    )
}

function Description2({title, item: description, onRemove, onChange}) {
    const theme = useTheme();
    const [data, setData] = useState({});
    const [orgDescription, setOrgDescription] = useState({
        firstTime: false
    });
    const [timer, setTimer] = useState(null);
    const [state, setState] = useState({
        activeLanguage: sLang[siteLang]
    })

    useEffect(() => {
        const newDescription = {};
        _.forEach(sLang, (l, key) => {
            newDescription[key] = htmlToEditorContent(description[key] ? description[key] : '')
        });
        setData(newDescription)
        return () => {
            clearTimeout(timer)
        }
    }, []);

    useEffect(() => {
        const newOrgDescription = orgDescription;
        if (!orgDescription.firstTime) {
            _.forEach(sLang, (l, key) => {
                newOrgDescription[key] = Boolean(description[key])
            });
            setOrgDescription({
                ...newOrgDescription,
                firstTime: true
            });
            return
        }
        newOrgDescription[state.activeLanguage.key] = Boolean(description[state.activeLanguage.key]);
        setOrgDescription({
            ...newOrgDescription,
        });
    }, [description]);

    function handlerDescriptionChange(content) {
        const newData = data;
        newData[state.activeLanguage.key] = content;
        setData({
            ...newData
        });

        clearTimeout(timer);
        setTimer(setTimeout(() => {
            const newData = description;
            newData[state.activeLanguage.key] = editorContentToHtml(content);
            onChange(newData)
        }, 4000))
    }

    return (
        <ItemContainer>
            <FormContainer
                width={1}
                title={title}
                activeLanguage={state.activeLanguage}
                activeLanguageValue={orgDescription}
                onLanguageChange={(lang) => {
                    setState({
                        ...state,
                        activeLanguage: lang
                    })
                }}
                style={{
                    overflow: 'inherit'
                }}>
                <Box display={'flex'} flexDirection={'column'}>
                    {onRemove &&
                    <Box p={2}>
                        <BaseButton
                            onClick={() => {
                                onRemove()
                            }}
                            style={{
                                backgroundColor: theme.palette.error.main,
                                color: '#fff'
                            }}>
                            حذف
                        </BaseButton>
                    </Box>}
                    <Editor
                        content={data[state.activeLanguage.key]}
                        onContentChange={handlerDescriptionChange}/>
                </Box>
            </FormContainer>
        </ItemContainer>
    )
}

function Cities({active, data, onChange, ...props}) {
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();
    const [state, setState] = useState({
        state: undefined,
        city: undefined,
    })
    const [selectedCities, setSelectedCities] = useState(data.cities)
    const apiKey = `cities-${state.state ? state.state.id : ""}`
    const {data: cities, error: citiesError} = useSWR(apiKey, () => {
        if (!state.state)
            return undefined
        return ControllerSite.cities.get({stateId: state.state.id}).then((res) => res.data.cities)
    }, {
        dedupingInterval: 60000 * 20
    })
    //TODO: ADD server data

    return (
        <FormContainer active={active}
                       title={lang.get("city")}>
            <Box display={'flex'} width={1} flexDirection={'column'}>
                <Box display={'flex'} alignItems={'flex-start'} width={1} px={2} py={2}>
                    <Box display={'flex'} flex={1}>
                        <Box width={0.5} pr={1}>
                            <AutoFill
                                inputId='stateInput'
                                items={states}
                                activeItem={state.state}
                                onActiveItemChanged={(item) => {

                                    setState({
                                        ...state,
                                        state: item,
                                        city: null
                                    });
                                }}
                                title={lang.get('state')}
                                placeholder={lang.get('search_state')}/>
                        </Box>
                        <Box width={0.5} pr={1}>
                            <AutoFill
                                inputId='stateInput'
                                items={cities || []}
                                activeItem={state.city}
                                disabled={!cities}
                                onActiveItemChanged={(item) => {
                                    setState({
                                        ...state,
                                        city: item
                                    });
                                }}
                                title={lang.get('city')}
                                placeholder={lang.get('search_state')}/>
                        </Box>
                    </Box>
                    <SuccessButton
                        variant={"outlined"}
                        onClick={() => {
                            if (_.findIndex(selectedCities, (cy) => {
                                return cy.id === state.city.id
                            }) !== -1) {
                                enqueueSnackbar(`شهر ${state.city.name} تکراری میباشد`,
                                    {
                                        variant: "error",
                                        action: (key) => (
                                            <Fragment>
                                                <Button onClick={() => {
                                                    closeSnackbar(key)
                                                }}>
                                                    {lang.get('close')}
                                                </Button>
                                            </Fragment>
                                        )
                                    });
                            } else {
                                setSelectedCities([
                                    ...selectedCities,
                                    {
                                        ...state.city,
                                        stateId: state.city.state,
                                        state: _.find(states, (s) => s.id === state.city.state)
                                    }
                                ])

                            }
                            setState({
                                ...state,
                                city: null,
                            })
                        }}
                        style={{
                            marginRight: theme.spacing(1)
                        }}>
                        <Add
                            style={{
                                marginLeft: theme.spacing(1)
                            }}/>
                        افزودن
                    </SuccessButton>
                </Box>
                <Box display={'flex'} flexWrap={'wrap'} p={2}>
                    {
                        selectedCities.map(ci => (
                            <Box key={ci.id} p={2}>
                                <NoneTextField name={createName({group: "cities", name: _.toString(ci.id)})}/>
                                <Box display={'flex'} minWidth={150} flexDirection={'column'} p={2}
                                     component={Card}>
                                    <Box display={'flex'} alignItems={'center'}>
                                        <Box display={'flex'} flexDirection={'column'} pr={2} flex={1}>
                                            <Typography variant={"h6"} fontWeight={500}>
                                                {ci.name}
                                            </Typography>
                                            <Typography variant={"body1"}>
                                                استان:
                                                {" " + ci.state.label}
                                            </Typography>
                                        </Box>
                                        <Tooltip title={"حذف"}>
                                            <IconButton
                                                onClick={() => {
                                                    const newList = selectedCities;
                                                    _.remove(newList, (cy) => {
                                                        return cy.id === ci.id
                                                    })
                                                    setSelectedCities([...newList])
                                                }}>
                                                <DeleteOutline/>
                                            </IconButton>
                                        </Tooltip>
                                    </Box>
                                </Box>
                            </Box>
                        ))
                    }
                </Box>
            </Box>
        </FormContainer>
    )
}

function Address({active, item, ...props}) {
    const {address, shortAddress} = item;
    const [state, setState] = useState({
        activeLanguage: sLang[siteLang]
    })

    return (
        <FormContainer
            active={active}
            title={lang.get("address")}
            activeLanguageValue={[address, shortAddress]}
            activeLanguage={state.activeLanguage}
            onLanguageChange={(lang) => {
                setState({
                    ...state,
                    activeLanguage: lang
                })
            }}
            boxProps={{
                flexDirection: 'column'
            }}>
            {Object.keys(sLang).map((langKey) => {
                const elLang = sLang[langKey];
                return (
                    <Box key={langKey} display={elLang.key !== state.activeLanguage.key ? 'none' : 'flex'} px={2}
                         py={1}>
                        <ItemContainer width={0.4} pr={2}>
                            <TextFieldContainer
                                name={createName({name: "shortAddress", langKey: elLang.key})}
                                defaultValue={shortAddress[elLang.key]}
                                render={(ref, {name: inputName, initialize, valid, errorIndex, setValue, inputProps, style, props}) => {
                                    return (
                                        <TextField
                                            {...props}
                                            error={!valid}
                                            name={inputName}
                                            helperText={errorList[errorIndex]}
                                            inputRef={ref}
                                            fullWidth
                                            required={elLang.key === siteLang}
                                            label={lang.get("short_address")}
                                            style={{
                                                ...style,
                                                minWidth: '90%'
                                            }}
                                            inputProps={{
                                                ...inputProps,
                                                ...createInputProps({langKey: elLang.key}),
                                            }}/>
                                    )
                                }}/>
                        </ItemContainer>
                        <ItemContainer width={0.6} pl={2}>
                            <TextFieldContainer
                                name={createName({name: "address", langKey: elLang.key})}
                                defaultValue={address[elLang.key]}
                                render={(ref, {name: inputName, initialize, valid, errorIndex, setValue, inputProps, style, props}) => {
                                    return (
                                        <TextField
                                            {...props}
                                            error={!valid}
                                            name={inputName}
                                            helperText={errorList[errorIndex]}
                                            inputRef={ref}
                                            fullWidth
                                            required={elLang.key === siteLang}
                                            label={lang.get("address")}
                                            style={{
                                                ...style,
                                                minWidth: '90%'
                                            }}
                                            inputProps={{
                                                ...inputProps,
                                                ...createInputProps({langKey: elLang.key}),
                                            }}/>
                                    )
                                }}/>
                        </ItemContainer>
                    </Box>
                )
            })}
        </FormContainer>
    )
}

//region Map

function MapCo({active, item, onChange, ...props}) {
    const theme = useTheme();
    const {location} = item;
    const [state, setState] = useState({
        showMap: false,
        mapOpen: false,
    });

    return (
        <FormContainer
            active={active}
            title={lang.get("map")}
            boxProps={{flexDirection: 'column'}}>
            <Box display={'flex'} flexWrap={'wrap'} px={1} py={2} justifyContent={'center'} alignItems={'center'}>
                <BaseButton variant={'contained'}
                            onClick={() => setState({...state, mapOpen: true})}
                            style={{
                                backgroundColor: cyan[300],
                                color: '#fff',
                                marginLeft: !_.isEmpty(location) ? theme.spacing(1) : null
                            }}>
                    <Map style={{marginLeft: theme.spacing(0.5)}}/>
                    {lang.get(location ? "edit_address" : "add_address")}
                </BaseButton>
                {!_.isEmpty(location) ?
                    <BaseButton variant={"text"} size={'small'}
                                onClick={() => onChange(null)}
                                style={{marginRight: theme.spacing(1)}}>
                        <Close fontSize={'small'} style={{marginLeft: theme.spacing(0.5)}}/>
                        حذف آدرس
                    </BaseButton>
                    : null}
            </Box>
            {!_.isEmpty(location) ?
                <Box my={2} mx={2} height={state.showMap ? 400 : 300}
                     display='flex'
                     justifyContent={'center'}
                     alignItems={'center'}
                     style={{
                         backgroundImage: state.showMap ? null : `url(${MapImage})`,
                         border: `1px solid ${grey[700]}`,
                         backgroundRepeat: 'no-repeat',
                         backgroundSize: 'cover',
                         ...UtilsStyle.borderRadius(5)
                     }}>
                    {state.showMap ?
                        <MapComponent
                            location={[
                                location.lat,
                                location.lng,
                            ]}/> :
                        <BaseButton
                            variant={"contained"}
                            onClick={() => setState({
                                ...state,
                                showMap: true
                            })}
                            style={{
                                backgroundColor: cyan[300],
                                color: "#fff",
                                padding: theme.spacing(2)
                            }}>
                            {lang.get("show_address_on_map")}
                            <Map
                                style={{
                                    marginRight: theme.spacing(1)
                                }}/>
                        </BaseButton>}
                </Box>
                : null
            }
            <SelectLocation
                open={state.mapOpen}
                title={lang.get(location ? "edit_address" : "add_address")}
                targetLocation={{
                    lat: (location && location.lat) ? location.lat : 37.28049,
                    lng: (location && location.lng) ? location.lng : 49.59051
                }}
                onSelect={(val) => {
                    setState({
                        ...state,
                        mapOpen: false
                    })
                    if (val)
                        onChange(val)
                }}/>
        </FormContainer>
    )
}

let DefaultIcon = L.icon({
    iconUrl: icon,
});
L.Marker.prototype.options.icon = DefaultIcon;
const useMapStyles = makeStyles({
    map: {
        '& .leaflet-marker-pane img': {
            top: -41,
            right: -12.5,
            width: 25,
            height: 41
        },
        '& .leaflet-popup': {
            bottom: '22px !important',
        }
    }
});

function MapComponent({location, ...props}) {
    const classes = useMapStyles(props);
    return (
        <LeafletMap
            className={classes.map}
            animate={true}
            center={location}
            doubleClickZoom={true}
            boxZoom={true}
            zoomControl={true}
            minZoom={7}
            zoom={14}
            maxZoom={19}
            length={4}
            style={{
                width: '100%',
                height: '100%',
            }}>
            <TileLayer
                attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"/>
            <Marker position={location}/>
        </LeafletMap>
    )
}

const useMapStyle = makeStyles(theme => ({
    mapContainer: {},
    mapSubmitBtn: {
        paddingTop: theme.spacing(2),
        paddingBottom: theme.spacing(2),
        ...UtilsStyle.borderRadius(5),
    },
    mapSubmitBtnContainer: {
        position: 'absolute',
        zIndex: 3,
        bottom: 0,
        left: 0,
        right: 0,
        width: '100%',
    },
}));

export function FullScreenMap(props) {
    const {firstLocation, onSubmitClick, onReturnClick} = props;
    const classes = useMapStyle(props);
    const defaultLocation = firstLocation ? firstLocation : {
        lat: 37.28049,
        lng: 49.59051,
    };
    const [map, setMap] = React.useState({
        centerLocation: defaultLocation,
    });
    return (
        <Box display='flex'
             boxShadow={3}
             className={classes.mapContainer}
             flexDirection='column'
             width={1}
             height={1}>
            <AddAddressMapComponent
                centerLocation={map.centerLocation}
                targetLocation={defaultLocation}
                onMoveEnd={(center) => {
                    setMap({
                        ...map,
                        centerLocation: center
                    })
                }}
                style={{
                    zIndex: 2
                }}/>
            <Box className={classes.mapSubmitBtnContainer} display='flex'>
                <Box width="70%">
                    <BaseButton
                        disabled={false}
                        size='large'
                        className={classes.mapSubmitBtn}
                        onClick={() => onSubmitClick(map.centerLocation)}
                        style={{
                            backgroundColor: blue[500],
                            color: "#fff",
                            width: '100%'
                        }}>
                        {lang.get('submit_location')}
                    </BaseButton>
                </Box>
                <Box width='30%'>
                    <BaseButton
                        disabled={false}
                        variant={"text"}
                        size='large'
                        className={classes.mapSubmitBtn}
                        onClick={onReturnClick}
                        style={{
                            backgroundColor: red[500],
                            color: "#fff",
                            width: '100%'
                        }}>
                        {lang.get('return')}
                    </BaseButton>
                </Box>
            </Box>
        </Box>
    )
}

FullScreenMap.propTypes = {
    firstLocation: PropTypes.object,
    onSubmitClick: PropTypes.func.isRequired,
    onReturnClick: PropTypes.func.isRequired
};
//endregion Map

//region Details

const detailsDefaultValue = {
    servingDays: {
        fa: [
            {
                name: 'خالی',
                data: '',
            },
            {
                name: 'با هماهنگی',
            },
            {
                name: 'شنبه تا پنجشنبه',
            },
            {
                name: 'پنج شنبه و جمعه',
            },
        ],
        en: [
            {
                name: 'none',
                data: '',
            },
            {
                name: 'Harmony',
            },
            {
                name: 'Saturday to Thursday',
            },
            {
                name: 'Thursday and Friday',
            },
        ],
    },
    servingHours: {
        fa: [
            {
                name: 'خالی',
                data: '',
            },
            {
                name: 'با هماهنگی',
            },
            {
                name: '24 ساعته',
            },
            {
                name: '8 صبح تا 12 شب',
            },
            {
                name: '8 صبح تا 1 ظهر - 5 غروب تا 12 شب',
            },
        ],
        en: [
            {
                name: 'none',
                data: '',
            },
            {
                name: 'Harmony',
            },
            {
                name: '24 hour',
            },
            {
                name: '8am to 12pm',
            },
            {
                name: '8am to 1pm - 5pm to 12pm',
            },
        ],
    }
}

function Details({active, data, onChange, ...props}) {
    const {details} = data;
    const [state, setState] = useState({
        activeLanguage: sLang[siteLang]
    })

    return (
        <FormContainer active={active}
                       activeLanguageValue={details}
                       activeLanguage={state.activeLanguage}
                       title={lang.get("details")}
                       onLanguageChange={(lang) => {
                           setState({
                               ...state,
                               activeLanguage: lang
                           })
                       }}>
            {Object.keys(sLang).map((langKey) => {
                const elLang = sLang[langKey];
                return (
                    <Box key={langKey} display={elLang.key !== state.activeLanguage.key ? 'none' : 'flex'} px={2}
                         py={1}
                         flexWrap={'wrap'}>
                        <ItemContainer width={0.5} px={2} flexDirection={'column'}>
                            <TextFieldContainer
                                name={createName({group: 'details', name: "servingDays", langKey: elLang.key})}
                                defaultValue={details[elLang.key] ? details[elLang.key].servingDays : ''}
                                render={(ref, {name: inputName, initialize, valid, errorIndex, setValue, inputProps, style, props}) => {
                                    return (
                                        <TextField
                                            {...props}
                                            error={!valid}
                                            name={inputName}
                                            helperText={errorList[errorIndex]}
                                            inputRef={ref}
                                            fullWidth
                                            label={lang.get("serving_days")}
                                            style={{
                                                ...style,
                                                width: '90%'
                                            }}
                                            inputProps={{
                                                ...inputProps,
                                                ...createInputProps({langKey: elLang.key})
                                            }}/>
                                    )
                                }}/>
                            {detailsDefaultValue.servingDays[state.activeLanguage.key] ?
                                <TemplateContainer>
                                    {detailsDefaultValue.servingDays[state.activeLanguage.key].map(({name, data}, index) => (
                                        <TemplateItem key={index} title={name}
                                                      data={data}
                                                      onClick={(data) => {
                                                          const newD = details;
                                                          newD[state.activeLanguage.key].servingDays = data
                                                          onChange(newD)
                                                      }}/>
                                    ))}
                                </TemplateContainer>
                                : null}
                        </ItemContainer>
                        <ItemContainer width={0.5} px={2} flexDirection={'column'}>
                            <TextFieldContainer
                                name={createName({group: 'details', name: "servingHours", langKey: elLang.key})}
                                defaultValue={details[elLang.key] ? details[elLang.key].servingHours : ''}
                                render={(ref, {name: inputName, initialize, valid, errorIndex, setValue, inputProps, style, props}) => {
                                    return (
                                        <TextField
                                            {...props}
                                            error={!valid}
                                            name={inputName}
                                            helperText={errorList[errorIndex]}
                                            inputRef={ref}
                                            fullWidth
                                            label={lang.get("serving_hours")}
                                            style={{
                                                ...style,
                                                width: '90%'
                                            }}
                                            inputProps={{
                                                ...inputProps,
                                                ...createInputProps({langKey: elLang.key})
                                            }}/>
                                    )
                                }}/>
                            {detailsDefaultValue.servingHours[state.activeLanguage.key] ?
                                <TemplateContainer>
                                    {detailsDefaultValue.servingHours[state.activeLanguage.key].map(({name, data}, index) => (
                                        <TemplateItem key={index} title={name} data={data}
                                                      onClick={(data) => {
                                                          const newD = details;
                                                          newD[state.activeLanguage.key].servingHours = data
                                                          onChange(newD)
                                                      }}/>
                                    ))}
                                </TemplateContainer>
                                : null}
                        </ItemContainer>
                        <ItemContainer width={1} flexDirection={'column'} mt={3}>
                            <CallItems
                                phones={details[elLang.key] ? details[elLang.key].phones : []}
                                onChange={({phones}) => {
                                    const newD = details;
                                    newD[state.activeLanguage.key].phones = phones
                                    onChange(newD)
                                }}/>
                        </ItemContainer>
                        <ItemContainer width={1} flexDirection={'column'} mt={3}>
                            <Typography variant={'h6'} py={2} pr={2}>
                                توضیحات اضافه:
                            </Typography>
                            <DetailsText data={data}
                                         activeLanguage={state.activeLanguage}
                                         onChange={(text) => {
                                             onChange({
                                                 ...details,
                                                 text: text
                                             })
                                         }}/>
                        </ItemContainer>
                    </Box>
                )
            })}
        </FormContainer>
    )
}


const callItemErrorList = [...errorList, 'ساختار شماره تلفن اشتباه است'];

function CallItems({phones = [], onChange, ...props}) {
    const theme = useTheme();
    const ref = useRef();

    const {enqueueSnackbar, closeSnackbar} = useSnackbar();
    const [state, setState] = useState({
        value: '',
        callMethod: 0,
        open: false,
        onEdit: null,
    });

    function handlePhoneAdd() {

        setTimeout(() => {
            if (ref.current.hasError()) {
                enqueueSnackbar('لطفا شماره تلفن را درست وارد کنید.',
                    {
                        variant: "error",
                        action: (key) => (
                            <Fragment>
                                <Button onClick={() => {
                                    closeSnackbar(key)
                                }}>
                                    {lang.get('close')}
                                </Button>
                            </Fragment>
                        )
                    });
                return;
            }
            const {phone} = ref.current.serialize();
            const ph = [...phones];
            if (state.onEdit) {
                ph.splice(state.onEdit.index, 0, {
                    id: state.onEdit.id,
                    type: state.callMethod === 0 ? 'mobile' : 'phone',
                    value: _.clone(phone),
                })
            } else {
                ph.push({
                    id: UtilsData.createId(phones),
                    type: state.callMethod === 0 ? 'mobile' : 'phone',
                    value: phone,
                });
            }
            onChange({phones: ph});
            setState({
                ...state,
                open: false,
                callMethod: 0,
                value: '',
                onEdit: null,
            })
        }, 1000);
    }

    function handlePhoneCancel() {
        setState({
            ...state,
            open: false,
            callMethod: 0,
            value: ''
        })
    }

    function handlePhoneEdit(phone, index) {
        const isMobile = phone.type === 'mobile';
        setState({
            open: true,
            callMethod: isMobile ? 0 : 1,
            value: phone.value,
            onEdit: {
                id: phone.id,
                index: index,
                callMethod: isMobile ? 0 : 1,
                value: phone.value,
            }
        });
        handlePhoneRemove(phone, index)
    }

    function handlePhoneRemove(phone, index) {
        const ph = [...phones];
        ph.splice(index, 1);
        onChange({phones: ph})
    }


    return (
        <Box display={'flex'} flexDirection={'column'} mt={3}>
            <Typography variant={'body1'}>
                {lang.get('call_numbers')}:
            </Typography>
            {!state.open ?
                <Box display={'flex'} mt={2}>
                    <BaseButton variant={"outlined"} size={"small"}
                                onClick={() => {
                                    setState({
                                        ...state,
                                        open: true
                                    })
                                }}
                                style={{borderColor: cyan[300], color: theme.palette.text.secondary}}>
                        <Add style={{marginLeft: theme.spacing(0.3)}}/>{lang.get('add')}
                    </BaseButton>
                </Box> : null}
            {state.open &&
            <Collapse in={state.open}>
                <Box display={'flex'} flexWrap={'wrap'}>
                    <Select
                        labelId="call method"
                        value={state.callMethod}
                        onChange={(e) => {
                            setState({
                                ...state,
                                callMethod: e.target.value
                            })
                        }}
                        style={{
                            marginTop: theme.spacing(1)
                        }}>
                        <MenuItem value={0}>
                            <Box display={'flex'} alignItems={'center'}>
                                <Phone fontSize={'small'} style={{marginLeft: theme.spacing(0.3)}}/>
                                موبایل
                            </Box>
                        </MenuItem>
                        <MenuItem value={1}>
                            <PhoneIphone fontSize={'small'} style={{marginLeft: theme.spacing(0.3)}}/>
                            تلفن
                        </MenuItem>
                    </Select>
                    <Box ml={2} mt={1}>
                        <FormControl name={'phoneForm'} innerref={ref}>
                            <TextFieldContainer
                                name={"phone"}
                                errorPatterns={[`^[0-9][0-9]*$`]}
                                defaultValue={state.value}
                                render={(ref, {name: inputName, initialize, valid, errorIndex, setValue, inputProps, style, props}) => {
                                    return (
                                        <TextField
                                            {...props}
                                            error={!valid}
                                            name={inputName}
                                            type={'number'}
                                            helperText={callItemErrorList[errorIndex]}
                                            inputRef={ref}
                                            required={true}
                                            label={'شماره'}
                                            style={{
                                                ...style,
                                                minWidth: 300
                                            }}
                                            inputProps={{
                                                ...inputProps
                                            }}/>
                                    )
                                }}/>
                        </FormControl>
                    </Box>
                    <Box mt={1} ml={2} display={'flex'} flexWrap={'wrap'} alignContent={'flex-end'}>
                        <BaseButton
                            onClick={handlePhoneAdd}
                            style={{
                                backgroundColor: cyan[300],
                                color: '#fff',
                            }}>
                            {lang.get('save')}
                        </BaseButton>
                        <BaseButton variant={"text"} size={'small'}
                                    onClick={handlePhoneCancel}
                                    style={{marginRight: theme.spacing(0.7)}}>
                            {lang.get('cancel')}
                        </BaseButton>
                    </Box>
                </Box>
            </Collapse>}
            <Box display={'flex'} flexDirection={'column'} mt={2}>
                <DraggableList
                    items={phones}
                    onItemsChange={(items) => {
                        onChange({phones: items});
                    }}
                    render={(item, props, {index}) => {
                        const isMobile = item.type === 'mobile';
                        return (
                            <Box key={item.id}
                                 display={'flex'}
                                 alignItems={'center'}
                                 py={1}
                                 px={1.5}
                                 my={1}
                                 {...props}
                                 style={{
                                     ...props.style,
                                     border: `1px solid ${cyan[300]}`,
                                     ...UtilsStyle.widthFitContent(),
                                     ...UtilsStyle.borderRadius(5)
                                 }}>
                                <Box
                                    display={'flex'}
                                    alignItems={'center'}>
                                    <Box minWidth={75} display={'flex'}>
                                        {isMobile ?
                                            <Phone fontSize={'small'} style={{marginLeft: theme.spacing(0.3)}}/> :
                                            <PhoneIphone fontSize={'small'}
                                                         style={{marginLeft: theme.spacing(0.3)}}/>}
                                        <Typography variant={'body1'} display={'flex'} alignItems={'center'} mr={0.7}>
                                            {isMobile ? 'موبایل' : 'تلفن'}
                                        </Typography>
                                    </Box>
                                    <Typography variant={'h6'} mr={1.5} fontWeight={500}>
                                        {item.value}
                                    </Typography>
                                </Box>
                                <Box display={'flex'} mr={1.5}>
                                    <IconButton size={"small"}
                                                onClick={() => {
                                                    handlePhoneEdit(item, index)
                                                }}
                                                style={{marginLeft: theme.spacing(0.5)}}>
                                        <Edit fontSize={"small"}/>
                                    </IconButton>
                                    <IconButton
                                        size={"small"}
                                        onClick={() => {
                                            handlePhoneRemove(item, index)
                                        }}>
                                        <Delete fontSize={"small"}/>
                                    </IconButton>
                                </Box>
                            </Box>
                        )
                    }}
                />
            </Box>
        </Box>
    )
}

function DetailsText({data: dd, activeLanguage, onChange}) {
    const {details} = dd;
    const [data, setData] = useState({
        init: false
    })
    const [timer, setTimer] = useState(null)

    useEffect(() => {
        const newData = data;
        _.forEach(sLang, (l, key) => {
            const text = (details[key] && details[key].text) ? details[key].text : '';
            newData[key] = htmlToEditorContent(text);
        });
        setData({
            ...newData,
            init: true
        });
        return () => {
            clearTimeout(timer);
        }
    }, [])


    function handlerTextChange(content) {
        const newData = data;
        newData[activeLanguage.key] = content;
        setData({
            ...newData
        });

        if (timer != null)
            clearTimeout(timer);
        setTimer(setTimeout(() => {
            const newDef = details;
            newDef[activeLanguage.key].text = editorContentToHtml(content);
            onChange({...newDef})
        }, 4000))
    }

    return (
        <React.Fragment>

            {data.init &&
            <Editor
                mx={2}
                minHeight={200}
                content={data[activeLanguage.key]}
                onContentChange={handlerTextChange}
                style={{
                    border: `1px solid ${grey[400]}`,
                    overflow: 'auto',
                    ...UtilsStyle.borderRadius(5),
                }}/>
            }
        </React.Fragment>
    )
}

//endregion Details

//region Properties

function Properties({active, data, onChange, ...props}) {
    const theme = useTheme();
    const {properties} = data;
    const [state, setState] = useState({
        activeLanguage: sLang[siteLang],
    });

    return (
        <FormContainer
            active={active}
            title={lang.get("properties")}
            activeLanguageValue={properties}
            activeLanguage={state.activeLanguage}
            onLanguageChange={(lang) => {
                setState({
                    ...state,
                    activeLanguage: lang
                })
            }}>
            <Box display={'flex'} width={1} flexDirection={'column'}>
                <Box p={2}>
                    <SuccessButton
                        onClick={() => {
                            try {
                                const newData = properties;
                                newData.data[siteLang].items.push(createPropertiesItem(UtilsData.createId(newData.data[siteLang].items)))
                                onChange(newData)
                            } catch (e) {
                            }
                        }}>
                        ویژگی جدید
                    </SuccessButton>
                </Box>
                {properties.data[siteLang].items.map(it => (
                    <PropertiesContainer
                        key={it.key}
                        box={data.box}
                        item={it}
                        onChange={(newIt) => {
                            const newData = properties.data[siteLang];
                            const index = _.findIndex(newData.items, (d) => {
                                return d.id === newIt.id;
                            });
                            if (index === -1)
                                return;
                            newData.items[index] = newIt;
                            const res = properties;
                            res.data[siteLang] = newData
                            onChange(res)
                        }}
                        onRemove={() => {
                            const newData = properties.data[siteLang];
                            _.remove(newData.items, (d) => d.id === it.id)
                            const res = properties;
                            res.data[siteLang] = newData
                            onChange(res)
                        }}/>
                ))}
            </Box>
        </FormContainer>
    )
};

function createPropertiesItem(id) {
    return {
        id: id,
        type: undefined,
        items: []
    }
}


const PropertiesTemplate = [
    {
        key: "attributes",
        label: "ویژگی ها",
    },
    {
        key: "terms_of_use",
        label: "شرایط استفاده",
    },
    {
        key: "description",
        label: "توضیحات",
    },
    // {
    //     key:"custom",
    //     label: "دستی",
    //     color: cyan[300],
    //     iconColor: ""
    // },
]

function getPropertiesTemplateTheme(type) {
    switch (type.key) {
        case "attributes": {
            return {
                ...type,
                color: cyan[300],
                iconColor: "svgBlue"
            }
        }
        case "terms_of_use": {
            return {
                ...type,
                color: orange[300],
                iconColor: "svgRed"
            }
        }
        case "description": {
            return {
                ...type,
                color: grey[500],
                iconColor: undefined
            }
        }
        default: {
            return type
        }
    }
}

function PropertiesContainer({box, item, onChange, onRemove}) {
    const theme = useTheme();
    const [state, setState] = useState({
        activeLanguage: sLang[siteLang],
        add: false,
        edit: undefined,
        addFromArchive: false,
        editType: false,
        remove: false
    })

    useEffect(() => {
        if (item.type) {
            setState({
                ...state,
                editType: false
            })
        }
    }, [item])


    function saveType(type) {
        setTimeout(() => {

            if (!type) {
                onRemove()
            } else {
                onChange({...item, type: type})
            }
            setState({
                ...state,
                editTitle: false
            })
        }, 700)
    }


    const type = item.type ? getPropertiesTemplateTheme(item.type) : undefined

    return (
        <Box width={1} p={2}>
            {
                type &&
                <Box display={'flex'} flexDirection={'column'} width={1} px={2}
                     style={{
                         border: `2px solid ${type.color}`,
                         ...UtilsStyle.borderRadius(5)
                     }}>
                    <Box width={1} pt={2} pb={1} display={'flex'} alignItems={'center'} flexWrap={'wrap'}>
                        <ButtonBase
                            style={{
                                marginLeft: theme.spacing(1)
                            }}>
                            <Typography
                                variant={"h6"}
                                fontWeight={400}
                                onClick={() => {
                                    setState({
                                        ...state,
                                        editType: true
                                    })
                                }}
                                style={{minWidth: 140}}>
                                {type.label}
                            </Typography>
                        </ButtonBase>
                        <BaseButton
                            variant={"outlined"}
                            onClick={() => {
                                setState({
                                    ...state,
                                    add: true
                                })
                            }}
                            style={{
                                borderColor: cyan[300]
                            }}>
                            <AddBoxOutlined
                                style={{
                                    marginLeft: theme.spacing(1)
                                }}/>
                            افرودن ویژگی جدید
                        </BaseButton>
                        <BaseButton
                            variant={"outlined"}
                            onClick={() => {
                                setState({
                                    ...state,
                                    addFromArchive: true
                                })
                            }}
                            style={{
                                marginRight: theme.spacing(1),
                                borderColor: cyan[300]
                            }}>
                            <AddToPhotosOutlined
                                style={{
                                    marginLeft: theme.spacing(1)
                                }}/>
                            افرودن ویژگی پیشفرض
                        </BaseButton>
                        <BaseButton
                            variant={"outlined"}
                            onClick={() => {
                                const newItems = item.items;
                                const high = [];
                                const medium = [];
                                const low = [];
                                _.forEach(item.items, (it) => {
                                    switch (it.priority) {
                                        case "medium": {
                                            medium.push(it)
                                            break
                                        }
                                        case "low": {
                                            low.push(it)
                                            break
                                        }
                                        default: {
                                            high.push(it)
                                        }
                                    }
                                })
                                onChange({
                                    ...item,
                                    items: [
                                        ...high,
                                        ...medium,
                                        ...low
                                    ]
                                })
                            }}
                            style={{
                                marginRight: theme.spacing(1),
                                borderColor: green[300]
                            }}>
                            <SortOutlined
                                style={{
                                    marginLeft: theme.spacing(1)
                                }}/>
                            مرتب سازی
                        </BaseButton>
                        <BaseButton
                            variant={"outlined"}
                            onClick={() => {
                                setState({
                                    ...state,
                                    remove: true
                                })
                            }}
                            style={{
                                marginRight: theme.spacing(1),
                                borderColor: red[300]
                            }}>
                            <DeleteOutlineOutlined
                                style={{
                                    marginLeft: theme.spacing(1)
                                }}/>
                            حذف
                        </BaseButton>
                    </Box>
                    <Box p={2} display={'flex'} flexDirection={'column'}>
                        <DraggableList
                            items={item.items}
                            onItemsChange={(items) => {
                                onChange({
                                    ...item,
                                    items: [...items]
                                })
                            }}
                            render={(it, draggableListProps, {index}) => (
                                <Box
                                    key={index}
                                    {...draggableListProps}>
                                    <PropertiesItem
                                        key={index}
                                        item={it}
                                        type={type}
                                        disableEdit={Boolean(state.edit)}
                                        onRemove={() => {
                                            const newItems = item.items;
                                            _.remove(newItems, (newIt) => {
                                                return newIt.id === it.id
                                            })
                                            onChange({
                                                ...item,
                                                items: [...newItems]
                                            })
                                        }}
                                        onEdit={() => {
                                            setState({
                                                ...state,
                                                edit: item
                                            })
                                        }}
                                        isUsageCondition={true}/>
                                </Box>
                            )}/>

                    </Box>
                    <CreateProperty
                        open={state.add || Boolean(state.edit)}
                        item={state.edit}
                        saveToServer={false}
                        onClose={(data) => {
                            if (data) {
                                setTimeout(() => {
                                    let newItems = item.items;
                                    _.forEach(data[siteLang].items, it => {
                                        let i = _.findIndex(item.items, (it2) => {
                                            return it2.id === it.id
                                        });
                                        const itt = {
                                            ...it,
                                            id: it.id || UtilsData.createId(newItems)
                                        };

                                        if (i === -1) {
                                            newItems.push(itt)
                                        } else {
                                            newItems[i] = itt
                                        }
                                    })
                                    onChange({
                                        ...item,
                                        items: newItems
                                    })
                                }, 1000)
                            }
                            setState({
                                ...state,
                                add: false,
                                edit: false
                            })
                        }}/>
                    <SelectProperty
                        open={state.addFromArchive}
                        boxId={box.id}
                        select={true}
                        multi={true}
                        add={false}
                        onClose={(data) => {
                            if (data)
                                setTimeout(() => {
                                    let newItems = item.items;
                                    _.forEach(data[siteLang].items, it => {
                                        newItems.push({
                                            ...it,
                                            id: UtilsData.createId(newItems)
                                        })
                                    })
                                    onChange({
                                        ...item,
                                        items: newItems
                                    })
                                }, 1000)
                            setState({
                                ...state,
                                addFromArchive: false
                            })
                        }}/>
                </Box>
            }
            <BaseDialog open={state.editType || !type}>
                <Box display={'flex'} p={2} flexDirection={'column'}>
                    <TemplateContainer title={"یک قالب را انتخاب کنید"}>
                        {PropertiesTemplate.map(pt => (
                            <TemplateItem key={pt.label} data={pt} title={pt.label}
                                          borderColor={getPropertiesTemplateTheme(pt).color}
                                          onClick={() => saveType(pt)}/>
                        ))}
                    </TemplateContainer>
                    <Box p={1} display={'flex'}>
                        <BaseButton
                            variant={"text"}
                            onClick={() => {
                                if (!type) {
                                    onRemove()
                                }
                                setState({
                                    ...state,
                                    editType: false
                                })
                            }}
                            style={{
                                marginRight: theme.spacing(1)
                            }}>
                            لغو
                        </BaseButton>
                    </Box>
                </Box>
            </BaseDialog>
            <SaveDialog
                open={state.remove}
                text={"آیا از حذف اطمینان دارید؟"}
                submitText={"بله"}
                cancelText={"خیر"}
                onSubmit={() => {
                    setState({
                        ...state,
                        remove: false
                    })
                    setTimeout(() => {
                        onRemove()
                    }, 700)
                }}
                onCancel={() => {
                    setState({
                        ...state,
                        remove: false
                    })
                }}/>
        </Box>
    )
}

function PropertiesItem({item, type, disableEdit, onEdit, onRemove, isUsageCondition, index, onDragOver, onDragStart, onDragEnd, ...props}) {
    const theme = useTheme();

    const {enqueueSnackbar, closeSnackbar} = useSnackbar();


    const isHigh = item.priority === "high"
    const isMedium = item.priority === "medium"
    const isLow = !(isHigh || isMedium)

    const iconWidth = isHigh ? 60 : isMedium ? 45 : 35
    return (
        <Box py={1}>
            <Box
                py={0.7} px={1}
                display={'flex'}
                alignItems={'center'}
                style={{
                    border: `${isHigh ? 2 : isMedium ? 1.5 : 0.7}px solid ${type.color}`,
                    ...UtilsStyle.borderRadius(5),
                    ...UtilsStyle.transition(),
                }}
                {...props}>
                <Box display={'flex'}>
                    <Tooltip title={lang.get("edit")}>
                        <IconButton
                            size={'small'}
                            onClick={(e) => {
                                if (disableEdit) {
                                    enqueueSnackbar('یک ویژگی در حال ادیت میاشد.',
                                        {
                                            variant: "error",
                                            action: (key) => (
                                                <Fragment>
                                                    <Button onClick={() => {
                                                        closeSnackbar(key)
                                                    }}>
                                                        {lang.get('close')}
                                                    </Button>
                                                </Fragment>
                                            )
                                        });
                                    return
                                }
                                onEdit(e)
                            }}
                            style={{marginLeft: theme.spacing(0.5)}}>
                            <Edit fontSize={"small"} style={{color: cyan[200]}}/>
                        </IconButton>
                    </Tooltip>
                    <Tooltip title={lang.get("delete")}>
                        <IconButton size={'small'} onClick={onRemove}>
                            <Delete fontSize={"small"} style={{color: red[200]}}/>
                        </IconButton>
                    </Tooltip>
                </Box>
                <Box px={2}>
                    <Box height={iconWidth} width={60} display={'flex'} alignItems={'center'} justifyContent={'center'}>
                        <Icon
                            height={iconWidth}
                            width={iconWidth}
                            alt={item.icon}
                            color={type.iconColor}
                            showSkeleton={false}
                            src={convertIconUrl(item.icon)}/>
                    </Box>
                </Box>
                <Typography
                    variant={!isLow ? 'subtitle1' : 'subtitle2'}
                    fontWeight={isHigh ? '400' : isMedium ? '300' : '200'}>
                    {item.text}
                </Typography>
            </Box>
        </Box>
    )
}


function PropertiesIcon({isUsageCondition, type, ...props}) {
    const color = isUsageCondition ? orange[600] : green[500];
    return (
        <Box width={35} height={35} display={'flex'}
             justifyContent={'center'}
             alignItems={'center'}>
            {
                type === 'date' ?
                    <DateRange style={{color: color}}/>
                    :
                    type === 'phone' ?
                        <PhoneEnabled style={{color: color}}/>
                        :
                        <Gem2
                            gemColor={color}
                            style={{
                                width: 15
                            }}
                            {...props}/>
            }
        </Box>
    )
}

//endregion Properties

//region Tags

const useTagsStyle = makeStyles(theme => ({
    root: {
        backgroundColor: cyan[100],
        color: grey[700],
        '&:focus': {
            backgroundColor: cyan[100],
            color: grey[700],
        },
    }
}));

function Tags({data: dd, onChange, onTagGpAdd: onTagGpChange, ...props}) {
    const {tags, box, tag_groups} = dd;
    const theme = useTheme();

    const {enqueueSnackbar, closeSnackbar} = useSnackbar();
    const [setting, setSetting] = useState({
        openAddTagDialog: false,
        dialogValue: '',
        dialogPermalink: '',
        selectGpTag: false,
    });
    const [defValue, setDefValue] = useState("");
    const [inputValue, setInputValue] = useState("")
    const [value, setValue] = useState("");
    const classes = useTagsStyle(props);

    const d = ControllerProduct.Product.Tags.search({query: value});
    const apiKey = d[0] + value;
    const {data: da, error} = useSWR(apiKey, () => {
        return d[1]()
    }, {errorRetryCount: 1});
    const {tags: data} = da ? da.data : [];

    useEffect(() => {
        setValue("");
        setDefValue("");
    }, []);

    function handleAdd(d, show) {
        try {
            const newTags = tags;
            const newList = [];
            if (show) {
                const i = _.findIndex(tags, (t) => t.id === d.id);
                if (i === -1)
                    return;
                newTags[i] = d;
            } else {
                if (!_.isArray(d))
                    d = [d];
                let notAdded = 0;
                let showTag = 0;
                _.forEach(tags, (t) => {
                    showTag = (t.show ? 1 : 0) + showTag
                })

                _.forEach(d, (tag) => {
                    const i = _.findIndex(tags, (t) => {
                        return tag.id === t.id;
                    })
                    if (i !== -1) {
                        notAdded = notAdded + 1;
                        return
                    }
                    if (tag.show) {
                        if (maxShowableTagCount <= showTag) {
                            tag.show = false;
                        }
                        showTag = showTag + 1;
                    }
                    newList.push(tag)
                })
                if (notAdded > 0) {
                    enqueueSnackbar(notAdded + ' انتخاب شده است.',
                        {
                            variant: "warning",
                            action: (key) => (
                                <Fragment>
                                    <Button onClick={() => {
                                        closeSnackbar(key)
                                    }}>
                                        {lang.get('close')}
                                    </Button>
                                </Fragment>
                            )
                        });
                }
                if (_.isEmpty(newList))
                    return
            }

            onChange([...newTags, ...newList])
        } catch (e) {
            gcError("ProductSingle::Tags::handleAdd", e)
        }
    }

    function handleDelete(d) {
        const newData = tags;
        _.remove(newData, (t) => {
            return t.id === d.id
        });
        onChange([...newData])
    }

    function handleAddNewTag() {
        if (!_.isEmpty(data)) {
            const i = _.findIndex(data, (t) => {
                return t.name.fa === value
            });
            if (i !== -1) {
                enqueueSnackbar('این تگ موجود میباشد.',
                    {
                        variant: "error",
                        action: (key) => (
                            <Fragment>
                                <Button onClick={() => {
                                    closeSnackbar(key)
                                }}>
                                    {lang.get('close')}
                                </Button>
                            </Fragment>
                        )
                    });
                return;
            }
        }


        setSetting({
            ...setting,
            openAddTagDialog: true,
            dialogPermalink: '',
            dialogValue: value
        })

        // enqueueSnackbar('فزوده شد.',
        //     {
        //         variant: "success",
        //         action: (key) => (
        //             <Fragment>
        //                 <Button onClick={() => {
        //                     closeSnackbar(key)
        //                 }}>
        //                     {lang.get('close')}
        //                 </Button>
        //             </Fragment>
        //         )
        //     });
    }

    function handleCloseDialog(save) {

        if (save) {
            if (!setting.dialogPermalink || !setting.dialogValue) {
                enqueueSnackbar('تمام فیلدها را پرکنید.',
                    {
                        variant: "error",
                        action: (key) => (
                            <Fragment>
                                <Button onClick={() => {
                                    closeSnackbar(key)
                                }}>
                                    {lang.get('close')}
                                </Button>
                            </Fragment>
                        )
                    });
                return;
            }
            ControllerProduct.Product.Tags.set({
                permalink: setting.dialogPermalink,
                name: createMultiLanguage({fa: setting.dialogValue})
            }).then(res => {
                res.item.hasActive = true;
                handleAdd(res.item);
                enqueueSnackbar('افزوده شد.',
                    {
                        variant: "success",
                        action: (key) => (
                            <Fragment>
                                <Button onClick={() => {
                                    closeSnackbar(key)
                                }}>
                                    {lang.get('close')}
                                </Button>
                            </Fragment>
                        )
                    });
                handleCloseDialog(false)
                mutate(apiKey);
            });
            return;
        }


        setSetting({
            ...setting,
            openAddTagDialog: false,
            dialogPermalink: ''
        })
    }

    return (
        <React.Fragment>
            <FormContainer title={lang.get("tags")}>
                <Box display={'flex'} width={1} px={2} py={1} flexDirection={'column'}>
                    {!error ?
                        <Box display={'flex'} justifyContent={'center'} width={1}>
                            <Box width={1} mt={2} mb={2}>
                                {!_.isEmpty(tag_groups) &&
                                <Box display={'flex'} flexDirection={'column'} mb={3}>
                                    <Typography variant={'body1'} pb={1} fontWeight={400}>
                                        گروه تگ‌های فعال:
                                    </Typography>
                                    <Box display={'flex'} flexWrap={'wrap'}>
                                        {
                                            tag_groups.map(tg => (
                                                <Box width={1 / 2}>
                                                    <TagItem
                                                        key={tg.id}
                                                        item={tg}
                                                        product={true}
                                                        selectable={false}
                                                        editable={false}
                                                        width={1}
                                                        onRemoved={() => {
                                                            const newList = _.cloneDeep(tag_groups);
                                                            _.remove(newList, (it => {
                                                                return it.id === tg.id
                                                            }))
                                                            onTagGpChange(newList)
                                                        }}/>
                                                </Box>
                                            ))
                                        }
                                    </Box>
                                </Box>}
                                <Box display={'flex'} flexDirection={'column'} mb={3}>
                                    <Typography variant={'body1'} pb={1} fontWeight={400}>
                                        تگ‌های فعال:
                                    </Typography>
                                    <Box display={'flex'} flexWrap={'wrap'}>
                                        {
                                            tags.map((t, i) => (
                                                <Box key={i} ml={2} mb={1.5}>
                                                    <Chip
                                                        avatar={t.show ? <VisibilityOutlined/> :
                                                            <VisibilityOffOutlined/>}
                                                        clickable
                                                        onClick={() => {
                                                            if (!t.show) {
                                                                let showCount = 0
                                                                _.forEach(tags, t => {
                                                                    showCount = (t.show ? 1 : 0) + showCount
                                                                })
                                                                if (showCount >= maxShowableTagCount) {
                                                                    enqueueSnackbar("حداکثر تعداد قابل نمایش " + maxShowableTagCount + " میباشد.",
                                                                        {
                                                                            variant: "error",
                                                                            action: (key) => (
                                                                                <Fragment>
                                                                                    <Button onClick={() => {
                                                                                        closeSnackbar(key)
                                                                                    }}>
                                                                                        {lang.get('close')}
                                                                                    </Button>
                                                                                </Fragment>
                                                                            )
                                                                        });
                                                                    return
                                                                }
                                                            }
                                                            handleAdd({
                                                                ...t,
                                                                show: !t.show
                                                            }, true)
                                                        }}
                                                        className={classes.root}
                                                        label={t.name.fa}
                                                        onDelete={() => handleDelete(t)}
                                                        style={{
                                                            backgroundColor: !t.show ? cyan[100] : green[100]
                                                        }}/>
                                                </Box>
                                            ))
                                        }
                                    </Box>
                                </Box>
                                <Box display={'flex'} width={1} alignItems={'center'} justifyContent={'center'}>
                                    <Box flex={1} pr={4}>
                                        <TextFieldContainer
                                            name={createName({name: "tag-name"})}
                                            defaultValue={defValue}
                                            onChange={(val) => {
                                                setInputValue(val)
                                            }}
                                            render={(ref, {name: inputName, initialize, valid, errorIndex, inputProps, style, props}) => {
                                                return (
                                                    <TextField
                                                        {...props}
                                                        variant={'outlined'}
                                                        error={!valid}
                                                        name={inputName}
                                                        inputRef={ref}
                                                        fullWidth
                                                        onKeyPress={(e) => {
                                                            try {
                                                                if (e.key === 'Enter') {
                                                                    const val = ref.current.getAttribute(textFieldNewValue)
                                                                    setValue(val)
                                                                    setDefValue(val)
                                                                }
                                                            } catch (e) {
                                                            }
                                                        }}
                                                        endAdornment={(inputValue) && (
                                                            <IconButton onClick={() => {
                                                                try {
                                                                    setDefValue("");
                                                                    setValue("");
                                                                    setInputValue("");
                                                                } catch (e) {
                                                                }
                                                            }}>
                                                                <CancelOutlined/>
                                                            </IconButton>
                                                        )}
                                                        label={'جست‌و‌جو تگ'}
                                                        placeholder={'بعد تایپ نام تگ enter بزنید'}
                                                        style={{
                                                            ...style,
                                                            minWidth: '90%'
                                                        }}
                                                        inputProps={{
                                                            ...inputProps,
                                                        }}/>
                                                )
                                            }}/>
                                    </Box>
                                    <Box display={'flex'} alignItems={'center'}>
                                        <BaseButton
                                            disabled={value.length < 2}
                                            size={"medium"}
                                            style={{
                                                backgroundColor: cyan[300],
                                                color: '#fff'
                                            }}
                                            onClick={handleAddNewTag}>
                                            {lang.get("add")}
                                        </BaseButton>
                                        <Box pl={2} display={'flex'}>
                                            <BaseButton
                                                size={"medium"}
                                                style={{
                                                    backgroundColor: cyan[300],
                                                    color: '#fff'
                                                }}
                                                onClick={() => {
                                                    setSetting({
                                                        ...setting,
                                                        selectGpTag: true
                                                    })
                                                }}>
                                                گروه تگ‌ها
                                            </BaseButton>
                                        </Box>
                                    </Box>
                                </Box>
                                <Box mt={2} component={Card}>
                                    {

                                        data ?
                                            <Collapse in={!_.isEmpty(data)}>
                                                <Box display={'flex'} flexWrap={'wrap'} py={2} px={2}>
                                                    {
                                                        !_.isEmpty(data) && data.map((d, i) => (
                                                            <Box key={i} ml={2} mb={1.5}>
                                                                <Chip
                                                                    label={d.name.fa}
                                                                    disabled={_.findIndex(tags, (t) => {
                                                                        return d.permalink === t.permalink;
                                                                    }) !== -1}
                                                                    onClick={() => handleAdd(d)}
                                                                    style={{
                                                                        color: d.hasActive ? theme.palette.action.hover : null
                                                                    }}/>
                                                            </Box>
                                                        ))
                                                    }
                                                </Box>
                                            </Collapse> :
                                            <PleaseWait/>
                                    }
                                </Box>
                            </Box>
                        </Box> :
                        <ComponentError
                            statusCode={(_.isObject(error) && error.request) ? error.request.status : undefined}
                            tryAgainFun={() => mutate(apiKey)}/>
                    }
                </Box>
            </FormContainer>
            <TagSingle
                open={setting.openAddTagDialog}
                name={value}
                onClose={(item) => {
                    if (item) {
                        item.hasActive = true;
                        handleAdd(item);
                        enqueueSnackbar('افزوده شد.',
                            {
                                variant: "success",
                                action: (key) => (
                                    <Fragment>
                                        <Button onClick={() => {
                                            closeSnackbar(key)
                                        }}>
                                            {lang.get('close')}
                                        </Button>
                                    </Fragment>
                                )
                            });
                        handleCloseDialog(false);
                        mutate(apiKey)
                        return
                    }
                    setSetting({
                        ...setting,
                        openAddTagDialog: false,
                        dialogPermalink: ''
                    })
                }}/>
            <SelectTag open={setting.selectGpTag}
                       boxId={box.id}
                       selectable={true}
                       multiSelect={true}
                       onClose={(data) => {
                           if (data) {
                               let i = 0;
                               const newTg = []
                               _.forEach(data, (it) => {
                                   const index = _.findIndex(tag_groups, d => {
                                       return d.id === it.id
                                   });
                                   if (-1 !== index) {
                                       i = i + 1;
                                   } else {
                                       newTg.push(it);
                                   }
                               })
                               if (!_.isEmpty(newTg))
                                   onTagGpChange([
                                       ...tag_groups,
                                       ...newTg
                                   ])
                               if (i > 0)
                                   enqueueSnackbar(i + " گروه از قبل انتخاب شده است.",
                                       {
                                           variant: "warning",
                                           action: (key) => (
                                               <Fragment>
                                                   <Button onClick={() => {
                                                       closeSnackbar(key)
                                                   }}>
                                                       {lang.get('close')}
                                                   </Button>
                                               </Fragment>
                                           )
                                       });
                           }
                           setSetting({
                               ...setting,
                               selectGpTag: false
                           })
                       }}/>
        </React.Fragment>
    )
}

//endregion Tags

//region HelperComponents

//region ItemContainer

function FormContainer({title, openDef = false, active, boxProps = {}, ...props}) {

    const [state, setState] = useState({
        open: openDef
    })

    return (
        <BaseFormContainer open={state.open}
                           active={active}
                           title={title}
                           onOpenClick={(e, open) => {
                               setState({
                                   ...state,
                                   open: open
                               })
                           }}
                           {...props}>
            <Box width={1} display={'flex'} {...boxProps}>
                {props.children}
            </Box>
        </BaseFormContainer>
    )
}

function ItemContainer(props) {
    return (
        <ErrorBoundary>
            <Box display='flex'
                 my={1}
                 width={'100%'}
                 {...props}>
                {props.children}
            </Box>
        </ErrorBoundary>
    )
}


//endregion ItemContainer

//region TemplateItem
function TemplateContainer({title = 'متن های آماده', ...props}) {
    return (
        <Box display={'flex'} flexDirection={'column'} mt={2}>
            <Typography variant={'body2'} color={cyan[600]}>
                {title}:
            </Typography>
            <Box display={'flex'} flexWrap={'wrap'}>
                {props.children}
            </Box>
        </Box>
    )
}

function TemplateItem({title, data, borderColor = cyan[200], onClick, ...props}) {
    const theme = useTheme();

    return (
        <BaseButton variant={"outlined"}
                    size={'small'}
                    onClick={() => onClick(data !== undefined ? data : title)}
                    style={{
                        borderColor: borderColor,
                        color: theme.palette.text.secondary,
                        marginTop: theme.spacing(0.5),
                        marginLeft: theme.spacing(0.5)
                    }}>
            {title}
        </BaseButton>
    )
}

//endregion TemplateItem


//endregion HelperComponents
