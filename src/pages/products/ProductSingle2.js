import React, {createRef, Fragment, useCallback, useEffect, useState} from "react";
import Box from "@material-ui/core/Box";
import Skeleton from "@material-ui/lab/Skeleton";
import ControllerProduct from "../../controller/ControllerProduct";
import BaseTextField from "../../components/base/textField/base/BaseTextField";
import FormControl from "../../components/base/formController/FormController";
import StickyBox from "react-sticky-box";
import Typography from "../../components/base/Typography";
import {activeLang, colors, getActiveLanguage, lang, sLang, theme, webRout} from "../../repository";
import rout, {siteRout} from "../../router";
import {blue, cyan, deepOrange, green, grey, orange, red} from "@material-ui/core/colors";
import BaseButton from "../../components/base/button/BaseButton";
import {UtilsData, UtilsRouter, UtilsStyle} from "../../utils/Utils";
import _ from 'lodash'
import {useSnackbar} from "notistack";
import Button from "@material-ui/core/Button";
import Editor, {editorContentToHtml, htmlToEditorContent} from "../../components/base/editor/Editor";
import {Card, makeStyles} from "@material-ui/core";
import BoxWithTopBorder from "../../components/base/textHeader/BoxWithTopBorder";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Switch from "@material-ui/core/Switch";
import Collapse from "@material-ui/core/Collapse";
import {
    Add,
    Close,
    DateRange,
    Delete,
    DeleteOutlineOutlined,
    Edit,
    ExpandLess,
    ExpandMore,
    Map,
    Menu,
    MenuOpen,
    Phone,
    PhoneEnabled,
    PhoneIphone,
    Save,
    Sync
} from "@material-ui/icons";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import IconButton from "@material-ui/core/IconButton";
import BaseTextFieldMultiLanguage from "../../components/base/textField/base/BaseTextFieldMultiLanguage";
import UploadItem from "../../components/base/uploader/UploadItem";
import ControllerSite from '../../controller/ControllerSite'
import {convertProperty, createMultiLanguage, states} from '../../controller/converter'
import {serverFileTypes} from '../../controller/type'
import {Map as LeafletMap, Marker, TileLayer} from "react-leaflet";
import L from "leaflet";
import icon from "../../drawable/mapImage/marker-icon.png";
import MapImage from "../../drawable/image/map.jpg"
import Dialog from "@material-ui/core/Dialog";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import AddAddressMapComponent from '../../components/addAddress/Map'
import PropTypes from "prop-types";
import DraggableList from "../../components/base/draggableList/DraggableList";
import Gem2 from "../../components/base/icon/Gem2";
import Tooltip from "@material-ui/core/Tooltip";
import AutoFill from "../../components/base/autoFill/AutoFill";
import DialogActions from "@material-ui/core/DialogActions";
import Slide from "@material-ui/core/Slide";
import Backdrop from "@material-ui/core/Backdrop";
import CircularProgress from "@material-ui/core/CircularProgress";
import ComponentError from "../../components/base/ComponentError";
import Chip from "@material-ui/core/Chip";
import {Redirect} from "react-router-dom";
import RadioGroup from "@material-ui/core/RadioGroup";
import Radio from "@material-ui/core/Radio";
import Img from "../../components/base/img/Img";
import SuccessButton from "../../components/base/button/buttonVariant/SuccessButton";
import BaseLink from "../../components/base/link/Link";
import useSWR, {mutate} from "swr";
import SelectBox from "../../components/SelectBox";
import Divider from "@material-ui/core/Divider";
import ButtonBase from "@material-ui/core/ButtonBase";
import {EditBrand} from "../brands/Brands";
import CategorySingle, {getActiveParents} from "../categories/CategorySingle";


export default function ({...props}) {
    //region var
    const formRef = createRef();
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();
    const pId = props.match.params.pId;

    //region state
    const [state, setState] = useState({
        data: pId ? false : {
            box: null,
            name: {},
            permalink: '',
            type: {},
            city: {},
            description: {},
            shortDescription: {},
            address: {},
            shortAddress: {},
            details: {},
            properties: {},
            tags: [],
            thumbnail: null,
            media: [],
            brand: null,
        },
    });
    const [elState, setElState] = useState({
        permalink: {
            onEdit: !pId
        },
        media: {
            open: false,
        },
        city: {
            active: false,
            open: false,
        },
        address: {
            active: false,
            open: false,
            activeLanguage: undefined,
        },
        location: {
            active: false,
            open: false,
            mapOpen: false,
            showMap: false,
            label: 'map'
        },
        details: {
            active: false,
            open: false,
            activeLanguage: undefined,
        },
        description: {
            open: false,
            activeLanguage: undefined,
        },
        shortDescription: {
            open: false,
            activeLanguage: undefined,
        },
        properties: {
            active: false,
            open: false,
            activeLanguage: undefined,
        },
        tags: {
            open: false,
        },
        thumbnail: {
            open: true,
        },
        brand: {
            active: false,
            open: false,
        },
        category: {
            open: false,
        },
        panelFeatures: {
            open: true,
        },
    });
    const [setting, setSetting] = useState({
        saveDialog: false,
        loading: false,
        error: false,
        redirect: false
    });
    //endregion state
    //endregion var

    //region useEffect
    useEffect(() => {
        if (pId) {
            getProduct();
        } else {
            const newData = state.data;
            _.forEach(sLang, (l, key) => {
                newData.description[key] = '';
                newData.shortDescription[key] = '';
                newData.address[key] = '';
                newData.shortAddress[key] = '';
            });
            setState({
                ...state,
                data: {...newData}
            })
        }
        setAllElementState({activeLanguage: getActiveLanguage()})
    }, []);

    useEffect(() => {
        if (setting.loading)
            saveProduct();
    }, [setting.loading]);


    //endregion useEffect

    //region requests
    function getProduct() {
        ControllerProduct.Product.get({pId: pId}).then((res) => {
                const data = res.data;
                setState({
                    ...state,
                    data: {
                        ...data,
                    }
                });
                setElState({
                    ...elState,
                    city: {
                        ...elState.city,
                        active: Boolean(data.city),
                    },
                    address: {
                        ...elState.address,
                        active: Boolean(!_.isEmpty(data.address) || !_.isEmpty(data.shortAddress)),
                    },
                    location: {
                        ...elState.location,
                        active: !_.isEmpty(data.location),
                    },
                    details: {
                        ...elState.details,
                        active: !data.details.isEmpty(data.details),
                    },
                    properties: {
                        ...elState.properties,
                        active: !data.properties.isEmpty(data.properties)
                    },
                    brand: {
                        ...elState.brand,
                        active: !_.isEmpty(data.brand)
                    }
                })

            }
        ).catch(() => {
            setSetting({
                ...setting,
                error: true
            })
        })
    }

    function getSaveProps() {
        const d = state.data;
        const el = elState;

        if (_.isEmpty(d.permalink)) {
            reqCancel('پرمالینک مشکل دارد.');
            return;
        }
        if (_.isEmpty(d.type)) {
            reqCancel('نوع محصول را مشخص کنید.');
            return;
        }
        if (_.isEmpty(d.name)) {
            reqCancel('نام محصول مشکل دارد.');
            return;
        }
        if (_.isEmpty(d.category)) {
            reqCancel('محصول دارای دسته نمیباشد.');
            return;
        }
        if (_.isEmpty(d.tags) || d.tags.length < 3) {
            reqCancel('محصول باید دارای حداقل 3 تگ باشد.');
            return;
        }

        const media = [];
        _.forEach(d.media, (m) => {
            media.push(m.id);
        });

        const tags = [];
        _.forEach(d.tags, (t) => {
            tags.push(t.id)
        });

        const pr = {
            id: d.id,
            boxId: d.box.id,
            permalink: d.permalink,
            name: d.name,
            categoryId: d.category.id,
            description: d.description,
            shortDescription: d.shortDescription,
            city: null,
            address: {},
            shortAddress: {},
            location: {},
            details: {},
            properties: {},
            tags: tags,
            thumbnail: d.thumbnail ? d.thumbnail.id : '',
            media: media,
            brandId: null
        };

        if (!d.id) {
            pr.type = d.type.value
        }

        if (el.brand.active) {
            if (_.isEmpty(d.brand)) {
                reqCancel('برند باید انتخاب شود.');
                handleElChange('brand', {open: true});
                return
            }

            pr.brandId = d.brand.id;
        }
        if (el.city.active) {
            if (_.isEmpty(d.city)) {
                reqCancel('شهر باید انتخاب شود.');
                handleElChange('city', {open: true});
                return
            }
            pr.city = d.city.id;
        }

        if (el.address.active) {
            if (_.isEmpty(d.address.fa) || _.isEmpty(d.shortAddress.fa)) {
                reqCancel('آدرس را درست وارد کنید.');
                handleElChange('address', {open: true});
                return;
            }
            pr.address = d.address;
            pr.shortAddress = d.shortAddress;
        }

        if (el.location.active) {
            if (_.isEmpty(d.location)) {
                reqCancel('آدرس نقشه را درست وارد کنید.');
                handleElChange('location', {open: true});
                return;
            }
            pr.location = d.location;
        }

        if (el.details.active) {
            if (_.isEmpty(d.details)) {
                reqCancel('جزئیات را درست وارد کنید.');
                handleElChange('details', {open: true});
                return;
            }
            pr.details = d.details.toServer(d.details);
        }

        if (el.properties.active) {
            if (_.isEmpty(d.properties)) {
                reqCancel('جزئیات را درست وارد کنید.');
                handleElChange('properties', {open: true});
                return;
            }
            pr.properties = d.properties.toServer(d.properties);
        }

        return pr;
    }

    function saveProduct() {
        const pr = getSaveProps();
        if (!pr)
            return;

        setSetting({
            ...setting,
            loading: false
        });

        ControllerProduct.Product.save(pr).then((res) => {
            enqueueSnackbar(pId ? 'محصول بروز شد.' : 'محصول ذخیره شد.',
                {
                    variant: "success",
                    action: (key) => (
                        <Fragment>
                            <Button onClick={() => {
                                closeSnackbar(key)
                            }}>
                                {lang.get('close')}
                            </Button>
                            <BaseButton
                                variant={"text"}
                                onClick={() => {
                                    closeSnackbar(key);
                                    if (!pId) {
                                        UtilsRouter.goTo(props.history, {routUrl: rout.Product.Single.editProduct(res.data.id)});
                                        return;
                                    }
                                    setSetting({
                                        ...setting,
                                        redirect: true
                                    })
                                }}
                                style={{
                                    marginRight: theme.spacing(0.5)
                                }}>
                                {pId ? " بازگشت به صفحه محصولات" : 'بازگشت به ویرایش محصول'}
                            </BaseButton>
                        </Fragment>
                    )
                });

            const set = {
                ...setting,
                saveDialog: false,
                loading: false,
            };
            if (!pId) {
                set.redirect = true
            }

            setSetting({...set});

        }).catch((e) => {
            reqCancel('مشکلی در ذخیره محصول پیش آمده است.');
            setSetting({
                ...setting,
                saveDialog: false,
                loading: false
            })
        });
    }

    function reqCancel(text = null) {
        if (text)
            enqueueSnackbar(text,
                {
                    variant: "error",
                    action: (key) => (
                        <Fragment>
                            <Button onClick={() => {
                                closeSnackbar(key)
                            }}>
                                {lang.get('close')}
                            </Button>
                        </Fragment>
                    )
                });

        setSetting({
            ...setting,
            saveDialog: false,
            loading: false
        })

    }

    //endregion requests

    //region functions

    function setData(newData) {
        console.log("PermalinkRebuild", {
            ...state,
            ...newData
        })

        setState({
            ...state,
            data: {
                ...state.data,
                ...newData
            }
        })
    }

    function setElementStateItem(el, {loading, open, active, activeLanguage, returnObject = false, ...props}) {
        console.log(`fffffffffasfasfas ${el}`, elState)

        if (el === 'all') {
            setAllElementState({loading, open, active});
            return
        }
        const e = {...elState};
        if (!e[el])
            e[el] = {};
        if (loading !== undefined)
            e[el].loading = loading;
        if (open !== undefined)
            e[el].open = open;
        if (activeLanguage !== undefined)
            e[el].activeLanguage = activeLanguage;
        if (active !== undefined) {
            if (el === 'address' && active && !elState.city.active) {
                e.city.active = true
            }
            if (el === 'city' && !active && elState.address.active) {
                enqueueSnackbar('اول فیلد آدرس را غیر فعال کنید.',
                    {
                        variant: "error",
                        action: (key) => (
                            <Fragment>
                                <Button onClick={() => {
                                    closeSnackbar(key)
                                }}>
                                    {lang.get('close')}
                                </Button>
                            </Fragment>
                        )
                    });
            } else {
                e[el].active = active;
            }
        }
        if (props) {
            _.forEach(props, (p, key) => {
                e[el][key] = p;
            })
        }

        if (returnObject) {
            return e[el];
        }
        setElState({
            ...e
        })
    }

    function setAllElementState(props) {
        const e = elState;
        _.forEach(elState, (el, key) => {
            e[key] = setElementStateItem(key, {...props, returnObject: true});
        });

        setElState({
            ...e
        })
    }

    function isOnEdit(el) {
        return (elState[el] && elState[el].onEdit)
    }

    function isOnLoading(el) {
        return (elState[el] && elState[el].loading)
    }

    //endregion functions

    //region handler
    function handleElChange(el, data) {
        setElementStateItem(el, data)
    }

    //endregion handler

    //region renderVar
    const {data} = state;

    //endregion renderVar


    const onButtonClick = useCallback(({edit}) => {
        setElementStateItem('permalink', {edit: !edit});
    }, []);

    const newSetElementStateItem = useCallback((el, {edit, loading, open, active, activeLanguage, returnObject = false, ...props}) => {
        if (el === 'all') {
            setAllElementState({edit, loading, open, active});
            return
        }
        const e = {...elState};
        if (!e[el])
            e[el] = {};
        if (edit !== undefined)
            e[el].onEdit = edit;
        if (loading !== undefined)
            e[el].loading = loading;
        if (open !== undefined)
            e[el].open = open;
        if (activeLanguage !== undefined)
            e[el].activeLanguage = activeLanguage;
        if (active !== undefined) {
            if (el === 'address' && active && !elState.city.active) {
                e.city.active = true
            }
            if (el === 'city' && !active && elState.address.active) {
                enqueueSnackbar('اول فیلد آدرس را غیر فعال کنید.',
                    {
                        variant: "error",
                        action: (key) => (
                            <Fragment>
                                <Button onClick={() => {
                                    closeSnackbar(key)
                                }}>
                                    {lang.get('close')}
                                </Button>
                            </Fragment>
                        )
                    });
            } else {
                e[el].active = active;
            }
        }
        if (props) {
            _.forEach(props, (p, key) => {
                e[el][key] = p;
            })
        }

        if (returnObject) {
            return e[el];
        }
        setElState({
            ...e
        })
    }, [elState]);

    const newSetData = useCallback((newData) => {
        setData(newData)
        return;
        setState({
            ...state,
            data: {
                ...state.data,
                ...newData
            }
        });
    }, [state]);


    const formController = () => (
        <FormControl
            innerref={formRef}
            flex={1}>
            <Box display={'flex'}
                 flexDirection={'column'}
                 px={3}
                 py={2}>
                <Box display={'flex'} alignItems={'center'} flexWrap={'wrap'} pb={2}>
                    <Typography variant={'body2'} pl={1}>
                        {lang.get('box')}: {data.box.name[activeLang]}
                    </Typography>
                    <BaseLink toHref={data.storageLink}>
                        رفتن به انبار
                    </BaseLink>
                </Box>
                <Name name={data.name}
                      onDataChange={newSetData}/>
                <Permalink
                    pId={pId}
                    data={data.permalink}
                    edit={isOnEdit('permalink')}
                    onDataChange={newSetData}
                    onElChange={newSetElementStateItem}/>
                <ProductType type={data.type}
                             pId={pId}
                             onTypeChange={(type) => {
                                 setData({type: type})
                             }}/>
                <Media
                    boxId={data.box.id}
                    data={data.media}
                    elMedia={_.clone(elState.media)}
                    onElChange={handleElChange}
                    onChange={({list}) => {
                        setData({media: list})
                    }}/>
                <Description description={data.description}
                             elDescription={_.clone(elState.description)}
                             onElChange={handleElChange}
                             onChange={({activeLang, description}) => {
                                 const newData = data.description;
                                 newData[activeLang.key] = editorContentToHtml(description);
                                 setData({description: {...newData}});
                             }}/>
                <Description
                    isShortDescription={true}
                    description={data.shortDescription}
                    elDescription={_.clone(elState.shortDescription)}
                    onElChange={handleElChange}
                    onChange={({activeLang, description}) => {
                        const newData = data.shortDescription;
                        newData[activeLang.key] = editorContentToHtml(description);
                        setData({shortDescription: {...newData}});
                    }}/>
                <City elCity={_.clone(elState.city)}
                      city={data.city}
                      onElChange={handleElChange}
                      onChange={(item) => {
                          setData({city: item})
                      }}/>
                <Address
                    address={data.address}
                    shortAddress={data.shortAddress}
                    elAddress={_.clone(elState.address)}
                    onElChange={handleElChange}
                    onChange={(({address, shortAddress, location}) => {
                        const newData = {};
                        if (address !== undefined)
                            newData.address = address;
                        if (shortAddress !== undefined)
                            newData.shortAddress = shortAddress;
                        setData(newData);
                    })}/>
                <MapCo
                    location={data.location}
                    elLocation={elState.location}
                    onElChange={handleElChange}
                    onChange={(({location}) => {
                        const newData = {};
                        if (location !== undefined)
                            newData.location = location;
                        setData(newData);
                    })}/>
                <Details
                    details={data.details}
                    elDetails={_.clone(elState.details)}
                    onElChange={handleElChange}
                    onChange={(({servingDays, servingHours, phones, text}) => {
                        const newData = data.details[elState.details.activeLanguage.key] ? data.details[elState.details.activeLanguage.key] : {};
                        if (servingDays !== undefined)
                            newData.servingDays = servingDays;
                        if (servingHours !== undefined)
                            newData.servingHours = servingHours;
                        if (phones)
                            newData.phones = phones;
                        if (text)
                            newData.text = editorContentToHtml(text);

                        const newD = data.details;
                        newD[elState.details.activeLanguage.key] = newData;
                        setData({
                            details: {...newD}
                        });
                    })}/>
                <Properties
                    properties={data.properties}
                    elProperties={_.clone(elState.properties)}
                    onChange={({properties}) => {
                        let newData = {};
                        if (properties !== undefined) {
                            newData = properties
                        }
                        setData({
                            properties: newData
                        })
                    }}
                    onElChange={handleElChange}/>
                <Tags
                    tags={data.tags}
                    elTags={_.clone(elState.tags)}
                    onChange={(data) => {
                        setData({'tags': data})
                    }}
                    onElChange={handleElChange}/>
                {false &&
                <React.Fragment>

                </React.Fragment>}
            </Box>
            <SaveDialog open={setting.saveDialog}
                        onChange={(save) => {
                            setTimeout(() => {
                                setSetting({
                                    ...setting,
                                    saveDialog: false,
                                    loading: save
                                })
                            }, 100);
                        }}/>
        </FormControl>
    );


    return (
        setting.redirect ?
            <Redirect
                to={{
                    pathname: _.isString(setting.redirect) ? setting.redirect : rout.Product.rout
                }}/>
            :
            !setting.error ?
                <>
                    <Box>
                        {state.data !== false ?
                            data.box ?
                                <Box display={'flex'}>
                                    {formController()}
                                    {true &&
                                    <Box maxWidth={2 / 7} minWidth={1 / 4} pl={3}>
                                        <StickyBox offsetTop={80} offsetBottom={20}>
                                            <SidePanel
                                                data={data}
                                                elData={_.cloneDeep(elState)}
                                                onDataChange={setData}
                                                onElChange={handleElChange}
                                                onSaveClick={() => {
                                                    setTimeout(() => {
                                                        if (!getSaveProps())
                                                            return;
                                                        setSetting({
                                                            ...setting,
                                                            saveDialog: true
                                                        })
                                                    }, 1000)

                                                }}/>
                                        </StickyBox>
                                    </Box>}
                                </Box> :
                                <SelectBox
                                    onBoxSelected={(box) => {
                                        setData({box: box})
                                    }}/> :
                            <Box p={3}>
                                <Skeleton variant={"rect"} height={600}/>
                            </Box>}
                    </Box>
                    <Backdrop open={setting.loading}
                              style={{
                                  zIndex: '9999'
                              }}>
                        <CircularProgress color="inherit"
                                          style={{
                                              color: cyan[300],
                                              width: 100,
                                              height: 100
                                          }}/>
                    </Backdrop>
                </> :
                <ComponentError tryAgainFun={getProduct}/>
    )
}

//region Components

//region SidePanel

const SidePanel = React.memo(function SidePanel({data, elData, onElChange, onDataChange, onSaveClick, ...props}) {

    console.log("ssssssiiiiiddeee Paneeel", elData)
    return (
        <Box display={'flex'} flexDirection={'column'} {...props}>
            <SidePanelActions mt={2} mb={0.5} onSaveClick={onSaveClick}/>
            <FormContainer open={elData.thumbnail.open}
                           mt={1}
                           title={lang.get("thumbnail_image")}
                           onOpenClick={(e, open) => {
                               onElChange('thumbnail', {open: open})
                           }}>
                <Box px={3} py={1.5} display={'flex'} flexWrap={'wrap'}>
                    <UploadItem
                        boxId={data.box.id}
                        src={data.thumbnail ? data.thumbnail.image : ''}
                        width={serverFileTypes.Image.Thumbnail.width}
                        height={serverFileTypes.Image.Thumbnail.height}
                        type={serverFileTypes.Image.Thumbnail.type}
                        onSelected={(file) => {
                            onDataChange({
                                'thumbnail': file,
                            })
                        }}/>
                </Box>
            </FormContainer>
            <SelectCategory item={data.category} box={data.box} elCategory={elData.category} onElChange={onElChange}
                            onDataChange={onDataChange}/>
            <SelectBrand item={data.brand} box={data.box} elBrand={elData.brand} onElChange={onElChange}
                         onDataChange={onDataChange}/>
            <FormContainer component={Card}
                           open={elData.panelFeatures.open}
                           title={lang.get("features")}
                           mt={2}
                           onOpenClick={(e, open) => {
                               onElChange('panelFeatures', {open: open})
                           }}>
                <Box px={3} py={1.5} display={'flex'} flexWrap={'wrap'}>
                    {
                        Object.keys(elData).map((elKey, i) => {
                            const el = elData[elKey];
                            return (
                                <React.Fragment key={elKey}>
                                    {(el && _.isBoolean(el.active)) &&
                                    <ItemContainer width={0.5} justifyContent={'center'}>
                                        <FormControlLabel
                                            control={
                                                <Switch
                                                    checked={el.active}
                                                    onChange={() => {
                                                        onElChange(elKey, {active: !el.active})
                                                    }}
                                                    value={elKey}
                                                    color="primary"/>
                                            }
                                            label={lang.get(el.label ? el.label : elKey)}
                                            labelPlacement={'top'}/>
                                    </ItemContainer>}
                                </React.Fragment>
                            )
                        })
                    }
                </Box>
            </FormContainer>
            <BoxWithTopBorder component={Card} mt={2}
                              title={lang.get("page_setting")}>
                <Box pr={4.5} pl={3} py={1.5} display={'flex'} flexWrap={'wrap'}>
                    <Box display={'flex'} alignItems={'center'} justifyContent={'center'}>
                        <BaseButton variant={"outlined"} size={'small'}
                                    onClick={() => onElChange('all', {open: true})}
                                    style={{
                                        borderColor: cyan[300],
                                        marginLeft: theme.spacing(1)
                                    }}>
                            بازکردن همه تب‌ها <ExpandMore style={{paddingRight: theme.spacing(0.2)}}/>
                        </BaseButton>
                        <BaseButton variant={"outlined"} size={'small'}
                                    onClick={() => onElChange('all', {open: false})}
                                    style={{
                                        borderColor: deepOrange[300],
                                        marginRight: theme.spacing(1)
                                    }}>
                            بستن همه تب‌ها<ExpandLess style={{paddingRight: theme.spacing(0.2)}}/>
                        </BaseButton>
                    </Box>
                </Box>
            </BoxWithTopBorder>
            <SidePanelActions my={2} onSaveClick={onSaveClick}/>
        </Box>
    )
}, (pp, np) => {
    const ppData = {
        box: pp.data.box,
        thumbnail: pp.data.thumbnail,
        category: pp.data.category,
        brand: pp.data.brand,
    };
    const npData = {
        box: np.data.box,
        thumbnail: np.data.thumbnail,
        category: np.data.category,
        brand: np.data.brand,
    };

    return _.isEqual(pp.elData, np.elData) && _.isEqual(ppData, npData)
});


function SidePanelActions({onSaveClick, ...props}) {
    return (
        <Box {...props}>
            <BaseButton size={'small'}
                        onClick={onSaveClick}
                        style={{
                            backgroundColor: cyan[300],
                            color: '#fff',
                            width: '100%',
                        }}>
                <Typography color={'#fff'} py={1.5} variant={'h6'}
                            display={'flex'} alignItems={'center'}
                            style={{
                                display: 'flex',
                                alignItems: 'center'
                            }}>
                    ذخیره
                    <Save fontSize={"large"} style={{paddingRight: theme.spacing(1)}}/>
                </Typography>
            </BaseButton>
        </Box>
    )
}

function SelectBrand({item, box, elBrand, onElChange, onDataChange, ...props}) {
    const d = ControllerProduct.Brands.get();
    const apiKey = d[0] + box.id;
    const {data, error} = useSWR(apiKey, () => {
        return d[1]({
            step: 100,
            all: true
        })
    });
    const [state, setState] = useState({
        addDialog: false,
    });

    function handleBrandChange(brand) {
        onDataChange({
            brand: brand
        });
    }

    return (
        data ?
            <FormContainer open={elBrand.open}
                           active={elBrand.active}
                           title={lang.get("brand")}
                           onOpenClick={(e, open) => {
                               onElChange('brand', {open: open})
                           }}>
                <Box display={'flex'} justifyContent={'center'} alignItems={'center'}>
                    <Tooltip title={'افزودن'}>
                        <IconButton onClick={() => setState({
                            ...state,
                            addDialog: true
                        })}
                                    style={{
                                        marginRight: theme.spacing(0.5)
                                    }}>
                            <Add/>
                        </IconButton>
                    </Tooltip>
                    <Typography variant={'h6'} py={2} display={'flex'} justifyContent={'center'} alignItems={'center'}
                                style={{
                                    flex: 1,
                                    ...UtilsStyle.disableTextSelection()
                                }}>
                        <Tooltip title={'حذف'}>
                            <IconButton onClick={() => handleBrandChange(null)}
                                        disabled={!item}
                                        style={{
                                            color: red[400],
                                            marginLeft: theme.spacing(0.5)
                                        }}>
                                <DeleteOutlineOutlined/>
                            </IconButton>
                        </Tooltip>
                        برند: {(item && item.name) ? item.name[activeLang] : '------'}
                    </Typography>
                    <Tooltip title={'بروزرسانی'}>
                        <IconButton onClick={() => mutate(apiKey)}
                                    style={{
                                        marginLeft: theme.spacing(0.5)
                                    }}>
                            <Sync/>
                        </IconButton>
                    </Tooltip>
                </Box>
                <Box display={'flex'} flexDirection={'column'} mx={2}
                     style={{
                         height: 300,
                         overflowY: 'auto'
                     }}>
                    {(data.data) && data.data.map((b, i) => (
                        <React.Fragment>
                            <Divider style={{
                                opacity: 0.5
                            }}/>
                            <ButtonBase
                                onClick={() => handleBrandChange(b)}
                                style={{
                                    cursor: 'pointer',
                                    display: 'flex',
                                    justifyContent: 'flex-start',
                                    ...UtilsStyle.disableTextSelection(),
                                }}>
                                <Box key={b.id} py={1} display={'flex'} flex={'wrap'} alignItems={'center'}>
                                    <Radio
                                        checked={item && b.id === item.id}
                                        value="b"
                                        name="radio-button-demo"
                                        inputProps={{'aria-label': 'B'}}
                                    />
                                    <Typography variant={'body1'}>
                                        {b.name[activeLang]}
                                    </Typography>
                                </Box>
                            </ButtonBase>
                        </React.Fragment>
                    ))}
                </Box>
                <EditBrand open={state.addDialog}
                           onClose={(br) => {
                               if (br) {
                                   mutate(apiKey);
                                   handleBrandChange(br)
                               }
                               setState({
                                   ...state,
                                   addDialog: false
                               })
                           }}/>
            </FormContainer> :
            elBrand.active ?
                <Box width={1} pt={2}>
                    <Skeleton height={90} variant={"rect"}/>
                </Box> :
                <React.Fragment/>
    )
}

//region Category
const FullScreenTransition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

function SelectCategory({item, box, elCategory, onElChange, onDataChange, ...props}) {
    const d = ControllerProduct.Categories.getAllCategories({});
    const apiKey = d[0] + box.id;
    const {data, error} = useSWR(apiKey, () => {
        return d[1]({
            boxId: box.id,
        }).then(res => {
            return res.categories;
        })
    });
    const [state, setState] = useState({
        addDialog: false,
    });


    function getData() {
        mutate(apiKey);
    }

    function handleCategoryChange(cat) {
        onDataChange({
            category: cat
        });
    }

    return (
        !error ?
            data ?
                <FormContainer open={elCategory.open}
                               active={elCategory.active}
                               title={lang.get("category")}
                               onOpenClick={(e, open) => {
                                   onElChange('category', {open: open})
                               }}>
                    <Box display={'flex'} justifyContent={'center'} alignItems={'center'}>
                        <Tooltip title={'افزودن'}>
                            <IconButton onClick={() => setState({
                                ...state,
                                addDialog: true
                            })}
                                        style={{
                                            marginRight: theme.spacing(0.5)
                                        }}>
                                <Add/>
                            </IconButton>
                        </Tooltip>
                        <Typography variant={'h6'} py={2} display={'flex'} justifyContent={'center'}
                                    alignItems={'center'}
                                    style={{
                                        flex: 1,
                                        ...UtilsStyle.disableTextSelection()
                                    }}>
                            دسته: {item ? item.name[activeLang] : '------'}
                        </Typography>
                        <Tooltip title={'بروزرسانی'}>
                            <IconButton onClick={getData}
                                        style={{
                                            marginLeft: theme.spacing(0.5)
                                        }}>
                                <Sync/>
                            </IconButton>
                        </Tooltip>
                    </Box>
                    <Box display={'flex'} flexDirection={'column'} mx={2}
                         style={{
                             height: 300,
                             overflowY: 'auto'
                         }}>
                        <CategoryContainer activeItem={item} items={data}
                                           activeParents={item ? getActiveParents(item.id, data) : []}
                                           onCategoryChange={handleCategoryChange}/>
                    </Box>
                    <Dialog fullScreen open={state.addDialog} TransitionComponent={FullScreenTransition}>
                        <CategorySingle
                            box={box}
                            onClose={(cat) => {
                                if (cat) {
                                    handleCategoryChange(cat);
                                    getData();
                                }
                                setState({
                                    ...state,
                                    addDialog: false
                                })
                            }}/>
                    </Dialog>
                </FormContainer> :
                <Box width={1} pt={2}>
                    <Skeleton height={90} variant={"rect"}/>
                </Box> :
            <ComponentError tryAgainFun={getData}/>
    )
}

function CategoryContainer({activeItem, items, level = 0, activeParents = [], onCategoryChange, ...props}) {
    return (
        items ?
            <Box width={1} px={1} py={1}
                 boxShadow={1}
                 style={{
                     backgroundColor: level !== 0 ? grey[level * 100] : null,
                     ...UtilsStyle.borderRadius('0 0 5px 5px')
                 }}>
                {
                    items.map((cat, i) => (
                        <CategoryItem key={cat.id} index={i} activeItem={activeItem}
                                      activeParents={activeParents}
                                      item={cat} level={level}
                                      onCategoryChange={onCategoryChange}/>
                    ))
                }
            </Box> :
            <React.Fragment/>
    )
}

function CategoryItem({activeItem, index, item, level = 0, activeParents, onCategoryChange, ...props}) {
    const [state, setState] = useState({
        open: false,
    });

    const isActive = _.findIndex(activeParents, (p) => p.id === item.id) !== -1;

    return (
        <React.Fragment>
            {index !== 0 &&
            <Divider style={{
                opacity: 0.5
            }}/>}
            <Box width={1} display={'flex'} alignItems={'center'}>
                <ButtonBase
                    onClick={() => onCategoryChange(item)}
                    style={{
                        cursor: 'pointer',
                        display: 'flex',
                        flexWrap: 'wrap',
                        justifyContent: 'flex-start',
                        flex: 1,
                        ...UtilsStyle.disableTextSelection(),
                    }}>
                    <Box key={item.id} py={1} display={'flex'} flex={'wrap'} alignItems={'center'}>
                        <Radio
                            checked={(activeItem && item.id === activeItem.id || isActive)}
                            value="b"
                            name="radio-button-demo"
                            inputProps={{'aria-label': 'B'}}
                            style={{
                                color: (activeItem && item.id === activeItem.id) ? red[700] : isActive ? red[200] : null
                            }}
                        />
                        <Typography variant={'body1'}>
                            {item.name[activeLang]}
                        </Typography>
                    </Box>
                </ButtonBase>
                {item.child &&
                <Box mx={0.5}>
                    <Tooltip title={"نمایش فرزندان"}>
                        <IconButton onClick={() => setState({
                            ...state,
                            open: !state.open
                        })}>
                            {state.open ? <ExpandLess/> : <ExpandMore/>}
                        </IconButton>
                    </Tooltip>
                </Box>}
            </Box>
            {item.child &&
            <Collapse in={state.open}>
                <CategoryContainer activeItem={activeItem} items={item.child}
                                   activeParents={activeParents}
                                   onCategoryChange={onCategoryChange}
                                   level={level + 1}/>
            </Collapse>
            }
        </React.Fragment>
    )
}

//endregion Category
//endregion SidePanel


const Name = React.memo(function Name({name, onDataChange}) {
    return (
            <BaseTextFieldMultiLanguage
                variant={"outline"}
                value={name}
                label={lang.get('product_name')}
                isRequired
                validatable={true}
                onTextChange={(text) => {

                    onDataChange({name: text})
                }}
                textFieldStyle={{
                    width: '100%'
                }}/>
    )
}, (pp, np) => {
    return _.isEqual(pp.name, np.name);
});

const Permalink = React.memo(function Permalink({pId, edit, loading, data = "", onDataChange, onElChange}) {
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();

    const [state, setState] = useState({
        loading: false
    });

    console.log("PermalinkRebuild")

    function onSaveHandler() {

        if (!edit) {
            onElChange("permalink", {
                edit: true
            });
            return;
        }
        if (_.isEmpty(data)) {
            enqueueSnackbar(`لطفا فیلد ${lang.get('permalink')} را پر کنید.`,
                {
                    variant: "error",
                    action: (key) => (
                        <Fragment>
                            <Button onClick={() => {
                                closeSnackbar(key)
                            }}>
                                {lang.get('close')}
                            </Button>
                        </Fragment>
                    )
                });
            return;
        }

        if (pId) {
            setState({
                ...state,
                loading: true
            });
            ControllerProduct.Product.updatePermalink({pId: pId, permalink: data}).then((res) => {
                setState({
                    ...state,
                    loading: false
                });
                onElChange("permalink", {
                    edit: false
                });
                enqueueSnackbar(`با موفقیت ${lang.get('permalink')} تفییر کرد.`,
                    {
                        variant: "success",
                        action: (key) => (
                            <Fragment>
                                <Button onClick={() => {
                                    closeSnackbar(key)
                                }}>
                                    {lang.get('close')}
                                </Button>
                            </Fragment>
                        )
                    });
            }).catch((e) => {
                setState({
                    ...state,
                    loading: false
                });

                enqueueSnackbar(`تغییر ${lang.get('permalink')} به مشکل برخود.`,
                    {
                        variant: "error",
                        action: (key) => (
                            <Fragment>
                                <Button onClick={() => {
                                    closeSnackbar(key)
                                }}>
                                    {lang.get('close')}
                                </Button>
                            </Fragment>
                        )
                    });
            });
            return;
        }

        onElChange("permalink", {
            edit: false
        });
    }

    return (
        <ItemContainer flexWrap={'wrap'} alignItems={'flex-end'}>
            {
                (!edit) ?
                    <Typography variant={'subtitle1'}>
                        <Box display={'flex'} alignItems={'center'}>
                            {lang.get("permalink")}:
                            <Typography component={pId ? 'a' : 'span'}
                                        variant={'subtitle1'}
                                        color={grey[800]}
                                        mr={1}
                                        target={'_blank'}
                                        dir={'ltr'}
                                        href={webRout + siteRout.Product.Single.create({permalink: data})}>
                                {webRout + siteRout.Product.Single.create({permalink: data})}
                            </Typography>
                        </Box>
                    </Typography> :
                    <BaseTextField
                        variant={"standard"}
                        value={data}
                        disabled={state.loading}
                        dir={'ltr'}
                        pattern={'^[A-Za-z\u0591-\u07FF\uFB1D-\uFDFD\uFE70-\uFEFC][A-Za-z0-9-\u0591-\u07FF\uFB1D-\uFDFD\uFE70-\uFEFC]*$'}
                        label={lang.get("permalink")}
                        isRequired
                        validatable={true}
                        onTextChange={(text) => {
                            onDataChange({permalink: text})
                        }}
                        style={{
                            minWidth: '40%'
                        }}/>
            }
            <BaseButton
                variant={'outlined'}
                size={"superSmall"}
                fontWeight={300}
                loading={state.loading}
                onClick={onSaveHandler}
                style={{
                    border: `1px solid ${loading ? grey[400] : cyan[300]}`,
                    marginRight: theme.spacing(1.5),
                }}>
                {lang.get(!edit ? 'edit' : 'submit')}
            </BaseButton>
        </ItemContainer>
    )
}, (pp, np) => {
    return !(!_.isEqual(pp.pId, np.pId) || !_.isEqual(pp.edit, np.edit) || !_.isEqual(pp.loading, np.loading) || !_.isEqual(pp.data, np.data));
});

const ProductType = React.memo(function ProductType({pId, type, onTypeChange}) {
    return (
        <ItemContainer flexWrap={'wrap'} alignItems={'center'}>
            <Typography variant={'body1'}>
                {lang.get("type")}:
            </Typography>
            {pId ?
                <Typography variant={'body1'} fontWeight={600} pr={1}>
                    {type.label}
                </Typography> :
                (
                    <RadioGroup row aria-label="position" name="position" value={type.value} onChange={(e, type) => {
                        onTypeChange({
                            ...type,
                            value: type
                        })
                    }}>
                        <FormControlLabel
                            value="service"
                            control={<Radio color="primary"/>}
                            label="سرویس"
                            labelPlacement="end"
                        />
                        <FormControlLabel
                            value="product"
                            control={<Radio color="primary"/>}
                            label="فیزیکی"
                            labelPlacement="end"
                        />
                    </RadioGroup>
                )}
        </ItemContainer>
    )
}, (pp, np) => {
    return _.isEqual(pp.type, np.type)
});

//region Media
const Media = React.memo(function Media({boxId, data, elMedia, onElChange, onChange, ...props}) {
    const [state, setState] = useState({
        sorting: false
    });

    return (
        <FormContainer open={elMedia.open}
                       title={lang.get("media")}
                       onOpenClick={(e, open) => {
                           onElChange('media', {open: open})
                       }}>
            <Box py={2} px={2}>
                {lang.get("sorting")}: <Switch value={state.sorting} disabled={!data || data.length < 2}
                                               onChange={(v) => {
                                                   setState({
                                                       ...state,
                                                       sorting: !state.sorting
                                                   })
                                               }}/>
            </Box>
            {
                state.sorting ?
                    <Box width={1}>
                        <DraggableList
                            items={data}
                            onItemsChange={(items) => {
                                onChange({list: items});
                            }}
                            rootStyle={(dragging) => {
                                return {
                                    backgroundColor: dragging ? null : grey[200],
                                    ...UtilsStyle.transition(500)
                                }
                            }}
                            render={(item, props, {isDraggingItem, ...p}) => (
                                <Box key={item.id}
                                     display={'flex'}
                                     width={1}
                                     {...props}>
                                    <Box display={'flex'} alignItems={'center'}
                                         py={1}>
                                        <Box px={2} py={1}>
                                            {isDraggingItem ?
                                                <MenuOpen/> :
                                                <Menu/>
                                            }
                                        </Box>
                                        <Img maxWidth={70} minHeight={'unset'} src={item.image}/>
                                        <Typography pr={2} pl={1} variant={'body1'} fontWeight={600}
                                                    style={{
                                                        justifyContent: 'center',
                                                    }}>
                                            {item.title.fa}
                                        </Typography>
                                    </Box>
                                </Box>
                            )}
                        />
                    </Box> :
                    <Box
                        display={'flex'}
                        flexWrap={'wrap'}
                        width={1}>
                        {data.map(item => (
                            <MediaItem key={item.id} item={item} onRemove={() => {
                                const newList = [...data];
                                _.remove(newList, d => d.id === item.id);
                                onChange({list: newList});
                            }}/>
                        ))}
                        <Box width={1 / 3} px={2} py={2} display={'flex'} alignItems={'center'}
                             justifyContent={'center'}>
                            <UploadItem
                                boxId={boxId}
                                holderWidth={200}
                                holderHeight={'auto'}
                                width={serverFileTypes.Image.Media.width}
                                height={serverFileTypes.Image.Media.height}
                                type={serverFileTypes.Image.Media.type}
                                onSelected={(file) => {
                                    onChange({list: [...data, file]})
                                }}/>
                        </Box>
                    </Box>
            }
        </FormContainer>
    )
}, (pp, np) => {
    return !(!_.isEqual(pp.data, np.data) || !_.isEqual(pp.elMedia, np.elMedia))
});


function MediaItem({item, onRemove, ...props}) {
    return (
        <Box px={2} py={2} width={1 / 3}>
            <Box component={Card} display={'flex'} flexDirection={'column'}>
                <Img src={item.image}/>
                <Box display={'flex'} flexWrap={'wrap'} py={1} px={1} alignItems={'center'}>
                    <Box pl={1}>
                        <IconButton onClick={() => onRemove()}>
                            <Delete fontSize={"small"}/>
                        </IconButton>
                    </Box>
                    <Typography variant={'body1'} fontWeight={600} style={{
                        justifyContent: 'center',
                    }}>
                        {item.title.fa}
                    </Typography>
                </Box>
            </Box>
        </Box>
    )
}

//endregion Media

const Description = React.memo(function Description({description, elDescription, isShortDescription = false, onElChange, onChange}) {
    const [data, setData] = useState({});
    const [orgDescription, setOrgDescription] = useState({
        firstTime: false
    });
    const [timer, setTimer] = useState(null);

    useEffect(() => {
        const newDescription = {};
        _.forEach(sLang, (l, key) => {
            newDescription[key] = htmlToEditorContent(description[key] ? description[key] : '')
        });
        setData(newDescription)

        return () => {
            clearTimeout(timer);
        }
    }, []);

    useEffect(() => {
        const newOrgDescription = orgDescription;
        if (!orgDescription.firstTime) {
            _.forEach(sLang, (l, key) => {
                newOrgDescription[key] = Boolean(description[key])
            });
            setOrgDescription({
                ...newOrgDescription,
                firstTime: true
            });
            return
        }
        newOrgDescription[elDescription.activeLanguage.key] = Boolean(description[elDescription.activeLanguage.key]);

        setOrgDescription({
            ...newOrgDescription,
        });
    }, [description]);

    function handlerDescriptionChange(content) {
        const newData = data;
        newData[elDescription.activeLanguage.key] = content;
        setData({
            ...newData
        });

        if (timer != null)
            clearTimeout(timer);
        setTimer(setTimeout(() => {
            onChange({
                activeLang: elDescription.activeLanguage,
                description: content
            })
        }, 4000))
    }

    return (
        <ItemContainer>
            <FormContainer open={elDescription.open}
                           width={1}
                           title={lang.get(!isShortDescription ? "description" : 'short_description')}
                           activeLanguage={elDescription.activeLanguage}
                           activeLanguageValue={orgDescription}
                           onLanguageChange={(lang) => {
                               onElChange(!isShortDescription ? 'description' : 'shortDescription', {activeLanguage: lang})
                           }}
                           onOpenClick={(e, open) => {
                               onElChange(!isShortDescription ? 'description' : 'shortDescription', {open: open})
                           }}>
                <Editor
                    content={data[elDescription.activeLanguage.key]}
                    onContentChange={handlerDescriptionChange}/>
            </FormContainer>
        </ItemContainer>
    )
}, (pp, np) => {
    return !(!_.isEqual(pp.description, np.description) || !_.isEqual(pp.elDescription, np.elDescription))
});

const City = React.memo(function City({city, elCity, onElChange, onChange, ...props}) {

    const [setting, setSetting] = useState({
        firstRequest: false
    });

    const [state, setState] = useState({
        activeItem: null
    });
    const [cityState, setCityState] = useState({
        data: null,
    });

    useEffect(() => {
        if (city) {
            const index = _.findIndex(states, (s) => {
                return s.id === city.state
            });
            if (index === -1)
                return;
            setState({
                activeItem: states[index]
            })
        }
    }, []);


    useEffect(() => {
        if (setting.countChange <= 2) {
            setSetting({
                ...setting,
                countChange: setting.countChange + 1
            })
            return
        }

        if (state.activeItem) {
            setCityState({
                ...cityState,
                data: [],
            });
            handleCityChange(null)
            getCity(state.activeItem.id)
        }
    }, [state.activeItem]);

    function getCity(stateId) {
        ControllerSite.cities.get({stateId: stateId}).then((res) => {
            const data = {
                ...cityState,
                data: res.data.cities,
            };
            setCityState(data);
            if (!setting.firstRequest) {
                setSetting({
                    ...setting,
                    firstRequest: true
                });
                onChange(city)
            }
        }).catch(e => {
            setCityState({
                ...cityState,
                data: undefined
            })
        })
    };


    function handleCityChange(item) {
        if (setting.firstRequest)
            onChange(item)
    }

    return (
        <FormContainer open={elCity.open} active={elCity.active}
                       title={lang.get("city")}
                       onOpenClick={(e, open) => {
                           onElChange('city', {open: open})
                       }}>
            <Box display={'flex'} px={2} py={2}>
                <Box width={0.5} pl={1}>
                    <AutoFill
                        inputId='stateInput'
                        items={states}
                        disabled={(_.isArray(cityState.data) && _.isEmpty(cityState.data)) || props.disable}
                        activeItem={state.activeItem}
                        onActiveItemChanged={(item) => {
                            setState({
                                ...state.state,
                                activeItem: item
                            });
                        }}
                        title={lang.get('state')}
                        placeholder={lang.get('search_state')}/>
                </Box>
                <Box width={0.5} pr={1}>
                    <AutoFill
                        inputId='cityInput'
                        items={cityState.data ? cityState.data : []}
                        disabled={(_.isEmpty(cityState.data)) || props.disable}
                        activeItem={city}
                        onActiveItemChanged={(item) => {
                            handleCityChange(item)
                        }}
                        title={lang.get('city')}
                        placeholder={lang.get('search_city')}/>
                </Box>
            </Box>
        </FormContainer>
    )
}, (pp, np) => {
    return !(!_.isEqual(pp.city, np.city) || !_.isEqual(pp.elCity, np.elCity))
});

//region Address

const Address = React.memo(function Address({address: orgAddress, shortAddress: orgShortAddress, elAddress, onElChange, onChange, ...props}) {

    const [address, setAddress] = useState(orgAddress[elAddress.activeLanguage.key]);
    const [addressTimer, setAddressTimer] = useState(null);
    const [shortAddress, setShortAddress] = useState(orgShortAddress[elAddress.activeLanguage.key]);
    const [shortAddressTimer, setShortAddressTimer] = useState(null);

    console.log("fffffffffsafasfasfa")

    useEffect(() => {
        setAddress(orgAddress[elAddress.activeLanguage.key]);
        setShortAddress(orgShortAddress[elAddress.activeLanguage.key]);
    }, [elAddress.activeLanguage.key]);

    return (
        <FormContainer open={elAddress.open} active={elAddress.active}
                       title={lang.get("address")}
                       activeLanguageValue={[orgAddress, orgShortAddress]}
                       activeLanguage={elAddress.activeLanguage}
                       onLanguageChange={(lang) => {
                           onElChange('address', {activeLanguage: lang})
                       }}
                       onOpenClick={(e, open) => {
                           onElChange('address', {open: open})
                       }}>
            <Box display={'flex'} px={2} py={1}>
                <ItemContainer width={0.4}>
                    <BaseTextField
                        variant={"standard"}
                        value={shortAddress}
                        label={lang.get("short_address")}
                        isRequired
                        validatable={true}
                        onTextChange={(text) => {
                            setShortAddress(text);
                            // if (shortAddressTimer != null)
                            //     clearTimeout(shortAddressTimer);
                            // setShortAddressTimer(setTimeout(() => {
                            //     const newVal = orgShortAddress;
                            //     newVal[elAddress.activeLanguage.key] = text;
                            //     onChange({shortAddress: newVal})
                            // }, 1500));
                        }}
                        style={{
                            minWidth: '90%'
                        }}/>
                </ItemContainer>
            </Box>
        </FormContainer>
    )
}, (pp, np) => {
    return !(!_.isEqual(pp.address, np.address) || !_.isEqual(pp.shortAddress, np.shortAddress) || !_.isEqual(pp.elAddress, np.elAddress))
});


//endregion Address

//region Map

const MapCo = React.memo(function MapCo({location, elLocation, onElChange, onChange, ...props}) {
    return (
        <FormContainer open={elLocation.open}
                       active={elLocation.active}
                       title={lang.get("map")}
                       flexDirection={'column'}
                       onOpenClick={(e, open) => {
                           onElChange('location', {open: open})
                       }}>
            <Box display={'flex'} flexWrap={'wrap'} px={1} py={2} justifyContent={'center'} alignItems={'center'}>
                <BaseButton variant={'contained'}
                            onClick={() => onElChange('location', {mapOpen: true})}
                            style={{marginLeft: !_.isEmpty(location) ? theme.spacing(1) : null}}>
                    <Map style={{marginLeft: theme.spacing(0.5)}}/>
                    {lang.get(location ? "edit_address" : "add_address")}
                </BaseButton>
                {!_.isEmpty(location) ?
                    <BaseButton variant={"text"} size={'small'}
                                onClick={() => onChange({location: null})}
                                style={{marginRight: theme.spacing(1)}}>
                        <Close fontSize={'small'} style={{marginLeft: theme.spacing(0.5)}}/>
                        حذف آدرس
                    </BaseButton>
                    : null}
            </Box>
            {!_.isEmpty(location) ?
                <Box my={2} mx={2} height={elLocation.showMap ? 400 : 300}
                     display='flex'
                     justifyContent={'center'}
                     alignItems={'center'}
                     style={{
                         backgroundImage: elLocation.showMap ? null : `url(${MapImage})`,
                         border: `1px solid ${grey[700]}`,
                         backgroundRepeat: 'no-repeat',
                         backgroundSize: 'cover',
                         ...UtilsStyle.borderRadius(5)
                     }}>

                    {elLocation.showMap ?
                        <MapComponent
                            location={[
                                location.lat,
                                location.lng,
                            ]}/> :
                        <BaseButton
                            onClick={() => onElChange('location', {showMap: true})}
                            style={{
                                backgroundColor: '#fff',
                                color: grey[800],
                                padding: theme.spacing(2)
                            }}>
                            {lang.get("show_address_on_map")}
                            <Map
                                style={{
                                    marginRight: theme.spacing(1)
                                }}/>
                        </BaseButton>}
                </Box>
                : null
            }
            <Dialog
                open={elLocation.mapOpen}
                fullScreen
                aria-labelledby="address_edit_alert-dialog-title"
                aria-describedby="address_edit_alert-dialog-description">
                <AppBar style={{position: 'relative'}}>
                    <Toolbar
                        style={{
                            display: 'flex',
                            flexDirection: 'row-reverse',
                        }}>
                        <IconButton edge="start" color="inherit"
                                    onClick={() => onElChange('location', {mapOpen: false})}
                                    aria-label="close">
                            <Close/>
                        </IconButton>
                        <Typography variant="h6" ml={2} color={"#fff"}>
                            {lang.get(location ? "edit_address" : "add_address")}
                        </Typography>
                    </Toolbar>
                </AppBar>
                <Box display='flex'
                     flexDirection='column'
                     height={1}
                     width={1}>
                    <FullScreenMap
                        firstLocation={{
                            lat: (location && location.lat) ? location.lat : 37.28049,
                            lng: (location && location.lng) ? location.lng : 49.59051
                        }}
                        onReturnClick={() => onElChange('location', {mapOpen: false})}
                        onSubmitClick={(val) => {
                            onChange({location: val})
                            onElChange('location', {mapOpen: false})
                        }}/>
                </Box>
            </Dialog>
        </FormContainer>
    )
}, (pp, np) => {
    return !(!_.isEqual(pp.elLocation, np.elLocation) || !_.isEqual(pp.location, np.location))
});

let DefaultIcon = L.icon({
    iconUrl: icon,
});
L.Marker.prototype.options.icon = DefaultIcon;
const useMapStyles = makeStyles({
    map: {
        '& .leaflet-marker-pane img': {
            top: -41,
            right: -12.5,
            width: 25,
            height: 41
        },
        '& .leaflet-popup': {
            bottom: '22px !important',
        }
    }
});

function MapComponent({location, ...props}) {
    const classes = useMapStyles(props);
    return (
        <LeafletMap
            className={classes.map}
            animate={true}
            center={location}
            doubleClickZoom={true}
            boxZoom={true}
            zoomControl={true}
            minZoom={7}
            zoom={14}
            maxZoom={19}
            length={4}
            style={{
                width: '100%',
                height: '100%',
            }}>
            <TileLayer
                attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"/>
            <Marker position={location}/>
        </LeafletMap>
    )
}

const useMapStyle = makeStyles(theme => ({
    mapContainer: {},
    mapSubmitBtn: {
        paddingTop: theme.spacing(2),
        paddingBottom: theme.spacing(2),
        ...UtilsStyle.borderRadius(5),
    },
    mapSubmitBtnContainer: {
        position: 'absolute',
        zIndex: 3,
        bottom: 0,
        left: 0,
        right: 0,
        width: '100%',
    },
}));

export function FullScreenMap(props) {
    const {firstLocation, onSubmitClick, onReturnClick} = props;
    const classes = useMapStyle(props);
    const defultLocation = firstLocation ? firstLocation : {
        lat: 37.28049,
        lng: 49.59051,
    };
    const [map, setMap] = React.useState({
        centerLocation: defultLocation,
    });
    return (
        <Box display='flex'
             boxShadow={3}
             className={classes.mapContainer}
             flexDirection='column'
             width={1}
             height={1}>
            <AddAddressMapComponent
                centerLocation={map.centerLocation}
                targetLocation={defultLocation}
                onMoveEnd={(center) => {
                    setMap({
                        ...map,
                        centerLocation: center
                    })
                }}
                style={{
                    zIndex: 2
                }}/>
            <Box className={classes.mapSubmitBtnContainer} display='flex'>
                <Box width="70%">
                    <BaseButton
                        disabled={false}
                        size='large'
                        className={classes.mapSubmitBtn}
                        onClick={() => onSubmitClick(map.centerLocation)}
                        style={{
                            backgroundColor: blue[500],
                            color: "#fff",
                            width: '100%'
                        }}>
                        {lang.get('submit_location')}
                    </BaseButton>
                </Box>
                <Box width='30%'>
                    <BaseButton
                        disabled={false}
                        variant={"text"}
                        size='large'
                        className={classes.mapSubmitBtn}
                        onClick={onReturnClick}
                        style={{
                            backgroundColor: red[500],
                            color: "#fff",
                            width: '100%'
                        }}>
                        {lang.get('return')}
                    </BaseButton>
                </Box>
            </Box>
        </Box>
    )
}

FullScreenMap.propTypes = {
    firstLocation: PropTypes.object,
    onSubmitClick: PropTypes.func.isRequired,
    onReturnClick: PropTypes.func.isRequired
};
//endregion Map

//region Details

const detailsDefaultValue = {
    servingDays: {
        fa: [
            {
                name: 'خالی',
                data: '',
            },
            {
                name: 'با هماهنگی',
            },
            {
                name: 'شنبه تا پنجشنبه',
            },
            {
                name: 'پنج شنبه و جمعه',
            },
        ],
        en: [
            {
                name: 'none',
                data: '',
            },
            {
                name: 'Harmony',
            },
            {
                name: 'Saturday to Thursday',
            },
            {
                name: 'Thursday and Friday',
            },
        ],
    },
    servingHours: {
        fa: [
            {
                name: 'خالی',
                data: '',
            },
            {
                name: 'با هماهنگی',
            },
            {
                name: '24 ساعته',
            },
            {
                name: '8 صبح تا 12 شب',
            },
            {
                name: '8 صبح تا 1 ظهر - 5 غروب تا 12 شب',
            },
        ],
        en: [
            {
                name: 'none',
                data: '',
            },
            {
                name: 'Harmony',
            },
            {
                name: '24 hour',
            },
            {
                name: '8am to 12pm',
            },
            {
                name: '8am to 1pm - 5pm to 12pm',
            },
        ],
    }
}

const Details = React.memo(function Details({details, elDetails, onElChange, onChange, ...props}) {

    const [state, setState] = useState({
        servingDays: true,
        servingHours: true,
    });

    return (
        <FormContainer open={elDetails.open} active={elDetails.active}
                       activeLanguageValue={details}
                       activeLanguage={elDetails.activeLanguage}
                       title={lang.get("details")}
                       onLanguageChange={(lang) => {
                           onElChange('details', {activeLanguage: lang})
                       }}
                       onOpenClick={(e, open) => {
                           onElChange('details', {open: open})
                       }}>
            <Box display={'flex'} px={2} py={1} flexWrap={'wrap'}>
                <ItemContainer width={0.5} flexDirection={'column'}>
                    <BaseTextField
                        variant={"standard"}
                        value={details[elDetails.activeLanguage.key] ? details[elDetails.activeLanguage.key].servingDays : ''}
                        label={lang.get("serving_days")}
                        validatable={true}
                        onTextChange={(text) => {
                            onChange({servingDays: text});
                        }}
                        style={{
                            width: '90%'
                        }}/>
                    {detailsDefaultValue.servingDays[elDetails.activeLanguage.key] ?
                        <TemplateContainer>
                            {detailsDefaultValue.servingDays[elDetails.activeLanguage.key].map(({name, data}, index) => (
                                <TemplateItem key={index} title={name} data={data}
                                              onClick={(data) => {
                                                  setState({
                                                      servingDays: data
                                                  })
                                              }}/>
                            ))}
                        </TemplateContainer>
                        : null}
                </ItemContainer>
                <ItemContainer width={0.5} flexDirection={'column'}>
                    <BaseTextField
                        variant={"standard"}
                        value={details[elDetails.activeLanguage.key] ? details[elDetails.activeLanguage.key].servingHours : ''}
                        label={lang.get("serving_hours")}
                        validatable={true}
                        onTextChange={(text) => {
                            onChange({servingHours: text})
                        }}
                        style={{
                            width: '90%'
                        }}/>
                    {detailsDefaultValue.servingHours[elDetails.activeLanguage.key] ?
                        <TemplateContainer>
                            {detailsDefaultValue.servingHours[elDetails.activeLanguage.key].map(({name, data}, index) => (
                                <TemplateItem key={index} title={name} data={data}
                                              onClick={(data) => {
                                                  setState({
                                                      servingHours: data
                                                  })
                                              }}/>
                            ))}
                        </TemplateContainer>
                        : null}
                </ItemContainer>
                <ItemContainer width={1} flexDirection={'column'} mt={3}>
                    <CallItems
                        phones={details[elDetails.activeLanguage.key] ? details[elDetails.activeLanguage.key].phones : []}
                        onChange={({phones}) => {
                            onChange({phones: phones})
                        }}/>
                </ItemContainer>
                <ItemContainer width={1} flexDirection={'column'} mt={3}>
                    <Typography variant={'h6'} py={2} pr={2}>
                        توضیحات اضافه:
                    </Typography>
                    <DetailsText details={details} elDetails={elDetails}
                                 onElChange={onElChange}
                                 onChange={onChange}/>
                </ItemContainer>
            </Box>
        </FormContainer>
    )
}, (pp, np) => {
    return !(!_.isEqual(pp.elDetails, np.elDetails) || !_.isEqual(pp.details, np.details))
});

function CallItems({phones = [], onChange, ...props}) {
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();
    const [state, setState] = useState({
        value: '',
        callMethod: 0,
        open: false,
        onEdit: null,
    });

    function handlePhoneAdd() {
        setTimeout(() => {
            if (!state.value) {
                enqueueSnackbar('لطفا شماره تلفن را درست وارد کنید.',
                    {
                        variant: "error",
                        action: (key) => (
                            <Fragment>
                                <Button onClick={() => {
                                    closeSnackbar(key)
                                }}>
                                    {lang.get('close')}
                                </Button>
                            </Fragment>
                        )
                    });
                return
            }
            const ph = [...phones];
            if (state.onEdit) {
                ph.splice(state.onEdit.index, 0, {
                    id: state.onEdit.id,
                    type: state.callMethod === 0 ? 'mobile' : 'phone',
                    value: _.clone(state.value),
                })
            } else {
                ph.push({
                    id: UtilsData.createId(phones),
                    type: state.callMethod === 0 ? 'mobile' : 'phone',
                    value: _.clone(state.value),
                });
            }
            onChange({phones: ph});
            setState({
                ...state,
                open: false,
                callMethod: 0,
                value: '',
                onEdit: null,
            })
        }, 1000);
    }

    function handlePhoneCancel() {
        setState({
            ...state,
            open: false,
            callMethod: 0,
            value: ''
        })
    }

    function handlePhoneEdit(phone, index) {
        const isMobile = phone.type === 'mobile';
        setState({
            open: true,
            callMethod: isMobile ? 0 : 1,
            value: phone.value,
            onEdit: {
                id: phone.id,
                index: index,
                callMethod: isMobile ? 0 : 1,
                value: phone.value,
            }
        });
        handlePhoneRemove(phone, index)
    }

    function handlePhoneRemove(phone, index) {
        const ph = [...phones];
        ph.splice(index, 1);
        onChange({phones: ph})
    }


    return (
        <Box display={'flex'} flexDirection={'column'} mt={3}>
            <Typography variant={'body1'}>
                {lang.get('call_numbers')}:
            </Typography>
            {!state.open ?
                <Box display={'flex'} mt={2}>
                    <BaseButton variant={"outlined"} size={"small"}
                                onClick={() => {
                                    setState({
                                        ...state,
                                        open: true
                                    })
                                }}
                                style={{borderColor: cyan[300], color: grey[700]}}>
                        <Add style={{marginLeft: theme.spacing(0.3)}}/>{lang.get('add')}
                    </BaseButton>
                </Box> : null}
            <Collapse in={state.open}>
                <Box display={'flex'} flexWrap={'wrap'}>
                    <Select
                        labelId="call method"
                        value={state.callMethod}
                        onChange={(e) => {
                            setState({
                                ...state,
                                callMethod: e.target.value
                            })
                        }}
                        style={{
                            marginTop: theme.spacing(1)
                        }}>
                        <MenuItem value={0}>
                            <Box display={'flex'} alignItems={'center'}>
                                <Phone fontSize={'small'} style={{marginLeft: theme.spacing(0.3)}}/>
                                موبایل
                            </Box>
                        </MenuItem>
                        <MenuItem value={1}>
                            <PhoneIphone fontSize={'small'} style={{marginLeft: theme.spacing(0.3)}}/>
                            تلفن
                        </MenuItem>
                    </Select>
                    <Box mr={2} mt={1}>
                        <BaseTextField value={state.value}
                                       label={'شماره'}
                                       dir={"ltr"}
                                       optionalLabel={''}
                                       onTextChange={(text) => setState({
                                           ...state,
                                           value: text
                                       })}
                                       style={{
                                           minWidth: 300
                                       }}/>
                    </Box>
                    <Box mt={1} mr={2} display={'flex'} flexWrap={'wrap'} alignContent={'flex-end'}>
                        <BaseButton
                            onClick={handlePhoneAdd}
                            style={{
                                backgroundColor: cyan[300],
                                color: '#fff',
                            }}>
                            {lang.get('save')}
                        </BaseButton>
                        <BaseButton variant={"text"} size={'small'}
                                    onClick={handlePhoneCancel}
                                    style={{marginRight: theme.spacing(0.7)}}>
                            {lang.get('cancel')}
                        </BaseButton>
                    </Box>
                </Box>
            </Collapse>
            <Box display={'flex'} flexDirection={'column'} mt={2}>
                <DraggableList
                    items={phones}
                    onItemsChange={(items) => {
                        onChange({phones: items});
                    }}
                    render={(item, props, {index}) => {
                        const isMobile = item.type === 'mobile';
                        return (
                            <Box key={item.id}
                                 display={'flex'}
                                 alignItems={'center'}
                                 py={1}
                                 px={1.5}
                                 my={1}
                                 {...props}
                                 style={{
                                     ...props.style,
                                     border: `1px solid ${cyan[300]}`,
                                     ...UtilsStyle.widthFitContent(),
                                     ...UtilsStyle.borderRadius(5)
                                 }}>
                                <Box
                                    display={'flex'}
                                    alignItems={'center'}>
                                    <Box minWidth={75} display={'flex'}>
                                        {isMobile ?
                                            <Phone fontSize={'small'} style={{marginLeft: theme.spacing(0.3)}}/> :
                                            <PhoneIphone fontSize={'small'}
                                                         style={{marginLeft: theme.spacing(0.3)}}/>}
                                        <Typography variant={'body1'} display={'flex'} alignItems={'center'} mr={0.7}>
                                            {isMobile ? 'موبایل' : 'تلفن'}
                                        </Typography>
                                    </Box>
                                    <Typography variant={'h6'} mr={1.5} fontWeight={500}>
                                        {item.value}
                                    </Typography>
                                </Box>
                                <Box display={'flex'} mr={1.5}>
                                    <IconButton size={"small"}
                                                onClick={() => {
                                                    handlePhoneEdit(item, index)
                                                }}
                                                style={{marginLeft: theme.spacing(0.5)}}>
                                        <Edit fontSize={"small"}/>
                                    </IconButton>
                                    <IconButton
                                        size={"small"}
                                        onClick={() => {
                                            handlePhoneRemove(item, index)
                                        }}>
                                        <Delete fontSize={"small"}/>
                                    </IconButton>
                                </Box>
                            </Box>
                        )
                    }}
                />
            </Box>
        </Box>
    )
}

function DetailsText({details, elDetails, onElChange, onChange}) {
    const [data, setData] = useState({})
    const [timer, setTimer] = useState(null)
    useEffect(() => {
        const newData = data;
        _.forEach(sLang, (l, key) => {
            newData[key] = htmlToEditorContent((details[key] && details[key].text) ? details[key].text : '')
        });
        setData(newData);
    }, [])


    function handlerTextChange(content) {
        const newData = data;
        newData[elDetails.activeLanguage.key] = content;
        setData({
            ...newData
        });

        if (timer != null)
            clearTimeout(timer);
        setTimer(setTimeout(() => {
            onChange({
                text: content
            })
        }, 4000))
    }

    return (
        <Editor
            mx={2}
            minHeight={200}
            content={data[elDetails.activeLanguage.key]}
            onContentChange={handlerTextChange}
            style={{
                border: `1px solid ${grey[400]}`,
                overflow: 'auto',
                ...UtilsStyle.borderRadius(5),
            }}/>
    )
}

//endregion Details

//region Properties

const Properties = React.memo(function Properties({properties, elProperties, onElChange, onChange, ...props}) {
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();
    const [state, setState] = useState({
        onEdit: false,
        defIndex: null,
        defId: null,
        defType: null,
        type: 'property',
        defIcon: null,
        icon: 'default',
        defPriority: null,
        priority: 'high',
        defText: null,
        text: '',
        selfValue: true
    });
    const [propertyHigh, setPropertyHigh] = useState([]);
    const [propertyMedium, setPropertyMedium] = useState([]);
    const [propertyLow, setPropertyLow] = useState([]);
    const [usageConditionHigh, setUsageConditionHigh] = useState([]);
    const [usageConditionMedium, setUsageConditionMedium] = useState([]);
    const [usageConditionLow, setUsageConditionLow] = useState([]);


    useEffect(() => {
        initialize()
    }, [elProperties.activeLanguage.key]);

    useEffect(() => {
        const newData = properties;
        newData[elProperties.activeLanguage.key] = {
            property: [
                ...propertyHigh,
                ...propertyMedium,
                ...propertyLow,
            ],
            usageCondition: [
                ...usageConditionHigh,
                ...usageConditionMedium,
                ...usageConditionLow,
            ]
        };
        onChange({
            properties: newData
        })
    }, [propertyHigh, propertyMedium, propertyLow, usageConditionHigh, usageConditionMedium, usageConditionLow])


    function initialize() {
        const newPropertyHigh = [];
        const newPropertyMedium = [];
        const newPropertyLow = [];

        const newUsageConditionHigh = [];
        const newUsageConditionMedium = [];
        const newUsageConditionLow = [];
        if (properties[elProperties.activeLanguage.key]) {
            if (properties[elProperties.activeLanguage.key].property) {
                _.forEach(properties[elProperties.activeLanguage.key].property, (p) => {
                    switch (p.priority) {
                        case  'high': {
                            newPropertyHigh.push(p);
                            break;
                        }
                        case 'medium': {
                            newPropertyMedium.push(p);
                            break;
                        }
                        default: {
                            newUsageConditionLow.push(p);
                        }
                    }
                })
            }
            if (properties[elProperties.activeLanguage.key].usageCondition) {
                _.forEach(properties[elProperties.activeLanguage.key].usageCondition, (p) => {
                    switch (p.priority) {
                        case  'high': {
                            newUsageConditionHigh.push(p);
                            break;
                        }
                        case 'medium': {
                            newUsageConditionMedium.push(p);
                            break;
                        }
                        default: {
                            newPropertyLow.push(p);
                        }
                    }
                })
            }
        }

        setPropertyHigh([...newPropertyHigh]);
        setPropertyMedium([...newPropertyMedium]);
        setPropertyLow([...newPropertyLow]);
        setUsageConditionHigh([...newUsageConditionHigh]);
        setUsageConditionMedium([...newUsageConditionMedium]);
        setUsageConditionLow([...newUsageConditionLow]);
    }

    function resetState() {
        setState({
            ...state,
            onEdit: false,
            defId: null,
            defIndex: null,
            defType: null,
            type: 'property',
            defIcon: null,
            icon: 'default',
            defPriority: null,
            priority: 'high',
            defText: null,
            text: '',
        })
    }

    function itemsChangeHandler({key, items}) {
        switch (key) {
            case 'propertyHigh': {
                setPropertyHigh([...items]);
                break;
            }
            case 'propertyMedium': {
                setPropertyMedium([...items]);
                break;
            }
            case 'propertyLow': {
                setPropertyLow([...items]);
                break;
            }
            case 'usageConditionHigh': {
                setUsageConditionHigh([...items]);
                break;
            }
            case 'usageConditionMedium': {
                setUsageConditionMedium([...items]);
                break;
            }
            case 'usageConditionLow': {
                setUsageConditionLow([...items]);
                break;
            }
            default: {

            }
        }
    }

    function itemSaveHandler() {
        setTimeout(() => {
            if (_.isEmpty(state.text)) {
                enqueueSnackbar('متن ویژگی را کامل کنید.',
                    {
                        variant: "error",
                        action: (key) => (
                            <Fragment>
                                <Button onClick={() => {
                                    closeSnackbar(key)
                                }}>
                                    {lang.get('close')}
                                </Button>
                            </Fragment>
                        )
                    });
                return;
            }
            let data = null;
            let type = null;

            switch (state.type) {
                case 'property': {
                    type = 'property';
                    switch (state.priority) {
                        case 'high': {
                            type = type + 'High';
                            data = propertyHigh;
                            break;
                        }
                        case 'medium': {
                            type = type + 'Medium';
                            data = propertyMedium;
                            break;
                        }
                        case 'low': {
                            type = type + 'Low';
                            data = propertyLow;
                            break;
                        }
                        default: {
                            break;
                        }
                    }
                    break;
                }
                case 'usageCondition': {
                    type = 'usageCondition';
                    switch (state.priority) {
                        case 'high': {
                            type = type + 'High';
                            data = usageConditionHigh;
                            break;
                        }
                        case 'medium': {
                            type = type + 'Medium';
                            data = usageConditionMedium;
                            break;
                        }
                        case 'low': {
                            type = type + 'Low';
                            data = usageConditionLow;
                            break;
                        }
                        default: {
                            break;
                        }
                    }
                    break;
                }
                default: {
                    alert(`type: ${type}`)
                    break;
                }
            }
            if (data === null)
                return;
            if (state.id && type === state.defType + _.startCase(state.defPriority)) {
                data.splice(state.defIndex, 0, convertProperty({
                    id: state.defId,
                    text: state.text,
                    type: state.icon,
                    priority: state.priority
                }));
            } else {
                data.push(convertProperty({
                    id: UtilsData.createId(data),
                    text: state.text,
                    type: state.icon,
                    priority: state.priority
                }))
            }

            resetState();
            itemsChangeHandler({key: type, items: data});
        }, 1000)
    }

    function cancelHandler() {
        setTimeout(() => {
            if (state.defIndex === null) {
                resetState();
                return;
            }
            let data = null;
            let type = null;
            switch (state.defType) {
                case 'property': {
                    type = 'property';
                    switch (state.defPriority) {
                        case 'high': {
                            type = type + 'High';
                            data = propertyHigh;
                            break;
                        }
                        case 'medium': {
                            type = type + 'Medium';
                            data = propertyMedium;
                            break;
                        }
                        case 'low': {
                            type = type + 'Low';
                            data = propertyLow;
                            break;
                        }
                        default: {
                            break;
                        }
                    }
                    break;
                }
                case 'usageCondition': {
                    type = 'usageCondition';
                    switch (state.defPriority) {
                        case 'high': {
                            type = type + 'High';
                            data = usageConditionHigh;
                            break;
                        }
                        case 'medium': {
                            type = type + 'Medium';
                            data = usageConditionMedium;
                            break;
                        }
                        case 'low': {
                            type = type + 'Low';
                            data = usageConditionLow;
                            break;
                        }
                        default: {
                            break;
                        }
                    }
                    break;
                }
                default: {
                    break;
                }
            }

            if (data === null && !_.isArray(data)) {
                resetState()
                return;
            }
            data.splice(state.defIndex, 0, convertProperty({
                id: state.defId,
                text: state.defText,
                type: state.defIcon,
                priority: state.defPriority
            }));

            resetState();
            itemsChangeHandler({key: type, items: data});
        }, 1000);
    }

    function removeItem({key, item}) {
        switch (key) {
            case 'propertyHigh': {
                const newData = propertyHigh;
                _.remove(newData, (d) => {
                    return d.id === item.id
                });
                setPropertyHigh([...newData]);
                break;
            }
            case 'propertyMedium': {
                const newData = propertyMedium;
                _.remove(newData, (d) => {
                    return d.id === item.id
                });
                setPropertyMedium([...newData]);
                break;
            }
            case 'propertyLow': {
                const newData = propertyLow;
                _.remove(newData, (d) => {
                    return d.id === item.id
                });
                setPropertyLow([...newData]);
                break;
            }
            case 'usageConditionHigh': {
                const newData = usageConditionHigh;
                _.remove(newData, (d) => {
                    return d.id === item.id
                });
                setUsageConditionHigh([...newData]);
                break;
            }
            case 'usageConditionMedium': {
                const newData = usageConditionMedium;
                _.remove(newData, (d) => {
                    return d.id === item.id
                });
                setUsageConditionMedium([...newData]);
                break;
            }
            case 'usageConditionLow': {
                const newData = usageConditionLow;
                _.remove(newData, (d) => {
                    return d.id === item.id
                });
                setUsageConditionLow([...newData]);
                break;
            }
            default: {
            }
        }
    }

    function onEditHandler({item, index, type}) {
        const i = item;
        setState({
            ...state,
            onEdit: true,
            defIndex: index,
            defId: i.id,
            id: i.id,
            defType: type,
            type: type,
            defPriority: i.priority,
            priority: i.priority,
            defText: i.text,
            text: i.text,
            selfValue: i.text
        });
        removeItem({key: type + _.startCase(item.priority), item: item})

    }

    const editDisable = (state.onEdit && state.defId !== null);
    return (
        <FormContainer open={elProperties.open} active={elProperties.active}
                       title={lang.get("properties")}
                       activeLanguageValue={properties}
                       activeLanguage={elProperties.activeLanguage}
                       onLanguageChange={(lang) => {
                           onElChange('properties', {activeLanguage: lang})
                       }}
                       onOpenClick={(e, open) => {
                           onElChange('properties', {open: open})
                       }}>
            <Box display={'flex'} px={2} py={1} flexDirection={'column'}>
                <Box mt={1}>
                    {!state.onEdit ?
                        <BaseButton
                            variant={"outlined"}
                            onClick={() => {
                                setState({
                                    ...state,
                                    onEdit: true
                                })
                            }}
                            style={{
                                borderColor: cyan[300]
                            }}>
                            افرودن ویژگی جدید
                        </BaseButton> :
                        <Box display={'flex'} flexDirection={'column'} component={Card} pt={1} pb={2} px={2}>
                            <Typography variant={'subtitle1'} fontWeight={300} mt={1} mb={2}>
                                افزودن ویژگی:
                            </Typography>
                            <Box display={'flex'} flexWrap={"wrap"}>
                                <Box display={'flex'} flexDirection={'column'}>
                                    <Typography variant={'subtitle2'} fontWeight={300}>
                                        نوع ویژگی:
                                    </Typography>
                                    <Select
                                        labelId="select-type"
                                        value={state.type}
                                        onChange={(event) => {
                                            setState({
                                                ...state,
                                                type: event.target.value
                                            })
                                        }}
                                        style={{
                                            marginTop: theme.spacing(1),
                                        }}>
                                        <MenuItem value={'property'}>
                                            {lang.get('properties')}
                                        </MenuItem>
                                        <MenuItem value={'usageCondition'}>
                                            {lang.get('usage_condition')}
                                        </MenuItem>
                                    </Select>
                                </Box>
                                <Box display={'flex'} flexDirection={'column'} mr={3}>
                                    <Typography variant={'subtitle2'} fontWeight={300}>
                                        اولویت:
                                    </Typography>
                                    <Select
                                        labelId="priority-icon"
                                        value={state.priority}
                                        onChange={(event) => {
                                            setState({
                                                ...state,
                                                priority: event.target.value
                                            })
                                        }}
                                        style={{
                                            marginTop: theme.spacing(1),
                                        }}>
                                        <MenuItem value={'high'}>
                                            بالا
                                        </MenuItem>
                                        <MenuItem value={'medium'}>
                                            متوسط
                                        </MenuItem>
                                        <MenuItem value={'low'}>
                                            پایین
                                        </MenuItem>
                                    </Select>
                                </Box>
                                <Box display={'flex'} flexDirection={'column'} mr={3}>
                                    <Typography variant={'subtitle2'} fontWeight={300}>
                                        آیکون:
                                    </Typography>
                                    <Select
                                        labelId="select-icon"
                                        value={state.icon}
                                        onChange={(event) => {
                                            setState({
                                                ...state,
                                                icon: event.target.value
                                            })
                                        }}
                                        style={{
                                            marginTop: theme.spacing(1),
                                        }}>
                                        <MenuItem value={'default'}>
                                            <Gem2
                                                gemColor={state.type !== 'property' ? orange[600] : green[500]}
                                                style={{
                                                    width: 15
                                                }}/>
                                        </MenuItem>
                                        <MenuItem value={'date'}>
                                            <DateRange
                                                style={{color: state.type !== 'property' ? orange[600] : green[500]}}/>
                                        </MenuItem>
                                        <MenuItem value={'phone'}>
                                            <PhoneEnabled
                                                style={{color: state.type !== 'property' ? orange[600] : green[500]}}/>
                                        </MenuItem>
                                    </Select>
                                </Box>
                                <Box display={'flex'} flexDirection={'column'} mr={3}>
                                    <BaseTextField
                                        value={state.text}
                                        label={'متن'}
                                        optionalLabel={''}
                                        onTextChange={(text) => setState({
                                            ...state,
                                            text: text
                                        })}
                                        style={{
                                            minWidth: 300
                                        }}/>
                                </Box>
                                <Box mt={1} mr={2} display={'flex'} flexWrap={'wrap'} alignContent={'flex-end'}>
                                    <BaseButton
                                        onClick={itemSaveHandler}
                                        style={{
                                            backgroundColor: cyan[300],
                                            color: '#fff',
                                        }}>
                                        {lang.get('save')}
                                    </BaseButton>
                                    <BaseButton variant={"text"} size={'small'}
                                                onClick={cancelHandler}
                                                style={{marginRight: theme.spacing(0.7)}}>
                                        {lang.get('cancel')}
                                    </BaseButton>
                                </Box>
                            </Box>
                        </Box>
                    }
                </Box>
                <Box display={'flex'} mt={2}>
                    <Box display={'flex'} flexDirection={'column'} width={0.5} pl={0.5}>
                        <Typography variant={'subtitle1'} mb={0.7}>
                            {lang.get("usage_condition")}
                        </Typography>
                        {!_.isEmpty(usageConditionHigh) &&
                        <Box display={'flex'} flexDirection={'column'} mt={2}>
                            <Typography variant={'subtitle2'} color={orange[600]}>
                                {lang.get("high_priority")}:
                            </Typography>
                            <DraggableList
                                items={usageConditionHigh}
                                onItemsChange={(items) => {
                                    itemsChangeHandler({key: "usageConditionHigh", items: items})
                                }}
                                render={(item, draggableListProps, {index}) => (
                                    <Box {...draggableListProps}>
                                        <PropertiesItem
                                            key={index}
                                            item={item}
                                            disableEdit={editDisable}
                                            onRemove={() => removeItem({key: "usageConditionHigh", item: item})}
                                            onEdit={() => onEditHandler({
                                                item: item,
                                                index: index,
                                                type: 'usageCondition'
                                            })}
                                            isUsageCondition={true} {...props}/>
                                    </Box>
                                )}/>
                        </Box>
                        }
                        {!_.isEmpty(usageConditionMedium) &&
                        <Box display={'flex'} flexDirection={'column'} mt={2}>
                            <Typography variant={'subtitle2'} color={orange[600]}>
                                {lang.get("medium_priority")}:
                            </Typography>

                            <DraggableList
                                items={usageConditionMedium}
                                onItemsChange={(items) => {
                                    itemsChangeHandler({key: "usageConditionMedium", items: items})
                                }}
                                render={(item, draggableListProps, {index}) => (
                                    <Box {...draggableListProps}>
                                        <PropertiesItem
                                            key={index}
                                            item={item}
                                            disableEdit={editDisable}
                                            onRemove={() => removeItem({key: "usageConditionMedium", item: item})}
                                            onEdit={() => onEditHandler({
                                                item: item,
                                                index: index,
                                                type: 'usageCondition'
                                            })}
                                            isUsageCondition={true} {...props}/>
                                    </Box>
                                )}/>
                        </Box>
                        }

                        {!_.isEmpty(usageConditionLow) &&
                        <Box display={'flex'} flexDirection={'column'} mt={2}>
                            <Typography variant={'subtitle2'} color={orange[600]}>
                                {lang.get("low_priority")}:
                            </Typography>
                            <DraggableList
                                items={usageConditionLow}
                                onItemsChange={(items) => {
                                    itemsChangeHandler({key: "usageConditionLow", items: items})
                                }}
                                render={(item, draggableListProps, {index}) => (
                                    <Box {...draggableListProps}>
                                        <PropertiesItem
                                            key={index}
                                            item={item}
                                            disableEdit={editDisable}
                                            onRemove={() => removeItem({key: "usageConditionLow", item: item})}
                                            onEdit={() => onEditHandler({
                                                item: item,
                                                index: index,
                                                type: 'usageCondition'
                                            })}
                                            isUsageCondition={true} {...props}/>
                                    </Box>
                                )}/>
                        </Box>
                        }

                    </Box>
                    <Box display={'flex'} flexDirection={'column'} width={0.5} pr={0.5}>
                        <Typography variant={'subtitle1'} mb={0.7}>
                            {lang.get("properties")}
                        </Typography>
                        {!_.isEmpty(propertyHigh) &&
                        <Box display={'flex'} flexDirection={'column'} mt={2}>
                            <Typography variant={'subtitle2'} color={green[500]}>
                                {lang.get("high_priority")}:
                            </Typography>
                            <DraggableList
                                items={propertyHigh}
                                onItemsChange={(items) => {
                                    itemsChangeHandler({key: "propertyHigh", items: items})
                                }}
                                render={(item, draggableListProps, {index}) => (
                                    <Box {...draggableListProps}>
                                        <PropertiesItem
                                            key={index}
                                            item={item}
                                            disableEdit={editDisable}
                                            onRemove={() => removeItem({key: "propertyHigh", item: item})}
                                            onEdit={() => onEditHandler({
                                                item: item,
                                                index: index,
                                                type: 'property'
                                            })}
                                            isUsageCondition={false} {...props}/>
                                    </Box>
                                )}/>
                        </Box>
                        }
                        {!_.isEmpty(propertyMedium) &&
                        <Box display={'flex'} flexDirection={'column'} mt={2}>
                            <Typography variant={'subtitle2'} color={green[500]}>
                                {lang.get("medium_priority")}:
                            </Typography>
                            <DraggableList
                                items={propertyMedium}
                                onItemsChange={(items) => {
                                    itemsChangeHandler({key: "propertyMedium", items: items})
                                }}
                                render={(item, draggableListProps, {index}) => (
                                    <Box {...draggableListProps}>
                                        <PropertiesItem
                                            key={index}
                                            item={item}
                                            disableEdit={editDisable}
                                            onRemove={() => removeItem({key: "propertyMedium", item: item})}
                                            onEdit={() => onEditHandler({
                                                item: item,
                                                index: index,
                                                type: 'property'
                                            })}
                                            isUsageCondition={false} {...props}/>
                                    </Box>
                                )}/>
                        </Box>
                        }

                        {!_.isEmpty(propertyLow) &&
                        <Box display={'flex'} flexDirection={'column'} mt={2}>
                            <Typography variant={'subtitle2'} color={green[500]}>
                                {lang.get("low_priority")}:
                            </Typography>
                            <DraggableList
                                items={propertyLow}
                                onItemsChange={(items) => {
                                    itemsChangeHandler({key: "propertyLow", items: items})
                                }}
                                render={(item, draggableListProps, {index}) => (
                                    <Box {...draggableListProps}>
                                        <PropertiesItem
                                            key={index}
                                            item={item}
                                            disableEdit={editDisable}
                                            onRemove={() => removeItem({key: "propertyLow", item: item})}
                                            onEdit={() => onEditHandler({
                                                item: item,
                                                index: index,
                                                type: 'property'
                                            })}
                                            isUsageCondition={false} {...props}/>
                                    </Box>
                                )}/>
                        </Box>
                        }
                    </Box>
                </Box>
            </Box>
        </FormContainer>
    )
}, (pp, np) => {
    return !(!_.isEqual(pp.elProperties, np.elProperties) || !_.isEqual(pp.properties, np.properties))
});


function PropertiesItem({item, disableEdit, onEdit, onRemove, isUsageCondition, index, onDragOver, onDragStart, onDragEnd, ...props}) {
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();

    return (
        <Box py={1}>
            <Box
                py={0.7} px={1}
                display={'flex'}
                alignItems={'center'}
                style={{
                    border: `1px solid ${isUsageCondition ? orange[600] : green[500]}`,
                    ...UtilsStyle.borderRadius(5),
                    ...UtilsStyle.transition(),
                }}
                {...props}>
                <Box display={'flex'}>
                    <Tooltip title={lang.get("edit")}>
                        <IconButton size={'small'} onClick={(e) => {
                            if (disableEdit) {
                                enqueueSnackbar('یک ویژگی در حال ادیت میاشد.',
                                    {
                                        variant: "error",
                                        action: (key) => (
                                            <Fragment>
                                                <Button onClick={() => {
                                                    closeSnackbar(key)
                                                }}>
                                                    {lang.get('close')}
                                                </Button>
                                            </Fragment>
                                        )
                                    });
                                return
                            }
                            onEdit(e)
                        }}
                                    style={{marginLeft: theme.spacing(0.5)}}>
                            <Edit fontSize={"small"} style={{color: cyan[200]}}/>
                        </IconButton>
                    </Tooltip>
                    <Tooltip title={lang.get("delete")}>
                        <IconButton size={'small'} onClick={onRemove}>
                            <Delete fontSize={"small"} style={{color: red[200]}}/>
                        </IconButton>
                    </Tooltip>
                </Box>
                <PropertiesIcon isUsageCondition={isUsageCondition} type={item.type}/>
                <Typography
                    variant={!item.isLow ? 'subtitle1' : 'subtitle2'}
                    fontWeight={item.isHigh ? '400' : item.isMedium ? '300' : '200'}>
                    {item.text}
                </Typography>
            </Box>
        </Box>
    )
}


function PropertiesIcon({isUsageCondition, type, ...props}) {
    const color = isUsageCondition ? orange[600] : green[500];
    return (
        <Box width={35} height={35} display={'flex'}
             justifyContent={'center'}
             alignItems={'center'}>
            {
                type === 'date' ?
                    <DateRange style={{color: color}}/>
                    :
                    type === 'phone' ?
                        <PhoneEnabled style={{color: color}}/>
                        :
                        <Gem2
                            gemColor={color}
                            style={{
                                width: 15
                            }}
                            {...props}/>
            }
        </Box>
    )
}

//endregion Properties

//region Tags

const useTagsStyle = makeStyles(theme => ({
    root: {
        backgroundColor: cyan[100],
        color: grey[700],
        '&:focus': {
            backgroundColor: cyan[100],
            color: grey[700],
        },
    }
}));

const Tags = React.memo(function Tags({tags, elTags, onElChange, onChange, ...props}) {

    const {enqueueSnackbar, closeSnackbar} = useSnackbar();
    const [setting, setSetting] = useState({
        openAddTagDialog: false,
        dialogValue: '',
        dialogPermalink: '',
    });
    const [orgData, setOrgData] = useState([]);
    const [data, setData] = useState([]);
    const [value, setValue] = useState("");
    const [timer, setTimer] = useState(null);
    const classes = useTagsStyle(props);

    useEffect(() => {
        getData('');
    }, []);

    useEffect(() => {
        checkFilter(data);
    }, [tags]);


    function valueHandler(val) {
        setValue(val);
        if (timer != null)
            clearTimeout(timer);
        setTimer(setTimeout(() => {
            if (_.isEmpty(val)) {
                setData([...orgData]);
                return;
            }
            const newList = [];
            _.forEach(orgData, (t) => {
                if (t.name.fa.includes(val))
                    newList.push(t)
            });
            setData([...newList]);
        }, 500))
    }

    function getData(value) {
        ControllerProduct.Product.Tags.get({
            contain: value,
            page: 1,
            step: 100,
        }).then((res) => {
            setTimer(null);
            checkFilter(res.data)
        }).catch(() => {
            setData(false)
        })
    }

    function handleAdd(d) {
        onChange([...tags, d])
    }

    function handleDelete(d) {
        const newData = tags;
        _.remove(newData, (t) => {
            return t.id === d.id
        });
        onChange([...newData])
    }

    function checkFilter(data) {
        const newList = [];
        _.forEach(data, (d) => {
            let a = false;
            _.forEach(tags, (t) => {
                if (d.id === t.id) {
                    newList.push({
                        ...d,
                        hasActive: true
                    });
                    a = true
                }
            });
            if (!a) {
                newList.push({
                    ...d,
                    hasActive: false
                })
            }
        });
        if (_.isEmpty(value))
            setOrgData([...newList]);
        setData([...newList]);
    }

    function handleAddNewTag() {
        if (!_.isEmpty(data)) {
            const i = _.findIndex(data, (t) => {
                return t.name.fa === value
            });
            if (i !== -1) {
                enqueueSnackbar('این تگ موجود میباشد.',
                    {
                        variant: "error",
                        action: (key) => (
                            <Fragment>
                                <Button onClick={() => {
                                    closeSnackbar(key)
                                }}>
                                    {lang.get('close')}
                                </Button>
                            </Fragment>
                        )
                    });
                return;
            }
        }


        setSetting({
            ...setting,
            openAddTagDialog: true,
            dialogPermalink: '',
            dialogValue: value
        })

        // enqueueSnackbar('فزوده شد.',
        //     {
        //         variant: "success",
        //         action: (key) => (
        //             <Fragment>
        //                 <Button onClick={() => {
        //                     closeSnackbar(key)
        //                 }}>
        //                     {lang.get('close')}
        //                 </Button>
        //             </Fragment>
        //         )
        //     });
    }

    function handleCloseDialog(save) {

        if (save) {
            if (!setting.dialogPermalink || !setting.dialogValue) {
                enqueueSnackbar('تمام فیلدها را پرکنید.',
                    {
                        variant: "error",
                        action: (key) => (
                            <Fragment>
                                <Button onClick={() => {
                                    closeSnackbar(key)
                                }}>
                                    {lang.get('close')}
                                </Button>
                            </Fragment>
                        )
                    });
                return;
            }
            ControllerProduct.Product.Tags.set({
                permalink: setting.dialogPermalink,
                name: createMultiLanguage({fa: setting.dialogValue})
            }).then(res => {
                res.item.hasActive = true;
                setOrgData([res.item, ...orgData]);
                setData([res.item, ...data]);
                handleAdd(res.item);
                enqueueSnackbar('افزوده شد.',
                    {
                        variant: "success",
                        action: (key) => (
                            <Fragment>
                                <Button onClick={() => {
                                    closeSnackbar(key)
                                }}>
                                    {lang.get('close')}
                                </Button>
                            </Fragment>
                        )
                    });
                handleCloseDialog(false)
            });
            return;
        }


        setSetting({
            ...setting,
            openAddTagDialog: false,
            dialogPermalink: ''
        })
    }

    return (
        <React.Fragment>
            <FormContainer open={elTags.open}
                           title={lang.get("tags")}
                           onLanguageChange={(lang) => {
                               onElChange('tags', {activeLanguage: lang})
                           }}
                           onOpenClick={(e, open) => {
                               onElChange('tags', {open: open})
                           }}>
                <Box display={'flex'} px={2} py={1} flexDirection={'column'}>
                    {data !== false ?
                        <Box display={'flex'} justifyContent={'center'}>
                            <Box width={0.7} mt={2} mb={2}>
                                <Box display={'flex'} flexDirection={'column'} mb={3}>
                                    <Typography variant={'body1'} pb={1} fontWeight={400}>
                                        تگ‌های فعال:
                                    </Typography>
                                    <Box display={'flex'} flexWrap={'wrap'}>
                                        {
                                            tags.map((t, i) => (
                                                <Box key={i} ml={2} mb={1.5}>
                                                    <Chip
                                                        className={classes.root}
                                                        label={t.name.fa}
                                                        onDelete={() => handleDelete(t)}
                                                        style={{}}/>
                                                </Box>
                                            ))
                                        }
                                    </Box>
                                </Box>
                                <Box display={'flex'} width={1} alignItems={'center'} justifyContent={'center'}>
                                    <BaseTextField
                                        variant={"outline"}
                                        label={'جست‌و‌جو تگ'}
                                        optionalLabel={''}
                                        value={value}
                                        onTextChange={valueHandler}
                                        style={{
                                            flex: 1,
                                            paddingLeft: theme.spacing(1.5)
                                        }}/>
                                    <Box display={'flex'}>
                                        <Tooltip title={'بروزرسانی'}>
                                            <IconButton
                                                onClick={() => {
                                                    getData(value)
                                                }}
                                                style={{
                                                    marginLeft: theme.spacing(0.5)
                                                }}>
                                                <Sync/>
                                            </IconButton>
                                        </Tooltip>
                                        <BaseButton
                                            disabled={value.length < 2}
                                            size={"medium"}
                                            style={{
                                                backgroundColor: cyan[300],
                                                color: '#fff'
                                            }}
                                            onClick={handleAddNewTag}>
                                            {lang.get("add")}
                                        </BaseButton>
                                    </Box>
                                </Box>
                                <Box mt={2} component={Card}>
                                    <Collapse in={!_.isEmpty(data)}>
                                        <Box display={'flex'} flexWrap={'wrap'} py={2} px={2}>
                                            {
                                                !_.isEmpty(data) && data.map((d, i) => (
                                                    <Box key={i} ml={2} mb={1.5}>
                                                        <Chip
                                                            label={d.name.fa}
                                                            disabled={d.hasActive}
                                                            onClick={() => handleAdd(d)}/>
                                                    </Box>
                                                ))
                                            }
                                        </Box>
                                    </Collapse>
                                </Box>
                            </Box>
                        </Box> :
                        <ComponentError tryAgainFun={() => getData(value)}/>
                    }
                </Box>
            </FormContainer>
            <Dialog
                open={setting.openAddTagDialog}
                TransitionComponent={Transition}
                keepMounted
                onClose={() => {
                }}
                aria-labelledby="add-tag-dialog-slide-title"
                aria-describedby="add-tag-dialog-slide-description">
                <Box py={3} px={2} display={'flex'} flexDirection={'column'} minWidth={400}>
                    <Typography variant={'h6'} fontWeight={600} pb={1.5}>
                        افزودن تگ جدید
                    </Typography>
                    <Box display={'flex'} flexDirection={'column'}>
                        <Box pb={2} width={1}>
                            <BaseTextField
                                isRequired={true}
                                label={lang.get("permalink")}
                                value={setting.dialogPermalink}
                                dir={'ltr'}
                                pattern={'^[A-Za-z\u0591-\u07FF\uFB1D-\uFDFD\uFE70-\uFEFC][A-Za-z0-9-\u0591-\u07FF\uFB1D-\uFDFD\uFE70-\uFEFC]*$'}
                                onTextChange={(v) => setSetting({
                                    ...setting,
                                    dialogPermalink: v,
                                })}
                                style={{
                                    width: '100%'
                                }}/>
                        </Box>
                        <BaseTextField
                            isRequired={true}
                            label={'نام تگ'}
                            value={setting.dialogValue}
                            onTextChange={(v) => setSetting({
                                ...setting,
                                dialogValue: v
                            })}/>
                        <Box pt={2} display={'flex'}>
                            <SuccessButton onClick={() => handleCloseDialog(true)}>
                                {lang.get("add")}
                            </SuccessButton>
                            <Box pr={1}>
                                <BaseButton variant={"text"} onClick={() => handleCloseDialog(false)}>
                                    {lang.get("close")}
                                </BaseButton>
                            </Box>
                        </Box>
                    </Box>
                </Box>
            </Dialog>
        </React.Fragment>
    )
}, (pp, np) => {
    return !(!_.isEqual(pp.tags, np.tags) || !_.isEqual(pp.elTags, np.elTags))
});

//endregion Tags

//region HelperComponents

//region FormContainer
export function FormContainer({title, active = true, open, activeLanguageValue, activeLanguage, onLanguageChange, onOpenClick, ...props}) {
    return (
        <Collapse in={active} style={{width: '100%'}}>
            <BoxWithTopBorder component={Card} open={open} display={'flex'}
                              activeLanguageValue={activeLanguageValue}
                              activeLanguage={activeLanguage}
                              title={title} mt={2}
                              width={1}
                              onLanguageChange={onLanguageChange}
                              onOpenClick={onOpenClick}
                              {...props}>
                {
                    active ?
                        <Collapse in={open}>
                            {open && props.children}
                        </Collapse> : null
                }
            </BoxWithTopBorder>
        </Collapse>
    )
}

//endregion FormContainer

//region ItemContainer
function ItemContainer(props) {
    return (
        <Box display='flex'
             my={1}
             width={['100%']}
             {...props}>
            {props.children}
        </Box>
    )
}

//endregion ItemContainer

//region TemplateItem
function TemplateContainer({title = 'متن های آماده', ...props}) {
    return (
        <Box display={'flex'} flexDirection={'column'} mt={2}>
            <Typography variant={'body2'} color={cyan[600]}>
                {title}:
            </Typography>
            <Box display={'flex'} flexWrap={'wrap'}>
                {props.children}
            </Box>
        </Box>
    )
}

function TemplateItem({title, data, onClick, ...props}) {
    return (
        <BaseButton variant={"outlined"}
                    size={'small'}
                    onClick={() => onClick(data !== undefined ? data : title)}
                    style={{
                        borderColor: cyan[200],
                        color: grey[700],
                        marginTop: theme.spacing(0.5),
                        marginLeft: theme.spacing(0.5)
                    }}>
            {title}
        </BaseButton>
    )
}

//endregion TemplateItem

//endregion HelperComponents

//region SaveDialog
const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

function SaveDialog({open, onChange}) {
    const [state, setState] = useState({
        disable: false
    });

    useEffect(() => {
        setState({
            ...state,
            disable: false
        })
    }, [open]);
    const handleClose = (save) => {
        setState({
            ...state,
            disable: true
        });
        onChange(save)
    };

    return (
        <Dialog
            open={open}
            TransitionComponent={Transition}
            keepMounted
            onClose={() => handleClose(false)}
            aria-labelledby="save-dialog-slide-title"
            aria-describedby="save-dialog-slide-description">
            <Box display={'flex'} flexDirection={'column'} px={3} py={2}>
                <Typography variant={'h6'} fontWeight={400} pb={2}>
                    ذخیره محصول
                </Typography>
                <Typography variant={'body1'}>
                    آیا از ذخیره این محصول اطمینان دارید؟
                </Typography>
                <Typography variant={'caption'} color={grey[700]} pt={0.5}>
                                <span style={{color: orange[300], fontWeight: 600, paddingLeft: theme.spacing(0.3)}}>
                                    توجه:
                                </span>
                    در صورت ذخیره، اطلاعات تغییریافته قابل بازگشت نمیباشد.
                </Typography>

            </Box>
            <DialogActions>
                <BaseButton
                    variant={"text"}
                    size={"small"}
                    disabled={state.disable}
                    onClick={() => handleClose(false)}
                    style={{
                        color: red[300],
                        marginLeft: theme.spacing(0.5),
                        marginRight: theme.spacing(0.5),
                    }}>
                    لغو
                </BaseButton>
                <BaseButton
                    disabled={state.disable}
                    onClick={() => handleClose(true)} color="primary"
                    style={{
                        backgroundColor: colors.success.main,
                        color: '#fff'
                    }}>
                    ذخیره
                </BaseButton>
            </DialogActions>
        </Dialog>
    );
}

//endregion SaveDialog

//endregion Components
