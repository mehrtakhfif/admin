import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { useSelector } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import { Box, Button } from "@material-ui/core";
import Name from "../../components/NewProduct/Name";
import { gcLog, getSafe } from "../../utils/ObjectUtils";
import Media from "../../components/NewProduct/Media";
import Request from "../../components/NewProduct/Request";
import CreateProduct from "../../components/NewProduct/CreateProduct";
import { siteLang } from "../../repository";
import UsageConditions from "../../components/NewProduct/UsageConditions";
import Features from "../../components/NewProduct/Features";
import TagAndBrand from "../../components/NewProduct/TagAndBrand";
import Address from "../../components/NewProduct/Address";
import StorageOrPrice from "../../components/NewProduct/StorageOrPrice.js"

function TabPanel(props) {
  const { children, value, index, ...other } = props;
  const style = {
    padding: "10px",
    width: "100%",
    height: "100%",
    margin: "8px",
  };

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      style={style}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...other}
    >
      {value === index && children}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `vertical-tab-${index}`,
    "aria-controls": `vertical-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
    display: "flex",
    width: "100%",
    height: "100%",
    padding: "16px",
    margin: "8px",
  },
  tabs: {
    borderRight: `1px solid ${theme.palette.divider}`,
  },
  langContainer: { right: 8 },
  langButton: {
    padding: "0px",
    margin: "0px",
    fontWeight: 400,
  },
  langButtonActive: {
    padding: "0px",
    color: theme.palette.primary.main,
    margin: "0px",
    fontWeight: 400,
  },
}));

function ProductsNew({ ...props }) {
  // const whyDidYouRender = true
  const classes = useStyles();
  const [value, setValue] = useState(0);
  const pId = props?.match?.params?.pId;
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };


  if (pId === undefined)
    return (
      <Box
        display={"flex"}
        mt={"2%"}
        alignItems={"center"}
        justifyContent={"center"}
        width={1}
      >
        <CreateProduct />
      </Box>
    );

  return (
    <Box position={"relative"}>
      <Box
        position={"absolute"}
        alignItems={"center"}
        className={classes.langContainer}
        display={"flex"}
        flexDirection={"row"}
        p={1}
      >
        <Typography>زبان:</Typography>
        <Button
          size={"small"}
          className={
            siteLang === "en" ? classes.langButtonActive : classes.langButton
          }
        >
          En
        </Button>
        <Button
          size={"small"}
          className={
            siteLang === "fa" ? classes.langButtonActive : classes.langButton
          }
        >
          fa
        </Button>
      </Box>
      <Box display={"flex"} height={1}>
        <Tabs
          orientation="vertical"
          variant="scrollable"
          value={value}
          onChange={handleChange}
          aria-label="Vertical tabs example"
          className={classes.tabs}
        >
          <Tab label="اطلاعات کلی" {...a11yProps(0)} />
          <Tab label="عکس و مدیا" {...a11yProps(1)} />
          <Tab label="تگ و برند" {...a11yProps(2)} />
          <Tab label="توضیحات" {...a11yProps(3)} />
          <Tab label="ویژگی ها" {...a11yProps(4)} />
          <Tab label="اطلاعات تماس" {...a11yProps(5)} />
          <Tab label="انبار" {...a11yProps(6)} />
        </Tabs>
        <TabPanel value={value} index={0}>
          <Request
            id={pId}
            language={siteLang}
            onlyFields={[
              "name",
              "available",
              "category",
              "disable",
              "type",
              "categories",
              "short_description",
              "description",
              "permalink",
              "settings",
              "description",
            ]}
          >
            <Name />
          </Request>
        </TabPanel>
        <TabPanel value={value} index={1}>
          <Request
            id={pId}
            language={siteLang}
            onlyFields={["media", "thumbnail", "category"]}
          >
            <Media />
          </Request>
        </TabPanel>
        <TabPanel value={value} index={2}>
          <Request
            id={pId}
            language={siteLang}
            onlyFields={["category", "tags", "tag_groups", "brand"]}
          >
            <TagAndBrand />
          </Request>
        </TabPanel>
        <TabPanel value={value} index={3}>
          <Request
            id={pId}
            language={siteLang}
            onlyFields={["category", "description"]}
          >
            <UsageConditions />
          </Request>
        </TabPanel>
        <TabPanel value={value} index={4}>
                    <Request id={pId} language={siteLang}
                             onlyFields={[
                                 'category',
                                 'features',
                                 'categories'
                             ]}>
                        <Features/>
                    </Request>
                </TabPanel>
                <TabPanel value={value} index={5}>
          <Request
            id={pId}
            language={siteLang}
            onlyFields={[
              "category",
              "type",
              "cities",
              "location",
              "short_address",
              "address",
              "details",
            ]}
          >
            <Address />
          </Request>
        </TabPanel>
        <TabPanel index={6} value={value}>
            <StorageOrPrice productId={pId}/>
        </TabPanel>
      </Box>
    </Box>
  );
}

export default React.memo(ProductsNew);
