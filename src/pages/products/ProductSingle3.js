import React, {Fragment, useCallback, useEffect, useMemo, useRef, useState} from "react";
import Box from "@material-ui/core/Box";
import Skeleton from "@material-ui/lab/Skeleton";
import ControllerProduct from "../../controller/ControllerProduct";
import FormControl from "../../components/base/formController/FormController";
import StickyBox from "react-sticky-box";
import Typography from "../../components/base/Typography";
import {activeLang, lang, siteLang, sLang, webRout} from "../../repository";
import rout, {siteRout} from "../../router";
import {blue, cyan, deepOrange, green, grey, orange, red} from "@material-ui/core/colors";
import BaseButton from "../../components/base/button/BaseButton";
import {UtilsData, UtilsRouter, UtilsStyle} from "../../utils/Utils";
import _ from 'lodash'
import {useSnackbar} from "notistack";
import Button from "@material-ui/core/Button";
import Editor, {editorContentToHtml, htmlToEditorContent} from "../../components/base/editor/Editor";
import {Card, makeStyles} from "@material-ui/core";
import BoxWithTopBorder from "../../components/base/textHeader/BoxWithTopBorder";
import MaterialFormControl from "@material-ui/core/FormControl";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Switch from "@material-ui/core/Switch";
import Collapse from "@material-ui/core/Collapse";
import {
    Add,
    CancelOutlined,
    Close,
    DateRange,
    Delete,
    DeleteOutlineOutlined,
    Edit,
    ExpandLess,
    ExpandMore,
    Map,
    Menu,
    MenuOpen,
    Phone,
    PhoneEnabled,
    PhoneIphone,
    Save,
    Sync,
    VisibilityOffOutlined,
    VisibilityOutlined
} from "@material-ui/icons";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import IconButton from "@material-ui/core/IconButton";
import UploadItem from "../../components/base/uploader/UploadItem";
import ControllerSite from '../../controller/ControllerSite'
import {
    convertDetails,
    convertIconUrl,
    convertListProperties,
    convertProperty,
    createMultiLanguage,
    states,
    toServer
} from '../../controller/converter'
import {
    productType,
    serverFileTypes,} from '../../controller/type'
import {Map as LeafletMap, Marker, TileLayer} from "react-leaflet";
import L from "leaflet";
import icon from "../../drawable/mapImage/marker-icon.png";
import MapImage from "../../drawable/image/map.jpg"
import Dialog from "@material-ui/core/Dialog";
import AddAddressMapComponent from '../../components/addAddress/Map'
import PropTypes from "prop-types";
import DraggableList from "../../components/base/draggableList/DraggableList";
import Gem2 from "../../components/base/icon/Gem2";
import Tooltip from "@material-ui/core/Tooltip";
import AutoFill from "../../components/base/autoFill/AutoFill";
import Slide from "@material-ui/core/Slide";
import Backdrop from "@material-ui/core/Backdrop";
import CircularProgress from "@material-ui/core/CircularProgress";
import ComponentError from "../../components/base/ComponentError";
import Chip from "@material-ui/core/Chip";
import {Prompt, Redirect} from "react-router-dom";
import RadioGroup from "@material-ui/core/RadioGroup";
import Radio from "@material-ui/core/Radio";
import Img from "../../components/base/img/Img";
import BaseLink from "../../components/base/link/Link";
import useSWR, {mutate} from "swr";
import SelectBox from "../../components/SelectBox";
import Divider from "@material-ui/core/Divider";
import ButtonBase from "@material-ui/core/ButtonBase";
import {EditBrand} from "../brands/Brands";
import CategorySingle, {getActiveParents} from "../categories/CategorySingle";
import useTheme from "@material-ui/core/styles/useTheme";
import Checkbox from "@material-ui/core/Checkbox";
import LocalStorageUtils from "../../utils/LocalStorageUtils";
import CacheFindDialog from "../../components/dialog/CacheFindDialog";
import BaseSaveDialog from "../../components/dialog/SaveDialog";
import TextField from "../../components/base/textField/TextField";
import TextFieldMultiLanguageContainer, {
    createInputProps,
    createName
} from "../../components/base/textField/TextFieldMultiLanguageContainer";
import TextFieldContainer, {errorList} from "../../components/base/textField/TextFieldContainer";
import FormContainer from "../../components/base/FormContainer";
import TagSingle from "../tags/TagSingle";
import FormLabel from "@material-ui/core/FormLabel";
import NoneTextField from "../../components/base/textField/NoneTextField";
import CreateProperty from "../box/themplate/property/CreateProperty";
import SelectProperty from "../box/themplate/property/SelectProperty";
import PermalinkTextField from "../../components/base/textField/PermalinkTextField";
import Icon from "../../components/icon/Icon";
import {gcError} from "../../utils/ObjectUtils";
import SelectLocation from "../../components/addAddress/SelectLocation";
import PleaseWait from "../../components/base/loading/PleaseWait";
import MultiTextField from "../../components/base/textField/MultiTextField";
import SelectTag from "../box/themplate/tag/SelectTag";
import {maxShowableTagCount} from "../box/themplate/tag/CreateTag";

export default function ({...props}) {
    const theme = useTheme();

    //region var
    const ref = useRef();
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();
    const pId = props.match.params.pId;

    //region state
    const [state, setState] = useState({
        data: !pId,
        refreshCache: 0,
        checkRedirect: true
    });
    //region data
    const [box, setBox] = useState();
    const [name, setName] = useState({val: createMultiLanguage(), el: {}});
    const [permalink, setPermalink] = useState({val: "", el: {}});
    const [type, setType] = useState({
        val: {
            value: ""
        }, el: {}
    });
    const [disable, setDisable] = useState({val: false, el: {}});
    const [media, setMedia] = useState({
        val: [], el: {
            open: false,
        }
    });
    const [description, setDescription] = useState({
        val: createMultiLanguage(),
        el: {
            open: false,
            activeLanguage: sLang[siteLang],
        }
    });
    const [shortDescription, setShortDescription] = useState({
        val: createMultiLanguage(),
        el: {
            open: false,
            activeLanguage: sLang[siteLang],
        }
    });
    const [city, setCity] = useState({
        val: {},
        el: {
            active: false,
            open: false,
        }
    });
    const [address, setAddress] = useState({
        val: {
            address: createMultiLanguage(),
            shortAddress: createMultiLanguage()
        },
        el: {
            active: false,
            open: false,
            activeLanguage: sLang[siteLang],
        }
    });
    const [location, setLocation] = useState({
        val: null,
        el: {
            active: false,
            open: false,
            mapOpen: false,
            showMap: false,
            label: 'map'
        }
    });
    const [details, setDetails] = useState({
        val: convertDetails(),
        el: {
            active: false,
            open: false,
            activeLanguage: sLang[siteLang],
        }
    });
    const [properties, setProperties] = useState({
        val: convertListProperties(),
        el: {
            active: false,
            open: false,
            activeLanguage: sLang[siteLang],
        }
    });

    const [productsSettings, setProductsSettings] = useState({});

    const [tags, setTags] = useState({
        val: [],
        el: {
            open: false,
        }
    });
    const [brand, setBrand] = useState({
        val: null,
        el: {
            active: false,
            open: false,

        }
    });
    const [categories, setCategories] = useState({
        val: [],
        el: {open: false}
    });
    const [thumbnail, setThumbnail] = useState({
        val: null,
        el: {open: true}
    });
    const [panelFeatures, setPanelFeatures] = useState({
        val: null,
        el: {open: true}
    });

    //endregion data
    const [setting, setSetting] = useState({
        saveDialog: false,
        loading: false,
        error: false,
        redirect: false,
        cacheFind: false,
    });
    const [cacheTimer, setCacheTimer] = useState(null);
    //endregion state

    //endregion var

    //region useEffect
    useEffect(() => {
        if (pId) {
            getProduct();
            return
        }
        if (LocalStorageUtils.get(getCacheKey())) {
            setSetting({...setting, cacheFind: true})
        }
    }, []);

    useEffect(() => {
        if (setting.loading)
            saveProduct();
    }, [setting.loading]);

    useEffect(() => {
        if (!box || !state.data || state.cacheFind)
            return;
        clearTimeout(cacheTimer);

        setCacheTimer(setTimeout(() => {

            const props = {
                box, name, permalink, type, media, description, shortDescription,
                city, address, location, details, properties, tags, brand, categories, thumbnail
            };
            try {
                const form = ref.current.serialize();
                if (form.name) {
                    props.name.val = form.name;
                }
                if (form.permalink) {
                    props.permalink.val = form.permalink;
                }
                if (form.address) {
                    props.address.val.address = form.address
                }
                if (form.shortAddress) {
                    props.address.val.shortAddress = form.shortAddress
                }
                if (form.details) {
                    _.forEach(sLang, (l, key) => {
                        if (form.details.servingDays[key]) {
                            props.details.val[key].servingDays = form.details.servingDays[key];
                        }
                        if (form.details.servingHours) {
                            props.details.val[key].servingHours = form.details.servingHours[key];
                        }
                    });
                }

            } catch (e) {
            }
            LocalStorageUtils.set(getCacheKey(), props)
        }, 3000))

    }, [state.refreshCache, box, name, permalink, type, media, description, shortDescription,
        city, address, location, details, properties, tags, brand, categories, thumbnail]);

    const getCacheKey = () => {
        return `productSingle-${pId ? pId : -1}`
    };


    //endregion useEffect

    //region requests
    function getProduct() {
        ControllerProduct.Product.get({pId: pId}).then((res) => {

                const data = res.data;
                setBox(data.box);
                handleName({val: data.name});
                handlePermalink({val: data.permalink});
                handleType({val: data.type});
                handleMedia({val: data.media});
                setDisable({...disable, val: data.disable})

                handleDescription({val: data.description});
                handleShortDescription({val: data.shortDescription});
                handleCity({
                    val: data.city,
                    el: {
                        ...city.el,
                        active: Boolean(data.city)
                    }
                });
                handleAddress({
                    val: {
                        address: data.address,
                        shortAddress: data.shortAddress,
                    },
                    el: {
                        ...address.el,
                        active: Boolean(!_.isEmpty(data.address) || !_.isEmpty(data.shortAddress))
                    }
                });
                handleLocation({
                    val: data.location,
                    el: {
                        ...location.el,
                        active: !_.isEmpty(data.location)
                    }
                });


                handleDetails({
                    val: data.details,
                    el: {
                        ...details.el,
                        active: !data.details.isEmpty(data.details),
                    }
                });
                handleProperties({
                    val: data.properties,
                    el: {
                        ...properties.el,
                        active: !data.properties.isEmpty(data.properties)
                    }
                });
                handleTags({val: data.tags});
                handleBrand({
                    val: data.brand,
                    el: {
                        ...brand.el,
                        active: !_.isEmpty(data.brand)
                    }
                });
                handleCategories({val: data.categories});
                handleThumbnail({val: data.thumbnail});

                setProductsSettings(data.settings);

                const se = {
                    ...setting,
                    error: false,
                }
                if (LocalStorageUtils.get(getCacheKey())) {
                    se.error = false
                }
                setSetting({
                    ...se
                })


                setState({
                    ...state,
                    data: {
                        storageLink: res.data.storageLink
                    }
                });
            }
        ).catch(() => {
            setSetting({
                ...setting,
                error: true
            })
        })
    }

    function getSaveProps() {
        const formError = ref.current.hasError();
        if (formError) {
            reqCancel(`فیلد ${formError} دارای مشکل است.`);
            return
        }

        const formData = ref.current.serialize();
        const isPackageItem = type.val.value !== productType.package_item.type;

        if (_.isEmpty(formData.permalink)) {
            reqCancel('پرمالینک را درست وارد کنید.');
            return;
        }
        if (_.isEmpty(type.val) || type.val.value === "") {
            reqCancel('نوع محصول را مشخص کنید.');
            return;
        }
        if (_.isEmpty(formData.name)) {
            reqCancel('نام محصول را پرکنید.');
            return;
        }

        // if (_.isEmpty(categories.val) && !isPackageItem) {
        //     reqCancel('محصول دارای دسته نمیباشد.');
        //     return;
        // }


        // if (_.isEmpty(description.val.fa) && !isPackageItem) {
        //     reqCancel('توضیحات را وارد کنید.');
        //     return;
        // }


        // if (_.isEmpty(description.val.fa) && !isPackageItem) {
        //     reqCancel('توضیحات کوتاه را وارد کنید.');
        //     return;
        // }


        // if ((_.isEmpty(tags.val) || tags.val.length < 3) && !isPackageItem) {
        //     reqCancel('محصول باید دارای حداقل 3 تگ باشد.');
        //     return;
        // }


        const newCategory = [];
        try {
            _.forEach(categories.val, (cat) => {
                newCategory.push(cat.id);
            });
        } catch (e) {
            gcError("getSaveProps::newCategory", e)
        }

        const newMedia = [];
        try {
            _.forEach(media.val, (m) => {
                newMedia.push(m.id);
            });
        } catch (e) {
            gcError("getSaveProps::newMedia", e)
        }

        const newTags = [];
        try {
            _.forEach(tags.val, (t) => {
                newTags.push({tag_id: t.id, show: Boolean(t.show)})
            });
        } catch (e) {
            gcError("getSaveProps::newTags", e)
        }


        const pr = {
            id: pId,
            boxId: box.id,
            permalink: formData.permalink,
            name: formData.name,
            categories: newCategory,
            description: description.val,
            shortDescription: shortDescription.val,
            city: null,
            address: {},
            shortAddress: {},
            location: undefined,
            details: {},
            properties: {},
            tags: newTags,
            thumbnail: thumbnail.val ? thumbnail.val.id : undefined,
            media: newMedia,
            brandId: null,
            settings: {
                ...formData.settings,
                adminNote: formData.settings.adminNote,
                productStatus: _.toInteger(formData.settings.productStatus)
            }
        };


        if (!pId) {
            pr.type = type.val.value
        }

        if (brand.el.active) {
            if (_.isEmpty(brand.val)) {
                reqCancel('برند باید انتخاب شود.');
                handleBrand({el: {...brand.el, open: true}});
                return
            }

            pr.brandId = brand.val.id;
        }

        if (city.el.active) {
            if (_.isEmpty(city.val)) {
                reqCancel('شهر باید انتخاب شود.');
                handleCity({el: {...city.el, open: true}});
                return
            }
            pr.city = city.val.id;
        }

        if (address.el.active) {
            if (_.isEmpty(formData.address) || _.isEmpty(formData.shortAddress)) {
                reqCancel('آدرس را درست وارد کنید.');
                handleAddress({el: {...address.el, open: true}});
                return;
            }
            pr.address = formData.address;
            pr.shortAddress = formData.shortAddress;
        }

        if (location.el.active) {
            if (_.isEmpty(location.val)) {
                reqCancel('آدرس نقشه را درست وارد کنید.');
                handleLocation({el: {...location.el, open: true}});
                return;
            }
            pr.location = location.val;
        }

        if (details.el.active) {

            const param = {
                phones: {},
                text: {}
            };
            _.forEach(sLang, (l, key) => {
                if (!details.val[key])
                    return;
                param.phones[key] = details.val[key].phones;
                param.text[key] = details.val[key].text;
            });

            pr.details = toServer.details({
                servingHours: formData.details.servingHours,
                servingDays: formData.details.servingDays,
                phones: param.phones,
                text: param.text,
            });
        }

        if (properties.el.active) {
            if (_.isEmpty(properties.val)) {
                reqCancel('جزئیات را درست وارد کنید.');
                handleProperties({el: {...properties.el, open: true}});
                return;
            }
            pr.properties = properties.val.toServer(properties.val);
        }

        return pr;
    }

    function saveProduct() {
        const pr = getSaveProps();
        if (!pr)
            return;

        setSetting({
            ...setting,
            loading: false
        });

        ControllerProduct.Product.save(pr).then((res) => {
            enqueueSnackbar(pId ? 'محصول بروز شد.' : 'محصول ذخیره شد.',
                {
                    variant: "success",
                    action: (key) => (
                        <Fragment>
                            <Button onClick={() => {
                                closeSnackbar(key)
                            }}>
                                {lang.get('close')}
                            </Button>
                            <BaseButton
                                variant={"text"}
                                onClick={() => {
                                    closeSnackbar(key);
                                    try {
                                        if (!pId) {
                                            UtilsRouter.goTo(props.history, {routUrl: rout.Product.Single.editProduct(res.data.id)});
                                            return;
                                        }
                                    } catch (e) {
                                    }
                                    setSetting({
                                        ...setting,
                                        redirect: true
                                    })
                                }}
                                style={{
                                    marginRight: theme.spacing(0.5)
                                }}>
                                {pId ? " بازگشت به صفحه محصولات" : 'بازگشت به ویرایش محصول'}
                            </BaseButton>
                        </Fragment>
                    )
                });


            LocalStorageUtils.remove(getCacheKey());

            const set = {
                ...setting,
                saveDialog: false,
                loading: false,
            };
            if (!pId) {
                setState({
                    ...state,
                    checkRedirect: false
                })
                set.redirect = true
            }

            setSetting({...set});

        }).catch((e) => {
            reqCancel('مشکلی در ذخیره محصول پیش آمده است.');
            setSetting({
                ...setting,
                saveDialog: false,
                loading: false
            })
        });
    }

    function reqCancel(text = null) {
        if (text)
            enqueueSnackbar(text,
                {
                    variant: "error",
                    action: (key) => (
                        <Fragment>
                            <Button onClick={() => {
                                closeSnackbar(key)
                            }}>
                                {lang.get('close')}
                            </Button>
                        </Fragment>
                    )
                });

        setSetting({
            ...setting,
            saveDialog: false,
            loading: false
        })

    }

    //endregion requests

    //region handler

    const handleName = useCallback((newData) => {
        setName({
            ...name,
            ...newData
        });
    }, [name, setName]);
    const handlePermalink = useCallback((newData) => {
        setPermalink({
            ...permalink,
            ...newData
        });
    }, [permalink, setPermalink]);
    const handleType = useCallback((newData) => {
        setType({
            ...type,
            ...newData
        });
    }, [type, setType]);
    const handleMedia = useCallback((newData) => {
        setMedia({
            ...media,
            ...newData
        });
    }, [media, setMedia]);
    const handleDescription = useCallback((newData) => {
        setDescription({
            ...description,
            ...newData
        });
    }, [description, setDescription]);
    const handleShortDescription = useCallback((newData) => {
        setShortDescription({
            ...shortDescription,
            ...newData
        });
    }, [shortDescription, setShortDescription]);
    const handleCity = useCallback((newData) => {
        setCity({
            ...city,
            ...newData
        });
    }, [city, setCity]);
    const handleAddress = useCallback((newData) => {
        setAddress({
            ...address,
            ...newData
        });
    }, [address, setAddress]);
    const handleLocation = useCallback((newData) => {
        setLocation({
            ...location,
            ...newData
        });
    }, [location, setLocation]);
    const handleDetails = useCallback((nd) => {
        const newD = details.val;

        if (nd.val) {
            if (nd.val.fa) {
                _.forEach(nd.val, (val, key) => {
                    const {servingDays, servingHours, phones, text} = val;
                    const newData = details.val[key] ? details.val[key] : {};
                    if (servingDays !== undefined)
                        newData.servingDays = servingDays;
                    if (servingHours !== undefined)
                        newData.servingHours = servingHours;
                    if (phones)
                        newData.phones = phones;
                    if (text)
                        newData.text = text;

                    newD[key] = newData;
                })
            } else {
                const {servingDays, servingHours, phones, text} = nd.val;
                const newData = details.val[details.el.activeLanguage.key] ? details.val[details.el.activeLanguage.key] : {};
                if (servingDays !== undefined)
                    newData.servingDays = servingDays;
                if (servingHours !== undefined)
                    newData.servingHours = servingHours;
                if (phones)
                    newData.phones = phones;
                if (text)
                    newData.text = editorContentToHtml(text);

                newD[details.el.activeLanguage.key] = newData;
            }
        }
        setDetails(_.cloneDeep({
            ...details,
            val: {
                ...details.val,
                ...newD,
            },
            el: {
                ...details.el,
                ...nd.el
            }
        }));

    }, [details, setDetails]);

    const handleProperties = useCallback((nd) => {
        const {val: pr} = nd;
        // let newData = properties.val;
        // if (pr !== undefined) {
        //     newData = pr
        // }
        setProperties({
            val: {
                ...properties.val,
                ...nd.val
            },
            el: {
                ...properties.el,
                ...nd.el
            }
        })
    }, [properties, setProperties]);
    const handleTags = useCallback((newData) => {
        setTags({
            ...tags,
            ...newData
        });
    }, [tags, setTags]);
    const handleBrand = useCallback((newData) => {
        setBrand({
            ...brand,
            ...newData
        });
    }, [brand, setBrand]);
    const handleCategories = useCallback((newData) => {
        setCategories({
            ...categories,
            ...newData
        });
    }, [categories, setCategories]);
    const handleThumbnail = useCallback((newData) => {
        setThumbnail({
            ...thumbnail,
            ...newData
        });
    }, [thumbnail, setThumbnail]);
    const handlePanelFeatures = useCallback((newData) => {
        setPanelFeatures({
            ...panelFeatures,
            ...newData
        });
    }, [panelFeatures, setPanelFeatures]);
    const handleSaveClick = useCallback(() => {
        setTimeout(() => {
            setSetting(s => ({
                ...s,
                saveDialog: 1
            }))
        }, 1000)
    }, [setSetting]);
    const handleSyncCache = useCallback(() => {
        const props = LocalStorageUtils.get(getCacheKey());
        if (_.isEmpty(props))
            return;
        const {
            box, name, permalink, type, media, description, shortDescription,
            city, address, location, details, properties, tags, brand, categories, thumbnail
        } = props;

        setBox(box);
        handleName(name);
        handlePermalink(permalink);
        handleType(type);
        handleMedia(media);
        handleDescription(description);
        handleShortDescription(shortDescription);
        handleCity(city);
        handleAddress(address);
        handleLocation(location);
        handleDetails(details);
        handleProperties(properties);
        handleTags(tags);
        handleBrand(brand);
        handleCategories(categories);
        handleThumbnail(thumbnail);

        setTimeout(() => {
            setSetting({...setting, cacheFind: false})
        }, 1500)
    }, []);
    const handleCloseCache = useCallback(() => {
        LocalStorageUtils.remove(getCacheKey());
        setSetting({...setting, cacheFind: false})
    }, []);
    //endregion handler

    const sideEl = useMemo(() => {
        return {
            city: {
                el: city.el,
                handle: handleCity
            },
            address: {
                el: address.el,
                handle: handleAddress
            },
            location: {
                el: location.el,
                handle: handleLocation
            },
            details: {
                el: details.el,
                handle: handleDetails
            },
            properties: {
                el: properties.el,
                handle: handleProperties
            },
            brand: {
                el: brand.el,
                handle: handleBrand
            }
        }
    }, [city.el, address.el, location.el, details.el, properties.el, brand.el]);

    const otherEl = useMemo(() => {
        return {
            media: {
                el: media.el,
                handle: handleMedia
            },
            description: {
                el: description.el,
                handle: handleDescription
            },
            shortDescription: {
                el: shortDescription.el,
                handle: handleShortDescription
            },
            category: {
                el: categories.el,
                handle: handleCategories
            },
            thumbnail: {
                el: thumbnail.el,
                handle: handleThumbnail
            },
            tags: {
                el: tags.el,
                handle: handleTags
            },
            panelFeatures: {
                el: panelFeatures.el,
                handle: handlePanelFeatures
            },
        }
    }, [media.el, description.el, shortDescription.el, categories.el, thumbnail.el, panelFeatures.el]);

    const handleSide = useCallback((newData) => {
        if (_.isBoolean(newData)) {
            _.forEach({...sideEl, ...otherEl}, (el) => {
                el.handle({el: {...el.el, open: newData}})
            });
            return
        }
        sideEl[newData].handle({el: {...sideEl[newData].el, active: !sideEl[newData].el.active}})
    }, [
        city.el, address.el,
        location.el, details.el,
        properties.el, brand.el,
        media.el.active, description.el.active,
        shortDescription.el.active, categories.el.active,
        brand.el.active,
    ]);


    if (setting.saveDialog === 1) {
        setSetting({
            ...setting,
            saveDialog: Boolean(getSaveProps())
        })
    }


    return (
        <React.Fragment>
            <Prompt
                when={state.checkRedirect}
                message={location => `آیا اطمینان دارید که میخواهید این صفحه را ترک کنید؟`}/>
            {setting.redirect &&
            <Redirect
                to={{
                    pathname: _.isString(setting.redirect) ? setting.redirect : rout.Product.rout
                }}/>}
            {
                !setting.error ?
                    <>
                        <Box>
                            {
                                (!setting.cacheFind && state.data !== false) ?
                                    state.data !== false ?
                                        box ?
                                            <FormControl display={'flex'}
                                                         name={"productSingle"}
                                                         innerref={ref}
                                                         onChange={() => {
                                                             setState({
                                                                 ...state,
                                                                 refreshCache: state.refreshCache + 1
                                                             })
                                                         }}>
                                                <Box flex={1}>
                                                    <Box display={'flex'}
                                                         flexDirection={'column'}
                                                         px={3}
                                                         py={2}>
                                                        <Box display={'flex'} alignItems={'center'}
                                                             flexWrap={'wrap'}
                                                             pb={2}>
                                                            <Typography variant={'body2'} pl={1}>
                                                                {lang.get('box')}: {box.name[activeLang]}
                                                            </Typography>
                                                            {pId &&
                                                            <BaseLink toHref={state.data.storageLink}>
                                                                رفتن به انبار
                                                            </BaseLink>
                                                            }
                                                        </Box>
                                                        <Name data={name}
                                                              onDataChange={handleName}/>
                                                        <Permalink
                                                            pId={pId}
                                                            data={permalink}
                                                            onDataChange={handlePermalink}/>

                                                        <ProductType data={type}
                                                                     pId={pId}
                                                                     onTypeChange={handleType}/>
                                                        <Media
                                                            boxId={box.id}
                                                            data={media}
                                                            onChange={handleMedia}/>
                                                        <Description data={description}
                                                                     onChange={handleDescription}/>
                                                        <Description data={shortDescription}
                                                                     isShortDescription={true}
                                                                     onChange={handleShortDescription}/>
                                                        <City
                                                            data={city}
                                                            onChange={handleCity}/>
                                                        <Address
                                                            data={address}
                                                            onChange={handleAddress}/>
                                                        <MapCo
                                                            data={location}
                                                            onChange={handleLocation}/>
                                                        <Details
                                                            data={details}
                                                            onChange={handleDetails}/>
                                                        <Properties
                                                            box={box}
                                                            data={properties}
                                                            onChange={handleProperties}/>
                                                        <Tags
                                                            box={box}
                                                            data={tags}
                                                            onChange={handleTags}/>
                                                    </Box>
                                                    <SaveDialog open={setting.saveDialog === true}
                                                                onChange={(save) => {
                                                                    setTimeout(() => {
                                                                        setSetting({
                                                                            ...setting,
                                                                            saveDialog: false,
                                                                            loading: save
                                                                        })
                                                                    }, 100);
                                                                }}/>
                                                </Box>
                                                <Box maxWidth={2 / 7} minWidth={1 / 4} pr={3}>
                                                    <StickyBox offsetTop={80} offsetBottom={20}>
                                                        <SidePanel
                                                            id={pId}
                                                            box={box}
                                                            settings={productsSettings}
                                                            brand={brand}
                                                            category={categories}
                                                            thumbnail={thumbnail}
                                                            panelFeatures={panelFeatures}
                                                            sideEl={sideEl}
                                                            disable={disable}
                                                            onBrandChange={handleBrand}
                                                            onCategoryChange={handleCategories}
                                                            onThumbnailChange={handleThumbnail}
                                                            onPanelFeaturesChange={handlePanelFeatures}
                                                            onChange={handleSide}
                                                            onSaveClick={handleSaveClick}/>
                                                    </StickyBox>
                                                </Box>
                                            </FormControl> :
                                            <SelectBox
                                                onBoxSelected={(box) => {
                                                    setBox(box)
                                                }}/> :
                                        <Box p={3}>
                                            <Skeleton variant={"rect"} height={600}/>
                                        </Box> :
                                    <CacheFindDialog
                                        open={setting.cacheFind}
                                        onSubmit={handleSyncCache}
                                        onCancel={handleCloseCache}/>
                            }
                        </Box>
                        <Backdrop open={setting.loading}
                                  style={{
                                      zIndex: '9999'
                                  }}>
                            <CircularProgress color="inherit"
                                              style={{
                                                  color: cyan[300],
                                                  width: 100,
                                                  height: 100
                                              }}/>
                        </Backdrop>
                    </> :
                    <ComponentError
                        tryAgainFun={getProduct}/>}
        </React.Fragment>
    )
}

//region Components

//region SidePanel

const SidePanel = React.memo(function SidePanel({
                                                    id,
                                                    box,
                                                    settings,
                                                    disable,
                                                    thumbnail, category, brand, panelFeatures, onCategoryChange, onBrandChange,
                                                    onThumbnailChange, onPanelFeaturesChange, onChange, sideEl,
                                                    onSaveClick, ...props
                                                }) {
    const theme = useTheme();

    return (
        <Box display={'flex'} flexDirection={'column'} {...props}>
            <SidePanelActions mt={2} mb={0.5} onSaveClick={onSaveClick}/>
            <ActiveProduct id={id} data={disable}/>
            <Thumbnail box={box} data={thumbnail} onChange={onThumbnailChange}/>
            <SelectCategory data={category} box={box}
                            onChange={onCategoryChange}/>
            <SelectBrand data={brand} box={box}
                         onChange={onBrandChange}/>
            <PanelFeatures data={panelFeatures} sideEl={sideEl} onChange={onPanelFeaturesChange}
                           onDataChange={onChange}/>
            <ProductSetting data={settings}/>
            <BoxWithTopBorder component={Card} mt={2}
                              title={lang.get("page_setting")}>
                <Box pr={4.5} pl={3} py={1.5} display={'flex'} flexWrap={'wrap'}>
                    <Box display={'flex'} alignItems={'center'} justifyContent={'center'}>
                        <BaseButton variant={"outlined"} size={'small'}
                                    onClick={() => onChange(true)}
                                    style={{
                                        borderColor: cyan[300],
                                        marginLeft: theme.spacing(1)
                                    }}>
                            بازکردن همه تب‌ها <ExpandMore style={{paddingRight: theme.spacing(0.2)}}/>
                        </BaseButton>
                        <BaseButton variant={"outlined"} size={'small'}
                                    onClick={() => onChange(false)}
                                    style={{
                                        borderColor: deepOrange[300],
                                        marginRight: theme.spacing(1)
                                    }}>
                            بستن همه تب‌ها<ExpandLess style={{paddingRight: theme.spacing(0.2)}}/>
                        </BaseButton>
                    </Box>
                </Box>
            </BoxWithTopBorder>
            <SidePanelActions my={2} onSaveClick={onSaveClick}/>
        </Box>
    )
});


function ActiveProduct({id, data}) {
    const [disable, setDisable] = useState(data.val);
    const [state, setState] = useState({
        loading: false
    });
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();

    useEffect(() => {
        setDisable(data.val);
    }, [data.val])


    function handleActiveStorage() {
        setState({
            ...state,
            loading: true
        });

        ControllerProduct.Product.activeProduct({pId: id, disable: !disable}).then(res => {
            if (res.status === 202) {
                setDisable(!disable)
            }
            setState({
                ...state,
                loading: false
            });
        }).catch(() => {
            setState({
                ...state,
                loading: false
            });
        })
    }

    useEffect(() => {
        setDisable(data.val)
    }, [data])

    return (
        <React.Fragment>
            {(id) &&
            <Box width={1} p={1}>
                <Box component={Card} display={'flex'} alignItems={'center'} justifyContent={'center'} p={1}>
                    <Box display={'flex'} justifyContent={'center'} width={1}>
                        <FormControlLabel
                            control={
                                <Switch
                                    checked={!disable}
                                    onChange={handleActiveStorage}
                                    color="primary"/>
                            }
                            label={<Typography
                                variant={'body2'}>فعال سازی محصول</Typography>}
                            labelPlacement={'top'}/>
                    </Box>
                </Box>
            </Box>}
        </React.Fragment>

    )
}

const Thumbnail = React.memo(function Thumbnail({box, data, onChange}) {
    return (
        <FormContainer open={data.el.open}
                       mt={1}
                       title={lang.get("thumbnail_image")}
                       onOpenClick={(e, open) => {
                           onChange({el: {...data.el, open: open}});
                       }}>
            <Box px={3} py={1.5} display={'flex'} flexWrap={'wrap'}>
                <UploadItem
                    boxId={box.id}
                    src={(data && data.val) ? data.val.image : ''}
                    width={serverFileTypes.Image.Thumbnail.width}
                    height={serverFileTypes.Image.Thumbnail.height}
                    type={serverFileTypes.Image.Thumbnail.type}
                    onSelected={(file) => {
                        onChange({val: file});
                    }}/>
            </Box>
        </FormContainer>

    )
});


const PanelFeatures = React.memo(function PanelFeatures({data, sideEl, onChange, onDataChange, ...props}) {

    const {el} = data;
    return (
        <FormContainer component={Card}
                       open={el.open}
                       title={"امکانات"}
                       mt={2}
                       onOpenClick={(e, open) => {
                           onChange({el: {...el, open: open}});
                       }}>
            <Box px={3} py={1.5} display={'flex'} flexWrap={'wrap'}>
                {
                    Object.keys(sideEl).map((elKey, i) => {
                        const {el} = sideEl[elKey];
                        return (
                            <React.Fragment key={elKey}>
                                {(el && _.isBoolean(el.active)) &&
                                <ItemContainer width={0.5} justifyContent={'center'}>
                                    <FormControlLabel
                                        control={
                                            <Switch
                                                checked={el.active}
                                                onChange={() => {
                                                    onDataChange(elKey)
                                                }}
                                                value={elKey}
                                                color="primary"/>
                                        }
                                        label={<Typography
                                            variant={'body2'}>{lang.get(el.label ? el.label : elKey)}</Typography>}
                                        labelPlacement={'top'}/>
                                </ItemContainer>}
                            </React.Fragment>
                        )
                    })
                }
            </Box>
        </FormContainer>
    )
});


function SidePanelActions({onSaveClick, ...props}) {
    const theme = useTheme();

    return (
        <Box {...props}>
            <BaseButton size={'small'}
                        onClick={onSaveClick}
                        style={{
                            backgroundColor: cyan[300],
                            color: '#fff',
                            width: '100%',
                        }}>
                <Typography color={'#fff'} py={1.5} variant={'h6'}
                            display={'flex'} alignItems={'center'}
                            style={{
                                display: 'flex',
                                alignItems: 'center'
                            }}>
                    ذخیره
                    <Save fontSize={"large"} style={{paddingRight: theme.spacing(1)}}/>
                </Typography>
            </BaseButton>
        </Box>
    )
};


const SelectBrand = React.memo(function SelectBrand({data: dd, box, onChange, ...props}) {
    const {val: item, el: elBrand} = dd;
    const theme = useTheme();

    const d = ControllerProduct.Brands.get();
    const apiKey = d[0] + box.id;
    const {data, error} = useSWR(apiKey, () => {
        return d[1]({
            step: 100,
            all: true
        })
    });
    const [state, setState] = useState({
        addDialog: false,
    });

    function handleBrandChange(brand) {
        onChange({val: brand});
    }

    return (
        data ?
            <FormContainer open={elBrand.open}
                           active={elBrand.active}
                           title={lang.get("brand")}
                           onOpenClick={(e, open) => {
                               onChange({el: {...elBrand, open: open}});
                           }}>
                <Box display={'flex'} justifyContent={'center'} alignItems={'center'}>
                    <Tooltip title={'افزودن'}>
                        <IconButton onClick={() => setState({
                            ...state,
                            addDialog: true
                        })}
                                    style={{
                                        marginRight: theme.spacing(0.5)
                                    }}>
                            <Add/>
                        </IconButton>
                    </Tooltip>
                    <Typography variant={'h6'} py={2} display={'flex'} justifyContent={'center'} alignItems={'center'}
                                style={{
                                    flex: 1,
                                    ...UtilsStyle.disableTextSelection()
                                }}>
                        <Tooltip title={'حذف'}>
                            <IconButton onClick={() => handleBrandChange(null)}
                                        disabled={!item}
                                        style={{
                                            color: red[400],
                                            marginLeft: theme.spacing(0.5)
                                        }}>
                                <DeleteOutlineOutlined/>
                            </IconButton>
                        </Tooltip>
                        برند: {(item && item.name) ? item.name[activeLang] : '------'}
                    </Typography>
                    <Tooltip title={'بروزرسانی'}>
                        <IconButton onClick={() => mutate(apiKey)}
                                    style={{
                                        marginLeft: theme.spacing(0.5)
                                    }}>
                            <Sync/>
                        </IconButton>
                    </Tooltip>
                </Box>
                <Box display={'flex'} flexDirection={'column'} mx={2}
                     style={{
                         height: 300,
                         overflowY: 'auto'
                     }}>
                    {(data.data) && data.data.map((b, i) => (
                        <React.Fragment key={b.id}>
                            <Divider style={{
                                opacity: 0.5
                            }}/>
                            <ButtonBase
                                onClick={() => handleBrandChange(b)}
                                style={{
                                    cursor: 'pointer',
                                    display: 'flex',
                                    justifyContent: 'flex-start',
                                    ...UtilsStyle.disableTextSelection(),
                                }}>
                                <Box key={b.id} py={1} display={'flex'} flex={'wrap'} alignItems={'center'}>
                                    <Radio
                                        checked={item && b.id === item.id}
                                        value="b"
                                        name="radio-button-demo"
                                        inputProps={{'aria-label': 'B'}}
                                    />
                                    <Typography variant={'body1'}>
                                        {b.name[activeLang]}
                                    </Typography>
                                </Box>
                            </ButtonBase>
                        </React.Fragment>
                    ))}
                </Box>
                <EditBrand open={state.addDialog}
                           onClose={(br) => {
                               if (br) {
                                   mutate(apiKey);
                                   handleBrandChange(br)
                               }
                               setState({
                                   ...state,
                                   addDialog: false
                               })
                           }}/>
            </FormContainer> :
            elBrand.active ?
                <Box width={1} pt={2}>
                    <Skeleton height={90} variant={"rect"}/>
                </Box> :
                <React.Fragment/>
    )
});

function ProductSetting({data}) {
    const [state, setState] = useState({
        open: false,
    });

    const [productStatus, setProductStatus] = useState(_.toString(data.productStatus || 0));


    return (
        <FormContainer open={state.open}
                       title={"تنظیمات"}
                       onOpenClick={(e, open) => {
                           setState({
                               ...state,
                               open: open
                           })
                       }}>
            <Box display={'flex'} flexDirection={'column'}>
                <Box p={2}>
                    <NoneTextField
                        name={createName({group: 'settings', name: "productStatus"})}
                        defaultValue={productStatus}/>
                    <MaterialFormControl component="fieldset">
                        <FormLabel component="legend">
                            <Typography pb={0.75} variant={"body1"}>
                                وضعیت محصول
                            </Typography>
                            <Typography pb={2} variant={'caption'}>
                                این بخش جهت داشتن آمار وضعیت محصول میباشد و به کاربران نشان داده نخواهد شد.
                                <br/>
                                درصورتی که محصول به یک حد قابل قبول از اطلاعات رسیده است وضعیت مطلوب را انتخاب کنید.
                                <br/>
                                درصورتی که محصول نیاز به هیچ تغییری ندارد و به کامل ترین حالت اطلاعات خود رسیده است
                                وضعیت کامل را انتخاب کنید.
                            </Typography>
                        </FormLabel>
                        <RadioGroup row aria-label="productStatus" value={productStatus}
                                    onChange={(event) => {
                                        setProductStatus(event.target.value)
                                    }}>
                            <FormControlLabel
                                value="0"
                                control={<Radio color="primary"/>}
                                label="ناقص"
                                labelPlacement="top"/>
                            <FormControlLabel
                                value="1"
                                control={<Radio color="primary"/>}
                                label="مطلوب"
                                labelPlacement="top"/>
                            <FormControlLabel
                                value="2"
                                control={<Radio color="primary"/>}
                                label="کامل"
                                labelPlacement="top"/>
                        </RadioGroup>
                    </MaterialFormControl>
                </Box>
                <Box p={2}>
                    <MultiTextField
                        label={"یادداشت"}
                        placeholder={"یک متن برای یاد داشت خودتان بنویسد. (این قسمت به کاربر نشان داده نخواهد شد)"}
                        group={"settings"}
                        name={"adminNote"}
                        defaultValue={data.adminNote || ""}
                        rows={4}
                    />
                </Box>
            </Box>
        </FormContainer>
    )
}

//region Category
const FullScreenTransition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});


const SelectCategory = React.memo(function SelectCategory({data: dd, box, onChange, ...props}) {
    console.log("cheeeccck SelectCategory");
    const theme = useTheme();
    const {val: item, el: elCategory} = dd;

    const d = ControllerProduct.Categories.getAllCategories({});
    const apiKey = d[0] + box.id;
    const {data, error} = useSWR(apiKey, () => {
        return d[1]({
            boxId: box.id,
        }).then(res => {
            return res.categories;
        })
    });
    const [state, setState] = useState({
        addDialog: false,
    });


    function getData() {
        mutate(apiKey);
    }

    function handleCategoryChange(cat) {
        const newList = item ? item : [];
        const remove = _.remove(newList, (c) => {
            return c.id === cat.id
        });

        if (_.isEmpty(remove)) {
            newList.push(cat);
        }

        onChange({val: newList});
    }


    const itemParentsIdList = getParents();

    function getParents() {
        const list = [];
        _.forEach(item, (i) => {
            list.push(i.id)
        });
        return getActiveParents(list, data);
    }

    return (
        !error ?
            data ?
                <FormContainer open={elCategory.open}
                               title={lang.get("category")}
                               onOpenClick={(e, open) => {
                                   onChange({el: {...elCategory, open: open}});
                               }}>
                    <Box display={'flex'} justifyContent={'center'} alignItems={'center'}>
                        <Tooltip title={'افزودن'}>
                            <IconButton onClick={() => setState({
                                ...state,
                                addDialog: true
                            })}
                                        style={{
                                            marginRight: theme.spacing(0.5)
                                        }}>
                                <Add/>
                            </IconButton>
                        </Tooltip>
                        <Typography variant={'h6'} py={2} display={'flex'} justifyContent={'center'}
                                    alignItems={'center'}
                                    style={{
                                        flex: 1,
                                        ...UtilsStyle.disableTextSelection()
                                    }}>
                            انتخاب دسته
                        </Typography>
                        <Tooltip title={'بروزرسانی'}>
                            <IconButton onClick={getData}
                                        style={{
                                            marginLeft: theme.spacing(0.5)
                                        }}>
                                <Sync/>
                            </IconButton>
                        </Tooltip>
                    </Box>
                    <Box display={'flex'} flexDirection={'column'} mx={2}
                         style={{
                             height: 300,
                             overflowY: 'auto'
                         }}>
                        <CategoryContainer activeItems={item}
                                           items={data}
                                           activeParents={itemParentsIdList ? itemParentsIdList : []}
                                           onCategoryChange={handleCategoryChange}/>
                    </Box>
                    <Dialog fullScreen open={state.addDialog} TransitionComponent={FullScreenTransition}>
                        <CategorySingle
                            box={box}
                            onClose={(cat) => {
                                if (cat) {
                                    handleCategoryChange(cat);
                                    getData();
                                }
                                setState({
                                    ...state,
                                    addDialog: false
                                })
                            }}/>
                    </Dialog>
                </FormContainer> :
                <Box width={1} pt={2}>
                    <Skeleton height={90} variant={"rect"}/>
                </Box> :
            <ComponentError
                tryAgainFun={getData}/>
    )
});

function CategoryContainer({activeItems, items, level = 0, activeParents = [], onCategoryChange, ...props}) {

    const theme = useTheme();
    const darkMode = theme.palette.type === "dark";

    return (
        items ?
            <Box width={1} px={1} py={1}
                 boxShadow={1}
                 style={{
                     backgroundColor: level !== 0 ? grey[darkMode ? (800 - (level * 100)) : level * 100] : null,
                     ...UtilsStyle.borderRadius('0 0 5px 5px')
                 }}>
                {
                    items.map((cat, i) => (
                        <CategoryItem key={cat.id} index={i} activeItems={activeItems}
                                      activeParents={activeParents}
                                      item={cat} level={level}
                                      onCategoryChange={onCategoryChange}/>
                    ))
                }
            </Box> :
            <React.Fragment/>
    )
}

function CategoryItem({activeItems, index, item, level = 0, activeParents, onCategoryChange, ...props}) {
    const [state, setState] = useState({
        open: false,
    });


    const isActive = _.findIndex(activeItems, (i) => i.id === item.id) !== -1;
    const isParent = !isActive && _.findIndex(activeParents, (p) => p.id === item.id) !== -1;

    return (
        <React.Fragment>
            {index !== 0 &&
            <Divider style={{
                opacity: 0.5
            }}/>}
            <Box width={1} display={'flex'} alignItems={'center'}>
                <ButtonBase
                    onClick={() => onCategoryChange(item)}
                    style={{
                        cursor: 'pointer',
                        display: 'flex',
                        flexWrap: 'wrap',
                        justifyContent: 'flex-start',
                        flex: 1,
                        ...UtilsStyle.disableTextSelection(),
                    }}>
                    <Box key={item.id} py={1} display={'flex'} flex={'wrap'} alignItems={'center'}>
                        <Checkbox
                            checked={isActive}
                            value={item.id}
                            indeterminate={isParent}
                            name={`category-${item.id}`}
                            inputProps={{'aria-label': `category-${item.id}`}}
                            style={{
                                color: isActive ? red[400] : isParent ? red[200] : null
                            }}
                        />
                        <Typography variant={'body1'}>
                            {item.name[activeLang]}
                        </Typography>
                    </Box>
                </ButtonBase>
                {item.child &&
                <Box mx={0.5}>
                    <Tooltip title={"نمایش فرزندان"}>
                        <IconButton onClick={() => setState({
                            ...state,
                            open: !state.open
                        })}>
                            {state.open ? <ExpandLess/> : <ExpandMore/>}
                        </IconButton>
                    </Tooltip>
                </Box>}
            </Box>
            {item.child &&
            <Collapse in={state.open}>
                <CategoryContainer activeItems={activeItems} items={item.child}
                                   activeParents={activeParents}
                                   onCategoryChange={onCategoryChange}
                                   level={level + 1}/>
            </Collapse>
            }
        </React.Fragment>
    )
}

//endregion Category
//endregion SidePanel

const Name = React.memo(function Name({data, onDataChange}) {
    const {val: name, el} = data;
    return (
        <ItemContainer mt={0}>
            <TextFieldMultiLanguageContainer
                name={"name"}
                render={(ref, {lang: inputLang, name: inputName, initialize, valid, errorIndex, setValue, inputProps, style, props}) => {
                    return (
                        <TextField
                            {...props}
                            error={!valid}
                            variant="outlined"
                            name={inputName}
                            defaultValue={name[inputLang.key]}
                            fullWidth
                            helperText={errorList[errorIndex]}
                            inputRef={ref}
                            required={inputLang.key === siteLang}
                            label={lang.get('product_name')}
                            style={style}
                            inputProps={{
                                ...inputProps
                            }}/>
                    )
                }}/>
        </ItemContainer>
    )
});

const permalinkErrorList = [...errorList, 'ساختار یوندیکتا درست نمیباشد'];
const Permalink = React.memo(function Permalink({pId, nameRef, loading, data, onDataChange}) {
    const {val: permalink} = data;
    const theme = useTheme();
    const [inputRef, setInputRef] = useState();

    const {enqueueSnackbar, closeSnackbar} = useSnackbar();

    const [state, setState] = useState({
        loading: false,
        edit: !pId
    });

    function onSaveHandler() {

        if (!state.edit) {
            setState({
                ...state,
                edit: true
            });
            return;
        }
        const permalink = inputRef.current.value

        if (_.isEmpty(permalink)) {
            enqueueSnackbar(`لطفا فیلد ${lang.get('permalink')} را پر کنید.`,
                {
                    variant: "error",
                    action: (key) => (
                        <Fragment>
                            <Button onClick={() => {
                                closeSnackbar(key)
                            }}>
                                {lang.get('close')}
                            </Button>
                        </Fragment>
                    )
                });
            return;
        }

        if (pId) {
            setState({
                ...state,
                loading: true
            });
            ControllerProduct.Product.updatePermalink({pId: pId, permalink: permalink}).then((res) => {

                onDataChange({val: permalink});
                setState({
                    ...state,
                    loading: false,
                    edit: false
                });

                enqueueSnackbar(`با موفقیت ${lang.get('permalink')} تفییر کرد.`,
                    {
                        variant: "success",
                        action: (key) => (
                            <Fragment>
                                <Button onClick={() => {
                                    closeSnackbar(key)
                                }}>
                                    {lang.get('close')}
                                </Button>
                            </Fragment>
                        )
                    });
            }).catch((e) => {
                setState({
                    ...state,
                    loading: false
                });

                enqueueSnackbar(`تغییر ${lang.get('permalink')} به مشکل برخود.`,
                    {
                        variant: "error",
                        action: (key) => (
                            <Fragment>
                                <Button onClick={() => {
                                    closeSnackbar(key)
                                }}>
                                    {lang.get('close')}
                                </Button>
                            </Fragment>
                        )
                    });
            });
            return;
        }

        onDataChange({val: permalink});
        setState({
            ...state,
            edit: false
        })
    }

    return (
        <ItemContainer flexWrap={'wrap'} alignItems={'flex-end'}>
            {
                (!state.edit) ?
                    <React.Fragment>
                        <NoneTextField
                            name={"permalink"}
                            defaultValue={permalink}/>
                        <Typography variant={'subtitle1'}>
                            <Box display={'flex'} alignItems={'center'}>
                                {lang.get("permalink")}:
                                <Typography component={pId ? 'a' : 'span'}
                                            variant={'subtitle1'}
                                            color={theme.palette.text.disabled}
                                            mr={1}
                                            target={'_blank'}
                                            dir={'ltr'}
                                            href={webRout + siteRout.Product.Single.create({permalink: permalink})}>
                                    {webRout + siteRout.Product.Single.create({permalink: permalink})}
                                </Typography>
                            </Box>
                        </Typography>
                    </React.Fragment> :
                    <Box px={2} width={0.7}>
                        <PermalinkTextField
                            name={"permalink"}
                            variant={'standard'}
                            syncedRef={nameRef}
                            defaultValue={permalink}
                            onRefChange={(ref) => {
                                if (ref) {
                                    setInputRef(ref)
                                }
                            }}/>
                    </Box>
            }

            <BaseButton
                variant={'outlined'}
                size={"superSmall"}
                fontWeight={300}
                loading={state.loading}
                onClick={onSaveHandler}
                style={{
                    border: `1px solid ${loading ? grey[400] : cyan[300]}`,
                    marginRight: theme.spacing(1.5),
                }}>
                {lang.get(!state.edit ? 'edit' : 'submit')}
            </BaseButton>
        </ItemContainer>
    )
});

const ProductType = React.memo(function ProductType({pId, data, onTypeChange}) {
    const theme = useTheme();
    const {val: type} = data;
    return (
        <ItemContainer flexWrap={'wrap'} alignItems={'center'}>
            <Typography variant={'body1'} pl={2}>
                {lang.get("type")}:
            </Typography>
            {pId ?
                <Typography variant={'body1'} fontWeight={600} pr={1}>
                    {type.label}
                </Typography> :
                (
                    <RadioGroup row aria-label="position" name="position" value={type.value} onChange={(e, type) => {
                        onTypeChange({
                            val: {
                                ...type,
                                value: type
                            }
                        })
                    }}>

                        {
                            Object.keys(productType).map((key, index) => {
                                const item = productType[key];
                                return (
                                    <FormControlLabel
                                        key={key}
                                        value={item.type}
                                        control={<Radio color="primary"/>}
                                        label={<Typography color={theme.palette.text.primary}>{item.label}</Typography>}
                                        labelPlacement="end"
                                    />
                                )
                            })
                        }
                    </RadioGroup>
                )}
        </ItemContainer>
    )
});

//region Media
const Media = React.memo(function Media({boxId, data: dd, onChange, ...props}) {
    const {val: data, el: elMedia} = dd;
    const [state, setState] = useState({
        sorting: false
    });

    return (
        <FormContainer open={elMedia.open}
                       title={lang.get("media")}
                       onOpenClick={(e, open) => {
                           onChange({el: {open: open}});
                       }}>
            <Box py={2} px={2}>
                {lang.get("sorting")}: <Switch value={state.sorting} disabled={!data || data.length < 2}
                                               onChange={(v) => {
                                                   setState({
                                                       ...state,
                                                       sorting: !state.sorting
                                                   })
                                               }}/>
            </Box>
            {
                state.sorting ?
                    <Box width={1}>
                        <DraggableList
                            items={data}
                            onItemsChange={(items) => {
                                onChange({val: items});
                            }}
                            rootStyle={(dragging) => {
                                return {
                                    backgroundColor: dragging ? null : grey[200],
                                    ...UtilsStyle.transition(500)
                                }
                            }}
                            render={(item, props, {isDraggingItem, ...p}) => (
                                <Box key={item.id}
                                     display={'flex'}
                                     width={1}
                                     {...props}>
                                    <Box display={'flex'} alignItems={'center'}
                                         py={1}>
                                        <Box px={2} py={1}>
                                            {isDraggingItem ?
                                                <MenuOpen/> :
                                                <Menu/>
                                            }
                                        </Box>
                                        <Img maxWidth={70} minHeight={'unset'} src={item.image}/>
                                        <Typography pr={2} pl={1} variant={'body1'} fontWeight={600}
                                                    style={{
                                                        justifyContent: 'center',
                                                    }}>
                                            {item.title.fa}
                                        </Typography>
                                    </Box>
                                </Box>
                            )}
                        />
                    </Box> :
                    <Box
                        display={'flex'}
                        flexWrap={'wrap'}
                        width={1}>
                        {data.map(item => (
                            <MediaItem key={item.id}
                                       item={item}
                                       onRemove={() => {
                                           const newList = [...data];
                                           _.remove(newList, d => d.id === item.id);
                                           onChange({val: newList});
                                       }}/>
                        ))}
                        <Box width={1 / 3} px={2} py={2} display={'flex'} alignItems={'center'}
                             justifyContent={'center'}>
                            <UploadItem
                                boxId={boxId}
                                holderWidth={200}
                                holderHeight={'auto'}
                                multiSelect={true}
                                width={serverFileTypes.Image.Media.width}
                                height={serverFileTypes.Image.Media.height}
                                type={serverFileTypes.Image.Media.type}
                                onSelected={(files) => {
                                    _.forEach(data, (d) => {
                                        _.remove(files, (f) => {
                                            return f.id === d.id;
                                        })
                                    })

                                    onChange({val: [...data, ...files]})
                                }}/>
                        </Box>
                    </Box>
            }
        </FormContainer>
    )
});


function MediaItem({item, onRemove, ...props}) {
    return (
        <Box px={2} py={2} width={1 / 3}>
            <Box component={Card} display={'flex'} flexDirection={'column'}>
                <Img src={item.image}/>
                <Box display={'flex'} flexWrap={'wrap'} py={1} px={1} alignItems={'center'}>
                    <Box pl={1}>
                        <IconButton onClick={() => onRemove()}>
                            <Delete fontSize={"small"}/>
                        </IconButton>
                    </Box>
                    <Typography variant={'body1'} fontWeight={600} style={{
                        justifyContent: 'center',
                    }}>
                        {item.title.fa}
                    </Typography>
                </Box>
            </Box>
        </Box>
    )
}

//endregion Media

const Description = React.memo(function Description({data: dd, isShortDescription = false, onChange}) {
    const {val: description, el: elDescription} = dd;
    const [data, setData] = useState({});
    const [orgDescription, setOrgDescription] = useState({
        firstTime: false
    });
    const [timer, setTimer] = useState(null);

    useEffect(() => {
        const newDescription = {};
        _.forEach(sLang, (l, key) => {
            newDescription[key] = htmlToEditorContent(description[key] ? description[key] : '')
        });
        setData(newDescription)
        return () => {
            clearTimeout(timer)
        }
    }, []);

    useEffect(() => {
        const newOrgDescription = orgDescription;
        if (!orgDescription.firstTime) {
            _.forEach(sLang, (l, key) => {
                newOrgDescription[key] = Boolean(description[key])
            });
            setOrgDescription({
                ...newOrgDescription,
                firstTime: true
            });
            return
        }
        newOrgDescription[elDescription.activeLanguage.key] = Boolean(description[elDescription.activeLanguage.key]);

        setOrgDescription({
            ...newOrgDescription,
        });
    }, [description]);

    function handlerDescriptionChange(content) {
        const newData = data;
        newData[elDescription.activeLanguage.key] = content;
        setData({
            ...newData
        });

        if (timer != null)
            clearTimeout(timer);
        setTimer(setTimeout(() => {
            const newData = description;
            newData[elDescription.activeLanguage.key] = editorContentToHtml(content);
            onChange({val: newData})
        }, 4000))
    }

    return (
        <ItemContainer>
            <FormContainer open={elDescription.open}
                           width={1}
                           title={lang.get(!isShortDescription ? "description" : 'short_description')}
                           activeLanguage={elDescription.activeLanguage}
                           activeLanguageValue={orgDescription}
                           onLanguageChange={(lang) => {
                               onChange({el: {...elDescription, activeLanguage: lang}});
                           }}
                           onOpenClick={(e, open) => {
                               onChange({el: {...elDescription, open: open}});
                           }}>
                <Editor
                    content={data[elDescription.activeLanguage.key]}
                    onContentChange={handlerDescriptionChange}/>
            </FormContainer>
        </ItemContainer>
    )
});

const City = React.memo(function City({data, onChange, ...props}) {

    const {val: city, el: elCity} = data;

    const [setting, setSetting] = useState({
        firstRequest: false
    });

    const [state, setState] = useState({
        activeItem: null
    });
    const [cityState, setCityState] = useState({
        data: null,
    });

    useEffect(() => {
        if (city) {
            const index = _.findIndex(states, (s) => {
                return s.id === city.state
            });
            if (index === -1)
                return;
            setState({
                activeItem: states[index]
            })
        }
    }, []);


    useEffect(() => {
        if (setting.countChange <= 2) {
            setSetting({
                ...setting,
                countChange: setting.countChange + 1
            })
            return
        }

        if (state.activeItem) {
            setCityState({
                ...cityState,
                data: [],
            });
            handleCityChange(null)
            getCity(state.activeItem.id)
        }
    }, [state.activeItem]);

    function getCity(stateId) {
        ControllerSite.cities.get({stateId: stateId}).then((res) => {
            const data = {
                ...cityState,
                data: res.data.cities,
            };
            setCityState(data);
            if (!setting.firstRequest) {
                setSetting({
                    ...setting,
                    firstRequest: true
                });
                onChange({val: city});
            }
        }).catch(e => {
            setCityState({
                ...cityState,
                data: undefined
            })
        })
    };


    function handleCityChange(item) {
        if (setting.firstRequest)
            onChange({val: item})
    }

    return (
        <FormContainer open={elCity.open} active={elCity.active}
                       title={lang.get("city")}
                       onOpenClick={(e, open) => {
                           onChange({el: {open: open}})
                       }}>
            <Box display={'flex'} px={2} py={2}>
                <Box width={0.5} pr={1}>
                    <AutoFill
                        inputId='stateInput'
                        items={states}
                        disabled={(_.isArray(cityState.data) && _.isEmpty(cityState.data)) || props.disable}
                        activeItem={state.activeItem}
                        onActiveItemChanged={(item) => {
                            setState({
                                ...state.state,
                                activeItem: item
                            });
                        }}
                        title={lang.get('state')}
                        placeholder={lang.get('search_state')}/>
                </Box>
                <Box width={0.5} pl={1}>
                    <AutoFill
                        inputId='cityInput'
                        items={cityState.data ? cityState.data : []}
                        disabled={(_.isEmpty(cityState.data)) || props.disable}
                        activeItem={city}
                        onActiveItemChanged={(item) => {
                            handleCityChange(item)
                        }}
                        title={lang.get('city')}
                        placeholder={lang.get('search_city')}/>
                </Box>
            </Box>
        </FormContainer>
    )
});

//region Address

const Address = React.memo(function Address({data, onChange, ...props}) {
    const elAddress = data.el;
    const {address, shortAddress} = data.val;
    return (
        <FormContainer open={elAddress.open} active={elAddress.active}
                       title={lang.get("address")}
                       activeLanguageValue={[address, shortAddress]}
                       activeLanguage={elAddress.activeLanguage}
                       onLanguageChange={(lang) => {
                           onChange({el: {...elAddress, activeLanguage: lang}});
                       }}
                       onOpenClick={(e, open) => {
                           onChange({el: {...elAddress, open: open}});
                       }}>
            {Object.keys(sLang).map((langKey) => {
                const elLang = sLang[langKey];
                return (
                    <Box key={langKey} display={elLang.key !== elAddress.activeLanguage.key ? 'none' : 'flex'} px={2}
                         py={1}>
                        <ItemContainer width={0.4} pr={2}>
                            <TextFieldContainer
                                name={createName({name: "shortAddress", langKey: elLang.key})}
                                defaultValue={shortAddress[elLang.key]}
                                render={(ref, {name: inputName, initialize, valid, errorIndex, setValue, inputProps, style, props}) => {
                                    return (
                                        <TextField
                                            {...props}
                                            error={!valid}
                                            name={inputName}
                                            helperText={errorList[errorIndex]}
                                            inputRef={ref}
                                            fullWidth
                                            required={elLang.key === siteLang}
                                            label={lang.get("short_address")}
                                            style={{
                                                ...style,
                                                minWidth: '90%'
                                            }}
                                            inputProps={{
                                                ...inputProps,
                                                ...createInputProps({langKey: elLang.key}),
                                            }}/>
                                    )
                                }}/>
                        </ItemContainer>
                        <ItemContainer width={0.6} pl={2}>
                            <TextFieldContainer
                                name={createName({name: "address", langKey: elLang.key})}
                                defaultValue={address[elLang.key]}
                                render={(ref, {name: inputName, initialize, valid, errorIndex, setValue, inputProps, style, props}) => {
                                    return (
                                        <TextField
                                            {...props}
                                            error={!valid}
                                            name={inputName}
                                            helperText={errorList[errorIndex]}
                                            inputRef={ref}
                                            fullWidth
                                            required={elLang.key === siteLang}
                                            label={lang.get("address")}
                                            style={{
                                                ...style,
                                                minWidth: '90%'
                                            }}
                                            inputProps={{
                                                ...inputProps,
                                                ...createInputProps({langKey: elLang.key}),
                                            }}/>
                                    )
                                }}/>
                        </ItemContainer>
                    </Box>
                )
            })}
        </FormContainer>
    )
});


//endregion Address

//region Map

const MapCo = React.memo(function MapCo({data, onChange, ...props}) {
    console.log("cheeeccck MapCo");
    const {val: location, el: elLocation} = data;
    const theme = useTheme();

    return (
        <FormContainer open={elLocation.open}
                       active={elLocation.active}
                       title={lang.get("map")}
                       flexDirection={'column'}
                       onOpenClick={(e, open) => {
                           onChange({el: {...elLocation, open: open}})
                       }}>
            <Box display={'flex'} flexWrap={'wrap'} px={1} py={2} justifyContent={'center'} alignItems={'center'}>
                <BaseButton variant={'contained'}
                            onClick={() => onChange({el: {...elLocation, mapOpen: true}})}
                            style={{
                                backgroundColor: cyan[300],
                                color: '#fff',
                                marginLeft: !_.isEmpty(location) ? theme.spacing(1) : null
                            }}>
                    <Map style={{marginLeft: theme.spacing(0.5)}}/>
                    {lang.get(location ? "edit_address" : "add_address")}
                </BaseButton>
                {!_.isEmpty(location) ?
                    <BaseButton variant={"text"} size={'small'}
                                onClick={() => onChange({val: null})}
                                style={{marginRight: theme.spacing(1)}}>
                        <Close fontSize={'small'} style={{marginLeft: theme.spacing(0.5)}}/>
                        حذف آدرس
                    </BaseButton>
                    : null}
            </Box>
            {!_.isEmpty(location) ?
                <Box my={2} mx={2} height={elLocation.showMap ? 400 : 300}
                     display='flex'
                     justifyContent={'center'}
                     alignItems={'center'}
                     style={{
                         backgroundImage: elLocation.showMap ? null : `url(${MapImage})`,
                         border: `1px solid ${grey[700]}`,
                         backgroundRepeat: 'no-repeat',
                         backgroundSize: 'cover',
                         ...UtilsStyle.borderRadius(5)
                     }}>

                    {elLocation.showMap ?
                        <MapComponent
                            location={[
                                location.lat,
                                location.lng,
                            ]}/> :
                        <BaseButton
                            variant={"contained"}
                            onClick={() => onChange({el: {...elLocation, showMap: true}})}
                            style={{
                                backgroundColor: cyan[300],
                                color: "#fff",
                                padding: theme.spacing(2)
                            }}>
                            {lang.get("show_address_on_map")}
                            <Map
                                style={{
                                    marginRight: theme.spacing(1)
                                }}/>
                        </BaseButton>}
                </Box>
                : null
            }
            <SelectLocation
                open={elLocation.mapOpen}
                title={lang.get(location ? "edit_address" : "add_address")}
                targetLocation={{
                    lat: (location && location.lat) ? location.lat : 37.28049,
                    lng: (location && location.lng) ? location.lng : 49.59051
                }}
                onSelect={(val) => {
                    const props = {el: {...elLocation, mapOpen: false}}
                    if (val) {
                        props.val = val;
                    }
                    onChange({...props})
                }}/>
        </FormContainer>
    )
});

let DefaultIcon = L.icon({
    iconUrl: icon,
});
L.Marker.prototype.options.icon = DefaultIcon;
const useMapStyles = makeStyles({
    map: {
        '& .leaflet-marker-pane img': {
            top: -41,
            right: -12.5,
            width: 25,
            height: 41
        },
        '& .leaflet-popup': {
            bottom: '22px !important',
        }
    }
});

function MapComponent({location, ...props}) {
    const classes = useMapStyles(props);
    return (
        <LeafletMap
            className={classes.map}
            animate={true}
            center={location}
            doubleClickZoom={true}
            boxZoom={true}
            zoomControl={true}
            minZoom={7}
            zoom={14}
            maxZoom={19}
            length={4}
            style={{
                width: '100%',
                height: '100%',
            }}>
            <TileLayer
                attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"/>
            <Marker position={location}/>
        </LeafletMap>
    )
}

const useMapStyle = makeStyles(theme => ({
    mapContainer: {},
    mapSubmitBtn: {
        paddingTop: theme.spacing(2),
        paddingBottom: theme.spacing(2),
        ...UtilsStyle.borderRadius(5),
    },
    mapSubmitBtnContainer: {
        position: 'absolute',
        zIndex: 3,
        bottom: 0,
        left: 0,
        right: 0,
        width: '100%',
    },
}));

export function FullScreenMap(props) {
    const {firstLocation, onSubmitClick, onReturnClick} = props;
    const classes = useMapStyle(props);
    const defaultLocation = firstLocation ? firstLocation : {
        lat: 37.28049,
        lng: 49.59051,
    };
    const [map, setMap] = React.useState({
        centerLocation: defaultLocation,
    });
    return (
        <Box display='flex'
             boxShadow={3}
             className={classes.mapContainer}
             flexDirection='column'
             width={1}
             height={1}>
            <AddAddressMapComponent
                centerLocation={map.centerLocation}
                targetLocation={defaultLocation}
                onMoveEnd={(center) => {
                    setMap({
                        ...map,
                        centerLocation: center
                    })
                }}
                style={{
                    zIndex: 2
                }}/>
            <Box className={classes.mapSubmitBtnContainer} display='flex'>
                <Box width="70%">
                    <BaseButton
                        disabled={false}
                        size='large'
                        className={classes.mapSubmitBtn}
                        onClick={() => onSubmitClick(map.centerLocation)}
                        style={{
                            backgroundColor: blue[500],
                            color: "#fff",
                            width: '100%'
                        }}>
                        {lang.get('submit_location')}
                    </BaseButton>
                </Box>
                <Box width='30%'>
                    <BaseButton
                        disabled={false}
                        variant={"text"}
                        size='large'
                        className={classes.mapSubmitBtn}
                        onClick={onReturnClick}
                        style={{
                            backgroundColor: red[500],
                            color: "#fff",
                            width: '100%'
                        }}>
                        {lang.get('return')}
                    </BaseButton>
                </Box>
            </Box>
        </Box>
    )
}

FullScreenMap.propTypes = {
    firstLocation: PropTypes.object,
    onSubmitClick: PropTypes.func.isRequired,
    onReturnClick: PropTypes.func.isRequired
};
//endregion Map

//region Details

const detailsDefaultValue = {
    servingDays: {
        fa: [
            {
                name: 'خالی',
                data: '',
            },
            {
                name: 'با هماهنگی',
            },
            {
                name: 'شنبه تا پنجشنبه',
            },
            {
                name: 'پنج شنبه و جمعه',
            },
        ],
        en: [
            {
                name: 'none',
                data: '',
            },
            {
                name: 'Harmony',
            },
            {
                name: 'Saturday to Thursday',
            },
            {
                name: 'Thursday and Friday',
            },
        ],
    },
    servingHours: {
        fa: [
            {
                name: 'خالی',
                data: '',
            },
            {
                name: 'با هماهنگی',
            },
            {
                name: '24 ساعته',
            },
            {
                name: '8 صبح تا 12 شب',
            },
            {
                name: '8 صبح تا 1 ظهر - 5 غروب تا 12 شب',
            },
        ],
        en: [
            {
                name: 'none',
                data: '',
            },
            {
                name: 'Harmony',
            },
            {
                name: '24 hour',
            },
            {
                name: '8am to 12pm',
            },
            {
                name: '8am to 1pm - 5pm to 12pm',
            },
        ],
    }
}

const Details = React.memo(function Details({data, onChange, ...props}) {
    const {val: details, el: elDetails} = data;


    return (
        <FormContainer open={elDetails.open} active={elDetails.active}
                       activeLanguageValue={details}
                       activeLanguage={elDetails.activeLanguage}
                       title={lang.get("details")}
                       onLanguageChange={(lang) => {
                           onChange({el: {...elDetails, activeLanguage: lang}})
                       }}
                       onOpenClick={(e, open) => {
                           onChange({el: {...elDetails, open: open}})
                       }}>
            {Object.keys(sLang).map((langKey) => {
                const elLang = sLang[langKey];
                return (
                    <Box key={langKey} display={elLang.key !== elDetails.activeLanguage.key ? 'none' : 'flex'} px={2}
                         py={1}
                         flexWrap={'wrap'}>
                        <ItemContainer width={0.5} px={2} flexDirection={'column'}>
                            <TextFieldContainer
                                name={createName({group: 'details', name: "servingDays", langKey: elLang.key})}
                                defaultValue={details[elLang.key] ? details[elLang.key].servingDays : ''}
                                render={(ref, {name: inputName, initialize, valid, errorIndex, setValue, inputProps, style, props}) => {
                                    return (
                                        <TextField
                                            {...props}
                                            error={!valid}
                                            name={inputName}
                                            helperText={errorList[errorIndex]}
                                            inputRef={ref}
                                            fullWidth
                                            label={lang.get("serving_days")}
                                            style={{
                                                ...style,
                                                width: '90%'
                                            }}
                                            inputProps={{
                                                ...inputProps,
                                                ...createInputProps({langKey: elLang.key})
                                            }}/>
                                    )
                                }}/>
                            {detailsDefaultValue.servingDays[elDetails.activeLanguage.key] ?
                                <TemplateContainer>
                                    {detailsDefaultValue.servingDays[elDetails.activeLanguage.key].map(({name, data}, index) => (
                                        <TemplateItem key={index} title={name} data={data}
                                                      onClick={(data) => {
                                                          onChange({val: {servingDays: data}});
                                                      }}/>
                                    ))}
                                </TemplateContainer>
                                : null}
                        </ItemContainer>
                        <ItemContainer width={0.5} px={2} flexDirection={'column'}>
                            <TextFieldContainer
                                name={createName({group: 'details', name: "servingHours", langKey: elLang.key})}
                                defaultValue={details[elLang.key] ? details[elLang.key].servingHours : ''}
                                render={(ref, {name: inputName, initialize, valid, errorIndex, setValue, inputProps, style, props}) => {
                                    return (
                                        <TextField
                                            {...props}
                                            error={!valid}
                                            name={inputName}
                                            helperText={errorList[errorIndex]}
                                            inputRef={ref}
                                            fullWidth
                                            label={lang.get("serving_hours")}
                                            style={{
                                                ...style,
                                                width: '90%'
                                            }}
                                            inputProps={{
                                                ...inputProps,
                                                ...createInputProps({langKey: elLang.key})
                                            }}/>
                                    )
                                }}/>
                            {detailsDefaultValue.servingHours[elDetails.activeLanguage.key] ?
                                <TemplateContainer>
                                    {detailsDefaultValue.servingHours[elDetails.activeLanguage.key].map(({name, data}, index) => (
                                        <TemplateItem key={index} title={name} data={data}
                                                      onClick={(data) => {
                                                          onChange({val: {servingHours: data}});
                                                      }}/>
                                    ))}
                                </TemplateContainer>
                                : null}
                        </ItemContainer>
                        <ItemContainer width={1} flexDirection={'column'} mt={3}>
                            <CallItems
                                phones={details[elLang.key] ? details[elLang.key].phones : []}
                                onChange={({phones}) => {
                                    onChange({val: {phones: phones}})
                                }}/>
                        </ItemContainer>
                        <ItemContainer width={1} flexDirection={'column'} mt={3}>
                            <Typography variant={'h6'} py={2} pr={2}>
                                توضیحات اضافه:
                            </Typography>
                            <DetailsText data={data}
                                         onChange={onChange}/>
                        </ItemContainer>
                    </Box>
                )
            })}

        </FormContainer>
    )
});


const callItemErrorList = [...errorList, 'ساختار شماره تلفن اشتباه است'];

function CallItems({phones = [], onChange, ...props}) {
    const theme = useTheme();
    const ref = useRef();

    const {enqueueSnackbar, closeSnackbar} = useSnackbar();
    const [state, setState] = useState({
        value: '',
        callMethod: 0,
        open: false,
        onEdit: null,
    });

    function handlePhoneAdd() {

        setTimeout(() => {
            if (ref.current.hasError()) {
                enqueueSnackbar('لطفا شماره تلفن را درست وارد کنید.',
                    {
                        variant: "error",
                        action: (key) => (
                            <Fragment>
                                <Button onClick={() => {
                                    closeSnackbar(key)
                                }}>
                                    {lang.get('close')}
                                </Button>
                            </Fragment>
                        )
                    });
                return;
            }
            const {phone} = ref.current.serialize();
            const ph = [...phones];
            if (state.onEdit) {
                ph.splice(state.onEdit.index, 0, {
                    id: state.onEdit.id,
                    type: state.callMethod === 0 ? 'mobile' : 'phone',
                    value: _.clone(phone),
                })
            } else {
                ph.push({
                    id: UtilsData.createId(phones),
                    type: state.callMethod === 0 ? 'mobile' : 'phone',
                    value: phone,
                });
            }
            onChange({phones: ph});
            setState({
                ...state,
                open: false,
                callMethod: 0,
                value: '',
                onEdit: null,
            })
        }, 1000);
    }

    function handlePhoneCancel() {
        setState({
            ...state,
            open: false,
            callMethod: 0,
            value: ''
        })
    }

    function handlePhoneEdit(phone, index) {
        const isMobile = phone.type === 'mobile';
        setState({
            open: true,
            callMethod: isMobile ? 0 : 1,
            value: phone.value,
            onEdit: {
                id: phone.id,
                index: index,
                callMethod: isMobile ? 0 : 1,
                value: phone.value,
            }
        });
        handlePhoneRemove(phone, index)
    }

    function handlePhoneRemove(phone, index) {
        const ph = [...phones];
        ph.splice(index, 1);
        onChange({phones: ph})
    }


    return (
        <Box display={'flex'} flexDirection={'column'} mt={3}>
            <Typography variant={'body1'}>
                {lang.get('call_numbers')}:
            </Typography>
            {!state.open ?
                <Box display={'flex'} mt={2}>
                    <BaseButton variant={"outlined"} size={"small"}
                                onClick={() => {
                                    setState({
                                        ...state,
                                        open: true
                                    })
                                }}
                                style={{borderColor: cyan[300], color: theme.palette.text.secondary}}>
                        <Add style={{marginLeft: theme.spacing(0.3)}}/>{lang.get('add')}
                    </BaseButton>
                </Box> : null}
            {state.open &&
            <Collapse in={state.open}>
                <Box display={'flex'} flexWrap={'wrap'}>
                    <Select
                        labelId="call method"
                        value={state.callMethod}
                        onChange={(e) => {
                            setState({
                                ...state,
                                callMethod: e.target.value
                            })
                        }}
                        style={{
                            marginTop: theme.spacing(1)
                        }}>
                        <MenuItem value={0}>
                            <Box display={'flex'} alignItems={'center'}>
                                <Phone fontSize={'small'} style={{marginLeft: theme.spacing(0.3)}}/>
                                موبایل
                            </Box>
                        </MenuItem>
                        <MenuItem value={1}>
                            <PhoneIphone fontSize={'small'} style={{marginLeft: theme.spacing(0.3)}}/>
                            تلفن
                        </MenuItem>
                    </Select>
                    <Box ml={2} mt={1}>
                        <FormControl name={'phoneForm'} innerref={ref}>
                            <TextFieldContainer
                                name={"phone"}
                                errorPatterns={[`^[0-9][0-9]*$`]}
                                defaultValue={state.value}
                                render={(ref, {name: inputName, initialize, valid, errorIndex, setValue, inputProps, style, props}) => {
                                    return (
                                        <TextField
                                            {...props}
                                            error={!valid}
                                            name={inputName}
                                            type={'number'}
                                            helperText={callItemErrorList[errorIndex]}
                                            inputRef={ref}
                                            required={true}
                                            label={'شماره'}
                                            style={{
                                                ...style,
                                                minWidth: 300
                                            }}
                                            inputProps={{
                                                ...inputProps
                                            }}/>
                                    )
                                }}/>
                        </FormControl>
                    </Box>
                    <Box mt={1} ml={2} display={'flex'} flexWrap={'wrap'} alignContent={'flex-end'}>
                        <BaseButton
                            onClick={handlePhoneAdd}
                            style={{
                                backgroundColor: cyan[300],
                                color: '#fff',
                            }}>
                            {lang.get('save')}
                        </BaseButton>
                        <BaseButton variant={"text"} size={'small'}
                                    onClick={handlePhoneCancel}
                                    style={{marginRight: theme.spacing(0.7)}}>
                            {lang.get('cancel')}
                        </BaseButton>
                    </Box>
                </Box>
            </Collapse>}
            <Box display={'flex'} flexDirection={'column'} mt={2}>
                <DraggableList
                    items={phones}
                    onItemsChange={(items) => {
                        onChange({phones: items});
                    }}
                    render={(item, props, {index}) => {
                        const isMobile = item.type === 'mobile';
                        return (
                            <Box key={item.id}
                                 display={'flex'}
                                 alignItems={'center'}
                                 py={1}
                                 px={1.5}
                                 my={1}
                                 {...props}
                                 style={{
                                     ...props.style,
                                     border: `1px solid ${cyan[300]}`,
                                     ...UtilsStyle.widthFitContent(),
                                     ...UtilsStyle.borderRadius(5)
                                 }}>
                                <Box
                                    display={'flex'}
                                    alignItems={'center'}>
                                    <Box minWidth={75} display={'flex'}>
                                        {isMobile ?
                                            <Phone fontSize={'small'} style={{marginLeft: theme.spacing(0.3)}}/> :
                                            <PhoneIphone fontSize={'small'}
                                                         style={{marginLeft: theme.spacing(0.3)}}/>}
                                        <Typography variant={'body1'} display={'flex'} alignItems={'center'} mr={0.7}>
                                            {isMobile ? 'موبایل' : 'تلفن'}
                                        </Typography>
                                    </Box>
                                    <Typography variant={'h6'} mr={1.5} fontWeight={500}>
                                        {item.value}
                                    </Typography>
                                </Box>
                                <Box display={'flex'} mr={1.5}>
                                    <IconButton size={"small"}
                                                onClick={() => {
                                                    handlePhoneEdit(item, index)
                                                }}
                                                style={{marginLeft: theme.spacing(0.5)}}>
                                        <Edit fontSize={"small"}/>
                                    </IconButton>
                                    <IconButton
                                        size={"small"}
                                        onClick={() => {
                                            handlePhoneRemove(item, index)
                                        }}>
                                        <Delete fontSize={"small"}/>
                                    </IconButton>
                                </Box>
                            </Box>
                        )
                    }}
                />
            </Box>
        </Box>
    )
}

function DetailsText({data: dd, onChange}) {
    const {val: details, el: elDetails} = dd;
    const [data, setData] = useState({
        init: false
    })
    const [timer, setTimer] = useState(null)
    useEffect(() => {
        const newData = data;
        _.forEach(sLang, (l, key) => {
            newData[key] = htmlToEditorContent((details[key] && details[key].text) ? details[key].text : '')
        });
        setData({
            ...newData,
            init: true
        });
        return () => {
            clearTimeout(timer);
        }
    }, [])


    function handlerTextChange(content) {
        const newData = data;
        newData[elDetails.activeLanguage.key] = content;
        setData({
            ...newData
        });

        if (timer != null)
            clearTimeout(timer);
        setTimer(setTimeout(() => {
            onChange({
                val: {
                    text: content
                }
            })
        }, 4000))
    }

    return (
        <React.Fragment>
            {data.init &&
            <Editor
                mx={2}
                minHeight={200}
                content={data[elDetails.activeLanguage.key]}
                onContentChange={handlerTextChange}
                style={{
                    border: `1px solid ${grey[400]}`,
                    overflow: 'auto',
                    ...UtilsStyle.borderRadius(5),
                }}/>
            }
        </React.Fragment>
    )
}

//endregion Details

//region Properties

const Properties = React.memo(function Properties({box, data, onChange, ...props}) {
    const {val: properties, el: elProperties} = data;
    const theme = useTheme();

    const {enqueueSnackbar, closeSnackbar} = useSnackbar();
    const [state, setState] = useState({
        onEdit: false,
        defIndex: null,
        defId: null,
        defType: null,
        type: 'property',
        defIcon: null,
        icon: 'default',
        defPriority: null,
        priority: 'high',
        defText: null,
        text: '',
        selfValue: true
    });
    const [actions, setActions] = useState({
        add: false,
        addFromArchive: false,
        edit: false,
    });
    const [propertyHigh, setPropertyHigh] = useState([]);
    const [propertyMedium, setPropertyMedium] = useState([]);
    const [propertyLow, setPropertyLow] = useState([]);
    const [usageConditionHigh, setUsageConditionHigh] = useState([]);
    const [usageConditionMedium, setUsageConditionMedium] = useState([]);
    const [usageConditionLow, setUsageConditionLow] = useState([]);


    useEffect(() => {
        initialize()
    }, [elProperties.activeLanguage.key]);

    useEffect(() => {
        const newData = {...properties};
        newData[elProperties.activeLanguage.key] = {
            property: [
                ...propertyHigh,
                ...propertyMedium,
                ...propertyLow,
            ],
            usageCondition: [
                ...usageConditionHigh,
                ...usageConditionMedium,
                ...usageConditionLow,
            ]
        };

        onChange({
            val: {...newData}
        })
    }, [propertyHigh, propertyMedium, propertyLow, usageConditionHigh, usageConditionMedium, usageConditionLow]);


    function initialize(pries) {
        if (!pries) {
            pries = properties
        }

        const newPropertyHigh = propertyHigh;
        const newPropertyMedium = propertyMedium;
        const newPropertyLow = propertyLow;

        const newUsageConditionHigh = usageConditionHigh;
        const newUsageConditionMedium = usageConditionMedium;
        const newUsageConditionLow = usageConditionLow;
        if (pries[elProperties.activeLanguage.key]) {
            if (pries[elProperties.activeLanguage.key].property) {
                _.forEach(pries[elProperties.activeLanguage.key].property, (p) => {
                    switch (p.priority) {
                        case  'high': {
                            newPropertyHigh.push({
                                ...p,
                                id: UtilsData.createId(newPropertyHigh)
                            });
                            break;
                        }
                        case 'medium': {
                            newPropertyMedium.push({
                                ...p,
                                id: UtilsData.createId(newPropertyMedium)
                            });
                            break;
                        }
                        default: {
                            newPropertyLow.push({
                                ...p,
                                id: UtilsData.createId(newPropertyLow)
                            });
                        }
                    }
                })
            }

            if (!pries[elProperties.activeLanguage.key].usageCondition && pries[elProperties.activeLanguage.key].usage_condition) {
                pries[elProperties.activeLanguage.key].usageCondition = pries[elProperties.activeLanguage.key].usage_condition;
            }
            if (pries[elProperties.activeLanguage.key].usageCondition) {
                _.forEach(pries[elProperties.activeLanguage.key].usageCondition, (p) => {
                    switch (p.priority) {
                        case  'high': {
                            newUsageConditionHigh.push({
                                ...p,
                                id: UtilsData.createId(newUsageConditionHigh)
                            });
                            break;
                        }
                        case 'medium': {
                            newUsageConditionMedium.push({
                                ...p,
                                id: UtilsData.createId(newUsageConditionMedium)
                            });
                            break;
                        }
                        default: {
                            newUsageConditionLow.push({
                                ...p,
                                id: UtilsData.createId(newUsageConditionLow)
                            });
                        }
                    }
                })
            }
        }

        setPropertyHigh([...newPropertyHigh]);
        setPropertyMedium([...newPropertyMedium]);
        setPropertyLow([...newPropertyLow]);
        setUsageConditionHigh([...newUsageConditionHigh]);
        setUsageConditionMedium([...newUsageConditionMedium]);
        setUsageConditionLow([...newUsageConditionLow]);
    }

    function resetState() {
        setState({
            ...state,
            onEdit: false,
            defId: null,
            defIndex: null,
            defType: null,
            type: 'property',
            defIcon: null,
            icon: 'default',
            defPriority: null,
            priority: 'high',
            defText: null,
            text: '',
        })
    }

    function itemsChangeHandler({key, items}) {
        switch (key) {
            case 'propertyHigh': {
                setPropertyHigh([...items]);
                break;
            }
            case 'propertyMedium': {
                setPropertyMedium([...items]);
                break;
            }
            case 'propertyLow': {
                setPropertyLow([...items]);
                break;
            }
            case 'usageConditionHigh': {
                setUsageConditionHigh([...items]);
                break;
            }
            case 'usageConditionMedium': {
                setUsageConditionMedium([...items]);
                break;
            }
            case 'usageConditionLow': {
                setUsageConditionLow([...items]);
                break;
            }
            default: {

            }
        }
    }

    function itemSaveHandler() {
        setTimeout(() => {
            if (_.isEmpty(state.text)) {
                enqueueSnackbar('متن ویژگی را کامل کنید.',
                    {
                        variant: "error",
                        action: (key) => (
                            <Fragment>
                                <Button onClick={() => {
                                    closeSnackbar(key)
                                }}>
                                    {lang.get('close')}
                                </Button>
                            </Fragment>
                        )
                    });
                return;
            }
            let data = null;
            let type = null;

            switch (state.type) {
                case 'property': {
                    type = 'property';
                    switch (state.priority) {
                        case 'high': {
                            type = type + 'High';
                            data = propertyHigh;
                            break;
                        }
                        case 'medium': {
                            type = type + 'Medium';
                            data = propertyMedium;
                            break;
                        }
                        case 'low': {
                            type = type + 'Low';
                            data = propertyLow;
                            break;
                        }
                        default: {
                            break;
                        }
                    }
                    break;
                }
                case 'usageCondition': {
                    type = 'usageCondition';
                    switch (state.priority) {
                        case 'high': {
                            type = type + 'High';
                            data = usageConditionHigh;
                            break;
                        }
                        case 'medium': {
                            type = type + 'Medium';
                            data = usageConditionMedium;
                            break;
                        }
                        case 'low': {
                            type = type + 'Low';
                            data = usageConditionLow;
                            break;
                        }
                        default: {
                            break;
                        }
                    }
                    break;
                }
                default: {
                    break;
                }
            }
            if (data === null)
                return;
            if (state.id && type === state.defType + _.startCase(state.defPriority)) {
                data.splice(state.defIndex, 0, convertProperty({
                    id: state.defId,
                    text: state.text,
                    type: state.icon,
                    priority: state.priority
                }));
            } else {
                data.push(convertProperty({
                    id: UtilsData.createId(data),
                    text: state.text,
                    type: state.icon,
                    priority: state.priority
                }))
            }

            resetState();
            itemsChangeHandler({key: type, items: data});
        }, 1000)
    }

    function cancelHandler() {
        setTimeout(() => {
            if (state.defIndex === null) {
                resetState();
                return;
            }
            let data = null;
            let type = null;
            switch (state.defType) {
                case 'property': {
                    type = 'property';
                    switch (state.defPriority) {
                        case 'high': {
                            type = type + 'High';
                            data = propertyHigh;
                            break;
                        }
                        case 'medium': {
                            type = type + 'Medium';
                            data = propertyMedium;
                            break;
                        }
                        case 'low': {
                            type = type + 'Low';
                            data = propertyLow;
                            break;
                        }
                        default: {
                            break;
                        }
                    }
                    break;
                }
                case 'usageCondition': {
                    type = 'usageCondition';
                    switch (state.defPriority) {
                        case 'high': {
                            type = type + 'High';
                            data = usageConditionHigh;
                            break;
                        }
                        case 'medium': {
                            type = type + 'Medium';
                            data = usageConditionMedium;
                            break;
                        }
                        case 'low': {
                            type = type + 'Low';
                            data = usageConditionLow;
                            break;
                        }
                        default: {
                            break;
                        }
                    }
                    break;
                }
                default: {
                    break;
                }
            }

            if (data === null && !_.isArray(data)) {
                resetState()
                return;
            }
            data.splice(state.defIndex, 0, convertProperty({
                id: state.defId,
                text: state.defText,
                type: state.defIcon,
                priority: state.defPriority
            }));

            resetState();
            itemsChangeHandler({key: type, items: data});
        }, 1000);
    }

    function removeItem({key, item}) {
        switch (key) {
            case 'propertyHigh': {
                const newData = propertyHigh;
                _.remove(newData, (d) => {
                    return d.id === item.id
                });
                setPropertyHigh([...newData]);
                break;
            }
            case 'propertyMedium': {
                const newData = propertyMedium;
                _.remove(newData, (d) => {
                    return d.id === item.id
                });
                setPropertyMedium([...newData]);
                break;
            }
            case 'propertyLow': {
                const newData = propertyLow;
                _.remove(newData, (d) => {
                    return d.id === item.id
                });
                setPropertyLow([...newData]);
                break;
            }
            case 'usageConditionHigh': {
                const newData = usageConditionHigh;
                _.remove(newData, (d) => {
                    return d.id === item.id
                });
                setUsageConditionHigh([...newData]);
                break;
            }
            case 'usageConditionMedium': {
                const newData = usageConditionMedium;
                _.remove(newData, (d) => {
                    return d.id === item.id
                });
                setUsageConditionMedium([...newData]);
                break;
            }
            case 'usageConditionLow': {
                const newData = usageConditionLow;
                _.remove(newData, (d) => {
                    return d.id === item.id
                });
                setUsageConditionLow([...newData]);
                break;
            }
            default: {
            }
        }
    }

    function onEditHandler({item, index, type}) {
        setActions({
            ...actions,
            edit: _.cloneDeep(item)
        });
        // const i = item;
        // setState({
        //     ...state,
        //     onEdit: true,
        //     defIndex: index,
        //     defId: i.id,
        //     id: i.id,
        //     defType: type,
        //     type: type,
        //     defPriority: i.priority,
        //     priority: i.priority,
        //     defText: i.text,
        //     text: i.text,
        //     selfValue: i.text
        // });
        removeItem({key: type + _.startCase(item.priority), item: item})
    }

    const editDisable = (actions.edit || (state.onEdit && state.defId !== null));
    return (
        <FormContainer open={elProperties.open} active={elProperties.active}
                       title={lang.get("properties")}
                       activeLanguageValue={properties}
                       activeLanguage={elProperties.activeLanguage}
                       onLanguageChange={(lang) => {
                           onChange({el: {...elProperties, activeLanguage: lang}})
                       }}
                       onOpenClick={(e, open) => {
                           onChange({el: {...elProperties, open: open}})
                       }}>
            <Box display={'flex'} px={2} py={1} flexDirection={'column'}>
                <Box mt={1} display={'flex'}>
                    <BaseButton
                        variant={"outlined"}
                        onClick={() => {
                            setActions({
                                ...actions,
                                add: true
                            })
                        }}
                        style={{
                            borderColor: cyan[300]
                        }}>
                        افرودن ویژگی جدید
                    </BaseButton>
                    <BaseButton
                        variant={"outlined"}
                        onClick={() => {
                            setActions({
                                ...actions,
                                addFromArchive: true
                            })
                        }}
                        style={{
                            marginRight: theme.spacing(1),
                            borderColor: cyan[300]
                        }}>
                        افرودن ویژگی پیشفرض
                    </BaseButton>
                </Box>
                {false &&
                <Box mt={1}>
                    {!state.onEdit ?
                        <Box display={'flex'}>
                            <BaseButton
                                variant={"outlined"}
                                onClick={() => {
                                    setState({
                                        ...state,
                                        onEdit: true
                                    })
                                }}
                                style={{
                                    borderColor: cyan[300]
                                }}>
                                افرودن ویژگی جدید
                            </BaseButton>
                            <BaseButton
                                variant={"outlined"}
                                onClick={() => {
                                    setState({
                                        ...state,
                                        onEdit: true
                                    })
                                }}
                                style={{
                                    marginRight: theme.spacing(1),
                                    borderColor: cyan[300]
                                }}>
                                افرودن ویژگی پیشفرض
                            </BaseButton>
                        </Box> :
                        <Box display={'flex'} flexDirection={'column'} component={Card} pt={1} pb={2} px={2}>
                            <Typography variant={'subtitle1'} fontWeight={300} mt={1} mb={2}>
                                افزودن ویژگی:
                            </Typography>
                            <Box display={'flex'} flexWrap={"wrap"}>
                                <Box display={'flex'} flexDirection={'column'}>
                                    <Typography variant={'subtitle2'} fontWeight={300}>
                                        نوع ویژگی:
                                    </Typography>
                                    <Select
                                        labelId="select-type"
                                        value={state.type}
                                        onChange={(event) => {
                                            setState({
                                                ...state,
                                                type: event.target.value
                                            })
                                        }}
                                        style={{
                                            marginTop: theme.spacing(1),
                                        }}>
                                        <MenuItem value={'property'}>
                                            {lang.get('properties')}
                                        </MenuItem>
                                        <MenuItem value={'usageCondition'}>
                                            {lang.get('usage_condition')}
                                        </MenuItem>
                                    </Select>
                                </Box>
                                <Box display={'flex'} flexDirection={'column'} mr={3}>
                                    <Typography variant={'subtitle2'} fontWeight={300}>
                                        اولویت:
                                    </Typography>
                                    <Select
                                        labelId="priority-icon"
                                        value={state.priority}
                                        onChange={(event) => {
                                            setState({
                                                ...state,
                                                priority: event.target.value
                                            })
                                        }}
                                        style={{
                                            marginTop: theme.spacing(1),
                                        }}>
                                        <MenuItem value={'high'}>
                                            بالا
                                        </MenuItem>
                                        <MenuItem value={'medium'}>
                                            متوسط
                                        </MenuItem>
                                        <MenuItem value={'low'}>
                                            پایین
                                        </MenuItem>
                                    </Select>
                                </Box>
                                <Box display={'flex'} flexDirection={'column'} mr={3}>
                                    <Typography variant={'subtitle2'} fontWeight={300}>
                                        آیکون:
                                    </Typography>
                                    <Select
                                        labelId="select-icon"
                                        value={state.icon}
                                        onChange={(event) => {
                                            setState({
                                                ...state,
                                                icon: event.target.value
                                            })
                                        }}
                                        style={{
                                            marginTop: theme.spacing(1),
                                        }}>
                                        <MenuItem value={'default'}>
                                            <Gem2
                                                gemColor={state.type !== 'property' ? orange[600] : green[500]}
                                                style={{
                                                    width: 15
                                                }}/>
                                        </MenuItem>
                                        <MenuItem value={'date'}>
                                            <DateRange
                                                style={{color: state.type !== 'property' ? orange[600] : green[500]}}/>
                                        </MenuItem>
                                        <MenuItem value={'phone'}>
                                            <PhoneEnabled
                                                style={{color: state.type !== 'property' ? orange[600] : green[500]}}/>
                                        </MenuItem>
                                    </Select>
                                </Box>
                                <Box display={'flex'} flexDirection={'column'} mr={3}>
                                    <TextField
                                        value={state.text}
                                        label={'متن'}
                                        optionalLabel={''}
                                        onChange={(text) => setState({
                                            ...state,
                                            text: text.target.value
                                        })}
                                        style={{
                                            width: 300
                                        }}
                                    />
                                </Box>
                                <Box mt={1} mr={2} display={'flex'} flexWrap={'wrap'} alignContent={'flex-end'}>
                                    <BaseButton
                                        onClick={itemSaveHandler}
                                        style={{
                                            backgroundColor: cyan[300],
                                            color: '#fff',
                                        }}>
                                        {lang.get('save')}
                                    </BaseButton>
                                    <BaseButton variant={"text"} size={'small'}
                                                onClick={cancelHandler}
                                                style={{marginRight: theme.spacing(0.7)}}>
                                        {lang.get('cancel')}
                                    </BaseButton>
                                </Box>
                            </Box>
                        </Box>
                    }
                </Box>
                }
                <Box display={'flex'} mt={2}>
                    <Box display={'flex'} flexDirection={'column'} width={0.5} pr={1}>
                        <Typography variant={'subtitle1'} mb={0.7}>
                            {lang.get("usage_condition")}
                        </Typography>
                        {!_.isEmpty(usageConditionHigh) &&
                        <Box display={'flex'} flexDirection={'column'} mt={2}>
                            <Typography variant={'subtitle2'} color={orange[600]}>
                                {lang.get("high_priority")}:
                            </Typography>
                            <DraggableList
                                items={usageConditionHigh}
                                onItemsChange={(items) => {
                                    itemsChangeHandler({key: "usageConditionHigh", items: items})
                                }}
                                render={(item, draggableListProps, {index}) => (
                                    <Box {...draggableListProps}>
                                        <PropertiesItem
                                            key={index}
                                            item={item}
                                            disableEdit={editDisable}
                                            onRemove={() => removeItem({key: "usageConditionHigh", item: item})}
                                            onEdit={() => onEditHandler({
                                                item: item,
                                                index: index,
                                                type: 'usageCondition'
                                            })}
                                            isUsageCondition={true}/>
                                    </Box>
                                )}/>
                        </Box>
                        }
                        {!_.isEmpty(usageConditionMedium) &&
                        <Box display={'flex'} flexDirection={'column'} mt={2}>
                            <Typography variant={'subtitle2'} color={orange[600]}>
                                {lang.get("medium_priority")}:
                            </Typography>

                            <DraggableList
                                items={usageConditionMedium}
                                onItemsChange={(items) => {
                                    itemsChangeHandler({key: "usageConditionMedium", items: items})
                                }}
                                render={(item, draggableListProps, {index}) => (
                                    <Box {...draggableListProps}>
                                        <PropertiesItem
                                            key={index}
                                            item={item}
                                            disableEdit={editDisable}
                                            onRemove={() => removeItem({key: "usageConditionMedium", item: item})}
                                            onEdit={() => onEditHandler({
                                                item: item,
                                                index: index,
                                                type: 'usageCondition'
                                            })}
                                            isUsageCondition={true} {...props}/>
                                    </Box>
                                )}/>
                        </Box>
                        }

                        {!_.isEmpty(usageConditionLow) &&
                        <Box display={'flex'} flexDirection={'column'} mt={2}>
                            <Typography variant={'subtitle2'} color={orange[600]}>
                                {lang.get("low_priority")}:
                            </Typography>
                            <DraggableList
                                items={usageConditionLow}
                                onItemsChange={(items) => {
                                    itemsChangeHandler({key: "usageConditionLow", items: items})
                                }}
                                render={(item, draggableListProps, {index}) => (
                                    <Box {...draggableListProps}>
                                        <PropertiesItem
                                            key={index}
                                            item={item}
                                            disableEdit={editDisable}
                                            onRemove={() => removeItem({key: "usageConditionLow", item: item})}
                                            onEdit={() => onEditHandler({
                                                item: item,
                                                index: index,
                                                type: 'usageCondition'
                                            })}
                                            isUsageCondition={true} {...props}/>
                                    </Box>
                                )}/>
                        </Box>
                        }

                    </Box>
                    <Box display={'flex'} flexDirection={'column'} width={0.5} pl={1}>
                        <Typography variant={'subtitle1'} mb={0.7}>
                            {lang.get("properties")}
                        </Typography>
                        {!_.isEmpty(propertyHigh) &&
                        <Box display={'flex'} flexDirection={'column'} mt={2}>
                            <Typography variant={'subtitle2'} color={green[500]}>
                                {lang.get("high_priority")}:
                            </Typography>
                            <DraggableList
                                items={propertyHigh}
                                onItemsChange={(items) => {
                                    itemsChangeHandler({key: "propertyHigh", items: items})
                                }}
                                render={(item, draggableListProps, {index}) => (
                                    <Box {...draggableListProps}>
                                        <PropertiesItem
                                            key={index}
                                            item={item}
                                            disableEdit={editDisable}
                                            onRemove={() => removeItem({key: "propertyHigh", item: item})}
                                            onEdit={() => onEditHandler({
                                                item: item,
                                                index: index,
                                                type: 'property'
                                            })}
                                            isUsageCondition={false} {...props}/>
                                    </Box>
                                )}/>
                        </Box>
                        }
                        {!_.isEmpty(propertyMedium) &&
                        <Box display={'flex'} flexDirection={'column'} mt={2}>
                            <Typography variant={'subtitle2'} color={green[500]}>
                                {lang.get("medium_priority")}:
                            </Typography>
                            <DraggableList
                                items={propertyMedium}
                                onItemsChange={(items) => {
                                    itemsChangeHandler({key: "propertyMedium", items: items})
                                }}
                                render={(item, draggableListProps, {index}) => (
                                    <Box {...draggableListProps}>
                                        <PropertiesItem
                                            key={index}
                                            item={item}
                                            disableEdit={editDisable}
                                            onRemove={() => removeItem({key: "propertyMedium", item: item})}
                                            onEdit={() => onEditHandler({
                                                item: item,
                                                index: index,
                                                type: 'property'
                                            })}
                                            isUsageCondition={false} {...props}/>
                                    </Box>
                                )}/>
                        </Box>
                        }

                        {!_.isEmpty(propertyLow) &&
                        <Box display={'flex'} flexDirection={'column'} mt={2}>
                            <Typography variant={'subtitle2'} color={green[500]}>
                                {lang.get("low_priority")}:
                            </Typography>
                            <DraggableList
                                items={propertyLow}
                                onItemsChange={(items) => {
                                    itemsChangeHandler({key: "propertyLow", items: items})
                                }}
                                render={(item, draggableListProps, {index}) => (
                                    <Box {...draggableListProps}>
                                        <PropertiesItem
                                            key={index}
                                            item={item}
                                            disableEdit={editDisable}
                                            onRemove={() => removeItem({key: "propertyLow", item: item})}
                                            onEdit={() => onEditHandler({
                                                item: item,
                                                index: index,
                                                type: 'property'
                                            })}
                                            isUsageCondition={false} {...props}/>
                                    </Box>
                                )}/>
                        </Box>
                        }
                    </Box>
                </Box>
            </Box>
            <CreateProperty open={actions.add || actions.edit}
                            item={actions.edit ? actions.edit : undefined}
                            saveToServer={false}
                            onClose={(data) => {
                                if (actions.edit) {
                                    setActions({
                                        ...actions,
                                        edit: false
                                    });
                                    try {
                                        const item = !_.isEmpty(data.fa.property) ? data.fa.property[0] : data.fa.usage_condition[0];
                                        if (!item) {
                                            return;
                                        }
                                        if (item.priority === "high") {
                                            if (item.type === "property") {
                                                setPropertyHigh([...propertyHigh, {
                                                    ...item,
                                                    id: UtilsData.createId(propertyHigh)
                                                }]);
                                            } else {
                                                setUsageConditionHigh([...usageConditionHigh, {
                                                    ...item,
                                                    id: UtilsData.createId(usageConditionHigh)
                                                }]);
                                            }
                                        } else if (item.priority === "medium") {
                                            if (item.type === "property") {
                                                setPropertyMedium([...propertyMedium, {
                                                    ...item,
                                                    id: UtilsData.createId(propertyMedium)
                                                }]);
                                            } else {
                                                setUsageConditionMedium([...usageConditionMedium, {
                                                    ...item,
                                                    id: UtilsData.createId(usageConditionMedium)
                                                }]);
                                            }
                                        } else {
                                            if (item.type === "property") {
                                                setPropertyLow([...propertyLow, {
                                                    ...item,
                                                    id: UtilsData.createId(propertyLow)
                                                }]);

                                            } else {
                                                setUsageConditionLow([...usageConditionLow, {
                                                    ...item,
                                                    id: UtilsData.createId(usageConditionLow)
                                                }]);
                                            }
                                        }
                                    } catch (e) {
                                    }
                                    return
                                }
                                setActions({
                                    ...actions,
                                    add: false
                                });
                                initialize(data);
                            }}/>
            <SelectProperty
                open={actions.addFromArchive}
                boxId={box.id}
                select={true}
                multi={true}
                add={false}
                onClose={(data) => {
                    const dd = data;
                    _.forEach(data, (d, key) => {
                        data[key] = {
                            ...d,
                            usageCondition: d.usage_condition
                        }
                    })
                    initialize(dd);
                    setActions({
                        ...actions,
                        addFromArchive: false
                    })
                }}/>
        </FormContainer>
    )
});


function PropertiesItem({item, disableEdit, onEdit, onRemove, isUsageCondition, index, onDragOver, onDragStart, onDragEnd, ...props}) {
    const theme = useTheme();

    const {enqueueSnackbar, closeSnackbar} = useSnackbar();

    return (
        <Box py={1}>
            <Box
                py={0.7} px={1}
                display={'flex'}
                alignItems={'center'}
                style={{
                    border: `1px solid ${isUsageCondition ? orange[600] : green[500]}`,
                    ...UtilsStyle.borderRadius(5),
                    ...UtilsStyle.transition(),
                }}
                {...props}>
                <Box display={'flex'}>
                    <Tooltip title={lang.get("edit")}>
                        <IconButton size={'small'} onClick={(e) => {
                            if (disableEdit) {
                                enqueueSnackbar('یک ویژگی در حال ادیت میاشد.',
                                    {
                                        variant: "error",
                                        action: (key) => (
                                            <Fragment>
                                                <Button onClick={() => {
                                                    closeSnackbar(key)
                                                }}>
                                                    {lang.get('close')}
                                                </Button>
                                            </Fragment>
                                        )
                                    });
                                return
                            }
                            onEdit(e)
                        }}
                                    style={{marginLeft: theme.spacing(0.5)}}>
                            <Edit fontSize={"small"} style={{color: cyan[200]}}/>
                        </IconButton>
                    </Tooltip>
                    <Tooltip title={lang.get("delete")}>
                        <IconButton size={'small'} onClick={onRemove}>
                            <Delete fontSize={"small"} style={{color: red[200]}}/>
                        </IconButton>
                    </Tooltip>
                </Box>
                <Box px={2} height={60}>
                    <Icon
                        height={60}
                        width={60}
                        alt={item.icon}
                        showSkeleton={false}
                        src={convertIconUrl(item.icon)}/>
                </Box>
                {/*<PropertiesIcon isUsageCondition={isUsageCondition} type={item.type}/>*/}
                <Typography
                    variant={!item.isLow ? 'subtitle1' : 'subtitle2'}
                    fontWeight={item.isHigh ? '400' : item.isMedium ? '300' : '200'}>
                    {item.text}
                </Typography>
            </Box>
        </Box>
    )
}


function PropertiesIcon({isUsageCondition, type, ...props}) {
    const color = isUsageCondition ? orange[600] : green[500];
    return (
        <Box width={35} height={35} display={'flex'}
             justifyContent={'center'}
             alignItems={'center'}>
            {
                type === 'date' ?
                    <DateRange style={{color: color}}/>
                    :
                    type === 'phone' ?
                        <PhoneEnabled style={{color: color}}/>
                        :
                        <Gem2
                            gemColor={color}
                            style={{
                                width: 15
                            }}
                            {...props}/>
            }
        </Box>
    )
}

//endregion Properties

//region Tags

const useTagsStyle = makeStyles(theme => ({
    root: {
        backgroundColor: cyan[100],
        color: grey[700],
        '&:focus': {
            backgroundColor: cyan[100],
            color: grey[700],
        },
    }
}));

const Tags = React.memo(function Tags({box, data: dd, onChange, ...props}) {
    const {val: tags, el: elTags} = dd;
    const theme = useTheme();

    const {enqueueSnackbar, closeSnackbar} = useSnackbar();
    const [setting, setSetting] = useState({
        openAddTagDialog: false,
        dialogValue: '',
        dialogPermalink: '',
        selectGpTag: false,
    });
    const [defValue, setDefValue] = useState("");
    const [value, setValue] = useState("");
    const classes = useTagsStyle(props);

    const d = ControllerProduct.Product.Tags.search({query: value});
    const apiKey = d[0] + value;
    const {data: da, error} = useSWR(apiKey, () => {
        return d[1]()
    });
    const {tags: data} = da ? da.data : [];

    useEffect(() => {
        setValue("");
        setDefValue("");
    }, []);

    function handleAdd(d, show) {
        const newTags = tags;
        const newList = [];
        if (show) {
            const i = _.findIndex(tags, (t) => t.id === d.id);
            if (i === -1)
                return;
            newTags[i] = d;
        } else {
            if (!_.isArray(d))
                d = [d];
            let notAdded = 0;
            let showTag = 0;
            _.forEach(tags, (t) => {
                showTag = (t.show ? 1 : 0) + showTag
            })

            _.forEach(d, (tag) => {
                const i = _.findIndex(tags, (t) => {
                    return tag.id === t.id;
                })
                if (i !== -1) {
                    notAdded = notAdded + 1;
                    return
                }
                if (tag.show) {
                    if (maxShowableTagCount <= showTag) {
                        tag.show = false;
                    }
                    showTag = showTag + 1;
                }
                newList.push(tag)
            })
            if (notAdded > 0) {
                enqueueSnackbar(notAdded + ' انتخاب شده است.',
                    {
                        variant: "warning",
                        action: (key) => (
                            <Fragment>
                                <Button onClick={() => {
                                    closeSnackbar(key)
                                }}>
                                    {lang.get('close')}
                                </Button>
                            </Fragment>
                        )
                    });
            }
            if (_.isEmpty(newList))
                return
        }

        onChange({val: [...newTags, ...newList]})
    }

    function handleDelete(d) {
        const newData = tags;
        _.remove(newData, (t) => {
            return t.id === d.id
        });
        onChange({val: [...newData]})
    }

    function handleAddNewTag() {
        if (!_.isEmpty(data)) {
            const i = _.findIndex(data, (t) => {
                return t.name.fa === value
            });
            if (i !== -1) {
                enqueueSnackbar('این تگ موجود میباشد.',
                    {
                        variant: "error",
                        action: (key) => (
                            <Fragment>
                                <Button onClick={() => {
                                    closeSnackbar(key)
                                }}>
                                    {lang.get('close')}
                                </Button>
                            </Fragment>
                        )
                    });
                return;
            }
        }


        setSetting({
            ...setting,
            openAddTagDialog: true,
            dialogPermalink: '',
            dialogValue: value
        })

        // enqueueSnackbar('فزوده شد.',
        //     {
        //         variant: "success",
        //         action: (key) => (
        //             <Fragment>
        //                 <Button onClick={() => {
        //                     closeSnackbar(key)
        //                 }}>
        //                     {lang.get('close')}
        //                 </Button>
        //             </Fragment>
        //         )
        //     });
    }

    function handleCloseDialog(save) {

        if (save) {
            if (!setting.dialogPermalink || !setting.dialogValue) {
                enqueueSnackbar('تمام فیلدها را پرکنید.',
                    {
                        variant: "error",
                        action: (key) => (
                            <Fragment>
                                <Button onClick={() => {
                                    closeSnackbar(key)
                                }}>
                                    {lang.get('close')}
                                </Button>
                            </Fragment>
                        )
                    });
                return;
            }
            ControllerProduct.Product.Tags.set({
                permalink: setting.dialogPermalink,
                name: createMultiLanguage({fa: setting.dialogValue})
            }).then(res => {
                res.item.hasActive = true;
                handleAdd(res.item);
                enqueueSnackbar('افزوده شد.',
                    {
                        variant: "success",
                        action: (key) => (
                            <Fragment>
                                <Button onClick={() => {
                                    closeSnackbar(key)
                                }}>
                                    {lang.get('close')}
                                </Button>
                            </Fragment>
                        )
                    });
                handleCloseDialog(false)
                mutate(apiKey);
            });
            return;
        }


        setSetting({
            ...setting,
            openAddTagDialog: false,
            dialogPermalink: ''
        })
    }

    return (
        <React.Fragment>
            <FormContainer open={elTags.open}
                           title={lang.get("tags")}
                           onLanguageChange={(lang) => {
                               onChange({el: {...elTags, activeLanguage: lang}});
                           }}
                           onOpenClick={(e, open) => {
                               onChange({el: {...elTags, open: open}});
                           }}>
                <Box display={'flex'} px={2} py={1} flexDirection={'column'}>
                    {!error ?
                        <Box display={'flex'} justifyContent={'center'}>
                            <Box width={0.7} mt={2} mb={2}>
                                <Box display={'flex'} flexDirection={'column'} mb={3}>
                                    <Typography variant={'body1'} pb={1} fontWeight={400}>
                                        تگ‌های فعال:
                                    </Typography>
                                    <Box display={'flex'} flexWrap={'wrap'}>
                                        {
                                            tags.map((t, i) => (
                                                <Box key={i} ml={2} mb={1.5}>
                                                    <Chip
                                                        avatar={t.show ? <VisibilityOutlined/> :
                                                            <VisibilityOffOutlined/>}
                                                        clickable
                                                        onClick={() => {
                                                            if (!t.show) {
                                                                let showCount = 0
                                                                _.forEach(tags, t => {
                                                                    showCount = (t.show ? 1 : 0) + showCount
                                                                })
                                                                if (showCount >= maxShowableTagCount) {
                                                                    enqueueSnackbar("حداکثر تعداد قابل نمایش " + maxShowableTagCount + " میباشد.",
                                                                        {
                                                                            variant: "error",
                                                                            action: (key) => (
                                                                                <Fragment>
                                                                                    <Button onClick={() => {
                                                                                        closeSnackbar(key)
                                                                                    }}>
                                                                                        {lang.get('close')}
                                                                                    </Button>
                                                                                </Fragment>
                                                                            )
                                                                        });
                                                                    return
                                                                }
                                                            }
                                                            handleAdd({
                                                                ...t,
                                                                show: !t.show
                                                            }, true)
                                                        }}
                                                        className={classes.root}
                                                        label={t.name.fa}
                                                        onDelete={() => handleDelete(t)}
                                                        style={{
                                                            backgroundColor: !t.show ? cyan[100] : green[100]
                                                        }}/>
                                                </Box>
                                            ))
                                        }
                                    </Box>
                                </Box>
                                <Box display={'flex'} width={1} alignItems={'center'} justifyContent={'center'}>
                                    <Box flex={1} pr={4}>
                                        <TextFieldContainer
                                            name={createName({name: "tag-name"})}
                                            defaultValue={defValue}
                                            onChange={(val) => {
                                                setValue(val)
                                                setDefValue(val)
                                            }}
                                            onChangeDelay={800}
                                            render={(ref, {name: inputName, initialize, valid, errorIndex, setValue, inputProps, style, props}) => {
                                                return (
                                                    <TextField
                                                        {...props}
                                                        variant={'outlined'}
                                                        error={!valid}
                                                        name={inputName}
                                                        inputRef={ref}
                                                        fullWidth
                                                        endAdornment={value && (
                                                            <IconButton onClick={() => {
                                                                setDefValue('');
                                                                setValue("");
                                                            }}>
                                                                <CancelOutlined/>
                                                            </IconButton>
                                                        )}
                                                        label={'جست‌و‌جو تگ'}
                                                        placeholder={'نام تگ'}
                                                        style={{
                                                            ...style,
                                                            minWidth: '90%'
                                                        }}
                                                        inputProps={{
                                                            ...inputProps,
                                                        }}/>
                                                )
                                            }}/>
                                    </Box>
                                    <Box display={'flex'} alignItems={'center'}>
                                        <BaseButton
                                            disabled={value.length < 2}
                                            size={"medium"}
                                            style={{
                                                backgroundColor: cyan[300],
                                                color: '#fff'
                                            }}
                                            onClick={handleAddNewTag}>
                                            {lang.get("add")}
                                        </BaseButton>
                                        <Box pl={2} display={'flex'}>
                                            <BaseButton
                                                size={"medium"}
                                                style={{
                                                    backgroundColor: cyan[300],
                                                    color: '#fff'
                                                }}
                                                onClick={() => {
                                                    setSetting({
                                                        ...setting,
                                                        selectGpTag: true
                                                    })
                                                }}>
                                                گروه تگ‌ها
                                            </BaseButton>
                                        </Box>
                                    </Box>
                                </Box>
                                <Box mt={2} component={Card}>
                                    {

                                        data ?
                                            <Collapse in={!_.isEmpty(data)}>
                                                <Box display={'flex'} flexWrap={'wrap'} py={2} px={2}>
                                                    {
                                                        !_.isEmpty(data) && data.map((d, i) => (
                                                            <Box key={i} ml={2} mb={1.5}>
                                                                <Chip
                                                                    label={d.name.fa}
                                                                    disabled={_.findIndex(tags, (t) => {
                                                                        return d.permalink === t.permalink;
                                                                    }) !== -1}
                                                                    onClick={() => handleAdd(d)}
                                                                    style={{
                                                                        color: d.hasActive ? theme.palette.action.hover : null
                                                                    }}/>
                                                            </Box>
                                                        ))
                                                    }
                                                </Box>
                                            </Collapse> :
                                            <PleaseWait/>
                                    }
                                </Box>
                            </Box>
                        </Box> :
                        <ComponentError
                            statusCode={(_.isObject(error) && error.request) ? error.request.status : undefined}
                            tryAgainFun={() => mutate(apiKey)}/>
                    }
                </Box>
            </FormContainer>
            <TagSingle
                open={setting.openAddTagDialog}
                name={value}
                onClose={(item) => {
                    if (item) {
                        item.hasActive = true;
                        handleAdd(item);
                        enqueueSnackbar('افزوده شد.',
                            {
                                variant: "success",
                                action: (key) => (
                                    <Fragment>
                                        <Button onClick={() => {
                                            closeSnackbar(key)
                                        }}>
                                            {lang.get('close')}
                                        </Button>
                                    </Fragment>
                                )
                            });
                        handleCloseDialog(false);
                        mutate(apiKey)
                        return
                    }
                    setSetting({
                        ...setting,
                        openAddTagDialog: false,
                        dialogPermalink: ''
                    })
                }}/>
            <SelectTag open={setting.selectGpTag}
                       boxId={box.id}
                       selectable={true}
                       onClose={(data) => {
                           if (data)
                               handleAdd(data.tags)
                           setSetting({
                               ...setting,
                               selectGpTag: false
                           })
                       }}/>
        </React.Fragment>
    )
});

//endregion Tags

//region HelperComponents


//region ItemContainer
function ItemContainer(props) {
    return (
        <Box display='flex'
             my={1}
             width={['100%']}
             {...props}>
            {props.children}
        </Box>
    )
}

//endregion ItemContainer

//region TemplateItem
function TemplateContainer({title = 'متن های آماده', ...props}) {
    return (
        <Box display={'flex'} flexDirection={'column'} mt={2}>
            <Typography variant={'body2'} color={cyan[600]}>
                {title}:
            </Typography>
            <Box display={'flex'} flexWrap={'wrap'}>
                {props.children}
            </Box>
        </Box>
    )
}

function TemplateItem({title, data, onClick, ...props}) {
    const theme = useTheme();

    return (
        <BaseButton variant={"outlined"}
                    size={'small'}
                    onClick={() => onClick(data !== undefined ? data : title)}
                    style={{
                        borderColor: cyan[200],
                        color: theme.palette.text.secondary,
                        marginTop: theme.spacing(0.5),
                        marginLeft: theme.spacing(0.5)
                    }}>
            {title}
        </BaseButton>
    )
}

//endregion TemplateItem

//endregion HelperComponents

//region SaveDialog
function SaveDialog({open, onChange}) {
    const theme = useTheme();

    const [state, setState] = useState({
        disable: false
    });

    useEffect(() => {
        setState({
            ...state,
            disable: false
        })
    }, [open]);

    const handleClose = (save) => {
        setState({
            ...state,
            disable: true
        });
        onChange(save)
    };

    return (
        <BaseSaveDialog
            open={open}
            text={(
                <Box display={'flex'} flexDirection={'column'} px={3}>
                    <Typography variant={'h6'} fontWeight={400} pb={2}>
                        ذخیره محصول
                    </Typography>
                    <Typography variant={'body1'}>
                        آیا از ذخیره این محصول اطمینان دارید؟
                    </Typography>
                    <Typography variant={'caption'} color={theme.palette.text.secondary} pt={0.5}>
                                <span style={{color: orange[300], fontWeight: 600, paddingLeft: theme.spacing(0.3)}}>
                                    توجه:
                                </span>
                        در صورت ذخیره، اطلاعات تغییریافته قابل بازگشت نمیباشد.
                    </Typography>
                </Box>)}
            onSubmit={() => {
                handleClose(true)
            }}
            onCancel={() => handleClose(false)}/>
    );
}

//endregion SaveDialog

//endregion Components

