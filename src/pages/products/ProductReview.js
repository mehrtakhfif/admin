import React, {useEffect, useState} from "react";
import Box from "@material-ui/core/Box";
import {useDispatch, useSelector} from "react-redux";
import ControllerProduct from "../../controller/ControllerProduct";
import Table, {createTableHeader, headerItemTemplate} from "../../components/base/table/Table";
import {createFilter} from "../../components/base/table/Filters";
import _ from 'lodash';
import {activeLang, lang, theme, webRout} from "../../repository";
import rout, {siteRout} from "../../router";
import DataUtils, {filterType} from "../../utils/DataUtils";
import Link from "../../components/base/link/Link";
import {
    EditOutlined,
    FiberManualRecordTwoTone,
    RateReview, RateReviewOutlined,
    StorageOutlined,
    VisibilityOutlined
} from "@material-ui/icons";
import IconButton from "@material-ui/core/IconButton";
import Typography from "../../components/base/Typography";
import Tooltip from "@material-ui/core/Tooltip";
import storage from "../../storage";
import useSWR, {mutate} from 'swr'
import ComponentError from "../../components/base/ComponentError";
import {ApiHelper} from "../../controller/converter";
import Img from "../../components/base/img/Img";
import {UtilsStyle} from "../../utils/Utils";
import {cyan, green, orange, red} from "@material-ui/core/colors";
import {Review} from "../Dashboard";
import {gcLog} from "../../utils/ObjectUtils";
import BaseButton from "../../components/base/button/BaseButton";
import {useTheme} from "@material-ui/core";
import LocalStorageUtils from "../../utils/LocalStorageUtils";

const headers = [
    ...headerItemTemplate.idAction(),
    createTableHeader({id: "thumbnail", type: 'str', label: 'تامبنیل', sortable: false}),
    headerItemTemplate.name,
];
const filters = [
    createFilter({id: 'name__fa', type: filterType.text, label: 'نام', value: ''}),
];
const CookieKey = "product-review";
const reviewStateKey = CookieKey + "review"


export default function ({...props}) {
    //region var
    const dispatch = useDispatch();
    const theme = useTheme()
    const {boxes, roll} = useSelector(state => state.user);
    const ownerBox = _.find(boxes, b => b.is_owner);
    //region state
    const [filtersState, setFiltersState] = useState({
        data: filters,
        resetFilters: () => {
            setFiltersState({
                ...filtersState,
                data: DataUtils().TableFilter().resetFilter({filters: filtersState.data}),
            })
        }
    });
    const [state, setState] = useState({
        filters: _.cloneDeep(filters),
        activeBox: storage.PageSetting.getActiveBox(CookieKey, boxes),
        order: undefined,
        pagination: {
            rowPerPage: undefined,
            page: 0
        }
    });
    const [thumbnailDialog, setThumbnailDialog] = useState(false);
    const [tableData, setTableData] = useState()
    const [reviewState, setReviewState] = useState(LocalStorageUtils.get(reviewStateKey, (roll === "superuser" || roll === "content_manager") ? "reviewed" : "request_review"))

    function getCookieKey() {
        try {
            return CookieKey + state.activeBox
        } catch (e) {
        }
        return CookieKey + storage.PageSetting.getActiveBox(getCookieKey(), boxes)
    }

    //endregion state
    //endregion var


    //region requests
    const d = ControllerProduct.Products.reviewProduct({
        boxId: state.activeBox,
        page: state.pagination.page + 1,
        step: state.pagination.rowPerPage,
        order: state.order,
        filters: state.filters,
        reviewState:reviewState
    });


    const {data: dd, error} = useSWR(d[0], () => {
        if (!ownerBox && !(roll === "content_manager" || roll === "superuser")) {
            return undefined
        }
        return d[1]()
    });


    function requestFilters() {
        ControllerProduct.Products.Filters.get({boxId: state.activeBox}).then((res) => {
            const newList = (_.isArray(filtersState.data) && !_.isEmpty(filtersState.data)) ? filtersState.data : filters;
            _.forEach(res.data, (fi) => {
                const data = createFilter({id: fi.name, type: fi.type, label: fi.label, data: fi.filters, value: null});
                switch (fi.name) {
                    case 'categories': {
                        data.value = [];
                        break;
                    }
                    default: {
                    }
                }

                const index = _.findIndex(newList, (l) => l.id === data.id);
                newList[index !== -1 ? index : newList.length] = data;
            });
            setFiltersState({
                ...filtersState,
                data: [...newList]
            })
        }).catch((e) => {
            setFiltersState({
                ...filtersState,
                data: false
            })
        });
    }

    //endregion requests

    //region useEffect
    useEffect(() => {
        if (!state.activeBox)
            return;
        setState({
            ...state,
            order: storage.PageSetting.getOrder(getCookieKey()),
            pagination: {
                rowPerPage: storage.PageSetting.getRowPerPage(getCookieKey()),
                page: storage.PageSetting.getPage(getCookieKey())
            }
        })
        requestFilters();
    }, [state.activeBox]);

    useEffect(() => {
        storage.PageSetting.setPage(getCookieKey(), {page: state.pagination.page});
    }, [state.pagination.page]);

    useEffect(() => {
        setTableData((dd && dd.data ? dd.data : []).map((pr, i) => {
                return pr ? {
                    id: {
                        label: pr.id,
                    },
                    actions: {
                        label:
                            <Box display={'flex'} alignItems={'center'} pl={1}>
                                <Tooltip title={lang.get('show_in_site')}>
                                    <IconButton size={"small"}
                                                no_hover={"true"}
                                                onClick={(e) => {
                                                    e.stopPropagation();
                                                }}
                                                style={{
                                                    marginRight: theme.spacing(0.5),
                                                    marginLeft: theme.spacing(0.5)
                                                }}>
                                        <Typography component={'a'}
                                                    variant={'body1'}
                                                    hoverColor={theme.palette.primary.main}
                                                    href={webRout + siteRout.Product.Single.create({permalink: pr.permalink})}
                                                    target={'_blank'}
                                                    style={{display: 'flex'}}>
                                            <VisibilityOutlined style={{fontSize: 20, padding: 2}}/>
                                        </Typography>
                                    </IconButton>
                                </Tooltip>
                                <Tooltip title={lang.get('edit')}>
                                    <IconButton size={"small"}
                                                onClick={(e) => {
                                                    e.stopPropagation();
                                                }}
                                                style={{
                                                    marginRight: theme.spacing(0.5),
                                                    marginLeft: theme.spacing(0.5)
                                                }}>
                                        <Link toHref={pr.link} linkStyle={{display: 'flex'}}
                                              hoverColor={theme.palette.primary.main}>
                                            <EditOutlined style={{fontSize: 20, padding: 2}}/>
                                        </Link>
                                    </IconButton>
                                </Tooltip>
                                <Tooltip title={lang.get('storages')}>
                                    <IconButton size={"small"}
                                                onClick={(e) => {
                                                    e.stopPropagation();
                                                }}
                                                style={{
                                                    marginRight: theme.spacing(0.5),
                                                    marginLeft: theme.spacing(0.5)
                                                }}>
                                        <Link toHref={pr.storageLink} linkStyle={{display: 'flex'}}
                                              hoverColor={theme.palette.primary.main}>
                                            <StorageOutlined style={{fontSize: 20, padding: 2}}/>
                                        </Link>
                                    </IconButton>
                                </Tooltip>
                                <Tooltip title={"بازبینی"}>
                                    <IconButton size={"small"}
                                                onClick={(e) => {
                                                    e.stopPropagation();
                                                }}
                                                style={{
                                                    marginRight: theme.spacing(0.5),
                                                    marginLeft: theme.spacing(0.5)
                                                }}>
                                        <Link toHref={rout.Product.Review.Single.editProductReview(pr.id)}
                                              target={"_blank"} linkStyle={{display: 'flex'}}
                                              hoverColor={theme.palette.primary.main}>
                                            <RateReviewOutlined style={{fontSize: 20, padding: 2}}/>
                                        </Link>
                                    </IconButton>
                                </Tooltip>
                            </Box>
                    },
                    thumbnail: {
                        label: pr.thumbnail &&
                            <Img src={pr.thumbnail.image}
                                 zoomable={true}
                                 width={60} maxWidth={'auto'} minHeight={'auto'}
                                 onClick={(e) => {
                                     setThumbnailDialog(pr.id);
                                     e.stopPropagation();
                                 }}
                                 style={{cursor: "pointer", ...UtilsStyle.borderRadius(5)}}/>
                    },
                    name: {
                        label: pr.name[activeLang],
                        link: pr.link
                    },
                } : {}
            }
        ))
    }, [dd])
    //endregion useEffect

    //region handler
    function handleFilterChange({item, value}) {
        setState({
            ...state,
            pagination: {
                ...state.pagination,
                page: 0
            }
        })
        setFiltersState({
            ...filtersState,
            data: DataUtils().TableFilter().filterItemChange({filters: filtersState.data, item: item, value: value})
        });
    }


    function handleApplyFilter() {
        setState({
            ...state,
            filters: _.cloneDeep(filtersState.data)
        })
    }

    function handleResetFilter() {
        filtersState.resetFilters()
    }

    function handleChangeOrder(order) {
        storage.PageSetting.setOrder(getCookieKey(), {
            order: order
        });
        setState({
            ...state,
            order: {
                ...order
            }
        });
    }

    function handleChangeBox(e, box) {
        if (!box || state.activeBox === box.id)
            return;
        storage.PageSetting.setActiveBox(CookieKey, {activeBox: box.id});
        filtersState.resetFilters();
        setState({
            ...state,
            activeBox: box.id,
            pagination: {
                ...state.pagination,
                page: 0
            }
        });
    }

    function handleReviewStateChange(state) {
        setReviewState(state)
        LocalStorageUtils.set(reviewStateKey,state)
    }

    //endregion handler

    //region renderData
    const pg = state.pagination;
    //endregion renderData



    return (
        <Box
            my={2}
            px={2}
            display={'flex'}
            flexDirection={'column'}>
            <Box display={'flex'} pb={2}>
                <BaseButton
                    variant={"outlined"}
                    onClick={() => {
                        handleReviewStateChange("reviewed")
                    }}
                    style={{
                        borderColor: orange[300]
                    }}>
                    بازبینی نشده
                </BaseButton>
                <BaseButton
                    variant={"outlined"}
                    onClick={() => {
                        handleReviewStateChange("request_review")
                    }}
                    style={{
                        marginRight:theme.spacing(1),
                        borderColor: cyan[300]
                    }}>
                    بازبینی شده
                </BaseButton>
            </Box>
            {(!error && state.order) ?
                <Table headers={headers}
                       cookieKey={getCookieKey()}
                       title={lang.get("products")}
                       data={tableData}
                       filters={filtersState.data}
                       onFilterChange={handleFilterChange}
                       loading={false}
                       activePage={pg.page}
                       rowsPerPage={pg.rowPerPage}
                       lastPage={dd ? dd.pagination.lastPage : 0}
                       rowCount={dd ? dd.pagination.count : 0}
                       orderType={state.order}
                       activeBox={state.activeBox}
                       addNewButton={{
                           label: lang.get('add_new_product'),
                           link: rout.Product.Single.createNewProduct,
                       }}
                       onActivePageChange={(page) => {
                           setState({
                               ...state,
                               pagination: {
                                   ...state.pagination,
                                   page: page
                               }
                           })
                           // requestData({page: e})
                       }}
                       onRowPerPageChange={(rowPerPage) => {
                           storage.PageSetting.setRowPerPage(getCookieKey(), {rowPerPage: rowPerPage});
                           setState({
                               ...state,
                               pagination: {
                                   ...state.pagination,
                                   rowPerPage: rowPerPage
                               }
                           })
                       }}
                       onApplyFilterClick={handleApplyFilter}
                       onResetFilterClick={handleResetFilter}
                       onChangeOrder={handleChangeOrder}
                       onChangeBox={handleChangeBox}/> :
                <Box>
                    <ComponentError
                        statusCode={(_.isObject(error) && error.request) ? error.request.status : undefined}
                        tryAgainFun={() => mutate(d[0])}/>
                </Box>
            }
        </Box>
    )
}
