import React, {Fragment, useEffect, useRef, useState} from "react";
import Box from "@material-ui/core/Box";
import SelectBox from "../../components/SelectBox";
import ControllerProduct from "../../controller/ControllerProduct";
import ButtonBase from "@material-ui/core/ButtonBase";
import SelectCategory from "../categories/SelectCategory";
import {UtilsStyle} from "../../utils/Utils";
import {cyan} from "@material-ui/core/colors";
import Typography from "../../components/base/Typography";
import {DEBUG, lang, siteLang, theme} from "../../repository";
import {gcLog} from "../../utils/ObjectUtils";
import _ from "lodash";
import {Pricing, Unavailable} from "../storages/StorageSingle";
import BaseButton from "../../components/base/button/BaseButton";
import {canEditMultiStorage, saveMultiSave} from "../storages/StorageMultiEdit";
import CircularProgress from "@material-ui/core/CircularProgress";
import Backdrop from "@material-ui/core/Backdrop";
import {Card} from "@material-ui/core";
import SuccessButton from "../../components/base/button/buttonVariant/SuccessButton";
import rout from "../../router";
import StickyBox from "react-sticky-box";
import Img from "../../components/base/img/Img";
import FormControl from "../../components/base/formController/FormController";
import red from "@material-ui/core/colors/red";
import {Redirect} from "react-router-dom";
import Link from "../../components/base/link/Link";
import BaseDialog from "../../components/base/BaseDialog";
import Button from "@material-ui/core/Button";
import {useSnackbar} from "notistack";
import Switch from "@material-ui/core/Switch";
import ErrorBoundary from "../../components/base/ErrorBoundary";


export default function ({...props}) {
    const [box, setBox] = useState();
    const [selectCategoryDialog, setSelectCategoryDialog] = useState(true);
    const [selectedCategories, setSelectedCategories] = useState([])
    const [availableProduct, setAvailableProduct] = useState(true)

    useEffect(() => {
    }, [selectedCategories])


    return (
        <Box pt={2}>
            {
                box ?
                    !_.isEmpty(selectedCategories) ?
                        <BaseComponent
                            boxId={box.id}
                            availableProduct={availableProduct}
                            categories={selectedCategories}/> :
                        <Box display={"flex"} flexWrap={"wrap"}>
                            <Typography variant={"body2"} p={2} style={{width: '100%'}}>
                                توجه: برای تغییر قیمت محصولات چند انباری باید انبار های محصول از سیستم ضریب انبار
                                پشتیبانی کنند.
                            </Typography>
                            <Box width={1 / 4} p={2} display={'flex'} flexDirection={'column'} alignItems={'center'}
                                 justifyContent={'center'}>
                                <Typography variant={"subtitle1"} pb={2}>
                                    فقط محصولات موجود
                                </Typography>
                                <Switch
                                    checked={availableProduct}
                                    onChange={() => {
                                        setAvailableProduct(!availableProduct)
                                    }}
                                    color="primary"
                                    name="checkedB"
                                    inputProps={{'aria-label': 'primary checkbox'}}/>
                            </Box>
                            <Box width={1 / 4} p={2}>
                                <ButtonBase
                                    onClick={() => {
                                        setSelectCategoryDialog(true)
                                    }}
                                    style={{
                                        width: "100%",
                                        ...UtilsStyle.borderRadius(5)
                                    }}>
                                    <Box width={1} height={100} display={'flex'} alignItems={'center'}
                                         justifyContent={'center'} style={{
                                        backgroundColor: cyan[300],
                                        ...UtilsStyle.borderRadius(5)
                                    }}>
                                        <Typography variant={"h6"} color={"#fff"}>
                                            دستهبندی
                                        </Typography>
                                    </Box>
                                </ButtonBase>
                            </Box>
                            <SelectCategory
                                open={selectCategoryDialog}
                                boxId={box.id}
                                multiSelect={true}
                                onClose={(cats) => {
                                    if (cats) {
                                        const ids = []
                                        _.forEach(cats, c => {
                                            ids.push(c.id)
                                        })
                                        setSelectedCategories(ids)
                                    }
                                    setSelectCategoryDialog(false)
                                }}/>
                        </Box> :
                    <SelectBox
                        onBoxSelected={(box) => {
                            setBox(box)
                        }}/>
            }

        </Box>
    )
}


const productsQLength = 3

function BaseComponent({boxId, availableProduct, categories}) {
    const ref = useRef();
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();
    const [initialize, setInitialize] = useState(false)
    const [productList, setProductList] = useState()
    const [product, setProduct] = useState()
    const [productQ, setProductQ] = useState([])
    const [loadingQ, setLoadingQ] = useState(false)
    const [saveProduct, setSaveProduct] = useState()
    const [saveQ, setSaveQ] = useState([])
    const [failQ, setFailQ] = useState([])
    const [loadingSave, setLoadingSave] = useState(false)
    const [redirectTimer, setRedirectTimer] = useState()
    const [redirect, setRedirect] = useState()

    useEffect(() => {
        ControllerProduct.Products.getIds({
            boxId: boxId,
            available: availableProduct,
            categoriesId: categories
        })[1]().then(res => {
            if (res.data.length < productsQLength + 1) {
                res.data = [...res.data, ...(new Array(productsQLength + 2 - res.data.length))]
            }
            // if (DEBUG) {
            //     setProductList([res.data[0], res.data[1], res.data[2], res.data[3]])
            //     return
            // }
            setProductList(res.data)
        })
    }, [])

    useEffect(() => {
        if (_.isEmpty(productList) || productQ.length === productsQLength || loadingQ)
            return
        setLoadingQ(true)
        const ids = productList;
        const p = ids.splice(0, 1)
        const finallyFun = () => {
            setLoadingQ(false)
            setProductList(ids)
        }
        if (!p[0]) {
            finallyFun()
            return;
        }
        ControllerProduct.Storage.get({pId: p[0]}).then(res => {
            if (_.isEmpty(res.storages)) {
                return
            }
            if (res.storages.length !== 1 && !canEditMultiStorage(res.storages).can) {
                return;
            }
            setProductQ([...productQ, res])
        }).finally(() => {
            finallyFun()
        })
    }, [productQ, productList, loadingQ])

    useEffect(() => {
        if (product || _.isEmpty(productQ)) {
            return
        }
        const nList = productQ;
        const p = nList.splice(0, 1)
        setProduct(convertProduct(p[0]))
        setProductQ([...nList])
        setLoadingSave(false)
    }, [productQ, product])

    useEffect(() => {
        if (saveProduct || _.isEmpty(saveQ))
            return
        const nItem = saveQ;
        const p = nItem.splice(0, 1)
        setSaveProduct(p[0]);
        setSaveQ(nItem)
    }, [saveProduct, saveQ])

    useEffect(() => {
        if (!initialize || saveProduct || !_.isEmpty(productList) || !_.isEmpty(saveQ) || !_.isEmpty(productQ) || !_.isEmpty(failQ) || product) {
            setInitialize(true)
            clearTimeout(redirectTimer)
            return
        }
        setRedirectTimer(setTimeout(() => {
            setRedirect(true)
        }, 2000))
    }, [saveQ, saveProduct, productList, productQ, failQ, product])

    function save() {
        const {unavailable, pricing} = ref.current.serialize()
        if (!unavailable || !pricing.startPrice || !pricing.discountPrice || !pricing.discountPrice) {
            return
        }
        setLoadingSave(true)
        const item = {
            product: _.cloneDeep(product),
            data: {
                unavailable: unavailable === "true",
                ...pricing
            }
        }

        if (!saveProduct && !_.isEmpty(saveQ)) {
            setSaveProduct(item)
        }

        setSaveQ(sa => [
            ...sa,
            item
        ])

        setTimeout(() => {
            skip(true)
        }, 700)
    }

    function skip(saved) {
        const prod = _.cloneDeep(product)
        if (!saved) {
            enqueueSnackbar(prod.product.name[siteLang],
                {
                    variant: "info",
                    action: (key) => (
                        <Fragment>
                            <BaseButton variant={"outlined"} onClick={() => {
                                retry([prod.product.id])
                                closeSnackbar(key)
                            }}>
                                ویرایش دوباره
                            </BaseButton>
                            <BaseButton variant={"text"} onClick={() => {
                                closeSnackbar(key)
                            }}>
                                {lang.get('close')}
                            </BaseButton>
                        </Fragment>
                    )
                });
        }
        setProduct(undefined)
        setInitialize(false)
    }

    function retry(items) {
        if (!_.isArray(items) || _.isEmpty(items))
            return
        setProductList(pr => _.uniq([
            ...pr,
            ...items
        ]))
        setInitialize(false)
    }

    function convertProduct(pr) {
        if (pr.storages.length === 1)
            return {
                ...pr,
                storage: pr.storages[0]
            }
        let prod = undefined;
        _.forEach(pr.storages, s => {
            if (s.settings.ratio === 1) {
                prod = s;
                return false
            }
        })

        return {
            ...pr,
            storage: prod
        }
    }

    const saveAndNextEl = (
        <SuccessButton
            loading={loadingSave}
            onClick={save}>
            ذخیره
        </SuccessButton>
    )


    const skipEl = (
        <BaseButton
            variant={"outlined"}
            disabled={_.isEmpty(productQ) && !product}
            onClick={() => skip()}
            style={{
                borderColor: cyan[300]
            }}>
            ردکردن
        </BaseButton>
    )
    return (
        !redirect ?
            <Box display={'flex'} flexDirection={'column'}>
                <Box display={'flex'} pb={3} px={2}>
                    <FormControl innerref={ref} width={"80%"} pr={2}>
                        {
                            product &&
                            <Box display={'flex'} flexDirection={'column'}>
                                <Box display={'flex'} alignItems={"center"}>
                                    <Box height={100}>
                                        {
                                            product.product.thumbnail &&
                                            <Box pr={1}>
                                                <Img src={product.product.thumbnail.image} width={130} minHeight={1}/>
                                            </Box>
                                        }
                                    </Box>
                                    <Box display={'flex'} flexDirection={'column'}>

                                        <Link variant={"h6"} fontWeight={400}
                                              toHref={rout.Storage.Single.showStorageDashboard({productId: product.product.id})}
                                              target={"_blank"}>
                                            {product.product.name[siteLang]}
                                        </Link>
                                        <ErrorBoundary>
                                            <Typography pt={0.5}>
                                                انبار:
                                                {" "+ product.storage.storageTitle[siteLang]}
                                            </Typography>
                                        </ErrorBoundary>
                                    </Box>
                                </Box>
                                <Unavailable
                                    pt={2} px={2}
                                    alignItems={"unset"}
                                    unavailable={product.storage.unavailable}/>
                                <Pricing
                                    box={product.product.box}
                                    open={true}
                                    item={{
                                        startPrice: product.storage.startPrice,
                                        finalPrice: product.storage.finalPrice,
                                        discountPrice: product.storage.discountPrice,
                                        shipping_cost: product.storage.shipping_cost,
                                        tax: product.storage.tax,
                                    }}
                                    shippingCostShow={false}
                                    taxShow={false}
                                    ratioShow={false}/>
                                <Box pt={2} display={'flex'}>
                                    {saveAndNextEl}
                                    <Box pl={2}>
                                        {skipEl}
                                    </Box>
                                </Box>
                            </Box>
                        }
                    </FormControl>
                    <Box width={'20%'}>
                        <StickyBox offsetTop={150} offsetBottom={20}>
                            <Box display={'flex'} py={2} width={1} flexDirection={'column'}>
                                <Box display={'flex'} alignItems={'center'}
                                     component={Card}
                                     py={2}
                                     justifyContent={'center'} mt={1}>
                                    {saveAndNextEl}
                                    <Box pl={2}>
                                        {skipEl}
                                    </Box>
                                </Box>

                                <Box display={'flex'} flexDirection={"column"} widht={1} pt={3} pb={1}>
                                    {
                                        (saveProduct || !_.isEmpty(saveQ)) &&
                                        <Typography variant={"subtitle1"}>
                                            لیست ذخیره سازی:
                                        </Typography>
                                    }
                                    <Box display={'flex'} height={1} pt={1} width={1} flexDirection={"column"}>
                                        {
                                            saveProduct &&
                                            <SaveItemComponent
                                                index={0}
                                                item={saveProduct}
                                                onSaved={(saved) => {
                                                    if (DEBUG || !saved) {
                                                        setFailQ([...failQ, _.cloneDeep(saveProduct)])
                                                    }
                                                    setSaveProduct(undefined)
                                                }}/>
                                        }
                                        {saveQ.map((item, index) => index < 3 ?
                                            <SaveItemComponent key={index} index={index + 1} item={item}/> :
                                            <React.Fragment key={index}/>)}
                                        {
                                            saveQ.length > 3 &&
                                            <Box width={1} height={80} style={{position: 'relative'}}>
                                                <Box
                                                    width={1}
                                                    display={'flex'}
                                                    alignItems={'center'}
                                                    justifyContent={'center'}
                                                    style={{
                                                        position: 'absolute',
                                                        top: 0,
                                                        bottom: 0,
                                                        left: 0,
                                                        right: 0
                                                    }}>
                                                    <CircularProgress/>
                                                    <Typography variant={"h6"} fontWeight={500}
                                                                style={{position: 'absolute'}}>
                                                        {saveQ.length - 3}
                                                    </Typography>
                                                </Box>
                                            </Box>
                                        }
                                        <Failed
                                            items={failQ}
                                            onSkipClick={() => {
                                                setFailQ([])
                                            }}
                                            onRetryClick={() => {
                                                const items = []
                                                _.forEach(failQ, f => {
                                                    items.push(f.product.product.id)
                                                })
                                                setFailQ([])
                                                retry(items)
                                            }}/>
                                    </Box>
                                </Box>
                            </Box>
                        </StickyBox>
                    </Box>
                </Box>
            </Box> :
            <Redirect
                to={{
                    pathname: rout.Product.rout
                }}/>
    )
}

function SaveItemComponent({index, item, onSaved, ...props}) {
    const {data} = item;
    const {product, storages} = item.product;
    const f = index === 0

    useEffect(() => {
        try {
            if (!f)
                return;
            const {unavailable, discountPrice, finalPrice, startPrice} = data;
            if (storages.length > 1) {
                saveMultiSave({
                    storageId: storages[0].storageId,
                    unavailable: unavailable,
                    startPrice: startPrice,
                    discountPrice: discountPrice,
                    finalPrice: finalPrice,
                    storages: storages
                }).then(res => {
                    onSaved(true)
                }).catch(() => {
                    onSaved(false)
                })
                return;
            }
            ControllerProduct.Storage.save({
                sId: storages[0].storageId,
                unavailable: unavailable,
                startPrice: startPrice,
                discountPrice: discountPrice,
                finalPrice: finalPrice
            }).then(res => {
                onSaved(true)
            }).catch(() => {
                onSaved(false)
            })
        } catch (e) {
        }
    }, [])

    return (
        <Box
            py={1}
            width={1}
            height={1}
            key={product.id}
            {...props}
            style={{
                ...props.style,
                position: 'relative',
            }}
        >
            <Box component={Card} display={'flex'} flexDriection={'column'}
                 justifyContent={'center'} alignItems={'center'} p={1}
                 height={1}
                 style={{
                     opacity: f ? 1 : 0.5,
                 }}>
                {
                    product.thumbnail &&
                    <Box pr={1}>
                        <Img src={product.thumbnail.image} width={100} minHeight={1}/>
                    </Box>
                }
                <Typography variant={"subtitle1"} fontWeight={400}>
                    {product.name[siteLang]}
                </Typography>
            </Box>
            {
                !f &&
                <Box width={1}
                     display={'flex'}
                     alignItems={'center'}
                     justifyContent={'center'}
                     style={{
                         position: 'absolute',
                         top: 0,
                         bottom: 0,
                         left: 0,
                         right: 0
                     }}>
                    <CircularProgress/>
                </Box>
            }
        </Box>
    )
}

function Failed({items, onRetryClick, onSkipClick, ...props}) {
    const [open, setOpen] = useState(false)

    return (
        !_.isEmpty(items) ?
            <React.Fragment>
                <ButtonBase onClick={() => setOpen(true)}>
                    <Typography pt={2} variant={"h6"} color={red[700]} alignItems={"center"}
                                justifyContent={"center"}>
                        <Typography pl={1.5} variant={"h5"} fontWeight={600} color={red[700]}>
                            {items.length}
                        </Typography>
                        محصول به مشکل برخورد
                    </Typography>
                </ButtonBase>
                <BaseDialog open={open} maxWidth={"lg"} onClose={() => setOpen(false)}>
                    <Box width={1} display={'flex'} flexWrap={"wrap"}>
                        <Box display={'flex'} width={1} px={2} py={2} alignitems={'center'}>
                            <Typography variant={"h6"}>
                                محصولات به مشکل خورده:({items.length})
                            </Typography>
                            <Box px={1}>
                                <BaseButton variant={"outlined"} onClick={onRetryClick}
                                            style={{
                                                borderColor: cyan[300]
                                            }}>
                                    ویرایش دوباره
                                </BaseButton>
                            </Box>
                            <Box px={1}>
                                <BaseButton
                                    variant={"outlined"}
                                    onClick={onSkipClick}>
                                    نادیده گرفتن موارد
                                </BaseButton>
                            </Box>
                        </Box>
                        {
                            items.map(({product}) => (
                                <Box key={product.id} display={'flex'} width={1 / 4} p={1}>
                                    <Box display={'flex'} alignItems={'center'} component={Card} width={1} p={1}>
                                        <Box height={100}>
                                            {
                                                product.product.thumbnail &&
                                                <Box pr={1}>
                                                    <Img src={product.product.thumbnail.image} width={130}
                                                         minHeight={1}/>
                                                </Box>
                                            }
                                        </Box>
                                        <Link variant={"subtitle1"} fontWeight={400}
                                              toHref={rout.Storage.Single.showStorageDashboard({productId: product.product.id})}
                                              target={"_blank"}>
                                            {product.product.name[siteLang]}
                                        </Link>
                                    </Box>
                                </Box>
                            ))
                        }
                    </Box>
                </BaseDialog>
            </React.Fragment> :
            <React.Fragment/>
    )
}
