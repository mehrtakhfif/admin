import React, {Fragment, useEffect, useState} from "react";
import Box from "@material-ui/core/Box";
import BaseDialog from "../../components/base/BaseDialog";
import Typography from "../../components/base/Typography";
import ControllerProduct from "../../controller/ControllerProduct";
import Backdrop from "@material-ui/core/Backdrop";
import CircularProgress from "@material-ui/core/CircularProgress";
import {cyan, grey} from "@material-ui/core/colors";
import {useSnackbar} from "notistack";
import Button from "@material-ui/core/Button";
import {lang, siteLang} from "../../repository";
import _ from 'lodash'
import DraggableList from "../../components/base/draggableList/DraggableList";
import ComponentError from "../../components/base/ComponentError";
import {UtilsStyle} from "../../utils/Utils";
import {featureType} from "../../controller/type";
import OrderItems from "../../components/base/OrderItems";
import {gcLog} from "../../utils/ObjectUtils";


export default function ({open, productId, onClose, ...props}) {
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();

    const [product, setProduct] = useState()
    const [data, setData] = useState(true)
    const [error, setError] = useState(false)

    useEffect(() => {
        if (!(open && productId)) {
            return
        }
        getData()
    }, [open, productId])


    function getData() {
        setData(true)
        ControllerProduct.Product.get({pId: productId}).then(res => {
            if (_.isEmpty(res.data.features)) {
                showSnack("هیچ فیچری یافت نشد.", "info")
                onClose()
                return
            }
            setData(()=>res.data.features)
            setProduct(res.data)
        }).catch(() => {
            setError(true)
        })
    }

    function showSnack(text, variant = "error") {
        enqueueSnackbar(text,
            {
                variant: variant,
                action: (key) => (
                    <Fragment>
                        <Button onClick={() => {
                            closeSnackbar(key)
                        }}>
                            {lang.get('close')}
                        </Button>
                    </Fragment>
                )
            });
    }


    return (
        <OrderItems
            open={open && productId}
            data={error?false:data}
            header={"مرتبسازی فیچر"}
            model={"product_feature"}
            helperText={"( اگر در ادیت محصول هستید اول باید فیچر های جدید را ذخیره کنید بعد مرتبسازی کنید. )"}
            onRetry={()=>{
                getData()
            }}
            onDataChange={(data) => {
                setData(data)
            }}
            onClose={(saved) => {
                onClose(saved)
            }}
            renderItem={(item, {isDraggingItem}) => {
                const {id, feature} = item;
                return (
                    <Box key={id}
                         display={'flex'}
                         alignItems={'center'}
                         py={1.5}
                         px={1}
                         width={1}
                         style={{
                             ...UtilsStyle.borderRadius(5),
                             backgroundColor: isDraggingItem ? grey[300] : undefined
                         }}>
                        <Typography variant={"h6"} pl={1}>
                            {feature.name[siteLang]}
                        </Typography>
                        <Typography variant={"body1"}>
                            ( {featureType[feature.type].typeLabel} )
                        </Typography>
                    </Box>
                )
            }}
        />
    )
}
