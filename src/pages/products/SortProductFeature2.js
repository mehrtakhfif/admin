import React, {Fragment, useEffect, useState} from "react";
import Box from "@material-ui/core/Box";
import BaseDialog from "../../components/base/BaseDialog";
import Typography from "../../components/base/Typography";
import ControllerProduct from "../../controller/ControllerProduct";
import Backdrop from "@material-ui/core/Backdrop";
import CircularProgress from "@material-ui/core/CircularProgress";
import {cyan, grey} from "@material-ui/core/colors";
import {useSnackbar} from "notistack";
import Button from "@material-ui/core/Button";
import {lang, siteLang} from "../../repository";
import _ from 'lodash'
import DraggableList from "../../components/base/draggableList/DraggableList";
import ComponentError from "../../components/base/ComponentError";
import {UtilsStyle} from "../../utils/Utils";
import {featureType} from "../../controller/type";


export default function ({open, productId, onClose, ...props}) {
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();

    const [product, setProduct] = useState()
    const [data, setData] = useState()
    const [error, setError] = useState(false)
    const [loading, setLoading] = useState(false)

    useEffect(() => {
        if (!(open && productId)) {
            return
        }
        getData()
    }, [open, productId])


    function getData() {
        setError(false)
        ControllerProduct.Product.get({pId: productId}).then(res => {
            if (_.isEmpty(res.data.features)) {
                showSnack("هیچ فیچری یافت نشد.", "info")
                onClose()
                return
            }
            setData((() => {
                const list = []
                _.forEach(res.data.features, (f) => {
                    list.push({
                        ...f,
                        id: f.feature.id
                    })
                })
                return list
            })())
            setProduct(res.data)
        }).catch(() => {
            setError(true)
        })
    }

    function showSnack(text, variant = "error") {
        enqueueSnackbar(text,
            {
                variant: variant,
                action: (key) => (
                    <Fragment>
                        <Button onClick={() => {
                            closeSnackbar(key)
                        }}>
                            {lang.get('close')}
                        </Button>
                    </Fragment>
                )
            });
    }


    return (
        <BaseDialog
            open={open && productId}
            header={"مرتبسازی فیچر"}
            onClose={() => onClose()}>
            <Box p={2} display={'flex'} flexDirection={'column'}>
                <Typography variant={"caption"} pr={0.5}>
                    ( اگر در ادیت محصول هستید اول باید فیچر های جدید را ذخیره کنید بعد مرتبسازی کنید. )
                </Typography>
                {
                    data &&
                    <React.Fragment>
                        <DraggableList
                            items={data}
                            onItemsChange={(items) => {
                                setData(items)
                            }}
                            rootStyle={(dragging) => {
                                return {
                                    backgroundColor: dragging ? null : grey[200],
                                    ...UtilsStyle.transition(500)
                                }
                            }}
                            render={(item, props, {isDraggingItem, ...p}) => {
                                const {id, feature} = item;
                                return (
                                    <Box key={id}
                                         display={'flex'}
                                         alignItems={'center'}
                                         py={1.5}
                                         width={1}
                                         {...props}>
                                        <Typography variant={"h6"} pl={1}>
                                            {feature.name[siteLang]}
                                        </Typography>
                                        <Typography variant={"body1"}>
                                            ( {featureType[feature.type].typeLabel} )
                                        </Typography>
                                    </Box>
                                )
                            }}/>
                    </React.Fragment>
                }
                {
                    error &&
                    <ComponentError
                        tryAgainFun={() => {
                            getData()
                        }}/>
                }
            </Box>
            <Backdrop open={loading || !(data || error)}
                      style={{
                          zIndex: '9999'
                      }}>
                <CircularProgress color="inherit"
                                  style={{
                                      color: cyan[300],
                                      width: 100,
                                      height: 100
                                  }}/>
            </Backdrop>
        </BaseDialog>
    )
}
