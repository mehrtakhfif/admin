import React, {useEffect, useState} from "react";
import Box from "@material-ui/core/Box";
import {useDispatch, useSelector} from "react-redux";
import ControllerProduct from "../../controller/ControllerProduct";
import Table, {createTableHeader, headerItemTemplate} from "../../components/base/table/Table";
import {createFilter} from "../../components/base/table/Filters";
import _ from 'lodash';
import {activeLang, cookieVersion, lang, theme, webRout} from "../../repository";
import rout, {siteRout} from "../../router";
import DataUtils, {filterType} from "../../utils/DataUtils";
import Link from "../../components/base/link/Link";
import {
    EditOutlined,
    FiberManualRecordTwoTone,
    RateReviewOutlined,
    StorageOutlined,
    VisibilityOutlined,
    Edit
} from "@material-ui/icons";
import IconButton from "@material-ui/core/IconButton";
import Typography from "../../components/base/Typography";
import Tooltip from "@material-ui/core/Tooltip";
import storage from "../../storage";
import useSWR, {mutate} from 'swr'
import ComponentError from "../../components/base/ComponentError";
import Img from "../../components/base/img/Img";
import {UtilsStyle} from "../../utils/Utils";
import {cyan, green, purple, red} from "@material-ui/core/colors";
import {Review} from "../Dashboard";
import {gcLog} from "../../utils/ObjectUtils";
import BaseButton from "../../components/base/button/BaseButton";
import ButtonLink from "../../components/base/link/ButtonLink";


const headers = [
    ...headerItemTemplate.idAction(),
    createTableHeader({id: "thumbnail", type: 'str', label: 'تامبنیل', sortable: false}),
    headerItemTemplate.name,
    createTableHeader({id: "disable", type: 'str', label: 'فعال'}),
    createTableHeader({id: "categories", type: 'str', label: 'دسته‌ها'}),
    createTableHeader({id: "type", type: 'str', label: 'نوع'}),
    createTableHeader({id: "storages", type: 'str', label: 'انبارها'}),
    createTableHeader({id: "active_storages", type: 'str', label: 'انبارهای فعال', sortable: false}),
    createTableHeader({id: "productStatus", type: 'str', label: 'وضعیت', sortable: false}),
    ...headerItemTemplate.createByUpdatedBy(),
];
const filters = [
    createFilter({id: 'name__fa', type: filterType.text, label: 'نام', value: ''}),
];
const CookieKey = "product";

export default function ({...props}) {
    //region var
    const dispatch = useDispatch();
    const {boxes} = useSelector(state => state.user);

    const [tableData, setTableData] = useState()
    const {canSetReview} = useSelector(state => state.user)


    //region state
    const [filtersState, setFiltersState] = useState({
        data: filters,
        resetFilters: () => {
            setFiltersState({
                ...filtersState,
                data: DataUtils().TableFilter().resetFilter({filters: filtersState.data}),
            })
        }
    });

    const [state, setState] = useState(oldState => ({
        ...oldState,
        filters: _.cloneDeep(filters),
        activeBox: storage.PageSetting.getActiveBox(CookieKey, boxes),
        order: storage.PageSetting.getOrder(CookieKey),
        pagination: {
            rowPerPage: storage.PageSetting.getRowPerPage(cookieVersion),
            page: storage.PageSetting.getPage(cookieVersion)
        }
    }));

    const [thumbnailDialog, setThumbnailDialog] = useState(false);


    function getCookieKey() {
        try {
            return CookieKey + state.activeBox
        } catch (e) {
        }
        return CookieKey + storage.PageSetting.getActiveBox(getCookieKey(), boxes)
    }

    //endregion state
    //endregion var

    //region requests
    const d = ControllerProduct.Products.get({
        boxId: state.activeBox,
        page: state.pagination.page + 1,
        step: state.pagination.rowPerPage,
        order: state.order,
        filters: state.filters,
    });


    const {data: dd, error} = useSWR(d[0], () => {
        if (state.order === undefined) {
            return {}
        }

        return d[1]()
    }, {
        revalidateOnFocus: true
    });


    function requestFilters() {
        ControllerProduct.Products.Filters.get({boxId: state.activeBox}).then((res) => {
            const newList = (_.isArray(filtersState.data) && !_.isEmpty(filtersState.data)) ? filtersState.data : filters;
            _.forEach(res.data, (fi) => {
                const data = createFilter({id: fi.name, type: fi.type, label: fi.label, data: fi.filters, value: null});
                switch (fi.name) {
                    case 'categories': {
                        data.value = [];
                        break;
                    }
                    default: {
                    }
                }

                const index = _.findIndex(newList, (l) => l.id === data.id);
                newList[index !== -1 ? index : newList.length] = data;
            });
            setFiltersState(oldState => ({
                ...oldState,
                data: [...newList]
            }))
        }).catch((e) => {

            setFiltersState(oldState => ({
                ...oldState,
                data: false
            }))
        });
    }

    //endregion requests

    //region useEffect
    useEffect(() => {
        if (!state.activeBox)
            return;
        setState(oldState => ({
            ...oldState,
            activeBox: state.activeBox,
            order: storage.PageSetting.getOrder(getCookieKey()),
            pagination: {
                rowPerPage: storage.PageSetting.getRowPerPage(getCookieKey()),
                page: storage.PageSetting.getPage(getCookieKey())
            }
        }))
        requestFilters();
    }, [state.activeBox]);

    useEffect(() => {
        storage.PageSetting.setPage(getCookieKey(), {page: state.pagination.page});
    }, [state.pagination.page]);

    useEffect(() => {
        setTableData(((dd && dd.data) ? dd.data : [...Array(state.pagination.rowPerPage)]).map((pr, i) => {
                return pr ? {
                    id: {
                        label: pr.id,
                    },
                    actions: {
                        label:
                            <Box display={'flex'} alignItems={'center'} pl={1}>
                                <Tooltip title={lang.get('show_in_site')}>
                                    <IconButton size={"small"}
                                                no_hover={"true"}
                                                onClick={(e) => {
                                                    e.stopPropagation();
                                                }}
                                                style={{
                                                    marginRight: theme.spacing(0.5),
                                                    marginLeft: theme.spacing(0.5)
                                                }}>
                                        <Typography component={'a'}
                                                    variant={'body1'}
                                                    hoverColor={theme.palette.primary.main}
                                                    href={webRout + siteRout.Product.Single.create({permalink: pr.permalink})}
                                                    target={'_blank'}
                                                    style={{display: 'flex'}}>
                                            <VisibilityOutlined style={{fontSize: 20, padding: 2}}/>
                                        </Typography>
                                    </IconButton>
                                </Tooltip>
                                <Tooltip title={lang.get('edit_beta')}>
                                    <IconButton size={"small"}
                                                onClick={(e) => {
                                                    e.stopPropagation();
                                                }}
                                                style={{
                                                    marginRight: theme.spacing(0.5),
                                                    marginLeft: theme.spacing(0.5)
                                                }}>
                                        <Link toHref={rout.Product.Single.ProductsNew.editProduct(pr.id)} linkStyle={{display: 'flex'}}
                                              hoverColor={theme.palette.primary.main}>
                                            <Edit style={{fontSize: 20, padding: 2}}/>
                                        </Link>
                                    </IconButton>
                                </Tooltip>
                                <Tooltip title={lang.get('edit')}>
                                    <IconButton size={"small"}
                                                onClick={(e) => {
                                                    e.stopPropagation();
                                                }}
                                                style={{
                                                    marginRight: theme.spacing(0.5),
                                                    marginLeft: theme.spacing(0.5)
                                                }}>
                                        <Link toHref={pr.link} linkStyle={{display: 'flex'}}
                                              hoverColor={theme.palette.primary.main}>
                                            <EditOutlined style={{fontSize: 20, padding: 2}}/>
                                        </Link>
                                    </IconButton>
                                </Tooltip>
                                <Tooltip title={lang.get('storages')}>
                                    <IconButton size={"small"}
                                                onClick={(e) => {
                                                    e.stopPropagation();
                                                }}
                                                style={{
                                                    marginRight: theme.spacing(0.5),
                                                    marginLeft: theme.spacing(0.5)
                                                }}>
                                        <Link toHref={pr.storageLink} linkStyle={{display: 'flex'}}
                                              hoverColor={theme.palette.primary.main}>
                                            <StorageOutlined style={{fontSize: 20, padding: 2}}/>
                                        </Link>
                                    </IconButton>
                                </Tooltip>
                                <Tooltip title={"بازبینی"}>
                                    <IconButton size={"small"}
                                                onClick={(e) => {
                                                    e.stopPropagation();
                                                }}
                                                style={{
                                                    marginRight: theme.spacing(0.5),
                                                    marginLeft: theme.spacing(0.5)
                                                }}>
                                        <Link toHref={rout.Product.Review.Single.editProductReview(pr.id)}
                                              target={"_blank"} linkStyle={{display: 'flex'}}
                                              hoverColor={theme.palette.primary.main}>
                                            <RateReviewOutlined style={{fontSize: 20, padding: 2}}/>
                                        </Link>
                                    </IconButton>
                                </Tooltip>
                            </Box>
                    },
                    thumbnail: {
                        label: pr.thumbnail &&
                            <Img src={pr.thumbnail.image}
                                 zoomable={true}
                                 width={60} maxWidth={'auto'} minHeight={'auto'}
                                 style={{cursor: "pointer", ...UtilsStyle.borderRadius(5)}}/>
                    },
                    name: {
                        label:<span className={"enNum"}>
                            { pr.name[activeLang]}
                        </span>,
                        link: pr.link
                    },
                    categories: {
                        label:
                            <Box display={'flex'} flexWrap={'wrap'} minWidth={250} width={"auto"} maxWidth={300}
                                 maxHeight={75}
                                 style={{overflowY: 'auto'}}>
                                {pr.categories && pr.categories.map((cat, index) => (
                                    <Link key={cat.id} toHref={cat.link} ml={1} my={0.5}>
                                        <Typography variant={"caption"}>
                                            {cat.name[activeLang]}{pr.categories.length < index + 1 && ", "}
                                        </Typography>
                                    </Link>
                                ))}
                            </Box>,
                    },
                    type: {
                        label: <Typography variant={"body2"} justifyContent={"center"}>
                            {pr?.type?.label}
                        </Typography>,
                    },
                    storages: {
                        label:
                            <Typography display={'flex'} justifyContent={'center'}
                                        variant={'h6'} fontWeight={400}
                                        color={undefined}>
                                {pr.storages_count}
                            </Typography>
                    },
                    active_storages: {
                        label:
                            <Typography display={'flex'} justifyContent={'center'}
                                        variant={'h6'} fontWeight={400}
                                        color={undefined}>
                                {pr.active_storages_count}
                            </Typography>
                    },
                    disable: {
                        label: (() => {
                            const params = !pr.disable ?
                                {
                                    title: 'فعال',
                                    color: green[300]
                                }
                                :
                                {
                                    title: 'غیرفعال',
                                    color: red[300]
                                };

                            return (
                                <Box display={'flex'} flexWrap={'wrap'} justifyContent={'center'} alignItems={'center'}
                                     maxWidth={300}>
                                    <Tooltip title={params.title}>
                                        <FiberManualRecordTwoTone style={{color: params.color}}/>
                                    </Tooltip>
                                </Box>
                            )
                        })(),
                    },
                    productStatus: {
                        label: (() => {
                            const params = pr.settings.productStatus === 1 ?
                                {
                                    title: 'مطلوب',
                                    color: cyan[300]
                                }
                                : pr.settings.productStatus === 2 ?
                                    {
                                        title: 'کامل',
                                        color: green[300]
                                    } :
                                    {
                                        title: 'ناقص',
                                        color: red[300]
                                    };

                            return (

                                <Box display={'flex'} flexWrap={'wrap'} justifyContent={'center'} alignItems={'center'}
                                     maxWidth={300}>
                                    <Tooltip title={params.title}>
                                        <FiberManualRecordTwoTone style={{color: params.color}}/>
                                    </Tooltip>
                                </Box>
                            )
                        })(),
                    },
                    updated_at: {
                        label: pr.updatedAt.date,
                    },
                    updated_by: {
                        label: pr.updatedAt.by.name,
                        link: pr.updatedAt.by.link,
                    },
                    created_at: {
                        label: pr.createdAt.date,
                    },
                    created_by: {
                        label: pr.createdAt.by.name,
                        link: pr.createdAt.by.link,
                    },
                } : {}
            }
        ))
    }, [dd])
    //endregion useEffect

    //region handler
    function handleFilterChange({item, value}) {
        setState(oldState => ({
            ...oldState,
            pagination: {
                ...state.pagination,
                page: 0
            }
        }))
        setFiltersState(oldState => ({
            ...oldState,
            data: DataUtils().TableFilter().filterItemChange({filters: filtersState.data, item: item, value: value})
        }));
    }

    function handleApplyFilter() {
        setState(oldState => ({
            ...oldState,
            filters: _.cloneDeep(filtersState.data)
        }))
    }

    function handleResetFilter() {
        filtersState.resetFilters()
    }

    function handleChangeOrder(order) {
        storage.PageSetting.setOrder(getCookieKey(), {
            order: order
        });
        setState(oldState => ({
            ...oldState,
            order: {
                ...order
            }
        }));
    }

    function handleChangeBox(e, box) {
        if (!box || state.activeBox === box.id)
            return;
        storage.PageSetting.setActiveBox(CookieKey, {activeBox: box.id});
        filtersState.resetFilters();
        setState({
            ...state,
            activeBox: box.id,
            filters: {},
            pagination: {
                ...state.pagination,
                page: 0
            }
        });
    }

    //endregion handler

    //region renderData
    const pg = state.pagination;
    //endregion renderData

    return (
        <Box
            my={2}
            px={2}
            display={'flex'}
            flexDirection={'column'}>
            <Box width={1}
                 display={'flex'}>
                <Review mt={1} mb={2}/>
            </Box>
            {(!error && state.order) ?
                <Table
                    headers={headers}
                    cookieKey={getCookieKey()}
                    title={lang.get("products")}
                    data={tableData}
                    filters={filtersState.data}
                    onFilterChange={handleFilterChange}
                    loading={false}
                    activePage={pg.page}
                    rowsPerPage={pg.rowPerPage}
                    lastPage={dd ? dd.pagination.lastPage : 0}
                    rowCount={dd ? dd.pagination.count : 0}
                    orderType={state.order}
                    activeBox={state.activeBox}
                    addNewButton={(
                        <Box display={'flex'}>
                            <ButtonLink
                                toHref={rout.Product.Single.createNewProduct}
                                color={"#FFF"}
                                style={{
                                    backgroundColor: cyan[300]
                                }}>
                                {lang.get('add_new_product')}
                            </ButtonLink>
                            <ButtonLink
                                toHref={rout.Product.Single.ProductsNew.createNewProduct}
                                color={"#FFF"}
                                style={{
                                    backgroundColor: cyan[300],
                                    marginRight:'8px',
                                    marginLeft:'8px',
                                }}>
                                {lang.get('add_new_product_beta')}
                            </ButtonLink>
                            <Box pl={2}>
                                <ButtonLink
                                    toHref={rout.Product.Single.MultiEdit.rout}
                                    color={"#fff"}
                                    style={{
                                        backgroundColor: purple[300]
                                    }}>
                                    تغییر قیمت گروهی
                                </ButtonLink>
                            </Box>
                        </Box>
                    )}
                    onActivePageChange={(page) => {
                        setState({
                            ...state,
                            pagination: {
                                ...state.pagination,
                                page: page
                            }
                        })
                        // requestData({page: e})
                    }}
                    onRowPerPageChange={(rowPerPage) => {
                        storage.PageSetting.setRowPerPage(getCookieKey(), {rowPerPage: rowPerPage});
                        setState({
                            ...state,
                            pagination: {
                                ...state.pagination,
                                rowPerPage: rowPerPage
                            }
                        })
                    }}
                    onApplyFilterClick={handleApplyFilter}
                    onResetFilterClick={handleResetFilter}
                    onChangeOrder={handleChangeOrder}
                    onChangeBox={handleChangeBox}/> :
                <Box>
                    <ComponentError
                        statusCode={(_.isObject(error) && error.request) ? error.request.status : undefined}
                        tryAgainFun={() => mutate(d[0])}/>
                </Box>
            }
        </Box>
    )
}
