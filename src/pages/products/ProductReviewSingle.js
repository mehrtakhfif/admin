import React, {useEffect, useRef, useState} from "react";
import Box from "@material-ui/core/Box";
import Typography, {Typography2} from "../../components/base/Typography";
import useSWR from "swr/esm/use-swr";
import ControllerProduct from "../../controller/ControllerProduct";
import {gcLog, tryIt} from "../../utils/ObjectUtils";
import PleaseWait from "../../components/base/loading/PleaseWait";
import ComponentError from "../../components/base/ComponentError";
import {mutate} from "swr";
import FormController from "../../components/base/formController/FormController";
import MultiTextField from "../../components/base/textField/MultiTextField";
import {siteLang} from "../../repository";
import Img from "../../components/base/img/Img";
import SuccessButton from "../../components/base/button/buttonVariant/SuccessButton";
import {CheckBox, CheckBoxOutlineBlank, Close, FiberManualRecordTwoTone} from "@material-ui/icons";
import Tooltip from "../../components/base/Tooltip";
import {useTheme} from "@material-ui/core";
import {cyan, green, orange, red} from "@material-ui/core/colors";
import _ from 'lodash'
import {Utils, UtilsParser, UtilsStyle} from "../../utils/Utils";
import JDate from "../../utils/JDate";
import userAvatar from "../../drawable/svg/userAvatar.svg";
import ControllerUser from "../../controller/ControllerUser";
import BaseButton from "../../components/base/button/BaseButton";
import IconButton from "@material-ui/core/IconButton";
import Icon from "@material-ui/core/Icon";
import EditorDialog from "../../components/base/editor/EditorDialog";
import {useSelector} from "react-redux";
import BaseDialog from "../../components/base/BaseDialog";
import CircularProgress from "@material-ui/core/CircularProgress";
import Backdrop from "@material-ui/core/Backdrop";

export default function ProductReviewSingle({open, productId, onClose, ...props}) {

    const el = (!_.isBoolean(open) || open) ?
        <CM
            onClose={_.isBoolean(open) ? (e) => {
                onClose(e)
            } : undefined}
            productId={productId} {...props}/> : <React.Fragment/>
    return (
        _.isBoolean(open) ?
            <BaseDialog fullScreen={true} open={open} onClose={(e) => onClose(e)}>
                <Box display={'flex'} flexDirecion={'column'}>
                    {el}
                </Box>
            </BaseDialog> :
            el
    )
}


function CM({onClose, productId, ...props}) {
    const {id: userId, roll} = useSelector(state => state.user);

    const pId = productId || props.match.params.pId;
    const ref = useRef();
    const theme = useTheme();
    const [state, setState] = useState({
        loading: false
    })
    const [review, setReview] = useState(false)
    const [editorOpen, setEditorOpen] = useState(false)
    const [editorText, setEditorText] = useState()
    const [question, setQuestion] = useState((roll === "superuser") ? undefined : roll === "content_manager")
    const [resetTextField, setResetTextField] = useState(false)

    const apiKey = `product-${pId}`;
    const {data, error} = useSWR(apiKey, () => {
        return ControllerProduct.Product.get({pId: pId})
    }, {
        onSuccess: (res) => {
            setReview(res.data.review.state !== "ready")
            setState({
                ...state,
                loading: false
            })
        },
        onError: () => {
            setState({
                ...state,
                loading: false
            })
        }
    })


    function handleSave(active) {
        setState({
            ...state,
            loading: true
        })
        setTimeout(() => {
            try {
                const reviewText = editorText || ref.current.serialize().review;

                (reviewText ? ControllerProduct.Product.Review.add({
                    productId: pId,
                    user_id: userId,
                    reviewText: reviewText,
                    question: question,
                    state: review
                }) : ControllerProduct.Product.Review.stateReview({
                    productId: pId,
                    state: review,
                    question: question,
                })).then(() => {
                    if (_.isBoolean(active)) {
                        ControllerProduct.Product.activeProduct({pId: pId, disable: !active}).finally(() => {
                            mutate(apiKey)
                            setResetTextField(true)
                        })
                        return
                    }
                    mutate(apiKey)
                    setResetTextField(true)
                }).catch(() => {
                    setState({
                        ...state,
                        loading: false
                    })
                })
            } catch (e) {
                setState({
                    ...state,
                    loading: false
                })
            }
        }, 2000)
    }


    useEffect(() => {
        if (resetTextField)
            setTimeout(() => {
                setResetTextField(false)
            }, 1000)
    }, [resetTextField])


    return (
        <Box width={1}
             style={{}}>
            {!error ?
                data ?
                    <Box display={'flex'} flexWrap={'wrap'}>
                        <Box display={'flex'} flexDirection={'column'} width={0.4}>
                            <Box display={'flex'} alignItems={'center'}>
                                {
                                    onClose &&
                                    <IconButton
                                        onClick={(e) => {
                                            onClose(e)
                                        }}
                                        style={{
                                            marginRight: theme.spacing(1),
                                            marginLeft: theme.spacing(1),
                                        }}>
                                        <Close/>
                                    </IconButton>
                                }
                                <Typography variant={"h6"} p={2}>
                                    ثبت بازبینی محصول
                                </Typography>
                            </Box>
                            <Box py={2} display={'flex'}>
                                <Img src={data.data.thumbnail.image} width={200}/>
                                <Box px={2} display={'flex'} flexDirection={'column'}>
                                    <Box py={2} display={'flex'}>
                                        <Tooltip title={data.data.disable ? "غیرفعال" : "فعال"}>
                                            <FiberManualRecordTwoTone
                                                style={{
                                                    color: data.data.disable ? orange[400] : green[400]
                                                }}/>
                                        </Tooltip>
                                        <Typography px={2}>
                                            {data.data.name[siteLang]}
                                        </Typography>
                                    </Box>
                                </Box>
                            </Box>
                        </Box>
                        <Box display={'flex'} flexDirection={'column'}
                             width={0.6}
                             height={'91vh'}
                             pr={2}
                             position={"relative"}>
                            {
                                (data.data.review && _.isArray(data.data.review.chats)) &&
                                <ReviewItems items={data.data.review.chats}/>
                            }
                            {
                                _.isUndefined(question) ?
                                    <Box display={'flex'} alignItems={'center'} justifyContent={'center'} width={1}
                                         p={4}>
                                        <Box display={'flex'} alignItems={'center'} justifyContent={'center'} px={2}
                                             flexWrap={'wrap'}
                                             p={2}
                                             width={0.8}
                                             style={{
                                                 backgroundColor: '#f3f3f3',
                                                 ...UtilsStyle.borderRadius(5)
                                             }}>
                                            <Typography variant={"h6"} pb={2} width={"100%"} alignItems={'center'}
                                                        justifyContent={'center'}>
                                                لطفا نقش خود را انتخاب کنید
                                            </Typography>
                                            <BaseButton variant={"outlined"}
                                                        onClick={() => setQuestion(true)}
                                                        style={{
                                                            borderColor: cyan[300]
                                                        }}>
                                                ایراد گیرنده
                                            </BaseButton>
                                            <Box px={2}>
                                                <BaseButton variant={"outlined"}
                                                            onClick={() => setQuestion(false)}
                                                            style={{
                                                                borderColor: orange[300]
                                                            }}>
                                                    مسئول رسته
                                                </BaseButton>
                                            </Box>
                                        </Box>
                                    </Box> :
                                    <Box display={'flex'}
                                         py={2}
                                         px={1}
                                         style={{
                                             backgroundColor: '#fff',
                                             position: 'sticky',
                                             bottom: 0
                                         }}>
                                        <FormController pt={1} width={1} name={"product_review"} innerref={ref}>
                                            {
                                                _.isString(editorText) ?
                                                    <Tooltip title={"برای حذف دوبار کلیک کنید"}>
                                                        <Box maxHeight={150}
                                                             onDoubleClick={() => {
                                                                 setEditorText(undefined)
                                                             }} style={{overflow: 'auto'}}>
                                                            {UtilsParser.html(editorText)}
                                                        </Box>
                                                    </Tooltip> :
                                                    !resetTextField &&
                                                    <MultiTextField
                                                        variant={"outlined"}
                                                        name={"review"}
                                                        defaultValue={""}
                                                        placeholder={"متن پیام"}
                                                        rows={4}/>
                                            }
                                        </FormController>
                                        <Box minWidth={0.3} pt={1} display={'flex'} flexWrap={'wrap'}>
                                            <Box display={'flex'} width={0.35} flexDirection={'column'} pl={1}>
                                                <Box display={'flex'} alignItems={'center'}
                                                     justifyContent={'center'}>
                                                    <Tooltip title={"ویرایشگر پیشرفته"}>
                                                        <IconButton
                                                            onClick={() => {
                                                                setEditorOpen(true)
                                                            }}>
                                                            <Icon className={"fal fa-underline"}/>
                                                        </IconButton>
                                                    </Tooltip>
                                                </Box>
                                                <Tooltip
                                                    title={review ? (question ? "نیاز به بازبینی دارد" : "نیاز به درخواست بازبینی دارد") : (question ? "نیاز به بازبینی ندارد" : "نیاز به درخواست بازبینی ندارد")}>
                                                    <Box pt={1} display={'flex'} flexDirection={'column'}
                                                         alignItems={'center'}
                                                         justifyContent={'center'}
                                                         onClick={() => {
                                                             setReview(!review)
                                                         }}
                                                         style={{
                                                             cursor: 'pointer'
                                                         }}>
                                                        {
                                                            review ?
                                                                <CheckBox style={{color: orange[400]}}/> :
                                                                <CheckBoxOutlineBlank/>
                                                        }
                                                        <Typography pt={0.2} variant={"body1"}>
                                                            بازبینی
                                                        </Typography>
                                                    </Box>
                                                </Tooltip>
                                            </Box>
                                            <Box display={'flex'} width={0.65} flexDirection={'column'} pl={1}>
                                                <SuccessButton
                                                    variant={"outlined"}
                                                    loading={state.loading}
                                                    onClick={() => handleSave()}
                                                    style={{
                                                        width: '100%'
                                                    }}>
                                                    ذخیره
                                                </SuccessButton>
                                                {
                                                    question &&
                                                    <React.Fragment>
                                                        <SuccessButton
                                                            loading={state.loading}
                                                            onClick={() => handleSave(true)}
                                                            style={{
                                                                marginTop: theme.spacing(0.7),
                                                                width: '100%',
                                                            }}>
                                                            ذخیره و فعالسازی
                                                        </SuccessButton>
                                                        <SuccessButton
                                                            loading={state.loading}
                                                            onClick={() => handleSave(false)}
                                                            style={{
                                                                marginTop: theme.spacing(0.7),
                                                                width: '100%',
                                                                backgroundColor: red[300]
                                                            }}>
                                                            ذخیره و غیرفعالسازی
                                                        </SuccessButton>
                                                    </React.Fragment>
                                                }
                                            </Box>
                                        </Box>
                                    </Box>
                            }
                        </Box>
                        <EditorDialog
                            open={editorOpen}
                            boxId={data.data.box.id}
                            value={_.isString(editorText) ? editorText : ""}
                            onClose={(value) => {
                                setEditorText(value);
                                setEditorOpen(false);
                            }}/>
                        <Backdrop open={state.loading}
                                  style={{
                                      zIndex: '9999'
                                  }}>
                            <CircularProgress color="inherit"
                                              style={{
                                                  color: cyan[300],
                                                  width: 100,
                                                  height: 100
                                              }}/>
                        </Backdrop>
                    </Box> :
                    <PleaseWait/> :
                <React.Fragment>
                    {
                        onClose &&
                        <IconButton
                            onClick={(e) => {
                                onClose(e)
                            }}
                            style={{
                                marginRight: theme.spacing(1),
                                marginLeft: theme.spacing(1),
                            }}>
                            <Close/>
                        </IconButton>
                    }
                    <ComponentError
                        tryAgainFun={() => {
                            mutate(apiKey)
                        }}/>
                </React.Fragment>
            }
        </Box>
    )
}

export function ReviewItems({items = [], ...props}) {
    const [users, setUsers] = useState({})

    function getUser(userId) {
        if (users[userId])
            return;
        ControllerUser.user.get({userId}).then(res => {
            let newUsers = users;
            newUsers[userId] = res.user;
            setUsers({...newUsers});
        })
    }

    useEffect(() => {
        setTimeout(() => {
            tryIt(() => {
                if (items) {
                    const objDiv = document.getElementById("chats_div");
                    objDiv.scrollTop = objDiv.scrollHeight + 200;
                }
            })
        }, 1000)
    }, [items])

    return (
        <Box display={'flex'} flexDirection={'column'}
             id={"chats_div"}
             pt={2}
             {...props}
             style={{
                 overflowY: 'scroll',
                 ...props.style,
             }}>
            {
                items.map((it, i) => {
                    const isHtml = it.text.startsWith("<p>")
                    let user = users[it.user_id];
                    return (
                        <Box key={it.id}
                             px={1} width={1} display={'flex'}
                             alignItems={'flex-start'}
                             pb={2}
                             flexDirection={it.question ? "row-reverse" : "row"}>
                            <Tooltip disable={Boolean(user)}
                                     title={"برای نمایش کاربر کلیک کنید"}>
                                <Box
                                    pr={it.question ? 0 : 0.5}
                                    pl={it.question ? 0.5 : 0}
                                    display={'flex'}
                                    flexDirection={'column'}
                                    alignItems={'center'}
                                    justifyConetnt={'center'}
                                    width={75}
                                    onClick={(e) => {
                                        e.stopPropagation()
                                        getUser(it.user_id)
                                    }}
                                    style={{
                                        cursor: user ? "default" : 'pointer'
                                    }}>
                                    <Img
                                        src={(user && user.avatar) ? user.avatar : userAvatar}
                                        width={40} minHeight={1}
                                        style={{
                                            ...UtilsStyle.borderRadius("50%")
                                        }}/>
                                    <Typography variant={"body2"} pt={0.5}
                                                alignItems={"center"}
                                                justifyContent={'center'}
                                                style={{
                                                    textAlign: "center",
                                                }}>
                                        {(user && user.fullName) ? user.fullName : it.user_id}
                                    </Typography>
                                </Box>
                            </Tooltip>
                            <Box minWidth={0.4} maxWidth={0.7} display={'flex'}
                                 flexDirection={'column'}
                                 position={'relative'}>
                                <Box
                                    p={2}
                                    style={{
                                        backgroundColor: it.question ? cyan[300] : green[300],
                                        ...UtilsStyle.borderRadius(5)
                                    }}>
                                    <Typography display={isHtml ? "block" : undefined}
                                                whiteSpace={!isHtml}
                                                variant={"body1"} color={"#fff"}>
                                        {isHtml ? UtilsParser.html(it.text) : it.text}
                                    </Typography>
                                </Box>
                                <Typography variant={"caption"} px={1} pt={0.2}
                                            dir={"ltr"}
                                            style={{
                                                position: 'absolute',
                                                bottom: -16,
                                                left: it.question ? 0 : undefined
                                            }}>
                                    {JDate.timestampFormat(it.created_at, "jYYYY/jM/jD HH:mm")}
                                </Typography>
                            </Box>
                        </Box>
                    )
                })
            }
        </Box>
    )
}
