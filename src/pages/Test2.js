import React, {useState} from "react";
import Box from "@material-ui/core/Box";
import IconSelector from "../components/icon/IconSelector";
import FeaturesGroupSingle from "./featuresGroup/FeaturesGroupSingle";


export default React.memo(function Test2 (props) {

    const [open, setOpen] = useState(false)


    return (
        <Box display={'flex'} width={1} flexWrap={'wrap'}>
            <IconSelector
                open={true}
                onSelect={() => {
                }}/>
            <FeaturesGroupSingle
                featureGroupId={1}
                open={open}
                onClose={() => {
                    setOpen(false)
                }}/>
        </Box>
    )
})