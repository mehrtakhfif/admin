import React, { Fragment, useEffect, useRef, useState } from "react";
import Box from "@material-ui/core/Box";
import ControllerProduct from "../../controller/ControllerProduct";
import { useSnackbar } from "notistack";
import SelectBox from "../../components/SelectBox";
import { useSelector } from "react-redux";
import _ from "lodash";
import {
  CloseOutlined,
  DeleteOutlined,
  NavigateBefore,
  PeopleAltOutlined,
  PersonAddOutlined,
} from "@material-ui/icons";
import { activeLang, lang, siteLang, theme } from "../../repository";
import Skeleton from "@material-ui/lab/Skeleton";
import PleaseWait from "../../components/base/loading/PleaseWait";
import { Card, useTheme } from "@material-ui/core";
import Typography from "../../components/base/Typography";
import Divider from "@material-ui/core/Divider";
import Collapse from "@material-ui/core/Collapse";
import BaseButton from "../../components/base/button/BaseButton";
import SuccessButton from "../../components/base/button/buttonVariant/SuccessButton";
import { cyan, green, grey, orange, red } from "@material-ui/core/colors";
import Link from "../../components/base/link/Link";
import { UtilsStyle } from "../../utils/Utils";
import StickyBox from "react-sticky-box";
import Button from "@material-ui/core/Button";
import {
  convertFeatures,
  convertFeaturesGroup,
  createMultiLanguage,
} from "../../controller/converter";
import { serverFileTypes } from "../../controller/type";
import rout from "../../router";
import { Redirect } from "react-router-dom";
import ComponentError from "../../components/base/ComponentError";
import TextFieldMultiLanguageContainer from "../../components/base/textField/TextFieldMultiLanguageContainer";
import {
  errorList,
  notValidErrorTextField,
} from "../../components/base/textField/TextFieldContainer";
import PermalinkTextField from "../../components/base/textField/PermalinkTextField";
import SaveDialog from "../../components/dialog/SaveDialog";
import FormControl from "../../components/base/formController/FormController";
import TextField from "../../components/base/textField/TextField";
import UploadItem from "../../components/base/uploader/UploadItem";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Switch from "@material-ui/core/Switch";
import SelectFeaturesGroup, {
  Item as FeatureGroupItem,
} from "../featuresGroup/SelectFeaturesGroup";
import { gcError, gcLog, getSafe } from "../../utils/ObjectUtils";
import FeatureGroup from "../../components/category/FeatureGroup";
import TreeViewComponent from "../../components/category/TreeViewComponent";

export default function CategorySingle({ onClose, box, catId, ...props }) {
  catId = _.toInteger(props?.match?.params?.catId);
  const { boxes } = useSelector((state) => state.user);

  const ref = useRef();
  const [nameRef, setNameRef] = useState();

  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const [catThumbnail, setCatThumbnail] = useState({});
  const [rootCategory, setRootCategory] = useState(box ? box : null);
  const [category, setCategory] = useState({
    disable: false,
    name: createMultiLanguage(),
    permalink: "",
    feature_groups: [],
  });
  const [disable, setDisable] = useState(false);
  const [redirect, setRedirect] = useState(false);
  const [saveDialog, setSaveDialog] = useState(false);
  const [loading, setLoading] = useState(false);

  const [categories, setCategories] = useState(false);
  const [seoTitleText, setSeoTitleText] = useState();
  const [seoDescriptionText, setSeoDescriptionText] = useState();

  useEffect(() => {
    if (catId) {
      ControllerProduct.Categories.getCategory({ catId: catId }).then((res) => {
        const b = _.find(boxes, (b) => b.id === res.data.boxId);
        setRootCategory(b);
        gcLog("setCategory -useEffect", res);
        setCategory((s) => res.data);
        setSeoTitleText(res.data?.name?.seoTitle);
        setSeoDescriptionText(res.data?.name?.seoDescription);
        setCatThumbnail(res.data.media ? res.data.media : {});
        getCategories(res.data.parent.id);
      });
      return;
    }
    // setCategories(
    //     boxes
    // )
    // getCategories()
  }, []);

  function getCategories(id) {
    if (!id) return;
    ControllerProduct.Categories.getAllCategories({})[1]({ boxId: id })
      .then((res) => {
        setCategories(res.categories);
      })
      .catch((e) => {
        gcLog("get Categories Error", e);
        setCategories(false);
      });
  }

  function handleSaveCategory() {
    if (ref.current.hasError()) {
      let errorText = "فرم مشکل دارد";
      try {
        const el = ref.current.getErrorElement();
        const t = el.getAttribute(notValidErrorTextField);
        if (t) errorText = t;
        el.focus();
      } catch (e) {}
      reqCancel(errorText);
      return;
    }

    setDisable(true);

    const { name, permalink, ...fp } = ref.current.serialize();

    setTimeout(() => {
      ControllerProduct.Categories.updateCategory({
        id: category.id,
        boxId: rootCategory.id,
        parentId: category.parent ? category.parent.id : null,
        name: {
          ...name,
          seoTitle: seoTitleText,
          seoDescription: seoDescriptionText,
        },
        permalink: permalink,
        mediaId: catThumbnail.id,
      })
        .then((res) => {
          if (onClose) {
            onClose(res.data);
            setDisable(false);
            return;
          }
          setDisable(false);
          setRedirect(!catId);
          setSaveDialog(false);
        })
        .catch((e) => {
          setDisable(false);
          setSaveDialog(false);
          gcError("Error Saving Category", e);
          enqueueSnackbar("مشکلی به وجود آمده", {
            variant: "error",
            action: (key) => (
              <Fragment>
                <Button
                  onClick={() => {
                    closeSnackbar(key);
                  }}
                >
                  {lang.get("close")}
                </Button>
              </Fragment>
            ),
          });
        });
    }, 1100);
  }

  function reqCancel(text = null) {
    if (text)
      enqueueSnackbar(text, {
        variant: "error",
        action: (key) => (
          <Button
            onClick={() => {
              closeSnackbar(key);
            }}
          >
            {lang.get("close")}
          </Button>
        ),
      });
    setLoading(false);
  }

  function handleActiveStorage() {
    setLoading(true);

    ControllerProduct.Categories.activeCategory({
      catId: catId,
      disable: !category.disable,
    })
      .then((res) => {
        setCategory((s) => ({ ...s, disable: !s.disable }));
        setLoading(false);
      })
      .catch(() => {
        setLoading(false);
      });
  }

  if (redirect)
    return (
      <Redirect
        to={{ pathname: _.isString(redirect) ? redirect : rout.Category.rout }}
      />
    );

  if (onClose)
    return (
      <Typography variant={"h5"} py={2} pr={5} pl={2}>
        {lang.get("category")}:
      </Typography>
    );

  if (rootCategory === null)
    return (
      <SelectBox
        onBoxSelected={(box) => {
          setRootCategory(box);
        }}
      />
    );



  if (category)
    return (
      <Box display={"flex"} py={2} width={1} px={5} pt={7} alignItems="stretch">
        <Box width={"65%"} pr={2} display={"flex"} flexDirection={"column"}>
          <Box component={Card} p={3} my={2}>
                {
                    category?.name?.seoTitle&&
                    <TextField
                    style={{ marginTop: "16px" }}
                      fullWidth={true}
                      variant={"outlined"}
                      label={"عنوان سئو"}
                      defaultValue={category?.name?.seoTitle}
                      onChange={(e) => {
                        setSeoTitleText(e.target.value);
                      }}
                    />
                }
            
            <TextField
              style={{ marginTop: "16px" }}
              variant={"outlined"}
              multiline={true}
              fullWidth={true}
              rows={5}
              rowsMax={5}
              label={"توضیحات سئو"}
              defaultValue={category?.name?.seoDescription}
              onChange={(e) => {
                setSeoDescriptionText(e.target.value);
              }}
            />
          </Box>
          {
            <TreeViewComponent
              categories={categories}
              activeCategory={category.parent}
              rootCategory={rootCategory}
              catId={catId}
              onActiveCategoryChange={(cat) => {
                if (cat && cat.id === catId) return;
                setCategory((s) => ({ ...s, parent: cat }));
              }}
            />
          }
          <FeatureGroup
            boxId={rootCategory.id}
            catId={catId}
            featureGroups={category.feature_groups}
          />
        </Box>
        <Box width={"35%"} pl={2}>
          {category ? (
            <StickyBox offsetTop={80} offsetBottom={20}>
              <Box component={Card}>
                {catId && (
                  <Box
                    display={"flex"}
                    alignItems={"center"}
                    justifyContent={"center"}
                    p={1}
                  >
                    <Box display={"flex"} justifyContent={"center"} width={1}>
                      <FormControlLabel
                        control={
                          <Switch
                            checked={!category.disable}
                            onChange={handleActiveStorage}
                            color="primary"
                          />
                        }
                        label={
                          <Typography variant={"body2"}>
                            فعال سازی دسته‌بندی
                          </Typography>
                        }
                        labelPlacement={"top"}
                      />
                    </Box>
                  </Box>
                )}
                <Box py={2}>
                  <UploadItem
                    boxId={rootCategory.id}
                    holderWidth={200}
                    src={catThumbnail ? catThumbnail.image : ""}
                    type={serverFileTypes.Image.Category.type}
                    width={serverFileTypes.Image.Category.width}
                    height={serverFileTypes.Image.Category.height}
                    onSelected={(file) => {
                      setCatThumbnail(file);
                    }}
                  />
                </Box>
                <FormControl
                  innerref={ref}
                  px={2}
                  py={2}
                  display={"flex"}
                  flexDirection={"column"}
                >
                  <TextFieldMultiLanguageContainer
                    name={"name"}
                    render={(
                      ref,
                      {
                        lang: inputLang,
                        name: inputName,
                        initialize,
                        valid,
                        errorIndex,
                        setValue,
                        inputProps,
                        style,
                        props,
                      }
                    ) => {
                      if (inputLang.key === "fa") {
                        setNameRef(ref);
                      }
                      return (
                        <TextField
                          {...props}
                          error={!valid}
                          variant="outlined"
                          name={inputName}
                          defaultValue={category?.name[siteLang]}
                          fullWidth
                          helperText={errorList[errorIndex]}
                          inputRef={ref}
                          required={inputLang.key === siteLang}
                          label={"نام"}
                          style={style}
                          inputProps={{
                            ...inputProps,
                          }}
                        />
                      );
                    }}
                  />
                  <Box pt={2}>
                    <PermalinkTextField
                      name={"permalink"}
                      syncedRef={nameRef}
                      defaultValue={category.permalink}
                      inputProps={{
                        fullWidth: true,
                      }}
                    />
                  </Box>
                  <Box pt={2} display={"flex"}>
                    <SuccessButton
                      loading={loading}
                      onClick={() => {
                        setSaveDialog(true);
                      }}
                    >
                      {lang.get(catId ? "save" : "add")}
                    </SuccessButton>
                    {onClose && (
                      <Box pr={1}>
                        <BaseButton
                          disabled={loading}
                          variant={"text"}
                          onClick={() => onClose()}
                        >
                          {lang.get("close")}
                        </BaseButton>
                      </Box>
                    )}
                  </Box>
                </FormControl>
              </Box>
            </StickyBox>
          ) : (
            <PleaseWait />
          )}
        </Box>
        <SaveDialog
          text={
            <Box display={"flex"} flexDirection={"column"} px={3}>
              <Typography variant={"h6"} fontWeight={400} pb={2}>
                ذخیره دسته
              </Typography>
              <Typography variant={"body1"}>
                آیا از ذخیره این دسته اطمینان دارید؟
              </Typography>
              <Typography
                variant={"caption"}
                color={theme.palette.text.secondary}
                pt={0.5}
              >
                <span
                  style={{
                    color: orange[300],
                    fontWeight: 600,
                    paddingLeft: theme.spacing(0.3),
                  }}
                >
                  توجه:
                </span>
                در صورت ذخیره، اطلاعات تغییریافته قابل بازگشت نمیباشد.
              </Typography>
            </Box>
          }
          open={saveDialog}
          submitText={lang.get(catId ? "save" : "add")}
          onCancel={() => {
            setSaveDialog(false);
          }}
          onSubmit={() => {
            setSaveDialog(false);
            handleSaveCategory();
          }}
        />
      </Box>
    );

  return <PleaseWait />;
}

//region parent

export const getActiveParents = (activesId, categories) => {
  if (!_.isArray(activesId)) return [];
  const list = {};
  _.forEach(activesId, (id) => {
    const p = getParents(id, categories);
    _.forEach(p, (pa) => {
      list[pa.id] = pa;
    });
  });

  return _.values(list);
};

const getParents = (activeId, categories) => {
  let parents = [];
  _.forEach(categories, (cat) => {
    if (cat.id === activeId) {
      parents.push(cat);
      return false;
    }
    if (cat.child) {
      const res = getParents(activeId, cat.child);
      if (!_.isEmpty(res)) {
        parents = [...parents, ...res, cat];
        return false;
      }
    }
  });
  return parents;
};

//endregion parent

//region FeatureGroup
//endregion FeatureGroup
