import React, {useEffect, useState} from "react";
import SelectBox from "../../components/SelectBox";
import Box from "@material-ui/core/Box";
import BaseDialog from "../../components/base/BaseDialog";
import _ from "lodash";
import ControllerProduct from "../../controller/ControllerProduct";
import useSWR, {mutate} from "swr";
import PleaseWait from "../../components/base/loading/PleaseWait";
import ComponentError from "../../components/base/ComponentError";
import ButtonBase from "@material-ui/core/ButtonBase";
import {Card} from "@material-ui/core";
import {gcLog} from "../../utils/ObjectUtils";
import Typography from "../../components/base/Typography";
import {DEBUG, siteLang} from "../../repository";
import {cyan} from "@material-ui/core/colors";
import Tooltip from "../../components/base/Tooltip";
import IconButton from "@material-ui/core/IconButton";
import {ArrowForward, ChildCare} from "@material-ui/icons";
import SuccessButton from "../../components/base/button/buttonVariant/SuccessButton";
import BaseButton from "../../components/base/button/BaseButton";

export default function ({open, boxId: b, multiSelect, onClose, ...props}) {
    const [boxId, setBoxId] = useState(b)
    const [selectedCat, setSelectedCat] = useState([]);
    const [parents, setParents] = useState([])
    const d = ControllerProduct.Categories.get({
        boxId: boxId,
        step: 100,
        parentId: _.isEmpty(parents) ? undefined : parents[parents.length - 1].id
    })
    const {data, error} = useSWR(d[0] + open, () => {
        if (_.isBoolean(open) && !open) {
            return undefined
        }
        return d[1]()
    });

    useEffect(() => {
        // if (DEBUG && data) {
        //     const d = data.data;
        //     onClose([d[0], d[1], d[3], d[4], d[5], d[6], d[7]])
        // }
    },[data])

    const el = (
        boxId ?
            !error ?
                data ?
                    <Box display={'flex'} flexWrap={'wrap'}>
                        {
                            !_.isEmpty(parents) &&
                            <Box width={1} px={3} pb={1}>
                                <ButtonBase onClick={() => {
                                    const nList = parents;
                                    nList.splice(nList.length - 1, 1)
                                    setParents([...nList])
                                }}>
                                    <Box display={'flex'} alignItems={"center"} justifyContent={'center'}>
                                        <ArrowForward/>
                                        <Typography px={1}>
                                            بازگشت
                                        </Typography>
                                    </Box>
                                </ButtonBase>
                            </Box>
                        }
                        {data.data.map(cat => (
                            <Item
                                key={cat.id}
                                selected={_.findIndex(selectedCat, c => c.id === cat.id) !== -1}
                                category={cat}
                                onClick={() => {
                                    const index = _.findIndex(selectedCat, c => c.id === cat.id);
                                    if (index === -1) {
                                        setSelectedCat([...selectedCat, cat])
                                        return
                                    }
                                    const nList = selectedCat;
                                    nList.splice(index, 1)
                                    setSelectedCat([...nList])
                                }}
                                onShowChildClick={() => {
                                    setParents([...parents, cat])
                                }}/>
                        ))}
                        <Box width={1} px={2} py={1} display={'flex'}>
                            <SuccessButton
                                variant={"outlined"}
                                disabled={_.isEmpty(selectedCat)}
                                onClick={() => {
                                    onClose(selectedCat)
                                }}>
                                ذخیره
                            </SuccessButton>
                            <Box px={2}>
                                <BaseButton variant={"outlined"}
                                            onClick={() => {
                                                onClose()
                                            }}>
                                    لغو
                                </BaseButton>
                            </Box>
                        </Box>
                    </Box> :
                    <PleaseWait/> :
                <ComponentError tryAgainFun={() => mutate(d[0])}/> :
            <SelectBox
                onBoxSelected={(b) => {
                    try {
                        setBoxId(b.id)
                    } catch (e) {
                    }
                }}/>
    )
    return (
        _.isBoolean(open) ?
            <BaseDialog maxWidth={"lg"} open={open} onClose={onClose}>
                <Box width={1} py={4}>
                    {el}
                </Box>
            </BaseDialog> :
            el
    )
}

function Item({category: cat, selected, onClick, onShowChildClick}) {

    return (
        <Box width={1 / 3} p={1}>
            <Box component={Card} width={1}>
                <ButtonBase onClick={onClick}
                            style={{
                                width: "100%",
                                backgroundColor: selected ? cyan[500] : undefined,
                            }}>
                    <Typography py={4} variant={"h6"} color={selected ? "#fff" : undefined}>
                        {cat.name[siteLang]}
                    </Typography>
                    {
                        cat.childCount > 0 &&
                        <Box ml={1}>
                            <Tooltip title={"نمایش فرزندان"}>
                                <IconButton onClick={(e) => {
                                    e.stopPropagation()
                                    onShowChildClick()
                                }}>
                                    <ChildCare style={{
                                        color: selected ? "#fff" : undefined
                                    }}/>
                                </IconButton>
                            </Tooltip>
                        </Box>
                    }
                </ButtonBase>
            </Box>
        </Box>
    )
}
