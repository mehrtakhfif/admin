import React, {Fragment, useEffect, useRef, useState} from "react";
import Box from "@material-ui/core/Box";
import DefaultTextField from "../../components/base/textField/DefaultTextField";
import {gcLog} from "../../utils/ObjectUtils";
import FormController from "../../components/base/formController/FormController";
import useSWR from "swr/esm/use-swr";
import ControllerProduct from "../../controller/ControllerProduct";
import PleaseWait from "../../components/base/loading/PleaseWait";
import ComponentError from "../../components/base/ComponentError";
import {mutate} from "swr";
import {lang, siteLang} from "../../repository";
import Typography from "../../components/base/Typography";
import Card from "@material-ui/core/Card";
import BaseButton from "../../components/base/button/BaseButton";
import ButtonBase from "@material-ui/core/ButtonBase";
import Img from "../../components/base/img/Img";
import {Utils, UtilsData, UtilsStyle} from "../../utils/Utils";
import UploadItem from "../../components/base/uploader/UploadItem";
import {serverFileTypes} from "../../controller/type";
import SelectBox from "../../components/SelectBox";
import SuccessButton from "../../components/base/button/buttonVariant/SuccessButton";
import _ from "lodash";
import DefaultTextFieldMultiLanguage from "../../components/base/textField/DefaultTextFieldMultiLanguage";
import {createMultiLanguage} from "../../controller/converter";
import {useSnackbar} from "notistack";
import Tooltip from "../../components/base/Tooltip";
import Button from "@material-ui/core/Button";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Switch from "@material-ui/core/Switch";
import Backdrop from "@material-ui/core/Backdrop";
import CircularProgress from "@material-ui/core/CircularProgress";
import {cyan, orange} from "@material-ui/core/colors";
import {notValidErrorTextField} from "../../components/base/textField/TextFieldContainer";
import {useSelector} from "react-redux";


export default function ({catId, ...props}) {
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();
    const ref = useRef();
    const {boxes} = useSelector(state => state.user);
    catId = catId ? _.toInteger(catId) : (props && props.match && props.match.params && props.match.params.catId) ? _.toInteger(props.match.params.catId) : undefined;
    const [item, setItem] = useState(catId ? undefined : {
        name: createMultiLanguage(),
        disable: false
    })
    const [catName, setCatName] = useState("");
    const d = ControllerProduct.Categories.search({query: catName});
    const {data, error} = useSWR(...d)
    const [state, setState] = useState({
        loading: false,
        box: undefined,
    })
    const [catThumbnail, setCatThumbnail] = useState(undefined)
    const [selectedCat, setSelectedCat] = useState(undefined)

    useEffect(() => {
        if (catId) {
            ControllerProduct.Categories.getCategory({catId: catId}).then(res => {
                const b = _.find(boxes, (b) => b.id === res.data.boxId);
                setState({
                    ...state,
                    box: b,
                });
                setItem(res.data)

                setSelectedCat(res.data.parent)
                setCatThumbnail(res.data.media ? res.data.media : {})

            })
        }
    }, [catId]);

    useEffect(() => {
        if (!selectedCat)
            return;
        if (!catThumbnail && selectedCat.media) {
            setCatThumbnail(selectedCat.media)
        }
        const newItem = item;
        if (!item.name[siteLang] && selectedCat.name) {
            newItem.name = selectedCat.name
        }
        setItem({
            ...newItem
        })
    }, [selectedCat])


    function handleChange() {
        try {
            setCatName(ref.current.serialize().cat_name)
        } catch (e) {
        }
    }

    function handleActiveStorage() {
        setState({
            ...state,
            loading: true
        });

        ControllerProduct.Categories.activeCategory({catId: catId, disable: !item.disable}).then(res => {

            if (res.status === 202) {
                setItem({
                    ...item,
                    disable: !item.disable
                })
            }
        }).finally(() => {
            setState({
                ...state,
                loading: false
            })
        })
    }


    function handleSaveCategory() {
        if (ref.current.hasError() || !selectedCat) {
            let errorText = !selectedCat ? "لطفا یک دسته‌بندی انتخاب کنید" : "فرم مشکل دارد";
            try {
                const el = ref.current.getErrorElement();
                const t = el.getAttribute(notValidErrorTextField);
                if (t)
                    errorText = t;
                el.focus();
            } catch (e) {
            }
            reqCancel(errorText);
            return;
        }
        setState({
            ...state,
            loading: true
        });

        const {name, ...fp} = ref.current.serialize();

        setTimeout(() => {
            ControllerProduct.Categories.updateCategory({
                id: item.id,
                boxId: state.box.id,
                parentId: selectedCat.id,
                name: name,
                mediaId: catThumbnail ? catThumbnail.id : undefined
            }).then((res) => {
                setState({
                    ...state,
                    loading: false,
                    redirect: !catId,
                    saveDialog: false
                })
            }).catch(() => {
                setState({
                    ...state,
                    disable: false,
                    saveDialog: false
                });
            })
        }, 1100)
    }


    function reqCancel(text = null) {
        if (text)
            enqueueSnackbar(text,
                {
                    variant: "error",
                    action: (key) => (
                        <Button onClick={() => {
                            closeSnackbar(key)
                        }}>
                            {lang.get('close')}
                        </Button>
                    )
                });

        setState({
            loading: false
        })

    }


    return (
        item ?
            state.box ?
                <FormController
                    name={"cat"}
                    innerref={ref} py={4} px={2} display={'flex'}
                    checkInterval={300}
                    onChange={handleChange}
                    onSubmit={handleChange}>
                    <Box
                        display={'flex'}
                        flexDirection={'column'}
                        width={0.7}
                        pr={1}>
                        {selectedCat &&
                        <Box display={'flex'} mb={2} p={2}
                             style={{
                                 border: `1px solid ${orange[400]}`,
                                 ...UtilsStyle.borderRadius(5)
                             }}>
                            {selectedCat.media &&
                            <Tooltip title={"استفاده به عنوان عکس دسته"}>
                                <ButtonBase
                                    onClick={() => {
                                        setCatThumbnail({
                                            ...selectedCat.media
                                        })
                                    }}>
                                    <Img width={'auto'} height={60} minHeight={1} src={selectedCat.media.image}
                                         style={{
                                             ...UtilsStyle.borderRadius(5)
                                         }}/>
                                </ButtonBase>
                            </Tooltip>
                            }
                            <Box px={2}>
                                <Tooltip title={"کپی"}>
                                    <ButtonBase
                                        onClick={() => {
                                            if (Utils.copyToClipboard(selectedCat.name[siteLang])) {
                                                enqueueSnackbar("کپی شد.",
                                                    {
                                                        variant: "success",
                                                        action: (key) => (
                                                            <Fragment>
                                                                <Button onClick={() => {
                                                                    closeSnackbar(key)
                                                                }}>
                                                                    {lang.get('close')}
                                                                </Button>
                                                            </Fragment>
                                                        ),
                                                        anchorOrigin: {
                                                            vertical: 'bottom',
                                                            horizontal: 'left',
                                                        },
                                                    });
                                            }
                                        }}>
                                        <Typography variant={"body1"}>
                                            {selectedCat.name[siteLang]}
                                        </Typography>
                                    </ButtonBase>
                                </Tooltip>
                            </Box>
                            <Box display={"flex"} flexDirection={'column'}>
                                {selectedCat.box &&
                                <Typography variant={"caption"}>
                                    رسته:
                                    {selectedCat.box.name[siteLang]}
                                </Typography>
                                }
                                {selectedCat.parent &&
                                <Typography variant={"caption"}>
                                    والد:
                                    {selectedCat.parent.name[siteLang]}
                                </Typography>
                                }
                            </Box>
                        </Box>}
                        <Box pb={2}>
                            <DefaultTextField
                                label={"جستجو دسته‌بندی"}
                                defaultValue={""}
                                name={"cat_name"}
                                variant={"outlined"}
                            />
                        </Box>
                        <Box display={'flex'} flexWrap={'wrap'}>
                            {!error ? data ?
                                <React.Fragment>
                                    {
                                        data.data.categories.map(cat => {
                                            return (
                                                <Box p={1}>
                                                    <Box component={Card}>
                                                        <ButtonBase
                                                            onClick={() => {
                                                                setSelectedCat(cat)
                                                            }}
                                                            style={{
                                                                padding: 0
                                                            }}>
                                                            <Box display={'flex'} px={2}
                                                                 py={1}>
                                                                <Box pr={2}>
                                                                    <Img height={50} minHeight={1} width={"auto"}
                                                                         src={cat.media ? cat.media.image : ""}
                                                                         style={{
                                                                             ...UtilsStyle.borderRadius(5)
                                                                         }}/>
                                                                </Box>
                                                                <Box display={'flex'} flexDirection={'column'}>
                                                                    <Typography variant={"body1"}>
                                                                        {cat.name[siteLang]}
                                                                    </Typography>
                                                                    {cat.parent &&
                                                                    <Typography pt={1} variant={"caption"}>
                                                                        والد:
                                                                        {cat.parent.name[siteLang]}
                                                                    </Typography>
                                                                    }
                                                                    {cat.box &&
                                                                    <Typography pt={1} variant={"caption"}>
                                                                        رسته:
                                                                        {cat.box.name[siteLang]}
                                                                    </Typography>
                                                                    }
                                                                </Box>
                                                            </Box>
                                                        </ButtonBase>
                                                    </Box>
                                                </Box>
                                            )
                                        })
                                    }
                                </React.Fragment> :
                                <PleaseWait/> :
                                <ComponentError
                                    tryAgainFun={() => mutate(d[0])}/>}

                        </Box>
                    </Box>
                    <Box pl={1} flex={1}>
                        <Box component={Card} p={2} display={'flex'} flexDirection={'column'}>
                            {catId &&
                            <Box display={'flex'} alignItems={'center'}
                                 justifyContent={'center'} p={1}>
                                <Box display={'flex'} justifyContent={'center'} width={1}>
                                    <FormControlLabel
                                        control={
                                            <Switch
                                                checked={!item.disable}
                                                onChange={handleActiveStorage}
                                                color="primary"/>
                                        }
                                        label={<Typography
                                            variant={'body2'}>فعال سازی دسته‌بندی</Typography>}
                                        labelPlacement={'top'}/>
                                </Box>
                            </Box>}
                            <Box py={2}>
                                <UploadItem
                                    boxId={state.box.id}
                                    holderWidth={200}
                                    src={(catThumbnail) ? catThumbnail.image : ''}
                                    type={serverFileTypes.Image.Category.type}
                                    width={serverFileTypes.Image.Category.width}
                                    height={serverFileTypes.Image.Category.height}
                                    onSelected={(file) => {
                                        setCatThumbnail(file)
                                    }}/>
                            </Box>
                            <DefaultTextFieldMultiLanguage
                                variant={"outlined"}
                                name={"name"}
                                label={"نام"}
                                defaultValue={item.name}
                                required={true}/>
                            <Box pt={2} display={'flex'}>
                                <SuccessButton
                                    onClick={handleSaveCategory}>
                                    ذخیره
                                </SuccessButton>
                            </Box>
                        </Box>
                    </Box>
                    <Backdrop open={state.loading}
                              style={{
                                  zIndex: '9999'
                              }}>
                        <CircularProgress color="inherit"
                                          style={{
                                              color: cyan[300],
                                              width: 100,
                                              height: 100
                                          }}/>
                    </Backdrop>
                </FormController> :

                <SelectBox onBoxSelected={(box) => setState({
                    ...state,
                    box: box
                })}/> :
            <PleaseWait/>
    )
}
