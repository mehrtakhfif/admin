import React, {useEffect, useState} from "react";
import Box from "@material-ui/core/Box";
import Table, {createTableHeader, headerItemTemplate} from "../../components/base/table/Table";
import {createFilter} from "../../components/base/table/Filters";
import DataUtils, {filterType} from "../../utils/DataUtils";
import {useSelector} from "react-redux";
import _ from "lodash";
import storage from "../../storage";
import ControllerProduct from "../../controller/ControllerProduct";
import {ApiHelper} from "../../controller/converter";
import useSWR, {mutate} from "swr";
import Tooltip from "@material-ui/core/Tooltip";
import {activeLang, lang} from "../../repository";
import IconButton from "@material-ui/core/IconButton";
import Link from "../../components/base/link/Link";
import {AccountTreeOutlined, EditOutlined, FiberManualRecordTwoTone} from "@material-ui/icons";
import rout from "../../router";
import ComponentError from "../../components/base/ComponentError";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import Typography from "../../components/base/Typography";
import {useTheme} from "@material-ui/core";
import LocalStorageUtils from "../../utils/LocalStorageUtils";
import {green, red} from "@material-ui/core/colors";
import { gcError } from "../../utils/ObjectUtils";



const headers = [
    ...headerItemTemplate.idActionName(),
    createTableHeader({id: "disable", type: 'str', label: 'فعال'}),
    createTableHeader({id: 'catCount', label: 'تعداد محصولات', type: filterType.text, sortable: false, dir: 'ltr',}),
    createTableHeader({
        id: 'catChildProductCount',
        label: 'تعداد محصولات فرزندان',
        type: filterType.text,
        sortable: false,
        dir: 'ltr',
    }),
    ...headerItemTemplate.createByUpdatedBy(),
];
const filters = [
    createFilter({id: 'name__fa', type: filterType.text, label: 'نام', value: ''}),
];
const CookieKey = "categories";

export default function ({...props}) {
    //region var
    const theme = useTheme()
    const {boxes} = useSelector(state => state.user);
    //region state
    const [state, setState] = useState({
        filters: _.cloneDeep(filters),
        activeBox: storage.PageSetting.getActiveBox(CookieKey, boxes),
        order: storage.PageSetting.getOrder(CookieKey),
        parent: [],
        pagination: {
            rowPerPage: storage.PageSetting.getRowPerPage(CookieKey),
            page: 0
        }
    });

    
    const [filtersState, setFiltersState] = useState({
        data: filters,
        resetFilters: () => {
            setFiltersState({
                ...filtersState,
                data: DataUtils().TableFilter().resetFilter({filters: filtersState.data}),
            })
        }
    });

    //endregion state
    //endregion var

    //region useEffect
    useEffect(() => {
        if (!state.activeBox)
            return;
        requestFilters();
        setState({
            ...state,
            parent: LocalStorageUtils.getArray(`cat-b${state.activeBox}`)
        })
    }, [state.activeBox]);

    useEffect(() => {
        storage.PageSetting.setPage(CookieKey, {page: state.pagination.page});
    }, [state.pagination.page]);

    useEffect(() => {
        if (!state.activeBox)
            return;
        LocalStorageUtils.set(`cat-b${state.activeBox}`, state.parent);
    }, [state.parent]);
    //endregion useEffect
    //region requests


    const parent = [];
    _.forEach(state.parent, (p) => {
        parent.push(p.id)
    });
    const d = ControllerProduct.Categories.get({
        boxId: state.activeBox,
        page: state.pagination.page + 1,
        step: state.pagination.rowPerPage,
        order: state.order,
        filters: state.filters,
        parentId: !_.isEmpty(parent) ? parent[parent.length - 1] : undefined
    });


    const {data, error} = useSWR(...d);


    function requestFilters() {
        ControllerProduct.Categories.Filters.get({boxId: state.activeBox}).then((res) => {
            const newList = _.isArray(filtersState.data) && !_.isEmpty(filtersState.data) ? filtersState.data : filters;
            _.forEach(res.data, (fi) => {
                const data = createFilter({id: fi.name, type: fi.type, label: fi.label, data: fi.filters, value: null});
                switch (fi.name) {
                    case 'category': {
                        data.value = [];
                        break;
                    }
                    default: {
                    }
                }

                const index = _.findIndex(newList, (l) => l.id === data.id);
                newList[index !== -1 ? index : newList.length] = data;
            });
            setFiltersState({
                ...filtersState,
                data: [...newList]
            })
        }).catch((e) => {
            setFiltersState({
                ...filtersState,
                data: false
            })
        });
    }

    //endregion requests

    //region handler
    function handleFilterChange({item, value}) {
        setState({
            ...state,
            pagination: {
                ...state.pagination,
                page: 0
            }
        })
        setFiltersState({
            ...filtersState,
            data: DataUtils().TableFilter().filterItemChange({filters: filtersState.data, item: item, value: value})
        });
    }

    function handleApplyFilter() {
        setState({
            ...state,
            filters: _.cloneDeep(filtersState.data)
        })
    }

    function handleResetFilter() {
        filtersState.resetFilters()
    }

    function handleChangeOrder(order) {
        storage.PageSetting.setOrder(CookieKey, {order: order});
        setState({
            ...state,
            order: {
                ...order
            }
        });
    }

    function handleChangeBox(e, box) {
        if (!box || state.activeBox === box.id)
            return;
        storage.PageSetting.setActiveBox(CookieKey, {activeBox: box.id});
        filtersState.resetFilters();
        setState({
            ...state,
            activeBox: box.id,
            pagination: {
                ...state.pagination,
                page: 0
            }
        });
    }

    function handleShowChild(cat) {
        if (_.findIndex(state.parent, p => p.id === cat.id) !== -1)
            return;
        setState({
            ...state,
            parent: [...state.parent, cat],
            pagination: {
                ...state.pagination,
                page: 0
            }
        });
    }

    //endregion handler

    //region renderData
    const pg = state.pagination;


    const tableData = (data && data.data ? data.data : [...Array(state.pagination.rowPerPage)]).map((cat, i) => (
        cat ? {
            id: {
                label: cat.id,
            },
            actions: {
                label: <Box display={'flex'} alignItems={'center'} pl={1}>
                    <Tooltip title={lang.get('edit')}>
                        <IconButton size={"small"}
                                    style={{
                                        marginRight: theme.spacing(0.5),
                                        marginLeft: theme.spacing(0.5)
                                    }}>
                            <Link toHref={cat.link} linkStyle={{display: 'flex'}}
                                  hoverColor={theme.palette.primary.main}>
                                <EditOutlined style={{fontSize: 20, padding: 2}}/>
                            </Link>
                        </IconButton>
                    </Tooltip>
                    <Tooltip title={"نمایش فرزندان"}>
                        <span onClick={(e) => {
                            e.stopPropagation();
                        }}>
                        <IconButton size={"small"}
                                    onClick={(e) => {
                                        e.stopPropagation();
                                        handleShowChild(cat)
                                    }}
                                    disabled={(cat.childCount < 1)}
                                    style={{
                                        marginRight: theme.spacing(0.5),
                                        marginLeft: theme.spacing(0.5)
                                    }}>
                            <Link linkStyle={{display: 'flex'}}
                                  color={cat.childCount > 0 ? theme.palette.text.primary : theme.palette.text.disabled}
                                  hoverColor={theme.palette.primary.main}>
                                <AccountTreeOutlined style={{fontSize: 20, padding: 2}}/>
                            </Link>
                        </IconButton>
                        </span>
                    </Tooltip>
                </Box>
            },
            name: {
                label: cat.name[activeLang],
                link: cat.link
            },
            disable: {
                label: () => {
                    const params = !cat.disable ?
                        {
                            title: 'فعال',
                            color: green[300]
                        }
                        :
                        {
                            title: 'غیرفعال',
                            color: red[300]
                        };

                    return (
                        <Box display={'flex'} flexWrap={'wrap'} justifyContent={'center'} alignItems={'center'}
                             maxWidth={300}>
                            <Tooltip title={params.title}>
                                <FiberManualRecordTwoTone style={{color: params.color}}/>
                            </Tooltip>
                        </Box>
                    )
                },
            },
            catCount: {
                label: cat.productCount,
            },
            catChildProductCount: {
                label: cat.categoryChildProductCount
            },
            created_at: {
                label: cat.createdAt.date,
            },
            created_by: {
                label: cat.createdAt.by.name,
                link: cat.createdAt.by.link,
            },
            updated_at: {
                label: cat.updatedAt.date,
            },
            updated_by: {
                label: cat.updatedAt.by.name,
                link: cat.updatedAt.by.link,
            },
        } : {}
    ));


    //endregion renderData


    if (error) return  <Box>
        <ComponentError
            statusCode={(_.isObject(error) && error.request) ? error.request.status : undefined}
            tryAgainFun={() => mutate(d[0])}/>
    </Box>

    return (
        <Box my={2} px={2}>
                    <Table headers={headers}
                           cookieKey={CookieKey}
                           showActionPanel={true}
                           title={(
                               <React.Fragment>
                                   <Typography variant={'h6'}>{lang.get("categories")}</Typography>
                                   {!_.isEmpty(state.parent) &&
                                   <Breadcrumbs aria-label="breadcrumb"
                                                style={{
                                                    paddingRight: theme.spacing(2),
                                                    display: 'flex',
                                                    alignItems: 'center'
                                                }}>
                                       <Link onClick={() => {
                                           setState({
                                               ...state,
                                               parent: []
                                           })
                                       }}>
                                           همه
                                       </Link>
                                       {state.parent.map((p, index) => (
                                           state.parent.length !== index + 1 ?
                                               <Link key={p.id}
                                                     onClick={() => {
                                                         const newParent = [];
                                                         _.forEach(state.parent, (pr) => {
                                                             newParent.push(pr);
                                                             if (p.id === pr.id)
                                                                 return false
                                                         });
                                                         setState({
                                                             ...state,
                                                             parent: [...newParent]
                                                         })
                                                     }}>
                                                   {p.name[activeLang]}
                                               </Link> :
                                               <Typography key={p.id}
                                                           variant={'body2'}>{p.name[activeLang]}</Typography>
                                       ))}
                                   </Breadcrumbs>}
                               </React.Fragment>
                           )}
                           data={tableData}
                           filters={filtersState.data}
                           onFilterChange={handleFilterChange}
                           loading={false}
                           activePage={pg.page}
                           rowsPerPage={pg.rowPerPage}
                           lastPage={data ? data.pagination.lastPage : 0}
                           rowCount={data ? data.pagination.count : 0}
                           orderType={state.order}
                           activeBox={state.activeBox}
                           addNewButton={{
                               label: lang.get('add_new_category'),
                               link: rout.Category.Single.createNewProduct,
                           }}
                           onActivePageChange={(page) => {
                               setState({
                                   ...state,
                                   pagination: {
                                       ...state.pagination,
                                       page: page
                                   }
                               })
                               // requestData({page: e})
                           }}
                           onRowPerPageChange={(rowPerPage) => {
                               storage.PageSetting.setRowPerPage(CookieKey, {rowPerPage: rowPerPage});
                               setState({
                                   ...state,
                                   pagination: {
                                       ...state.pagination,
                                       rowPerPage: rowPerPage
                                   }
                               })
                           }}
                           onApplyFilterClick={handleApplyFilter}
                           onResetFilterClick={handleResetFilter}
                           onChangeOrder={handleChangeOrder}
                           onChangeBox={handleChangeBox}/>
        </Box>
    )
}
