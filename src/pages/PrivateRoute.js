import React from 'react';
import {useSelector} from "react-redux"
import {Route, Redirect} from "react-router-dom";
import {DEBUG_AUTH} from "../repository";
import rout from "../router";

function PrivateRoute({component: Component, ...rest}) {
    const {user, isLogin, boxes} = useSelector(state => state.user);
    return (
        <Route
            {...rest}
            render={props =>
                DEBUG_AUTH || isLogin ? (
                    <Component {...props} />
                ) : (
                    <Redirect
                        to={{
                            pathname: rout.Main.index,
                            state: {
                                from: props.location,
                            }
                        }}
                    />
                )
            }
        />
    );
}
export default PrivateRoute;