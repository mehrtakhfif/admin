import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
// import TextField from "../components/base/textField/DefaultTextField"
import {Box,Button,TextField} from '@material-ui/core'
import BaseButton from "../components/base/button/BaseButton";
import axios from "axios";
import {gcError, gcLog} from "../utils/ObjectUtils";
import Icon from "@material-ui/core/Icon";


const useStyles = makeStyles((theme) => ({
    root: {
        "& > *": {
            margin: theme.spacing(1),
            width: "25ch"
        }
    }
}));

export default function Test() {
    const [text,setText] = useState("")
    function apiReq(type){
        // send a POST request
        gcLog("url",text)
        try {

        axios({
            method: type,
            baseURL: text,
            data: {
                // firstName: 'Finn',
                // lastName: 'Williams'
            }
        });
        }   catch (e) {
            gcError(`${type} request error`,e)
        }
    }

    return (
        <Box display={'flex'} alignItems={'center'} justifyContent={'center'} height={'100%'}>
            <Icon
                className={`mt mt-qrcode`}
               >
            </Icon>     <Icon
                className={`mt-mt-qrcode `}
               >
            </Icon>     <Icon
                className={`mt-qrcode `}
               >
            </Icon>
        </Box>
    );
}
