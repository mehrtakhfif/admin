import React, {useEffect, useState} from "react";
import Box from "@material-ui/core/Box";
import {useDispatch, useSelector} from "react-redux";
import ControllerProduct from "../../controller/ControllerProduct";
import Table, {createTableHeader, headerItemTemplate} from "../../components/base/table/Table";
import {createFilter} from "../../components/base/table/Filters";
import _ from 'lodash';
import {activeLang, cookieVersion, lang, theme} from "../../repository";
import rout from "../../router";
import DataUtils, {filterType} from "../../utils/DataUtils";
import Link from "../../components/base/link/Link";
import {VisibilityOutlined} from "@material-ui/icons";
import IconButton from "@material-ui/core/IconButton";
import Typography from "../../components/base/Typography";
import Tooltip from "@material-ui/core/Tooltip";
import storage from "../../storage";
import useSWR, {mutate} from 'swr'
import ComponentError from "../../components/base/ComponentError";
import {Review} from "../Dashboard";


const headers = [
    ...headerItemTemplate.idActionName(),
    createTableHeader({id: "features_count", type: 'str', label: 'تعداد فیچرها', sortable: false}),
    ...headerItemTemplate.createByUpdatedBy(),
];
const filters = [
    createFilter({id: 'name__fa', type: filterType.text, label: 'نام', value: ''}),
];
const CookieKey = "feature-group";

export default function ({...props}) {
    //region var
    const dispatch = useDispatch();
    const {boxes} = useSelector(state => state.user);
    const [tableData, setTableData] = useState()

    //region state
    const [filtersState, setFiltersState] = useState({
        data: filters,
        resetFilters: () => {
            setFiltersState({
                ...filtersState,
                data: DataUtils().TableFilter().resetFilter({filters: filtersState.data}),
            })
        }
    });
    const [state, setState] = useState(oldState=>({
        ...oldState,
        filters: _.cloneDeep(filters),
        activeBox: storage.PageSetting.getActiveBox(CookieKey, boxes),
        order: storage.PageSetting.getOrder(CookieKey),
        pagination: {
            rowPerPage: storage.PageSetting.getRowPerPage(cookieVersion),
            page: storage.PageSetting.getPage(cookieVersion)
        }
    }));
    const [thumbnailDialog, setThumbnailDialog] = useState(false);


    function getCookieKey() {
        try {
            return CookieKey + state.activeBox
        } catch (e) {
        }
        return CookieKey + storage.PageSetting.getActiveBox(getCookieKey(), boxes)
    }

    //endregion state
    //endregion var


    //region requests
    const d = ControllerProduct.FeaturesGroup.get({
        boxId: state.activeBox,
        page: state.pagination.page + 1,
        step: state.pagination.rowPerPage,
        order: state.order,
        filters: state.filters,
    });


    const {data: dd, error} = useSWR(d[0], () => {
        if (state.order === undefined) {
            return {}
        }

        return d[1]()
    });



    function requestFilters() {
        ControllerProduct.Products.Filters.get({boxId: state.activeBox}).then((res) => {
            const newList = (_.isArray(filtersState.data) && !_.isEmpty(filtersState.data)) ? filtersState.data : filters;
            _.forEach(res.data, (fi) => {
                const data = createFilter({id: fi.name, type: fi.type, label: fi.label, data: fi.filters, value: null});
                switch (fi.name) {
                    case 'categories': {
                        data.value = [];
                        break;
                    }
                    default: {
                    }
                }

                const index = _.findIndex(newList, (l) => l.id === data.id);
                newList[index !== -1 ? index : newList.length] = data;
            });
            setFiltersState(oldState=>({
                ...oldState,
                data: [...newList]
            }))
        }).catch((e) => {

            setFiltersState(oldState=>({
                ...oldState,
                data: false
            }))
        });
    }

    //endregion requests

    //region useEffect
    useEffect(() => {
        if (!state.activeBox)
            return;
        setState(oldState=>({
            ...oldState,
            activeBox: state.activeBox,
            order: storage.PageSetting.getOrder(getCookieKey()),
            pagination: {
                rowPerPage: storage.PageSetting.getRowPerPage(getCookieKey()),
                page: storage.PageSetting.getPage(getCookieKey())
            }
        }))
        requestFilters();
    }, [state.activeBox]);

    useEffect(() => {
        storage.PageSetting.setPage(getCookieKey(), {page: state.pagination.page});
    }, [state.pagination.page]);

    useEffect(() => {

        setTableData(((dd && dd.data )? dd.data : [...Array(state.pagination.rowPerPage)]).map((pr, i) => {
            const singleRout = rout.FeatureGroup.Single.editFeatureGroup(pr?pr.id : i)
                return pr ? {
                    id: {
                        label: pr.id,
                    },
                    actions: {
                        label:
                            <Box display={'flex'} alignItems={'center'} pl={1}>
                                <Tooltip title={lang.get('show')}>
                                    <IconButton size={"small"}
                                                onClick={(e) => {
                                                    e.stopPropagation();
                                                }}
                                                style={{
                                                    marginRight: theme.spacing(0.5),
                                                    marginLeft: theme.spacing(0.5)
                                                }}>
                                        <Link toHref={singleRout} linkStyle={{display: 'flex'}}
                                              hoverColor={theme.palette.primary.main}>
                                            <VisibilityOutlined style={{fontSize: 20, padding: 2}}/>
                                        </Link>
                                    </IconButton>
                                </Tooltip>
                            </Box>
                    },
                    name: {
                        label: pr.name[activeLang],
                        link: singleRout
                    },
                    features_count: {
                        label: <Typography variant={'h6'} justifyContent={"center"}>{pr.features.length}</Typography>,
                    },
                    updated_at: {
                        label: pr.updatedAt.date,
                    },
                    updated_by: {
                        label: pr.updatedAt.by.name,
                        link: pr.updatedAt.by.link,
                    },
                    created_at: {
                        label: pr.createdAt.date,
                    },
                    created_by: {
                        label: pr.createdAt.by.name,
                        link: pr.createdAt.by.link,
                    },
                } : {}
            }
        ))
    }, [dd])
    //endregion useEffect

    //region handler
    function handleFilterChange({item, value}) {
        setState(oldState=>({
            ...oldState,
            pagination: {
                ...state.pagination,
                page: 0
            }
        }))
        setFiltersState(oldState=>({
            ...oldState,
            data: DataUtils().TableFilter().filterItemChange({filters: filtersState.data, item: item, value: value})
        }));
    }

    function handleApplyFilter() {
        setState(oldState=>({
            ...oldState,
            filters: _.cloneDeep(filtersState.data)
        }))
    }

    function handleResetFilter() {
        filtersState.resetFilters()
    }

    function handleChangeOrder(order) {
        storage.PageSetting.setOrder(getCookieKey(), {
            order: order
        });
        setState(oldState =>({
            ...oldState,
            order: {
                ...order
            }
        }));
    }

    function handleChangeBox(e, box) {
        if (!box || state.activeBox === box.id)
            return;
        storage.PageSetting.setActiveBox(CookieKey, {activeBox: box.id});
        filtersState.resetFilters();
        setState({
            ...state,
            activeBox: box.id,
            filters : {},
            pagination: {
                ...state.pagination,
                page: 0
            }
        });
    }

    //endregion handler

    //region renderData
    const pg = state.pagination;
    //endregion renderData

    return (
        <Box
            my={2}
            px={2}
            display={'flex'}
            flexDirection={'column'}>
            <Box width={1}
                 display={'flex'}>
                <Review mt={1} mb={2}/>
            </Box>
            {(!error && state.order) ?
                <Table headers={headers}
                       cookieKey={getCookieKey()}
                       title={lang.get("products")}
                       data={tableData}
                       filters={filtersState.data}
                       onFilterChange={handleFilterChange}
                       loading={false}
                       activePage={pg.page}
                       rowsPerPage={pg.rowPerPage}
                       lastPage={dd ? dd.pagination.lastPage : 0}
                       rowCount={dd ? dd.pagination.count : 0}
                       orderType={state.order}
                       activeBox={state.activeBox}
                       addNewButton={{
                           label: "افزودن گروه فیچر جدید",
                           link: rout.FeatureGroup.Single.createNewProduct,
                       }}
                       onActivePageChange={(page) => {
                           setState({
                               ...state,
                               pagination: {
                                   ...state.pagination,
                                   page: page
                               }
                           })
                           // requestData({page: e})
                       }}
                       onRowPerPageChange={(rowPerPage) => {
                           storage.PageSetting.setRowPerPage(getCookieKey(), {rowPerPage: rowPerPage});
                           setState({
                               ...state,
                               pagination: {
                                   ...state.pagination,
                                   rowPerPage: rowPerPage
                               }
                           })
                       }}
                       onApplyFilterClick={handleApplyFilter}
                       onResetFilterClick={handleResetFilter}
                       onChangeOrder={handleChangeOrder}
                       onChangeBox={handleChangeBox}/> :
                <Box>
                    <ComponentError
                        statusCode={(_.isObject(error) && error.request) ? error.request.status : undefined}
                        tryAgainFun={() => mutate(d[0])}/>
                </Box>
            }
        </Box>
    )
}
