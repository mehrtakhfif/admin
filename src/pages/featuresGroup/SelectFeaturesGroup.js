import React, {useEffect, useState} from "react";
import BaseDialog from "../../components/base/BaseDialog";
import useSWR, {mutate} from "swr";
import ControllerProduct from "../../controller/ControllerProduct";
import Box from "@material-ui/core/Box";
import BaseButton from "../../components/base/button/BaseButton";
import IconButton from "@material-ui/core/IconButton";
import PleaseWait from "../../components/base/loading/PleaseWait";
import ComponentError from "../../components/base/ComponentError";
import ButtonBase from "@material-ui/core/ButtonBase";
import {siteLang} from "../../repository";
import {Typography, useTheme} from "@material-ui/core";
import {UtilsStyle} from "../../utils/Utils";
import {grey} from "@material-ui/core/colors";
import DefaultTextField from "../../components/base/textField/DefaultTextField";
import SelectBox from "../../components/SelectBox";
import Checkbox from "@material-ui/core/Checkbox";
import {DeleteOutline, ExpandLessOutlined, ExpandMoreOutlined} from "@material-ui/icons";
import _ from "lodash";
import SuccessButton from "../../components/base/button/buttonVariant/SuccessButton";
import {gcLog} from "../../utils/ObjectUtils";
import Collapse from "@material-ui/core/Collapse";
import {useSelector} from "react-redux";


export default function  ({open, boxId: bId, multiSelect = false, onSelect, ...props}) {

    const theme = useTheme();
    const {boxes} = useSelector(state => state.user);
    const [search, setSearch] = useState("");
    const [boxId, setBoxId] = useState(bId ? bId : undefined)
    const [selected, setSelected] = useState([]);

    const d = ControllerProduct.FeaturesGroup.search({boxId: boxId, search: search})
    const {data, error} = useSWR(...d)


    useEffect(() => {
        setSearch("")
        handleBoxSelect(undefined)
        setSelected([]);
    }, [open])

    // useEffect(() => {
    //     if (bId) {
    //         setBoxId(bId);
    //     }
    // }, [bId])

    function handleBoxSelect(box = {}) {
        if (bId)
            return
        setBoxId(box.id)
    }


    return (
        <BaseDialog open={open}
                    onClose={() => onSelect()}>
            {
                !boxId ?
                    <SelectBox onBoxSelected={handleBoxSelect}/>
                    :
                    <Box display={'flex'} flexDirection={'column'}>
                        <Box display={'flex'} py={2}>
                            <BaseButton variant={"text"} onClick={() => onSelect()}>
                                بستن
                            </BaseButton>
                            {
                                !bId && (boxes.length > 1) &&
                                <Box px={2}>
                                    <BaseButton variant={"outlined"}
                                                pb={1}
                                                onClick={() => {
                                                    setBoxId(undefined)
                                                }}>
                                        تغییر رسته
                                    </BaseButton>
                                </Box>
                            }
                            {
                                multiSelect &&
                                <React.Fragment>
                                    <Box px={2} display={'flex'} flexDirection={'column'}>
                                        <Typography variant={"caption"} justifyContent={'center'}
                                                    style={{
                                                        textAlign: 'center'
                                                    }}>
                                            تعداد انتخاب شده ها:
                                        </Typography>
                                        <Typography pt={1} variant={"h6"} justifyContent={'center'}
                                                    style={{
                                                        textAlign: 'center'
                                                    }}>
                                            {selected.length}
                                        </Typography>
                                    </Box>
                                    <Box px={2}>
                                        <SuccessButton
                                            onClick={() => {
                                                onSelect(selected)
                                            }}>
                                            انتخاب
                                        </SuccessButton>
                                    </Box>
                                    <Box px={2}>
                                        <BaseButton
                                            variant={"outlined"}
                                            onClick={() => {
                                                onSelect()
                                            }}
                                            style={{
                                                borderColor: theme.palette.error.main
                                            }}>
                                            بسته
                                        </BaseButton>
                                    </Box>
                                </React.Fragment>
                            }
                        </Box>
                        <Box py={2} px={4}>
                            <DefaultTextField
                                name={"search"}
                                label={'جست و جو'}
                                variant={'outlined'}
                                defaultValue={''}
                                onChange={(text) => {
                                    setSearch(text)
                                }}
                            />
                        </Box>
                        {
                            !error ?
                                data ?
                                    <Box display={'flex'} flexWrap={'wrap'}>
                                        {data.data.data.map(it => (
                                            <Item
                                                key={it.id}
                                                item={it}
                                                maxWidth={1/2}
                                                checked={_.findIndex(selected, (item) => item === it.id) !== -1}
                                                multiSelect={multiSelect}
                                                onSelect={() => {
                                                    if (!multiSelect) {
                                                        onSelect(it)
                                                        return;
                                                    }
                                                    const newList = selected;
                                                    const index = _.findIndex(newList, (item) => item === it.id)
                                                    if (index === -1) {
                                                        setSelected([...newList, it.id])
                                                        return
                                                    }
                                                    newList.splice(index, 1)
                                                    setSelected([...newList])
                                                }}/>
                                        ))}
                                    </Box> :
                                    <PleaseWait/>
                                : <ComponentError tryAgainFun={() => mutate(d[0])}/>
                        }
                    </Box>
            }
        </BaseDialog>
    )
}


export function Item({item: it, checked, multiSelect, onSelect, onRemove, ...props}) {
    const theme = useTheme();
    const [open, setOpen] = useState(false);
    const el = (
        <Box display={'flex'} flexDirection={'column'}
             style={{
                 border: `1px solid ${grey[300]}`,
                 ...UtilsStyle.borderRadius(5)
             }}>
            <Box display={'flex'}
                 p={2}>
                <Box display={'flex'} flexDirection={'column'}
                     flex={1}>
                    <Typography variant={'h6'}>
                        {it.name[siteLang]}
                    </Typography>
                    <Typography pt={2} variant={'body2'}>
                        تعداد فیچرها: {it.features.length}
                    </Typography>
                </Box>
                <Box minWidth={'20%'}
                     display={'flex'}
                     flexDirection={'column'}
                     justifyContent={'center'}
                     px={1}>
                    {
                        (multiSelect) &&
                        <Box pb={0.5}>
                            <Checkbox
                                size={"small"}
                                checked={checked}
                                onChange={(e) => {
                                    onSelect(it)
                                    e.stopPropagation()
                                }}
                                inputProps={{'aria-label': 'primary checkbox'}}
                            />
                        </Box>
                    }
                    {
                        onRemove &&
                        <Box display={'flex'} justifyContent={'center'}>
                            <IconButton
                                size={"small"}
                                onClick={onRemove}>
                                <DeleteOutline fontSize={"small"} style={{color: theme.palette.error.main}}/>
                            </IconButton>
                        </Box>
                    }

                    {
                        !_.isEmpty(it.features) &&
                        <Box display={'flex'} justifyContent={'center'}>
                            <IconButton
                                size={"small"}
                                onClick={(e) => {
                                    setOpen(!open)
                                    e.stopPropagation();
                                }}>
                                {
                                    open ?
                                        <ExpandLessOutlined fontSize={"small"}/> :
                                        <ExpandMoreOutlined fontSize={"small"}/>

                                }
                            </IconButton>
                        </Box>
                    }
                </Box>
            </Box>
            {
                !_.isEmpty(it.features) &&
                <Collapse in={open}>
                    <Box display={'flex'} flexWrap={'wrap'} pt={1}>
                        {
                            it.features.map(it => (
                                <Box key={it.id} p={0.5}>
                                    <Typography variant={"body2"}
                                                style={{
                                                    padding: theme.spacing(0.5),
                                                    border: `1px solid ${grey[400]}`,
                                                    ...UtilsStyle.borderRadius(5)
                                                }}>
                                        {it?.name===undefined?it?.feature?.name[siteLang]:it?.name[siteLang]}
                                    </Typography>
                                </Box>
                            ))
                        }
                    </Box>
                </Collapse>
            }
        </Box>
    )

    return (
        <Box p={1}
             {...props}
             style={{
                 ...UtilsStyle.borderRadius(5),
                 ...props.style
             }}>
            {
                onSelect ?
                    <ButtonBase
                        onClick={(e) => {
                            onSelect(it)
                            e.stopPropagation()
                        }}
                        style={{
                            ...UtilsStyle.borderRadius(5)
                        }}>
                        {el}
                    </ButtonBase> :
                    el
            }
        </Box>
    )
}
