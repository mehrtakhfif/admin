import React, {useEffect, useRef, useState} from "react";
import Box from "@material-ui/core/Box";
import ControllerProduct from "../../controller/ControllerProduct";
import {createMultiLanguage} from "../../controller/converter";
import PleaseWait from "../../components/base/loading/PleaseWait";
import ComponentError from "../../components/base/ComponentError";
import SelectBox from "../../components/SelectBox";
import Backdrop from "@material-ui/core/Backdrop";
import CircularProgress from "@material-ui/core/CircularProgress";
import {cyan} from "@material-ui/core/colors";
import FormController from "../../components/base/formController/FormController";
import {Card} from "@material-ui/core";
import DefaultTextFieldMultiLanguage from "../../components/base/textField/DefaultTextFieldMultiLanguage";
import _ from "lodash";
import {UtilsRouter, UtilsStyle} from "../../utils/Utils";
import StickyBox from "react-sticky-box";
import SuccessButton from "../../components/base/button/buttonVariant/SuccessButton";
import rout from "../../router";
import BaseButton from "../../components/base/button/BaseButton";
import SelectFeature, {Item as FeatureItem} from "../features/SelectFeature";
import SaveDialog from "../../components/dialog/SaveDialog";
import {siteLang} from "../../repository";
import Typography from "../../components/base/Typography";
import BaseDialog from "../../components/base/BaseDialog";
import DialogAppBar from "../../components/dialog/DialogAppBar";
import Checkbox from "@material-ui/core/Checkbox";
import {gcLog, tryIt} from "../../utils/ObjectUtils";
import OrderItems from "../../components/base/OrderItems";
import DraggableList from "../../components/base/draggableList/DraggableList";
import grey from "@material-ui/core/colors/grey";


export default function FeaturesGroupSingle ({featureGroupId, boxId: bId, open, onClose, ...props}) {
    const fgId = _.isBoolean(open) ? featureGroupId || undefined : props.match.params.fgId;

    const ref = useRef()
    const [state, setState] = useState({
        loading: false,
        error: false
    });
    const [data, setData] = useState(fgId ? undefined : {
        name: createMultiLanguage(),
        features: []
    })
    const [showTitle, setShowTitle] = useState(false)

    const [boxId, setBoxId] = useState(bId)

    useEffect(() => {
        if (fgId) {
            getData()
            return
        }
    }, [fgId])

    function getData() {
        ControllerProduct.FeaturesGroup.getSingle({fgId: fgId}).then((res) => {
            const data = res.data;
            const list = []

            _.forEach(data.features, (f) => {
                list.push({
                    ...f,
                    id: f.id
                })
            })
            setData({
                name: data.name,
                features: list,
            })
            setBoxId(data.category_id===undefined?-1:data.category_id)
            const showTitle = tryIt(() => Boolean(res.data.settings.ui.show_title), false)
            setShowTitle(showTitle)
        })
    }

    function handleSave() {
        const params = ref.current.serialize();
        setState({
            ...state,
            loading: true
        });
        ControllerProduct.FeaturesGroup.save({fgId: fgId, boxId: boxId, name: params.name,showTitle:showTitle}).then((res) => {
            if (_.isBoolean(open)) {
                onClose(res.data.id)
                return
            }
            if (!fgId) {
                try {
                    UtilsRouter.goTo(props.history, {routUrl: rout.FeatureGroup.Single.editFeatureGroup(res.data.id)})
                    window.location.reload()
                } catch (e) {
                }
            }
        }).finally(() => {
            setState({
                ...state,
                loading: false
            })
        })
    }

    const el = (
        <Box my={3} display={'flex'}>
            {
                !state.error ?
                    data ?
                        boxId ?
                            <React.Fragment>
                                <Box width={1} display={'flex'}>
                                    <FormController innerref={ref} width={'80%'} display={'flex'}
                                                    flexDirection={'column'}
                                                    p={2}>
                                        <Box display={'flex'} component={Card} p={2} alignItems={'center'}
                                             justifyConetent={"center"}>
                                            <Box width={'70%'} pr={1}>
                                                <DefaultTextFieldMultiLanguage
                                                    label={"نام"}
                                                    required={true}
                                                    name={"name"}
                                                    defaultValue={data.name}
                                                    variant={"outlined"}/>
                                            </Box>
                                            <Box width={"30%"} alignItems={'center'} display={'flex'}
                                                 flexDirection={'column'}>
                                                <Typography pb={1} variant={"subtitle1"}>
                                                    نمایش نام در مشخصات
                                                </Typography>
                                                <Checkbox checked={showTitle}
                                                          onChange={() => setShowTitle(!showTitle)}/>
                                            </Box>
                                        </Box>
                                        <Features fgId={fgId} features={data.features}/>
                                    </FormController>
                                    <Box width={'20%'} display={'flex'} p={2} flexDirection={'column'}>
                                        <StickyBox offsetTop={180} offsetBottom={20}>
                                            <Box display={'flex'} component={Card} justifyContent={'center'} p={2}
                                                 width={1}
                                                 alignItems={'center'}
                                                 flexDirection={'column'}>
                                                <SuccessButton
                                                    onClick={handleSave}>
                                                    ذخیره
                                                </SuccessButton>
                                            </Box>
                                        </StickyBox>
                                    </Box>
                                </Box>
                                <Backdrop open={state.loading}
                                          style={{
                                              zIndex: '9999'
                                          }}>
                                    <CircularProgress color="inherit"
                                                      style={{
                                                          color: cyan[300],
                                                          width: 100,
                                                          height: 100
                                                      }}/>
                                </Backdrop>
                            </React.Fragment> :
                            <SelectBox onBoxSelected={(box) => {
                                try {
                                    setBoxId(box.id)
                                } catch (e) {
                                }
                            }}/> :
                        <PleaseWait/> :
                    <ComponentError tryAgainFun={getData}/>
            }
        </Box>
    )

    return (
        _.isBoolean(open) ?
            <BaseDialog open={open} maxWidth={"xl"} onClose={() => onClose()}>
                <DialogAppBar onClose={() => onClose()}/>
                <Box display={'flex'} flexDirection={'column'}>
                    {el}
                </Box>
            </BaseDialog>
            :
            el
    )
}


function Features({fgId, features: fe, ...props}) {
    const [open, setOpen] = useState(false);
    const [state, setState] = useState({
        loading: false,
    });
    const [sorting, setSorting] = useState(false)
    const [features, setFeatures] = useState(fe)
    const [remove, setRemove] = useState(undefined)


    useEffect(() => {
        setFeatures(fe)
    }, [fe])

    function handleFeatureAdd(items) {
        setOpen(false)
        if (items === undefined)
            return;
        const newList = [];
        _.forEach(features, (f) => {
            newList.push(f.id)
        })

        updateData([...newList, ...items])
    }

    function handleRemove(item) {
        const i = _.findIndex(features, (f => f.id === item.id))
        if (i === -1)
            return;
        const newFeatures = features;
        newFeatures.splice(i, 1)
        const newList = [];
        _.forEach(newFeatures, (f) => {
            newList.push(f.id)
        })

        updateData(newList);
    }

    function handleSortingChange() {
        setSorting(!sorting)
        if (sorting) {
            getData()
        }
    }

    function updateData(list) {
        setState({
            ...state,
            loading: true
        })
        ControllerProduct.FeaturesGroup.updateFeatures({id: fgId, features: list}).finally(() => {
            getData()
        })
    }

    function getData() {
        ControllerProduct.FeaturesGroup.getSingle({fgId}).then((res => {
            setFeatures(res.data.features)
        })).finally(() => {
            setState({
                ...state,
                loading: false
            })
        })
    }
    return (
        fgId ?
            <Box my={2}>
                <Box display={'flex'} flexDirection={'column'} component={Card} p={2}>
                    <Box display={'flex'} pb={1}>
                        <Box px={1}>
                            <BaseButton variant={"outlined"}
                                        disabled={sorting}
                                        onClick={() => {
                                            setOpen(true)
                                        }}>
                                انتخاب فیچر جدید
                            </BaseButton>
                        </Box>
                        <Box px={1}>
                            <BaseButton variant={"outlined"}
                                        disabled={sorting}
                                        onClick={handleSortingChange}
                                        style={{
                                            borderColor: cyan[300]
                                        }}>
                                {!sorting ? "مرتب سازی" : "تایید و برگشت به حالت قبل"}
                            </BaseButton>
                        </Box>
                    </Box>
                    {
                        !sorting ?
                            <Box display={'flex'} flexWrap={'wrap'}>
                                {
                                    features.map(it => (
                                        <FeatureItem
                                            key={it.id}
                                            item={it}
                                            onRemove={() => setRemove(it)}/>
                                    ))
                                }
                            </Box>
                            :
                            <Box>
                                <OrderItems
                                    data={features}
                                    model={"feature_group_feature"}
                                    onDataChange={(items) => {
                                        setFeatures(items);
                                    }}
                                    onClose={(saved)=>{
                                        handleSortingChange()
                                    }}
                                    renderItem={(item,{isDraggingItem})=>(
                                        <Box
                                            key={item.id}
                                            p={1.25}
                                            style={{
                                                backgroundColor: isDraggingItem ? cyan[800] : undefined
                                            }}
                                            {...props}>
                                            <Box
                                                component={Card}
                                                display={'flex'}
                                                alignItems={'center'}
                                                p={1}
                                                width={'50%'}
                                                style={{
                                                    backgroundColor: isDraggingItem ? cyan[800] : undefined
                                                }}>
                                                <Typography variant={"body1"}
                                                            color={isDraggingItem ? '#fff' : undefined}>
                                                    {item.name[siteLang]}
                                                </Typography>
                                                <Typography variant={"caption"} pr={3}
                                                            color={isDraggingItem ? '#fff' : undefined}>
                                                    نوع:
                                                    {item.type.typeLabel}
                                                </Typography>
                                            </Box>
                                        </Box>
                                    )}
                                />
                            </Box>
                    }
                </Box>
                <SelectFeature
                    open={open}
                    multiSelect={true}
                    onSelect={(items) => {
                        handleFeatureAdd(items)
                    }}/>
                <SaveDialog
                    open={Boolean(remove)}
                    text={"ایا از حذف اطمینان دارید؟"}
                    submitText={"بله"}
                    cancelText={"خیر"}
                    onCancel={() => {
                        setRemove(undefined)
                    }}
                    onSubmit={() => {
                        handleRemove(_.clone(remove))
                        setRemove(undefined)
                    }}/>
                <Backdrop open={state.loading}
                          style={{
                              zIndex: '9999'
                          }}>
                    <CircularProgress color="inherit"
                                      style={{
                                          color: cyan[300],
                                          width: 100,
                                          height: 100
                                      }}/>
                </Backdrop>
            </Box> :
            <React.Fragment/>
    )
}
