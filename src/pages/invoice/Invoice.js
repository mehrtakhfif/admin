import React, {Fragment, useEffect, useState} from "react";
import Box from "@material-ui/core/Box";
import Table, {createTableHeader, headerItemTemplate} from "../../components/base/table/Table";
import {createFilter} from "../../components/base/table/Filters";
import DataUtils, {filterType} from "../../utils/DataUtils";
import _ from "lodash";
import storage, {defaultBase} from "../../storage";
import ControllerProduct from "../../controller/ControllerProduct";
import {ApiHelper, deliver_status, invoice_status} from "../../controller/converter";
import useSWR, {mutate} from "swr";
import Tooltip from "@material-ui/core/Tooltip";
import {activeLang, lang, theme} from "../../repository";
import IconButton from "@material-ui/core/IconButton";
import Link from "../../components/base/link/Link";
import {FiberManualRecordTwoTone, ScreenShareOutlined, VisibilityOutlined} from "@material-ui/icons";
import rout from "../../router";
import ComponentError from "../../components/base/ComponentError";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import Typography from "../../components/base/Typography";
import {useTheme} from "@material-ui/core";
import LocalStorageUtils from "../../utils/LocalStorageUtils";
import {UtilsFormat, UtilsStyle} from "../../utils/Utils";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import {gcLog} from "../../utils/ObjectUtils";
import {useSnackbar} from "notistack";
import Button from "@material-ui/core/Button";
import {object} from "prop-types";
import ButtonBase from "@material-ui/core/ButtonBase";
import teal from "@material-ui/core/colors/teal";
import api from "../../controller/api";
import {BookingType} from "../products/ProductSingle";
import {bookingType} from "../../controller/type";
import {adminOtherKey} from "../../sv";


const headers = [
    createTableHeader({id: "id", label: 'شماره صورتحساب', dir: 'ltr', hasPadding: false}),
    headerItemTemplate.actions,
    createTableHeader({id: 'status', label: 'وضعیت'}),
    createTableHeader({id: 'amount', label: 'هزینه', dir: "ltr", hasPadding: false}),
    createTableHeader({id: 'deliver_status', label: 'وضعیت ارسال', dir: "ltr", hasPadding: false}),
    createTableHeader({id: 'product_count', label: 'تعداد محصولات', dir: "ltr", hasPadding: false}),
    createTableHeader({id: 'user', label: 'کاربر'}),
    createTableHeader({id: 'payed_at', label: 'تاریخ خرید', dir: "ltr"}),
    ...headerItemTemplate.createByUpdatedBy(),

];


const filters = [
    createFilter({id: 'name__fa', type: filterType.text, label: 'نام', value: ''}),
];
const CookieKey = "invoice";
const BookingTypeCookieKey = "invoice-booking";


export default function ({...props}) {
    //region var
    const theme = useTheme()
    const [bookingTypeVal, setBookingTypeVal] = useState(bookingType[LocalStorageUtils.get(BookingTypeCookieKey,bookingType.unbookable.key)])
    //region state
    const [state, setState] = useState({
        filters: _.cloneDeep(filters),
        order: storage.PageSetting.getOrder(CookieKey),
        activeStatus: storage.PageSetting.getPageSetting(CookieKey).status || "all",
        parent: [],
        pagination: {
            rowPerPage: storage.PageSetting.getRowPerPage(CookieKey),
            page: 0
        }
    });
    const [filtersState, setFiltersState] = useState({
        data: filters,
        resetFilters: () => {
            setFiltersState({
                ...filtersState,
                data: DataUtils().TableFilter().resetFilter({filters: filtersState.data}),
            })
        }
    });
    //endregion state
    //endregion var

    //region useEffect
    useEffect(() => {
        requestFilters();
    }, []);

    useEffect(() => {
        storage.PageSetting.setPage(CookieKey, {page: state.pagination.page});
    }, [state.pagination.page]);

    useEffect(() => {
        if (!state.activeBox)
            return;
        LocalStorageUtils.set(`cat-b${state.activeBox}`, state.parent);
    }, [state.parent]);
    //endregion useEffect


    //region requests

    const d = ControllerProduct.Invoices.get();
    const bookingTypeProps = bookingTypeVal ? (bookingTypeVal.key === bookingType.datetime.key || bookingTypeVal.key === bookingType.range.key) : undefined
    let apiKey = ApiHelper.createSWRKey({
        api: d[0],
        order: state.order,
        filters: state.filters,
        box: state.activeBox,
        page: state.pagination.page,
        step: state.pagination.rowPerPage,
    }) + `boo-${bookingTypeProps ? "t" : "f"}`

    const parent = [];
    _.forEach(state.parent, (p) => {
        parent.push(p.id)
    });
    apiKey = apiKey + _.join(parent, '~')
    apiKey = apiKey + "status-" + state.activeStatus;


    const {data, error} = useSWR(apiKey, () => {
        return d[1]({
            status: state.activeStatus !== "all" ? state.activeStatus : undefined,
            page: state.pagination.page + 1,
            step: state.pagination.rowPerPage,
            order: state.order,
            filters: state.filters,
            onlyBooking: bookingTypeProps,
            parent_id: !_.isEmpty(parent) ? parent[parent.length - 1] : undefined,
        })
    }, {
        refreshInterval: 30000
    });


    function requestFilters() {
        ControllerProduct.Invoices.Filters.get().then((res) => {
            const newList = _.isArray(filtersState.data) && !_.isEmpty(filtersState.data) ? filtersState.data : filters;
            _.forEach(res.data, (fi) => {
                const data = createFilter({id: fi.name, type: fi.type, label: fi.label, data: fi.filters, value: null});
                switch (fi.name) {
                    case 'category': {
                        data.value = [];
                        break;
                    }
                    default: {
                    }
                }

                const index = _.findIndex(newList, (l) => l.id === data.id);
                newList[index !== -1 ? index : newList.length] = data;
            });
            setFiltersState({
                ...filtersState,
                data: [...newList]
            })
        }).catch((e) => {
            setFiltersState({
                ...filtersState,
                data: false
            })
        });
    }

    //endregion requests

    //region handler
    function handleFilterChange({item, value}) {
        setState({
            ...state,
            pagination: {
                ...state.pagination,
                page: 0
            }
        })
        setFiltersState({
            ...filtersState,
            data: DataUtils().TableFilter().filterItemChange({filters: filtersState.data, item: item, value: value})
        });
    }

    function handleApplyFilter() {
        setState({
            ...state,
            filters: _.cloneDeep(filtersState.data)
        })
    }

    function handleResetFilter() {
        filtersState.resetFilters()
    }

    const {enqueueSnackbar, closeSnackbar} = useSnackbar();

    function handleStateChange(menuItem, invoiceItem) {
        ControllerProduct.Invoices.put({id: invoiceItem.id, status: menuItem.key}).then((res) => {
        }).catch((e) => {
            enqueueSnackbar(e.message,
                {
                    variant: "error",
                    action: (key) => (
                        <Fragment>
                            <Button onClick={() => {
                                closeSnackbar(key)
                            }}>
                                {lang.get('close')}
                            </Button>
                        </Fragment>
                    )
                });
        }).finally(() => {
            setEditItem(undefined)
            mutate(apiKey).then(r => {
            })
        });

    }

    function handleChangeOrder(order) {
        storage.PageSetting.setOrder(CookieKey, {order: order});
        setState({
            ...state,
            order: {
                ...order
            }
        });
    }

    function handleChangeBox(e, box) {
        if (!box || state.activeBox === box.id)
            return;
        storage.PageSetting.setActiveBox(CookieKey, {activeBox: box.id});
        filtersState.resetFilters();
        setState({
            ...state,
            activeBox: box.id,
            pagination: {
                ...state.pagination,
                page: 0
            }
        });
    }

    function handleShowChild(cat) {
        if (_.findIndex(state.parent, p => p.id === cat.id) !== -1)
            return;
        setState({
            ...state,
            parent: [...state.parent, cat]
        });
    }

    //endregion handler

    //region renderData
    const pg = state.pagination;
    const [editItem, setEditItem] = useState(undefined)
    const tableData = (data && data.data ? data.data : [...Array(state.pagination.rowPerPage)]).map((inv, i) => (
        inv ? {
            id: {
                label: (
                    <Typography display={'flex'} justifyContent={'center'}
                                variant={'body2'} fontWeight={400}
                                color={undefined}>
                        {`MT-${inv.id}`}
                    </Typography>)
            },
            actions: {
                label: <Box display={'flex'} alignItems={'center'}>
                    <Tooltip title={lang.get('show')}>
                        <IconButton size={"small"}
                                    style={{
                                        marginRight: theme.spacing(0.5),
                                        marginLeft: theme.spacing(0.5)
                                    }}>
                            <Link toHref={rout.Invoice.Single.showInvoice(inv.id)} linkStyle={{display: 'flex'}}
                                  hoverColor={theme.palette.primary.main}>
                                <VisibilityOutlined style={{fontSize: 20, padding: 2}}/>
                            </Link>
                        </IconButton>
                    </Tooltip>
                    <Tooltip title={"تغییر وضعیت"}>
                        <IconButton size={"small"}
                                    disabled={inv.status.key === invoice_status.pending.key}
                                    no_hover={"true"}
                                    onClick={(e) => {
                                        setEditItem({e: e.currentTarget, item: inv})
                                        e.stopPropagation();
                                    }}
                                    style={{
                                        marginRight: theme.spacing(0.5),
                                        marginLeft: theme.spacing(0.5),
                                        opacity: inv.status.key === invoice_status.pending.key ? 0.4 : 1
                                    }}>
                            <Typography variant={'body1'}
                                        hoverColor={theme.palette.primary.main}
                                        style={{display: 'flex'}}>
                                <ScreenShareOutlined style={{fontSize: 20, padding: 2}}/>
                            </Typography>
                        </IconButton>
                    </Tooltip>

                </Box>
            },
            status: {
                label: () => {
                    return (
                        <Box display={'flex'} flexWrap={'wrap'} justifyContent={'center'} alignItems={'center'}
                             maxWidth={300}>
                            <Tooltip title={inv.status.label}>
                                <FiberManualRecordTwoTone style={{color: inv.status.color}}/>
                            </Tooltip>
                        </Box>
                    )
                }
            },
            amount: {
                label: UtilsFormat.numberToMoney(inv.amount),
            },
            deliver_status: {
                label: <Typography display={'flex'} justifyContent={'center'}
                                   variant={'body2'} fontWeight={400}
                                   color={undefined}>
                    {inv.deliver_status}
                </Typography>,

            },
            product_count: {
                label: <Typography display={'flex'} justifyContent={'center'}
                                   variant={'h6'} fontWeight={400}
                                   color={undefined}>
                    {inv.products_count}
                </Typography>,
            },
            user: {
                label: (
                    <Tooltip title={inv.user.username}>
                        <Link toHref={rout.User.Profile.showUser(inv.user.id)} linkStyle={{display: 'flex'}}
                              hoverColor={theme.palette.primary.main}>
                            {inv.user.name}
                        </Link>
                    </Tooltip>
                ),
            },
            payed_at: {
                label: inv.payed_at ? inv.payed_at : "----",
            },
            created_at: {
                label: inv.createdAt.date,
            },
            created_by: {
                label: inv.createdAt.by.name,
                link: inv.createdAt.by.link,
            },
            updated_at: {
                label: inv.updatedAt.date,
            },
            updated_by: {
                label: inv.updatedAt.by.name,
                link: inv.updatedAt.by.link,
            },
        } : {}
    ));


    //endregion renderData

    return (
        <Box my={2} px={2}>
            <Box width={1} display={'flex'} flexWrap={'wrap'}>
                {["all", ...Object.keys(invoice_status)].map(it => {
                    const isAll = it === "all"

                    const item = !isAll ? invoice_status[it] : {
                        key: "all",
                        label: "نمایش همه",
                        color: teal[500],
                        overflow: 'hidden'
                    };
                    return (
                        <Box p={1} key={it} minWidth={132} display={'flex'} justifyContent={'center'}
                             flexDirection={'initial'}>
                            <ButtonBase
                                onClick={() => {
                                    storage.PageSetting.setPageSetting(CookieKey, {status: item.key})
                                    setState({
                                        ...state,
                                        activeStatus: item.key
                                    })
                                }}
                                style={{
                                    border: `${state.activeStatus === item.key ? 2 : 1}px solid ${item.color}`,
                                    ...UtilsStyle.borderRadius(5)
                                }}>
                                <Typography p={1} variant={'body2'} alignItems={'center'}>
                                    <FiberManualRecordTwoTone
                                        style={{
                                            paddingLeft: theme.spacing(1),
                                            color: item.color,
                                            opacity: state.activeStatus === item.key ? 1 : 0.3
                                        }}/>
                                    {item.label}
                                </Typography>
                            </ButtonBase>
                        </Box>
                    )
                })}
                <BookingType dontCheckType={true}
                             bookingType={bookingTypeVal}
                             onChange={t => {
                                 if (!t)
                                     return
                                 setBookingTypeVal(t)
                                 LocalStorageUtils.set(BookingTypeCookieKey, t.key)
                             }}/>
            </Box>
            {(!error) ?
                <React.Fragment>
                    <Table headers={headers}
                           cookieKey={CookieKey}
                           title={(
                               <React.Fragment>
                                   <Typography variant={'h6'}>صورت حساب</Typography>
                                   {!_.isEmpty(state.parent) &&
                                   <Breadcrumbs aria-label="breadcrumb"
                                                style={{
                                                    paddingRight: theme.spacing(2),
                                                    display: 'flex',
                                                    alignItems: 'center'
                                                }}>
                                       <Link onClick={() => {
                                           setState({
                                               ...state,
                                               parent: []
                                           })
                                       }}>
                                           همه
                                       </Link>
                                       {state.parent.map((p, index) => (
                                           state.parent.length !== index + 1 ?
                                               <Link key={p.id}
                                                     onClick={() => {
                                                         const newParent = [];
                                                         _.forEach(state.parent, (pr) => {
                                                             newParent.push(pr);
                                                             if (p.id === pr.id)
                                                                 return false
                                                         });
                                                         setState({
                                                             ...state,
                                                             parent: [...newParent]
                                                         })
                                                     }}>
                                                   {p.name[activeLang]}
                                               </Link> :
                                               <Typography key={p.id}
                                                           variant={'body2'}>{p.name[activeLang]}</Typography>
                                       ))}
                                   </Breadcrumbs>}
                               </React.Fragment>
                           )}
                           data={tableData}
                           filters={filtersState.data}
                           onFilterChange={handleFilterChange}
                           loading={false}
                           activePage={pg.page}
                           rowsPerPage={pg.rowPerPage}
                           lastPage={data ? data.pagination.lastPage : 0}
                           rowCount={data ? data.pagination.count : 0}
                           orderType={state.order}
                           showBox={false}
                           onActivePageChange={(page) => {
                               setState({
                                   ...state,
                                   pagination: {
                                       ...state.pagination,
                                       page: page
                                   }
                               })
                               // requestData({page: e})
                           }}
                           onRowPerPageChange={(rowPerPage) => {
                               storage.PageSetting.setRowPerPage(CookieKey, {rowPerPage: rowPerPage});
                               setState({
                                   ...state,
                                   pagination: {
                                       ...state.pagination,
                                       rowPerPage: rowPerPage
                                   }
                               })
                           }}
                           onApplyFilterClick={handleApplyFilter}
                           onResetFilterClick={handleResetFilter}
                           onChangeOrder={handleChangeOrder}
                           onChangeBox={handleChangeBox}/>
                </React.Fragment> :
                <Box>
                    <ComponentError statusCode={(_.isObject(error) && error.request) ? error.request.status : undefined}
                                    tryAgainFun={() => mutate(apiKey)}/>
                </Box>}
            <Menu
                id="simple-menu"
                anchorEl={editItem ? editItem.e : undefined}
                keepMounted
                open={Boolean(editItem)}
                onClose={() => {
                    setEditItem(undefined)
                }}>
                {Object.keys(invoice_status).map(k => {
                    const item = invoice_status[k];
                    return (
                        item.selectable ?
                            <MenuItem
                                key={item.key}
                                onClick={() => {
                                    handleStateChange(item, editItem.item)
                                }}>
                                {item.label}
                            </MenuItem> :
                            <React.Fragment/>)
                })}
            </Menu>

        </Box>
    )
}
