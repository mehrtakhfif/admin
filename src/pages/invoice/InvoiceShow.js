import React, {useEffect, useState} from "react";
import Box from "@material-ui/core/Box";
import {useSnackbar} from "notistack";
import useTheme from "@material-ui/core/styles/useTheme";
import ControllerProduct from "../../controller/ControllerProduct";
import PleaseWait from "../../components/base/loading/PleaseWait";
import ComponentError from "../../components/base/ComponentError";
import useSWR, {mutate} from "swr";
import Typography from "../../components/base/Typography";
import {green, grey} from "@material-ui/core/colors";
import Link from "../../components/base/link/Link";
import rout from "../../router";
import {UtilsFormat, UtilsStyle} from "../../utils/Utils";
import {
    AccountBalanceOutlined,
    AccountCircleOutlined,
    AttachMoneyOutlined,
    CreditCardOutlined,
    Dvr,
    EmailOutlined,
    FiberManualRecordTwoTone,
    KeyboardArrowDown,
    KeyboardArrowUp,
    LocalAtmOutlined, LocationCity,
    MailOutline,
    PeopleOutline,
    PersonOutline,
    PhoneEnabledOutlined, Place,
    ReceiptOutlined,
    RecentActorsOutlined,
    ShoppingCartOutlined, StorageOutlined,
    SystemUpdateAltOutlined
} from "@material-ui/icons";
import Img from "../../components/base/img/Img";
import Tooltip from "../../components/base/Tooltip";
import {ButtonBase, Card, Paper} from "@material-ui/core";
import _ from "lodash";
import {makeStyles} from "@material-ui/core/styles";
import IconButton from "@material-ui/core/IconButton";
import Icon from "../../components/icon/Icon";
import {convertIconUrl, icons} from "../../controller/converter";
import {lang, siteLang} from "../../repository";
import Toman from "../../components/icon/Toman";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import Collapse from "@material-ui/core/Collapse";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableBody from "@material-ui/core/TableBody";
import TableContainer from "@material-ui/core/TableContainer";
import {gcLog, getSafe, tryIt} from "../../utils/ObjectUtils";
import moment from "moment";
import JDate from "../../utils/JDate";
import Popover from "@material-ui/core/Popover";
import {useSelector} from "react-redux";
import orange from "@material-ui/core/colors/orange";

const singleInvoiceStyles = makeStyles(theme => ({
    itemODDItem: {
        [theme.breakpoints.up('sm')]: {
            borderRightWidth: '1px !important',
        },
    },
}));

export default function ({...props}) {
    const theme = useTheme();
    const classes = singleInvoiceStyles(props)
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();
    const invId = props.match.params.invoiceId;
    const apiKey = `invoiceShow-${invId}`;
    const {data: da, error} = useSWR(apiKey, () => {
        return ControllerProduct.Invoices.getSingle({invoiceId: invId})
    });
    let {data} = da ? da : {};

    return (
        <Box p={2}>
            {!error ?
                data ?
                    <Box display={'flex'} flexDirection={'column'}>
                        <Box display={'flex'} flexWrap={'wrap'} alignItems={'center'}>
                            <Box display={'flex'} flexDirection={'column'}
                                 px={2}
                                 alignItems={'center'}
                                 justifyContent={'center'}>
                                <FiberManualRecordTwoTone
                                    style={{
                                        color: data.status.color
                                    }}/>
                                <Typography variant={"body2"} pt={0.8} style={{...UtilsStyle.disableTextSelection()}}>
                                    {data.status.label}
                                </Typography>
                            </Box>
                            <Box display={'flex'} flexDirection={'column'}
                                 style={{...UtilsStyle.widthFitContent()}}>
                                <Box
                                    pb={1}
                                    px={2}
                                    display={'flex'}
                                    minHeight={50}
                                    alignItems={'flex-end'}
                                    style={{
                                        borderBottom: `2px solid ${grey[300]}`
                                    }}>
                                    <Tooltip title={"شماره صورتحساب"}>
                                        <Box display={'flex'}>
                                            <Typography
                                                variant={"h6"}>
                                                {`MT-${data.id}`}
                                            </Typography>
                                        </Box>
                                    </Tooltip>
                                </Box>
                                <Box minHeight={50}>
                                    <Tooltip title={"خریدار"}>
                                        <Box display={'flex'}>
                                            <Link toHref={rout.User.Profile.showUser(data.user.id)}>
                                                <Typography variant={'body2'} px={2} pt={1}>
                                                    {data.user.name}
                                                </Typography>
                                            </Link>
                                        </Box>
                                    </Tooltip>
                                </Box>
                            </Box>
                            {data.ipg &&
                            <Box
                                display={'flex'}
                                flexDirection={'column'}
                                alignItems={'center'}
                                style={{...UtilsStyle.widthFitContent()}}>
                                <Box
                                    width={1}
                                    pb={1}
                                    px={2}
                                    display={'flex'}
                                    justifyContent={'center'}
                                    alignItems={'flex-end'}
                                    style={{
                                        minHeight: 50,
                                        borderBottom: `2px solid ${grey[300]}`
                                    }}>
                                    <Tooltip title={`بانک ${data.ipg.name}`}>
                                        <Box component={"span"}>
                                            <Img src={data.ipg.image}
                                                 width={30} minWidth={1}
                                                 minHeight={1}/>
                                        </Box>
                                    </Tooltip>
                                </Box>
                                <Box
                                    pt={1}
                                    px={2}
                                    minHeight={50}>
                                    <Tooltip title={"تاریخ پرداخت"}>
                                        <Box display={'flex'} component={'span'}>
                                            <Typography
                                                variant={"body1"}
                                                style={{
                                                    direction: 'ltr'
                                                }}>
                                                {data.payed_at || "-----"}
                                            </Typography>
                                        </Box>
                                    </Tooltip>
                                </Box>
                            </Box>
                            }
                            <Box display={'flex'} flexDirection={'column'}
                                 style={{...UtilsStyle.widthFitContent()}}>
                                <Box display={'flex'} flexDirection={'column'}
                                     style={{...UtilsStyle.widthFitContent()}}>
                                    <Box
                                        display={'flex'}
                                        pb={1}
                                        px={2}
                                        style={{
                                            alignItems: 'flex-end',
                                            minHeight: 50,
                                            borderBottom: `2px solid ${grey[300]}`
                                        }}>
                                        <Tooltip title={"تعداد محصولات"}>
                                            <Box display={'flex'}>
                                                <Typography
                                                    component={"span"}
                                                    variant={"h5"}
                                                    alignItems={'center'}
                                                    fontWeight={500}>
                                                    <ShoppingCartOutlined
                                                        fontSize={"small"}
                                                        style={{
                                                            marginLeft: theme.spacing(1)
                                                        }}/>
                                                    {data.products_count}
                                                </Typography>
                                            </Box>
                                        </Tooltip>
                                    </Box>
                                </Box>
                                <Box display={'flex'} minHeight={50}
                                     px={2} pt={1}>
                                    <Tooltip title={"مبلغ پرداختی (تومان)"}>
                                        <Box display={'flex'} height={"fit-content"}>
                                            <Typography variant={'body1'}
                                                        component={"span"}>
                                                {UtilsFormat.numberToMoney(data.amount + (data.post_invoice ? data.post_invoice.amount : 0))}
                                                <Toman/>
                                            </Typography>
                                        </Box>
                                    </Tooltip>
                                </Box>
                            </Box>
                            <Tooltip title={data.invoice ? "دانلود فاکتور" : "فاکتور موجود نمیباشد"}>
                                <IconButton
                                    disabled={!data.invoice}
                                    onClick={() => {

                                    }}
                                    style={{
                                        marginRight: theme.spacing(1),
                                        padding: '0px'
                                    }}>
                                    <Link href={data.invoice} download target={'_blank'}
                                          p={1.5}>
                                        <SystemUpdateAltOutlined/>
                                    </Link>
                                </IconButton>
                            </Tooltip>
                            {
                                data.recipient_info_a5 &&
                                <Tooltip title={"مشخصات گیرنده A5"}>
                                    <IconButton
                                        onClick={() => {

                                        }}
                                        style={{
                                            marginRight: theme.spacing(1),
                                            padding: '0px'
                                        }}>
                                        <Link href={data.recipient_info_a5} download target={'_blank'}
                                              p={1.5}>
                                            <Typography className={'fa'} fontWeight={800} variant={"h6"}
                                                        alignItems={'center'} justifyContent={'center'}
                                                        style={{width: 30, height: 30}}>
                                                A5
                                            </Typography>
                                        </Link>
                                    </IconButton>
                                </Tooltip>
                            }
                            {
                                data.recipient_info_a6 &&
                                <Tooltip title={"مشخصات گیرنده A6"}>
                                    <IconButton
                                        onClick={() => {

                                        }}
                                        style={{
                                            marginRight: theme.spacing(1),
                                            padding: '0px'
                                        }}>
                                        <Link href={data.recipient_info_a6} download target={'_blank'}
                                              p={1.5}>
                                            <Typography className={'fa'} fontWeight={800} variant={"h6"}
                                                        alignItems={'center'} justifyContent={'center'}
                                                        style={{width: 30, height: 30}}>
                                                A6
                                            </Typography>
                                        </Link>
                                    </IconButton>
                                </Tooltip>
                            }

                            {(data.start_date || data.max_shipping_time) &&
                            <MaxShippingTime time={data.start_date || data.max_shipping_time}/>
                            }
                        </Box>
                        <Box display={'flex'} width={1} flexWrap={"wrap"}>
                            <Box py={1} pr={2} width={0.5}>
                                <Box width={1} component={Card} display={'flex'}
                                     flexWrap={'wrap'}>
                                    <Typography px={3} py={2} variant={'h6'}
                                                color={grey[400]}
                                                fontWeight={500} alignItems={"center"}
                                                style={{width: '100%'}}>
                                        <LocalAtmOutlined
                                            style={{
                                                marginLeft: theme.spacing(1)
                                            }}/>
                                        مشخصات پرداخت
                                    </Typography>
                                    <Item
                                        label={(
                                            <React.Fragment>
                                                <AttachMoneyOutlined
                                                    fontSize={"small"}
                                                    style={{
                                                        marginLeft: theme.spacing(1)
                                                    }}/>
                                                قیمت بدون تخفیف
                                            </React.Fragment>
                                        )}
                                        value={(
                                            <React.Fragment>
                                                {UtilsFormat.numberToMoney(data.final_price)}
                                                <Toman/>
                                            </React.Fragment>
                                        )}/>
                                    <Item
                                        label={(
                                            <React.Fragment>
                                                <CreditCardOutlined
                                                    fontSize={"small"}
                                                    style={{
                                                        marginLeft: theme.spacing(1)
                                                    }}/>
                                                قیمت پرداختی کاربر
                                            </React.Fragment>
                                        )}
                                        value={(
                                            <React.Fragment>
                                                {UtilsFormat.numberToMoney(data.amount)}
                                                <Toman color={"svgSuccessDark"}/>
                                            </React.Fragment>
                                        )}
                                        valueProps={{
                                            color: green[500],
                                            fontWeight: 500,
                                        }}/>
                                    <Item
                                        label={(
                                            <React.Fragment>
                                                <AccountBalanceOutlined
                                                    fontSize={"small"}
                                                    style={{
                                                        marginLeft: theme.spacing(1)
                                                    }}/>
                                                مالیات بر ارزش افزوده
                                            </React.Fragment>
                                        )}
                                        value={(
                                            <React.Fragment>
                                                {UtilsFormat.numberToMoney(data.tax)}
                                                <Toman/>
                                            </React.Fragment>
                                        )}/>

                                    <Item
                                        labelProps={{
                                            alignItems: 'center'
                                        }}
                                        label={(
                                            <React.Fragment>
                                                <PeopleOutline
                                                    fontSize={"small"}
                                                    style={{
                                                        marginLeft: theme.spacing(1)
                                                    }}/>
                                                سهم تامیین کننده
                                            </React.Fragment>)
                                        }
                                        value={(
                                            <React.Fragment>
                                                {UtilsFormat.numberToMoney(data.start_price)}
                                                <Toman/>
                                            </React.Fragment>
                                        )}/>
                                    <Item
                                        labelProps={{
                                            alignItems: 'center'
                                        }}
                                        label={(
                                            <React.Fragment>
                                                <PeopleOutline
                                                    fontSize={"small"}
                                                    style={{
                                                        marginLeft: theme.spacing(1)
                                                    }}/>
                                                سهم خیریه

                                            </React.Fragment>)
                                        }
                                        value={(
                                            <React.Fragment>
                                                {UtilsFormat.numberToMoney(data.charity)}
                                                <Toman/>
                                            </React.Fragment>
                                        )}/>
                                    <Item
                                        labelProps={{
                                            alignItems: 'center'
                                        }}
                                        label={(
                                            <React.Fragment>
                                                <Icon
                                                    width={20}
                                                    src={convertIconUrl(icons.mehrTakhfif.media)}
                                                    color={false}
                                                    style={{
                                                        marginLeft: theme.spacing(1)
                                                    }}/>
                                                سهم {icons.mehrTakhfif.name[siteLang]}
                                            </React.Fragment>)
                                        }
                                        value={(
                                            <React.Fragment>
                                                {UtilsFormat.numberToMoney(data.mt_profit)}
                                                <Toman/>
                                            </React.Fragment>
                                        )}/>
                                    <Item
                                        label={(
                                            <React.Fragment>
                                                <EmailOutlined
                                                    fontSize={"small"}
                                                    style={{
                                                        marginLeft: theme.spacing(1)
                                                    }}/>
                                                هزینه ارسال
                                            </React.Fragment>)
                                        }
                                        value={(
                                            <React.Fragment>
                                                {(data.post_invoice && data.post_invoice.amount) ? UtilsFormat.numberToMoney(data.post_invoice.amount) : 0}
                                                <Toman/>
                                            </React.Fragment>
                                        )}/>
                                    <Item
                                        label={(
                                            <React.Fragment>
                                                <ReceiptOutlined
                                                    fontSize={"small"}
                                                    style={{
                                                        marginLeft: theme.spacing(1)
                                                    }}/>
                                                شماره پیگیری پرداخت
                                            </React.Fragment>)
                                        }
                                        value={data.sale_reference_id}/>
                                    <Item
                                        label={(
                                            <React.Fragment>
                                                <ReceiptOutlined
                                                    fontSize={"small"}
                                                    style={{
                                                        marginLeft: theme.spacing(1)
                                                    }}/>
                                                کارت بانکی خریدار
                                            </React.Fragment>)
                                        }
                                        valueProps={{dir: data.card_holder ? 'ltr' : 'rtl'}}
                                        value={data.card_holder}/>
                                </Box>
                            </Box>
                            <Box py={1} pr={2} width={0.5}>
                                <Box width={1} component={Card} display={'flex'}
                                     flexWrap={'wrap'}>
                                    <Box width={1}>
                                        <Link px={1} href={rout.User.Profile.showUser(data.user.id)}>
                                            <Typography px={3} py={2} variant={'h6'}
                                                        color={grey[400]}
                                                        fontWeight={500} alignItems={"center"}
                                                        style={{width: '100%'}}>
                                                <AccountCircleOutlined
                                                    style={{
                                                        marginLeft: theme.spacing(1)
                                                    }}/>
                                                مشخصات کاربر
                                            </Typography>
                                        </Link>
                                    </Box>
                                    <Item
                                        label={(
                                            <React.Fragment>
                                                <PersonOutline
                                                    fontSize={"small"}
                                                    style={{
                                                        marginLeft: theme.spacing(1)
                                                    }}/>
                                                نام و نام‌خانوادگی
                                            </React.Fragment>
                                        )}
                                        value={(
                                            <Box display={'flex'}>
                                                <Typography variant={"body1"} pl={0.5} fontWeight={400}>
                                                    {data.user.first_name}
                                                </Typography>
                                                <Typography variant={"body1"} fontWeight={500}>
                                                    {data.user.last_name}
                                                </Typography>
                                            </Box>
                                        )}
                                        valueProps={{
                                            color: green[500],
                                            fontWeight: 500,
                                        }}/>
                                    <Item
                                        label={(
                                            <React.Fragment>
                                                <PhoneEnabledOutlined
                                                    fontSize={"small"}
                                                    style={{
                                                        marginLeft: theme.spacing(1)
                                                    }}/>
                                                شماره موبایل
                                            </React.Fragment>
                                        )}
                                        value={(
                                            <Link px={1} dir={'ltr'} href={`tel:${data.user.username}`}>
                                                <Typography variant={"h6"}>
                                                    {UtilsFormat.toMobileTemplate(data.user.username)}
                                                </Typography>
                                            </Link>
                                        )}/>
                                    <Item
                                        label={(
                                            <React.Fragment>
                                                <RecentActorsOutlined
                                                    fontSize={"small"}
                                                    style={{
                                                        marginLeft: theme.spacing(1)
                                                    }}/>
                                                کد‌ملی
                                            </React.Fragment>
                                        )}
                                        valueProps={{
                                            dir: 'ltr',
                                            justifyContent: 'flex-end'
                                        }}
                                        value={UtilsFormat.toMeliCodeTemplate(data.user.meli_code)}/>
                                    <Item
                                        label={(
                                            <React.Fragment>
                                                <MailOutline
                                                    fontSize={"small"}
                                                    style={{
                                                        marginLeft: theme.spacing(1)
                                                    }}/>
                                                ایمیل
                                            </React.Fragment>
                                        )}
                                        value={(
                                            <Link px={1} dir={'ltr'}
                                                  href={`mailto: ${data.user.email}?subject = مهرتخفیف - MehrTakhfif`}>
                                                {data.user.email}
                                            </Link>
                                        )}/>
                                    <Item
                                        label={(
                                            <React.Fragment>
                                                <LocationCity
                                                    fontSize={"small"}
                                                    style={{
                                                        marginLeft: theme.spacing(1)
                                                    }}/>
                                                شهر:
                                            </React.Fragment>
                                        )}
                                        value={(
                                            <Link px={1} dir={'ltr'}>
                                                {getSafe(() => data.address.city.name, "------")}
                                            </Link>
                                        )}/>
                                    <Item
                                        label={(
                                            <React.Fragment>
                                                <Place
                                                    fontSize={"small"}
                                                    style={{
                                                        marginLeft: theme.spacing(1)
                                                    }}/>
                                                آدرس:
                                            </React.Fragment>
                                        )}
                                        value={(
                                            <Link px={1} dir={'ltr'}>
                                                {getSafe(() => data.address.address, "------")}
                                            </Link>
                                        )}/>
                                    {
                                        (data.details && data.details.cart_postal) &&
                                        <Item
                                            label={(
                                                <React.Fragment>
                                                    <Place
                                                        fontSize={"small"}
                                                        style={{
                                                            marginLeft: theme.spacing(1)
                                                        }}/>
                                                    کارت پستال
                                                </React.Fragment>
                                            )}
                                            value={(
                                                <Box display={'flex'} flexDirection={'column'}>
                                                    <Box display={'flex'}>
                                                        فرستنده:
                                                        {getSafe(() => data.details.cart_postal.sender, "------")}
                                                    </Box>
                                                    <Box pt={1} display={'flex'}>
                                                        متن:
                                                        {getSafe(() => data.details.cart_postal.text, "------")}
                                                    </Box>
                                                </Box>
                                            )}/>
                                    }
                                </Box>
                            </Box>
                            <Box py={1} width={1}>
                                <Box width={1} component={Card} display={'flex'}
                                     flexWrap={'wrap'}>
                                    <Box width={1} flexWrap={'wrap'}>
                                        <Typography px={3} py={2} variant={'h6'}
                                                    color={grey[400]}
                                                    fontWeight={500} alignItems={"center"}
                                                    style={{width: '100%'}}>
                                            <Dvr
                                                style={{
                                                    marginLeft: theme.spacing(1)
                                                }}/>
                                            محصولات
                                        </Typography>
                                        <ProductsTable data={data}/>
                                    </Box>
                                </Box>
                            </Box>
                        </Box>
                    </Box> :
                    <PleaseWait/> :
                <ComponentError
                    statusCode={(_.isObject(error) && error.request) ? error.request.status : undefined}
                    tryAgainFun={() => {
                        mutate(apiKey)
                    }}/>}
        </Box>
    )
}

function Item({label, value, labelProps, valueProps, ...props}) {
    const theme = useTheme();
    return (
        <Box px={3} pt={1.5} pb={1}
             minWidth={1 / 3}
             display={'flex'}
             flexDirection={'column'}
             justifyContent={'center'}
             style={{
                 ...props.style
             }}
             {...props}>
            <Typography variant={'body1'}
                        color={theme.palette.text.secondary}
                        {...labelProps}>
                {label}
            </Typography>
            <Typography pt={1} variant={'h6'}
                        color={theme.palette.text.primary}
                        {...valueProps}>
                {!_.isEmpty(value) ? value : '———————'}
            </Typography>
        </Box>
    )
}


const useRowStyles = makeStyles({
    root: {
        '& > *': {
            // borderBottom: 'unset',
        },
    },
});


function createMoreData({id, name, supplier, start_price, final_price, discount_price, mt_profit, ...p}) {
    return {
        id,
        name,
        supplier,
        start_price,
        final_price,
        discount_price,
        mt_profit,
        ...p
    }
}

function createData({img, name, start_price, final_price, discount_price, count, box, more, ...p}) {
    return {
        img,
        name,
        start_price,
        final_price,
        discount_price,
        count,
        box,
        more,
        ...p
    };
};

const createRows = (data) => {
    const list = [];

    _.forEach(data, ({product, storage, ...item}) => {
        list.push(createData({
            key: storage.title,
            img: product.thumbnail.image,
            name: storage.title,
            start_price: item.start_price,
            final_price: item.total_price,
            discount_price: item.discount_price,
            count: item.count,
            admin: item.admin,
            box: item.amer,
            tax: item.tax,
            dev: item.dev,
            mt_profit: item.mt_profit,
            charity: item.charity,
            more: createMoreData({
                id: product.id,
                name: storage.title,
                supplier: storage.supplier,
                start_price: _.toInteger(storage.start_price / item.count),
                final_price: _.toInteger(storage.final_price / item.count),
                discount_price: _.toInteger(item.discount_price / item.count),
                mt_profit: _.toInteger(item.mt_profit / item.count),
                admin: _.toInteger(item.admin / item.count),
                tax: _.toInteger(item.tax / item.count),
                dev: _.toInteger(item.dev / item.count),
                charity: _.toInteger(item.charity / item.count),
            })
        }))
    })
    return list;
};

function ProductsTable({data}) {
    const {invoice_products} = data;
    const rows = createRows(invoice_products);

    const {boxes, roll, ...user} = useSelector(state => state.user);
    const isAccountants = (roll === "superuser" || roll === "accountants");

    return (
        <TableContainer component={Paper}>
            <Table aria-label="collapsible table">
                <TableHead>
                    <TableRow>
                        <TableCell/>
                        <TableCell>نام</TableCell>
                        <TableCell>{lang.get("mt_box")}</TableCell>
                        <TableCell>تعداد</TableCell>
                        <TableCell>قیمت فروش بدون تخفیف</TableCell>
                        <TableCell>قیمت فروش</TableCell>
                        <TableCell>قیمت خرید</TableCell>
                        {
                            isAccountants &&
                            <React.Fragment>
                                <TableCell>ادمین</TableCell>
                                <TableCell>مهرتخفیف</TableCell>
                                <TableCell>برنامه‌نویس</TableCell>
                                <TableCell>ارزش‌افزوده</TableCell>
                                <TableCell>خیریه</TableCell>
                            </React.Fragment>
                        }
                    </TableRow>
                </TableHead>
                <TableBody>
                    {rows.map((row, i) => (
                        <ProductsTableRow key={i} isAccountants={isAccountants} row={row} data={data}/>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    )
}

function ProductsTableRow({isAccountants, row, data, ...props}) {
    const [open, setOpen] = React.useState(false);
    const classes = useRowStyles();
    const {more} = row;

    return (
        <React.Fragment>
            <TableRow className={classes.root}>
                <TableCell>
                    <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
                        {open ? <KeyboardArrowUp/> : <KeyboardArrowDown/>}
                    </IconButton>
                </TableCell>
                <TableCell component="th" scope="row">
                    <Box display={'flex'} alignItems={'center'}>
                        <Img width={80} minHeight={1} src={row.img} zoomable={true}/>
                        <Link
                            href={rout.Product.Single.editProduct(more.id)}
                            onClick={(e) => {
                                e.stopPropagation();
                            }}>
                            <Typography pl={1} variant={"body1"} fontWeight={400}>
                                {row.name[siteLang]}
                            </Typography>
                        </Link>
                    </Box>
                </TableCell>
                <TableCell>
                    <Typography variant={"body1"} fontWeight={400}>
                        {row.box}
                    </Typography>
                </TableCell>
                <TableCell>
                    <Typography variant={"h6"} fontWeight={400}>
                        {row.count}
                    </Typography>
                </TableCell>
                <TableCell>
                    <Typography variant={"h6"} fontWeight={400}>
                        {UtilsFormat.numberToMoney(row.final_price)}
                        <Toman/>
                    </Typography>
                </TableCell>
                <TableCell>
                    <Typography variant={"h6"} fontWeight={400} color={green[600]}>
                        {UtilsFormat.numberToMoney(row.discount_price)}
                        <Toman color={"svgSuccessDark"}/>
                    </Typography>
                </TableCell>
                <TableCell>
                    <Typography variant={"h6"} fontWeight={400}>
                        {UtilsFormat.numberToMoney(row.start_price)}
                        <Toman/>
                    </Typography>
                </TableCell>
                {
                    isAccountants &&
                    <React.Fragment>
                        <TableCell>
                            <Typography variant={"h6"} fontWeight={400}>
                                {UtilsFormat.numberToMoney(row.admin)}
                                <Toman/>
                            </Typography>
                        </TableCell>
                        <TableCell>
                            <Typography variant={"h6"} fontWeight={400}>
                                {UtilsFormat.numberToMoney(row.mt_profit)}
                                <Toman/>
                            </Typography>
                        </TableCell>
                        <TableCell>
                            <Typography variant={"h6"} fontWeight={400}>
                                {UtilsFormat.numberToMoney(row.dev)}
                                <Toman/>
                            </Typography>
                        </TableCell>
                        <TableCell>
                            <Typography variant={"h6"} fontWeight={400}>
                                {UtilsFormat.numberToMoney(row.tax)}
                                <Toman/>
                            </Typography>
                        </TableCell>
                        <TableCell>
                            <Typography variant={"h6"} fontWeight={400}>
                                {UtilsFormat.numberToMoney(row.charity)}
                                <Toman/>
                            </Typography>
                        </TableCell>
                    </React.Fragment>
                }
            </TableRow>
            <TableRow>
                <TableCell style={{paddingBottom: 0, paddingTop: 0}} colSpan={6}>
                    <Collapse in={open} timeout="auto" unmountOnExit>
                        <Box width={1} margin={1}>
                            <Typography py={0.5} variant="body2" gutterBottom component="div" color={orange[600]}>
                                اطلاعات زیر برای یک عدد محصول میباشد
                            </Typography>
                            <Table size="small" aria-label="purchases">
                                <TableHead>
                                    <TableRow>
                                        <TableCell>نام محصول</TableCell>
                                        <TableCell>تامین کننده</TableCell>
                                        <TableCell>قیمت فروش بدون تخفیف</TableCell>
                                        <TableCell>قیمت فروش</TableCell>
                                        {
                                            isAccountants &&
                                            <React.Fragment>
                                                <TableCell>قیمت خرید</TableCell>
                                                <TableCell>ادمین</TableCell>
                                                <TableCell>مهرتخفیف</TableCell>
                                                <TableCell>برنامه‌نویس</TableCell>
                                                <TableCell>ارزش‌افزوده</TableCell>
                                                <TableCell>خیریه</TableCell>
                                            </React.Fragment>
                                        }
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    <TableRow className={classes.root}>
                                        <TableCell component="th" scope="row">
                                            <Box display={'flex'} alignItems={'center'} minWidth={300}>
                                                <Link
                                                    href={rout.Product.Single.editProduct(more.id)}
                                                    onClick={(e) => {
                                                        e.stopPropagation();
                                                    }}>
                                                    <Typography py={1} variant={"body1"} fontWeight={400}>
                                                        {more.name[siteLang]}
                                                    </Typography>
                                                </Link>
                                                <Box pl={2}>
                                                    <Link
                                                        href={rout.Storage.Single.showStorageDashboard({productId: more.id})}
                                                        onClick={(e) => {
                                                            e.stopPropagation();
                                                        }}>
                                                        <Tooltip title={"انبار"}>
                                                            <IconButton>
                                                                <StorageOutlined/>
                                                            </IconButton>
                                                        </Tooltip>
                                                    </Link>
                                                </Box>
                                            </Box>
                                        </TableCell>
                                        <TableCell component="th" scope="row">
                                            <Box display={'flex'} flexDirection={'column'}>
                                                <Link
                                                    href={rout.Supplier.Single.editSupplier(more.supplier.id)}
                                                    onClick={(e) => {
                                                        e.stopPropagation();
                                                    }}>
                                                    {more.supplier.name}
                                                </Link>
                                                <Box pt={1}>
                                                    <Link
                                                        href={`tel:${more.supplier.username}`}
                                                        onClick={(e) => {
                                                            e.stopPropagation();
                                                        }}>
                                                        {more.supplier.username}
                                                    </Link>
                                                </Box>
                                            </Box>
                                        </TableCell>
                                        <TableCell>
                                            <Typography variant={"body1"} fontWeight={400}>
                                                {UtilsFormat.numberToMoney(more.final_price)}
                                                <Toman/>
                                            </Typography>
                                        </TableCell>
                                        <TableCell>
                                            <Typography variant={"body1"} color={green[600]} fontWeight={400}>
                                                {UtilsFormat.numberToMoney(more.discount_price)}
                                                <Toman color={"svgSuccessDark"}/>
                                            </Typography>
                                        </TableCell>

                                        {
                                            isAccountants &&
                                            <React.Fragment>
                                                <TableCell component="th" scope="row">
                                                    <Typography variant={"body1"} fontWeight={400}>
                                                        {UtilsFormat.numberToMoney(more.start_price)}
                                                        <Toman/>
                                                    </Typography>
                                                </TableCell>
                                                <TableCell component="th" scope="row">
                                                    <Typography variant={"body1"} fontWeight={400}>
                                                        {UtilsFormat.numberToMoney(more.admin)}
                                                        <Toman/>
                                                    </Typography>
                                                </TableCell>
                                                <TableCell component="th" scope="row">
                                                    <Typography variant={"body1"} fontWeight={400}>
                                                        {UtilsFormat.numberToMoney(more.mt_profit)}
                                                        <Toman/>
                                                    </Typography>
                                                </TableCell>
                                                <TableCell component="th" scope="row">
                                                    <Typography variant={"body1"} fontWeight={400}>
                                                        {UtilsFormat.numberToMoney(more.dev)}
                                                        <Toman/>
                                                    </Typography>
                                                </TableCell>
                                                <TableCell component="th" scope="row">
                                                    <Typography variant={"body1"} fontWeight={400}>
                                                        {UtilsFormat.numberToMoney(more.tax)}
                                                        <Toman/>
                                                    </Typography>
                                                </TableCell>
                                                <TableCell component="th" scope="row">
                                                    <Typography variant={"body1"} fontWeight={400}>
                                                        {UtilsFormat.numberToMoney(more.charity)}
                                                        <Toman/>
                                                    </Typography>
                                                </TableCell>
                                            </React.Fragment>
                                        }
                                    </TableRow>
                                </TableBody>
                            </Table>
                        </Box>
                    </Collapse>
                </TableCell>
            </TableRow>
        </React.Fragment>
    )

}

function MaxShippingTime({time}) {
    const [anchorEl, setAnchorEl] = React.useState(null);
    const open = Boolean(anchorEl);


    const handlePopoverOpen = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handlePopoverClose = () => {
        setAnchorEl(null);
    };


    return (
        <Box display={'flex'} flex={1} justifyContent={'flex-end'} px={2}>
            <ButtonBase
                onClick={handlePopoverOpen}>
                <Box
                    p={2}
                    aria-owns={open ? 'mouse-over-popover' : undefined}
                    aria-haspopup="true">
                    <Counter time={time}/>
                </Box>
            </ButtonBase>
            <Popover
                id="mouse-over-popover"
                open={open}
                anchorEl={anchorEl}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'left',
                }}
                onClose={handlePopoverClose}
                disableRestoreFocus>
                <Box display={'flex'} p={2} flexDirection={'column'}>
                    <Box display={'flex'} alignItems={'center'}>
                        <Typography variant={"body1"}>
                            حداکثر زمان ارسال:
                        </Typography>
                        <Typography dir={'ltr'} fontWeight={500} pr={1} component={'span'} variant={'subtitle1'}>
                            {JDate.timestampFormat(time, 'jYYYY-jMM-jDD dddd  HH:mm')}
                        </Typography>
                    </Box>
                </Box>
            </Popover>
        </Box>
    )
}


function Counter({time, ...props}) {

    const [timer, setTimer] = useState({
        days: undefined,
        hours: undefined,
        minutes: undefined,
        seconds: undefined
    })


    function serializeTime(val) {
        const v = _.toString(val);
        const a = v.length === 1 ? '0' + v : v;
        return a > 0 ? a : '00'
    }

    useEffect(() => {
        const interval = setInterval(() => {
            const liveDate = JDate.timeStampToJalali()
            const startDate = JDate.timeStampToJalali(time)
            const du = moment.duration(startDate.diff(liveDate));
            // gcLog("askflkaslkflkaslkflka", {
            //     liveDate: JDate.format(liveDate,"jYYYY-jMM-jDD dddd  HH:mm"),
            //     startDate: JDate.format(startDate,"jYYYY-jMM-jDD dddd  HH:mm"),
            //     days: _.toInteger(du.asDays() + 1),
            //     hours: _.toInteger(du.hours()),
            //     minutes: _.toInteger(du.minutes()),
            //     seconds: _.toInteger(du.seconds()),
            // })
            if (_.toInteger(du.seconds()) < 0) {
                tryIt(() => clearInterval())
                return
            }

            setTimer({
                months: _.toInteger(du.asMonths()),
                days: _.toInteger(du.asDays() + 1),
                hours: _.toInteger(du.hours()),
                minutes: _.toInteger(du.minutes()),
                seconds: _.toInteger(du.seconds())
            })
        }, 1000)
        return () => {
            clearInterval(interval)
        }
    }, [])

    return (
        <Box display={'flex'} justifyContent={'flex-end'} dir={'ltr'}>
            {
                (!_.isUndefined(timer.days) && timer.seconds >= 0) ?
                    <Box display={'flex'} flexDirection={'column'} alignItems={'center'}>
                        <Typography variant={"body2"} pb={1}>
                            حداکثر زمان باقی مانده تا ارسال
                        </Typography>
                        <Box display={'flex'} alignItems={'center'} justifyContent={'flex-end'} dir={'ltr'}>
                            {
                                timer.months &&
                                <React.Fragment>
                                    <Typography variant={"body2"}>
                                        ماه
                                    </Typography>
                                    <Box display={'flex'} justifyContent={'center'} pr={0.5} pl={1.5} fontSize={20}
                                         fontWeight={500}>
                                        {timer.months}
                                    </Box>
                                </React.Fragment>
                            }
                            {
                                (timer.months || timer.hours) &&
                                <React.Fragment>
                                    <Typography variant={"body2"}>
                                        روز
                                    </Typography>
                                    <Box display={'flex'} justifyContent={'center'} pr={0.5} pl={1.5} fontSize={20}
                                         fontWeight={500}>
                                        {timer.days}
                                    </Box>
                                </React.Fragment>
                            }

                            <Box display={'flex'} justifyContent={'center'} minWidth={40} fontSize={20}
                                 fontWeight={500}>
                                {serializeTime(timer.hours)}
                            </Box>
                            <Box fontSize={20} fontWeight={500}>
                                :
                            </Box>
                            <Box display={'flex'} justifyContent={'center'} minWidth={40} fontSize={20}
                                 fontWeight={500}>
                                {serializeTime(timer.minutes)}
                            </Box>
                            <Box fontSize={20} fontWeight={500}>
                                :
                            </Box>
                            <Box display={'flex'} justifyContent={'center'} minWidth={40} fontSize={20}
                                 fontWeight={500}>
                                {serializeTime(timer.seconds)}
                            </Box>
                        </Box>
                    </Box> :
                    time ?
                        <Box display={'flex'} alignItems={"center"}>
                            {(() => {
                                const du = JDate.timeStampToJalali(time);
                                const year = du.format("jYYYY");
                                const mont = du.format("jMMMM - jMM");
                                const day = du.format("jD");
                                const hour = du.format("HH");
                                const min = du.format("mm");

                                const El = ({val, ...props}) => {
                                    return (
                                        <Typography variant={"h6"} {...props}>
                                            {val}
                                        </Typography>
                                    )
                                }
                                return (
                                    <React.Fragment>
                                        <El val={year} pr={0.5}/>
                                        <El val={"/"}/>
                                        <El val={mont} px={0.5}/>
                                        <El val={"/"} pl={0.5}/>
                                        <El val={day}/>
                                        <El val={hour} pl={3} pr={0.5}/>:
                                        <El val={min} pl={0.5}/>
                                    </React.Fragment>
                                )
                            })()}
                        </Box> :
                        <React.Fragment/>
            }
        </Box>
    )
}
