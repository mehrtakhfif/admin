import React, {useState} from "react";
import Box from "@material-ui/core/Box";
import {grey} from "@material-ui/core/colors";
import _ from "lodash";
import Link from "../../components/base/link/Link";
import BaseButton from "../../components/base/button/BaseButton";
import {Card} from "@material-ui/core";
import Typography from "../../components/base/Typography";
import Icon from "@material-ui/core/Icon";
import BaseDialog from "../../components/base/BaseDialog";
import DialogItems from "../../components/base/Items/DialogItems";
import {createItemCard} from "../../components/base/Items/ItemCard";



const dashboard = [
    createItemCard({
        icon: <Icon
            fontSize={"large"} className={"fal fa-box-check"}/>,
        title: "آمار محصولات فروخته شده",
        dialog: (props) => <InvoiceProductsDialog {...props}/>
    })
]

export default function ({...props}) {

    return (
        <Box display={'flex'} flexDirection={'column'}>
            <Box py={1} display={'flex'} flexWrap={"wrap"}>
                {dashboard.map((item, i) => (
                    <DialogItems key={i} item={item}/>
                ))}
            </Box>
        </Box>
    )
}


function InvoiceProductsDialog({open, onClose}) {

    return (
        <BaseDialog maxWidth={"xl"} header={true} open={open} onClose={onClose}>
            <Box p={2} width={1}>
                DateDialog
            </Box>
        </BaseDialog>
    )
}
