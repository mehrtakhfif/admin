import React, {useEffect, useState} from "react";
import Box from "@material-ui/core/Box";
import {useDispatch, useSelector} from "react-redux";
import ControllerProduct from "../../controller/ControllerProduct";
import Table, {createTableHeader, headerItemTemplate} from "../../components/base/table/Table";
import {createFilter} from "../../components/base/table/Filters";
import _ from 'lodash';
import {lang, siteLang, theme} from "../../repository";
import rout from "../../router";
import DataUtils from "../../utils/DataUtils";
import Link from "../../components/base/link/Link";
import {FiberManualRecordTwoTone, ScreenShareOutlined, VisibilityOutlined} from "@material-ui/icons";
import IconButton from "@material-ui/core/IconButton";
import Typography from "../../components/base/Typography";
import Tooltip from "@material-ui/core/Tooltip";
import storage from "../../storage";
import useSWR, {mutate} from 'swr'
import ComponentError from "../../components/base/ComponentError";
import {deliver_status} from "../../controller/converter";
import Img from "../../components/base/img/Img";
import {UtilsFormat, UtilsStyle} from "../../utils/Utils";
import {cyan, grey} from "@material-ui/core/colors";
import {gcLog} from "../../utils/ObjectUtils";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import Backdrop from "@material-ui/core/Backdrop";
import CircularProgress from "@material-ui/core/CircularProgress";

const headers = [
    ...headerItemTemplate.idAction(),
    createTableHeader({id: "thumbnail", type: 'str', label: 'تامبنیل', hasPadding: false, sortable: false}),
    headerItemTemplate.name,
    createTableHeader({id: "deliver_status", type: 'str', label: 'وضعیت', hasPadding: false}),
    createTableHeader({id: "count", type: 'str', label: 'تعداد', hasPadding: false}),
    createTableHeader({id: "purchase_date", type: 'str', label: 'تاریخ خرید', dir: 'ltr'}),
    createTableHeader({id: "supplier", type: 'str', label: 'تامین کننده', sortable: false}),
    createTableHeader({id: "user", type: 'str', label: 'مشتری', sortable: false}),
    createTableHeader({id: "unit_price", type: 'str', label: 'قیمت هر واحد', sortable: false}),
    createTableHeader({id: "discount_price", type: 'str', label: 'قیمت فروش', sortable: false}),
];
const filters = [];
const CookieKey = "invoice_product";

export default function ({...props}) {
    //region var
    const dispatch = useDispatch();
    const {boxes,roll, ...user} = useSelector(state => state.user);
    const isAccountants = (roll === "superuser" || roll === "accountants");
    const [tableData, setTableData] = useState()
    const [editItem, setEditItem] = useState(undefined)
    const [setting, setSetting] = useState({
        loading: false
    })

    //region state
    const [filtersState, setFiltersState] = useState({
        data: filters,
        resetFilters: () => {
            setFiltersState({
                ...filtersState,
                data: DataUtils().TableFilter().resetFilter({filters: filtersState.data}),
            })
        }
    });

    const [state, setState] = useState({
        filters: _.cloneDeep(filters),
        activeBox: storage.PageSetting.getActiveBox(CookieKey, boxes),
        order: undefined,
        pagination: {
            rowPerPage: undefined,
            page: undefined
        }
    });


    function getCookieKey() {
        try {
            return CookieKey + state.activeBox
        } catch (e) {
        }
        return CookieKey + storage.PageSetting.getActiveBox(getCookieKey(), boxes)
    }

    //endregion state
    //endregion var


    //region requests
    const d = ControllerProduct.InvoiceProduct.get({
        boxId: state.activeBox,
        page: state.pagination.page + 1,
        step: state.pagination.rowPerPage,
        order: state.order,
        filters: state.filters,
    });


    const {data: dd, error} = useSWR(d[0], () => {
        if (state.order === undefined) {
            return {}
        }

        return d[1]()
    });



    function requestFilters() {
        ControllerProduct.Products.Filters.get({boxId: state.activeBox}).then((res) => {
            const newList = (_.isArray(filtersState.data) && !_.isEmpty(filtersState.data)) ? filtersState.data : filters;
            _.forEach(res.data, (fi) => {
                const data = createFilter({id: fi.name, type: fi.type, label: fi.label, data: fi.filters, value: null});
                switch (fi.name) {
                    case 'categories': {
                        data.value = [];
                        break;
                    }
                    default: {
                    }
                }

                const index = _.findIndex(newList, (l) => l.id === data.id);
                newList[index !== -1 ? index : newList.length] = data;
            });
            setFiltersState({
                ...filtersState,
                data: [...newList]
            })
        }).catch((e) => {
            setFiltersState({
                ...filtersState,
                data: false
            })
        });
    }

    //endregion requests

    //region useEffect
    useEffect(() => {
        if (!state.activeBox)
            return;
        setState({
            ...state,
            order: storage.PageSetting.getOrder(getCookieKey()),
            pagination: {
                rowPerPage: storage.PageSetting.getRowPerPage(getCookieKey()),
                page: storage.PageSetting.getPage(getCookieKey())
            }
        })
        requestFilters();
    }, [state.activeBox]);

    useEffect(() => {
        storage.PageSetting.setPage(getCookieKey(), {page: state.pagination.page});
    }, [state.pagination.page]);

    useEffect(() => {
        setTableData((dd && dd.data ? dd.data : [...Array(state.pagination.rowPerPage)]).map((pr, i) => {
            gcLog("xfl;kvndflkbndb'lkcnfblkb'gfkmnsflgkmns'fgknfgn",pr)
                if (!pr)
                    return {}
                let res = {
                    id: {
                        label: pr.id,
                    },
                    actions: {
                        label:
                            <Box display={'flex'} alignItems={'center'} pl={1}>
                                {
                                    user.roll !== "admin" &&
                                    <Tooltip title={"نمایش فاکتور"}>
                                        <IconButton size={"small"}
                                                    no_hover={"true"}
                                                    onClick={(e) => {
                                                        e.stopPropagation();
                                                    }}
                                                    style={{
                                                        marginRight: theme.spacing(0.5),
                                                        marginLeft: theme.spacing(0.5)
                                                    }}>

                                            <Link toHref={rout.Invoice.Single.showInvoice(pr.invoice_id)}
                                                  linkStyle={{display: 'flex'}}
                                                  hoverColor={theme.palette.primary.main}>
                                                <VisibilityOutlined style={{fontSize: 20, padding: 2}}/>
                                            </Link>
                                        </IconButton>
                                    </Tooltip>
                                }
                                <Tooltip title={"تغییر وضعیت"}>
                                    <IconButton size={"small"}
                                                no_hover={"true"}
                                                onClick={(e) => {
                                                    setEditItem({e: e.currentTarget, item: pr})
                                                    e.stopPropagation();
                                                }}
                                                style={{
                                                    marginRight: theme.spacing(0.5),
                                                    marginLeft: theme.spacing(0.5)
                                                }}>
                                        <Typography variant={'body1'}
                                                    hoverColor={theme.palette.primary.main}
                                                    style={{display: 'flex'}}>
                                            <ScreenShareOutlined style={{fontSize: 20, padding: 2}}/>
                                        </Typography>
                                    </IconButton>
                                </Tooltip>
                            </Box>
                    },
                    thumbnail: {
                        label: (pr.product && pr.product.thumbnail) &&
                            <Img src={pr.product.thumbnail.image}
                                 zoomable={true}
                                 width={60} maxWidth={'auto'} minHeight={'auto'}
                                 onClick={(e) => {
                                     e.stopPropagation();
                                 }}
                                 onClose={(e) => {
                                     try {
                                         e.stopPropagation();
                                     } catch (e) {
                                     }
                                 }}
                                 style={{cursor: "pointer", ...UtilsStyle.borderRadius(5)}}/>
                    },
                    name: {
                        label: pr.storage.storageTitle[siteLang],
                        link: rout.Storage.Single.showStorageDashboard({productId:pr.storage.id})
                    },
                    deliver_status: {
                        label: (
                            <Box display={'flex'} flexWrap={'wrap'} justifyContent={'center'} alignItems={'center'}
                                 maxWidth={300}>
                                <Tooltip title={pr.deliver_status.label}>
                                    <FiberManualRecordTwoTone style={{color: pr.deliver_status.color}}/>
                                </Tooltip>
                            </Box>
                        ),
                    },
                    count: {
                        label: pr.count,
                    },
                    purchase_date: {
                        label: pr.purchase_date
                    },
                    supplier: {
                        label: (
                            <Box py={0.25} display={'flex'} flexDirection={'column'} alignItems={'center'}
                                 justifyContent={'center'}>
                                <Typography variant={"body2"} pb={1}>
                                    {pr.storage.supplier.fullName}
                                </Typography>
                                <Link
                                    href={`tel:${pr.storage.supplier.username}`}
                                    onClick={(e) => {
                                        e.stopPropagation();
                                    }}>
                                    <Typography
                                        justifyContent="center" variant={'body2'} color={grey[800]}
                                        justify="center">
                                        {pr.storage.supplier.username}
                                    </Typography>
                                </Link>
                            </Box>
                        )
                    },
                    user: {
                        label: (
                            <Box py={0.25} display={'flex'} flexDirection={'column'} aligItems={'center'}
                                 justifyContent={'center'}>
                                <Typography variant={"body2"} pb={1}>
                                    {pr.user.fullName}
                                </Typography>
                                <Link
                                    href={`tel:${pr.user.username}`}
                                    onClick={(e) => {
                                        e.stopPropagation();
                                    }}>
                                    <Typography
                                        justifyContent="center" variant={'body2'} color={grey[800]}
                                        justify="center">
                                        {pr.user.username}
                                    </Typography>
                                </Link>
                            </Box>
                        )
                    },
                    unit_price: {
                        label: UtilsFormat.numberToMoney(pr.unit_price)
                    },
                    discount_price: {
                        label: UtilsFormat.numberToMoney(pr.discount_price)
                    },
                };
                if (isAccountants)
                    res = {
                        ...res,
                        admin:{
                            label: UtilsFormat.numberToMoney(pr.admin)
                        },
                        tax:{
                            label: UtilsFormat.numberToMoney(pr.tax)
                        },
                        dev:{
                            label: UtilsFormat.numberToMoney(pr.dev)
                        },
                        charity:{
                            label: UtilsFormat.numberToMoney(pr.charity)
                        }
                    }
                return res
            }
        ))
    }, [dd])
    //endregion useEffect

    //region handler
    function handleFilterChange({item, value}) {
        setState({
            ...state,
            pagination: {
                ...state.pagination,
                page: 0
            }
        })
        setFiltersState({
            ...filtersState,
            data: DataUtils().TableFilter().filterItemChange({filters: filtersState.data, item: item, value: value})
        });
    }

    function handleApplyFilter() {
        setState({
            ...state,
            filters: _.cloneDeep(filtersState.data)
        })
    }

    function handleResetFilter() {
        filtersState.resetFilters()
    }

    function handleChangeOrder(order) {
        storage.PageSetting.setOrder(getCookieKey(), {
            order: order
        });
        setState({
            ...state,
            order: {
                ...order
            }
        });
    }

    function handleChangeBox(e, box) {
        if (!box || state.activeBox === box.id)
            return;
        storage.PageSetting.setActiveBox(CookieKey, {activeBox: box.id});
        filtersState.resetFilters();
        setState({
            ...state,
            activeBox: box.id,
            pagination: {
                ...state.pagination,
                page: 0
            }
        });
    }

    function handleStateChange(deliver_status, item) {
        setEditItem(undefined);
        setSetting({
            ...setting,
            loading: true
        });
        ControllerProduct.InvoiceProduct.update({id: item.id, deliver_status: deliver_status.key})
            .then(() => mutate(d[0])).finally(() => {
            setSetting({
                ...setting,
                loading: false
            });
        })
    }

    //endregion handler

    //region renderData
    const pg = state.pagination;
    //endregion renderData

    return (
        <Box
            my={2}
            px={2}
            display={'flex'}
            flexDirection={'column'}>
            {(!error && state.order) ?
                <Table headers={headers.concat(isAccountants ? [
                    createTableHeader({id: "admin", type: 'str', label: 'ادمین', sortable: false}),
                    createTableHeader({id: "tax", type: 'str', label: 'ارزش‌افزوده', sortable: false}),
                    createTableHeader({id: "dev", type: 'str', label: 'برنامه‌نویس', sortable: false}),
                    createTableHeader({id: "charity", type: 'str', label: 'خیریه', sortable: false}),
                ] : [])}
                       cookieKey={getCookieKey()}
                       title={lang.get("products")}
                       data={tableData}
                       filters={filtersState.data}
                       onFilterChange={handleFilterChange}
                       loading={false}
                       activePage={pg.page}
                       rowsPerPage={pg.rowPerPage}
                       lastPage={dd ? dd.pagination.lastPage : 0}
                       rowCount={dd ? dd.pagination.count : 0}
                       orderType={state.order}
                       activeBox={state.activeBox}
                       addNewButton={{
                           label: lang.get('add_new_product'),
                           link: rout.Product.Single.createNewProduct,
                       }}
                       onActivePageChange={(page) => {
                           setState({
                               ...state,
                               pagination: {
                                   ...state.pagination,
                                   page: page
                               }
                           })
                           // requestData({page: e})
                       }}
                       onRowPerPageChange={(rowPerPage) => {
                           storage.PageSetting.setRowPerPage(getCookieKey(), {rowPerPage: rowPerPage});
                           setState({
                               ...state,
                               pagination: {
                                   ...state.pagination,
                                   rowPerPage: rowPerPage
                               }
                           })
                       }}
                       onApplyFilterClick={handleApplyFilter}
                       onResetFilterClick={handleResetFilter}
                       onChangeOrder={handleChangeOrder}
                       onChangeBox={handleChangeBox}/> :
                <Box>
                    <ComponentError
                        statusCode={(_.isObject(error) && error.request) ? error.request.status : undefined}
                        tryAgainFun={() => mutate(d[0])}/>
                </Box>
            }
            <Menu
                id="simple-menu"
                anchorEl={editItem ? editItem.e : undefined}
                keepMounted
                open={Boolean(editItem)}
                onClose={() => {
                    setEditItem(undefined)
                }}>
                {Object.keys(deliver_status).map(k => {
                    const item = deliver_status[k];
                    return (
                        (user.roll === "admin" ? (k === "pending" || k === 'packing' || k === 'referred') : true) ?
                            <MenuItem
                                key={k}
                                onClick={() => {
                                    handleStateChange(item, editItem.item)
                                }}>
                                {item.label}
                            </MenuItem> :
                            <React.Fragment/>
                    )
                })}
            </Menu>

            <Backdrop open={setting.loading}
                      style={{
                          zIndex: '9999'
                      }}>
                <CircularProgress color="inherit"
                                  style={{
                                      color: cyan[300],
                                      width: 100,
                                      height: 100
                                  }}/>
            </Backdrop>
        </Box>
    )
}
