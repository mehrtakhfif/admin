import React, {useEffect, useState} from "react";
import ControllerUser from "../controller/ControllerUser";
import {useDispatch, useSelector} from "react-redux";
import rout from "../router";
import {Redirect} from "react-router-dom";
import {DEBUG, webRout} from "../repository";
import {Hidden} from "@material-ui/core";
import Box from "@material-ui/core/Box";
import {gcLog} from "../utils/ObjectUtils";


function Index({...props}) {
    const dispatch = useDispatch();
    const [redirect, setRedirect] = useState(false);
    let redirectTo = rout.Main.dashboard;
    const {isLogin} = useSelector(state => state.user);
    try {
        redirectTo = props.history.location.state.from.pathname+props.history.location.state.from.search
    } catch (e) {
    }

    useEffect(() => {
        if (isLogin) {
            setRedirect(true);
            return;
        }
        ControllerUser.Roll.get(dispatch).then((res) => {
            if (res.status === 200)
                return;
            goToSite();
        }).catch(() => {
            goToSite()
        })
    }, []);


    function goToSite() {
        if (DEBUG)
            return;
        window.location.href = webRout
    }

    return (
        <Box>
            {redirect &&
            <React.Fragment>
                <Redirect
                    to={{
                        pathname: redirectTo,
                    }}
                />
            </React.Fragment>}
        </Box>
    )
}
export default Index;


export function ShowSM({...props}) {
    return (
        <Hidden lgUp={true}>
            {props.children}
        </Hidden>
    )
}

export function HiddenSM({...props}) {
    return (
        <Hidden mdDown={true}>
            {props.children}
        </Hidden>
    )
}
