import React from "react";
import Box from "@material-ui/core/Box";
import BaseButton from "../../components/base/button/BaseButton";
import Link from "../../components/base/link/Link";
import rout from "../../router";
import Typography from "../../components/base/Typography";
import {useTheme, Tabs, Tab} from "@material-ui/core";
import {amber, cyan, orange, teal} from "@material-ui/core/colors";
import {Item} from "../box/BoxDashboard";
import PropTypes from 'prop-types';
import Ads from "./ads/Ads";
import Slider from "./slider/Slider";


function TabPanel(props) {
    const {children, value, index, ...other} = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box sx={{p: 3}}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.number.isRequired,
    value: PropTypes.number.isRequired,
};

function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}

export default function SiteDashboard({...props}) {
    const [value, setValue] = React.useState(0);
    const handleChange = (event, newValue) => {
        setValue(newValue);
    };
    const theme = useTheme();

    return (
        <Box sx={{width: '100%'}}>
            <Box sx={{borderBottom: 1, borderColor: 'divider'}}>
                <Tabs value={value} onChange={handleChange} aria-label="basic tabs example">
                    <Tab label="تبلیغات" {...a11yProps(0)} />
                    <Tab label="اسلایدر" {...a11yProps(1)} />
                </Tabs>
            </Box>
            <TabPanel value={value} index={0}>
                <Ads/>
            </TabPanel>
            <TabPanel value={value} index={1}>
                <Slider/>
            </TabPanel>
        </Box>

    )
}



