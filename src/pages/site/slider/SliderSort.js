import React, {Fragment, useEffect, useState} from "react";
import {Dialog, useTheme} from "@material-ui/core";
import Transition from "../../../components/base/Transition";
import {Box,DialogContent,DialogTitle,DialogContentText,DialogActions} from "@material-ui/core";
import BaseButton from "../../../components/base/button/BaseButton";
import useSWR from "swr/esm/use-swr";
import ControllerBox from "../../../controller/ControllerBox";
import {gcLog} from "../../../utils/ObjectUtils";
import PleaseWait from "../../../components/base/loading/PleaseWait";
import ComponentError from "../../../components/base/ComponentError";
import {mutate} from "swr";
import ControllerSite from "../../../controller/ControllerSite";
import {cyan, green, grey, orange, red} from "@material-ui/core/colors";
import {UtilsStyle} from "../../../utils/Utils";
import {ComputerOutlined, DeleteOutline, Menu, MenuOpen, PhoneAndroidOutlined} from "@material-ui/icons";
import Img from "../../../components/base/img/Img";
import Typography from "../../../components/base/Typography";
import DraggableList from "../../../components/base/draggableList/DraggableList";
import {lang, siteLang} from "../../../repository";
import Skeleton from "@material-ui/lab/Skeleton";
import _ from 'lodash'
import Button from "@material-ui/core/Button";
import {useSnackbar} from "notistack";
import IconButton from "@material-ui/core/IconButton";
import SaveDialog from "../../../components/dialog/SaveDialog";
import SliderSelector from "./SliderGrid";
import {featureType} from "../../../controller/type";

export default function SliderSort({open, type = "home", onClose, ...props}) {
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();
    const theme = useTheme()
    const [state, setState] = useState({
        add: false,
        desktop: true,
        save: false
    })
    const [data, setData] = useState(undefined);
    const apiKey = `slider-priority-${type}-${open}`
    const boxApiKey = `box-${open}`
    const {data: da, error} = useSWR(apiKey, () => {
        if (!open)
            return undefined;
        return ControllerSite.Slider.getActives({type: type})
    })
    const {data: boxes, error: boxesError} = useSWR(boxApiKey, () => {
        if (!open)
            return undefined;
        return ControllerBox.getActive()
    })

    useEffect(() => {
        setData(da ? da.data : undefined)
    }, [da])


    function Output() {
        if (error)
            return <ComponentError tryAgainFun={() => mutate(apiKey)}/>
        if (data)
            return ( <Box display={'flex'} flexWrap={'wrap'} py={2}>
                <DraggableList
                    items={data}
                    onItemsChange={(items) => {
                        setData(items);
                    }}
                    rootStyle={(dragging) => {
                        return {
                            backgroundColor: dragging ? null : grey[200],
                            ...UtilsStyle.transition(500)
                        }
                    }}
                    render={(item, props, {isDraggingItem, index, ...p}) => (
                        <Box key={item.id}
                             display={'flex'}
                             width={1}
                             {...props}>
                            <Box display={'flex'} width={1} alignItems={'center'}
                                 py={1}>
                                <Box px={2} py={1}>
                                    {isDraggingItem ?
                                        <MenuOpen/> :
                                        <Menu/>
                                    }
                                </Box>
                                <Box p={1}>
                                    <Img maxWidth={160} zoomable={true} minHeight={'unset'}
                                         src={item.media ? item.media.image : ""}
                                         style={{
                                             overflow:'hidden',
                                             ...UtilsStyle.borderRadius(5)
                                         }}/>
                                </Box>
                                <Box flex={1}>
                                    <Typography  pr={2} pl={1} variant={'body1'}
                                                 fontWeight={600}
                                                 style={{
                                                     justifyContent: 'start',
                                                 }}>
                                        {index + 1}- {item.title[siteLang]}
                                    </Typography>
                                </Box>
                                <Box px={1}>
                                    <IconButton
                                        onClick={() => {
                                            const newList = data;
                                            _.remove(newList, (d) => d.id === item.id)
                                            setData([...newList])
                                        }}>
                                        <DeleteOutline style={{color: red[400]}}/>
                                    </IconButton>
                                </Box>
                            </Box>
                        </Box>
                    )}
                />
                <SaveDialog
                    open={state.save}
                    text={(
                        <Box display={'flex'} flexDirection={'column'}
                             px={3}>
                            <Typography variant={'h6'} fontWeight={400}
                                        pb={2}>
                                ذخیره محصول
                            </Typography>
                            <Typography variant={'body1'}>
                                آیا از ذخیره این محصول اطمینان دارید؟
                            </Typography>
                            <Typography variant={'caption'}
                                        color={theme.palette.text.secondary}
                                        pt={0.5}>
                                <span style={{color: orange[300], fontWeight: 600, paddingLeft: theme.spacing(0.3)}}>
                                    توجه:
                                </span>
                                در صورت ذخیره، اطلاعات تغییریافته قابل
                                بازگشت
                                نمیباشد.
                            </Typography>
                        </Box>)}
                    onCancel={() => {
                        setState({
                            ...state,
                            save: false
                        })
                    }}
                    onSubmit={() => {
                        save()
                    }}/>
            </Box> )
        return <PleaseWait/>

    }

    function save() {
        const idList = []
        _.forEach(data, (d, i) => {
            idList.push(d.id)
        })
        ControllerSite.Slider.setPriorities({data: idList}).then(res => {
            setState({
                ...state,
                save: false
            })
            onClose(true)
        })
    }

    function reqCancel(text) {
        enqueueSnackbar(text,
            {
                variant: "error",
                action: (key) => (
                    <Fragment>
                        <Button onClick={() => {
                            closeSnackbar(key)
                        }}>
                            {lang.get('close')}
                        </Button>
                    </Fragment>
                )
            });
    }

    return (
        <Dialog maxWidth={'md'} open={open}
                TransitionComponent={Transition}>
            <DialogTitle>مرتب‌سازی اسلایدر ها</DialogTitle>
            <DialogContent>
                <DialogContentText>
                    با درگ کردن اسلایدرها آن ها را مرتب کنید
                </DialogContentText>
                <Output/>
            </DialogContent>
            <DialogActions
                display={'flex'}>
                <Box pl={2}>
                    <Button variant={"outlined"}
                            onClick={() => {
                                setState({
                                    ...state,
                                    save: true
                                })
                            }}>
                        ذخیره
                    </Button>
                </Box>
                <Box pl={2}>
                    <Button variant={"outlined"}
                            onClick={() => {
                                onClose()
                            }}>
                        بستن
                    </Button>
                </Box>
            </DialogActions>
            {/*<Box display={'flex'} p={2} flexDirection={'column'}>
                <Box display={'flex'}>
                    <Box pl={2}>
                        <BaseButton variant={"outlined"}
                                    onClick={() => {
                                        setState({
                                            ...state,
                                            save: true
                                        })
                                    }}
                                    style={{
                                        borderColor: green[400]
                                    }}>
                            ذخیره
                        </BaseButton>
                    </Box>
                    <Box pl={2}>
                        <BaseButton variant={"outlined"}
                                    onClick={() => {
                                        onClose()
                                    }}
                                    style={{
                                        borderColor: red[300]
                                    }}>
                            بستن
                        </BaseButton>
                    </Box>
                </Box>
                {
                    !error ?
                        data ?
                           :
                            :

                }
            </Box>*/}
        </Dialog>
    )
}
