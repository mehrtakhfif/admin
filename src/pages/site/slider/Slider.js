import React, {useState} from "react";
import Box from "@material-ui/core/Box";
import {useTheme, Button} from "@material-ui/core";
import {cyan, green, orange} from "@material-ui/core/colors";
import {Item} from "../../box/BoxDashboard";
import SelectDialog, {createItem} from "../../../components/dialog/SelectDialog";
import rout from "../../../router";
import SliderEdit from "./SliderEdit";
import SliderSort from "./SliderSort";
import SliderGrid from "./SliderGrid";
import AdsGrid from "../ads/AdsGrid";
import AdsSort from "../ads/AdsSort";
import {mutate} from "swr"


export default function ({...props}) {
    const theme = useTheme();
    const [selectedItemIndex, setSelectedItemIndex] = useState(0)
    const [key, setKey] = useState(undefined)
    const [openSort, setOpenSort] = useState(false)
    const [addDialog, setAddDialog] = useState(false)

    return (
        <Box width={1} p={2}>
            <Box display={'flex'} flexDirection={'row'}>
                <Button
                    onClick={() => {
                        setOpenSort(s => !s)
                    }}>
                    مرتب‌سازی
                </Button>
                <Button
                    onClick={() => {
                        setAddDialog(s => !s)
                    }}>
                    افزودن
                </Button>

            </Box>
            <SliderGrid/>
            <SliderEdit
                open={addDialog}
                item={undefined}
                onClose={() => {
                    setAddDialog(false)
                }}/>
            <SliderSort
                open={openSort}
                type={"home"}
                editable={false}
                selectable={true}
                addable={true}
                onClose={() => {
                    setOpenSort(false)
                    mutate('slider-SliderGrid')
                }}/>
        </Box>
    )
}

