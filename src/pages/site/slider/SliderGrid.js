import React, {useState} from "react";
import Box from "@material-ui/core/Box";
import useSWR from "swr/esm/use-swr";
import ControllerSite from "../../../controller/ControllerSite";
import BaseDialog from "../../../components/base/BaseDialog";
import ComponentError from "../../../components/base/ComponentError";
import {mutate} from "swr";
import PleaseWait from "../../../components/base/loading/PleaseWait";
import Img from "../../../components/base/img/Img";
import {siteLang, theme} from "../../../repository";
import {Add, ComputerOutlined, EditOutlined, PhoneAndroidOutlined} from "@material-ui/icons";
import Tooltip from "../../../components/base/Tooltip";
import {red} from "@material-ui/core/colors";
import {UtilsStyle} from "../../../utils/Utils";
import Typography from "../../../components/base/Typography";
import Link from "../../../components/base/link/Link";
import ButtonBase from "@material-ui/core/ButtonBase";
import {Card} from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import BaseButton from "../../../components/base/button/BaseButton";
import SliderEdit from "./SliderEdit";

export default function SliderGrid ({...props}) {
    const apiKey = `slider-SliderGrid`
    const {data, error} = useSWR(apiKey, () => {
        return ControllerSite.Slider.get({type: "home", o: "-updated_at"})
    })


    return (
            <Box width={1} display={'flex'} flexWrap={'wrap'} p={2}>
                {
                    !error ?
                        data ?
                            <React.Fragment>
                                {data.data.map(d =>
                                    <Item
                                        key={d.id}
                                        item={d}
                                        onChange={() => mutate(apiKey)}/>)}
                            </React.Fragment> :
                            <PleaseWait/> :
                        <ComponentError tryAgainFun={mutate(apiKey)}/>}

            </Box>
    )
}


function Item({item, onChange, onSelect}) {
    const [state, setState] = useState({
        edit: false
    })


    const cm = (
        <Box
            display={'flex'}
            flexDirection={'column'}
            component={Card}
            onClick={() => {
                setState({
                    ...state,
                    edit: true
                })}}
            p={2}>
            <Box display={'flex'}>
                <Box pr={2}
                     style={{
                         position: 'relative',
                         ...UtilsStyle.borderRadius(5)
                     }}>
                    <Img
                        minHeight={1}
                        src={item.media ? item.media.image : ""}
                        zoomable={true}/>
                    <Box
                        width={35}
                        height={35}
                        display={'flex'}
                        alignItems={'center'}
                        justifyContent={'center'}
                        style={{
                            position: 'absolute',
                            backgroundColor: "rgba(224, 224, 224, 0.63)",
                            zIndex: 10,
                            left: '50%',
                            bottom: 0,
                            transform: 'translate(-50%, 0)',
                            ...UtilsStyle.borderRadius('50%')
                        }}>
                        <Tooltip title={"کامپیوتر"}>
                            <ComputerOutlined fontSize={"small"}/>
                        </Tooltip>
                    </Box>
                </Box>
                <Box pl={2}
                     style={{
                         position: 'relative',
                         ...UtilsStyle.borderRadius(5)
                     }}>
                    <Img
                        minHeight={1}
                        src={item.mobile_media ? item.mobile_media.image : ""}
                        zoomable={true}/>
                    <Box
                        width={35}
                        height={35}
                        alignItems={'center'}
                        justifyContent={'center'}
                        display={'flex'}
                        style={{
                            position: 'absolute',
                            backgroundColor: "rgba(224, 224, 224, 0.63)",
                            zIndex: 10,
                            left: '50%',
                            bottom: 0,
                            transform: 'translate(-50%, 0)',
                            ...UtilsStyle.borderRadius('50%')
                        }}>
                        <Tooltip title={"موبایل"}>
                            <PhoneAndroidOutlined fontSize={"small"}/>
                        </Tooltip>
                    </Box>
                </Box>
            </Box>
            {item.title &&
            <Typography pt={2} variant={"h6"}>
                {item.title[siteLang]}
            </Typography>
            }
            {
                item.url &&
                <Link
                    toHref={item.url}
                    target={"_blank"}
                    style={{
                        marginTop: theme.spacing(1)
                    }}>
                    لینک
                </Link>
            }
        </Box>
    )

    return (
        <Box key={item.id} width={1 / 3} p={1} display={'flex'} flexDirection={'column'}>
            <ButtonBase
                    onClick={onSelect}
                    style={{
                        padding: 0
                    }}>
                    {cm}
                </ButtonBase>
            <SliderEdit
                open={state.edit}
                type={"home"}
                item={item}
                onClose={(newItem) => {
                    setState({
                        ...state,
                        edit: false
                    })
                    if (!newItem)
                        return
                    onChange()
                }}/>
        </Box>
    )
}
