import React, {Fragment, useEffect, useState} from "react";
import {Dialog, useTheme,DialogActions,DialogContentText,DialogContent,DialogTitle} from "@material-ui/core";
import Transition from "../../../components/base/Transition";
import Box from "@material-ui/core/Box";
import BaseButton from "../../../components/base/button/BaseButton";
import AdsGrid from "./AdsGrid";
import useSWR from "swr/esm/use-swr";
import ControllerBox from "../../../controller/ControllerBox";
import {gcLog} from "../../../utils/ObjectUtils";
import PleaseWait from "../../../components/base/loading/PleaseWait";
import ComponentError from "../../../components/base/ComponentError";
import {mutate} from "swr";
import ControllerSite from "../../../controller/ControllerSite";
import {cyan, green, grey, orange, red} from "@material-ui/core/colors";
import {UtilsStyle} from "../../../utils/Utils";
import {ComputerOutlined, DeleteOutline, Menu, MenuOpen, PhoneAndroidOutlined} from "@material-ui/icons";
import Img from "../../../components/base/img/Img";
import Typography from "../../../components/base/Typography";
import DraggableList from "../../../components/base/draggableList/DraggableList";
import {lang, siteLang} from "../../../repository";
import Skeleton from "@material-ui/lab/Skeleton";
import _ from 'lodash'
import Button from "@material-ui/core/Button";
import {useSnackbar} from "notistack";
import IconButton from "@material-ui/core/IconButton";
import SaveDialog from "../../../components/dialog/SaveDialog";
import BaseDialog from "../../../components/base/BaseDialog";

export default function AdsSort ({open,type="home", onClose, ...props}) {
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();
    const theme = useTheme()
    const [state, setState] = useState({
        add: false,
        desktop: true,
        save:false
    })
    const [data, setData] = useState(undefined);
    const apiKey = `ads-priority-${type}-${open}`
    const boxApiKey = `box-${open}`
    const {data: da, error} = useSWR(apiKey, () => {
        if (!open)
            return undefined;
        return ControllerSite.Ads.getActives({type:type})
    })
    const {data: boxes, error: boxesError} = useSWR(boxApiKey, () => {
        if (!open)
            return undefined;
        return ControllerBox.getActive()
    })

    useEffect(() => {
        setData(da ? da.data : undefined)
    }, [da])


    function save() {
        const idList = []
        _.forEach(data, (d, i) => {
            idList.push(d.id)
        })
        ControllerSite.Ads.setPriorities({data: idList}).then(res => {
            setState({
                ...state,
                save: false
            })
            onClose(true)
        })
    }

    function reqCancel(text) {
        enqueueSnackbar(text,
            {
                variant: "error",
                action: (key) => (
                    <Fragment>
                        <Button onClick={() => {
                            closeSnackbar(key)
                        }}>
                            {lang.get('close')}
                        </Button>
                    </Fragment>
                )
            });
    }

    function Output(){
    if (error) return (<ComponentError tryAgainFun={() => mutate(apiKey)}/>)
    if (data) return (<Box width={1} display={'flex'} flexWrap={'wrap'} py={2}>
        <Box px={1}>
            <DraggableList
                items={data}
                onItemsChange={(items) => {
                    setData(items);
                }}
                rootStyle={(dragging) => {
                    return {
                        backgroundColor: dragging ? null : grey[200],
                        ...UtilsStyle.transition(500)
                    }
                }}
                render={(item, props, {isDraggingItem, index, ...p}) => (
                    <Box key={item.id}
                         display={'flex'}
                         {...props}>
                        <Box display={'flex'} width={1} alignItems={'center'}
                             py={1}>
                            <Box px={2} py={1}>
                                {isDraggingItem ?
                                    <MenuOpen/> :
                                    <Menu/>
                                }
                            </Box>
                            <Box  p={1}>
                                <Img width={160} zoomable={true} minHeight={'unset'}
                                     src={item.media ? item.media.image : ""}
                                     style={{
                                         overflow:'hidden',
                                         ...UtilsStyle.borderRadius(5)
                                     }}/>
                            </Box>
                           <Box flex={1}>
                               <Typography flex={1} pr={2} pl={1} variant={'body1'}
                                           fontWeight={600}
                                           style={{
                                               justifyContent: 'start',
                                           }}>
                                   {index + 1}- {item.title[siteLang]}
                               </Typography>
                           </Box>
                            <Box px={1}>
                                <IconButton
                                    onClick={() => {
                                        const newList = data;
                                        _.remove(newList,(d)=>d.id === item.id)
                                        setData([...newList])
                                    }}>
                                    <DeleteOutline style={{color: red[400]}}/>
                                </IconButton>
                            </Box>
                        </Box>
                    </Box>
                )}
            />
        </Box>
        {/*<Box width={1 / 2} px={1}>*/}
        {/*    {!boxesError ?*/}
        {/*        boxes ?*/}
        {/*            <Box display={'flex'} flexDirection={'column'}>*/}
        {/*                <Box display={'flex'} pb={2}>*/}
        {/*                    <Box px={2}>*/}
        {/*                        <BaseButton*/}
        {/*                            variant={"outlined"}*/}
        {/*                            onClick={() => {*/}
        {/*                                setState({*/}
        {/*                                    ...state,*/}
        {/*                                    desktop: true*/}
        {/*                                })*/}
        {/*                            }}*/}
        {/*                            style={{*/}
        {/*                                borderColor: state.desktop ? cyan[300] : undefined*/}
        {/*                            }}>*/}
        {/*                            <ComputerOutlined/>*/}
        {/*                        </BaseButton>*/}
        {/*                    </Box>*/}
        {/*                    <Box px={2}>*/}
        {/*                        <BaseButton*/}
        {/*                            variant={"outlined"}*/}
        {/*                            onClick={() => {*/}
        {/*                                setState({*/}
        {/*                                    ...state,*/}
        {/*                                    desktop: false*/}
        {/*                                })*/}
        {/*                            }}*/}
        {/*                            style={{*/}
        {/*                                borderColor: !state.desktop ? cyan[300] : undefined*/}
        {/*                            }}>*/}
        {/*                            <PhoneAndroidOutlined/>*/}
        {/*                        </BaseButton>*/}
        {/*                    </Box>*/}
        {/*                </Box>*/}
        {/*                <Box width={1} display={'flex'} flexDirection={'column'}*/}
        {/*                     alignItems={'center'}>*/}
        {/*                    <Box display={'flex'} flexDirection={'column'} alignItems={'center'}*/}
        {/*                         justifyContent={'center'} width={state.desktop ? 1 : 0.7}>*/}
        {/*                        {boxes.map((b, index) => {*/}
        {/*                            const adsIndex = state.desktop ? ((index + 1) * 2) - 2 : index;*/}
        {/*                            const ads1 = data[adsIndex]*/}
        {/*                            const ads2 = data[adsIndex + 1]*/}
        {/*                            return (*/}
        {/*                                (data[adsIndex]) ?*/}
        {/*                                    <React.Fragment>*/}
        {/*                                        <Box display={'flex'} width={1}*/}
        {/*                                             flexDirection={'column'} py={1}>*/}
        {/*                                            <Typography variant={"body2"} pb={1}>*/}
        {/*                                                {b.name[siteLang]}*/}
        {/*                                            </Typography>*/}
        {/*                                            <Box display={'flex'}>*/}
        {/*                                                {*/}
        {/*                                                    (state.desktop ? [1, 2, 3, 4] : [1, 2]).map((key) => (*/}
        {/*                                                        <Box p={0.5}*/}
        {/*                                                             key={key}*/}
        {/*                                                             width={state.desktop ? 1 / 4 : 1 / 2}>*/}
        {/*                                                            <Skeleton variant={"rect"}*/}
        {/*                                                                      width={"100%"}*/}
        {/*                                                                      height={70}/>*/}
        {/*                                                        </Box>*/}

        {/*                                                    ))*/}
        {/*                                                }*/}
        {/*                                            </Box>*/}
        {/*                                        </Box>*/}
        {/*                                        <Box display={'flex'}>*/}
        {/*                                            <Box px={1} width={state.desktop ? 0.5 : 1}>*/}
        {/*                                                <Img width={'100%'} minHeight={"unset"}*/}
        {/*                                                     zoomable={true}*/}
        {/*                                                     src={state.desktop ?*/}
        {/*                                                         (ads1.media ? ads1.media.image : ""):*/}
        {/*                                                         (ads1.mobile_media ? ads1.mobile_media.image : "")}*/}
        {/*                                                     style={{*/}
        {/*                                                         overflow:'hidden',*/}
        {/*                                                         ...UtilsStyle.borderRadius(5)*/}
        {/*                                                     }}/>*/}
        {/*                                            </Box>*/}
        {/*                                            {(state.desktop && ads2) &&*/}
        {/*                                            <Box px={1} width={0.5}>*/}
        {/*                                                <Img width={'100%'} minHeight={"unset"}*/}
        {/*                                                     zoomable={true}*/}
        {/*                                                     src={ads2.media ? ads2.media.image : ""}*/}

        {/*                                                     style={{*/}
        {/*                                                         overflow:'hidden',*/}
        {/*                                                         ...UtilsStyle.borderRadius(5)*/}
        {/*                                                     }}/>*/}
        {/*                                            </Box>*/}
        {/*                                            }*/}
        {/*                                        </Box>*/}
        {/*                                    </React.Fragment> :*/}
        {/*                                    <React.Fragment/>*/}
        {/*                            )*/}
        {/*                        })}*/}
        {/*                    </Box>*/}
        {/*                </Box>*/}
        {/*            </Box>*/}
        {/*            :*/}
        {/*            <PleaseWait/>*/}
        {/*        :*/}
        {/*        <ComponentError tryAgainFun={() => mutate(boxApiKey)}/>*/}
        {/*    }*/}
        {/*</Box>*/}
        <SaveDialog
            open={state.save}
            text={(
                <Box display={'flex'} flexDirection={'column'}
                     px={3}>
                    <Typography variant={'h6'} fontWeight={400}
                                pb={2}>
                        ذخیره محصول
                    </Typography>
                    <Typography variant={'body1'}>
                        آیا از ذخیره این محصول اطمینان دارید؟
                    </Typography>
                    <Typography variant={'caption'}
                                color={theme.palette.text.secondary}
                                pt={0.5}>
                                <span style={{color: orange[300], fontWeight: 600, paddingLeft: theme.spacing(0.3)}}>
                                    توجه:
                                </span>
                        در صورت ذخیره، اطلاعات تغییریافته قابل
                        بازگشت
                        نمیباشد.
                    </Typography>
                </Box>)}
            onCancel={() => {
                setState({
                    ...state,
                    save: false
                })
            }}
            onSubmit={() => {
                save()
            }}/>
    </Box> )
    return <PleaseWait fullPage={true} />
    }
    return (
        <Dialog open={open}
                maxWidth={"md"}
                TransitionComponent={Transition}>
            <DialogTitle>مرتب‌سازی تبلیغات</DialogTitle>
            <DialogContent>
                <DialogContentText>
                    با درگ کردن تبلیغات آن ها را مرتب کنید
                </DialogContentText>
                <Output/>
            </DialogContent>
                <DialogActions
                    display={'flex'}>
                    <Box pl={2}>
                        <Button variant={"outlined"}
                                    onClick={() => {
                                        setState({
                                            ...state,
                                            save: true
                                        })
                                    }}>
                            ذخیره
                        </Button>
                    </Box>
                    <Box pl={2}>
                        <Button variant={"outlined"}
                                    onClick={() => {
                                        onClose()
                                    }}>
                            بستن
                        </Button>
                    </Box>
                </DialogActions>
        </Dialog>
    )


}
