import React, {useState} from "react";
import Box from "@material-ui/core/Box";
import {useTheme,Button} from "@material-ui/core";
import {cyan, green, orange} from "@material-ui/core/colors";
import {Item} from "../../box/BoxDashboard";
import SelectDialog, {createItem} from "../../../components/dialog/SelectDialog";
import {gcLog} from "../../../utils/ObjectUtils";
import rout from "../../../router";
import AdsGrid from "./AdsGrid";
import AdsSort from "./AdsSort";
import ButtonText from "../../../components/base/button/ButtonText";
import AdsEdit from "./AdsEdit";
import fa from "../../../language/fa";
import {mutate} from "swr"



export default function Ads({...props}) {
    const [openSort, setOpenSort] = useState(false)
    const [addDialog, setAddDialog] = useState(false)

    return (
        <Box width={1} p={2}>
            <Box display={'flex'} flexDirection={'row'}>
                <Button onClick={()=>{setOpenSort(s=>!s)}}>
                    مرتب‌سازی
                </Button>
                <Button onClick={()=>{setAddDialog(s=>!s)}}>
                    افزودن
                </Button>

            </Box>
            <AdsGrid/>
            <AdsEdit
                open={addDialog}
                item={undefined}
                onClose={() => {
                    setAddDialog(false)
                }}

            />
            <AdsSort
                open={openSort}
                editable={false}
                selectable={true}
                addable={true}
                onClose={() => {
                    setOpenSort(false)
                    mutate('ads-adsGrid')
                }}/>
        </Box>
    )
}

