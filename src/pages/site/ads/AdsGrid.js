import React, {useState} from "react";
import Box from "@material-ui/core/Box";
import useSWR from "swr/esm/use-swr";
import ControllerSite from "../../../controller/ControllerSite";
import {gcLog} from "../../../utils/ObjectUtils";
import BaseDialog from "../../../components/base/BaseDialog";
import ComponentError from "../../../components/base/ComponentError";
import {mutate} from "swr";
import PleaseWait from "../../../components/base/loading/PleaseWait";
import Img from "../../../components/base/img/Img";
import {siteLang, sLang, theme} from "../../../repository";
import {Add, ComputerOutlined, EditOutlined, PhoneAndroidOutlined, SelectAllOutlined} from "@material-ui/icons";
import Tooltip from "../../../components/base/Tooltip";
import {grey, red} from "@material-ui/core/colors";
import {UtilsStyle} from "../../../utils/Utils";
import Typography from "../../../components/base/Typography";
import Link from "../../../components/base/link/Link";
import ButtonBase from "@material-ui/core/ButtonBase";
import {Card} from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import BaseButton from "../../../components/base/button/BaseButton";
import _ from "lodash";
import AdsEdit from "./AdsEdit";

export default function AdsGrid({open, onClose, ...props}) {
    const [state, setState] = useState({
        add: false
    })
    const apiKey = `ads-adsGrid`
    const {data, error} = useSWR(apiKey, () => {
        return ControllerSite.Ads.get({type: "home", o: "-updated_at"})
    })

    if (error) return <ComponentError tryAgainFun={mutate(apiKey)}/>


    if (data)
        return (<Box display={'flex'} flexWrap={'wrap'}>
            {data.data.map(d =>
                <Item
                    editable={true}
                    selectable={true}
                    item={d}
                    onSelect={() => {
                        onClose(d)
                    }}
                    onChange={() => mutate(apiKey)}/>)}
        </Box>)

    return  <PleaseWait fullPage={true}/>

}


function Item({editable, selectable, item, onChange, onSelect}) {
    const [state, setState] = useState({
        edit: false
    })


    const cm = (
        <Box
            display={'flex'}
            flexDirection={'column'}
            component={Card}
            height={'100%'}
            onClick={() => {
                setState({
                    ...state,
                    edit: true
                })
            }}
            p={2}>
            <Box display={'flex'}>
                <Box pr={2}
                     style={{
                         position: 'relative',
                         ...UtilsStyle.borderRadius(5)
                     }}>
                    <Img
                        minHeight={1}
                        src={item.media ? item.media.image : ""}
                        zoomable={true}/>
                    <Box
                        width={35}
                        height={35}
                        display={'flex'}
                        alignItems={'center'}
                        justifyContent={'center'}
                        style={{
                            position: 'absolute',
                            backgroundColor: "rgba(224, 224, 224, 0.63)",
                            zIndex: 10,
                            left: '50%',
                            bottom: 0,
                            transform: 'translate(-50%, 0)',
                            ...UtilsStyle.borderRadius('50%')
                        }}>
                        <Tooltip title={"کامپیوتر"}>
                            <ComputerOutlined fontSize={"small"}/>
                        </Tooltip>
                    </Box>
                </Box>
                <Box pl={2}
                     style={{
                         position: 'relative',
                         ...UtilsStyle.borderRadius(5)
                     }}>
                    <Img
                        minHeight={1}
                        src={item.mobile_media ? item.mobile_media.image : ""}
                        zoomable={true}/>
                    <Box
                        width={35}
                        height={35}
                        alignItems={'center'}
                        justifyContent={'center'}
                        display={'flex'}
                        style={{
                            position: 'absolute',
                            backgroundColor: "rgba(224, 224, 224, 0.63)",
                            zIndex: 10,
                            left: '50%',
                            bottom: 0,
                            transform: 'translate(-50%, 0)',
                            ...UtilsStyle.borderRadius('50%')
                        }}>
                        <Tooltip title={"موبایل"}>
                            <PhoneAndroidOutlined fontSize={"small"}/>
                        </Tooltip>
                    </Box>
                </Box>
            </Box>
            {item.title &&
            <Typography pt={2} variant={"h6"}>
                {item.title[siteLang]}
            </Typography>
            }
            {
                item.url &&
                <Link
                    toHref={item.url}
                    target={"_blank"}
                    style={{
                        marginTop: theme.spacing(1)
                    }}>
                    لینک
                </Link>
            }
        </Box>
    )

    return (
        <Box key={item.id} width={1 / 3} p={1} display={'flex'} flexDirection={'column'}>
            {(!editable && selectable) ?
                <ButtonBase
                    onClick={onSelect}
                    style={{
                        padding: 0
                    }}>
                    {cm}
                </ButtonBase> :
                cm}
            <AdsEdit
                open={state.edit}
                item={item}
                onClose={(newItem) => {
                    setState({
                        ...state,
                        edit: false
                    })
                    if (!newItem)
                        return
                    onChange()
                }}/>
        </Box>
    )
}
