import React, {Fragment, useEffect, useRef, useState} from "react";
import BaseDialog from "../../../components/base/BaseDialog";
import Box from "@material-ui/core/Box";
import Typography from "../../../components/base/Typography";
import UploadItem from "../../../components/base/uploader/UploadItem";
import {createMultiLanguage} from "../../../controller/converter";
import {serverFileTypes} from "../../../controller/type";
import FormController from "../../../components/base/formController/FormController";
import DefaultTextField from "../../../components/base/textField/DefaultTextField";
import DefaultTextFieldMultiLanguage from "../../../components/base/textField/DefaultTextFieldMultiLanguage";
import SuccessButton from "../../../components/base/button/buttonVariant/SuccessButton";
import BaseButton from "../../../components/base/button/BaseButton";
import {lang, siteLang, theme} from "../../../repository";
import ControllerSite from "../../../controller/ControllerSite";
import {Button, Checkbox,FormControlLabel} from "@material-ui/core";
import {useSnackbar} from "notistack";
import _ from 'lodash'
import {gcLog} from "../../../utils/ObjectUtils";

export default function AdsEdit({open, type = "home", item: it, onClose, ...props}) {
    const ref = useRef();
    const [state, setState] = useState({
        loading: false
    });
    const [item, setItem] = useState(undefined)

    const [checked, setChecked] = useState()
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();

    function getSize() {
        if (checked==="small")
            return 4
        if (checked==="medium")
            return 2
        if (checked==="large")
            return 1
    }

    function getItemSize(size) {
        if (size===4)
            return "small"
        if (size===2)
            return "medium"
        if (size===1)
            return "large"
    }

    useEffect(() => {
        setItem(it || {
            id: undefined,
            title: createMultiLanguage(),
            media: undefined,
            mobile_media: undefined,
            url: "",
        })

        setChecked(getItemSize(it?.settings?.size))

    }, [it, open])

    function save() {
        try {
            setState({
                ...state,
                loading: true
            })
            const {title, url} = ref.current.serialize();

            if (!title || _.isEmpty(title[siteLang]))
                throw "title is empty"

            const props = {
                id: item.id,
                type: type,
                media_id: item.media.id ,
                mobile_media_id: item.mobile_media.id ,
                title: title,
                url: url,
                settings:{size:getSize()}
            }

            ControllerSite.Ads.set(props).then(res => {
                setState({
                    ...state,
                    loading: false
                })
                onClose(res)
            }).catch(() => setState({
                ...state,
                loading: false
            }))
        } catch (e) {
            reqCancel("لطفا تمام فیلد هارا پرکنید")
            setState({
                ...state,
                loading: false
            })
        }
    }

    function reqCancel(text) {
        enqueueSnackbar(text,
            {
                variant: "error",
                action: (key) => (
                    <Fragment>
                        <Button onClick={() => {
                            closeSnackbar(key)
                        }}>
                            {lang.get('close')}
                        </Button>
                    </Fragment>
                )
            });
    }


    function handleChange(event) {
        setChecked(event.target.name)
    }

    return (
        <BaseDialog
            open={open}
            maxWidth={"xl"}
            onClose={() => {
            }}>
            {
                item &&
                <FormController
                    name={"edit-ads"} innerref={ref}
                    display={'flex'} p={2} flexDirection={'column'}
                    onSubmit={() => {
                        alert("onSubmit")
                    }}>
                    <Box p={2} display={'flex'}>
                        <Box
                            width={0.5}
                            display={'flex'}
                            flexDirection={'column'}
                            alignItems={'center'}>
                            <UploadItem
                                holderWidth={'50%'}
                                holderHeight={'auto'}
                                multiSelect={false}
                                src={item.media ? item.media.image : ""}
                                width={undefined}
                                height={undefined}
                                type={serverFileTypes.Image.Ads.type}
                                onSelected={(file) => {
                                    setItem({
                                        ...item,
                                        media: file
                                    })
                                }}/>
                            <Typography pt={2} variant={'h6'}>
                                عکس دسکتاپ
                            </Typography>
                        </Box>
                        <Box
                            width={0.5}
                            display={'flex'}
                            flexDirection={'column'}
                            alignItems={'center'}>
                            <UploadItem
                                holderWidth={'50%'}
                                holderHeight={'auto'}
                                multiSelect={false}
                                src={item.mobile_media ? item.mobile_media.image : ""}
                                width={undefined}
                                height={undefined}
                                type={serverFileTypes.Image.AdsMobile.type}
                                onSelected={(file) => {
                                    setItem({
                                        ...item,
                                        mobile_media: file
                                    })
                                }}/>
                            <Typography pt={2} variant={'h6'}>
                                عکس موبایل
                            </Typography>
                        </Box>
                    </Box>
                    <Box>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    checked={checked==="small"}
                                    onChange={handleChange}
                                    name={"small"}
                                    inputProps={{ 'aria-label': 'controlled' }}
                                />
                            }
                            label="کوچک"
                        /><FormControlLabel
                        control={
                            <Checkbox
                                checked={checked==="medium"}
                                onChange={handleChange}
                                name={"medium"}
                                inputProps={{ 'aria-label': 'controlled' }}
                            />
                        }
                        label="متوسط"
                    /><FormControlLabel
                        control={
                            <Checkbox
                                checked={checked==="large"}
                                onChange={handleChange}
                                value={4}
                                name={"large"}
                                inputProps={{ 'aria-label': 'controlled' }}
                            />
                        }
                        label="بزرگ"
                    />
                    </Box>
                    <Box display={'flex'}>
                        <Box p={2} width={0.5}>
                            <DefaultTextFieldMultiLanguage
                                name={"title"}
                                defaultValue={item.title}
                                label={"نام"}/>
                        </Box>
                        <Box p={2} width={0.5} display={"flex"}>
                            <DefaultTextField
                                name={"url"}
                                defaultValue={item.url}
                                label={"لینک"}/>
                        </Box>
                    </Box>
                    <Box pt={2} display={'flex'}>
                        <Box px={2}>
                            <SuccessButton
                                variant={"outlined"}
                                onClick={save}
                                loading={state.loading}>
                                ذخیره
                            </SuccessButton>
                        </Box>
                        <Box px={2}>
                            <BaseButton
                                variant={"outlined"}
                                disabled={state.loading}
                                onClick={() => {
                                    onClose()
                                }}
                                style={{
                                    borderColor: theme.palette.error.main
                                }}>
                                بستن
                            </BaseButton>
                        </Box>
                    </Box>
                </FormController>
            }
        </BaseDialog>
    )
}
