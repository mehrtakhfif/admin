import React, {useState} from "react";
import _ from "lodash"
import {gcLog} from "../utils/ObjectUtils";



export default function Test3 () {
    gcLog("Father")

    const [one, setOne] = useState(1000)
    const [two, setTwo] = useState(1000)

    const handleChange = () => {
        const val = one + 1
        setOne(val)
    }

    const handleChange2 = () => {
        const val = two + 1
        setTwo(val)
    }


    return (
        <div>
            <div>
                <button onClick={handleChange}>
                    change 1
                </button>
                <button onClick={handleChange2}>
                    change 2
                </button>
            </div>
            <Co d={one}/>
            <Co2 d={two}/>
        </div>
    )
}
Test3.whyDidYouRender = true

function Co(props) {

    gcLog("one")
    const [one, setOne] = useState(1000)

    const handleChange = () => {
        const val = one + 1
        setOne(val)
    }
    Co.whyDidYouRender = true

    return (
        <div>
        <h1>{props.d}</h1>
            <button onClick={handleChange}>
                change 1
            </button>
        </div>
    )
}

function Co2(props) {
    gcLog("two")


    return (
        <h1>{props.d}</h1>
    )
}
