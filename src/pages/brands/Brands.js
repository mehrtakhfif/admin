import React, {Fragment, useEffect, useRef, useState} from "react";
import Box from "@material-ui/core/Box";
import Table, {headerItemTemplate} from "../../components/base/table/Table";
import {createFilter} from "../../components/base/table/Filters";
import DataUtils, {filterType} from "../../utils/DataUtils";
import {useSelector} from "react-redux";
import storage from "../../storage";
import ControllerProduct from "../../controller/ControllerProduct";
import _ from "lodash";
import {ApiHelper, createMultiLanguage} from "../../controller/converter";
import useSWR, {mutate} from "swr";
import Tooltip from "@material-ui/core/Tooltip";
import {activeLang, lang, siteLang, theme} from "../../repository";
import IconButton from "@material-ui/core/IconButton";
import Link from "../../components/base/link/Link";
import {EditOutlined} from "@material-ui/icons";
import ComponentError from "../../components/base/ComponentError";
import {Dialog} from "@material-ui/core";
import SuccessButton from "../../components/base/button/buttonVariant/SuccessButton";
import BaseButton from "../../components/base/button/BaseButton";
import FormControl from "../../components/base/formController/FormController";
import TextFieldMultiLanguageContainer from "../../components/base/textField/TextFieldMultiLanguageContainer";
import {errorList, notValidErrorTextField} from "../../components/base/textField/TextFieldContainer";
import PermalinkTextField from "../../components/base/textField/PermalinkTextField";
import SaveDialog from "../../components/dialog/SaveDialog";
import {orange} from "@material-ui/core/colors";
import Typography from "../../components/base/Typography";
import TextField from "../../components/base/textField/TextField";
import Button from "@material-ui/core/Button";
import {useSnackbar} from "notistack";


const headers = [
    ...headerItemTemplate.idActionName(),
    ...headerItemTemplate.createByUpdatedBy(),
];
const filters = [
    createFilter({id: 'name__fa', type: filterType.text, label: 'نام', value: ''}),
];
const CookieKey = "brands";

export default function ({...props}) {
    //region var
    const {boxes} = useSelector(state => state.user);
    //region state
    const [state, setState] = useState({
        filters: _.cloneDeep(filters),
        order: storage.PageSetting.getOrder(CookieKey),
        pagination: {
            rowPerPage: storage.PageSetting.getRowPerPage(CookieKey),
            page: 0
        }
    });
    const [filtersState, setFiltersState] = useState({
        data: filters,
        resetFilters: () => {
            setFiltersState({
                ...filtersState,
                data: DataUtils().TableFilter().resetFilter({filters: filtersState.data}),
            })
        }
    });
    //create and create brand
    const [brand, setBrand] = useState({
        open: false,
        item: {}
    });

    //endregion state
    //endregion var

    //region useEffect
    useEffect(() => {
        storage.PageSetting.setPage(CookieKey, {page: state.pagination.page});
    }, [state.pagination.page]);
    //endregion useEffect

    //region requests

    const d = ControllerProduct.Brands.get();
    const apiKey = ApiHelper.createSWRKey({
        api: d[0],
        order: state.order,
        filters: state.filters,
        page: state.pagination.page,
        step: state.pagination.rowPerPage
    });
    const {data, error} = useSWR(apiKey, () => {
        return d[1]({
            page: state.pagination.page + 1,
            step: state.pagination.rowPerPage,
            order: state.order,
            filters: state.filters,
        })
    });

    useEffect(() => {
        requestFilters()
    }, [])

    function requestFilters() {
        ControllerProduct.Brands.Filters.get({}).then((res) => {
            const newList = _.isArray(filtersState.data) && !_.isEmpty(filtersState.data) ? filtersState.data : filters;
            _.forEach(res.data, (fi) => {
                const data = createFilter({id: fi.name, type: fi.type, label: fi.label, data: fi.filters, value: null});
                switch (fi.name) {
                    case 'category': {
                        data.value = [];
                        break;
                    }
                    default: {
                    }
                }

                const index = _.findIndex(newList, (l) => l.id === data.id);
                newList[index !== -1 ? index : newList.length] = data;
            });
            setFiltersState({
                ...filtersState,
                data: [...newList]
            })
        }).catch((e) => {
            setFiltersState({
                ...filtersState,
                data: false
            })
        });
    }

    //endregion requests

    //region handler
    function handleFilterChange({item, value}) {
        setState({
            ...state,
            pagination: {
                ...state.pagination,
                page: 0
            }
        })
        setFiltersState({
            ...filtersState,
            data: DataUtils().TableFilter().filterItemChange({filters: filtersState.data, item: item, value: value})
        });
    }

    function handleApplyFilter() {
        setState({
            ...state,
            filters: _.cloneDeep(filtersState.data)
        })
    }

    function handleResetFilter() {
        filtersState.resetFilters()
    }

    function handleChangeOrder(order) {
        storage.PageSetting.setOrder(CookieKey, {order: order});
        setState({
            ...state,
            order: {
                ...order
            }
        });
    }

    function handleEditBrand(b) {
        setBrand({
            ...brand,
            open: true,
            item: b ? b : {}
        });
    }

    //endregion handler

    const pg = state.pagination;

    const tableData = (data && data.data ? data.data : [...Array(state.pagination.rowPerPage)]).map((fe, i) => (
        fe ? {
            id: {
                label: fe.id,
            },
            actions: {
                label: <Box display={'flex'} alignItems={'center'} pl={1}>
                    <Tooltip title={lang.get('edit')}>
                        <IconButton size={"small"}
                                    onClick={(e) => {
                                        handleEditBrand(fe);
                                        e.stopPropagation();
                                    }}
                                    style={{
                                        marginRight: theme.spacing(0.5),
                                        marginLeft: theme.spacing(0.5)
                                    }}>
                            <Link linkStyle={{display: 'flex'}}
                                  hoverColor={theme.palette.primary.main}>
                                <EditOutlined style={{fontSize: 20, padding: 2}}/>
                            </Link>
                        </IconButton>
                    </Tooltip>
                </Box>
            },
            name: {
                label: fe.name[activeLang],
                onClick: () => {
                    handleEditBrand(fe)
                }
            },
            created_at: {
                label: fe.createdAt.date,
            },
            created_by: {
                label: fe.createdAt.by.name,
                link: fe.createdAt.by.link,
            },
            updated_at: {
                label: fe.updatedAt.date,
            },
            updated_by: {
                label: fe.updatedAt.by.name,
                link: fe.updatedAt.by.link,
            },
        } : {}
    ));


    return (
        <Box my={2} px={2}>
            {(!error) ?
                <React.Fragment>
                    <Table headers={headers}
                           cookieKey={CookieKey}
                           title={lang.get("brands")}
                           showBox={false}
                           data={tableData}
                           filters={filtersState.data}
                           onFilterChange={handleFilterChange}
                           loading={false}
                           activePage={pg.page}
                           rowsPerPage={pg.rowPerPage}
                           lastPage={data ? data.pagination.lastPage : 0}
                           rowCount={data ? data.pagination.count : 0}
                           orderType={state.order}
                           addNewButton={{
                               label: lang.get('add_new_brand'),
                               onClick: () => handleEditBrand()
                           }}
                           onActivePageChange={(page) => {
                               setState({
                                   ...state,
                                   pagination: {
                                       ...state.pagination,
                                       page: page
                                   }
                               })
                               // requestData({page: e})
                           }}
                           onRowPerPageChange={(rowPerPage) => {
                               storage.PageSetting.setRowPerPage(CookieKey, {rowPerPage: rowPerPage});
                               setState({
                                   ...state,
                                   pagination: {
                                       ...state.pagination,
                                       rowPerPage: rowPerPage
                                   }
                               })
                           }}
                           onApplyFilterClick={handleApplyFilter}
                           onResetFilterClick={handleResetFilter}
                           onChangeOrder={handleChangeOrder}/>
                    <EditBrand open={brand.open}
                               item={brand.item}
                               onClose={(brand) => {
                                   if (brand)
                                       mutate(apiKey);
                                   setBrand({
                                       ...state,
                                       open: false
                                   })
                               }}/>
                </React.Fragment> :
                <Box>
                    <ComponentError
                        statusCode={(_.isObject(error) && error.request) ? error.request.status : undefined}
                        tryAgainFun={() => mutate(apiKey)}/>
                </Box>}
        </Box>
    )
}


export function EditBrand({open, item, onClose}) {
    const ref = useRef();
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();

    const [state, setState] = useState({
        loading: false,
        saveDialog: false,
    });
    const [nameRef, setNameRef] = useState();
    const [data, setData] = useState({
        name: (item && item.name) ? item.name : createMultiLanguage({}),
        permalink: (item && item.permalink) ? item.permalink : ''
    });


    useEffect(() => {
        if (!item)
            return;
        setData({
            name: (item && item.name) ? item.name : createMultiLanguage({}),
            permalink: (item && item.permalink) ? item.permalink : ''
        })
    }, [item]);


    function handleSaveBrand() {
        if (state.loading)
            return;
        if (ref.current.hasError()) {
            let errorText = "فرم مشکل دارد";
            try {
                const el = ref.current.getErrorElement();
                const t = el.getAttribute(notValidErrorTextField);
                if (t)
                    errorText = t;
                el.focus();
            } catch (e) {
            }
            reqCancel(errorText);
            return;
        }
        setState({
            ...state,
            loading: true,
            saveDialog: false
        });
        const {name, permalink, ...fp} = ref.current.serialize();


        ControllerProduct.Brands.update({
            id: item ? item.id : undefined,
            permalink: permalink,
            name: name,
        }).then(res => {
            onClose(res.data);
            setState({
                ...state,
                loading: false,
                saveDialog: false
            });

        }).catch(() => {
            reqCancel("در ذخیره برند مشکلی پیش آمد.");
            setState({
                ...state,
                loading: false,
                saveDialog: false

            });
        })
    }

    function reqCancel(text = null) {
        if (text)
            enqueueSnackbar(text,
                {
                    variant: "error",
                    action: (key) => (
                        <Fragment>
                            <Button onClick={() => {
                                closeSnackbar(key)
                            }}>
                                {lang.get('close')}
                            </Button>
                        </Fragment>
                    )
                });

        setState({
            loading: false
        })

    }


    return (
        <Dialog open={open} onClose={null}>
            <Box py={3} px={2} display={'flex'} flexDirection={'column'} minWidth={400}>
                <Typography variant={'h6'} fontWeight={600} pb={1.5}>
                    {item ? "بروزرسانی برند" : "افزودن برند جدید"}
                </Typography>
                <FormControl minWidth={500} pt={2} px={2} innerref={ref}>
                    <TextFieldMultiLanguageContainer
                        name={"name"}
                        render={(ref, {lang: inputLang, name: inputName, initialize, valid, errorIndex, setValue, inputProps, style, props}) => {
                            if (inputLang.key === "fa") {
                                setNameRef(ref)
                            }
                            return (
                                <TextField
                                    {...props}
                                    error={!valid}
                                    variant="outlined"
                                    name={inputName}
                                    defaultValue={data.name[inputLang.key]}
                                    fullWidth
                                    helperText={errorList[errorIndex]}
                                    inputRef={ref}
                                    required={inputLang.key === siteLang}
                                    label={"نام"}
                                    style={style}
                                    inputProps={{
                                        ...inputProps
                                    }}/>
                            )
                        }}/>
                    <Box pt={2}>
                        <PermalinkTextField
                            name={"permalink"}
                            syncedRef={nameRef}
                            defaultValue={data.permalink}
                            inputProps={{
                                fullWidth: true
                            }}/>
                    </Box>
                    <Box pt={2} display={'flex'}>
                        <SuccessButton loading={state.loading} onClick={() => {
                            setState({
                                ...state,
                                saveDialog: true
                            })
                        }}>
                            {lang.get((item && item.id) ? "save" : "add")}
                        </SuccessButton>
                        {open &&
                        <Box pr={1}>
                            <BaseButton disabled={state.loading} variant={"text"} onClick={() => onClose()}>
                                {lang.get("close")}
                            </BaseButton>
                        </Box>
                        }
                    </Box>
                    <SaveDialog
                        text={(
                            <Box display={'flex'} flexDirection={'column'} px={3}>
                                <Typography variant={'h6'} fontWeight={400} pb={2}>
                                    ذخیره تگ
                                </Typography>
                                <Typography variant={'body1'}>
                                    آیا از ذخیره این تگ اطمینان دارید؟
                                </Typography>
                                <Typography variant={'caption'} color={theme.palette.text.secondary} pt={0.5}>
                                <span style={{color: orange[300], fontWeight: 600, paddingLeft: theme.spacing(0.3)}}>
                                    توجه:
                                </span>
                                    در صورت ذخیره، اطلاعات تغییریافته قابل بازگشت نمیباشد.
                                </Typography>
                            </Box>
                        )}
                        open={state.saveDialog}
                        onCancel={() => {
                            setState({
                                ...state,
                                saveDialog: false
                            })
                        }}
                        onSubmit={() => {
                            setState({
                                ...state,
                                saveDialog: false
                            });
                            handleSaveBrand()
                        }}/>
                </FormControl>
            </Box>
        </Dialog>
    )
}
