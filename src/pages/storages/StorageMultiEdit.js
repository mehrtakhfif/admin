import React, {Fragment, useEffect, useRef, useState} from "react";
import Box from "@material-ui/core/Box";
import rout from "../../router";
import {Redirect} from "react-router-dom";
import ControllerProduct from "../../controller/ControllerProduct";
import ComponentError from "../../components/base/ComponentError";
import PleaseWait from "../../components/base/loading/PleaseWait";
import _ from "lodash";
import {gcLog} from "../../utils/ObjectUtils";
import Button from "@material-ui/core/Button";
import {lang, siteLang, theme} from "../../repository";
import {useSnackbar} from "notistack";
import {Pricing} from "./StorageSingle";
import StickyBox from "react-sticky-box";
import {Card} from "@material-ui/core";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Switch from "@material-ui/core/Switch";
import Typography from "../../components/base/Typography";
import BaseButton from "../../components/base/button/BaseButton";
import {cyan} from "@material-ui/core/colors";
import SuccessButton from "../../components/base/button/buttonVariant/SuccessButton";
import FormControl from "../../components/base/formController/FormController";
import Backdrop from "@material-ui/core/Backdrop";
import CircularProgress from "@material-ui/core/CircularProgress";


export default function ({...props}) {
    const pId = props.match.params.pId;
    const type = props.match.params.type || "pricing";
    const ref = useRef();
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();

    const [redirect, setRedirect] = useState()
    const [product, setProduct] = useState()
    const [baseStorage, setBaseStorage] = useState()
    const [loading, setLoading] = useState(false)


    useEffect(() => {
        if (!pId) {
            setRedirect(rout.Storage.rout)
            return;
        }
        getData()
    }, [])


    function getData() {
        setProduct(undefined)
        ControllerProduct.Storage.get({pId}).then(res => {
            const r = canEditMultiStorage(res.storages)
            if (!r.can) {
                reqCancel(r.message);
                const ro = r.storageId ?
                    rout.Storage.Single.Edit.editStorage(r.storageId, {pricing: true}) :
                    rout.Storage.Single.showStorageDashboard({productId: pId})
                setRedirect(ro)
                return;
            }

            setProduct(res)
            setBaseStorage(r.baseStorage)
        }).catch(() => {
            setProduct(false)
        })
    }


    function handleSave() {
        setLoading(true)

        const {pricing} = ref.current.serialize()

        const {discountPrice, finalPrice, startPrice} = pricing;
        const list = [];
        _.forEach(product.storages, (s) => {
            if (s.storageId === baseStorage.storageId)
                return
            list.push(s)
        });
        saveMultiSave({
            storageId: baseStorage.storageId,
            discountPrice: discountPrice,
            finalPrice: finalPrice,
            startPrice: startPrice,
            storages:list
        }).then(res => {
                const list = [];
                _.forEach(product.storages, (s) => {
                    if (s.storageId === baseStorage.storageId)
                        return
                    list.push(s)
                });
                queueUpdateStorage(list, {
                    startPrice: startPrice,
                    discountPrice: discountPrice,
                    finalPrice: finalPrice,
                });
            }).catch(() => {
                reqCancel()
            })


        // const {discountPrice, finalPrice, startPrice} = (() => {
        //     const {discountPrice, finalPrice, startPrice} = pricing
        //     return {
        //         discountPrice: _.toNumber(discountPrice),
        //         finalPrice: _.toNumber(finalPrice),
        //         startPrice: _.toNumber(startPrice),
        //     }
        // })();
        // if (startPrice > discountPrice || startPrice > finalPrice || discountPrice > finalPrice) {
        //     reqCancel("قیمت گذاری اشتباه است.")
        //     return
        // }
        //
        // ControllerProduct.Storage.save({
        //     pId: pId,
        //     sId: baseStorage.storageId,
        //     startPrice: startPrice,
        //     discountPrice: discountPrice,
        //     finalPrice: finalPrice,
        // }).then(res => {
        //     const list = [];
        //     _.forEach(product.storages, (s) => {
        //         if (s.storageId === baseStorage.storageId)
        //             return
        //         list.push(s)
        //     });
        //     queueUpdateStorage(list, {
        //         startPrice: startPrice,
        //         discountPrice: discountPrice,
        //         finalPrice: finalPrice,
        //     });
        // }).catch(() => {
        //     reqCancel()
        // })
    }

    function queueUpdateStorage(list, newValue) {
        if (_.isEmpty(list)) {
            setRedirect(rout.Storage.Single.showStorageDashboard({productId: pId}))
            return
        }
        const {startPrice, discountPrice, finalPrice} = newValue
        const newList = _.cloneDeep(list);
        const storage = list[0];
        _.remove(newList, (s) => {
            return (s.storageId === storage.storageId)
        })
        const ratio = _.toNumber(storage.settings.ratio);
        const ratio_discount = _.toNumber(storage.settings.ratio_discount || 0);
        ControllerProduct.Storage.save({
            pId: pId,
            sId: storage.storageId,
            startPrice: startPrice * ratio,
            discountPrice: (discountPrice * ratio) - ratio_discount,
            finalPrice: finalPrice * ratio,
        }).finally(() => {
            queueUpdateStorage(newList, newValue)
        })
    }

    function reqCancel(text = null, warning) {
        if (text)
            enqueueSnackbar(text,
                {
                    variant: warning ? "warning" : "error",
                    action: (key) => (
                        <Fragment>
                            <Button onClick={() => {
                                closeSnackbar(key)
                            }}>
                                {lang.get('close')}
                            </Button>
                        </Fragment>
                    )
                });
        setLoading(false)
    }


    return (
        <React.Fragment>
            {!redirect ?
                !(_.isUndefined(product) || _.isUndefined(baseStorage)) ?
                    product ?
                        <Box display={'flex'}>
                            <FormControl name={"storage-multi-edit"} innerref={ref} width={'80%'} display={'flex'}
                                         py={3} px={2}>
                                <Box width={1} display={'flex'} flexDirection={"column"}>
                                    <Typography pr={1} pt={1} variant={"h5"} fontWeight={500}>
                                        {baseStorage.storageTitle[siteLang]}
                                    </Typography>
                                    <Pricing
                                        open={true}
                                        box={product.product.box}
                                        item={{
                                            startPrice: baseStorage.startPrice,
                                            finalPrice: baseStorage.finalPrice,
                                            discountPrice: baseStorage.discountPrice,
                                            shipping_cost: baseStorage.shipping_cost,
                                            tax: baseStorage.tax,
                                        }}
                                        shippingCostShow={false}
                                        taxShow={false}
                                        ratioShow={false}/>
                                </Box>
                            </FormControl>
                            <Box width={'20%'} pr={2}>
                                <StickyBox offsetTop={150} offsetBottom={20}>
                                    <Box display={'flex'} component={Card} py={2} width={1} flexDirection={'column'}>
                                        <Box display={'flex'} alignItems={'center'}
                                             justifyContent={'center'} mt={1}>
                                            <SuccessButton
                                                disabled={loading}
                                                onClick={handleSave}>
                                                ذخیره
                                            </SuccessButton>
                                            <BaseButton
                                                disabled={loading}
                                                onClick={() => setRedirect(rout.Storage.Single.showStorageDashboard({productId: pId}))}
                                                variant={"text"}
                                                style={{marginRight: theme.spacing(1)}}>
                                                {lang.get('close')}
                                            </BaseButton>
                                        </Box>
                                    </Box>
                                </StickyBox>
                            </Box>
                        </Box>
                        :
                        <ComponentError tryAgainFun={getData}/> :
                    <PleaseWait/>
                :
                <Redirect
                    to={{
                        pathname: redirect || rout.Storage.rout,
                    }}
                />}
            {
                loading &&
                <Backdrop open={loading}
                          style={{
                              zIndex: '9999'
                          }}>
                    <CircularProgress color="inherit"
                                      style={{
                                          color: cyan[300],
                                          width: 100,
                                          height: 100
                                      }}/>
                </Backdrop>
            }
        </React.Fragment>
    )
}


export function canEditMultiStorage(storages) {

    if (storages.length <= 1) {
        return {
            can: false,
            message: "حداقل 2 انبار نیاز است."
        }
    }
    let noHaveRatio = false;
    let baseStorage = undefined
    _.forEach(storages, f => {
        if (!f.settings.ratio || f.settings.ratio === "") {
            noHaveRatio = f
            return false
        }
        if (f.settings.ratio === 1) {
            baseStorage = f
        }
    })

    if (noHaveRatio) {
        return {
            can: false,
            message: "انبار " + noHaveRatio.storageTitle[siteLang] + " دارای ضریب نمیباشد.",
            storageId: noHaveRatio.storageId
        };
    }

    if (!baseStorage) {
        return {
            can: false,
            message: "ابتدا یک انبار با ضریب یک انتخاب کنید.",
        }
    }
    return {
        can: true,
        message: undefined,
        baseStorage: baseStorage
    }
}

export async function saveMultiSave({
                                        storageId,
                                        unavailable,
                                        discountPrice,
                                        finalPrice,
                                        startPrice,
                                        storages,
                                    }) {
    discountPrice = _.toNumber(discountPrice);
    finalPrice = _.toNumber(finalPrice);
    startPrice = _.toNumber(startPrice);

    if (startPrice > discountPrice || startPrice > finalPrice || discountPrice > finalPrice) {
        return {
            status: 500,
            message: "قیمت گذاری اشتباه است."
        }
    }

    return await ControllerProduct.Storage.save({
        sId: storageId,
        unavailable: unavailable,
        startPrice: startPrice,
        discountPrice: discountPrice,
        finalPrice: finalPrice,
    }).then(res => {
        const list = [];
        _.forEach(storages, (s) => {
            if (s.storageId === storageId)
                return
            list.push(s)
        });
        return queueUpdateStorage(list, {
            startPrice: startPrice,
            discountPrice: discountPrice,
            finalPrice: finalPrice,
        });
    }).catch(() => {
        return {
            status: 500,
            message: ""
        }
    })
}


async function queueUpdateStorage(list, newValue) {
    if (_.isEmpty(list)) {
        return {
            status: 200,
            message: ""
        }
    }
    const {startPrice, discountPrice, finalPrice} = newValue
    const newList = _.cloneDeep(list);
    const storage = list[0];
    _.remove(newList, (s) => {
        return (s.storageId === storage.storageId)
    })
    const ratio = _.toNumber(storage.settings.ratio);
    const ratio_discount = _.toNumber(storage.settings.ratio_discount || 0);
    await ControllerProduct.Storage.save({
        sId: storage.storageId,
        startPrice: startPrice * ratio,
        discountPrice: (discountPrice * ratio) - ratio_discount,
        finalPrice: finalPrice * ratio,
    }).finally(() => {
    })
    await queueUpdateStorage(newList, newValue)
}
