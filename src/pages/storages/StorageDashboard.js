import React, {Fragment, useCallback, useEffect, useState} from "react";
import Box from "@material-ui/core/Box";
import ControllerProduct from "../../controller/ControllerProduct";
import {DEBUG, lang, siteLang} from "../../repository";
import PleaseWait from "../../components/base/loading/PleaseWait";
import Typography from "../../components/base/Typography";
import Link from "../../components/base/link/Link";
import {UtilsFormat, UtilsStyle} from "../../utils/Utils";
import {Card, Radio, useTheme,AppBar,Dialog,List,Toolbar,Slide} from "@material-ui/core";
import {cyan, green, grey, orange} from "@material-ui/core/colors";
import Divider from "@material-ui/core/Divider";
import ComponentError from "../../components/base/ComponentError";
import red from "@material-ui/core/colors/red";
import Timer from "../../components/Timer";
import yellow from "@material-ui/core/colors/yellow";
import _ from 'lodash';
import {useSnackbar} from "notistack";
import Button from "@material-ui/core/Button";
import BaseButton from "../../components/base/button/BaseButton";
import {AttachMoney, Close, Edit, ExtensionOutlined, FileCopyOutlined, Menu, MenuOpen, Receipt, Settings } from "@material-ui/icons";
import DraggableList from "../../components/base/draggableList/DraggableList";
import Switch from "@material-ui/core/Switch";
import Info, {createInfoItem} from "../../components/base/info/Info";
import SuccessButton from "../../components/base/button/buttonVariant/SuccessButton";
import Tooltip from "../../components/base/Tooltip";
import IconButton from "@material-ui/core/IconButton";
import brown from "@material-ui/core/colors/brown";
import SaveDialog from "../../components/dialog/SaveDialog";
import Backdrop from "@material-ui/core/Backdrop/Backdrop";
import {gcLog} from "../../utils/ObjectUtils";
import rout from "../../router";
import MaterialIcon from '@material-ui/core/Icon'
import ButtonLink from "../../components/base/link/ButtonLink";
import Icon from "@material-ui/core/Icon";
import RightClickMenu, {MenuItem} from "../../components/base/rightClick/RightClickMenu";
import useSWR from "swr/esm/use-swr";
import CircularProgress from "@material-ui/core/CircularProgress";
import {Redirect} from "react-router-dom";
import {bookingType} from "../../controller/type";
import Img from "../../components/base/img/Img";
import StorageBase from "./StorageSingle";

const defInfo = [
    createInfoItem({text: 'انبار پیشفرض: انباری که قیمت و زمان پایان آن در صفحه محصول با اولویت اصلی انتخاب میشود و بشکل پیشفرض فعال میباشد.'}),
    createInfoItem({text: 'انبار زرد رنگ: انبار پیشفرض.'}),
];
const info = [
    createInfoItem({
        text: 'عملیات انبار(انبارپیشفرض ، ویرایش و...): روی انبار مورد نظر دوبار کلیک کنید یا روی آیکون چرخدنده کنار کارت کلیک کنید.',
        importance: 'h'
    }),
    ...defInfo,
    createInfoItem({text: 'انبار آبی رنگ: انباری که در انتظار پیشفرض شدن میباشد.'}),
];
const sortInfo = [
    ...defInfo,
    createInfoItem({text: 'در حالت موبایل و تبلت 4 آیتم اول در دید کاربر میباشد و بقیه آیتم ها بعد از کلیک برروی "بیشتر" نمیاش داده میشود.'}),
    createInfoItem({text: 'انبار آبی رنگ: انباری که زیر انبار پیشرفض در صفحه موبایل و تبلت در دید اول نمایش داده میشود.'}),
];


export default function StorageDashboard({productId,dialogeMode=false,...props}) {
    const [redirect, setRedirect] = useState()
    const [error, setError] = useState(false)
    const pId = productId||props.match.params.pId;
    const sId = pId.search("s-") === 0 ? pId.replace("s-", "") : undefined;

    useEffect(() => {
        if (!sId)
            return
        getData()
    }, [])

    function getData() {
        setError(false)
        ControllerProduct.Storage.getSingle({sId: sId})[1]().then(res => {
            setRedirect(rout.Storage.Single.Refresh.create(res.data.product.id))
        }).catch(e => {
            setError(true)
        })
    }


    return (
        !redirect ?
            !error ?
                !sId ?
                    <Dashboard dialogMode={dialogeMode} pId={pId} {...props}/> :
                    <Backdrop open={true} style={{zIndex: 99999999}}/> :
                <ComponentError tryAgainFun={getData}/> :
            <Redirect
                to={{
                    pathname: redirect || rout.Storage.rout,
                }}
            />
    )
}

function Dashboard({dialogMode,pId, ...props}) {
    const theme = useTheme();
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();
    const [product, setProduct] = useState(null);
    const [storages, setStorages] = useState(null);
    const [openEditStorageDialog, setOpenEditStorageDialog] = useState(false);
    const [editStorageId, setEditStorageId] = useState(undefined);
    const [defaultStorage, setDefaultStorage] = useState({
        id: null,
        nextId: null
    });
    const [setting, setSetting] = useState({
        saveDialog: false,
        loading: false,
        error: false,
        redirect: false,
        sortData: false,
        editData: undefined,
        manageDialog: false,
    });


    const {isPackage, isService, isProduct, isPackageItem} = product ? product.type : {};

    useEffect(() => {
        getStorages();
    }, []);

    useEffect(() => {
        if (defaultStorage.nextId === null)
            return;
        ControllerProduct.Product.changeDefaultStorage({pId: pId, storageId: defaultStorage.nextId}).then((res) => {
            setStorages(_.sortBy(storages, function (item) {
                return item.storageId !== defaultStorage.nextId;
            }));
            setDefaultStorage({
                ...defaultStorage,
                id: defaultStorage.nextId,
                nextId: null
            });

        }).catch(() => {
            setDefaultStorage({
                ...defaultStorage,
                nextId: null
            });
            enqueueSnackbar('تغییر انبار پیشفرض با مشکل برخود.',
                {
                    variant: "error",
                    action: (key) => (
                        <Fragment>
                            <Button onClick={() => {
                                closeSnackbar(key)
                            }}>
                                {lang.get('close')}
                            </Button>
                        </Fragment>
                    )
                });
        });
    }, [defaultStorage.nextId]);

    //region requests

    function getStorages() {
        setSetting({
            ...setting,
            loading: true,
            error: false
        });
        setProduct(undefined)
        setStorages(undefined)
        ControllerProduct.Storage.get({pId: pId}).then((res) => {
                setProduct(res.product);
                setStorages(res.storages);
                setSetting({
                    ...setting,
                    loading: false,
                });
                setDefaultStorage({
                    ...defaultStorage,
                    id: res.product.default_storage_id
                })
                if (false && DEBUG)
                    setSetting({
                        ...setting,
                        editData: res.storages[0]
                    })
            }
        ).catch(() => {
            setSetting({
                ...setting,
                error: true,
                loading: false
            })
        })
    }

    //endregion requests

    function handleSaveSort() {
        setSetting({
            ...setting,
            loading: true
        });
        const list = [];
        _.forEach(setting.sortData, (s) => {
            list.push(s.storageId)
        });
        ControllerProduct.Storage.sortChange({pId: pId, storagesId: list}).then(() => {
            setStorages(_.cloneDeep(setting.sortData));
            setSetting({
                ...setting,
                sortData: false
            })
        }).catch(() => {
            setSetting({
                ...setting,
                loading: false
            });
        });

    }

    const handleEditDialog = useCallback((newData) => {
        if (newData) {
            setStorages(null);
            getStorages();
        }
        setSetting(c => {
            return {
                ...c,
                editData: undefined
            }
        })
    }, [setSetting]);

    function handleActiveStorage(i) {
        setSetting({
            ...setting,
            loading: true
        });
        const item = storages[i];
        ControllerProduct.Storage.activeStorage({sId: item.storageId, active: item.disable}).then(res => {
            getStorages()
            // if (res.status === 202) {
            //     const data = storages;
            //     data[i] = {
            //         ...data[i],
            //         disable: !data[i].disable
            //     };
            //     setStorages([...data]);
            // }
            setSetting({
                ...setting,
                loading: false
            });
        }).catch(() => {
            setSetting({
                ...setting,
                loading: false
            });
        })
    }

    function handleChangeManage() {
        setSetting({
            ...setting,
            manageDialog: false,
            loading: true
        });

        ControllerProduct.Product.manage({pId: pId, manage: !product.manage}).then(() => {
            getStorages();
        }).catch(() => {
            setSetting({
                ...setting,
                manageDialog: false,
                loading: false
            });
        })
    }

    const isDarkMode = theme.palette.type === "dark";

    function onEditStorageClick(storageId) {
        setEditStorageId(storageId)
        setOpenEditStorageDialog(s=>!s)
    }

    const Transition = React.forwardRef(function Transition(props, ref) {
        return <Slide direction="up" ref={ref} {...props} />;
    });

    const handleCloseStorageEditDialog = () => {
        setOpenEditStorageDialog(false);
    };


    function EditStorageDialog({open,id,...props}){
        return  <Dialog
            fullScreen
            open={openEditStorageDialog}
            onClose={handleCloseStorageEditDialog}
            TransitionComponent={Transition}
            style={{marginTop:'40px'}}
        >
            <AppBar sx={{ position: 'relative' }}>
                <Toolbar>
                    <IconButton
                        edge="start"
                        color="#fff"
                        onClick={handleCloseStorageEditDialog}
                        aria-label="close"
                    >
                        <Close />
                    </IconButton>
                    <Typography color={'white'} sx={{ ml: 2, flex: 1 }} variant="h6" component="div">
                        ویرایش انبار
                    </Typography>
                </Toolbar>
            </AppBar>
            <StorageBase storageId={editStorageId} dialogMode={true} onCloseDialog={handleCloseStorageEditDialog}/>
        </Dialog>
    }

    return (
        <React.Fragment>
            {
                !setting.error ?
                    (!setting.loading && storages) ?
                        <React.Fragment>
                        <EditStorageDialog open={openEditStorageDialog} id={editStorageId}/>
                        <Box display={'flex'} flexWrap={'wrap'} width={1} p={2}>
                            <Box display={'flex'} width={1}>
                                {product &&
                                <Box pr={2}>
                                    <Link toHref={product.link} style={{...UtilsStyle.widthFitContent()}}>
                                        <Typography variant={'h6'} fontWeight={600} py={2}>
                                            {product.name[siteLang]}
                                        </Typography>
                                    </Link>
                                </Box>
                                }
                                <ButtonLink
                                    toHref={rout.Storage.Single.Create.createStorage(pId)}
                                    style={{
                                        background: theme.palette.primary.main,
                                    }}>
                                    <Typography variant={'h6'} px={1} color='#fff'>
                                        افزودن انبار جدید
                                    </Typography>
                                </ButtonLink>
                                {
                                    (isService || isProduct) &&
                                    <Box pl={1}>
                                        {
                                            storages.length <= 1 ?
                                                <BaseButton
                                                    disabled={true}
                                                    style={{
                                                        height: '100%'
                                                    }}>
                                                    ویرایش مبلغ جمعی
                                                </BaseButton> :
                                                <ButtonLink
                                                    toHref={rout.Storage.Single.MultiEdit.multiEdit(pId, {pricing: true})}
                                                    style={{
                                                        background: theme.palette.secondary.main,
                                                        height: '100%'
                                                    }}>
                                                    <Typography variant={'h6'} px={1} color='#fff'>
                                                        ویرایش مبلغ جمعی
                                                    </Typography>
                                                </ButtonLink>
                                        }
                                    </Box>
                                }
                            </Box>
                            <Box width={1} py={2} px={2} display={'flex'} alignItems={'center'}>
                                <Info
                                    ml={0.5}
                                    items={setting.sortData ? sortInfo : info}/>
                                <Tooltip title={"لیست تغییر کرده است و باید یکی از گزینه های زیر را انتخاب کنید."}
                                         disable={!_.isArray(setting.sortData)}>
                                    <Typography alignItems={'center'}>
                                        {lang.get("sorting")}:
                                        <Switch
                                            checked={Boolean(setting.sortData)}
                                            disabled={!storages || storages.length < 3 || _.isArray(setting.sortData || setting.loading)}
                                            onChange={(v) => {
                                                if (setting.loading)
                                                    return;
                                                let newData = false;
                                                if (setting.sortData === false) {
                                                    newData = true
                                                }
                                                setSetting({
                                                    ...setting,
                                                    sortData: newData
                                                })
                                            }}/>
                                    </Typography>
                                </Tooltip>
                                <Typography alignItems={'center'} ml={2}>
                                    انتخاب اتوماتیک انبار پیشفرض:
                                    <Switch
                                        checked={Boolean(product.manage)}
                                        disabled={setting.loading}
                                        onChange={(v) => {
                                            setSetting({
                                                ...setting,
                                                manageDialog: true
                                            });
                                            setTimeout(() => {
                                                // setProduct({
                                                //     ...product,
                                                //     manage: !product.manage
                                                // });
                                                // setSetting({
                                                //     ...setting,
                                                //     loading:false
                                                // })
                                            }, 2000)
                                        }}/>
                                </Typography>
                            </Box>
                            {!setting.sortData ?
                                storages.map((s, i) => (
                                    <StorageItem key={s.storageId || i}
                                                 productId={pId}
                                                 dialogMode={dialogMode}
                                                 onEditStorageClick={onEditStorageClick}
                                                 activeStorageId={defaultStorage.id}
                                                 nextActiveStorageId={defaultStorage.nextId}
                                                 product={product}
                                                 item={s}
                                                 onEdit={() => {
                                                     setSetting({
                                                         ...setting,
                                                         editData: s
                                                     })
                                                 }}
                                                 onRefresh={getStorages}
                                                 disable={Boolean(setting.editData || !!defaultStorage.nextId)}
                                                 changeDefaultStorage={(id) => {
                                                     if (id === defaultStorage.id)
                                                         return;
                                                     setDefaultStorage({
                                                         ...defaultStorage,
                                                         nextId: id
                                                     });
                                                 }}
                                                 changeActiveStorage={() => {
                                                     handleActiveStorage(i);
                                                 }}/>
                                )) :
                                <Box width={1} px={2}>
                                    <Box pb={2} display={'flex'}>
                                        <SuccessButton
                                            disabled={setting.loading || !_.isArray(setting.sortData)}
                                            variant={"outlined"}
                                            onClick={handleSaveSort}>
                                            {lang.get("save")}
                                        </SuccessButton>
                                        <Box pr={1}>
                                            <BaseButton
                                                variant={"text"}
                                                disabled={setting.loading || !_.isArray(setting.sortData)}
                                                onClick={() => setSetting({
                                                    ...setting,
                                                    sortData: false
                                                })}>
                                                {lang.get("reset")}
                                            </BaseButton>
                                        </Box>
                                    </Box>
                                    <DraggableList
                                        items={_.isArray(setting.sortData) ? setting.sortData : storages}
                                        idKey={'storageId'}
                                        onItemsChange={(items) => {
                                            if (items[0].storageId !== defaultStorage.id) {
                                                enqueueSnackbar('انبار پیشفرض باید اولین گزینه باشد.',
                                                    {
                                                        variant: "error",
                                                        action: (key) => (
                                                            <Fragment>
                                                                <Button onClick={() => {
                                                                    closeSnackbar(key)
                                                                }}>
                                                                    {lang.get('close')}
                                                                </Button>
                                                            </Fragment>
                                                        )
                                                    });
                                            }
                                            const data = _.sortBy(items, function (item) {
                                                return item.storageId !== defaultStorage.id;
                                            });
                                            setSetting({
                                                ...setting,
                                                sortData: _.isEqual(storages, data) ? true : data
                                            });
                                        }}
                                        rootStyle={(dragging) => {
                                            return {
                                                backgroundColor: dragging ? null : isDarkMode ? theme.palette.background.default : grey[200],
                                                ...UtilsStyle.borderRadius(5),
                                                ...UtilsStyle.transition(500)
                                            }
                                        }}
                                        render={(item, props, {isDraggingItem, index, ...p}) => (
                                            <Box
                                                display={'flex'}
                                                width={1}
                                                {...props}>
                                                <Box display={'flex'} alignItems={'center'}
                                                     py={1}
                                                     style={{
                                                         backgroundColor: item.storageId === defaultStorage.id ?
                                                             isDarkMode ? brown[700] : yellow[50] :
                                                             index < 4 ?
                                                                 isDarkMode ? brown[400] : cyan[50] : null
                                                     }}>
                                                    <Box px={2} py={1} style={{color: theme.palette.text.primary}}>
                                                        {isDraggingItem ?
                                                            <MenuOpen/> :
                                                            <Menu/>
                                                        }
                                                    </Box>
                                                    <Typography pr={2} pl={1} variant={'body1'} fontWeight={600}
                                                                style={{
                                                                    justifyContent: 'center',
                                                                }}>
                                                        {item.storageTitle ? item.storageTitle[siteLang] : "-------"}
                                                    </Typography>
                                                </Box>
                                            </Box>
                                        )}
                                    />
                                </Box>
                            }
                            <SaveDialog open={setting.manageDialog}
                                        submitText={"بله"}
                                        cancelText={"خیر"}
                                        text={(
                                            <Box display={'flex'} flexDirection={'column'} px={3}>
                                                <Typography variant={'h6'} fontWeight={400} pb={2}>
                                                    {product.manage && "لغو "}
                                                    انتخاب اتوماتیک انبار
                                                </Typography>
                                                <Typography variant={'body1'}>
                                                    {product.manage ?
                                                        "آیا اطمینان دارید که قصد کنترل دستی بر روی انبار پیشفرض خود دارید؟"
                                                        :
                                                        "در صورت رفتن به حالت خودکار تنظیمات پیشفرض مرتب‌سازی امکان دارد عوض شود."}
                                                </Typography>
                                                {product.manage &&
                                                <Typography variant={'caption'} color={theme.palette.text.secondary}
                                                            pt={0.5}>
                                <span style={{color: orange[300], fontWeight: 600, paddingLeft: theme.spacing(0.3)}}>
                                    توجه:
                                </span>
                                                    کنترل بر انبار پیشفرض را باید بشکل دستی رصد کنید و اگر انبار پیشفرض
                                                    بدون موجودی باشد محصول در برخی از قسمت های سایت نشان داده نخواهد شد.
                                                </Typography>
                                                }

                                            </Box>)}
                                        onSubmit={handleChangeManage}
                                        onCancel={() => {
                                            setSetting({
                                                ...setting,
                                                manageDialog: false
                                            })
                                        }}/>
                        </Box>
                        </React.Fragment>

                            : <PleaseWait/> :
                    <ComponentError tryAgainFun={getStorages}/>
            }
            <Backdrop open={setting.loading} style={{zIndex: 99999999}}/>
        </React.Fragment>
    )
}

//region Components
function StorageItem({onEditStorageClick,dialogMode=false,activeStorageId, nextActiveStorageId, product, disable, item, onRefresh, changeDefaultStorage, changeActiveStorage, onEdit}) {
    const theme = useTheme();
    const {booking_type} = product;
    const {booking_cost, least_booking_time} = item;

    const [state, setState] = useState({
        actionPanel: false,
        featurePanel: false
    });
    const [loading, setLoading] = useState(false)
    const [openDiscountCode, setDiscountCode] = useState(false)
    const {isPackage, isService, isProduct, isPackageItem} = product.type;
    const d = ControllerProduct.Storage.getSingle({sId: item.storageId});
    const {data: featurePanelData, error} = useSWR(d[0] + state.featurePanel, () => {
        if (!state.featurePanel)
            return undefined
        return d[1]()
    }, {
        onError: () => {
            setState({
                ...state,
                featurePanel: false
            })
        }
    })

    function handleActionPanel(open) {
        if (state.actionPanel === open)
            return;
        setState({
            ...state,
            actionPanel: open
        })
    }

    function handleFeaturePanel(open,dialogeMode) {
        if (state.featurePanel === open)
            return;
        setState({
            ...state,
            featurePanel: open
        })
    }

    useEffect(() => {
        if (!disable)
            return;
        setState({
            ...state,
            actionPanel: false
        })
    }, [disable]);

    const isDefaultStorage = activeStorageId === item.storageId;
    const isNextDefaultStorage = nextActiveStorageId === item.storageId;
    let index = 0;
    const isDarkMode = theme.palette.type === "dark";


    function openDialog(){
        onEditStorageClick(item.storageId)
    }


    const menuItem = (rightClick) => {
        return [
            (
                <MenuItem onClick={dialogMode?openDialog:undefined} href={dialogMode?undefined:rout.Storage.Single.Edit.editStorage(item.storageId)}>
                    <Box width={1} display={'flex'} alignItems={'center'}>
                        <IconButton
                            disabled={disable}
                            onClick={(e) => {
                                // e.stopPropagation();
                                // onEdit();
                                // handleActionPanel(false);
                            }}
                            style={{
                                color: cyan[300],
                                margin: theme.spacing(0.5),
                            }}>
                            <Edit/>
                        </IconButton>
                        {
                            rightClick &&
                            <Typography variant={"subtitle1"} px={1}>
                                ویرایش
                            </Typography>
                        }
                    </Box>
                </MenuItem>
            ),
            (
                <MenuItem href={rout.Storage.Single.Edit.editStorage(item.storageId, {pricing: true})}>
                    <Box width={1} display={'flex'} alignItems={'center'}>
                        <Tooltip title={"ویرایش قیمت"}>
                            <IconButton
                                disabled={disable}
                                onClick={(e) => {
                                    // e.stopPropagation();
                                    // onEdit();
                                    // handleActionPanel(false);
                                }}
                                style={{
                                    color: cyan[300],
                                    margin: theme.spacing(0.5),
                                }}>
                                <AttachMoney />
                            </IconButton>
                        </Tooltip>
                        {
                            rightClick &&
                            <Typography variant={"subtitle1"} px={1}>
                                ویرایش قیمت
                            </Typography>
                        }
                    </Box>
                </MenuItem>
            ),
            (
                (isProduct || isPackageItem) ?
                    <MenuItem  href={rout.Storage.Single.Edit.editStorage(item.storageId, {storages: true})}>
                        <Box width={1} display={'flex'} alignItems={'center'}>
                            <Tooltip title={"ویرایش انبار"}>
                                <IconButton
                                    disabled={disable}
                                    onClick={(e) => {
                                        // e.stopPropagation();
                                        // onEdit();
                                        // handleActionPanel(false);
                                    }}
                                    style={{
                                        color: cyan[300],
                                        margin: theme.spacing(0.5),
                                    }}>
                                    <Icon className={"fal fa-box-full"}/>
                                </IconButton>
                            </Tooltip>
                            {
                                rightClick &&
                                <Typography variant={"subtitle1"} px={1}>
                                    ویرایش انبار
                                </Typography>
                            }
                        </Box>
                    </MenuItem> :
                    undefined
            ),
            (
                isService ?
                    <MenuItem href={rout.Storage.Single.Code.editStorage(item.storageId)}>
                        <Box width={1} display={'flex'} alignItems={'center'}>
                            <Tooltip title={"کدها"}>
                                <IconButton
                                    disabled={disable}
                                    onClick={(e) => {
                                        // e.stopPropagation();
                                        // setDiscountCode(true)
                                    }}
                                    style={{
                                        color: cyan[300],
                                        margin: theme.spacing(0.5),
                                    }}>
                                    <Receipt  />
                                </IconButton>
                            </Tooltip>
                            {
                                rightClick &&
                                <Typography variant={"subtitle1"} px={1}>
                                    کدها
                                </Typography>
                            }
                        </Box>
                    </MenuItem> :
                    undefined
            ),
            (
                <MenuItem
                    onClick={(e) => {
                        e.stopPropagation();
                        setLoading(true);
                        ControllerProduct.Storage.save({
                            pId: product.id,
                            sId: item.storageId,
                            duplicate: true,
                            title: {
                                ...item.storageTitle,
                                fa: item.storageTitle["fa"] + " - جدید"
                            }
                        }).finally(()=>{
                            setLoading(false)
                            onRefresh()
                        })
                    }}
                style={{
                    minWidth:rightClick ? "100%" : undefined
                }}>
                    <Box width={1} display={'flex'} alignItems={'center'}>
                        <Tooltip title={"تکثیر کردن"}>
                            <IconButton
                                disabled={disable}
                                onClick={(e) => {
                                    // e.stopPropagation();
                                    // setDiscountCode(true)
                                }}
                                style={{
                                    color: cyan[300],
                                    margin: theme.spacing(0.5),
                                }}>
                                <FileCopyOutlined className={"fal fa-receipt"}/>
                            </IconButton>
                        </Tooltip>
                        {
                            rightClick &&
                            <Typography variant={"subtitle1"} px={1}>
                                تکثیر کردن
                            </Typography>
                        }
                    </Box>
                </MenuItem>
            ),
            (
                (!rightClick && !item.disable && !product.manage) ?
                    <Tooltip title={lang.get("default_storage")} disable={disable}>
                        <Radio checked={isDefaultStorage || isNextDefaultStorage}
                               disabled={!(isDefaultStorage || isNextDefaultStorage) && disable}
                               onClick={() => {
                                   if (disable)
                                       return;
                                   changeDefaultStorage(item.storageId);
                                   handleActionPanel(false);
                               }}
                               style={{
                                   color: isDefaultStorage ? yellow[800] : isNextDefaultStorage ? cyan[800] : grey[400]
                               }}/>
                    </Tooltip> :
                    undefined
            ),
            (
                <MenuItem onClick={changeActiveStorage} style={{minWidth: rightClick ? "100%" : "unset"}}>
                    <Box display={'flex'} alignItems={'center'}>
                        <Tooltip title={"فعال"}>
                            <Switch checked={!item.disable} disabled={disable}
                                    onChange={!rightClick ? changeActiveStorage : undefined}/>
                        </Tooltip>
                        {
                            rightClick &&
                            <Typography variant={"subtitle1"} px={1}
                                        style={{cursor: "pointer"}}>
                                {!item.disable ? "غیرفعال کردن" : "فعال کردن"}
                            </Typography>
                        }
                    </Box>
                </MenuItem>
            ),
            (
                !rightClick ?
                    <Tooltip title={lang.get("close")}>
                        <IconButton
                            disabled={disable}
                            onClick={(e) => {
                                e.stopPropagation();
                                handleActionPanel(false);
                            }}
                            style={{
                                color: cyan[300],
                                margin: theme.spacing(0.5),
                            }}>
                            <Close/>
                        </IconButton>
                    </Tooltip> :
                    undefined
            )
        ].filter(item => item !== undefined)
    }

    return (
        <Box width={1 / 3} p={1}>
            <Box
                style={{
                    ...UtilsStyle.transition(),
                    ...UtilsStyle.boxShadow(item.disable ? null : (isDefaultStorage || isNextDefaultStorage) ? `0px 0px 10px 0.5px ${isDefaultStorage ? isDarkMode ? brown[100] : yellow[300] : isDarkMode ? brown[50] : cyan[200]}` : null),
                    ...UtilsStyle.borderRadius(5)
                }}>
                <RightClickMenu component={Card} p={2}
                                display={'flex'} flexDirection={'column'}
                                height={1} width={1}
                                onDoubleClick={() => {
                                    handleActionPanel(true)
                                }}
                                items={menuItem(true)}
                                style={{
                                    position: 'relative',
                                    backgroundColor: item.disable ? theme.palette.action.disabledBackground : (isDefaultStorage || isNextDefaultStorage) ? isDefaultStorage ?
                                        isDarkMode ? brown[700] : yellow[50] : isDarkMode ? brown[400] : cyan[50] : theme.palette.background.paper,
                                    opacity: item.disable ? 0.5 : 1,
                                }}>
                    <Box display={'flex'}>
                        <Typography
                            className={"enNum"}
                            variant={'body1'} fontWeight={500} pb={2}
                                    color={theme.palette.text.secondary}
                                    style={{
                                        flex: 1,
                                        minHeight: 60
                                    }}>
                            {item.storageTitle ? item.storageTitle[siteLang] : "-----"}
                        </Typography>
                        <Box pr={0.5} pb={0.5} dir={'ltr'} display={'flex'} alignItems={'center'}>
                            <Tooltip title={lang.get("action")}>
                                <span>
                                    <IconButton
                                        size={"small"}
                                        disabled={disable}
                                        onClick={(e) => {
                                            e.stopPropagation();
                                            handleActionPanel(true);
                                        }}
                                        style={{
                                            marginTop: theme.spacing(0.5),
                                        }}>
                                    <Settings/>
                                </IconButton>
                                </span>
                            </Tooltip>
                            {
                                product.has_selectable_feature &&
                                <Box pr={0.5}>
                                    <Tooltip title={lang.get("features")}>
                                    <span>
                                        <IconButton
                                            size={"small"}
                                            disabled={disable}
                                            onClick={(e) => {
                                                e.stopPropagation();
                                                handleFeaturePanel(true);
                                            }}
                                            style={{
                                                marginTop: theme.spacing(0.5),
                                            }}>
                                            <ExtensionOutlined/>
                                        </IconButton>
                                    </span>
                                    </Tooltip>
                                </Box>
                            }
                            {
                                (item.media && item.media.image) &&
                            <Box pr={1}>
                                <Img height={45} width={"auto"} minHeight={1} src={item.media.image} zoomable={true} imgStyle={{...UtilsStyle.borderRadius(5)}}/>
                            </Box>
                            }
                        </Box>
                    </Box>
                    <Divider/>
                    <Box display={'flex'} flexWrap={'wrap'}>
                        <ShowValue
                            index={index++}
                            label={lang.get("start_price") + ':'}
                            value={UtilsFormat.numberToMoney(item.startPrice)}/>
                        <ShowValue
                            index={index++}
                            label={lang.get("final_price") + ':'}
                            value={UtilsFormat.numberToMoney(item.finalPrice)}/>
                        <ShowValue
                            index={index++}
                            label={lang.get("discount_price_with_label") + ':'}
                            value={UtilsFormat.numberToMoney(item.discountPrice)}
                            backgroundColor={isDarkMode ? green[700] : green[50]}/>
                        <ShowValue
                            index={index++}
                            label={lang.get("discount_percent") + ':'}
                            value={UtilsFormat.numberToMoney(item.discountPercent || 0) + '%'}
                            backgroundColor={isDarkMode ? green[700] : green[50]}/>
                        <ShowValue
                            index={index++}
                            label={lang.get("available_count") + ':'}
                            value={item.availableCountForSale}
                            backgroundColor={(item.availableCountForSale - item.minCountAlert) < 5 ? red[50] : null}/>
                        <ShowValue
                            index={index++}
                            variant={item.maxCountForSale ? undefined : 'body2'}
                            label={lang.get("max_count_for_sale") + ':'}
                            value={item.maxCountForSale || "نامحدود"}/>
                        <ShowValue
                            index={index++}
                            variant={item.minCountForSale ? undefined : 'body2'}
                            label={lang.get("min_count_for_sale") + ':'}
                            value={item.minCountForSale || "نامحدود"}/>
                        {
                            item.settings.ratio &&
                            <React.Fragment>
                                <ShowValue
                                    index={index++}
                                    variant={'body1'}
                                    label={'ضریب:'}
                                    value={item.settings.ratio}/>
                                <ShowValue
                                    index={index++}
                                    variant={'body1'}
                                    label={'ضریب تخفیف:'}
                                    value={item.settings.ratio_discount}/>
                            </React.Fragment>
                        }
                        {
                            booking_type.key === bookingType.datetime.key &&
                            <React.Fragment>
                                <ShowValue
                                    index={index++}
                                    variant={'body2'}
                                    label={"حداقل زمان رزرو (ساعت):"}
                                    value={least_booking_time}/>
                                <ShowValue
                                    index={index++}
                                    variant={'body2'}
                                    label={"هزینه ارسال رزرو:"}
                                    value={UtilsFormat.numberToMoney(booking_cost)}/>
                            </React.Fragment>
                        }
                    </Box>
                    <Box display={'flex'} flexWrap={'wrap'} alignItems={'center'}>
                        {item.deadline &&
                        <Box
                            style={{
                                margin: '0 auto',
                                ...UtilsStyle.disableTextSelection(),
                                ...UtilsStyle.widthFitContent()
                            }}>
                            <Tooltip title={(
                                <Typography variant={'body1'} color={'#fff'}>
                                    {item.jDeadline}
                                </Typography>
                            )}>
                                <Box>
                                    <Timer time={item.deadline}
                                           textColor={(item.deadline > Date.now() / 1000) ? theme.palette.text.secondary : isDarkMode ? red[200] : theme.palette.error.main}
                                           pt={2}/>
                                </Box>
                            </Tooltip>
                        </Box>
                        }
                    </Box>
                    {state.actionPanel &&
                    <Box
                        display={'flex'}
                        flexWrap={'wrap'}
                        alignItems={'center'}
                        justifyContent={'center'}
                        p={2}
                        onDoubleClick={(e) => {
                            e.stopPropagation();
                            handleActionPanel(false);
                        }}
                        style={{
                            position: 'absolute',
                            top: 0,
                            bottom: 0,
                            right: 0,
                            left: 0,
                            backgroundColor: 'rgba(0, 0, 0, 0.7)',
                            backdropFilter: ' blur(2px)',
                        }}>
                        {
                            menuItem(false).map((it, i) => (
                                <React.Fragment key={i}>
                                    {it}
                                </React.Fragment>
                            ))
                        }
                    </Box>
                    }
                    {item.features && state.featurePanel &&
                    <Box
                        display={'flex'}
                        flexWrap={'wrap'}
                        alignItems={'center'}
                        justifyContent={'center'}
                        flexDirection={'column'}
                        p={2}
                        onDoubleClick={(e) => {
                            e.stopPropagation();
                            handleFeaturePanel(false);
                        }}
                        style={{
                            position: 'absolute',
                            top: 0,
                            bottom: 0,
                            right: 0,
                            left: 0,
                            backgroundColor: 'rgba(0, 0, 0, 0.7)',
                            backdropFilter: ' blur(2px)',
                        }}>
                        {
                            featurePanelData ?
                                <React.Fragment>

                                    <Box pb={0.5} dir={"ltr"}>
                                        <Tooltip title={lang.get("close")}>
                                            <Box
                                                onClick={(e) => {
                                                    e.stopPropagation();
                                                    handleFeaturePanel(false);
                                                }}
                                                style={{
                                                    cursor: "pointer"
                                                }}>
                                                <Close
                                                    style={{color: "#fff"}}/>
                                            </Box>
                                        </Tooltip>
                                    </Box>
                                    <Box display={'flex'} flexDirection={'column'}>
                                        {
                                            featurePanelData.data.storage.features.map((it, index) => (
                                                <Box key={it.id} display={'flex'}
                                                     alignItems={'center'} py={0.5}
                                                     flexWrap={'wrap'}
                                                     style={{
                                                         borderColor: "#fff",
                                                         borderStyle: "solid",
                                                         borderWidth: '0px',
                                                         borderBottomWidth: item.features.length - 1 > index ? '1px' : "0px"
                                                     }}>
                                                    <Typography variant={"h6"} pr={0.3} color={"#fff"}>
                                                        {it.feature.name[siteLang]}:
                                                    </Typography>
                                                    {
                                                        it.values.map((v, i) => (
                                                            <React.Fragment key={v.id}>
                                                                {
                                                                    (v.selected && _.findIndex(v.storage_id, (s => (s === item.storageId))) !== -1) ?
                                                                        <Typography py={0.25} pr={1} variant={"body2"}
                                                                                    color={"#fff"}>
                                                                            {v.name[siteLang]} {i < it.feature.values.length - 1 ? ", " : ""}
                                                                        </Typography> :
                                                                        <React.Fragment/>
                                                                }
                                                            </React.Fragment>
                                                        ))
                                                    }
                                                </Box>
                                            ))
                                        }
                                    </Box>
                                </React.Fragment> :
                                <Box display={'flex'} alignitems={'center'} jsutifyContent={"center"}>
                                    <CircularProgress color="inherit"
                                                      style={{
                                                          color: cyan[300],
                                                          width: 80,
                                                          height: 80
                                                      }}/>
                                </Box>
                        }
                    </Box>
                    }
                </RightClickMenu>

                <Backdrop open={loading} style={{zIndex: 99999999}}/>
            </Box>
        </Box>
    )
}

function ShowValue({index, label, value, variant = "h6", backgroundColor, ...props}) {
    return (
        <Box display={'flex'}
             flexWrap={'wrap'}
             alignItems={'center'}
             width={1 / 2}
             px={1}
             {...props}
             style={{
                 borderColor: grey[200],
                 borderWidth: `0 0 1px ${index % 2 === 0 ? 1 : 0}px`,
                 borderStyle: 'solid',
                 backgroundColor: backgroundColor,
                 ...props
             }}>
            <Typography variant={'body2'} px={0.25} py={1}>
                {label}
            </Typography>
            <Typography variant={variant} fontWeight={600} px={0.25} py={1}>
                {value}
            </Typography>
        </Box>
    )
}

//region ComponentsHelper
//endregion Components
//endregion Components

