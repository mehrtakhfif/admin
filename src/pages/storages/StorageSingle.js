import React, {Fragment, useCallback, useEffect, useRef, useState} from "react";
import Box from "@material-ui/core/Box";
import ControllerProduct from "../../controller/ControllerProduct";
import {activeLang, DEBUG, lang, siteLang, theme} from "../../repository";
import PleaseWait from "../../components/base/loading/PleaseWait";
import Typography from "../../components/base/Typography";
import Link from "../../components/base/link/Link";
import {UtilsFormat, UtilsStyle} from "../../utils/Utils";
import {Card, Radio, useTheme} from "@material-ui/core";
import {cyan, green, grey, orange} from "@material-ui/core/colors";
import Divider from "@material-ui/core/Divider";
import red from "@material-ui/core/colors/red";
import _ from 'lodash';
import {useSnackbar} from "notistack";
import Button from "@material-ui/core/Button";
import BaseButton from "../../components/base/button/BaseButton";
import {
    AccountBalance,
    Add,
    AllInclusive,
    Delete,
    FitnessCenter,
    HighlightOff,
    Remove,
    Sync,
    SyncDisabled,
    TextRotateVertical,
    TextRotationAngleup,
    TextRotationNone,
    WorkOutline
} from "@material-ui/icons";
import Switch from "@material-ui/core/Switch";
import SuccessButton from "../../components/base/button/buttonVariant/SuccessButton";
import Tooltip from "../../components/base/Tooltip";
import IconButton from "@material-ui/core/IconButton";
import FormContainer from "../../components/base/FormContainer";
import {
    convertFeatures,
    convertGender,
    convertIconUrl,
    convertTax,
    createMultiLanguage,
    TaxValue
} from "../../controller/converter";
import BoxWithTopBorder from "../../components/base/textHeader/BoxWithTopBorder";
import ButtonBase from "@material-ui/core/ButtonBase";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import {makeStyles} from "@material-ui/styles";
import SelectSupplier, {UserItem} from "../supplier/SelectSupplier";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import DatePicker from "../../components/base/datePicker/DatePicker";
import moment from 'moment-jalaali';
import JDate from "../../utils/JDate";
import StickyBox from "react-sticky-box";
import SaveDialog from "../../components/dialog/SaveDialog";
import MaterialFormControl from "@material-ui/core/FormControl";
import FormControl from "../../components/base/formController/FormController";
import TextFieldMultiLanguageContainer, {createName} from "../../components/base/textField/TextFieldMultiLanguageContainer";
import TextFieldContainer, {
    errorList,
    notValidErrorTextField
} from "../../components/base/textField/TextFieldContainer";
import TextField from "../../components/base/textField/TextField";
import NewEditorMultiLanguage from "../../components/base/textField/editor/EditorMultiLanguage";
import NewPriceTextField from "../../components/base/textField/PriceTextField";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import NoneTextField from "../../components/base/textField/NoneTextField";
import Backdrop from "@material-ui/core/Backdrop/Backdrop";
import CircularProgress from "@material-ui/core/CircularProgress";
import LocalStorageUtils from "../../utils/LocalStorageUtils";
import CacheFindDialog from "../../components/dialog/CacheFindDialog";
import Icon from "../../components/icon/Icon";
import SelectVip from "../vip/SelectVip";
import warning from "react-redux/lib/utils/warning";
import DimensionsTextField from "../../components/base/textField/DimensionsTextField";
import LengthTextField from "../../components/base/textField/LengthTextField";
import SearchSelectStorageDialog from '../storages/SearchSelectStorage'
import Img from "../../components/base/img/Img";
import Slide from "@material-ui/core/Slide";
import {gcError, gcLog, isNumeric, tryIt} from "../../utils/ObjectUtils";
import StorageFeatures from "../../components/Feature/StorageFeatures";
import rout from "../../router";
import {Redirect} from "react-router-dom";
import PropTypes from "prop-types";
import DefaultTextField from "../../components/base/textField/DefaultTextField";
import PriceTextField from "../../components/base/textField/PriceTextField";
import Editor from "../../components/base/editor/Editor";
import ComponentError from "../../components/base/ComponentError";
import RightClickMenu from "../../components/base/rightClick/RightClickMenu";
import MaterialIcon from "@material-ui/core/Icon";
import yellow from "@material-ui/core/colors/yellow";
import useSWR from "swr";
import BaseDialog from "../../components/base/BaseDialog";


export default function StorageBase({
                                        onCloseDialog = undefined,
                                        dialogMode = false,
                                        storageId,
                                        productId,
                                        ...props
                                    }) {
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();

    const sId = storageId || _.toInteger(props?.match?.params?.sId);
    const pId = productId || _.toInteger(props?.match?.params?.pId);
    const {isStoragesMode, isPricingMode} = (() => {
        const t = pId;
        return {
            isStoragesMode: t === "storages",
            isPricingMode: t === "pricing"
        }
    })();
    const [product, setProduct] = useState();
    const [activeStorageId, setActiveStorageId] = useState(sId);
    const [error, setError] = useState(false);
    const [loading, setLoading] = useState(false);
    const [updateDataLoading, setUpdateDataLoading] = useState(false);
    const [storages, setStorages] = useState([]);
    const [defaultFeatures, setDefaultFeatures] = useState([]);
    const [redirect, setRedirect] = useState();


    useEffect(() => {
        if (product && activeStorageId < 1) {
            if (getStorageIndex(activeStorageId, true)) {
                return;
            }
            newStorage()
            return
        }
        getStorage()
    }, [activeStorageId])


    useEffect(() => {
        //init with new storage
        if (product && _.isEmpty(storages)) {
            // newStorage()
            setActiveStorageId(-1)
        }
    }, [product])


    async function getStorage(rId) {
        const reqId = rId || activeStorageId

        const i = getStorageIndex(reqId, true)
        if (i)
            return storages[i]
        setLoading(true)
        return await ControllerProduct.Storage.getSingle({sId: reqId, pId: pId})[1]().then(res => {
            const sts = storages;
            const storageId = reqId === 0 ? -1 : reqId;
            if (storageId !== -1) {
                const index = getStorageIndex(storageId)
                sts[index] = {
                    id: storageId,
                    data: res.data.storage || true,
                };
                setStorages([...sts]);
            }
            if (!product) {
                setProduct(res.data.product);
            }
            try {
                return res.data.storage || true
            } catch (e) {
            }
        }).catch(() => {
            setError(true)
        }).finally(() => {
            setLoading(false)
        })
    }

    function handleChange(id, data, nextStorageId, duplicate) {
        const newS = storages;
        const i = _.findIndex(newS, s => s.storageId === activeStorageId)

        const sts = storages;
        const index = getStorageIndex(id)
        sts[index] = {
            ...sts[index],
            params: data
        }
        setStorages([...sts])

        if (i !== -1) {
            newS[i] = {
                ...newS[i],
                storageTitle: data.title || newS[i].storageTitle
            }
        }

        setProduct({
            ...product,
            storages: newS
        })
        if (i === -1 && isStoragesMode) {
            handleSave(sts)
            return;
        }
        if (_.isBoolean(nextStorageId)) {
            handleSave(sts)
            return
        }

        if (duplicate) {
            getStorage(nextStorageId).then(res => {
                setTimeout(() => {
                    newStorage(nextStorageId)
                }, 1000)
            })
            return;
        }


        setActiveStorageId(nextStorageId > 0 ? nextStorageId : generateId())
    }


    function handleSave(storages, error = false) {
        setUpdateDataLoading(true);
        const newStorages = _.cloneDeep(storages);
        const item = newStorages.shift();

        if (!item) {
            if (!error) {
                if (dialogMode) {
                    onCloseDialog()
                    return;
                }
                setRedirect(rout.Storage.Single.showStorageDashboard({productId: product.id}))
                return;
            }

            setUpdateDataLoading(false);
            return
        }
        const {id, params} = item;
        ControllerProduct.Storage.save({
            pId: product.id,
            sId: id, ...params
        }).then(() => handleSave(newStorages, error)).catch(() => {
            reqCancel(`انبار: ${params.title[siteLang]} به مشکل برخورد.`);
            handleSave(newStorages, true)
        })
    }

    function handleClose() {
        if (dialogMode) {
            onCloseDialog()
            return
        }

        setRedirect(rout.Storage.Single.showStorageDashboard({productId: product.id}))

    }

    async function newStorage(duplicateId) {
        const sts = storages;
        const storageId = generateId()
        const index = getStorageIndex(storageId)
        sts[index] = {
            id: storageId,
            data: true,
        };
        let newTitle = undefined;
        if (duplicateId) {
            const {data: dItem} = sts[getStorageIndex(duplicateId)];
            newTitle = {
                ...dItem.storageTitle,
                fa: dItem.storageTitle.fa + " - جدید"
            }
            sts[index] = {
                ...sts[index],
                data: {
                    ...dItem,
                    storageTitle: newTitle,
                    storageId: storageId
                }
            }
        }

        setStorages([...sts]);

        if (!newTitle)
            newTitle = createMultiLanguage({fa: "انبار جدید"})

        setProduct({
            ...product,
            storages: [
                {
                    storageId: storageId,
                    storageTitle: newTitle
                },
            ]
        });
        if (duplicateId) {
            setActiveStorageId(storageId)
        }


        return await ControllerProduct.Features.getFeaturesStorage(pId)[1]().then(res => {
            setDefaultFeatures(res.data.features)
        }).catch((e) => {
            gcError("Storage Default Features", e)
        }).finally(() => {

        })


    }

    function generateId() {
        let id = -1
        _.forEach(storages, s => {
            if (s.id === id) {
                id = id - 1
            }
        })
        return id
    }

    function getStorageIndex(storageId, notSafe) {
        const i = _.findIndex(storages, s => s.id === storageId)
        return i !== -1 ? i : notSafe ? undefined : storages.length
    }


    function reqCancel(text = null, warning) {
        if (text)
            enqueueSnackbar(text,
                {
                    variant: warning ? "warning" : "error",
                    action: (key) => (
                        <Fragment>
                            <Button onClick={() => {
                                closeSnackbar(key)
                            }}>
                                {lang.get('close')}
                            </Button>
                        </Fragment>
                    )
                });
    }

    return (
        (!redirect && (sId || pId)) ?
            <React.Fragment>
                {
                    (!updateDataLoading && product && storages) &&
                    Object.keys(storages).map((key) => {
                        const {id, data} = storages[key];
                        return (
                            <EditItem
                                id={id}
                                key={key}
                                product={product}
                                item={data}
                                isStoragesMode={isStoragesMode}
                                isPricingMode={isPricingMode}
                                isModeActive={isStoragesMode || isPricingMode}
                                onChange={(data, nextStorageId, duplicate) => {
                                    handleChange(id, data, nextStorageId, duplicate)
                                }}
                                onClose={handleClose}
                                defaultFeatures={defaultFeatures}
                                style={{
                                    display: activeStorageId === id ? undefined : 'none'
                                }}/>
                        )
                    })
                }
                {
                    error ?
                        <ComponentError tryAgainFun={getStorage}/> :
                        (updateDataLoading || loading || !(product && storages)) &&
                        <PleaseWait/>
                }
            </React.Fragment> :
            <Redirect
                to={{
                    pathname: _.isString(redirect) ? redirect : rout.Storage.rout,
                }}
            />
    )
}

function EditItem({
                      id,
                      product,
                      item: defItem,
                      isStoragesMode,
                      isPricingMode,
                      isModeActive,
                      onChange,
                      onClose,
                      defaultFeatures,
                      ...props
                  }) {

    const productId = product.id;
    const {storageId} = defItem || {};
    const booking_type = product.booking_type;
    const {isPackage, isService} = product.type;
    const [checkedValues, setCheckedValues] = useState([])
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();
    const ref = useRef();

    const [state, setState] = useState({
        loading: false,
        saveDialog: false,
        closeDialog: false,
        cacheFind: false,
        selectVip: false
    });

    const [data, setData] = useState(false);

    function getInitialData() {
        return {
            title: createMultiLanguage(),
            disable: true,
            invoice: {
                title: createMultiLanguage(),
                description: createMultiLanguage()
            },
            storage: {
                availableCount: 0,
                availableCountForSale: 0,
                maxCountForSale: 0,
                minCountForSale: 0,
                minCountAlert: 0,
                maxShippingTime: 0
            },
            pricing: {
                startPrice: 0,
                finalPrice: 0,
                discountPrice: 0,
                shipping_cost: 0,
                tax: convertTax(),
                ratio_discount: 0,
                ratio: ""
            },
            booking: {
                booking_cost: 0,
                least_booking_time: 48,
            },
            features: [],
            moreData: {
                supplier: null,
                gender: convertGender(null, "همه"),
                startTime: undefined,
                deadline: undefined
            },
            vip_prices: [],
            dimensions: {},
            storages: isPackage ? [] : undefined,
            package_discount_price: 0,
        }
    }

    useEffect(() => {
        if (!defItem)
            return;
        if (LocalStorageUtils.get(getCacheKey())) {
            setState({...state, cacheFind: !DEBUG})
        }
        if (defItem === true) {
            setData(getInitialData());
            return;
        }

        const vip_prices = [];

        _.forEach(defItem.vip_prices, (v) => {
            const param = {
                discount_price: v.discount_price,
                available_count_for_sale: 0,
                max_count_for_sale: 0,
                ...v.vip_type
            };
            if (v.max_count_for_sale !== defItem.maxCountForSale) {
                param.max_count_for_sale = v.max_count_for_sale
            }
            if (v.available_count_for_sale !== defItem.availableCountForSale) {
                param.available_count_for_sale = v.available_count_for_sale
            }
            vip_prices.push(param)
        });

        setData({
            ...data,
            title: defItem.storageTitle,
            disable: defItem.disable,
            invoice: {
                ...state.invoice,
                title: !_.isEqual(defItem.invoiceTitle, defItem.storageTitle) ? defItem.invoiceTitle : '',
                description: defItem.invoiceDescription,
            },
            storage: {
                availableCount: defItem.availableCount || 0,
                availableCountForSale: defItem.availableCountForSale || 0,
                maxCountForSale: defItem.maxCountForSale || 0,
                minCountForSale: defItem.minCountForSale || 0,
                minCountAlert: defItem.minCountAlert || 0,
                maxShippingTime: defItem.maxShippingTime || 0
            },
            features: defItem.features || [],
            defaultFeatures: product.features,
            pricing: {
                ratio_discount: defItem.settings.ratio_discount || 0,
                ratio: defItem.settings.ratio || "",
                startPrice: defItem.startPrice,
                finalPrice: defItem.finalPrice,
                discountPrice: defItem.discountPrice,
                shipping_cost: defItem.shipping_cost,
                tax: defItem.tax,
            },
            moreData: {
                supplier: defItem.supplier,
                gender: defItem.gender,
                startTime: defItem.startTime,
                deadline: defItem.deadline
            },
            vip_prices: vip_prices,
            dimensions: {
                width: defItem.dimensions.width,
                length: defItem.dimensions.length,
                weight: defItem.dimensions.weight,
                height: defItem.dimensions.height
            },
            storages: defItem.items,
            booking: {
                booking_cost: defItem.booking_cost,
                least_booking_time: defItem.least_booking_time,
            },
            package_discount_price: defItem.package_discount_price
        });


    }, [defItem]);

    const getCacheKey = () => {
        return `storage-${productId ? productId : -1}-${storageId ? storageId : -1}`;
    };

    function getRequestProps(convertData) {
        let errorText = "";

        if (!convertData && ref.current.hasError()) {
            errorText = "فرم مشکل دارد";
            try {
                const el = ref.current.getErrorElement();
                const t = el.getAttribute(notValidErrorTextField);
                if (t)
                    errorText = t;
                el.focus();
            } catch (e) {
            }
            reqCancel(errorText);
            return;
        }

        let {
            title,
            storage_media,
            unavailable,
            booking,
            invoice,
            invoice_description,
            storage,
            pricing,
            vip,
            features,
            dimensions,
            moreData,
            package_discount_price,
            storages,
            send,
            ...fp
        } = convertData ? convertData : ref.current.serialize();

        if (isPackage) {
            package_discount_price = _.toNumber(package_discount_price)
            const p = []
            _.forEach(storages, (s, key) => {
                const pr = {}
                _.forEach(s, (sp) => {
                    _.forEach(sp, (spp, sppKey) => {
                        pr[sppKey] = _.toNumber(spp);

                    })
                })
                p.push(pr)
            })
            storages = p
        }

        try {
            dimensions.height = _.toNumber(dimensions.height);
            dimensions.width = _.toNumber(dimensions.width);
            dimensions.weight = _.toNumber(dimensions.weight);
            dimensions.length = _.toNumber(dimensions.length);
            storage.availableCount = _.toInteger(storage.availableCount);
            storage.availableCountForSale = _.toInteger(storage.availableCountForSale);
            storage.maxCountForSale = _.toInteger(storage.maxCountForSale);
            storage.minCountForSale = _.toInteger(storage.minCountForSale);
            storage.minCountAlert = _.toInteger(storage.minCountAlert);
            storage.maxShippingTime = _.toInteger(send.maxShippingTime);
            pricing.startPrice = _.toInteger(pricing.startPrice);
            pricing.finalPrice = _.toInteger(pricing.finalPrice);
            pricing.discountPrice = _.toInteger(pricing.discountPrice);
            pricing.shipping_cost = !_.isEmpty(pricing.shipping_cost) ? _.toInteger(pricing.shipping_cost) : null;
            moreData.supplierId = moreData.supplierId ? _.toInteger(moreData.supplierId) : undefined;
            moreData.startTime = moreData.startTime ? _.toInteger(moreData.startTime) : null;
            moreData.deadline = moreData.deadline ? _.toInteger(moreData.deadline) : null;
        } catch (e) {
        }

        if (convertData) {
            return {title, invoice, storage, pricing, moreData, ...fp}
        }

        try {
            if (!invoice.title[activeLang]) {
                invoice.title = title;
            }

            // if (storage.availableCount < storage.availableCountForSale) {
            //     reqCancel("تعداد موجود کمتر از تعداد موجود برای فروش است.");
            //     return
            // }
            //
            // if (pricing.discountPrice > pricing.finalPrice) {
            //     reqCancel("قیمت پایانی قبل ازفروش کمتر از قیمت پایانی است.");
            //     return
            // }
            //
            // if (pricing.discountPrice === pricing.finalPrice) {
            //     reqCancel("قیمت پایانی قبل ازفروش برابر است با قیمت پایانی و این موضوع ممکن است باعث فروش کمتر شود.", true);
            // }
            //
            // if (pricing.startPrice >= pricing.discountPrice) {
            //     reqCancel("قیمت خرید از قیمت پایانی بیشتر است.");
            //     return
            // }
            //
            // if (pricing.startPrice > 0 && !moreData.supplierId) {
            //     reqCancel("لطفا تامین کننده را انتخاب کنید.");
            //     return
            // }
        } catch (e) {
        }

        let vip_prices = []
        try {
            if (!_.isEmpty(vip)) {
                const vipData = [];
                let errorText = undefined;
                _.forEach(vip, (v, key) => {
                    const param = {};
                    param.vip_type_id = key;
                    _.forEach(v, (arr) => {
                        _.forEach(arr, (val, valKey) => {
                            param[valKey] = val
                        })
                    })
                    param.vip_type_id = _.toNumber(param.vip_type_id);
                    try {
                        if (!param.package_discount_price)
                            throw ""
                        param.package_discount_price = _.toNumber(param.package_discount_price)
                        if (package_discount_price > param.package_discount_price) {
                            errorText = 'قیمت "تخفیف فروش ویژه" کمتر از "قیمت تخفیف عادی" میباشد.'
                        }
                    } catch (e) {

                    }
                    try {
                        if (!param.discount_price)
                            throw ""
                        param.discount_price = _.toNumber(param.discount_price);
                        if (pricing.discountPrice < param.discount_price) {
                            errorText = "قیمت فروش " + param.name + " بیشتر از قیمت عادی محصول میباشد.";
                            return false;
                        }
                    } catch (e) {
                    }

                    vipData.push({
                        ...param,
                        discount_price: param.discount_price,
                        vip_type_id: param.vip_type_id,
                        package_discount_price: param.package_discount_price
                    })
                });
                if (errorText) {
                    reqCancel(errorText);
                    return
                }
                vip_prices = vipData;
            }
        } catch (e) {
        }

        const featuresParams = [];
        try {
            _.forEach(checkedValues, (value, key) => {
                const fe = {
                    product_feature_id: value.product_feature_id,
                    extra_data: {}
                };
                featuresParams.push(fe);
            })
        } catch (e) {
        }

        // if (isPackage) {
        //     return {
        //         pId: _.toInteger(productId),
        //         sId: defItem ? _.toInteger(defItem.storageId) : null,
        //         title: title,
        //         invoiceTitle: invoice.title,
        //         invoiceDescription: invoice.description,
        //         items: storages,
        //         package_discount_price: package_discount_price,
        //         vip_prices: vip_prices,
        //         gender: moreData.gender.value,
        //         features: featuresParams,
        //         startTime: (moreData.startTime && moreData.startTime !== 0) ? moreData.startTime : null,
        //         deadline: (moreData.deadline && moreData.deadline !== 0) ? moreData.deadline : null,
        //     }
        // }
        //
        // if (isService) {
        //     return {
        //         pId: _.toInteger(productId),
        //         sId: defItem ? _.toInteger(defItem.storageId) : null,
        //         title: title,
        //         invoiceTitle: invoice.title,
        //         invoiceDescription: invoice.description,
        //         startPrice: pricing.startPrice,
        //         finalPrice: pricing.finalPrice,
        //         discountPrice: pricing.discountPrice,
        //         maxCountForSale: storage.maxCountForSale || 0,
        //         minCountAlert: storage.minCountAlert || 0,
        //         tax: pricing.tax,
        //         supplierId: moreData.supplierId,
        //         gender: moreData.gender.value,
        //         startTime: (moreData.startTime && moreData.startTime !== 0) ? moreData.startTime : null,
        //         deadline: (moreData.deadline && moreData.deadline !== 0) ? moreData.deadline : null,
        //         features: featuresParams,
        //         vip_prices: vip_prices,
        //         dimensions: dimensions,
        //     }
        // }

        let ratio = {};

        try {
            if (!_.isObject(defItem) || !_.isObject(defItem.settings) || (pricing.ratio !== defItem.settings.ratio || pricing.ratio_discount !== defItem.settings.ratio_discount)) {
                if (_.toNumber(pricing.ratio) === 1 && _.toNumber(pricing.ratio_discount)) {
                    reqCancel("در ضریب 1 نمیتوان تخفیف در ضریب گروهی وارد کرد.")
                    return
                }

                if (isNumeric(pricing.ratio) && pricing.ratio < 1) {
                    reqCancel("ضریب باید 1 یا بزرگتر باشد.")
                    return;
                }

                ratio = {
                    ratio: isNumeric(pricing.ratio) ? _.toNumber(pricing.ratio) : "",
                    ratio_discount: _.toNumber(pricing.ratio_discount)
                }
            }
        } catch (e) {
        }

        let pr = {
            pId: _.toInteger(productId),
            sId: defItem ? _.toInteger(defItem.storageId) : null,
            settings: defItem.settings || {},
        }

        tryIt(() => {
            pr = {
                ...pr,
                media_id: storage_media
            }
        })

        //unavailable
        tryIt(() => {
            pr = {
                ...pr,
                unavailable: unavailable === "true",
            }
        })
        //title
        tryIt(() => {
            pr = {
                ...pr,
                title: title,
            }
        })
        //invoice
        tryIt(() => {
            pr = {
                ...pr,
                invoiceTitle: invoice.title,
                invoiceDescription: createMultiLanguage({fa: invoice_description}),
            }
        })
        //storage
        tryIt(() => {
            pr = {
                ...pr,
                availableCount: storage.availableCount,
                availableCountForSale: storage.availableCountForSale,
                maxCountForSale: storage.maxCountForSale || 0,
                minCountForSale: storage.minCountForSale || 0,
                minCountAlert: storage.minCountAlert || 0,
                maxShippingTime: storage.maxShippingTime || 0,
            }
        })

        //price
        tryIt(() => {
            const {least_booking_time, booking_cost} = booking;
            pr = {
                ...pr,
                least_booking_time: least_booking_time,
                booking_cost: booking_cost
            }
        })

        //price
        tryIt(() => {
            pr = {
                ...pr,
                ratio: pricing.ratio,
                ratio_discount: pricing.ratio_discount,
                startPrice: pricing.startPrice,
                finalPrice: pricing.finalPrice,
                discountPrice: pricing.discountPrice,
                shipping_cost: pricing.shipping_cost || null,
                tax: pricing.tax,
                settings: {
                    ...pr.settings,
                    ...ratio
                }
            }
        })

        //features
        tryIt(() => {
            if (isModeActive && !(isPricingMode || isStoragesMode))
                return
            pr = {
                ...pr,
                features: featuresParams,
            }
        })

        //vip prices
        tryIt(() => {
            pr = {
                ...pr,
                vip_prices: vip_prices,
            }
        })

        //dimensions
        tryIt(() => {
            pr = {
                ...pr,
                dimensions: dimensions,
            }
        })

        //more
        tryIt(() => {
            pr = {
                ...pr,
                supplierId: moreData.supplierId,
                gender: moreData.gender.value,
                startTime: (moreData.startTime && moreData.startTime !== 0) ? moreData.startTime : null,
                deadline: (moreData.deadline && moreData.deadline !== 0) ? moreData.deadline : null,
            }
        })

        return pr;
    }

    function handleSaveProduct() {
        if (state.loading)
            return

        setState({...state, saveDialog: false});
        onChange(getRequestProps(), true)
    }

    function handleActiveStorage() {
        setState({
            ...state,
            loading: true
        });

        ControllerProduct.Storage.activeStorage({sId: defItem.storageId, active: data.disable}).then(res => {
            if (res.status === 202) {
                setData({
                    ...data,
                    disable: !data.disable
                });
            }
            setState({
                ...state,
                loading: false
            });
        }).catch(() => {
            setState({
                ...state,
                loading: false
            });
        })
    }

    function reqCancel(text = null, warning) {
        if (text)
            enqueueSnackbar(text,
                {
                    variant: warning ? "warning" : "error",
                    action: (key) => (
                        <Fragment>
                            <Button onClick={() => {
                                closeSnackbar(key)
                            }}>
                                {lang.get('close')}
                            </Button>
                        </Fragment>
                    )
                });
        if (!warning)
            setState({
                loading: false
            })

    }

    const handleSyncCache = () => {
        const props = LocalStorageUtils.get(getCacheKey());
        if (_.isEmpty(props))
            return;
        const item = getRequestProps(props)

        setData({
            ...data,
            title: props.title,
            invoice: item.invoice,
            storage: item.storage,
            pricing: item.pricing,
            moreData: {
                ...data.moreData,
                gender: convertGender(item.moreData.gender === "true" ? true : item.moreData.gender === "false" ? false : undefined, "همه"),
                startTime: item.moreData.startTime,
                deadline: item.moreData.deadline
            }
        });
        setState({
            ...state,
            cacheFind: false
        })
    };

    const handleCloseCache = () => {
        LocalStorageUtils.remove(getCacheKey());
        setState({...state, cacheFind: false})
    };

    const handleVipRemove = (vip) => {
        const newVipPrice = data.vip_prices;
        _.remove(newVipPrice, (v) => {
            return v.id === vip.id;
        });
        setData({
            ...data,
            vip_prices: newVipPrice
        })
    };

    const handleVipOpen = () => {
        setState({
            ...state,
            selectVip: true
        })
    }

    useEffect(() => {
        let serverCheckedValues = []

        if (!_.isEmpty(data.features)) {

            data.features.map(feature => {

                const ll = feature.values.filter(it => (it.storage_id.includes(defItem.storageId)))
                serverCheckedValues = serverCheckedValues.concat(ll)

            })


        }
        setCheckedValues(serverCheckedValues)
    }, [data])

    function handleFeatureCheck({value}) {
        const oldValues = _.cloneDeep(checkedValues)
        const newValues = (_.findIndex(oldValues, (f) => {
            return f.id === value.id
        }) !== -1
            ? oldValues.filter(c => c.id !== value.id)
            : oldValues.concat([value]))

        setCheckedValues(_.uniq(newValues))

        /*let oldData = _.cloneDeep(data)
        oldData.features = (oldData.features.includes(it)
            ? oldData.features.filter(c => c !== it)
            : oldData.features.concat([it]))
        setData(s => ({
            ...oldData
        }))*/
    }


    return (
        <Box {...props}>
            {(!state.cacheFind && data) ?
                <FormControl innerref={ref} name={'editStorage'} width={1} display={'flex'} p={3}
                             onChange={() => {
                                 try {
                                     LocalStorageUtils.set(getCacheKey(), ref.current.serialize())
                                 } catch (e) {
                                 }
                             }}>
                    <Box width={'80%'} display={'flex'} flexDirection={'column'}>
                        {
                            !isModeActive ?
                                <StorageTitle storageTittle={data.title}/> :
                                <Typography variant={"h5"} pt={3} pb={1}>
                                    {data.title[siteLang]}
                                </Typography>
                        }
                        {
                            !isModeActive &&
                            <Invoice item={data.invoice}/>
                        }

                        {
                            (!isModeActive && !(isService || isPackage)) &&
                            <Dimensions item={data.dimensions}/>
                        }

                        {
                            booking_type &&
                            <Booking bookingType={booking_type} item={data.booking}/>
                        }

                        {
                            ((!isModeActive || isStoragesMode) && !(isPackage)) &&
                            <Storage isService={isService} item={data.storage}/>
                        }
                        {
                            (!isModeActive || isStoragesMode || isPricingMode) &&
                            <StorageFeatures
                                productId={productId} storageId={storageId}
                                item={data.features}
                                defaultFeatures={defaultFeatures}
                                onCheck={handleFeatureCheck} checkedValues={checkedValues}/>
                        }
                        {
                            (!isModeActive && !isService) &&
                            <Send item={data.storage}/>
                        }
                        {(!isModeActive && isPackage) &&
                        <SelectStorage
                            vip={data.vip_prices}
                            product={product}
                            packageDiscountPrice={data.package_discount_price}
                            storages={data.storages}
                            onSelectVipClick={handleVipOpen}/>}
                        {((!isModeActive || isPricingMode) && !isPackage) &&
                        <Pricing
                            box={product?.category}
                            vip={data.vip_prices}
                            item={data.pricing}
                            onVipRemove={handleVipRemove}
                            onSelectVipClick={handleVipOpen}/>
                        }
                        {
                            (!isModeActive || isPricingMode || isStoragesMode) &&
                            <MoreSetting product={product}
                                         onClose={onClose}
                                         storages={data.storages}
                                         item={data.moreData}/>
                        }
                    </Box>
                    <Box width={'20%'} pl={2}>
                        <StickyBox offsetTop={80} offsetBottom={20}>
                            <Box display={'flex'} width={1} flexDirection={'column'}>
                                <ThumbnailSelector product={product} storageMedia={defItem.media} width={1} pl={2}/>
                                <Box display={'flex'} component={Card} py={2} width={1} flexDirection={'column'}>
                                    {(defItem && defItem.storageId) &&
                                    <Box display={'flex'} justifyContent={'center'} width={1}>
                                        <FormControlLabel
                                            control={
                                                <Switch
                                                    checked={!data.disable}
                                                    onChange={handleActiveStorage}
                                                    color="primary"/>
                                            }
                                            label={(
                                                <Typography
                                                    variant={'body2'}>فعال سازی انبار</Typography>
                                            )}
                                            labelPlacement={'top'}/>
                                    </Box>
                                    }
                                    <Unavailable
                                        pt={3}
                                        unavailable={defItem.unavailable}/>
                                    <Box display={'flex'} justifyContent={'center'} p={2}>
                                        <BaseButton
                                            variant={'outlined'}
                                            onClick={handleVipOpen}
                                            style={{
                                                borderColor: cyan[300]
                                            }}>
                                            ساخت فروش ویژه جدید
                                        </BaseButton>
                                    </Box>
                                    <Box display={'flex'} alignItems={'center'}
                                         justifyContent={'center'} mt={1}>
                                        <SuccessButton
                                            loading={state.loading}
                                            onClick={() => {
                                                setTimeout(() => {
                                                    if (!getRequestProps())
                                                        return;
                                                    setState({...state, saveDialog: true})
                                                }, 1200)
                                            }}>
                                            {lang.get('save')}
                                        </SuccessButton>
                                        <BaseButton
                                            disabled={state.loading}
                                            onClick={() => setState({...state, closeDialog: true})} variant={"text"}
                                            style={{marginRight: theme.spacing(1)}}>
                                            {lang.get('close')}
                                        </BaseButton>
                                    </Box>
                                </Box>
                                {
                                    !isModeActive &&
                                    <Box width={1} pt={2}>
                                        <ButtonBase
                                            onClick={() => {
                                                const params = getRequestProps();
                                                if (params)
                                                    onChange(params, -1)
                                            }}
                                            style={{
                                                width: '100%',
                                                ...UtilsStyle.borderRadius(5)
                                            }}>
                                            <Box component={Card} width={1} display={'flex'}
                                                 alignItems={'center'}
                                                 justifyContent={'center'}
                                                 py={2}
                                                 px={1}
                                                 style={{
                                                     background: cyan[400],
                                                     border: `2px solid ${id === storageId ? cyan[300] : "transparent"}`,
                                                     ...UtilsStyle.borderRadius(5)
                                                 }}>
                                                <Add style={{color: '#fff'}}/>
                                                <Typography pr={1} variant={"h6"} fontWeight={400} color={"#fff"}>
                                                    ساخت انبار جدید
                                                </Typography>
                                            </Box>
                                        </ButtonBase>
                                    </Box>
                                }
                                {/*{
                                    product.storages.map(({storageId, storageTitle}) => (
                                        <Box key={storageId} width={1} pt={2}>
                                            {gcLog("aslfkalsklfkaslkf", {p: product.storage, storageId, storageTitle})}
                                            <RightClickMenu width={1}
                                                            items={[(
                                                                <MenuItem
                                                                    onClick={() => {
                                                                        const params = getRequestProps();
                                                                        if (params)
                                                                            onChange(params, storageId, true)
                                                                    }}>
                                                                    <Box width={1} display={'flex'}
                                                                         alignItems={'center'}>
                                                                        <IconButton
                                                                            style={{
                                                                                color: cyan[300],
                                                                                margin: theme.spacing(0.5),
                                                                            }}>
                                                                            <FileCopyOutlined
                                                                                className={"fal fa-receipt"}/>
                                                                        </IconButton>
                                                                        <Typography variant={"subtitle1"} px={1}>
                                                                            تکثیر
                                                                        </Typography>
                                                                    </Box>
                                                                </MenuItem>
                                                            )]}>
                                                <ButtonBase
                                                    onClick={() => {
                                                        const params = getRequestProps();
                                                        if (params)
                                                            onChange(params, storageId)
                                                    }}
                                                    style={{
                                                        width: '100%',
                                                        ...UtilsStyle.borderRadius(5)
                                                    }}>
                                                    <Box component={Card} width={1} display={'flex'}
                                                         py={2}
                                                         px={1}
                                                         style={{
                                                             border: `5px solid ${id === storageId ? cyan[300] : "transparent"}`,
                                                             ...UtilsStyle.borderRadius(5)
                                                         }}>
                                                        <Typography variant={"body1"} fontWeight={400}>
                                                            {storageTitle[siteLang] || "انبار جدید"}
                                                        </Typography>
                                                    </Box>
                                                </ButtonBase>
                                            </RightClickMenu>
                                        </Box>
                                    ))
                                }*/}
                            </Box>
                        </StickyBox>
                    </Box>
                    <SaveDialog open={state.saveDialog}
                                text={(
                                    <Box display={'flex'} flexDirection={'column'} px={3}>
                                        <Typography variant={'h6'} fontWeight={400} pb={2}>
                                            ذخیره انبار
                                        </Typography>
                                        <Typography variant={'body1'}>
                                            آیا از ذخیره این انبار اطمینان دارید؟
                                        </Typography>
                                        <Typography variant={'caption'} color={theme.palette.text.secondary} pt={0.5}>
                                <span style={{color: orange[300], fontWeight: 600, paddingLeft: theme.spacing(0.3)}}>
                                    توجه:
                                </span>
                                            در صورت ذخیره، اطلاعات تغییریافته قابل بازگشت نمیباشد.
                                        </Typography>
                                    </Box>)}
                                onSubmit={() => {
                                    handleSaveProduct();
                                }}
                                onCancel={() => setState({...state, saveDialog: false})}/>
                    <SaveDialog open={state.closeDialog}
                                submitText={"بله"}
                                cancelText={"خیر"}
                                text={<Typography variant={"body1"}>
                                    آیا از لغو تغییرات اطمینان دارید؟
                                </Typography>}
                                onSubmit={() => {
                                    onClose();
                                }}
                                onCancel={() => {
                                    setState({...state, closeDialog: false})
                                }}/>
                    <Backdrop open={state.loading}
                              style={{
                                  zIndex: '9999'
                              }}>
                        <CircularProgress color="inherit"
                                          style={{
                                              color: cyan[300],
                                              width: 100,
                                              height: 100
                                          }}/>
                    </Backdrop>
                </FormControl> :
                <PleaseWait/>}
            <SelectVip
                open={state.selectVip}
                onSelect={(vip) => {
                    if (vip) {
                        const newData = data;
                        if (_.findIndex(newData.vip_prices, (d) => {
                            return d.id === vip.id
                        }) !== -1) {
                            reqCancel(`گروه ${vip.name[siteLang]} از قبل انتخاب شده است.`)
                            return
                        }
                        newData.vip_prices.push({
                            ...vip,
                            max_count_for_sale: 0,
                            available_count_for_sale: 0,
                            discount_price: 0
                        })
                    }
                    setState({
                        ...state,
                        selectVip: false
                    })
                }}/>
            <CacheFindDialog
                open={state.cacheFind}
                onSubmit={handleSyncCache}
                onCancel={handleCloseCache}/>
        </Box>
    )
}

//region Components

function StorageTitle({storageTittle}) {
    return (
        <BoxContainer width={1}>
            <Box width={1} className={"enNum"}>
                <TextFieldMultiLanguageContainer
                    name={'title'}
                    renderGlobalErrorText={(index) => {
                        return "نام انبار اجباری است."
                    }}
                    render={(ref, {lang, name, initialize, valid, errorIndex, setValue, inputProps, style, props}) => {
                        return (
                            <TextField
                                {...props}
                                error={!valid}
                                name={name}
                                variant="outlined"
                                fullWidth
                                inputRef={ref}
                                helperText={errorList[errorIndex]}
                                defaultValue={storageTittle[lang.key]}
                                required={lang.key === siteLang}
                                label={"نام انبار"}
                                style={style}
                                inputProps={{
                                    ...inputProps
                                }}/>
                        )
                    }}/>
            </Box>
        </BoxContainer>
    )
}

function ThumbnailSelector({product, storageMedia: sm, ...props}) {
    const [openDialog, setOpenDialog] = useState(false);
    const [storageMedia, setStorageMedia] = useState(sm)


    return (
        <Box component={Card} p={2} mb={2} {...props}>
            <ButtonBase
                onClick={(e) => {
                    setOpenDialog(true)
                    e.stopPropagation()
                }}
                style={{
                    width: '100%',
                    ...UtilsStyle.borderRadius(5)
                }}>
                <Box display={'flex'} alignItems={'center'} justifyContent={'center'}>
                    {
                        storageMedia ?
                            <Tooltip title={"انتخاب عکس محصول"}>
                                <Box display={'flex'} flexDirection={'column'}>
                                    <Img src={storageMedia.image} imgStyle={{...UtilsStyle.borderRadius(5)}}/>
                                    <Box pt={1}>
                                        <Button variant={"outlined"}
                                                onClick={(e) => {
                                                    setStorageMedia(undefined)
                                                    e.stopPropagation()
                                                }}
                                                style={{borderColor: red[300]}}>
                                            حذف
                                        </Button>
                                    </Box>
                                </Box>
                            </Tooltip> :
                            <Typography py={2} px={1} variant={"h6"}>
                                انتخاب عکس محصول
                            </Typography>
                    }
                    <NoneTextField name={"storage_media"}
                                   defaultValue={_.isObject(storageMedia) ? storageMedia.id : ""}/>
                </Box>
            </ButtonBase>
            <BaseDialog open={openDialog} maxWidth={"lg"} style={{width: '100%'}}
                        onClose={() => {
                            setOpenDialog(false)
                        }}>
                <Box display={'flex'} flexWrap={'wrap'} width={1}>
                    {
                        product.media.map(m => (
                            <Box width={1 / 4} p={2} key={m.id}>
                                <ButtonBase
                                    onClick={() => {
                                        setOpenDialog(false)
                                        setStorageMedia(m)
                                    }}
                                    style={{
                                        width: '100%',
                                        ...UtilsStyle.borderRadius(5)
                                    }}>
                                    <Box width={1}>
                                        <Img src={m.image}/>
                                    </Box>
                                </ButtonBase>
                            </Box>
                        ))
                    }
                </Box>
            </BaseDialog>
        </Box>
    )
}

export function Unavailable({unavailable: uv, ...props}) {

    const [unavailable, setUnavailable] = useState(Boolean(uv))

    return (
        <Box display={'flex'} flexDirection={'column'} alignItems={'center'} {...props}>
            ناموجود:
            <Box pt={1}>
                <Switch
                    checked={unavailable}
                    onChange={(event) => {
                        setUnavailable(event.target.checked)
                    }}
                    name="checkedA"
                    inputProps={{'aria-label': 'secondary checkbox'}}/>
                <NoneTextField
                    name={"unavailable"}
                    defaultValue={unavailable ? "true" : "false"}/>
            </Box>
        </Box>
    )
}

//region Invoice

const Invoice = React.memo(function Invoice({item, ...props}) {
    const {title, description} = item;
    const [state, setState] = useState({
        open: false
    });
    const theme = useTheme();

    return (
        <FormContainer title={"فاکتور"} open={state.open} active={true}
                       mt={4}
                       justifyContent={'center'}
                       onOpenClick={(e, open) => {
                           setState({
                               ...state,
                               open: open
                           })
                       }}>
            <Box display={'flex'} flexDirection={'column'} alignItems={'center'} py={3}>
                <BoxContainer width={'80%'}>
                    <TextFieldMultiLanguageContainer
                        group={'invoice'}
                        name={'title'}
                        render={(ref, {
                            lang,
                            name,
                            initialize,
                            valid,
                            errorIndex,
                            setValue,
                            inputProps,
                            style,
                            props
                        }) => {
                            return (
                                <TextField
                                    {...props}
                                    name={name}
                                    variant="outlined"
                                    fullWidth
                                    inputRef={ref}
                                    helperTextIcon={true}
                                    helperText={'در صورت خالی بودن تایتل انبار جایگزین میشود.'}
                                    defaultValue={title[lang.key]===undefined?null:title[lang.key]}
                                    label={"نام محصول و انبار برای فاکتور"}
                                    style={style}
                                    inputProps={{
                                        ...inputProps
                                    }}/>
                            )
                        }}/>
                </BoxContainer>
                <BoxContainer width={1}>
                    <InvoiceDescription
                        description={description}
                        onChange={(text) => {
                            // onChange({
                            //     description: text
                            // });
                        }}
                    />
                </BoxContainer>
            </Box>
        </FormContainer>
    )
})


function InvoiceDescription({description, onChange}) {
    return (
        <BoxContainer width={'80%'} component={Card}>
            <Typography variant={'h6'} py={2} pr={2} style={{width: '100%'}}>
                توضیحات فاکتور:
            </Typography>
            <Editor
                content={description[siteLang]}
                name={"invoice_description"}/>
        </BoxContainer>
    )
}

//endregion Invoice

function SelectStorage({product, vip, item, storages = [], packageDiscountPrice = 0, onSelectVipClick, ...props}) {
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();
    const [state, setState] = useState({
        disable: false,
        open: false,
        selectStorage: false,
        newStorageDialog: false
    });


    const [products, setProducts] = useState(storages);


    let totalFinalPrice = 0
    let totalDiscountPrice = 0
    let totalStartPrice = 0

    _.forEach(products, (pr) => {
        const st = pr.defaultStorage;
        totalFinalPrice = totalFinalPrice + st.finalPrice;
        totalDiscountPrice = totalDiscountPrice + st.discountPrice;
        totalStartPrice = totalStartPrice + st.startPrice;
    })


    return (
        <FormContainer title={"انبارها"} open={state.open} active={true}
                       mt={4}
                       justifyContent={'center'}
                       onOpenClick={(e, open) => {
                           setState({
                               ...state,
                               open: open
                           })
                       }}>
            <Box display={'flex'} flexWrap={'wrap'}>
                <Box width={1} display={'flex'} pt={2} pb={1}>
                    <Typography flexDirection={"column"} alignItems={'center'} variant={"body1"} px={2}>
                        قیمت اولیه:
                        <Typography pt={1} variant={"h6"} fontWeight={500}>
                            {UtilsFormat.numberToMoney(totalStartPrice)}
                        </Typography>
                    </Typography>
                    <Typography flexDirection={"column"} alignItems={'center'} variant={"body1"} px={2}>
                        قیمت فروش:
                        <Typography pt={1} variant={"h6"} fontWeight={500}>
                            {UtilsFormat.numberToMoney(totalDiscountPrice)}
                        </Typography>
                    </Typography>
                    <Typography flexDirection={"column"} alignItems={'center'} variant={"body1"} px={2}>
                        قیمت فروش بدون تخفیف:
                        <Typography pt={1} variant={"h6"} fontWeight={500}>
                            {UtilsFormat.numberToMoney(totalFinalPrice)}
                        </Typography>
                    </Typography>
                </Box>
                <Box width={1} display={'flex'} flexWrap={'wrap'} alignItems={'flex-start'} py={1}>
                    <BaseButton
                        variant={"outlined"}
                        onClick={() => {
                            setState({...state, newStorageDialog: true})
                        }}
                        style={{
                            margin: theme.spacing(2),
                            borderColor: cyan[300]
                        }}>
                        <Box display={'flex'} alignItems={'center'}>
                            <Add style={{width: 25, height: "auto"}}/>
                            <Typography variant={"h6"} px={1}>
                                انتخاب انبار جدید
                            </Typography>
                        </Box>
                    </BaseButton>
                    <BaseButton
                        variant={"outlined"}
                        onClick={onSelectVipClick}
                        style={{
                            margin: theme.spacing(2),
                            borderColor: cyan[300]
                        }}>
                        <Box display={'flex'} alignItems={'center'}>
                            <WorkOutline style={{width: 25, height: "auto"}}/>
                            <Typography variant={"h6"} px={1}>
                                فروش ویژه
                            </Typography>
                        </Box>
                    </BaseButton>
                    <Box px={2} flex={1} display={'flex'} flexWrap={'wrap'}>
                        <Box p={2} width={1 / 2}>
                            <NewPriceTextField
                                value={packageDiscountPrice}
                                name={"package_discount_price"}
                                label="مقدار تخفیف کلی"/>
                        </Box>
                        {_.isArray(vip) && vip.map(vip => (
                            <Box key={vip.id} p={2} width={1 / 2}
                                 style={{
                                     ...UtilsStyle.disableTextSelection()
                                 }}>
                                <NewPriceTextField
                                    value={"0"}
                                    group={"vip"}
                                    array={vip.id}
                                    name={"package_discount_price"}
                                    label={(<Box display={'flex'} alignItems={'center'}>
                                        مقدار تخفیف کلی (
                                        <Box width={35} style={{position: 'relative'}}>
                                            <Icon showSkeleton={false} color={false} height={35} width={35}
                                                  src={convertIconUrl(vip.media)}
                                                  style={{
                                                      position: 'absolute',
                                                      top: -17.5
                                                  }}/>
                                        </Box>
                                        {vip.name[siteLang]})
                                    </Box>)}/>
                            </Box>
                        ))}
                    </Box>
                </Box>
                {products.map(pr => {
                    const st = pr.defaultStorage;
                    return (
                        <Box width={1 / 2} key={st.storageId} display={'flex'} px={2} py={1}>
                            <Slide direction="up" in={true} mountOnEnter unmountOnExit>
                                <Box display={'flex'} component={Card} alignItems={'center'} width={1} p={1}>
                                    <NoneTextField
                                        name={createName({
                                            group: "storages",
                                            array: st.storageId,
                                            name: 'package_item_id'
                                        })}
                                        defaultValue={st.storageId}/>
                                    <Img
                                        alt={pr.name[siteLang]}
                                        src={pr.thumbnail ? pr.thumbnail.image : ""}
                                        width={100}
                                        zoomable={true}
                                        showSkeleton={false}
                                        minHeight={1}
                                        style={{
                                            ...UtilsStyle.borderRadius(5),
                                            ...UtilsStyle.disableTextSelection()
                                        }}/>
                                    <Box display={'flex'} flexDirection={'column'} flex={1} pl={3}>
                                        <Box display={'flex'} flexDirection={'column'}>
                                            {(_.isArray(product.categories) && !_.isEmpty(product.categories)) &&
                                            <React.Fragment>
                                                <Typography variant={"body2"} pl={0.75}>
                                                    دسته‌ها:
                                                </Typography>
                                                {product.categories.map((cat, index) => (
                                                    <Typography key={cat.id} variant={"body2"} py={1}>
                                                        {cat.name[siteLang]} {product.categories.length < index - 1 && ","}
                                                    </Typography>
                                                ))}
                                            </React.Fragment>}
                                            <Link toHref={rout.Storage.Single.showStorageDashboard({productId: pr.id})}
                                                  target={"_blank"}>
                                                <Typography variant={"body1"} px={1} py={0.5}>
                                                    نام انبار: {st.storageTitle[siteLang]}
                                                </Typography>
                                            </Link>
                                            <Box display={'flex'} px={1} py={0.5}>
                                                <Typography variant={"body1"}>
                                                    قیمت اولیه:
                                                </Typography>
                                                <Typography variant={"body1"} fontWeight={500} pr={1}>
                                                    {UtilsFormat.numberToMoney(st.startPrice)}
                                                </Typography>
                                            </Box>
                                            <Box display={'flex'} px={1} py={0.5}>
                                                <Typography variant={"body1"}>
                                                    قیمت فروش:
                                                </Typography>
                                                <Typography variant={"body1"} fontWeight={500} pr={1}>
                                                    {UtilsFormat.numberToMoney(st.discountPrice)}
                                                </Typography>
                                            </Box>
                                            <Box display={'flex'} px={1} py={0.5}>
                                                <Typography variant={"body1"}>
                                                    تعداد موجود برای فروش:
                                                </Typography>
                                                <Typography variant={"body1"} fontWeight={500} pr={1}>
                                                    {st.availableCountForSale}
                                                </Typography>
                                            </Box>
                                        </Box>
                                    </Box>
                                    <Box display={'flex'} flexDirection={'column'} alignItems={'center'}
                                         justifyContent={'center'} py={1} px={1}>
                                        <Box py={0.5}>
                                            <Tooltip title={"حذف این انبار"}>
                                                <IconButton
                                                    onClick={() => {
                                                        const newProducts = products;
                                                        _.remove(newProducts, (p) => {
                                                            return p.defaultStorage.storageId === st.storageId;
                                                        });
                                                        setProducts([
                                                            ...newProducts
                                                        ]);
                                                    }}>
                                                    <HighlightOff/>
                                                </IconButton>
                                            </Tooltip>
                                        </Box>
                                        <Divider style={{width: '100%'}}/>
                                        <Box pt={1} pb={0.5}>
                                            <StorageInput
                                                group={"storages"}
                                                array={st.storageId}
                                                name={"count"}
                                                label={"تعداد"}
                                                value={st.count}
                                                noZero={true}/>
                                        </Box>
                                    </Box>
                                </Box>
                            </Slide>
                        </Box>
                    )
                })}
            </Box>
            <SearchSelectStorageDialog
                open={state.selectStorage}
                boxId={product.category.id}
                onSelect={(pr) => {
                    if (pr) {
                        const i = _.findIndex(products, (p) => {
                            return p.defaultStorage.storageId === pr.defaultStorage.storageId
                        })
                        if (i !== -1) {
                            enqueueSnackbar("این انبار از قبل انتخاب شده است.",
                                {
                                    variant: "error",
                                    action: (key) => (
                                        <Fragment>
                                            <Button onClick={() => {
                                                closeSnackbar(key)
                                            }}>
                                                {lang.get('close')}
                                            </Button>
                                        </Fragment>
                                    )
                                });
                        } else
                            setProducts([
                                ...products,
                                {
                                    ...pr,
                                    defaultStorage: {
                                        ...pr.defaultStorage,
                                        count: 1
                                    }
                                }
                            ])
                    }
                    setState({
                        ...state,
                        selectStorage: false
                    })
                }}/>
            <SaveDialog
                open={state.newStorageDialog}
                text={(
                    <Box display={'flex'} flexDirection={'column'} px={3}>
                        <Typography variant={'h6'} fontWeight={400} pb={2}>
                            انبار جدید
                        </Typography>
                        <Typography variant={'body1'}>
                            آیا از ساخت انبار جدید اطمینان دارید؟
                        </Typography>
                        <Typography variant={'caption'} color={theme.palette.text.secondary} pt={0.5}>
                                <span style={{color: orange[300], fontWeight: 600, paddingLeft: theme.spacing(0.3)}}>
                                    توجه:
                                </span>
                            در صورت ذخیره، قابل بازگشت نمیباشد.
                        </Typography>
                    </Box>)}
                onSubmit={() => setState({...state, selectStorage: true})}
                onCancel={() => setState({...state, newStorageDialog: false})}/>
        </FormContainer>
    )
}

//region storage

function Storage({item, isService, ...props}) {
    const {availableCount, availableCountForSale, maxCountForSale, minCountForSale, vipMaxCountForSale, minCountAlert} = item;
    const [state, setState] = useState({
        disable: false,
        open: false
    });
    const [availableCountData, setAvailableCountData] = useState(availableCount);

    return (
        <FormContainer title={"انبار"} open={state.open} active={true}
                       mt={4}
                       justifyContent={'center'}
                       onOpenClick={(e, open) => {
                           setState({
                               ...state,
                               open: open
                           })
                       }}>
            <Box display={'flex'} flexWrap={'wrap'}>
                {
                    !isService &&
                    <React.Fragment>
                        <StorageInput
                            px={2}
                            py={2}
                            label={'تعداد موجود'}
                            name={"availableCount"}
                            value={availableCount}
                            onChange={(e) => {
                                setAvailableCountData(e)
                            }}/>
                        <Box display={'flex'}>
                            <StorageInput
                                px={2}
                                py={2}
                                label={'تعداد موجود برای فروش'}
                                value={availableCountForSale}
                                name={"availableCountForSale"}
                                syncableData={availableCountData}/>
                        </Box>
                    </React.Fragment>
                }
                <Box display={'flex'}>
                    <StorageInput
                        px={2}
                        py={2}
                        zeroDisable={true}
                        label={'حداکثر تعداد خرید هر کاربر'}
                        value={maxCountForSale}
                        name={"maxCountForSale"}/>
                </Box>
                <Box display={'flex'}>
                    <StorageInput
                        px={2}
                        py={2}
                        zeroDisable={true}
                        label={'حداقل تعداد خرید هر کاربر'}
                        value={minCountForSale}
                        name={"minCountForSale"}/>
                </Box>
                <StorageInput
                    px={2}
                    py={2}
                    zeroDisable={true}
                    label={'هشدار اتمام انبار'}
                    name={"minCountAlert"}
                    value={minCountAlert}/>
            </Box>
        </FormContainer>
    )
}

const storagesItemStyle = makeStyles(() => ({
    storageInput: {
        width: 40,
        border: `1px solid ${grey[400]}`,
        padding: '0 !important',
        '& div[class*="MuiInputBase-root"]': {
            margin: '0 !important'
        },
        '& input': {
            textAlign: 'center'
        }
    },
    storageInputButton: {
        border: `1px solid ${cyan[400]} !important`,
        color: `${grey[600]} !important`,
        backgroundColor: grey[300],
        minWidth: 'unset',
        padding: '0 5px'
    }
}));

function StorageInput({
                          name,
                          group = 'storage',
                          array,
                          label,
                          noZero,
                          syncableData,
                          value,
                          disable: dis,
                          zeroDisable: zd,
                          maxCount,
                          onChange,
                          ...props
                      }) {

    let inputRef = null;
    const [lastData, setLastData] = useState(_.toString(value));
    const [zeroDisable, setDisableZero] = useState(_.isBoolean(zd) ? value === 0 : undefined);
    const [synced, setSynced] = useState(syncableData !== undefined ? syncableData === value : undefined);

    const classes = storagesItemStyle(props);
    const disable = dis || synced || zeroDisable;

    useEffect(() => {
        setLastData(value)
    }, [value]);

    function handleNumberButton(add) {
        try {
            inputRef.current.setValue(_.toString(_.toInteger(inputRef.current.value) + (add ? 1 : -1)))
        } catch (e) {

        }
    }

    return (
        <Box {...props}>
            <Box display={'flex'} alignItems={'center'}
                 justifyContent={'center'} flexDirection={'column'}
                 pr={(zeroDisable !== undefined || synced !== undefined) ? '65px' : 0}
                 style={{
                     position: 'relative'
                 }}>
                <Typography variant={'body1'} pb={2}>
                    {label}
                </Typography>
                <Box display={'flex'} alignItems={'center'}>
                    <ButtonGroup size="small" color="primary"
                                 aria-label="outlined primary button group"
                                 style={{
                                     ...UtilsStyle.widthFitContent()
                                 }}>
                        <Button className={classes.storageInputButton}
                                disabled={disable}
                                onClick={() => {
                                    handleNumberButton(true)
                                }}>
                            <Add fontSize="small"/>
                        </Button>
                        <TextFieldContainer
                            defaultValue={_.toString(synced ? syncableData : lastData)}
                            name={createName({group: group, name: name, array: array})}
                            errorPatterns={[noZero ? '^[1-9][0-9]*$' : '^[0-9][0-9]*$', (value, ref) => {
                                try {
                                    return ref.current.getAttribute("type") !== "number"
                                } catch (e) {
                                }
                            }, (value) => {
                                try {
                                    return _.toNumber(value) > maxCount
                                } catch (e) {
                                }
                            }]}
                            renderGlobalErrorText={() => {
                                return 'فیلد "' + label + '" مشکل دارد.'
                            }}
                            actived={!(dis || zeroDisable)}
                            type={'number'}
                            onChange={(e) => {
                                try {
                                    const el = inputRef.current;
                                    if (el.value.match("^0.+")) {
                                        el.setValue(el.value.replace("0", ""));
                                        return
                                    }
                                } catch (e) {
                                }
                                if (onChange)
                                    onChange(e)
                            }}
                            render={(ref, {name, initialize, valid, errorIndex, inputProps, setValue, props}) => {
                                inputRef = ref;
                                return (
                                    <TextField
                                        {...props}
                                        error={!valid}
                                        name={name}
                                        fullWidth
                                        disabled={disable}
                                        inputRef={ref}
                                        required={!disable}
                                        label=""
                                        inputProps={{
                                            ...inputProps,
                                            style: {
                                                textAlign: 'center',
                                            }
                                        }}
                                        style={{
                                            width: '60px',
                                        }}/>
                                )
                            }}/>
                        <Button className={classes.storageInputButton}
                                disabled={disable}
                                onClick={() => {
                                    handleNumberButton(false)
                                }}>
                            <Remove fontSize="small"/>
                        </Button>
                    </ButtonGroup>
                    {synced !== undefined &&
                    <Box display={'flex'} ml={1}
                         width={60}
                         style={{
                             position: 'absolute',
                             left: 0
                         }}>
                        <Tooltip title={(synced ? 'غیر' : '') + "فعال کردن همگامسازی با انبار"}>
                            <IconButton
                                onClick={() => {
                                    if (!synced)
                                        setLastData(inputRef.current.value);
                                    setSynced(!synced)
                                }}>
                                {synced ?
                                    <SyncDisabled/> :
                                    <Sync/>
                                }
                            </IconButton>
                        </Tooltip>
                    </Box>
                    }
                    {
                        _.isBoolean(zeroDisable) &&
                        <Box display={'flex'} ml={1}
                             width={60}
                             style={{
                                 position: 'absolute',
                                 left: 0
                             }}>
                            <Tooltip title={zeroDisable ? "فعال سازی" : "غیرفعال سازی"}>
                                <IconButton
                                    onClick={() => {
                                        setDisableZero(!zeroDisable);
                                    }}>
                                    {zeroDisable ?
                                        <AllInclusive/> :
                                        <SyncDisabled/>
                                    }
                                </IconButton>
                            </Tooltip>
                        </Box>
                    }
                </Box>
            </Box>
        </Box>)
}

//endregion storage

//region Dimensions

const DimensionsGp = "dimensions"

function Dimensions({item, ...props}) {

    const [weight, setWeight] = useState(0)
    const [height, setHeight] = useState(0)
    const [width, setWidth] = useState(0)
    const [length, setLength] = useState(0)

    useEffect(() => {
        if (item.weight !== undefined)
            setWeight(item.weight)
        if (item.height !== undefined)
            setHeight(item.height)
        if (item.width !== undefined)
            setWidth(item.width)
        if (item.length !== undefined)
            setLength(item.length)
    }, [item]);

    const [state, setState] = useState({
        disable: false,
        open: false
    });


    return (
        <FormContainer title={"ابعاد و وزن"} open={state.open} active={true}
                       mt={4}
                       justifyContent={'center'}
                       onOpenClick={(e, open) => {
                           setState({
                               ...state,
                               open: open
                           })
                       }}>
            <Box display={'flex'} alignItems={'center'} px={2} p={3}>
                <Box display={'flex'} flexWrap={'wrap'} width={2 / 5} p={2}>
                    <Box minWidth={1 / 2} p={1}>
                        <Box display={'flex'} my={1} p={1}
                             alignItems={'center'}
                             style={{
                                 border: `1px solid ${cyan[300]}`,
                                 ...UtilsStyle.borderRadius(5)
                             }}>
                            <Box minWidth={65} pr={2} display={'flex'} flexDirection={'column'} alignItems={'center'}
                                 justifyContent={'center'}>
                                <FitnessCenter/>
                                <Typography variant={"h6"}>
                                    وزن
                                </Typography>
                            </Box>
                            <Box display={'flex'} alignItems={'flex-start'} flexDirection={'column'} pr={2}>
                                <Typography variant={"body1"} pb={1}
                                            style={{
                                                direction: 'ltr'
                                            }}>
                                    {weight} g
                                </Typography>
                                <Typography variant={"body1"} pt={1}
                                            style={{
                                                direction: 'ltr'
                                            }}>
                                    {weight / 1000} kg
                                </Typography>
                            </Box>
                        </Box>
                    </Box>
                    <Box minWidth={1 / 2} p={1}>
                        <Box display={'flex'} my={1} p={1} alignItems={'center'}
                             style={{
                                 border: `1px solid ${cyan[300]}`,
                                 ...UtilsStyle.borderRadius(5)
                             }}>
                            <Box minWidth={65} pr={2} display={'flex'} flexDirection={'column'} alignItems={'center'}
                                 justifyContent={'center'}>
                                <TextRotationAngleup/>
                                <Typography variant={"h6"}>
                                    ارتفاع
                                </Typography>
                            </Box>
                            <Box display={'flex'} alignItems={'flex-start'} flexDirection={'column'} pr={2}>
                                <Typography variant={"body1"} pb={1}
                                            style={{
                                                direction: 'ltr'
                                            }}>
                                    {height} cm
                                </Typography>
                                <Typography variant={"body1"} pt={1}
                                            style={{
                                                direction: 'ltr'
                                            }}>
                                    {height / 100} m
                                </Typography>
                            </Box>
                        </Box>
                    </Box>
                    <Box minWidth={1 / 2} p={1}>
                        <Box display={'flex'} my={1} p={1} alignItems={'center'}
                             style={{
                                 border: `1px solid ${cyan[300]}`,
                                 ...UtilsStyle.borderRadius(5)
                             }}>
                            <Box minWidth={65} pr={2} display={'flex'} flexDirection={'column'} alignItems={'center'}
                                 justifyContent={'center'}>
                                <TextRotationNone/>
                                <Typography variant={"h6"}>
                                    طول
                                </Typography>
                            </Box>
                            <Box display={'flex'} alignItems={'flex-start'} flexDirection={'column'} pr={2}>
                                <Typography variant={"body1"} pb={1}
                                            style={{
                                                direction: 'ltr'
                                            }}>
                                    {length} cm
                                </Typography>
                                <Typography variant={"body1"} pt={1}
                                            style={{
                                                direction: 'ltr'
                                            }}>
                                    {length / 100} m
                                </Typography>
                            </Box>
                        </Box>
                    </Box>
                    <Box minWidth={1 / 2} p={1}>
                        <Box display={'flex'} my={1} p={1} alignItems={'center'}
                             style={{
                                 border: `1px solid ${cyan[300]}`,
                                 ...UtilsStyle.borderRadius(5)
                             }}>
                            <Box minWidth={65} pr={2} display={'flex'} flexDirection={'column'} alignItems={'center'}
                                 justifyContent={'center'}>
                                <TextRotateVertical/>
                                <Typography variant={"h6"}>
                                    عرض
                                </Typography>
                            </Box>
                            <Box display={'flex'} alignItems={'flex-start'} flexDirection={'column'} pr={2}>
                                <Typography variant={"body1"} pb={1}
                                            style={{
                                                direction: 'ltr'
                                            }}>
                                    {width} cm
                                </Typography>
                                <Typography variant={"body1"} pt={1}
                                            style={{
                                                direction: 'ltr'
                                            }}>
                                    {width / 100} m
                                </Typography>
                            </Box>
                        </Box>
                    </Box>
                </Box>
                <Box display={'flex'} flexWrap={'wrap'} flex={1}>
                    <Box display={'flex'} width={1 / 2} p={1}>
                        <DimensionsTextField
                            label={"وزن g"}
                            name={createName({group: DimensionsGp, name: "weight"})}
                            value={item.weight}
                            onChange={(v) => {
                                setWeight(v);
                            }}/>
                    </Box>
                    <Box display={'flex'} width={1 / 2} p={1}>
                        <LengthTextField
                            label={"ارتفاع cm"}
                            name={createName({group: DimensionsGp, name: "height"})}
                            value={item.height}
                            onChange={(v) => {
                                setHeight(v);
                            }}/>
                    </Box>
                    <Box display={'flex'} width={1 / 2} p={1}>
                        <LengthTextField
                            label={"طول cm"}
                            name={createName({group: DimensionsGp, name: "length"})}
                            value={item.length}
                            onChange={(v) => {
                                setLength(v);
                            }}/>
                    </Box>
                    <Box display={'flex'} width={1 / 2} p={1}>
                        <LengthTextField
                            label={"عرض cm"}
                            name={createName({group: DimensionsGp, name: "width"})}
                            value={item.width}
                            onChange={(v) => {
                                setWidth(v);
                            }}/>
                    </Box>
                </Box>
            </Box>
        </FormContainer>
    )
}

//endregion Dimensions

//region Pricing

export function Pricing({
                            box,
                            open = false,
                            item,
                            taxShow = true,
                            ratioShow = true,
                            shippingCostShow = true,
                            vip,
                            onSelectVipClick,
                            onVipRemove,
                            ...props
                        }) {
    const theme = useTheme();
    const [data, setData] = useState({
        startPrice: item.startPrice,
        finalPrice: item.finalPrice,
        discountPrice: item.discountPrice,
        shipping_cost: item.shipping_cost,
        taxData: item.tax
    });

    useEffect(() => {
        setData({
            startPrice: item.startPrice,
            finalPrice: item.finalPrice,
            discountPrice: item.discountPrice,
            shipping_cost: item.shipping_cost,
            taxData: item.tax
        })
    }, [item]);

    const [startPrice, setStartPrice] = useState(item.startPrice);
    const [finalPrice, setFinalPrice] = useState(item.finalPrice);
    const [discountPrice, setDiscountPrice] = useState(item.discountPrice);
    const [shipping_cost, set_shipping_cost] = useState(item.shipping_cost);
    const [taxData, setTaxData] = useState(item.tax);

    const [state, setState] = useState({
        disable: false,
        open: open
    });

    useEffect(() => {
        if (taxData.value === TaxValue.hasNot || taxData.value === TaxValue.fromProfit || taxData.value === TaxValue.fromTotalPrice)
            return;
        setTaxData(convertTax())
    }, [taxData]);


    function getDiscountTax(dis = discountPrice) {
        if (taxData.hasNot)
            return 0;
        return Math.round(dis - Math.round(dis / 1.09))
    }

    function handleTaxChange(value) {
        if (state.disable)
            return;
        setTaxData(convertTax(value));
    }


    return (
        <React.Fragment>
            <FormContainer title={"قیمت گذاری"} open={state.open} active={true}
                           mt={4}
                           justifyContent={'center'}
                           onOpenClick={(e, open) => {
                               setState({
                                   ...state,
                                   open: open
                               })
                           }}>
                <Box display={'flex'} flexDirection={'column'} alignItems={'center'} py={3}>
                    {
                        taxShow &&
                        <Box display={'flex'} flexWrap={'wrap'} width={'80%'}>
                            <Box display={'flex'} alignItems={'center'} pt={2} pr={2}>
                                <BaseButton variant={"outlined"} onClick={onSelectVipClick} style={{
                                    paddingTop: theme.spacing(1),
                                    paddingBottom: theme.spacing(1),
                                    borderColor: cyan[300]
                                }}>
                                    فروش ویژه
                                </BaseButton>
                            </Box>
                            <Box display={'flex'} alignItems={'center'} pt={2} px={1}
                                 style={{
                                     border: `2px solid ${cyan[300]}`,
                                     ...UtilsStyle.borderRadius(5)
                                 }}>
                                <Typography variant={'body1'} pr={2} alignItems={'center'}>
                                    <AccountBalance
                                        style={{
                                            color: cyan[300],
                                            marginLeft: theme.spacing(1)
                                        }}/>
                                    مالیات بر ارزش افزوده:
                                </Typography>
                                <RadioGroup aria-label="gender" name={createName({group: 'pricing', name: 'tax'})}
                                            value={taxData.value}>
                                    <Box display={'flex'} flexWrap={'wrap'}>
                                        <Typography variant={'body2'} display={'flex'} alignItems={'center'} mr={2}
                                                    onClick={() => handleTaxChange(TaxValue.hasNot)}
                                                    style={{
                                                        cursor: 'pointer'
                                                    }}>
                                            ندارد
                                            <Radio
                                                disabled={state.disable}
                                                checked={taxData.hasNot}
                                                value={TaxValue.hasNot}/>
                                        </Typography>
                                        <Typography variant={'body2'} display={'flex'}
                                                    alignItems={'center'} mr={2}
                                                    onClick={() => handleTaxChange(TaxValue.fromTotalPrice)}
                                                    style={{
                                                        cursor: 'pointer'
                                                    }}>
                                            از کل محصول
                                            <Radio
                                                disabled={state.disable}
                                                checked={taxData.fromTotalPrice}
                                                value={TaxValue.fromTotalPrice}/>
                                        </Typography>
                                        <Typography variant={'body2'} display={'flex'}
                                                    alignItems={'center'} mr={2}
                                                    onClick={() => {
                                                        handleTaxChange(TaxValue.fromProfit)
                                                    }}
                                                    style={{
                                                        cursor: 'default'
                                                    }}>
                                            فقط از سود
                                            <Radio
                                                checked={taxData.fromProfit}
                                                value={TaxValue.fromProfit}/>
                                        </Typography>
                                    </Box>
                                </RadioGroup>
                            </Box>
                        </Box>
                    }
                    <BoxContainer width={'80%'} display={'flex'} flexDirection={'column'}>
                        <Box display={'flex'} width={1} alignItems={'center'} justifyContent={'center'}>
                            <Box display={'flex'} width={1 / 2}>
                                <ReviewPrice
                                    boxId={box.id}
                                    vip={state.vip}
                                    finalPrice={finalPrice}
                                    startPrice={startPrice}
                                    discountPrice={discountPrice}
                                    shippingCost={shipping_cost}
                                    taxData={taxData}/>
                            </Box>
                            <Box display={'flex'} flex={1} flexDirection={'column'}>
                                <Box width={1} px={2} my={1}>
                                    <NewPriceTextField
                                        value={data.startPrice}
                                        group={"pricing"}
                                        name={"startPrice"}
                                        label="قیمت اولیه"
                                        onChange={(e, el) => {
                                            setStartPrice(e);
                                        }}/>
                                </Box>
                                <Box width={1} px={2} my={1}>
                                    <NewPriceTextField
                                        value={data.finalPrice}
                                        group={"pricing"}
                                        name={"finalPrice"}
                                        label="قیمت پایانی بدون تخفیف"
                                        onChange={(e) => {
                                            setFinalPrice(e);
                                        }}/>
                                </Box>
                                <Box width={1} px={2} my={1}>
                                    <NewPriceTextField
                                        value={data.discountPrice}
                                        group={"pricing"}
                                        name={"discountPrice"}
                                        label="قیمت فروش"
                                        onChange={(e) => {
                                            setDiscountPrice(e);
                                        }}/>
                                </Box>
                                {
                                    shippingCostShow &&
                                    <Box width={1} px={2} my={1}>
                                        <NewPriceTextField
                                            value={data.shipping_cost}
                                            group={"pricing"}
                                            name={"shipping_cost"}
                                            label="هزینه ارسال محصول"
                                            onChange={(e) => {
                                                set_shipping_cost(e);
                                            }}/>
                                        <Typography pt={2} variant={'body2'}>
                                            در صورتی که هیچ مقداری وارد نکنید هزینه ارسال بشکل سیستماتیک محاسبه میشود.
                                            <br/>
                                            درصورت وارد کردن قیمت هزینه ارسال رونوشت میشود.
                                        </Typography>
                                    </Box>
                                }
                            </Box>
                        </Box>
                    </BoxContainer>
                    {
                        ratioShow &&
                        <Box width={"80%"} mt={2} mb={1} p={2}
                             style={{
                                 border: `1px solid ${cyan[300]}`,
                                 ...UtilsStyle.borderRadius(5)
                             }}>
                            <Box display={'flex'} alignItems={'center'} pt={2} pl={1}>
                                <Box width={150}>
                                    <DefaultTextField
                                        defaultValue={_.toString(item.ratio)}
                                        label={"ضریب"}
                                        type={"number"}
                                        name={createName({group: "pricing", name: "ratio"})}
                                        variant={"outlined"}/>
                                </Box>
                                <Box width={250} pl={2}>
                                    <NewPriceTextField
                                        label={"مقدار تخفیف در ضریب گروهی"}
                                        value={_.toString(item.ratio_discount)}
                                        group={"pricing"}
                                        name={"ratio_discount"}
                                        variant={"outlined"}/>
                                </Box>
                            </Box>
                        </Box>
                    }
                </Box>
            </FormContainer>
            {
                vip && vip.map(v => (
                    <Vip key={v.id} item={v}
                         startPrice={startPrice}
                         finalPrice={finalPrice}
                         shipping_cost={shipping_cost}
                         taxData={taxData}
                         getDiscountTax={getDiscountTax}
                         onRemove={onVipRemove}/>
                ))
            }
        </React.Fragment>
    )
}

function ReviewPrice({
                         boxId,
                         vip,
                         finalPrice,
                         startPrice,
                         discountPrice,
                         shippingCost,
                         taxData
                     }) {

    const [data, setData] = useState();
    const delayedQuery = useCallback(_.debounce(() => {
        if (startPrice===0,
            discountPrice===0,
            finalPrice===0) {
                return
        }
        ControllerProduct.Storage.reviewPrice({
            boxId,
            startPrice,
            discountPrice,
            finalPrice,
            shippingCost,
            taxType: taxData.value
        }).then(res => {
            setData(res.data.data)
        })
    }, 2500), [finalPrice, startPrice, discountPrice, shippingCost, taxData]);

    useEffect(() => {
        setData(undefined)
        delayedQuery()
        return delayedQuery.cancel;
    }, [finalPrice, startPrice, discountPrice, shippingCost, taxData])


    return (
        <BoxWithTopBorder display={'flex'}
                          width={1}
                          borderStork={''}
                          component={"div"}
                          px={2}
                          title={null}
                          flexDirection={'column'}
                          style={{
                              border: vip ? `1px solid ${cyan[400]}` : null,
                          }}>
            {
                data ?
                    <React.Fragment>
                        {
                            isNumeric(data.discount_percent) &&
                            <Typography variant={'body1'} py={1} alignItems={'center'}>
                                درصد تخفیف:
                                <Typography dir={'ltr'} variant={'h6'} mr={1}
                                            color={data.discount_percent < 1 ? red[700] : green[600]}>
                                    {data.discount_percent}%
                                </Typography>
                            </Typography>
                        }
                        {
                            isNumeric(data.discount_percent) &&
                            <Typography variant={'body1'} py={1} alignItems={'center'}>
                                قیمت پایانی محصول:
                                <Typography dir={'ltr'} variant={'h6'} mr={1}
                                            color={data.discount_percent < 1 ? red[700] : green[600]}>
                                    {UtilsFormat.numberToMoney(data.discount_price)}
                                </Typography>
                            </Typography>
                        }
                        {
                            isNumeric(data.tax) &&
                            <Typography variant={'body1'} py={1} alignItems={'center'}>
                                مقدار ارزش افزوده:
                                <Typography dir={'ltr'} variant={'h6'} mr={1}
                                            color={(data.tax < 1 && !taxData.hasNot) ? red[700] : green[600]}>
                                    {UtilsFormat.numberToMoney(data.tax)}
                                </Typography>
                            </Typography>
                        }
                        {
                            isNumeric(data.profit) &&
                            <Typography variant={'body1'} py={1} alignItems={'center'}>
                                مقدار سود از فروش:
                                <Typography dir={'ltr'} variant={'h6'} mr={1} fontWeight={600}
                                            color={data.profit < 1 ? red[700] : green[600]}>
                                    {UtilsFormat.numberToMoney(data.profit)}
                                </Typography>
                            </Typography>
                        }
                        {
                            isNumeric(data.charity) &&
                            <Typography variant={'body1'} py={1} alignItems={'center'}>
                                سهم خیریه:
                                <Typography dir={'ltr'} variant={'h6'} mr={1}>
                                    {UtilsFormat.numberToMoney(data.charity)}
                                </Typography>
                            </Typography>
                        }
                        {
                            isNumeric(data.admin) &&
                            <Typography variant={'body1'} py={1} alignItems={'center'}>
                                سهم مدیر:
                                <Typography dir={'ltr'} variant={'h6'} mr={1}>
                                    {UtilsFormat.numberToMoney(data.admin)}
                                </Typography>
                            </Typography>
                        }
                        {
                            isNumeric(data.dev) &&
                            <Typography variant={'body1'} py={1} alignItems={'center'}>
                                سهم برنامه‌نویس:
                                <Typography dir={'ltr'} variant={'h6'} mr={1}>
                                    {UtilsFormat.numberToMoney(data.dev)}
                                </Typography>
                            </Typography>
                        }
                        {
                            isNumeric(data.mt_profit) &&
                            <Typography variant={'body1'} py={1} alignItems={'center'}>
                                سهم مهرتخفیف:
                                <Typography dir={'ltr'} variant={'h6'} mr={1}>
                                    {UtilsFormat.numberToMoney(data.mt_profit)}
                                </Typography>
                            </Typography>
                        }
                        {
                            (isNumeric(data.shipping_cost) && isNumeric(data.discount_price)) &&
                            <Typography variant={'body1'} py={1} alignItems={'center'}>
                                قیمت پرداختی کاربر:
                                <Typography dir={'ltr'} variant={'h6'} mr={1} fontWeight={600}
                                            color={(data.shipping_cost + data.discount_price) < 1 ? red[700] : green[600]}>
                                    {UtilsFormat.numberToMoney((data.shipping_cost + data.discount_price))}
                                </Typography>
                            </Typography>
                        }
                    </React.Fragment> :
                    <Box display={'flex'} width={1} alignItems={'center'} justifyContent={'center'}>
                        <CircularProgress color="inherit"
                                          style={{
                                              color: cyan[300],
                                              width: 100,
                                              height: 100
                                          }}/>
                    </Box>
            }
        </BoxWithTopBorder>
    )
}


//endregion Pricing

//region Booking

function Booking({bookingType, item, ...props}) {

    const [open, setOpen] = useState(false)
    return (
        <FormContainer
            title={(
                <Box display={'flex'} alignItems={'center'}>
                    <Typography variant={'h6'}>
                        رزرو
                    </Typography>
                </Box>)}
            open={open}
            active={true}
            mt={4} justifyContent={'center'}
            onOpenClick={(e, open) => {
                setOpen(open)
            }}>
            <Box width={1} display={'flex'} py={3}>
                <Box width={0.5} px={2}>
                    <DefaultTextField
                        label={"حداقل زمان رزرو (ساعت)"}
                        variant={"outlined"}
                        type={"number"}
                        name={createName({group: "booking", name: "least_booking_time"})}
                        defaultValue={item.least_booking_time}/>
                </Box>
                <Box width={0.5} px={2}>
                    <PriceTextField
                        label={"هزینه اضافه رزرو"}
                        variant={"outlined"}
                        group={"booking"}
                        name={"booking_cost"}
                        value={item.booking_cost}/>
                </Box>
            </Box>
        </FormContainer>
    )
}

//endregion Booking

//region Vip
function Vip({item, startPrice, finalPrice, shipping_cost, taxData, getDiscountTax, onRemove, ...props}) {
    const [state, setState] = useState({
        disable: false,
        open: false,
        remove: false
    });
    const [discountPrice, setDiscountPrice] = useState(item.discount_price)

    const discount = finalPrice - discountPrice;
    const discountPercent = Math.round(100 - ((discountPrice / finalPrice) * 100)) || 0;
    const tax = getDiscountTax(discountPrice);
    const profit = discountPrice - startPrice - tax;
    const purchasePrice = discountPrice + shipping_cost;
    return (
        <FormContainer
            title={(
                <Box display={'flex'} alignItems={'center'}>
                    <Icon showSkeleton={false} color={false} height={50} width={50} src={convertIconUrl(item.media)}/>
                    <Typography variant={'h6'}>
                        {item.name[siteLang]}
                    </Typography>
                </Box>)}
            open={state.open} active={true}
            mt={4} justifyContent={'center'}
            onOpenClick={(e, open) => {
                setState({
                    ...state,
                    open: open
                })
            }}>
            <Box display={'flex'}>
                <Box display={'flex'} width={1 / 2} justifyContent={'center'} alignItems={'center'}
                     flexDirection={'column'}>
                    <BoxWithTopBorder display={'flex'} maxWidth={1 / 2}
                                      borderStork={''}
                                      component={"div"}
                                      px={2}
                                      my={2}
                                      justifyContent={'center'}
                                      alignItems={'center'}
                                      title={null}
                                      flexDirection={'column'}
                                      style={{
                                          border: `1px solid ${cyan[400]}`,
                                          ...UtilsStyle.borderRadius(5)
                                      }}>
                        <Typography variant={'body1'} py={1} alignItems={'center'}>
                            درصد تخفیف:
                            <Typography dir={'ltr'} variant={'h6'} mr={1}
                                        color={discountPercent < 1 ? red[700] : green[600]}>
                                {discountPercent}%
                            </Typography>
                        </Typography>
                        <Typography variant={'body1'} py={1} alignItems={'center'}>
                            مقدار تخفیف:
                            <Typography dir={'ltr'} variant={'h6'} mr={1}
                                        color={discountPercent < 1 ? red[700] : green[600]}>
                                {UtilsFormat.numberToMoney(discount)}
                            </Typography>
                        </Typography>
                        <Typography variant={'body1'} py={1} alignItems={'center'}>
                            مقدار ارزش افزوده:
                            <Typography dir={'ltr'} variant={'h6'} mr={1}
                                        color={(tax < 1 && !taxData.hasNot) ? red[700] : green[600]}>
                                {UtilsFormat.numberToMoney(tax)}
                            </Typography>
                        </Typography>
                        <Typography variant={'body1'} py={1} alignItems={'center'}>
                            مقدار سود از فروش:
                            <Typography dir={'ltr'} variant={'h6'} mr={1} fontWeight={600}
                                        color={profit < 1 ? red[700] : green[600]}>
                                {UtilsFormat.numberToMoney(profit)}
                            </Typography>
                        </Typography>
                        <Typography variant={'body1'} py={1} alignItems={'center'}>
                            قیمت پرداختی کاربر:
                            <Typography dir={'ltr'} variant={'h6'} mr={1} fontWeight={600}
                                        color={purchasePrice < 1 ? red[700] : green[600]}>
                                {UtilsFormat.numberToMoney(purchasePrice)}
                            </Typography>
                        </Typography>
                    </BoxWithTopBorder>
                    <Box mt={2}>
                        <BaseButton
                            variant={"outlined"}
                            onClick={() => {
                                setState({
                                    ...state,
                                    remove: true
                                })
                            }}
                            style={{borderColor: red[300]}}>
                            <Typography variant={"body1"} alignItems={'center'}>
                                <Delete style={{color: red[400], marginLeft: theme.spacing(1)}}/>حذف
                            </Typography>
                        </BaseButton>
                    </Box>
                </Box>
                <Box display={'flex'} flex={1} flexDirection={'column'}
                     justifyContent={'center'} alignItems={'center'}
                     py={3}>
                    <NoneTextField name={createName({group: "vip", array: item.id, name: 'name'})}
                                   defaultValue={item.name[siteLang]}/>
                    <Box width={1} px={2} my={1} display={'flex'} justifyContent={'center'}>
                        <StorageInput
                            px={2}
                            py={2}
                            zeroDisable={true}
                            label={`تعداد موجود برای فروش ${item.name[siteLang]}`}
                            group={"vip"}
                            array={item.id}
                            name={"available_count_for_sale"}
                            value={item.available_count_for_sale}/>
                    </Box>
                    <Box width={1} px={2} my={1} display={'flex'} justifyContent={'center'}>
                        <StorageInput
                            px={2}
                            py={2}
                            zeroDisable={true}
                            label={`حداکثر تعداد خرید هر کاربر ${item.name[siteLang]}`}
                            group={"vip"}
                            array={item.id}
                            name={"max_count_for_sale"}
                            value={item.max_count_for_sale}/>
                    </Box>
                    <Box width={1} px={2} my={1}>
                        <NewPriceTextField
                            value={item.discount_price}
                            group={"vip"}
                            array={item.id}
                            name={"discount_price"}
                            label={`قیمت فروش ${item.name[siteLang]}`}
                            onChange={(e) => {
                                setDiscountPrice(e);
                            }}/>
                    </Box>
                </Box>
            </Box>
            <SaveDialog open={state.remove}
                        submitText={"بله"}
                        cancelText={"خیر"}
                        text={<Typography variant={"body1"}>
                            آیا از حذف فروش ویژه برای
                            {" " + item.name[siteLang] + " "}
                            اطمینان دارید؟
                        </Typography>}
                        onSubmit={() => {
                            onRemove(item)
                            setState({...state, remove: false})
                        }}
                        onCancel={() => {
                            setState({...state, remove: false})
                        }}/>
        </FormContainer>
    )
}

//endregion Vip

// region Send
function Send({item: it, ...props}) {
    //TODO: bullshit send

    const theme = useTheme();
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();

    const [state, setState] = useState({
        open: false,
        selectFeatures: false,
        removeDialog: false,
    });
    const [item, setItem] = useState([]);
    const [maxShippingTime, setMaxShippingTime] = useState(it.maxShippingTime);

    useEffect(() => {
        setItem(it);
    }, [it]);
    return (

        <FormContainer title={"ارسال"} open={state.open} active={true}
                       mt={4}
                       justifyContent={'center'}
                       onOpenClick={(e, open) => {
                           setState({
                               ...state,
                               open: open
                           })
                       }}>
            <Box display={'flex'} flexDirection={'column'} alignItems={'center'} py={3}>

                <Box display={'flex'} width={'80%'}>
                    <Typography variant={"h6"} style={{alignSelf: "center"}}> زمان ارسال محصول به ساعت : </Typography>
                    <TextField
                        name={createName({group: "send", name: "maxShippingTime"})}
                        style={{width: 50, marginRight: 20, marginLeft: 60}}
                        onChange={(e, value) => {
                            setMaxShippingTime(value)
                        }}
                        value={maxShippingTime}
                        inputProps={{
                            style: {
                                textAlign: 'center',
                                direction: 'ltr'
                            }
                        }}
                        type={"number"}
                    />
                </Box>
                <Box width={1} px={3} pt={2}>

                </Box>
            </Box>
            <SaveDialog open={Boolean(state.removeDialog)}
                        text={(
                            state.removeDialog &&
                            <Box display={'flex'} flexDirection={'column'} p={1}>
                                <Box display={'flex'} pb={2}>
                                    <Icon width={45} showSkeleton={false} minHeight={45}
                                          alt={state.removeDialog.iconName}
                                          src={state.removeDialog.iconLink}/>
                                    <Typography variant={'body1'} fontWeight={500} p={1}>
                                        {state.removeDialog.name[siteLang]}
                                    </Typography>
                                </Box>
                                <Typography variant={'h6'}>
                                    آیا از حذف این فیچر اطمینان دارید؟
                                </Typography>
                            </Box>
                        )}
                        submitText={"بله اطمینان دارم"}
                        cancelText={"خیر"}
                        onSubmit={() => {
                            const newItem = item;
                            _.remove(newItem, (i) => {
                                return i.id === state.removeDialog.id;
                            });
                            setItem([...newItem]);
                            setState({
                                ...state,
                                removeDialog: false
                            })
                        }}
                        onCancel={() => {
                            setState({
                                ...state,
                                removeDialog: false
                            })
                        }}/>
        </FormContainer>
    )
}

//endregion Send

//region MoreSetting
function MoreSetting({product, item, storages, onClose, ...props}) {
    const {isPackage} = product.type;

    const {supplier, gender: ge, startTime: strTime, deadline: deadLi} = item;

    const [state, setState] = useState({
        open: false,
    });
    const [gender, setGender] = useState(ge);
    const [startTime, setStartTime] = useState(strTime || undefined);
    const [deadline, setDeadline] = useState(deadLi);
    useEffect(() => {
        setGender(ge);
    }, [ge]);
    useEffect(() => {
        if (Date.now() / 1000 > strTime) {
            // setStartTime(Date.now() / 1000);
            return;
        }
        // setStartTime(strTime);
    }, [strTime]);

    useEffect(() => {
        if (Date.now() / 1000 > deadLi) {
            setDeadline(null);
            return;
        }
        setDeadline(deadLi);
    }, [deadLi]);

    function onChange() {

    }

    return (
        <FormContainer title={"تنظیمات دیگر"} open={state.open} active={true}
                       mt={4}
                       onOpenClick={(e, open) => {
                           setState({
                               ...state,
                               open: open
                           })
                       }}
                       style={{
                           overflow: 'visible'
                       }}>
            <Box
                display={'flex'}
                width={1}>
                <Box display={'flex'} width={1 / 3} flexDirection={'column'} p={2}>
                    <Supplier isPackage={isPackage} onClose={onClose} storages={storages} item={supplier}/>
                    {
                        isPackage &&
                        <Typography variant={"body2"}>
                            <Typography component={"span"} color={red[500]}>
                                توجه:
                            </Typography>
                            در صورت نیاز به تغییر تامین کننده تمام تنظیمات انبار را انجام دهید یکبار ذخیره کنید و بعد از
                            ذخیره بازگردید و مستقیم تامین کننده را تغییر دهید.
                        </Typography>
                    }
                </Box>
                <Box display={'flex'} p={2}>
                    <MaterialFormControl style={{minWidth: 250}}>
                        <NoneTextField
                            name={createName({group: 'moreData', name: 'gender'})}
                            defaultValue={_.toString(gender.value)}/>
                        <InputLabel id="gender-select-label"><Typography variant={'body1'}>جنسیت مشتریان این
                            محصول</Typography></InputLabel>
                        <Select
                            labelId="انتخاب جنسیت"
                            id="gender-select"
                            value={gender.value === null ? -1 : gender.value}
                            onChange={(e) => {
                                const val = e.target.value === -1 ? null : e.target.value;
                                setGender(convertGender(val, "همه"))
                            }}>
                            <MenuItem value={-1}><Typography variant={'body2'}>همه</Typography></MenuItem>
                            <MenuItem value={true}><Typography variant={'body2'}>مرد</Typography></MenuItem>
                            <MenuItem value={false}><Typography variant={'body2'}>زن</Typography></MenuItem>
                        </Select>
                    </MaterialFormControl>
                </Box>
                <DateSelector
                    label={"تاریخ شروع:"}
                    date={startTime}
                    name={createName({group: 'moreData', name: "startTime"})}
                    minimumDate={Date.now() / 1000}
                    onChange={(date) => {
                        setStartTime(date);
                        if (deadline > date)
                            return;
                        setDeadline(null)
                    }}/>
                <DateSelector label={"تاریخ پایان:"}
                              minimumDate={startTime || Date.now() / 1000}
                              name={createName({group: 'moreData', name: "deadline"})}
                              date={deadline}
                              onChange={(date) => {
                                  setDeadline(date);
                              }}/>
            </Box>
        </FormContainer>
    )
}

function DateSelector({label, name, date, minimumDate, maximumDate, onChange, ...props}) {
    if (!minimumDate)
        minimumDate = moment().add(1, "day").unix()
    const [open, setOpen] = useState(false);
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();
    return (
        <Box display={'flex'} flexDirection={'column'} px={2} py={2} {...props}>
            <NoneTextField
                name={name}
                defaultValue={_.toString(date)}/>
            <Typography variant={"body1"} alignItems={'center'} fontWeight={400} pb={1}>
                {label}
                {date &&
                <Tooltip title={"حذف"}>
                    <IconButton
                        size={"small"}
                        onClick={() => {
                            onChange(null)
                        }}
                        style={{
                            marginRight: theme.spacing(0.75)
                        }}>
                        <Delete fontSize={"small"}/>
                    </IconButton>
                </Tooltip>
                }
            </Typography>
            <ButtonBase onClick={() => {
                setOpen(true)
            }}>
                <Typography py={1} px={2} style={{direction: 'ltr'}}>
                    {date ?
                        JDate.timestampFormat(date, "jYYYY/jM/jD HH:mm") : 'انتخاب'}
                </Typography>
            </ButtonBase>
            <DatePicker open={open}
                        minimumDate={minimumDate}
                        maximumDate={maximumDate}
                        selectTime={true}
                        onClose={(t) => {
                            setOpen(false);
                            if ((minimumDate && t < minimumDate) || (maximumDate && t > maximumDate)) {
                                enqueueSnackbar('تاریخ انتخاب شده مورد قبول نمیباشد.',
                                    {
                                        variant: "error",
                                        action: (key) => (
                                            <Fragment>
                                                <Button onClick={() => {
                                                    closeSnackbar(key)
                                                }}>
                                                    {lang.get('close')}
                                                </Button>
                                            </Fragment>
                                        )
                                    });
                                return;
                            }
                            onChange(t);
                        }}/>
        </Box>
    )
}


//region Supplier
function Supplier({item: initItem, storages, isPackage, onClose, ...props}) {
    const theme = useTheme();
    const [state, setState] = useState({
        open: false,
    });
    const [loading, setLoading] = useState(false)

    const [item, setItem] = useState(initItem);

    useEffect(() => {
        setItem(item);
    }, [initItem]);


    const supplier = item;

    const handleOnChange = (su) => {
        setState(s => {
            return {...s, open: false}
        });

        if (su === undefined)
            return;

        if (isPackage) {
            setLoading(true)
            setTimeout(async () => {
                updateSupplier(0, su)
            }, 1500)
            return
        }
        setItem(su)
    };


    function updateSupplier(index, su) {
        const s = storages[index]
        ControllerProduct.Storage.save({
            pId: s.id,
            sId: s.default_storage_id,
            supplierId: su.id
        }).finally(() => {
            if (storages.length === index + 1) {
                setLoading(false)
                return
            }
            updateSupplier(index + 1, su)
        })
    }

    return (
        <Box p={1} {...props}>
            {
                !isPackage &&
                <NoneTextField
                    name={createName({group: 'moreData', name: 'supplierId'})}
                    defaultValue={item ? _.toString(item.id) : ''}/>
            }
            <Box
                style={{
                    position: 'relative',
                }}>
                <ButtonBase
                    onClick={() => {
                        setState({
                            ...state,
                            open: true
                        })
                    }}
                    style={{width: '100%'}}>
                    <Box component={Card} width={1} p={1} display={'flex'} flexDirection={'column'}
                         style={{
                             position: 'relative',
                         }}>
                        <UserItem showTitle={isPackage ? "تغییر تمام تامین کنندگان انبار" : true}
                                  justifyContent={'unset'} user={supplier}/>
                        {!supplier &&
                        <Box display={'flex'}
                             alignItems={'center'}
                             justifyContent={'center'}
                             style={{
                                 position: 'absolute',
                                 top: 0,
                                 bottom: 0,
                                 left: 0,
                                 right: 0,
                                 backgroundColor: theme.palette.background.default
                             }}>
                            <Typography variant={'h6'}>
                                انتخاب تامین کننده
                            </Typography>
                        </Box>
                        }
                    </Box>
                </ButtonBase>
                {(!isPackage && supplier) &&
                <Tooltip title={"حذف تامین کننده"}>
                    <IconButton
                        onClick={(e) => {
                            e.stopPropagation();
                            handleOnChange(null);
                        }}
                        style={{
                            position: 'absolute',
                            top: 5,
                            left: 5,
                        }}>
                        <Delete/>
                    </IconButton>
                </Tooltip>}
            </Box>
            <SelectSupplier open={state.open}
                            onClose={handleOnChange}/>
            <Backdrop open={loading}
                      style={{
                          zIndex: '9999'
                      }}>
                <CircularProgress color="inherit"
                                  style={{
                                      color: cyan[300],
                                      width: 100,
                                      height: 100
                                  }}/>
            </Backdrop>
        </Box>
    )
}

//endregion Supplier
//endregion MoreSetting

//region HelperComponents
function BoxContainer({item, ...props}) {
    return (
        <Box display={'flex'} flexDirection={'column'} alignItems={'center'} mt={2} {...props}>
            {props.children}
        </Box>
    )
}

//endregion HelperComponents

//endregion Components





