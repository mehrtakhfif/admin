import {
  Box,
  Button,
  DialogActions,
  IconButton,
  TextField,
} from "@material-ui/core";
import { Add, Delete } from "@material-ui/icons";
import { makeStyles } from "@material-ui/core/styles";
import { useSnackbar } from "notistack";
import React, { useState } from "react";
import BaseDialog from "../../components/base/BaseDialog";
import Typography from "../../components/base/Typography";
import axios from "axios";

const useStyle = makeStyles({
  code: {
    borderBottom: "1px solid #0000002b",
  },
});

export default function AddManualDiscountCode({ ...props }) {
  const classes = useStyle();
  const { open, sId: storageId, boxPrefix, onClose, dialogProps } = props;
  const [state, setstate] = useState();
  const [codes, setCodes] = useState([]);
  const { enqueueSnackbar } = useSnackbar();
  return (
    <BaseDialog
      open={open}
      header={"افزودن دستی کدتخفیف جدید"}
      onClose={onClose}
      maxWidth={"lg"}
    >
      <TextField
        fullWidth
        variant={"outlined"}
        name={"code"}
        label={"کد"}
        onKeyDown={handleAddCode}
        onChange={handleChange}
        InputProps={{
          endAdornment: (
            <IconButton onClick={handleAddCodeByButton}>
              <Add />
            </IconButton>
          ),
        }}
      />

      <Box height={"300px"} m={2} overflow={"auto"}>
        {codes.map((code) => (
          <Box
            className={classes.code}
            display={"flex"}
            flexDirection={"row"}
            justifyContent={"space-between"}
          >
            <Typography flex={1} m={1} p={1} key={codes.indexOf(code)}>
              {code}
            </Typography>
            <IconButton onClick={() => handleRemove(code)}>
              <Delete />
            </IconButton>
          </Box>
        ))}
      </Box>
      <DialogActions>
        <Button onClick={handleSave}>زخیره</Button>
      </DialogActions>
    </BaseDialog>
  );

  function handleRemove(code) {
    const el = codes.filter((c) => c !== code);
    console.log({ code, el });
    setCodes(el);
  }

  function handleAddCode(e) {
    if (e.keyCode === 13) {
      const { value } = e.target;
      addCode(value);
      e.target.value = null;
    }
  }

  function handleAddCodeByButton(e) {
    addCode(state);
  }

  function addCode(code) {
    if (!code) {
      enqueueSnackbar("کد کو؟", { variant: "error", autoHideDuration: 3000 });
      return;
    } else if (codes.findIndex((i) => i === code) !== -1) {
      enqueueSnackbar("کد مومجود است", {
        variant: "error",
        autoHideDuration: 3000,
      });
      return;
    }
    setCodes((s) => [...s, code]);
  }

  function handleChange(e) {
    const { value } = e.target;
    setstate(value);
  }
  function handleSave(params) {
    axios
      .post("manual_dicoount_code", { storage_id: storageId, codes: codes })
      .then((res) => {
        onClose();
      })
  }
}
