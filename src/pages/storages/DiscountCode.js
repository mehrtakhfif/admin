import React, {useState} from "react";
import ItemCard, {createItemCard} from "../../components/base/Items/ItemCard";
import Icon from "@material-ui/core/Icon";
import cyan from "@material-ui/core/colors/cyan";
import AddDiscountCode from "../../components/storage/AddDiscountCode";
import teal from "@material-ui/core/colors/teal";
import ShowDiscountCode from "../../components/storage/ShowDiscountCode";
import Box from "@material-ui/core/Box";
import DialogItems from "../../components/base/Items/DialogItems";
import _ from "lodash";
import rout from "../../router";
import {Redirect} from "react-router-dom";
import useSWR from "swr";
import ControllerProduct from "../../controller/ControllerProduct";
import {gcLog} from "../../utils/ObjectUtils";
import PleaseWait from "../../components/base/loading/PleaseWait";
import {grey, purple} from "@material-ui/core/colors";
import lime from "@material-ui/core/colors/lime";
import orange from "@material-ui/core/colors/orange";
import PrintDiscountCode from "../../components/storage/PrintDiscountCode";
import Typography from '../../components/base/Typography';
import AddManualDiscountCode from "./AddManualDiscountCode";
import { AddBox, KeyboardReturn, PersonAdd, Print, Reorder } from "@material-ui/icons";


const items = [
    createItemCard({
        icon: <AddBox  style={{color:'#FFFFFF'}}/>,
        title: "ساخت کد جدید",
        background: cyan[500],
        color: '#fff',
        dialog: (props) => <AddDiscountCode {...props}/>
    }),
    createItemCard({
        icon: <PersonAdd style={{color:'#FFFFFF'}}/> ,
        title: "افزودن کد دستی",
        background: "#5594dd",
        color: '#fff',
        dialog: (props) => <AddManualDiscountCode {...props}/>
    }),
    createItemCard({
        icon: <Reorder style={{color:'#FFFFFF'}}/>,
        title: "نمایش کدها",
        background: teal[400],
        color: '#fff',
        dialog: (props) => <ShowDiscountCode {...props}/>
    }),
    createItemCard({
        icon: <Print style={{color:'#FFFFFF'}}/>,
        title: "چاپ کد",
        background: orange[500],
        color: '#fff',
        dialog: (props) => <PrintDiscountCode {...props}/>
    }),
]

export default function ({...props}) {
    const [redirect,setRedirect] = useState()
    const sId = _.toInteger(props.match.params.sId);
    const d = ControllerProduct.Storage.getSingle({sId: sId, product_only: true})
    const {data: da, error} = useSWR(...d)
    const {data} = da||{}


     if (!data?.product?.category?.settings?.prefix){
         return(<Box width={'100%'} height={'100%'} display={'flex'} alignItems={'center'} justifyContent={'center'}>
             <Typography style={{margin:'32px',padding:'32px'}} >با پشتیبانی تماس بگیرید تا مشکل پریفیکس باکس را بر طرف کنند</Typography>
         </Box>)
     }

    return (
        <Box width={1} display={'flex'} flexWrap={'wrap'}>
            {
                (!redirect && (sId && (!data || data.product.type.isService))) ?
                    data ?
                        <React.Fragment>
                            {
                                items.map((item, i) => (
                                    <DialogItems key={i} item={item} dialogProps={{sId: sId, boxPrefix: data.product.category.settings.prefix}}/>
                                ))
                            }
                            <ItemCard
                                width={1/3}
                                item={createItemCard({
                                icon: <KeyboardReturn style={{color: "#fff"}}/>,
                                title: "بازگشت",
                                background: grey[500],
                                color: '#fff',
                            })}
                            onClick={()=>{
                                //Todo:redirect to storage
                                setRedirect(rout.Storage.Single.showStorageDashboard({storageId:sId}))
                            }}/>
                        </React.Fragment>:
                        <PleaseWait/> :
                    <Redirect
                        to={{
                            pathname: redirect || rout.Storage.rout,
                        }}
                    />
            }
        </Box>
    )
}

