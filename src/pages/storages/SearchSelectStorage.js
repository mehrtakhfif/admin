import React, {useEffect, useState} from "react";
import useSWR, {mutate} from "swr";
import ControllerProduct from "../../controller/ControllerProduct";
import Dialog from "@material-ui/core/Dialog";
import Transition from "../../components/base/Transition";
import Box from "@material-ui/core/Box";
import IconButton from "@material-ui/core/IconButton";
import {CancelOutlined, Close, Storage} from "@material-ui/icons";
import Typography from "../../components/base/Typography";
import Toolbar from "@material-ui/core/Toolbar";
import _ from "lodash";
import {productType} from "../../controller/type";
import ComponentError from "../../components/base/ComponentError";
import PleaseWait from "../../components/base/loading/PleaseWait";
import {DEBUG, siteLang} from "../../repository";
import Img from "../../components/base/img/Img";
import {Card} from "@material-ui/core";
import {Utils, UtilsStyle} from "../../utils/Utils";
import ButtonBase from "@material-ui/core/ButtonBase";
import {createName} from "../../components/base/textField/TextFieldMultiLanguageContainer";
import TextField from "../../components/base/textField/TextField";
import TextFieldContainer, {textFieldNewValue} from "../../components/base/textField/TextFieldContainer";
import ListIsEmpty from "../../components/base/ListIsEmpty";
import {gcLog, getSafe} from "../../utils/ObjectUtils";

export default function ({
                             open,
                             onSelect,
                             boxId,
                             types = [productType.product.type, productType.service.type, productType.package_item.type],
                             ...props
                         }) {

    const [defValue, setDefValue] = useState("");
    const [value, setValue] = useState("");
    const [inputValue, setInputValue] = useState("")

    const d = ControllerProduct.Product.search({
        query: value,
        boxId: boxId,
        types: types
    });

    useEffect(() => {
        if (open) {
            setDefValue("");
            setValue("");
        }
    }, [open])

    const {data: da, error} = useSWR(d[0], () => {
        return d[1]()
    });

    const data = da ? da.data.products : [];

    return (
        <Dialog fullScreen open={open} TransitionComponent={Transition}
                onClose={() => {
                    onSelect()
                }}>
            <Toolbar>
                <Box edge="start" display='flex' alignItems={'center'}
                     onClick={() => {
                         onSelect()
                     }}>
                    <IconButton color="inherit" aria-label="close">
                        <Close/>
                    </IconButton>
                    <Typography variant="body1" pr={1} style={{cursor: 'pointer'}}>
                        انصراف
                    </Typography>
                    <Typography variant={"h6"} px={2}>
                        انتخاب انبار
                    </Typography>
                </Box>
            </Toolbar>
            <Box display={'flex'} flexWrap={"wrap"}>
                <Box width={1 / 3} py={2} px={1}>
                    <TextFieldContainer
                        name={createName({name: "product-name"})}
                        defaultValue={defValue}
                        onChange={(val) => {
                            setInputValue(val)
                            // if (val.length < 3)
                            //     return
                            // setValue(val)
                            // setDefValue(val)
                        }}
                        render={(ref, {name: inputName, initialize, valid, errorIndex, inputProps, style, props}) => {
                            return (
                                <TextField
                                    {...props}
                                    variant={'outlined'}
                                    error={!valid}
                                    name={inputName}
                                    inputRef={ref}
                                    fullWidth
                                    onKeyPress={(e) => {
                                        try {
                                            if (e.key === 'Enter') {
                                                const val = ref.current.getAttribute(textFieldNewValue)
                                                setValue(val)
                                                setDefValue(val)
                                            }
                                        } catch (e) {
                                        }
                                    }}
                                    endAdornment={(inputValue) && (
                                        <IconButton onClick={() => {
                                            try {
                                                setDefValue("");
                                                setValue("");
                                                setInputValue("");
                                            } catch (e) {
                                            }
                                        }}>
                                            <CancelOutlined/>
                                        </IconButton>
                                    )}
                                    label={'جست‌و‌جو محصول'}
                                    placeholder={'نام محصول (حداقل 3 کارکتر) enter بزنید'}
                                    style={{
                                        ...style,
                                        minWidth: '90%'
                                    }}
                                    inputProps={{
                                        ...inputProps,
                                    }}/>
                            )
                        }}/>
                </Box>
                {!error ?
                    _.isArray(data) ?
                        !_.isEmpty(data) ?
                            <React.Fragment>
                                {
                                    data.map((d) => (
                                        <Item product={d}
                                              key={d.id}
                                              onSelect={(s) => {
                                                  onSelect(s)
                                              }}/>
                                    ))
                                }
                            </React.Fragment> :
                            <ListIsEmpty/> :
                        <PleaseWait/>
                    :
                    <ComponentError
                        statusCode={(_.isObject(error) && error.request) ? error.request.status : undefined}
                        tryAgainFun={() => mutate(d[0])}/>}
            </Box>
        </Dialog>
    )
}


function Item({product, onSelect, ...props}) {
    const [state, setState] = useState({
        open: false
    })
    return (
        <React.Fragment>
            {
                (DEBUG || !_.isEmpty(product.storages)) &&
                <Box px={2} py={1} width={1 / 3}>
                    <ButtonBase
                        onClick={() => {
                            setState({
                                ...state,
                                open: true
                            })
                        }}
                        style={{
                            display: 'block',
                            padding: 0,
                            width: '100%',
                            height: '100%'
                        }}>
                        <Box display={'flex'} component={Card} alignItems={'center'} p={2} height={1} width={1}>
                            <Img
                                alt={product.name[siteLang]}
                                src={product.thumbnail}
                                width={100}
                                showSkeleton={false}
                                minHeight={1}
                                style={{
                                    ...UtilsStyle.borderRadius(5),
                                    ...UtilsStyle.disableTextSelection()
                                }}/>
                            <Box display={'flex'} flexDirection={'column'} pl={3}>
                                <Typography variant={"body1"} pb={1}
                                            style={{
                                                textAlign: 'start'
                                            }}>
                                    {product.name[siteLang]}
                                </Typography>
                                <Box display={'flex'} flexWrap={'wrap'} alignItems={'center'}>
                                    {
                                        !_.isEmpty(product.storages) &&
                                        <Box display={'flex'} alignItems={'center'} py={1} pr={1}>
                                            <Typography variant={"body2"}>
                                                تعداد انبارها:
                                            </Typography>
                                            <Typography variant={"body1"} px={1} fontWeight={500}>
                                                {product.storages.length}
                                            </Typography>
                                        </Box>
                                    }
                                    {(_.isArray(product.categories) && !_.isEmpty(product.categories)) &&
                                    <React.Fragment>
                                        <Typography variant={"body2"} pl={0.75}>
                                            دسته‌ها:
                                        </Typography>
                                        {product.categories.map((cat, index) => (
                                            <Typography key={cat.id} variant={"body2"} py={1}>
                                                {cat.name[siteLang]} {product.categories.length < index - 1 && ","}
                                            </Typography>
                                        ))}
                                    </React.Fragment>}
                                </Box>
                            </Box>
                        </Box>
                    </ButtonBase>
                    {!_.isEmpty(product.storages) &&
                    <Dialog open={state.open} maxWidth={"xl"} fullWidth={true} onClose={() => {
                        setState({...state, open: false})
                    }}>
                        <Toolbar>
                            <Box edge="start" display='flex' alignItems={'center'}
                                 onClick={() => {
                                     setState({...state, open: false})
                                 }}>
                                <IconButton color="inherit" aria-label="close">
                                    <Close/>
                                </IconButton>
                                <Typography variant="body1" pr={1} style={{cursor: 'pointer'}}>
                                    انصراف
                                </Typography>
                                <Typography variant={"h6"} px={2}>
                                    یک انبار را انتخاب کنید
                                </Typography>
                            </Box>
                        </Toolbar>
                        <Box display={'flex'} width={1} flexWrap={'wrap'}>
                            {
                                product.storages.map(st => (
                                    <Box width={1 / 3} key={"st-" + st.storageId} px={2} py={1}>
                                        <ButtonBase
                                            onClick={() => {
                                                setState({...state, open: false});
                                                onSelect({
                                                    ...product,
                                                    defaultStorage: st
                                                });
                                            }}
                                            style={{
                                                width: '100%',
                                                padding: 0
                                            }}>
                                            <Box component={Card} width={1} display={'flex'} alignItems={'center'}
                                                 p={2}>
                                                <Storage
                                                    style={{
                                                        color: Utils.randomColor(),
                                                        width: 65,
                                                        height: "auto"
                                                    }}/>
                                                <Box display={'flex'} flexWrap={'wrap'}>
                                                    <Typography variant={"h6"} px={1} style={{width: '100%'}}>
                                                        {getSafe(()=>st.storageTitle[siteLang],"---")}
                                                    </Typography>
                                                    <Box display={'flex'} px={1} py={0.5}>
                                                        <Typography variant={"body1"}>
                                                            قیمت اولیه:
                                                        </Typography>
                                                        <Typography variant={"body1"} fontWeight={500} pr={1}>
                                                            {st.finalPrice}
                                                        </Typography>
                                                    </Box>
                                                    <Box display={'flex'} px={1} py={0.5}>
                                                        <Typography variant={"body1"}>
                                                            قیمت فروش:
                                                        </Typography>
                                                        <Typography variant={"body1"} fontWeight={500} pr={1}>
                                                            {st.discountPrice}
                                                        </Typography>
                                                    </Box>
                                                    <Box display={'flex'} px={1} py={0.5}>
                                                        <Typography variant={"body1"}>
                                                            تعداد موجود برای فروش:
                                                        </Typography>
                                                        <Typography variant={"body1"} fontWeight={500} pr={1}>
                                                            {st.availableCountForSale}
                                                        </Typography>
                                                    </Box>
                                                </Box>
                                            </Box>
                                        </ButtonBase>
                                    </Box>
                                ))
                            }
                        </Box>
                    </Dialog>}
                </Box>
            }
        </React.Fragment>
    )
}
