import React from 'react';
import {BrowserRouter as Router, Redirect, Route, Switch} from "react-router-dom"
import Test from "./Test";
import Test2 from "./Test2";
import rout from "../router";
import Test3 from "./Test3";
import Dashboard from "./Dashboard";
import ProductMultiEdit from "./products/ProductMultiEdit";
import ProductsNew from "./products/ProductsNew";
import ProductSingle3 from "./products/ProductSingle3";
import ProductReviewSingle from "./products/ProductReviewSingle";
import ProductReview from "./products/ProductReview";
import Products from "./products/Products";
import MultiEdit from "./storages/StorageMultiEdit";
import DiscountCode from "./storages/DiscountCode";
import StorageSingle from "./storages/StorageSingle";
import StorageDashboard from "./storages/StorageDashboard";
import FeatureSingle from "./features/FeatureSingle";
import Features from "./features/Features";
import FeaturesGroupSingle from "./featuresGroup/FeaturesGroupSingle";
import FeaturesGroup from "./featuresGroup/FeaturesGroup";
import Brands from "./brands/Brands";
import CategorySingle from "./categories/CategorySingle";
import RelationCategoriesSingle from "./categories/RelationCategoriesSingle";
import Categories from "./categories/Categories";
import SupplierSingle from "./supplier/SupplierSingle";
import Suppliers from "./supplier/Suppliers";
import TagSingle from "./tags/TagSingle";
import BoxTemplates from "./box/themplate/BoxTemplates";
import SpecialProduct from "./box/specialProduct/SpecialProduct";
import BoxDashboard from "./box/BoxDashboard";
import InvoiceShow from "./invoice/InvoiceShow";
import Invoice from "./invoice/Invoice";
import InvoiceProduct from "./invoiceProduct/InvoiceProduct";
import Booking from "./booking/Booking";
import Accountants from "./accountants/Accountants";
import Ads from "./site/ads/Ads";
import Slider from "./site/slider/Slider";
import SiteDashboard from "./site/SiteDashboard";
import Index from "./Index";
import {webRout} from "../repository";
import PermissionRequire from "./PermissionRequire";
import PrivateRoute from "./PrivateRoute";
import _ from 'lodash'

function RoutsSwitch({...props}) {
    const p = React.useMemo(()=>("/products/products_new/:pId"))
    return (
        <Switch>
            <Route path={'/test'} component={Test}/>
            {/*<Route path={p} component={ProductsNew}/>*/}
            <PermissionRequire path={'/test2'} component={Test2}/>
            <PermissionRequire path={rout.Main.dashboard+'/test3'} component={Test3}/>
            <PrivateRoute path={rout.Main.dashboard} component={Dashboard}/>
            <PermissionRequire path={rout.Product.Single.Refresh.rout + "/:pId"}
                               component={(props) => {
                                   const pId = props.match.params.pId;
                                   return (
                                       <Redirect
                                           to={{
                                               pathname: _.isString(pId) ? rout.Product.Single.editProduct(pId) : rout.Product.rout
                                           }}/>
                                   )
                               }}/>
            <PermissionRequire path={rout.Product.Single.MultiEdit.rout} component={ProductMultiEdit}/>
            {/*<PermissionRequire path={p}><ProductsNew/></PermissionRequire>*/}
            <PermissionRequire path={rout.Product.Single.ProductsNew.rout + "/:pId"} component={ProductsNew}/>
            <PermissionRequire path={rout.Product.Single.ProductsNew.rout} component={ProductsNew}/>
            <PermissionRequire path={rout.Product.Single.rout + "/:pId"} component={ProductsNew}/>
            <PermissionRequire path={rout.Product.Single.rout} component={ProductsNew}/>
            <PermissionRequire path={rout.Product.Single.rout + "2/:pId"} component={ProductSingle3}/>
            <PermissionRequire path={rout.Product.Single.rout + "2"} component={ProductSingle3}/>
            <PermissionRequire path={rout.Product.Review.Single.rout + "/:pId"} component={ProductReviewSingle}/>
            <PermissionRequire path={rout.Product.Review.rout} component={ProductReview}/>
            <PermissionRequire path={rout.Product.rout} component={Products}/>
            <PermissionRequire path={rout.Storage.Single.Refresh.rout + "/:pId"}
                               component={(props) => {
                                   const pId = props.match.params.pId;
                                   return (
                                       <Redirect
                                           to={{
                                               pathname: _.isString(pId) ? rout.Storage.Single.showStorageDashboard({productId: pId}) : rout.Product.rout
                                           }}/>
                                   )
                               }}/>
            <PermissionRequire path={rout.Storage.Single.MultiEdit.rout + "/:pId/:type"} component={MultiEdit}/>
            <PermissionRequire path={rout.Storage.Single.MultiEdit.rout} component={MultiEdit}/>
            <PermissionRequire path={rout.Storage.Single.Code.rout + "/:sId"} component={DiscountCode}/>
            <PermissionRequire path={rout.Storage.Single.Edit.rout + "/:sId/:pId"} component={StorageSingle}/>
            <PermissionRequire path={rout.Storage.Single.Edit.rout + "/:sId"} component={StorageSingle}/>
            <PermissionRequire path={rout.Storage.Single.rout + "/:pId"} component={StorageDashboard}/>
            <Redirect from={rout.Storage.rout} to={rout.Product.rout}/>
            <PermissionRequire path={rout.Feature.Single.rout + "/:fId"} component={FeatureSingle}/>
            <PermissionRequire path={rout.Feature.Single.createNewProduct} component={FeatureSingle}/>
            <PermissionRequire path={rout.Feature.rout} component={Features}/>
            <PermissionRequire path={rout.FeatureGroup.Single.rout + "/:fgId"} component={FeaturesGroupSingle}/>
            <PermissionRequire path={rout.FeatureGroup.Single.createNewProduct} component={FeaturesGroupSingle}/>
            <PermissionRequire path={rout.FeatureGroup.rout} component={FeaturesGroup}/>
            <PermissionRequire path={rout.Brand.rout} component={Brands}/>
            <PermissionRequire path={rout.Category.Single.rout + "/:catId"} component={CategorySingle}/>
            <PermissionRequire path={rout.Category.Single.createNewProduct} component={CategorySingle}/>
            <PermissionRequire path={rout.Category.RelationCat.Single.rout + "/:catId"}
                               component={RelationCategoriesSingle}/>
            <PermissionRequire path={rout.Category.RelationCat.Single.rout} component={RelationCategoriesSingle}/>
            <PermissionRequire path={rout.Category.rout} component={Categories}/>
            <PermissionRequire path={rout.Supplier.Single.rout + "/:userId"} component={SupplierSingle}/>
            <PermissionRequire path={rout.Supplier.Single.rout} component={SupplierSingle}/>
            <PermissionRequire path={rout.Supplier.rout} component={Suppliers}/>
            <PermissionRequire path={rout.Tags.Single.rout + "/:tagId"} component={TagSingle}/>
            <PermissionRequire path={rout.Tags.Single.createNewTags} component={TagSingle}/>
            <PermissionRequire path={rout.Tags.rout} component={Dashboard}/>
            <PermissionRequire path={rout.Box.Templates.rout + "/:templatesId"} component={BoxTemplates}/>
            <PermissionRequire path={rout.Box.Templates.rout} component={BoxTemplates}/>
            <PermissionRequire path={rout.Box.SpecialProduct.rout} component={SpecialProduct}/>
            <PermissionRequire path={rout.Box.Dashboard.rout} component={BoxDashboard}/>
            <PermissionRequire path={rout.Invoice.Single.show + "/:invoiceId"} component={InvoiceShow}/>
            <PermissionRequire path={rout.Invoice.rout} component={Invoice}/>
            <PermissionRequire path={rout.InvoiceProduct.rout} component={InvoiceProduct}/>
            <PermissionRequire path={rout.Booking.rout} component={Booking}/>
            <PermissionRequire path={rout.Accountants.rout} component={Accountants}/>
            <PermissionRequire path={rout.Site.Ads.rout} component={Ads}/>
            <PermissionRequire path={rout.Site.Slider.rout} component={Slider}/>
            <PermissionRequire path={rout.Site.Dashboard.rout} component={SiteDashboard}/>
            <Redirect from={rout.Box.rout} to={rout.Box.Dashboard.rout}/>
            <Route exact path={rout.Main.index} component={Index}/>
            <Route component={() => (
                <React.Fragment>
                    redirect->
                    {window.location.href = webRout}
                </React.Fragment>
            )}/>
        </Switch>
    )
}

export default React.memo(RoutsSwitch)