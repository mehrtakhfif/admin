import Box from "@material-ui/core/Box";
import {lang, siteLang, theme} from "../../repository";
import SuccessButton from "../../components/base/button/buttonVariant/SuccessButton";
import BaseButton from "../../components/base/button/BaseButton";
import BaseDialog from "../../components/base/BaseDialog";
import React, {Fragment, useEffect, useRef, useState} from "react";
import ControllerProduct from "../../controller/ControllerProduct";
import Typography from "../../components/base/Typography";
import {createMultiLanguage} from "../../controller/converter";
import PleaseWait from "../../components/base/loading/PleaseWait";
import error from "../../language/fa/error";
import ComponentError from "../../components/base/ComponentError";
import FormControl from "../../components/base/formController/FormController";
import {errorList, notValidErrorTextField} from "../../components/base/textField/TextFieldContainer";
import TextFieldMultiLanguageContainer from "../../components/base/textField/TextFieldMultiLanguageContainer";
import TextField from "../../components/base/textField/TextField";
import PermalinkTextField from "../../components/base/textField/PermalinkTextField";
import {useSnackbar} from "notistack";
import Button from "@material-ui/core/Button";
import SaveDialog from "../../components/dialog/SaveDialog";
import {orange} from "@material-ui/core/colors";
import _ from 'lodash'

export default function ({open, name, onClose, tagId: tId, ...props}) {
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();
    let tagId = null;
    try {
        tagId = tId ? tId : props.match.params.tagId;
    } catch (e) {
    }

    const ref = useRef();
    const [nameRef, setNameRef] = useState();

    const [data, setData] = useState(tagId ? null : initProps());
    const [state, setState] = useState({
        loading: false,
        error: false,
        saveDialog: false
    });


    function initProps() {
        const props = {
            name: createMultiLanguage(),
        };
        if (_.isString(name)) {
            props.name = createMultiLanguage({fa: name});
        }
        if (_.isObject(name)) {
            props.name = createMultiLanguage(name);
        }
        props.permalink = name;

        return props;
    }

    useEffect(getData, [open, tagId]);

    function getData() {
        if (!tagId) {
            setData(initProps());
            return
        }
        setData(null);

        ControllerProduct.Product.Tags.getSingle({tagId: tagId}).then(res => {
            setData({
                ...data,
                name: createMultiLanguage(res.data.name),
                permalink: res.data.permalink
            })
        }).catch(e => {
            setState({
                ...state,
                error: true
            })
        })
    }

    function handleAddTag() {
        if (ref.current.hasError()) {
            let errorText = "فرم مشکل دارد";
            try {
                const el = ref.current.getErrorElement();
                const t = el.getAttribute(notValidErrorTextField);
                if (t)
                    errorText = t;
                el.focus();
            } catch (e) {
            }
            reqCancel(errorText);
            return;
        }
        setState({
            ...state,
            loading: true
        });
        const {name, permalink, ...fp} = ref.current.serialize();

        ControllerProduct.Product.Tags.set({tagId: tagId, permalink: permalink, name: name}).then(res => {
            setState({
                ...state,
                saveDialog: false,
                loading: false
            });

            console.log("iteeeeem res",res)
            if (onClose)
                onClose(res.item)
        }).catch(() => {
            setState({
                ...state,
                saveDialog: false,
                loading: false
            });
        })
    }

    function reqCancel(text = null) {
        if (text)
            enqueueSnackbar(text,
                {
                    variant: "error",
                    action: (key) => (
                        <Fragment>
                            <Button onClick={() => {
                                closeSnackbar(key)
                            }}>
                                {lang.get('close')}
                            </Button>
                        </Fragment>
                    )
                });

        setState({
            loading: false
        })

    }


    const el = data && (
        <Box py={3} px={2} display={'flex'} flexDirection={'column'} minWidth={400}>
            <Typography variant={'h6'} fontWeight={600} pb={1.5}>
                {tagId ? "بروزرسانی تگ" : " افزودن تگ جدید"}
            </Typography>
            <FormControl pt={2} px={2} innerref={ref}>
                <TextFieldMultiLanguageContainer
                    name={"name"}
                    render={(ref, {lang: inputLang, name: inputName, initialize, valid, errorIndex, setValue, inputProps, style, props}) => {
                        if (inputLang.key === "fa") {
                            setNameRef(ref)
                        }
                        return (
                            <TextField
                                {...props}
                                error={!valid}
                                variant="outlined"
                                name={inputName}
                                defaultValue={data.name[inputLang.key]}
                                fullWidth
                                helperText={errorList[errorIndex]}
                                inputRef={ref}
                                required={inputLang.key === siteLang}
                                label={"نام"}
                                style={style}
                                inputProps={{
                                    ...inputProps
                                }}/>
                        )
                    }}/>
                <Box pt={2}>
                    <PermalinkTextField
                        name={"permalink"}
                        syncedRef={nameRef}
                        defaultValue={data.permalink}
                        inputProps={{
                            fullWidth: true
                        }}/>
                </Box>
                <Box pt={2} display={'flex'}>
                    <SuccessButton loading={state.loading} onClick={() => {
                        setState({
                            ...state,
                            saveDialog: true
                        })
                    }}>
                        {lang.get(tagId?"save" :"add")}
                    </SuccessButton>
                    {open &&
                    <Box pr={1}>
                        <BaseButton disabled={state.loading} variant={"text"} onClick={() => onClose()}>
                            {lang.get("close")}
                        </BaseButton>
                    </Box>
                    }
                </Box>
                <SaveDialog
                    text={(
                        <Box display={'flex'} flexDirection={'column'} px={3}>
                            <Typography variant={'h6'} fontWeight={400} pb={2}>
                                ذخیره تگ
                            </Typography>
                            <Typography variant={'body1'}>
                                آیا از ذخیره این تگ اطمینان دارید؟
                            </Typography>
                            <Typography variant={'caption'} color={theme.palette.text.secondary} pt={0.5}>
                                <span style={{color: orange[300], fontWeight: 600, paddingLeft: theme.spacing(0.3)}}>
                                    توجه:
                                </span>
                                در صورت ذخیره، اطلاعات تغییریافته قابل بازگشت نمیباشد.
                            </Typography>
                        </Box>
                    )}
                    open={state.saveDialog}
                    onCancel={() => {
                        setState({
                            ...state,
                            saveDialog: false
                        })
                    }}
                    onSubmit={() => {
                        setState({
                            ...state,
                            saveDialog: false
                        });
                        handleAddTag()
                    }}/>
            </FormControl>
        </Box>
    );
    return (
        state.error ?
            <ComponentError tryAgainFun={getData}/> :
            data ? _.isBoolean(open) ?
                <BaseDialog
                    open={open}
                    onClose={() => {
                    }}>
                    {el}
                </BaseDialog> : el :
                <PleaseWait/>
    )
}


// export function TagSingleDialog() {
//     return (
//         <BaseDialog
//             open={setting.openAddTagDialog}
//             onClose={() => {
//             }}
//             aria-labelledby="add-tag-dialog-slide-title"
//             aria-describedby="add-tag-dialog-slide-description">
//             <Box py={3} px={2} display={'flex'} flexDirection={'column'} minWidth={400}>
//                 <Typography variant={'h6'} fontWeight={600} pb={1.5}>
//                     افزودن تگ جدید
//                 </Typography>
//                 <Box display={'flex'} flexDirection={'column'}>
//                     <Box pb={2} width={1}>
//                         <BaseTextField
//                             isRequired={true}
//                             label={lang.get("permalink")}
//                             value={setting.dialogPermalink}
//                             dir={'ltr'}
//                             pattern={'^[A-Za-z\u0591-\u07FF\uFB1D-\uFDFD\uFE70-\uFEFC][A-Za-z0-9-\u0591-\u07FF\uFB1D-\uFDFD\uFE70-\uFEFC]*$'}
//                             onTextChange={(v) => setSetting({
//                                 ...setting,
//                                 dialogPermalink: v,
//                             })}
//                             style={{
//                                 width: '100%'
//                             }}/>
//                     </Box>
//                     <BaseTextField
//                         isRequired={true}
//                         label={'نام تگ'}
//                         value={setting.dialogValue}
//                         onTextChange={(v) => setSetting({
//                             ...setting,
//                             dialogValue: v
//                         })}/>
//                     <Box pt={2} display={'flex'}>
//                         <SuccessButton onClick={() => handleCloseDialog(true)}>
//                             {lang.get("add")}
//                         </SuccessButton>
//                         <Box pr={1}>
//                             <BaseButton variant={"text"} onClick={() => handleCloseDialog(false)}>
//                                 {lang.get("close")}
//                             </BaseButton>
//                         </Box>
//                     </Box>
//                 </Box>
//             </Box>
//         </BaseDialog>
//     )
// }
