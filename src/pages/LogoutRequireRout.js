import React from 'react';
import {useSelector} from 'react-redux'
import {Route, Redirect} from "react-router-dom";

import {DEBUG_AUTH} from "../repository";
import rout from "../router";

function LogoutRequireRout({component: Component, rout: string, ...rest}) {
    const user = useSelector(state => state.user);
    return (
        <Route
            {...rest}
            render={props =>
                DEBUG_AUTH || !user.isLogin ? (
                    <Component {...props} />
                ) : (
                    <Redirect
                        to={{
                            pathname: rout.Main.dashboard
                        }}
                    />

                )
            }
        />
    );
}

export default LogoutRequireRout;