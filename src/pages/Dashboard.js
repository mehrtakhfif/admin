import React, {useEffect, useState,Fragment} from "react";
import Box from "@material-ui/core/Box";
import {Card, useTheme} from "@material-ui/core";
import Typography from "../components/base/Typography";
import {
    AddAPhoto,
    Airplay,
    ArrowDropDown,
    Category,
    DvrOutlined,
    FiberManualRecordTwoTone,
    InboxOutlined,
    ReceiptOutlined,
    Refresh,
    ReportProblemOutlined
} from "@material-ui/icons";
import {activeLang, lang, siteLang} from "../repository";
import {useSelector} from "react-redux";
import rout from "../router";
import {amber, cyan, grey, orange, red, yellow} from "@material-ui/core/colors";
import Divider from "@material-ui/core/Divider";
import BaseButton from "../components/base/button/BaseButton";
import Link from "../components/base/link/Link";
import {Doughnut, HorizontalBar, Line, Pie} from 'react-chartjs-2';
import _ from "lodash";
import useSWR, {mutate} from "swr";
import ControllerSite from "../controller/ControllerSite";
import {Utils, UtilsFormat, UtilsStyle} from "../utils/Utils";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import LocalStorageUtils from "../utils/LocalStorageUtils";
import ControllerProduct from "../controller/ControllerProduct";
import ButtonBase from "@material-ui/core/ButtonBase";
import ControllerBox from "../controller/ControllerBox";
import ComponentError from "../components/base/ComponentError";
import Menu from "@material-ui/core/Menu";
import {deliver_status} from "../controller/converter";
import storage from "../storage";
import {isElement} from "../utils/ObjectUtils";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "../components/base/Tooltip";
import BaseDialog from "../components/base/BaseDialog";
import Table, {createTableHeader, headerItemTemplate} from "../components/base/table/Table";
import Icon from "@material-ui/core/Icon";
import DatePickerStartEndPanel from "../components/base/datePicker/DatePickerStartEndPanel";
import Skeleton from "@material-ui/lab/Skeleton";


function createItemCard({icon, title, rout, background = grey[100], darkBackground = grey[700]}) {
    return {icon, title, rout, background, darkBackground: darkBackground || background}
}

const admin2Item = [
    [
        createItemCard({
            icon: <DvrOutlined fontSize={"large"}/>,
            title: "محصولات",
            rout: rout.Product.rout,
            background: cyan[200]
        }),
        createItemCard({
            icon: <InboxOutlined fontSize={"large"}/>,
            title: lang.get("mt_box"),
            rout: rout.Box.Dashboard.rout,
            background: yellow[200]
        }),
        createItemCard({
            icon: <Category fontSize={"large"}/>,
            title: lang.get("categories"),
            rout: rout.Category.rout,
            background: orange[200]
        }),
    ]
];
const adminItem = [
    [
        ...admin2Item[0]
    ],
    [
        createItemCard({icon: <ReceiptOutlined fontSize={"large"}/>, title: "صورت حساب", rout: rout.Invoice.rout}),
    ]
];

const designerItem = [
    [
        createItemCard({
            icon: <AddAPhoto fontSize={"large"}/>,
            title: "تبلیغات",
            rout: rout.Site.Ads.rout,
            background: cyan[400]
        }),
        createItemCard({
            icon: <Airplay fontSize={"large"}/>,
            title: "اسلایدر",
            rout: rout.Site.Slider.rout,
            background: amber[400]
        }),
    ]
];

const accountantsItem = [
    [
        createItemCard({icon: <ReceiptOutlined fontSize={"large"}/>, title: "صورت حساب", rout: rout.Invoice.rout}),
        createItemCard({
            icon: <Icon fontSize={"large"} className={"far fa-calculator"}/>,
            title: "پنل حسابدار",
            rout: rout.Accountants.rout,
            background: cyan[300],
        }),

    ]
];

const postItem = [
    [
        createItemCard({icon: <ReceiptOutlined fontSize={"large"}/>, title: "صورت حساب", rout: rout.Invoice.rout}),
    ]
];

const dashboardItems = {
    admin: adminItem,
    admin2: admin2Item,
    support: [...adminItem],
    content_manager: [...adminItem],
    superuser: [...adminItem],
    designer: designerItem,
    ha_accountants: accountantsItem,
    accountants: accountantsItem,
    post: accountantsItem,
}

const legendOpts = {
    display: false,
    position: 'top',
    fullWidth: true,
    reverse: false,
    labels: {
        fontColor: 'rgb(255, 99, 132)'
    }
};

function createLineItems({label, data, borderColor = Utils.randomColor()}) {
    return {
        label: label,
        fill: false,
        lineTension: 0.1,
        borderColor: borderColor,
        borderCapStyle: 'butt',
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: 'miter',
        pointBorderColor: 'rgba(75,192,192,1)',
        pointBackgroundColor: '#fff',
        pointBorderWidth: 1,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: 'rgba(75,192,192,1)',
        pointHoverBorderColor: 'rgba(220,220,220,1)',
        pointHoverBorderWidth: 2,
        pointRadius: 1,
        pointHitRadius: 10,
        data: data
    }
}

export default function Dashboard({...props}) {
    const {roll, boxes} = useSelector(state => state.user);
    const items = dashboardItems[roll];

    return (
        <Box width={1}>
            <Box display={'flex'} flexWrap={'wrap'}>
                <Box width={1} display={'flex'} flexWrap={"wrap"} p={3}>
                    {
                        !_.isEmpty(boxes) &&
                        <SalesSummary roll={roll} boxes={boxes} mt={2} mx={2}/>
                    }
                </Box>
                <Box width={1} display={'flex'} flexWrap={"wrap"}>
                    <Review mt={2} mx={2}/>
                </Box>
                <Box width={1} display={'flex'} flexWrap={"wrap"}>
                    {
                        (roll === "superuser" || roll === "admin" || roll === "admin2" || roll === "support" || roll === "designer") &&
                        <ProductCountChart/>
                    }
                    {
                        (roll === "superuser" || roll === "accountants") &&
                        <ProfitSummery/>
                    }
                </Box>
                {items.map((item, index) => (
                    <React.Fragment>
                        {item.map((it, i) => (
                            <React.Fragment key={`${index}-${i}`}>
                                <ItemCard item={it}/>
                            </React.Fragment>
                        ))}
                        {
                            items.length > index + 1 &&
                            <Box width={1}>
                                <Divider/>
                            </Box>
                        }
                    </React.Fragment>
                ))}
            </Box>
        </Box>
    )
}

export function Review({...props}) {
    const theme = useTheme();
    const {roll, boxes} = useSelector(state => state.user);
    const ownerBox = _.find(boxes, b => b.is_owner);
    const isContentManager = roll === "content_manager" || roll === "superuser";
    const reviewC = ControllerProduct.Products.reviewProduct({
        boxId: ownerBox ? ownerBox.id : undefined,
        reviewState:"request_review",
    });
    const {data, error} = useSWR(reviewC[0], () => {
        if (!ownerBox && !isContentManager) {
            return undefined
        }
        return reviewC[1]()
    });

    return (
        (data && data.pagination.count > 0) ?
            <Link toHref={rout.Product.Review.rout} hoverColor={"#fff"}>
                <Box p={2}
                     {...props}
                     style={{
                         backgroundColor: isContentManager ? orange["A200"] : red["A200"],
                         ...UtilsStyle.borderRadius(5),
                         ...props.style,
                     }}>
                    <Typography variant={"body1"} alignItems={'center'} color={'#fff'}>
                        <ReportProblemOutlined
                            style={{
                                marginLeft: theme.spacing(1)
                            }}/>
                        تعداد
                        <Typography px={1} variant={'h4'} fontWeight={500} color={'#fff'}>
                            {data.pagination.count}
                        </Typography>
                        {
                            isContentManager ?
                                "محصول درخواست بازبینی دارد."
                                :
                                "محصول نیاز به بازبینی دارد."
                        }
                    </Typography>
                </Box>
            </Link>
            :
            <React.Fragment/>
    )
}


function ProductCountChart() {

    return <Fragment/>

    const {lite} = useSelector(state => state.base);
    const apiKey = "productCount" + lite;
    const {data: productCount, error: productCountError} = useSWR(apiKey, () => {
        if (lite)
            return
        return ControllerSite.Dashboard.productCount().then(res => {
            const data = res.data
            if (!_.isEmpty(data.boxes)) {
                const products = {
                    labels: [],
                    datasets: [
                        {
                            data: [],
                            backgroundColor: []
                        }
                    ],
                    horizontalLabel: [],
                    horizontalDatasets: {
                        label: 'محصولات',
                        backgroundColor: "#4dd0e16e",
                        borderColor: cyan[500],
                        borderWidth: 1,
                        hoverBackgroundColor: cyan[500],
                        data: []
                    }
                }
                const activeProducts = {
                    labels: [],
                    datasets: [
                        {
                            data: [],
                            backgroundColor: []
                        }
                    ],
                    horizontalLabel: [],
                    horizontalDatasets: {
                        label: 'محصولات فعال',
                        backgroundColor: "#4dd0e16e",
                        borderColor: cyan[500],
                        borderWidth: 1,
                        hoverBackgroundColor: cyan[500],
                        data: []
                    }
                }
                _.forEach(data.boxes, b => {
                    const name = b.name[siteLang];
                    const color = b.settings.color || Utils.randomColor();

                    //region Doughnut
                    products.labels.push(name)
                    products.datasets[0].data.push(b.product_count)
                    products.datasets[0].backgroundColor.push(color)

                    activeProducts.labels.push(name)
                    activeProducts.datasets[0].data.push(b.active_product_count)
                    activeProducts.datasets[0].backgroundColor.push(color)
                    //endregion Doughnut

                    //region horizontalBar
                    products.horizontalDatasets.data.push(b.product_count)
                    activeProducts.horizontalDatasets.data.push(b.active_product_count)
                    //endregion horizontalBar
                })


                data.boxes = {
                    products,
                    activeProducts
                }
            }

            return res.data
        })
    }, {
        dedupingInterval: 8000000
    })
    return (
        <React.Fragment>
            {!lite && productCount && (
                <React.Fragment>
                    {!_.isEmpty(productCount.boxes) &&
                    <React.Fragment>
                        {productCount.boxes.products &&
                        <Box display={'flex'} width={2 / 3}>
                            <ItemCard
                                minWidth={1 / 2}
                                p={1}
                                item={(
                                    <Box display={'flex'} flexDirection={'column'} justifyContent={'center'}
                                         p={1}
                                         style={{
                                             border: `2px solid ${grey[300]}`,
                                             ...UtilsStyle.borderRadius(5)
                                         }}>
                                        <Doughnut data={productCount.boxes.products} legend={legendOpts} redraw/>
                                        <Typography justifyContent={"center"} variant={'h6'} pt={1}>
                                            تمام محصولات
                                        </Typography>
                                    </Box>
                                )}/>
                            <ItemCard
                                minWidth={1 / 2}
                                p={1}
                                item={(
                                    <Box display={'flex'} flexDirection={'column'} justifyContent={'center'}
                                         p={1}
                                         style={{
                                             border: `2px solid ${grey[300]}`,
                                             ...UtilsStyle.borderRadius(5)
                                         }}>
                                        <HorizontalBar
                                            data={{
                                                labels: productCount.boxes.products.labels,
                                                datasets: [productCount.boxes.products.horizontalDatasets]
                                            }}
                                            legend={{
                                                display: false
                                            }}/>
                                        <Typography justifyContent={"center"} variant={'h6'} pt={1}>
                                            تمام محصولات
                                        </Typography>
                                    </Box>
                                )}/>
                        </Box>}
                        <ProductCountLineChart/>
                        {productCount.boxes.activeProducts &&
                        <Box display={'flex'} width={2 / 3}>
                            <ItemCard
                                minWidth={1 / 2}
                                p={1}
                                item={(
                                    <Box display={'flex'} flexDirection={'column'} justifyContent={'center'}
                                         p={1}
                                         style={{
                                             border: `2px solid ${grey[300]}`,
                                             ...UtilsStyle.borderRadius(5)
                                         }}>
                                        <Doughnut data={productCount.boxes.activeProducts} legend={legendOpts}
                                                  redraw/>
                                        <Typography justifyContent={"center"} variant={'h6'} pt={1}>
                                            محصولات فعال
                                        </Typography>
                                    </Box>
                                )}/>
                            <ItemCard
                                minWidth={1 / 2}
                                p={1}
                                item={(
                                    <Box display={'flex'} flexDirection={'column'} justifyContent={'center'}
                                         p={1}
                                         style={{
                                             border: `2px solid ${grey[300]}`,
                                             ...UtilsStyle.borderRadius(5)
                                         }}>
                                        <HorizontalBar
                                            data={{
                                                labels: productCount.boxes.activeProducts.labels,
                                                datasets: [productCount.boxes.activeProducts.horizontalDatasets]
                                            }}/>
                                        <Typography justifyContent={"center"} variant={'h6'} pt={1}>
                                            محصولات فعال
                                        </Typography>
                                    </Box>
                                )}/>
                        </Box>}
                    </React.Fragment>}
                </React.Fragment>
            )}
        </React.Fragment>
    )
}

const SalesSummaryStorageKey = "SALES_SUMMARY_STORAGE_KEY"

function SalesSummary({boxes, ...props}) {
    return <Fragment/>
    const theme = useTheme();
    const {lite} = useSelector(state => state.base);
    const [activeBox, setActiveBox] = useState(storage.PageSetting.getActiveBox(SalesSummaryStorageKey, boxes, true));
    const [boxSelector, setBoxSelector] = useState();

    const d = ControllerBox.getSummary({boxId: activeBox.id})
    const {data, error} = useSWR(d[0] + lite, () => {
        if (lite)
            return
        return d[1]()
    }, {
        dedupingInterval: 8000000
    })

    function handleCloseBoxSelector(e, box) {
        if (box) {
            setActiveBox(box);
            storage.PageSetting.setActiveBox(SalesSummaryStorageKey, {activeBox: box})
        }
        setBoxSelector(undefined)
    }

    return (
        lite ?
            <React.Fragment>
                {
                    (props.roll === "superuser" || props.roll === "admin" || props.roll === "support") ?
                        <Box m={1} width={1} display={'flex'} flexWrap={"wrap"}>
                            <Box display={'flex'} alignItems={'center'}>
                                <Typography variant={'body1'} mr={1.5} ml={0.5}>
                                    {lang.get('active_box')}:
                                </Typography>
                                {(boxes && !_.isEmpty(boxes)) &&
                                <Box mr={2}>
                                    {activeBox &&
                                    <Box display={'flex'} alignItems={'center'}>
                                        <ButtonBase
                                            onClick={(e) => setBoxSelector(e.currentTarget)}
                                            style={{
                                                border: `1px solid ${cyan[200]}`,
                                                ...UtilsStyle.borderRadius(5),
                                                ...UtilsStyle.disableTextSelection(),
                                            }}>
                                            <Box display={'flex'} alignItems={'center'}
                                                 py={0.75}
                                                 px={1}
                                                 style={{current: 'pointer'}}>
                                                <Typography variant={'body1'} ml={0.5}>
                                                    {activeBox.name[activeLang]}
                                                </Typography>
                                                <ArrowDropDown/>
                                            </Box>
                                        </ButtonBase>
                                    </Box>}
                                    <Menu
                                        id="simple-menu"
                                        anchorEl={boxSelector}
                                        keepMounted
                                        open={Boolean(boxSelector)}
                                        onClose={(e) => {
                                            handleCloseBoxSelector(e)
                                        }}>
                                        {
                                            boxes.map((b, index) => (
                                                <MenuItem key={index}
                                                          onClick={(e) => handleCloseBoxSelector(e, b)}><Typography>{b.name[activeLang]}</Typography></MenuItem>
                                            ))
                                        }
                                    </Menu>
                                </Box>}
                                <RefreshIcon onClick={() => mutate(d[0])}/>
                            </Box>
                        </Box> : <React.Fragment/>
                }
                {
                    error ? <ComponentError tryAgainFun={() => mutate(d[0])}/> :
                        data ?
                            <React.Fragment>
                                {Object.keys(deliver_status).map(k => {
                                    const item = deliver_status[k]
                                    return (
                                        <React.Fragment>
                                            {data.data[item.key] ?
                                                <Box display="inline-flex" px={3} alignItems={"center"}>
                                                    <FiberManualRecordTwoTone fontSize={"large"} style={{
                                                        color: item.color,
                                                        alignSelf: "center",
                                                        padding: "2px"
                                                    }}/>
                                                    <Typography variant={'h6'} fontWeight={300} alignItems={"center"}>
                                                        {item.label}:
                                                    </Typography>
                                                    <Typography px={1} variant={'h3'} fontWeight={500}>
                                                        {data.data[item.key]}
                                                    </Typography>
                                                </Box> :
                                                <React.Fragment/>
                                            }
                                        </React.Fragment>
                                    )
                                })}
                            </React.Fragment> :
                            <React.Fragment/>
                }
            </React.Fragment> :
            <React.Fragment/>)

}

const DateProductCountCookieKey = "dashboard-date-product-count";

function ProductCountLineChart() {
    const theme = useTheme();
    const [state, setState] = useState({
        activeBox: _.toNumber(LocalStorageUtils.get(DateProductCountCookieKey, -1))
    });
    const {lite} = useSelector(state => state.base);

    const apiKey = `dateProductCount-${state.activeBox}-${lite}`;
    const {data: dateProductCount, error: dateProductCountErrpr} = useSWR(apiKey, () => {
        if (lite)
            return
        return ControllerSite.Dashboard.dateProductCount().then(res => {
            const db = {
                labels: res.data.label,
                datasets: [],
                boxes: []
            };
            _.forEach(res.data.boxes, (b) => {
                db.boxes.push({
                    id: b.id,
                    name: b.name[siteLang],
                    settings: b.settings
                })
                if (state.activeBox === -1 || state.activeBox === 0 || b.id === state.activeBox) {
                    if (state.activeBox !== 0)
                        db.datasets.push(createLineItems({
                            id: b.id,
                            label: b.name[siteLang] + " - جدید",
                            data: b.created_products,
                            borderColor: b.settings.color
                        }))
                    if (state.activeBox !== -1)
                        db.datasets.push(createLineItems({
                            id: b.id,
                            label: b.name[siteLang] + " - ویرایش",
                            data: b.updated_products,
                            borderColor: b.settings.secondaryColor
                        }))
                }
            })
            return db
        })
    }, {
        dedupingInterval: 8000000
    })

    useEffect(() => {
        LocalStorageUtils.set(DateProductCountCookieKey, state.activeBox)
    }, [state.activeBox])

    return (
        <React.Fragment>
            {!lite && dateProductCount &&
            <ItemCard
                item={(
                    <Box
                        display={'flex'}
                        flexDirection={'column'}
                        justifyContent={'center'}
                        p={1}
                        style={{
                            border: `2px solid ${grey[300]}`,
                            ...UtilsStyle.borderRadius(5)
                        }}>
                        <Line
                            data={dateProductCount}
                            legend={{
                                display: false
                            }}/>
                        <Box display={'flex'} alignItems={'center'}>
                            <Box pr={2} py={1} display={'flex'} alignItems={'center'}>
                                <FormControl>
                                    <InputLabel id="dateProductCountLabel">رسته‌ها</InputLabel>
                                    <Select
                                        labelId="dateProductCountLabelLabelId"
                                        id="dateProductCountLabelLabelId"
                                        value={state.activeBox}
                                        onChange={(event) => {
                                            setState({
                                                ...state,
                                                activeBox: event.target.value
                                            })
                                        }}>
                                        <MenuItem
                                            value={-1}
                                            style={{
                                                paddingLeft: theme.spacing(1)
                                            }}>
                                            همه رسته‌ها - جدید
                                        </MenuItem>
                                        <MenuItem
                                            value={0}
                                            style={{
                                                paddingLeft: theme.spacing(1)
                                            }}>
                                            همه رسته‌ها - ویرایش
                                        </MenuItem>
                                        {dateProductCount.boxes.map(b => (
                                            <MenuItem
                                                key={b.id}
                                                value={b.id}
                                                onChange={() => {
                                                    setState({
                                                        ...state,
                                                        activeBox: b.id
                                                    })
                                                }}
                                                style={{
                                                    paddingLeft: theme.spacing(1)
                                                }}>
                                                {b.name}
                                            </MenuItem>
                                        ))}
                                    </Select>
                                </FormControl>
                                <RefreshIcon
                                    onClick={() => mutate(apiKey)}/>
                            </Box>
                        </Box>
                    </Box>
                )}/>}
        </React.Fragment>
    )
}


//region ProfitSummery

const ProfitSummeryLegendOpt = {
    display: true,
    position: 'top',
    fullWidth: true,
    reverse: false,
    labels: {
        fontColor: 'rgb(255, 99, 132)'
    }
};


const ProfitSummeryState = {
    mtProfit: {
        key: "mtProfit",
        label: "مهرتخفیف",
    },
    charity: {
        key: "charity",
        label: "خیریه",
    },
    post: {
        key: "post",
        label: "پست",
    },
    admin: {
        key: "admin",
        label: "ادمین",
    },
    dev: {
        key: "dev",
        label: "برنامه‌نویس",
    },
    supplier: {
        key: "supplier",
        label: "تامین کننده",
    },
    total: {
        key: "total_payment",
        label: "فروش کلی",
    },
    tax: {
        key: "tax",
        label: "ارزش‌افزوده",
    },
}


const storageKey = "ProfitSummeryKey"

function ProfitSummery() {
    return <Fragment/>

    const theme = useTheme();

    const c = useSelector(state => state);
    const {roll, boxes} = c.user
    const {lite} = c.base;
    const [startTime, setStartTime] = useState(undefined);
    const [endTime, setEndTime] = useState(undefined);
    const [activeType, setActiveType] = useState(LocalStorageUtils.get(storageKey + "activeType", ProfitSummeryState.mtProfit))
    const [typeSelector, setTypeSelector] = useState()

    const apiKey = `ProfitSummery-${activeType.key}-${startTime}-${endTime}-${lite}`;
    const {data, error} = useSWR(apiKey, () => {
        if (lite)
            return
        if (!(startTime && endTime)) {
            return undefined
        }
        return ControllerSite.Dashboard.profitSummery({startTime, endTime}).then(res => {
            let data = {};
            const id = [];
            const name = [];
            const mtProfit = [];
            const admin = [];
            const dev = [];
            const charity = [];
            const soldCount = []
            const totalPayment = []
            const supplierPrice = []
            const postPrice = []
            const tax = []
            const backgroundColor = [];
            const hoverBackgroundColor = [];
            _.forEach(res.data.boxes, (f) => {
                id.push(f.id);
                name.push(f.name[siteLang]);
                mtProfit.push(f.mt_profit)
                admin.push(f.admin)
                dev.push(f.dev)
                charity.push(f.charity)
                soldCount.push(f.sold_count)
                totalPayment.push(f.total_payment)
                supplierPrice.push(f.start_price)
                postPrice.push(f.post_price)
                tax.push(f.tax)
                backgroundColor.push(f.settings.color || grey[900])
                hoverBackgroundColor.push(f.settings.secondaryColor || grey[700])
            })

            let da = mtProfit;
            if (activeType === ProfitSummeryState.charity) {
                da = charity
            }
            if (activeType === ProfitSummeryState.admin) {
                da = admin
            }
            if (activeType === ProfitSummeryState.dev) {
                da = dev
            }
            if (activeType === ProfitSummeryState.post) {
                da = postPrice
            }
            if (activeType === ProfitSummeryState.supplier) {
                da = supplierPrice
            }
            if (activeType === ProfitSummeryState.total) {
                da = totalPayment
            }
            if (activeType === ProfitSummeryState.tax) {
                da = tax
            }


            data = {
                labels: name,
                more: {
                    id: id,
                    name,
                    mtProfit,
                    admin,
                    dev,
                    charity,
                    soldCount,
                    totalPayment,
                    supplierPrice,
                    tax,
                    backgroundColor,
                    hoverBackgroundColor
                },
                datasets: [
                    {
                        data: da,
                        backgroundColor: backgroundColor,
                        hoverBackgroundColor: hoverBackgroundColor
                    }
                ]
            }
            return {
                data: data,
                total: res.data.total
            }
        })
    }, {
        dedupingInterval: 8000000
    })


    const CreateItem = (label, value) => (
        <Box dsipalay={'flex'} flexDirextion={'column'} px={2} py={1} alignItems={'center'} justifyContent={'center'}>
            <Typography variant={"body1"} justifyContent={"center"}>
                {label}
            </Typography>
            <Typography variant={"h6"} justifyContent={"center"} pt={1}>
                {value}
            </Typography>
        </Box>
    )

    return (
        !lite ?
            <React.Fragment>
                {
                    !error &&
                    <ItemCard
                        minWidth={1 / 2}
                        p={1}
                        item={(
                            <Box
                                display={'flex'}
                                flexDirection={'column'} justifyContent={'center'}
                                p={1}
                                style={{
                                    border: `2px solid ${grey[300]}`,
                                    ...UtilsStyle.borderRadius(5)
                                }}>
                                {
                                    data ?
                                        <Pie data={data.data} legend={ProfitSummeryLegendOpt} redraw/> :
                                        <Skeleton height={350} component={"rect"}/>
                                }
                                <Box display={'flex'} width={1}>
                                    <Box width={1 / 3} display={'flex'} justifyContent={"center"}>
                                        <Box display={'flex'} alignItems={'center'}>
                                            <ButtonBase
                                                variant={"outlined"}
                                                onClick={e => setTypeSelector(e.currentTarget)}
                                                style={{
                                                    border: `1px solid ${cyan[300]}`,
                                                    ...UtilsStyle.borderRadius(5)
                                                }}>
                                                <Typography variant={"body1"} py={1} px={1}>
                                                    {activeType.label}
                                                    <ArrowDropDown style={{marginRight: theme.spacing(1)}}/>
                                                </Typography>
                                            </ButtonBase>
                                        </Box>
                                        <Menu
                                            anchorEl={typeSelector}
                                            keepMounted
                                            open={Boolean(typeSelector)}
                                            onClose={(e) => {
                                                setTypeSelector(undefined)
                                            }}>
                                            {
                                                Object.keys(ProfitSummeryState).map((ky, index) => {
                                                    const item = ProfitSummeryState[ky]
                                                    return (
                                                        (
                                                            <MenuItem
                                                                key={ky}
                                                                onClick={(e) => {
                                                                    setActiveType(item)
                                                                    LocalStorageUtils.set(storageKey + "activeType", item)
                                                                    setTypeSelector(undefined)
                                                                }}>
                                                                <Typography>{item.label}</Typography>
                                                            </MenuItem>
                                                        )
                                                    )
                                                })
                                            }
                                        </Menu>
                                    </Box>
                                    <DatePickerStartEndPanel
                                        width={2 / 3}
                                        key={storageKey}
                                        onChange={(startTime, endTime) => {
                                            setStartTime(startTime)
                                            setEndTime(endTime)
                                        }}/>
                                </Box>
                                {data ?
                                    <React.Fragment>
                                        <Box display={'flex'} flexWrap={'wrap'} pt={2} justifyContent={"center"}>
                                            {CreateItem("پرداخت کلی", UtilsFormat.numberToMoney(data.total.total_payment))}
                                            {CreateItem("مهرتخفیف", UtilsFormat.numberToMoney(data.total.mt_profit))}
                                            {CreateItem("ادمین", UtilsFormat.numberToMoney(data.total.admin))}
                                            {CreateItem("خیریه", UtilsFormat.numberToMoney(data.total.charity))}
                                            {CreateItem("برنامه‌نویس", UtilsFormat.numberToMoney(data.total.dev))}
                                            {CreateItem("تامین کننده", UtilsFormat.numberToMoney(data.total.start_price))}
                                            {CreateItem("پست", UtilsFormat.numberToMoney(data.total.post_price))}
                                        </Box>
                                        <Typography alignItems={"center"} justifyContent={"center"} variant={'h6'}
                                                    pt={1}>
                                            چکیده سود
                                            <RefreshIcon
                                                onClick={() => mutate(apiKey)}/>
                                            <ProfitSummeryTable data={data.data.more}/>
                                        </Typography>
                                    </React.Fragment> :
                                    <Skeleton height={150} component={"rect"}/>}
                            </Box>
                        )}/>
                }
            </React.Fragment> :
            <React.Fragment/>
    )
}


const ProfitSummeryHeaders = [
    headerItemTemplate.id,
    headerItemTemplate.name,
    createTableHeader({
        id: "soldCount",
        type: 'str',
        label: 'تعداد فروخته شده',
        dir: 'ltr',
        hasPadding: false,
        sortable: false
    }),
    createTableHeader({
        id: "mtProfit",
        type: 'str',
        label: 'مهرتخفیف',
        dir: 'ltr',
        hasPadding: false,
        sortable: false
    }),
    createTableHeader({
        id: "charity",
        type: 'str',
        label: 'خیریه',
        dir: 'ltr',
        hasPadding: false,
        sortable: false
    }),
    createTableHeader({
        id: "admin",
        type: 'str',
        label: 'ادمین',
        dir: 'ltr',
        hasPadding: false,
        sortable: false
    }),
    createTableHeader({
        id: "dev",
        type: 'str',
        label: 'برنامه‌نویس',
        dir: 'ltr',
        hasPadding: false,
        sortable: false
    }),
    createTableHeader({
        id: "supplierPrice",
        type: 'str',
        label: 'تامین کننده',
        dir: 'ltr',
        hasPadding: false,
        sortable: false
    }),
    createTableHeader({
        id: "tax",
        type: 'str',
        label: 'ارزش‌افزوده',
        dir: 'ltr',
        hasPadding: false,
        sortable: false
    }),
    createTableHeader({
        id: "totalPayment",
        type: 'str',
        label: 'فروش کلی',
        dir: 'ltr',
        hasPadding: false,
        sortable: false
    }),
];

function ProfitSummeryTable({data}) {
    const theme = useTheme();
    const [open, setOpen] = useState(false)


    const tableData = (data.id || []).map((fe, i) => (
        fe ? {
            id: {
                label: fe,
            },
            name: {
                label: (
                    <Typography variant={"h6"} fontWeight={500} dir={'ltr'} justifyContent={'flex-end'}>
                        {data.name[i]}
                    </Typography>
                ),
            },
            soldCount: {
                label: (
                    <Typography variant={"h6"} fontWeight={500} dir={'ltr'} justifyContent={'flex-end'}>
                        {data.soldCount[i]}
                    </Typography>
                )
            },
            mtProfit: {
                label: (
                    <Typography variant={"h6"} fontWeight={500} dir={'ltr'} justifyContent={'flex-end'}>
                        {UtilsFormat.numberToMoney(data.mtProfit[i])}
                    </Typography>
                )
            },
            admin: {
                label: (
                    <Typography variant={"h6"} fontWeight={500} dir={'ltr'} justifyContent={'flex-end'}>
                        {UtilsFormat.numberToMoney(data.admin[i])}
                    </Typography>
                )
            },
            dev: {
                label: (
                    <Typography variant={"h6"} fontWeight={500} dir={'ltr'} justifyContent={'flex-end'}>
                        {UtilsFormat.numberToMoney(data.dev[i])}
                    </Typography>
                )
            },
            charity: {
                label: (
                    <Typography variant={"h6"} fontWeight={500} dir={'ltr'} justifyContent={'flex-end'}>
                        {UtilsFormat.numberToMoney(data.charity[i])}
                    </Typography>
                )
            },
            totalPayment: {
                label: (
                    <Typography variant={"h6"} fontWeight={500} dir={'ltr'} justifyContent={'flex-end'}>
                        {UtilsFormat.numberToMoney(data.totalPayment[i])}
                    </Typography>
                )
            },
            tax: {
                label: (
                    <Typography variant={"h6"} fontWeight={500} dir={'ltr'} justifyContent={'flex-end'}>
                        {UtilsFormat.numberToMoney(data.tax[i])}
                    </Typography>
                )
            },
            supplierPrice: {
                label: (
                    <Typography variant={"h6"} fontWeight={500} dir={'ltr'} justifyContent={'flex-end'}>
                        {UtilsFormat.numberToMoney(data.supplierPrice[i])}
                    </Typography>
                )
            },
        } : {}
    ));

    return (
        <React.Fragment>
            <Tooltip title={"نمایش در جدول"}>
                <IconButton
                    onClick={() => setOpen(true)}
                    style={{
                        marginRight: theme.spacing(1)
                    }}>
                    <ReceiptOutlined/>
                </IconButton>
            </Tooltip>
            <BaseDialog open={open}
                        maxWidth={"xl"}
                        onClose={() => {
                            setOpen(false)
                        }}>
                <Box display={'flex'} width={1} flexDirection={'column'}>
                    <Table
                        title={"چکیده سود"}
                        addNewButton={<React.Fragment/>}
                        headers={ProfitSummeryHeaders}
                        data={tableData}
                        lastPage={0}
                        orderType={{}}
                        showActionPanel={false}/>
                </Box>
            </BaseDialog>
        </React.Fragment>
    )
}

//endregion ProfitSummery


function ItemCard({minWidth = 1 / 3, item, ...props}) {
    return (
        <Box minWidth={minWidth} p={2} {...props}>
            {
                isElement(item) ?
                    <React.Fragment>
                        {item}
                    </React.Fragment> :
                    _.isObject(item) ?
                        <Link toHref={item.rout} linkStyle={{width: '100%'}} style={{width: '100%'}}>
                            <BaseButton
                                style={{
                                    margin: 0,
                                    padding: 0,
                                    width: "100%"
                                }}>
                                <Box component={Card} width={1} py={4} px={2} display={'flex'}
                                     alignItems={'center'} justifyContent={'center'}
                                     flexDirection={'column'}
                                     style={{
                                         background: item.background
                                     }}>
                                    {item.icon}
                                    <Typography variant={'h4'} pt={2} px={3}>
                                        {item.title}
                                    </Typography>
                                </Box>
                            </BaseButton>
                        </Link>
                        : <React.Fragment/>
            }
        </Box>
    )
}


function RefreshIcon({onClick}) {
    const theme = useTheme();
    return (
        <Tooltip title={"بروزرسانی"}>
            <IconButton
                onClick={onClick}
                style={{
                    marginRight: theme.spacing(1)
                }}>
                <Refresh/>
            </IconButton>
        </Tooltip>
    )
}
