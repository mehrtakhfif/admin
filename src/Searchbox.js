import React from 'react';
import debounce from 'lodash.debounce';

export default class Searchbox extends React.Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.emitChangeDebounced = debounce(this.emitChange, 1000);
    }

    componentWillUnmount() {
        this.emitChangeDebounced.cancel();
    }

    render() {
        return (
            <input
                type="text"
                onChange={this.handleChange}
                placeholder="Search..."
                defaultValue={this.props.value}
            />
        );
    }

    handleChange(e) {
        // React pools events, so we read the value before debounce.
        // Alternately we could call `event.persist()` and pass the entire event.
        // For more info see reactjs.org/docs/events.html#event-pooling
        this.emitChangeDebounced(e.target.value);
    }

    emitChange(value) {
        console.log("value change -> " + value)
    }
}
