import React, {useEffect, useState} from 'react';
import './styles/App.css';
import "react-modern-calendar-datepicker/lib/DatePicker.css";
import {Provider} from 'react-redux'
import BaseSiteContainer from "./pages/BaseSiteContainer";
import store from "./redux/store";
import {checkCookieVersion, checkLocalStorageVersion, DEBUG} from "./repository";
import "./utils/ObjectUtils";


function App() {

    const [init, setInit] = useState(false);

    useEffect(() => {
        checkCookieVersion();
        checkLocalStorageVersion();
        setInit(true);
    }, []);

    return (
        <Provider store={store}>
            {init &&
            <BaseSiteContainer/>
            }
        </Provider>
    );

}


export default App;
