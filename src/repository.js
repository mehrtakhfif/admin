import createMuiTheme from "@material-ui/core/styles/createMuiTheme";
import Language from "./language/Language";
import {amber, cyan, green, grey, red} from "@material-ui/core/colors";
import createPalette from "@material-ui/core/styles/createPalette";
import moment from "moment-jalaali";
import CookiesUtils from "./utils/CookiesUtils";
import blueGrey from "@material-ui/core/colors/blueGrey";
import LocalStorageUtils from "./utils/LocalStorageUtils";


export const DEBUG = (false || ((!process.env.REACT_APP_NODE_ENV || process.env.REACT_APP_NODE_ENV === 'development') && true));


export const DEBUG_AUTH = DEBUG && false;
export const DEBUG_PERMISSION = DEBUG && false;
export const cookieVersion = '2';
export const localStorageVersion = '2';
export const newFeatures =(fun)=>{
    return [
        {
            v:"0.1.16",
            features:[
                "اضافه کردن حد اقل تعداد خرید",
                "اضافه کردن قیمت های بومگردی در انبار",
            ]
        },
        {
            v:"0.1.15",
            features:[
                "حذف باگ کتگوری"
            ]
        },{
            v:"0.1.14",
            features:[
                "اظافه کردن ci/cd"
            ]
        }, {
            v:"0.1.13",
            features:[
                "حذف باگ اضافه کردن کتگوری"
            ]
        },{
            v:"0.1.12",
            features:[
                "حذف باگ فیچر گروپ"
            ]
        },{
            v:"0.1.11",
            features:[
                "تغییر تنظیمات سایت بخش اسلایدر و اد"
            ]
        },
        {
            v:"0.1.10",
            features:[
                "حذف باکس"

            ]
        },
        {
            v:"0.1.9.1",
            features:[
                "حذف باگ های توضیحات"
            ]
        },{
            v:"0.1.9",
            features:[
                "حذف باگ های توضیحات",
                "دگمه موجودیت محصول",
                "افزودن انبار",
            ]
        }, {
            v:"0.1.8",
            features:[
                "حذف باگ ها",
            ]
        }, {
            v:"0.1.7",
            features:[
                "حذف باگ های توضیحات",
                "محدود کردن نام و توضیحات کوتاه",
                "اضافه شدن دگمه حذف همه تگ ها",
            ]
        }, {
            v:"0.1.6",
            features:[
                "تغییر متن بعد از ویرایش alt",
                "محدود کردن پرمالینک به 70 کاراکتر",
            ]
        },
        {
            v:"0.1.5",
            features:[
                "اضافه کردن تغییر متن alt در ویرایش محصول نسخه آزمایشی",
                "رفع باگ تامنیل محصول",
            ]
        },
        {
            v:"0.1.4",
            features:[
                "اضافه کردن شناسه به گروه فیچر در ویرایش محصول",
                "اضافه کردن توضیحات و عنوان سئو برای دسته بندی ها",
                "حذف طول کد برای سرویس ها",
            ]
        },
        {
            v:"0.1.3",
            features:[
                "رفع باگ حذف نشدن گروه تگ",
                "رفع باگ افزودن فیچر",
                "رفع باگ تغیر مقدار فیچر متنی",
            ]
        },
        {
            v:"0.1.2",
            features:[
                "افزودن زمان و روز عهای سرویس دهی و شماره تماس برای خدمات"
            ]
        },
        {
            v:"0.1.1",
            features:[
                "افزودن نقشه و آدرس برای خدمات",
                "رفع باگ حذف نشدن فیچر از فیچر گروه",
                "رفع باگ نمایش تگ در تگ و برند"
            ]
        },
        {
            v:"0.1",
            features:[
                "نسخه جدید افزودن و ویرایش محصول (آزمایشی)",
                "رفع باگ های جزئی"
            ]
        },
        {
            v:"0.0.83",
            features:[
                "تغییر توضیحات کوتاه در افزودن و ویرایش محصول"
            ]
        },
        {
            v:"0.0.82",
            features:[
                "رفع مشکل بازبینی محصول"
            ]
        },{
            v:"0.0.81",
            features:[
                "رفع مشکل گروه تگ در ویرایش محصول"
            ]
        },{
            v:"0.0.80",
            features:[
                "رفع مشکل مرتب سازی فیچر",
            ]
        },{
            v:"0.0.79",
            features:[
                "رفع مشکل باگ دسته بندی در ویرایش محصول",
                "اضافه کردن نام انبار در ویرایش گروهی محصول",
            ]
        },
        {
            v:"0.0.78",
            features:[
                "رفع مشکل خروج از انبار در صورت وجود ارور",
            ]
        },
        {
            v:"0.0.77",
            features:[
                "رفع مشکل انتخاب عکس محصول در انبار",
                "رفع مشکل ذخیره نشدن انبار",
                "رفع مشل عکس مدیا محصول در صفحه محصولات",
            ]
        },{
            v:"0.0.76",
            features:[
                "رفع مشکل در گروه فیچر در محصول",
                "رفع باگ گروه فیچر در صفحه گروه فیچر",
                " رفع باگ گروه فیچر در دسته بندی",
                "رفع مشکل انتخاب فیچر در انبار"
            ]
        },{
            v:"0.0.75",
            features:[
                "حذف امکان پاک کردن فیچر در گروه فیچر ",
                "ذفع مشکل باگ های ادیتور",
                "بهینه سازی سرعت صفحه محصول",
                "بهینه سازی سرعت ثفحه انبار",
                "رفع باگ های جزئی",
            ]
        },
        {
            v:"0.0.74",
            features:[
                "تغییر کلی بخش بازبینی محصول",
                "افزودن امکانات پیشرفته به بازبینی محصول",
            ]
        },
        {
            v:"0.0.73",
            features:[
                "رفع باگ در تغییر گروهی محصول",
                "رفع باگ های جزئی",
                (fun >= 3) ? "بهزاد اگه میتونی تشکر نکن  😈" : undefined
            ]
        },
        {
            v:"0.0.72",
            features:[
                "مرتبسازی فیچر در ویرایش محصول",
                "تغییر در ویرایشگر متنی",
                "توجه: مرتبسازی در گروه فیچر برای مدت کوتاهی غیر فعال میباشد.",
            ]
        },
        {
            v:"0.0.71",
            features:[
                "امکان انتخاب عکس انبار در ویرایش انبار",
                "حل مشکل وارد کردن فیچر متنی در صفحه محصول",
                "رفع مشکل تغییر قیمت گروهی",
                "افزوده شدن فیلتر فقط محصولات موجود به بخش تغغیر قیمت گروهی",
                "حل مشکل افزودن متن چند خطی به فیچر متنی",
            ]
        },
        {
            v:"0.0.70",
            features:[
                "نمایش دقیق‌تر قیمت گذاری در انبار",
                "فعال شدن مالیات از سود در انبار",
                "افزودن قابلیت متن چند خطی برای فیچر متنی",
                "حذف پیام: 'اطلاعاتی از پیش ذخیره شده برای این بخش یافت شد.'",
                "رفع باگ",
            ]
        },
        {
            v:"0.0.69",
            features:[
                "افزودن قابلیت رزرو به محصولات",
                "افزوده شدن فیلتر رزرو به فاکتور",
                "افزوده شدتن بخش رزرو به پنل"
            ]
        },
        {
            v:"0.0.68",
            features:[
                "افزوده شدن شرایط نگهداری و مقررات به ویرایش محصول",
                "رفع چند باگ جزئی",
            ]
        },
        {
            v:"0.0.67",
            features:[
                "رفع باگ در افزودن ویژگی های دسته بندی"
            ]
        },
        {
            v:"0.0.66",
            features:[
                "تکثیر انبار در ویرایش انبار (کلیک راست بر روی اسم انبار از پنل سمت چپ)",
                "افزوده شدن نوع فیچر در جست‌وجو فیچر",
                "افزودن نوع فیچر در صفحه ادیت پروداکت",
            ]
        },
        {
            v:"0.0.65",
            features:[
                `ویرایش تمام انبار های محصول در بخش "ویرایش انبار" ${fun ? "😊 🎉" : ""} `,
                "رفع باگ",
            ]
        },
        {
            v:"0.0.64",
            features:[
                "افزوده شدن ویراش گروهی انبار",
            ]
        },
    ]
}

export function checkCookieVersion() {
    const ck = LocalStorageUtils.get("cookieVersion", null);
    if (ck === cookieVersion)
        return;
    CookiesUtils.removeAll();
    LocalStorageUtils.set("cookieVersion", cookieVersion);
}

export function checkLocalStorageVersion() {
    const ck = LocalStorageUtils.get("localStorageVersion", null);
    if (ck === localStorageVersion)
        return;
    LocalStorageUtils.removeAll();
    LocalStorageUtils.set("localStorageVersion", localStorageVersion);
    LocalStorageUtils.set("cookieVersion", cookieVersion);
}

//region axios
export const webRout = process.env.REACT_APP_SITE_ROUT;
export const apiRout = process.env.REACT_APP_API;
export const mediaApiRout = process.env.REACT_APP_API+'/media';
export const adminApiRout = apiRout + '/admin';

//endregion axios

export const siteBackground = grey[100];
export const dir = "rtl";
export const isServer = () => !process.browser;
export const isClient = () => process.browser;
export const siteLang = 'fa';
export const lang = new Language(siteLang);


moment.loadPersian([]);


//region Theme
export let theme = createMuiTheme({
    direction: dir,
    palette: createPalette({
        primary: {
            light: cyan[400],
            main: cyan[800],
            dark: cyan[900]
        },
        secondary: {
            light: cyan[600],
            main: cyan["A700"],
            dark: cyan[900]
        }
    }),
    status: {},
    props: {
        MuiCard: {
            elevation: 2
        },
        MuiTooltip: {},
    },
        overrides: {
            MuiButton: {
                root: {
                    margin: "8px",
                    // padding: "px"
                }},
            MuiInput: {
                underline: {
                    "&:hover:not($disabled):not($error):not($focused):before": {
                        borderBottom: "1px solid rgba(0, 0, 0, 0.42)"
                    },
                    "&$focused:after": {
                        borderBottom: "1px solid rgba(0, 0, 0, 0.20)"
                    }
                }
            }
        }

});

//region fontSize
theme.typography.h1 = {
    ...theme.typography.h1,
    fontWeight: 300,
    fontSize: '3rem',
    [theme.breakpoints.up('lg')]: {
        fontSize: '3.8rem',
    },
    '@media (min-width:1299px)': {
        fontSize: '4rem',
    },
};
theme.typography.h2 = {
    ...theme.typography.h2,
    fontWeight: 300,
    fontSize: '2.2rem',
    [theme.breakpoints.up('lg')]: {
        fontSize: '2.3rem',
    },
    '@media (min-width:1299px)': {
        fontSize: '2rem',
    },
};
theme.typography.h3 = {
    ...theme.typography.h3,
    fontWeight: 300,
    fontSize: '1.9rem',
    [theme.breakpoints.up('lg')]: {
        fontSize: '2rem',
    },
    '@media (min-width:1299px)': {
        fontSize: '1.8rem',
    },
};
theme.typography.h4 = {
    ...theme.typography.h4,
    fontWeight: 300,
    fontSize: '1.65rem',
    [theme.breakpoints.up('lg')]: {
        fontSize: '1.8rem',
    },
    '@media (min-width:1299px)': {
        fontSize: '1.6rem',
    },
};
theme.typography.h5 = {
    ...theme.typography.h5,
    fontWeight: 300,
    fontSize: '1.2rem',
    [theme.breakpoints.up('lg')]: {
        fontSize: '1.4rem',
    },
    [theme.breakpoints.down("sm")]: {
        fontSize: '1.05rem !important',
    },
};
theme.typography.h6 = {
    ...theme.typography.h6,
    fontWeight: 300,
    fontSize: '1rem',
    [theme.breakpoints.up('lg')]: {
        fontSize: '1.1rem',
    },
    [theme.breakpoints.down("sm")]: {
        fontSize: '0.8rem !important',
    },
};
theme.typography.subtitle1 = {
    ...theme.typography.body1,
    fontSize: '0.9rem',
    fontWeight: 400,
    color: grey[800],
    [theme.breakpoints.up('lg')]: {
        fontSize: '1rem',
    },
    [theme.breakpoints.down("sm")]: {
        fontSize: '0.7rem !important',
    },
};
theme.typography.subtitle2 = {
    ...theme.typography.body2,
    fontWeight: 400,
    color: grey[800],
    fontSize: '0.76rem',
    [theme.breakpoints.up('lg')]: {
        fontSize: '0.9rem',
    },
    [theme.breakpoints.down("sm")]: {
        fontSize: '0.7rem !important',
    },
};

theme.typography.body1 = {
    ...theme.typography.body1,
    fontSize: '0.8rem',
    fontWeight: 300,
    [theme.breakpoints.up('lg')]: {
        fontSize: '0.9rem',
    },
    [theme.breakpoints.down("sm")]: {
        fontSize: '0.75rem',
    },
};
theme.typography.body2 = {
    ...theme.typography.body2,
    fontWeight: 300,
    fontSize: '0.71rem',
    [theme.breakpoints.up('lg')]: {
        fontSize: '0.8rem',
    },
    [theme.breakpoints.down("sm")]: {
        fontSize: '0.7rem',
    },
};
theme.typography.overline = {
    ...theme.typography.overline,
    fontWeight: 200,
    color: grey[800],
    fontSize: '0.65rem',
    [theme.breakpoints.up('lg')]: {
        fontSize: '0.71rem',
    },
    [theme.breakpoints.down("sm")]: {
        fontSize: '0.6rem',
    },
};
// theme.typography.button = {
//     ...theme.typography.button,
//     fontWeight: 200,
//     color: grey[800],
//     fontSize: '0.8rem',
//     [theme.breakpoints.up('md')]: {
//         fontSize: '1rem',
//     }
// };
theme.typography.caption = {
    ...theme.typography.caption,
    fontWeight: 200,
    color: grey[800],
    fontSize: '0.6rem',
    [theme.breakpoints.up('lg')]: {
        fontSize: '0.68rem',
    },
    [theme.breakpoints.down("sm")]: {
        fontSize: '0.55rem',
    },
};

//endregion


export const darkTheme = {
    ...theme,
    palette: createPalette({
        type: 'dark',
        primary: {
            light: blueGrey[200],
            main: blueGrey[400],
            dark: blueGrey[600]
        },
        secondary: {
            light: cyan[300],
            main: cyan["A700"],
            dark: cyan[600]
        }
    })
};
//endregion Theme


//region colors
export const colors = {
    primary: {
        light: red[500],
        main: red["A700"],
        dark: red[900]
    },
    success: {
        light: green[500],
        main: green["A700"],
        dark: green[900]
    },
    danger: {
        light: red[500],
        main: red["A700"],
        dark: red[900]
    },
    info: {
        light: amber[400],
        main: green["A700"],
        dark: green[700]
    },
    backgroundColor: {
        light: grey["50"],
        main: grey["100"],
        dark: grey["400"],
        darker: grey["600"],
    },
};

//endregion

export function getPageTitle(title) {
    return (
        lang.get("mehr_takhfif") + (title ? " | " + title : "")
    )
}

export function setPageTitle({title = "", withSiteName = true} = {}) {
    if (withSiteName) {
        title = getPageTitle(title)
    }
    document.title = title;
}


export const sLang = {
    fa: {
        key: 'fa',
        label: 'فارسی',
        shortLabel: 'FA',
    },
    en: {
        key: 'en',
        label: 'English',
        shortLabel: 'EN'
    },
    ar: {
        key: 'ar',
        label: 'عربی',
        shortLabel: 'AR'
    },
};

export function getActiveLanguage(lang = activeLang) {
    return sLang[lang]
}

export const activeLang = 'fa';

