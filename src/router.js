
export const rout = {
    User: {
        Auth: {
            login: "/login",
        },
        Profile: {
            rout: "/profile",
            createUser: `/profile/single`,
            editUser: (userId) => {
                return `${rout.User.Profile.createUser}/` + userId;
            },
            showUser: (userId) => {
                return `${rout.User.Profile.rout}/show/` + userId;
            }
        }
    },
    Product: {
        rout: '/products',
        Single: {
            rout: `/products/single`,
            createNewProduct: `/products/single`,
            editProduct: (productId) => {
                return `${rout.Product.Single.rout}/` + productId;
            }
            , ProductsNew: {
                rout: '/products/products_new',
                createNewProduct: `/products/products_new`,
                editProduct: (productId) => {
                    return `${rout.Product.Single.ProductsNew.rout}/` + productId;
                }
            },
            Refresh: {
                rout: `/products/single/refresh`,
                create: (productId) => {
                    return `${rout.Product.Single.Refresh.rout}/` + productId;
                }
            },
            MultiEdit: {
                rout: `/products/single/multi-edit`,
                multiEdit: () => {
                    return `${rout.Product.Single.MultiEdit.rout}`;
                }

            },
        },
        Review: {
            rout: '/products/review',
            Single: {
                rout: '/products/review/single',
                editProductReview: (productId) => {
                    return `${rout.Product.Review.Single.rout}/` + productId;
                },
            }
        }
    },
    Category: {
        rout: '/category',
        Single: {
            rout: `/category/single`,
            createNewProduct: `/category/single`,
            editCategory: (categoryId) => {
                return `${rout.Category.Single.rout}/` + categoryId;
            }
        },
        RelationCat: {
            rout: '/category/relation',
            Single: {
                rout: '/category/relation/single',
                createNewProduct: `/category/relation/single`,
                editCategory: (categoryId) => {
                    return `${rout.Category.RelationCat.Single.rout}/` + categoryId;
                }
            }
        }
    },
    Storage: {
        rout: '/storages',
        Single: {
            rout: `/storages/single`,
            showStorageDashboard: ({productId, storageId}) => {
                return `${rout.Storage.Single.rout}/` + (productId || `s-${storageId}`);
            },
            Create: {
                rout: `/storages/single/edit`,
                createStorage: (productId) => {
                    return `${rout.Storage.Single.Create.rout}/nan/` + productId;
                }
            },
            Edit: {
                rout: `/storages/single/edit`,
                editStorage: (storageId, {pricing, storages} = {}) => {
                    const type = (() => {
                        if (pricing)
                            return "pricing"
                        if (storages)
                            return "storages"
                        return undefined
                    })()
                    return `${rout.Storage.Single.Edit.rout}/` + storageId + (type ? `/${type}` : "");
                }
            },
            Code: {
                rout: `/storages/single/code`,
                editStorage: (storageId) => {
                    return `${rout.Storage.Single.Code.rout}/` + storageId;
                }

            },
            MultiEdit: {
                rout: `/storages/single/multi-edit`,
                multiEdit: (productId, {pricing, storages} = {}) => {
                    const type = (() => {
                        if (pricing)
                            return "pricing"
                        if (storages)
                            return "storages"
                        return "all"
                    })()
                    return `${rout.Storage.Single.MultiEdit.rout}/${productId}/${type}`;
                }
            },
            Refresh: {
                rout: `/storage/single/refresh`,
                create: (productId) => {
                    return `${rout.Storage.Single.Refresh.rout}/` + productId;
                }
            },
        }
    },
    Feature: {
        rout: '/feature',
        Single: {
            rout: `/feature/single`,
            createNewProduct: `/feature/single`,
            editFeature: (featureId) => {
                return `${rout.Feature.Single.rout}/` + featureId;
            }
        }
    },
    FeatureGroup: {
        rout: '/feature-group',
        Single: {
            rout: `/feature-group/single`,
            createNewProduct: `/feature-group/single`,
            editFeatureGroup: (featureId) => {
                return `${rout.FeatureGroup.Single.rout}/` + featureId;
            }
        }
    },
    Brand: {
        rout: '/brands',
    },
    Supplier: {
        rout: '/supplier',
        Single: {
            rout: `/supplier/single`,
            createNewSupplier: `/supplier/single`,
            editSupplier: (supplier) => {
                return `${rout.Supplier.Single.rout}/` + supplier;
            }
        }
    },
    Tags: {
        rout: '/tags',
        Single: {
            rout: `/tags/single`,
            createNewTags: `/tags/single`,
            editTag: (tagId) => {
                return `${rout.Tags.Single.rout}/` + tagId;
            }
        },
    },
    Main: {
        index: "/",
        dashboard: "/dashboard",
    },
    Box: {
        rout: '/box',
        Dashboard: {
            rout: '/box/boxDashboard'
        },
        SpecialProduct: {
            rout: '/box/special-product',
        },
        Templates: {
            rout: '/box/template',
        }
    },
    Invoice: {
        rout: '/invoice',
        Single: {
            show: `/invoice/show`,
            showInvoice: (invoiceId) => {
                return `${rout.Invoice.Single.show}/` + invoiceId;
            }
        }
    },
    InvoiceProduct: {
        rout: '/invoice-product',
    },
    Accountants: {
        rout: '/accountants',
    },
    Booking: {
        rout: '/booking',
    },
    Site: {
        rout: '/site',
        Dashboard: {
            rout: '/site/siteDashboard'
        },
        Ads: {
            rout: '/site/ads',
        },
        Slider: {
            rout: '/site/slider',
        },
    },
};
export default rout
export const siteRout = {
    User: {
        Auth: {
            login: "/login",
            signup: "/signup",
            verifyMobile: "/activate",
            resendCode: "/resend_code",
            resetPassword: "/reset-password",
        },
        Basket: {
            main: "/basket",
            Shopping: {
                main: "/shopping",
                Params: {
                    Details: {
                        param: "details",
                        rout: "/shopping/details"
                    },
                    Payment: {
                        param: "payment",
                        rout: "/shopping/payment"
                    },
                    Factor: {
                        param: "factor",
                        rout: "/shopping/factor"
                    },
                },
            },
        },
        Profile: {
            Main: {
                rout: "/profile",
                Params: {
                    Profile: {
                        param: "main",
                        rout: "/profile/main"
                    },
                    AllOrder: {
                        param: "all-order",
                        rout: "/profile/all-order"
                    },
                    Wishlist: {
                        param: "wishlist",
                        rout: "/profile/wishlist"
                    },
                    CommentsAndReview: {
                        param: "comment-and-review",
                        rout: "/profile/comment-and-review"
                    },
                    Addresses: {
                        param: "addresses",
                        rout: "/profile/addresses"
                    },
                    UserInfo: {
                        param: "user-info",
                        rout: "/profile/user-info"
                    },
                },
            },
            AddAddress: {
                rout: "/add-address",
            },
        }
    },
    Product: {
        Filter: {
            main: "/filter",
        },
        Single: {
            main: "/product",
            goTo: "/go-to-product",
            create: ({permalink}) => {
                return `/product/${permalink}`
            }
        }
    },
    Main: {
        home: "/",
        search: "/search",
        boxList: "/box-list"
    },
};

export const goBack = (routHistory, {defaultRout = rout.Main.home} = {defaultRout: rout.Main.home}) => {
    alert("check it goBack in router.js")
};
