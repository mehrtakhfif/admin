import {UserType} from "./Types";


export const userUpdate = ({user}) => ({
    type: UserType.USER.LOGIN,
    payload: {
        user: user,
    }
});

export const userLogout = () => ({
    type: UserType.USER.LOGOUT
});

