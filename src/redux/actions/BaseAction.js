import {BaseType} from "./Types";

export const setSiteMode = (darkMode) => ({
    type: BaseType.DARK_MODE,
    payload: {
        darkMode: darkMode,
    }
});

export const setLiteMode = (lite) => ({
    type: BaseType.LITE,
    payload: {
        lite: lite,
    }
});
