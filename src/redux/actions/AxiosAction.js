import {AxiosType} from "./Types";


export const startLoading = () => ({
    type: AxiosType.LOADING.START_LOADING,
});

export const finishLoading = () => ({
    type: AxiosType.LOADING.FINISH_LOADING,
});

