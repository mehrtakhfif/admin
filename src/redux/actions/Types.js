export const UserType = {
    USER: {
        LOGIN: "USER_LOGIN",
        LOGOUT: "USER_LOGOUT",

    },
};

export const BaseType = {
    LITE:"BASE_LITE",
        DARK_MODE: "BASE_DARK_MODE",
};

export const AxiosType = {
    LOADING: {
        START_LOADING: "START_LOADING",
        FINISH_LOADING: "FINISH_LOADING",
    }
}
