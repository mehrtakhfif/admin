import {combineReducers, createStore} from 'redux';
import BaseReducer from "./reducers/BaseReducer";
import UserReducer from "./reducers/UserReducer";

const store = createStore(combineReducers({
    base:BaseReducer,
    user: UserReducer,
}));

export default store;
