import {BaseType} from "../actions/Types";
import storage, {defaultBase} from "../../storage";

const initialState =(baseSiteCookie)=> {
    return{
        lite:baseSiteCookie[defaultBase.lite.key].val,
        darkMode:baseSiteCookie[defaultBase.darkMode.key].val
    }
};

export default function UserReducer(state = initialState(storage.Base.get()), action) {
    switch (action.type) {
        case BaseType.DARK_MODE: {
            const darkMode = action.payload.darkMode;
            storage.Base.DarkMode.set(darkMode)
            state = Object.assign({}, state, {
                ...state,
                darkMode:darkMode
            });
            break;
        }
        case BaseType.LITE: {
            const lite = action.payload.lite;
            storage.Base.Lite.set(lite)
            state = Object.assign({}, state, {
                ...state,
                lite:Boolean(lite)
            });
            break;
        }
        default: {
            break
        }
    }
    return state
}
