import {UserType} from "../actions/Types";
import JDate from "../../utils/JDate";
import {gcLog} from "../../utils/ObjectUtils";
import _ from "lodash"

const initialState = {
    isLogin: false
};

export default function UserReducer(state = initialState, action) {
    switch (action.type) {
        //region User
        case UserType.USER.LOGIN: {
            const user = action.payload.user;
            user.deadline = JDate.liveJalali().add(12, 'hour').unix();
            state = Object.assign({}, state, {
                ...user,
                isLogin: user && user.username,
            });
            break;
        }
        //endregion User
        default: {
            break
        }
    }
    return state
}
