import {AxiosType} from "../actions/Types";


const initialState = {
    loading: 0,
    totalRequest: 0,
};


export default function UserReducer(state = initialState, action) {
    switch (action.type) {
        //region Loading
        case AxiosType.LOADING.START_LOADING: {
            state = Object.assign({}, state, {totalRequest: state.totalRequest+1});
            break;
        }
        case AxiosType.LOADING.FINISH_LOADING: {
            state = Object.assign({}, state, {loading: (state.loading < state.totalRequest) ? state.loading+1 : state.loading});
            break;
        }
        //endregion Loading
        default: {
            break
        }
    }
    return state
}
