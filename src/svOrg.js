export function generateServerKey(firstKey, twiceKey) {
    const arr = [
        "2<Q3$]GDZYy;",
        "2<Q3$]GDZYy;",
        "2<Q3$]GDZYy;",
        "2<Q3$]GDZYy;",
        "2<Q3$]Gy;S/E",
        "2<Q3$]G;S/Ed",
        "2<Q3$]GDZYy;",
    ];
    const aab = [
        "S/Ed4sfgS4!PqX",
        "S/Ed)P{4S4!PFL",
        "S/Ed)P{4S4!PqX",
        "d)gP{4S4!PqXkF",
        ")P{4S4!PfqXkFL",
        "S/Ed)P{4SkFfgL",
        "S/Ed)P{4S4!PqX"
    ];
    const aaa = [
        "kFLy+='4?p2p",
        "kFLy+='4?p2p",
        "ssfy+='4?p2p",
        "kFLy+='4?p2}",
        "Ly+='4?p2aap",
        "y+='4?p2py}8",
        "y+='4?p42py}"
    ];
    const asz = [
        "y8a9:N&e[%!!@WSB&",
        "y}89:N&e[%!!@WSB&",
        "84a9:N&e[%!!@WSB&",
        "y}89:N&e[%!!@WSB&",
        "y}89:N&e[%!!@WSB&",
        "gfg9:N&e[%!!@WSB&",
        "8s9g:N&e[%!!@WSB&"
    ];

    return firstKey + twiceKey + arr[3] + aab[6] + aaa[1] + asz[4];
}

const keys = [
    "5jkLTfuPk*HzIddPWKUZ",
    "0Zt%89YF^2aJ!^bLhbVXy1K",
    "I1Q7oPkAvW!q&Xaxn",
    "OZzNYy6uMo%fksajfa",
    "665e3fFoYX6giJv",
    "@gDZiWVt615Oq1r&9A^2",
    "M@jlmkT3prs92#djU",
    "TJCGB3eOdXp$VQUIa7K",
    "znY4ePy66%WI%Sd0A",
    "OCAcmbDq$2hGZlM5j",
    "7s0ckDCj@OeT1tjxi",
    "ipPNeI$roYX^PT0Xl",
    "Mpr5*uZLkwjxWQH!X",
    "FmQo2w@@wBAUw7hb1",
    "Xp&*oAIQwi4Xv1qhz",
    "BtOZi#cS!gUjAA7tq",
    "a4!MDrh4sqetPA3U#",
    "P$^z7RMhX3Xoudcp%",
    "d*&ARRgFSlxW%ot#O",
    "KF3^a2%qWLNw7YbEY",
    "iz0gJ6uUSRrr!Y%$y",
    "R^ne@OoqO%&1l*@%O",
    "&BvEHvi^mmJ%0ZqN6",
    "1TLAwr8488E*nKJzC",
    "fW5tIHJuudp!3TFMf",
    "Im@Qjzpv9KCk%RMDf",
    "P6UgnL*!@fEnQW3Y5",
    "gACGUbP2!#OjSjiGW",
    "t2SDH%D&81cFtH@Du",
];

export const sitePrivateKeys = {
    uniqueHSACode: keys[16] + "3ql&lBHa%i#Jb@PQn" +keys[24]+ "rp5dEv@*GaYe*&au1",
    uniqueCookieCode: "qQyxhr$%9o86MSRJz" + keys[8] +keys[16]+"k3Nn8Xh7%zTsP2HIP"+keys[25]+"8E&07CCUEwyG75H3w",
    uniqueLocalStorage: keys[9] + keys[6] +"u@AMdB$0pzLND*^X8"+keys[22]+"zw^STesbZ4HGrabwi",
};

export const adminPrivateKeys = {
    uniqueHSACode: keys[4] + "nsfakhZdhsjahS6h9px" +keys[16]+ "Y$DD!ydiBa%BUG#bpTXX^V7XWpq",
    uniqueCookieCode: "quL4rXp4MXs#cVh" + keys[22] +keys[8]+"h@$qRiopXHPceL1%"+keys[15]+"@k",
    uniqueLocalStorage: keys[25] + keys[13] +"B2F&cI8*aUXXg"+keys[3]+"5vMZ60^7BKiO8!BTWsU4U9",
};

export const siteOtherKey = {
    user: `ULpUW&qTqB0VCg${keys[25]}safh9J8^Hkf99${keys[21]}JF@iEBpDS6F${keys[2]}4FYoj%%sdW4CS`,
    userIsLogin: `6x3r8JZ%n$HuM${keys[0]}46alsfgy8@sx&SbB7${keys[3]}W!uQtWwU#Z5NK@${keys[0]}6sufy5X4fP${keys[15]}Eyz`,
    basket: `Z@zky6p&GKQCJR${keys[7]}Q#p#&jksfasf869${keys[22]}82HYTm#ug6${keys[3]}YZX^xer${keys[1]}3f`,
    counter: `lq$E4FU${keys[13]}F2^186cxe${keys[18]}T&@Rkzgzdy${keys[3]}R$N%`,
    fcmToken: `lq$EWfdasU${keys[5]}F2^safasf${keys[22]}T&@Rkzgzdy${keys[1]}R$N%`,
};

export const adminOtherKey = {
    user: `ULpUW&qTqB0VCg${keys[16]}J8^Hkf99$PU1w4n${keys[7]}p*w52p2s${keys[22]}%%sdW4CS`,
    userIsLogin: `6x3r${keys[12]}safa8746alsfgy8@sx&S${keys[3]}!W!uQtWwU#Z5${keys[21]}sufy5X4fPgYeYJw5Eyz`,
    boxes: `Z@zky6p&G${keys[6]}pEsek4#6jQ#p#&j${keys[16]}GmV&7X8${keys[26]}8#YZX^xeryDM3rd3f`,
    pageSetting: `lq$E4${keys[13]}%ZSOzF2^186cxeZ%7PU4${keys[6]}5bf2386eR$N%`,
    base: `lq${keys[3]}p%*c%Z${keys[27]}ak657PU4jZTT&${keys[12]}6eR$N%`,
};
