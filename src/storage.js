import _ from "lodash";
import {adminOtherKey} from "./sv";
import LocalStorageUtils from './utils/LocalStorageUtils'
import {gcLog} from "./utils/ObjectUtils";
import packageJson from "../package.json";


export const defaultBase = {
    darkMode: {
        key: "darkMode",
        val: false
    },
    lite: {
        key: "lite",
        val: false
    },
    drawer: {
        key: "drawer",
        val: {
            open: false
        }
    },
    siteNewFeature: {
        key: "siteNewFeature",
        val: {
            checked: false,
            version: undefined
        }
    }
};

const defaultPageSetting = {
    activeBox: undefined,
    rowPerPage: 10,
    order: {
        asc: true,
        orderBy: 'id'
    },
    page: 0
};

const helper = {
    Base: {
        get: () => {
            try {
                return LocalStorageUtils.get(adminOtherKey.base, defaultBase)
            } catch (e) {
                return defaultBase
            }
        },
        set: (key, val) => {
            try {
                const b = helper.Base.get();
                // noinspection TypeScriptValidateTypes
                b[key] = val;
                return LocalStorageUtils.set(adminOtherKey.base, b)
            } catch (e) {
            }
        },
        Lite: {
            get: () => {
                try {
                    return helper.Base.get().lite.val
                } catch (e) {
                    try {
                        return defaultBase.lite.val
                    } catch (e) {
                        return false
                    }
                }
            },
            set: (lite) => {
                try {
                    return helper.Base.set("lite", {
                        ...defaultBase.lite,
                        val: lite
                    })
                } catch (e) {
                }
            },
        },
        DarkMode: {
            get: () => {
                try {
                    return helper.Base.get().darkMode.val
                } catch (e) {
                    return defaultBase.darkMode.val
                }
            },
            set: (darkMode) => {
                return helper.Base.set("darkMode", {
                    ...defaultBase.darkMode,
                    val: darkMode
                })
            },
        },
        Drawer: {
            getOpen: () => {
                try {
                    return helper.Base.get().drawer.val.open
                } catch (e) {
                    return defaultBase.drawer.val.open
                }
            },
            setOpen: (open) => {
                return helper.Base.set("drawer", {
                    ...defaultBase.drawer,
                    val: {
                        open: open
                    }
                })
            },
        },
        SiteNewFeature: {
            getChecked: () => {
                try {
                    const it = helper.Base.get().siteNewFeature.val;
                    return it.checked && it.version === packageJson.version
                } catch (e) {
                    return defaultBase.siteNewFeature.val.checked
                }
            },
            setChecked: () => {
                return helper.Base.set(defaultBase.siteNewFeature.key, {
                    ...defaultBase.siteNewFeature,
                    val: {
                        ...defaultBase.siteNewFeature.val,
                        checked: true,
                        version: packageJson.version
                    }
                })
            },
        },

    },
    PageSetting: {
        getPageSetting: (key) => {
            const pi = LocalStorageUtils.get(adminOtherKey.pageSetting, {});
            if (!key)
                return pi;
            if (pi[key]) {
                return pi[key];
            }
            return defaultPageSetting;
        },
        setPageSetting: (key, {activeBox, rowPerPage, order, page, ...props}) => {
            const pi = helper.PageSetting.getPageSetting();
            if (!pi[key]) {
                pi[key] = defaultPageSetting
            }
            if (activeBox)
                pi[key].activeBox = activeBox;
            if (rowPerPage)
                pi[key].rowPerPage = rowPerPage;
            if (order)
                pi[key].order = order;
            if (_.isNumber(page))
                pi[key].page = page;
            if (props) {
                pi[key] = {
                    ...pi[key],
                    ...props
                }
            }
            return LocalStorageUtils.set(adminOtherKey.pageSetting, {...pi,})
        },
        getActiveBox: (key, boxes, returnObj) => {
            const b = helper.PageSetting.getPageSetting(key).activeBox;

            if (_.isArray(boxes)) {
                const i = _.findIndex(boxes, (box) => {
                    return box.id === b
                });
                if (!b || boxes.length === 1 || i === -1) {
                    return returnObj ? boxes[0] : boxes[0].id;

                }
                return returnObj ? boxes[i] : boxes[i].id

            }
            return b;
        },
        setActiveBox: (key, {activeBox}) => {
            return helper.PageSetting.setPageSetting(key, {activeBox: activeBox})
        },
        getRowPerPage: (key) => {
            return helper.PageSetting.getPageSetting(key).rowPerPage;
        },
        setRowPerPage: (key, {rowPerPage}) => {
            return helper.PageSetting.setPageSetting(key, {rowPerPage: rowPerPage})
        },
        getOrder: (key) => {
            return helper.PageSetting.getPageSetting(key).order;
        },
        setOrder: (key, {order}) => {
            return helper.PageSetting.setPageSetting(key, {order: order})
        },
        getPage: (key) => {
            return helper.PageSetting.getPageSetting(key).page;
        },
        setPage: (key, {page}) => {
            return helper.PageSetting.setPageSetting(key, {page: page})
        },

    },
};

export default helper;
