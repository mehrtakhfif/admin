export default {
    "login": "login",
    "mobile_number": "mobile number",
    "password": "password",
    "password_hint": "Password - minimum length 6 characters",
}
