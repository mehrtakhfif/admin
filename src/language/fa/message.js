export default {
    "me_virtual_reserved_product": 'کالا های در سبد خرید شما بشکل مجازی رزرو شده است و امکان ناموجود شدن در آینده را دارد. درصورت نیاز میتوانید با ادامه سفارش کالا را خریداری کنید.',
    "me_shipping_cost": 'هزینه ارسال وابسته به مکان آدرس وارد شده شما و محصول های انتخابی متغییر میباشد.',
    "me_address_editing": 'شما در حال ویرایش و ثبت آدرس هستید.',
    "me_logout_successfully": 'شما با موفقیت خارج شدید.',
    "me_login_required": 'لطفا وارد شوید.',
    "me_product_add_to_basket_successfully": 'محصول با موفقیت به سبد خرید افزوده شد.',
    "me_product_not_add_to_basket_successfully": 'متاسفانه محصول به سبد خرید اضافه نشد.',
    "me_add_address_successful": 'آدرس جدید با موفقیت ثبت شد.',
    "me_select_ipg":'لطفا یکی از درگاه های بانکی زیر را انتخاب کنید.',
    "me_product_is_removed":'محصول حذف شد.',
    "me_details_successfully_update":'اطلاعات با موفقیت بروز شد.',
    "me_remove_address":"آیا از حذف این آدرس اطمینان دارید؟",
    "me_remove_product_on_basket":"آیا از حذف این محصول اطمینان دارید؟",

    //**************************************************

}
