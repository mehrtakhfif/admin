import _ from 'lodash'
import fa from "./fa"
import en from "./en"
import React from "react";

class Language {
    constructor(lan = "fa", fallback = "fa") {
        this.lan = lan;
        this.fallback = fallback;
    }

    get(key, args = null, convertTo = null) {
        try {
            if ((!convertTo && this.lan === "en") || (convertTo === "en")) {
                return preparing(en[key], args)
            }
            return preparing(fa[key], args)
        } catch (e) {
            return 'ss'
        }
    }
}

function preparing(string, args) {

    if (!args) {
        return setTag(string);
    }

    const regex = RegExp('{(\\w[^\\}]*)}');
    while (regex.test(string)) {
        const result = regex.exec(string);
        if (!result[0])
            break;
        let val = args[result[1]];
        if (_.isString(val)) {
            string = string.replace(result[0], val);
            continue;
        }
        if (_.isObject(val)) {
            val = setTypeFace(val);
            string = string.replace(result[0], val);
        }
    }
    return setTag(string);
}

function setTypeFace(string) {
    if (string.bold) {
    }
    if (string["italic"]) {
    }
    return string.val
}

function setTag(string) {
    const sl = string.split("[br]");
    if (sl.length ===1)
        return  string
    let element = (<></>);
    _.forEach(sl, (s, index) => {
        element = (
            <>
                {element}
                {s}
                {(index + 1 !== sl.length) &&<br/>}
            </>
        );
    });
    return element
}

export default Language;


// export const lang = {
//     get LOGIN() {
//         const lang = {
//             "fa": "ورود",
//             "en": "Login"
//         };
//         return get(lang)
//     },
//     get MOBILE_NUMBER() {
//         const lang = {
//             "fa": "شماره موبایل",
//             "en": "Mobile Number"
//         };
//         return get(lang)
//     }
// };



